#Keep raw ,drawable,layout
-verbose
-keepresourcefiles res/raw/**
-keepresources raw/**
-keepresources drawable/**
-keepresources layout/**
#Encrypt API KEY
-encryptstrings class co.real.productionreal2.config.AppConfig
-encryptclasses co.real.productionreal2.config.AppConfig
#Keep api model
-keep class co.real.productionreal2.model.** { *; }
-keepclassmembers class co.real.productionreal2.model.** { *; }
-keep class co.real.productionreal2.service.** { *; }
-keepclassmembers class co.real.productionreal2.service.** { *; }
-keep class co.real.productionreal2.dao.** { *; }
-keepclassmembers class co.real.productionreal2.dao.** { *; }
-keep class io.branch.indexing.** { *; }

#Keep Quickblox
-keep class org.jivesoftware.smack.initializer.VmArgInitializer { public *; }
-keep class org.jivesoftware.smack.ReconnectionManager { public *; }
-keep class com.quickblox.module.c.a.c { public *; }
-keep class com.quickblox.module.chat.QBChatService { public *; }
-keep class com.quickblox.module.chat.QBChatService.loginWithUser { public *; }
-keep class com.quickblox.module.chat.listeners.SessionCallback { public *; }
-keep class com.quickblox.chat.model.**{ *;}
-keepclassmembers class com.quickblox.chat.model.**{ *;}
-keep class * extends org.jivesoftware.smack { public *; }
-keep class org.jivesoftware.smack.** { public *; }
-keep class org.jivesoftware.smackx.** { public *; }
-keep class com.quickblox.** { public *; }
-keepclassmembers class com.quickblox.**{ *;}
-keep class * extends org.jivesoftware.smack { public *; }
-keep class * implements org.jivesoftware.smack.debugger.SmackDebugger { public *; }

#Fabric
-keepattributes *Annotation*
-keepattributes SourceFile,LineNumberTable
-keep public class * extends java.lang.Exception

# uCrop
-dontnote com.yalantis.**
-keep class com.yalantis.** { *; }
