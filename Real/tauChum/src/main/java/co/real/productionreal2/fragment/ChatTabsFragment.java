package co.real.productionreal2.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListPopupWindow;
import android.widget.SimpleAdapter;

import co.real.productionreal2.R;
import co.real.productionreal2.activity.MainActivityTabBase;
import co.real.productionreal2.newsfeed.BaseNewsFeedFragment;
import co.real.productionreal2.newsfeed.NewsFeedDetailFragment;
import co.real.productionreal2.newsfeed.NewsFeedProfileFragment;
import co.real.productionreal2.service.model.AgentProfiles;
import co.real.productionreal2.util.AppUtil;
import co.real.productionreal2.util.ImageUtil;
import co.real.productionreal2.view.TabMenu;
import co.real.productionreal2.view.TitleBar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import widget.EnbleableSwipeViewPager;
import widget.InterceptableRelativeLayout;
import widget.viewpagerindicator.PageIndicator;

public class ChatTabsFragment extends BaseNewsFeedFragment {
    private static final String TAG = "ChatTabsFragment";
    public LinearLayout pagerIndicator;
    @InjectView(R.id.viewpager)
    EnbleableSwipeViewPager pager;
    @InjectView(R.id.indicator)
    PageIndicator mIndicator;
    @InjectView(R.id.mInterceptableRelativeLayout)
    InterceptableRelativeLayout mInterceptableRelativeLayout;

    FragmentPagerAdapter chatPagerAdapter;

    private static final String TITLE = "title";
    private List<HashMap<String, Object>> data = new ArrayList<HashMap<String, Object>>();
    private TitleBar mTitlebar;
    private TabMenu menuTabHost;

    public static AgentProfiles agentProfile;
//    private FragmentTabHost menuTabHost;

    NewsFeedDetailFragment newsFeedDetailFragment;
    NewsFeedProfileFragment newsFeedProfileFragment;
    ChatLobbyFragment chatLobbyFragment;

    public static ChatTabsFragment newInstance() {

        Bundle args = new Bundle();

        ChatTabsFragment fragment = new ChatTabsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_following_tabs,
                container, false);
        ButterKnife.inject(this, view);

        pagerIndicator = (LinearLayout)view.findViewById(R.id.llChatPagerIndicator);

        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //init title bar
        mTitlebar = ((MainActivityTabBase) getActivity()).getTitleBar();
        menuTabHost = ((MainActivityTabBase) getActivity()).getTabHost();
        addItem(getString(R.string.reportpage__report));

        chatPagerAdapter = new ChatPagerAdapter(getChildFragmentManager(), getFragments());
        final int screenWidth = ImageUtil.getScreenWidth(getActivity());
        pager.setAdapter(chatPagerAdapter);
        pager.setOffscreenPageLimit(5);
        mIndicator.setViewPager(pager);
//        pager.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                if (ChatLobbyFragment.ableToSwipe)
//                    return false;
//                else {
////                    Toast.makeText(getActivity(), "This is not Agent", Toast.LENGTH_SHORT).show();
//                    pager.setScrollX(screenWidth);
//                    if (_rxBus.hasObservers()) {
//                        _rxBus.send(new BusSelectedEvent.ChatLobbyPagerStateEvent(pager
//                                .getCurrentItem(), ViewPager.SCROLL_STATE_SETTLING));
//                    }
//                    return true;
//                }
//
//            }
//        });
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            int oldPosition = 1;
            int currentPosition = 1;

            @Override
            public void onPageScrolled(int position, float positionOffset,
                                       int positionOffsetPixels) {
                currentPosition = position;
                if (position == 0) {
                    chatLobbyFragment.setCoverViewAlpha((1f - positionOffset) * 5);
                    newsFeedDetailFragment.updateUI();
                } else if (position == 1) {
                    chatLobbyFragment.setCoverViewAlpha((positionOffset) * 5);
                    newsFeedProfileFragment.updateUI(false);
                }
            }

            @Override
            public void onPageSelected(int position) {
                chatLobbyFragment.isShowing = false;
                if (position == 0) {
                    //Details
                    mTitlebar.setTitle(newsFeedDetailFragment.getTitileName());
                    mTitlebar.setupTitleBar(TitleBar.TAB_TYPE.NEWSFEED_DETAILS);
                    mTitlebar.getEditBtn().setVisibility(View.VISIBLE);
                    mTitlebar.getEditChatLobbyBtn().setVisibility(View.GONE);
                    mTitlebar.getEditBtn().setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            showListMenu(v, 4);
                        }
                    });

                    pager.setSwipeable(true);
                    newsFeedDetailFragment.setFollowing();

                } else if (position == 2) {
                    //Agent Profile
                    mTitlebar.setupTitleBar(TitleBar.TAB_TYPE.NEWSFEED_PROFILE);
                    mTitlebar.getEditBtn().setVisibility(View.VISIBLE);
                    mTitlebar.getEditChatLobbyBtn().setVisibility(View.GONE);
                    mTitlebar.getEditBtn().setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            showListMenu(v, 2);
                        }
                    });

                    pager.setSwipeable(true);
                    newsFeedProfileFragment.setFollowing();

                } else if (position == 1) {
                    //List view
                    mTitlebar.setupTitleBar(TitleBar.TAB_TYPE.CHAT);
                    if (!ChatLobbyFragment.searchingModeText.isEmpty()) {
                        mTitlebar.getTvTitle().setVisibility(View.GONE);
                        mTitlebar.getChatSearchLinearLayout().setVisibility(View.VISIBLE);
//                        mTitlebar.getAgentSearchBtn().setVisibility(View.GONE);
//                        mTitlebar.getSearchAgentEditText().setText(ChatLobbyFragment.searchingModeText);
//                        mTitlebar.getSearchAgentEditText().requestFocus();
//                        mTitlebar.getEditBtn().setVisibility(View.GONE);
//                        mTitlebar.getEditChatLobbyBtn().setVisibility(View.VISIBLE);
//                        AppUtil.showKeyboard(getActivity(), mTitlebar.getSearchAgentEditText());
                    }

                    pager.setSwipeable(false);
                    chatLobbyFragment.isShowing = true;
                    chatLobbyFragment.notifyDataSetChanged();

                }

                oldPosition = position;

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                /*SCROLL_STATE_IDLE
                 *SCROLL_STATE_DRAGGING
				 *SCROLL_STATE_SETTLING */
                Log.w(TAG, "state = " + state);
                chatLobbyFragment.viewPagerScrollState = state;

                if (state == ViewPager.SCROLL_STATE_IDLE) {
                    if (currentPosition == 2 || currentPosition == 0) {
                        ImageUtil.collapse(menuTabHost);
                    }else
                    if(currentPosition == 1){
                        pager.setSwipeable(false);
                        ((MainActivityTabBase)getActivity()).setTabClickEnable(true);
                    }
                } else if (state == ViewPager.SCROLL_STATE_DRAGGING) {
                    ((MainActivityTabBase)getActivity()).setTabClickEnable(false);
                    if (oldPosition == 0 || oldPosition == 2) {
                        ImageUtil.expand(menuTabHost);
                    }

                }
//                if (_rxBus.hasObservers()) {
//                    _rxBus.send(new BusSelectedEvent.ChatLobbyPagerStateEvent(pager
//                            .getCurrentItem(), state));
//                }

            }
        });
        pager.setCurrentItem(1);
    }

    private List<Fragment> getFragments() {
        List<Fragment> fList = new ArrayList<Fragment>();

        newsFeedDetailFragment = NewsFeedDetailFragment.newInstance("Fragment 1", pager, pagerIndicator,false);
        newsFeedProfileFragment = NewsFeedProfileFragment.newInstance("Fragment 3", pager);
        chatLobbyFragment = ChatLobbyFragment.newInstance("Fragment 2", mInterceptableRelativeLayout, pager, newsFeedDetailFragment, newsFeedProfileFragment);

        fList.add(newsFeedDetailFragment);
        fList.add(chatLobbyFragment);
        fList.add(newsFeedProfileFragment);
        return fList;
    }

    // Call this when you want to show the ListPopupWindow
    private void showListMenu(View anchor, final int problemType) {
        final ListPopupWindow popupWindow = new ListPopupWindow(context);

        ListAdapter adapter = new SimpleAdapter(
                context,
                data,
                R.layout.menu_list_item,
                new String[]{TITLE}, // These are just the keys that the data uses
                new int[]{android.R.id.text1}); // The view ids to map the data to

        popupWindow.setAnchorView(anchor);
        popupWindow.setAdapter(adapter);
        popupWindow.setContentWidth(AppUtil.measureContentWidth(context, adapter));
        popupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int memId;
                int listingId = 0;
                if (agentProfile != null) {
                    memId = agentProfile.memberID;
                    listingId = problemType == 4 ? agentProfile.agentListing.listingID : 0;
                }
//                callReportApi(problemType, memId, listingId);
                popupWindow.dismiss();
            }
        }); // the callback for when a list item is selected
        popupWindow.show();
    }

    // Use this to add items to the list that the ListPopupWindow will use
    private void addItem(String title) {
        data = new ArrayList<HashMap<String, Object>>();
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put(TITLE, title);
//        map.put(ICON, 0);
        data.add(map);
    }

    public static class ChatPagerAdapter extends FragmentPagerAdapter {
        public static final int CHAT_DETAILS = 0;
        public static final int CHAT_LOBBY = 1;
        public static final int CHAT_PROFILE = 2;
        private List<Fragment> fragments;

        public ChatPagerAdapter(FragmentManager fragmentManager, List<Fragment> fragments) {
            super(fragmentManager);
            this.fragments = fragments;
        }

        // Returns total number of pages
        @Override
        public int getCount() {
            return fragments.size();
        }

        // Returns the fragment to display for that page
        @Override
        public Fragment getItem(int position) {
            return this.fragments.get(position);

        }

        // Returns the page title for the top indicator
        @Override
        public CharSequence getPageTitle(int position) {
            return "Page " + position;
        }

    }

}
