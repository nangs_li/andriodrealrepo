package co.real.productionreal2.callback;

import com.quickblox.chat.exception.QBChatException;
import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.chat.model.QBDialog;
import com.quickblox.users.model.QBUser;

import org.jivesoftware.smack.XMPPConnection;

import java.util.List;

import co.real.productionreal2.dao.account.DBQBChatDialog;
import co.real.productionreal2.dao.account.DBQBChatMessage;

/**
 * Created by hohojo on 23/7/2015.
 */
public interface UpdateChatViewListener {

    public void updateChatView(List<DBQBChatMessage> dbqbChatMessageList,  boolean needScrolldown);
//    public void fetchUnReadOrUnSentUserMsgDbActionAndToQb();
    public void statusAndTypingTextViewOnChange(int textColor, String textContent);
    public void firstCreateDialogSuccess(QBDialog qbDialog, String msgTextOrPhotoPath, int type);

    public void receivedMsg( QBChatMessage chatMessage);
    public void processError(QBChatMessage originChatMessage, QBChatException error);
    public void processMessageDelivered(String msgId, String dialogId);
    public void processMessageRead(String msgId, String dialogId);

    public void connected(XMPPConnection connection);
    public void authenticated(XMPPConnection connection);
    public void connectionClosed();
    public void connectionClosedOnError(Exception e);
    public void reconnectingIn(int seconds);
    public void reconnectionSuccessful();
    public void reconnectionFailed(Exception error);


    // system message
    public void processSystemMessageChatSetting(QBChatMessage qbChatMessage, DBQBChatDialog dbqbChatDialog);
    public void processSystemMessageError(QBChatException e, QBChatMessage qbChatMessage);
    public void processSystemMessageChatPrivacy(QBChatMessage qbChatMessage, QBUser qbUser);


}
