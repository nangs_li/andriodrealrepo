package co.real.productionreal2;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.NotificationManager;
import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.gson.Gson;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.core.LogLevel;
import com.quickblox.core.QBSettings;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

import co.real.productionreal2.activity.MainActivityTabBase;
import co.real.productionreal2.activity.chat.ChatActivity;
import co.real.productionreal2.activity.createpost.CreatePostPhotosActivity;
import co.real.productionreal2.activity.login.IntroActivity;
import co.real.productionreal2.activity.login.SignUpInfoActivity;
import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.chat.ChatDbViewController;
import co.real.productionreal2.chat.ChatSessionUtil;
import co.real.productionreal2.chat.pushnotifications.GCMIntentService;
import co.real.productionreal2.common.LocalStorageHelper;
import co.real.productionreal2.config.AppConfig;
import co.real.productionreal2.dao.account.DBChatroomAgentProfiles;
import co.real.productionreal2.dao.account.DBQBChatDialog;
import co.real.productionreal2.dao.account.DBQBChatMessage;
import co.real.productionreal2.dao.account.DBQBUser;
import co.real.productionreal2.daomanager.DatabaseManager;
import co.real.productionreal2.service.model.AgentProfiles;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.response.ResponseVersion;
import co.real.productionreal2.service.model.request.RequestFollowAgent;
import co.real.productionreal2.service.model.request.RequestVersion;
import co.real.productionreal2.service.model.request.RequestWriteViralLog;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;
import co.real.productionreal2.util.AppUtil;
import co.real.productionreal2.util.CryptLib;
import co.real.productionreal2.util.StringUtils;
import co.real.productionreal2.util.TimeUtils;
import io.branch.referral.Branch;
import io.branch.referral.Defines;
import widget.RoundedImageView;


//import com.sina.weibo.sdk.component.WeiboSdkBrowser;
//import com.twitter.sdk.android.Twitter;
//import com.twitter.sdk.android.core.TwitterAuthConfig;

//import io.fabric.sdk.android.DefaultLogger;
//import io.fabric.sdk.android.Fabric;

public class ApplicationSingleton extends MultiDexApplication {
    private static final String TAG = "ApplicationSingleton";

    private static ApplicationSingleton instance;
    private AlertDialog updateDialog = null;
    public Activity currentActivity;
    private boolean applicationDidEnterBackground = true;
    public boolean isBackgroundBefore = false;
    private boolean needForceUpdate = false;
    private String updateURL = null;
    public int viralType = 3;//1 = Invite to Download , 2 = Invite to Chat , 3 = Invite to Follow

    private boolean aboutToShowChatNoti;
    private int incomingMessageCount;
    private Handler showChatNotiHandler;

    public static ApplicationSingleton getInstance() {
        return instance;
    }

    public void checkIsInvited() {
        SharedPreferences prefs = getSharedPreferences(Constants.PREF_BRANCH, MODE_PRIVATE);
        if (prefs == null)
            return;
        String branchJson = prefs.getString(Constants.PREF_BRANCH_OBJECT_JSON, null);
        if (branchJson == null || branchJson.isEmpty())
            return;
        final JSONObject branchObject = new Gson().fromJson(branchJson, JSONObject.class);
        if (branchObject == null)
            return;

        // Mixpanel
        boolean isFreshInstall = false;

        try {
            isFreshInstall = branchObject.getBoolean(Defines.Jsonkey.IsFirstSession.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (isFreshInstall) {

            MixpanelAPI mixpanel = MixpanelAPI.getInstance(getApplicationContext(), AppConfig.getMixpanelToken());

            String channel = null;

            try {
                channel = branchObject.optString("~channel");
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (channel.isEmpty()) {  //Install from Google play

                channel = "Organic";

            } else if (channel.equalsIgnoreCase(Constants.BRANCH_CHANNEL_CHAT) || channel.equalsIgnoreCase(Constants.BRANCH_CHANNEL_FOLLOW) || channel.equalsIgnoreCase(Constants.BRANCH_CHANNEL_DOWNLOAD)) {   // Install from Viral

                mixpanel.getPeople().set("Viral type", channel);
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("Viral type", channel);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mixpanel.registerSuperProperties(jsonObject);
                channel = "Viral";

                // Set Referrer ID
                try {
                    String referrerId = branchObject.getString(Constants.BRANCH_MEM_ID);
                    mixpanel.getPeople().set("Referrer ID", referrerId);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                // Set Referrer Name
                try {
                    String referrerName = branchObject.getString(Constants.BRANCH_MEM_NAME);
                    mixpanel.getPeople().set("Referrer Name", referrerName);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else if (channel.equalsIgnoreCase(Constants.BRANCH_CHANNEL_EDM)) {    // Install from eDM

                // Set Original Email
                try {
                    String originalEmail = branchObject.getString(Constants.BRANCH_ORIGINAL_EMAIL);
                    mixpanel.getPeople().set("Original Email", originalEmail);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                // Set Company
                try {
                    String company = branchObject.getString(Constants.BRANCH_COMPANY);
                    mixpanel.getPeople().set("Company", company);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            // Set Campaign
            try {
                String campaign = branchObject.getString("~campaign");
                mixpanel.getPeople().set("Campaign", campaign);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            mixpanel.getPeople().set("Media source", channel);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("Media source", channel);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            mixpanel.registerSuperProperties(jsonObject);
        }

        if ((DatabaseManager.getInstance(this) == null) || (DatabaseManager.getInstance(this).getLogInfo() == null) || (DatabaseManager.getInstance(this).getLogInfo().getQbUserId() == null)) {
            return;
        }

        String action = null;
        try {
            action = branchObject.getString(Constants.BRANCH_ACTION);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (action != null) {
            SharedPreferences.Editor editor = getSharedPreferences(Constants.PREF_BRANCH, MODE_PRIVATE).edit();
            editor.putString(Constants.PREF_BRANCH_OBJECT_JSON, null);
            editor.commit();
            if (action.equalsIgnoreCase(Constants.BRANCH_ACTION_CHAT)) {
                //TODO: to pass the viral type
                viralType = 2;

                String QBId = null;
                String memberId = null;
                try {
                    QBId = branchObject.getString(Constants.BRANCH_QBID);
                    memberId = branchObject.getString(Constants.BRANCH_MEM_ID);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (QBId != null && memberId != null) {
                    if (String.valueOf(DatabaseManager.getInstance(this).getLogInfo().getQbUserId()).equalsIgnoreCase(QBId))
                        return;
                    Bundle bundle = new Bundle();
                    DBQBChatDialog existDbQbChatDialog = DatabaseManager.getInstance(this).getQBChatDialogByQbId(Integer.parseInt(QBId));
                    if (existDbQbChatDialog != null) {
                        bundle.putString(ChatActivity.EXTRA_DIALOG_ID, existDbQbChatDialog.getDialogID());
                    }
                    bundle.putString(ChatActivity.EXTRA_USER_ID, QBId);
                    bundle.putInt(ChatActivity.EXTRA_MEM_ID, Integer.parseInt(memberId));

                    ChatActivity.start(currentActivity, bundle);
                    //write viral log api
                    ResponseLoginSocial.Content userCcontent = CoreData.getUserContent(currentActivity);
                    //TODO invite to chat
                    writeViralLogApi(branchObject, userCcontent, 0, Integer.parseInt(memberId), viralType);
                }
            } else if (action.equalsIgnoreCase(Constants.BRANCH_ACTION_FOLLOW)) {
                viralType = 3;
                // call follow agent api
                final ResponseLoginSocial.Content userContent = CoreData.getUserContent(currentActivity);
                String listingId = null;
                String memberId = null;
                try {
                    listingId = branchObject.getString(Constants.BRANCH_AGENT_LISTING_ID);
                    memberId = branchObject.getString(Constants.BRANCH_MEM_ID);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (memberId == null || userContent.memId == Integer.parseInt(memberId))
                    return;
                if (listingId == null)
                    return;
                //write viral log api
                writeViralLogApi(branchObject, userContent, Integer.parseInt(listingId), Integer.parseInt(memberId), viralType);
            }
        }
    }

    private void writeViralLogApi(final JSONObject branchObject, final ResponseLoginSocial.Content userContent, final int listingId, final int memId, final int vType) {
        RequestWriteViralLog witreViralLog = new RequestWriteViralLog(userContent.memId, LocalStorageHelper.getAccessToken(getBaseContext()), vType, memId, userContent.memId);
        witreViralLog.callWriteViralLogApi(ApplicationSingleton.this, new ApiCallback() {
            @Override
            public void success(ApiResponse apiResponse) {
                if (vType == 3) {
                    callFollowApi(branchObject, userContent, listingId, memId);
                } else {
                    //do nothing
                }
            }

            @Override
            public void failure(String errorMsg) {
                if (Constants.isDevelopment)
                    Toast.makeText(currentActivity, "writeViralLogApi fail: " + errorMsg, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void callFollowApi(final JSONObject branchObject, ResponseLoginSocial.Content userContent, int listingId, int memId) {
        RequestFollowAgent followAgent = new RequestFollowAgent(userContent.memId, LocalStorageHelper.getAccessToken(getBaseContext()));
        followAgent.followingListingId = listingId;
        followAgent.followingMemId = memId;
        followAgent.callFollowApi(ApplicationSingleton.this, new ApiCallback() {
            @Override
            public void success(ApiResponse apiResponse) {
                showFollowAgentToast(branchObject);
            }

            @Override
            public void failure(String errorMsg) {
                if (Constants.isDevelopment)
                    Toast.makeText(currentActivity, "callfollowApi fail: " + errorMsg, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void showFollowAgentToast(JSONObject branchObject) {
        final Toast toast = new Toast(getApplicationContext());
        LayoutInflater inflater = currentActivity.getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast_invite_to_follow,
                (ViewGroup) currentActivity.findViewById(R.id.toastLayout));
        LinearLayout rootView = (LinearLayout) layout.findViewById(R.id.rootView);
        rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(currentActivity, MainActivityTabBase.class);
                intent.putExtra(Constants.EXTRA_SELECT_TAB, Constants.SELECT_NEWS);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                Log.i(TAG, "showFollowAgentToast onClick");
                currentActivity.startActivity(intent);
            }
        });
        RoundedImageView agentImageView = (RoundedImageView) layout.findViewById(R.id.agentImageView);
        String imageUrl = null;
        String memberName = null;

        try {
            imageUrl = branchObject.getString(Constants.BRANCH_MEM_IMAGE_URL);
            memberName = branchObject.getString(Constants.BRANCH_MEM_NAME);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (imageUrl != null) {
            Picasso.with(currentActivity).load(imageUrl)
                    .error(R.drawable.default_profile)
                    .placeholder(R.drawable.default_profile)
                    .into(agentImageView);
        }
        ImageButton closeBtn = (ImageButton) layout.findViewById(R.id.closeBtn);
        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toast.cancel();
            }
        });
        TextView agentNameTextView = (TextView) layout.findViewById(R.id.agentNameTextView);
        if (memberName != null) {
            agentNameTextView.setText(Html.fromHtml(String.format(getString(R.string.newsfeed__youfollowed_justnow), memberName)));
        }

        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }

    public void showChatNotification(final DBQBChatMessage dbqbChatMessage) {
        if (dbqbChatMessage.getMessageCategory().equalsIgnoreCase(ChatDbViewController.MSG_TYPE_AGENTLISTING)) {
            return;
        }
        this.incomingMessageCount++;
        currentActivity.runOnUiThread(new Runnable() {
            public void run() {
                if (ApplicationSingleton.this.showChatNotiHandler == null) {
                    ApplicationSingleton.this.showChatNotiHandler = new Handler();
                    ApplicationSingleton.this.showChatNotiHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            String title = "";
                            String content = "";
                            String imageURL = "";

                            QBChatMessage message = new Gson().fromJson(dbqbChatMessage.getQBChatMessage(), QBChatMessage.class);
                            DBQBUser dbqbUser = DatabaseManager.getInstance(currentActivity).getQBUserByUserId((long) dbqbChatMessage.getSenderID());
                            if (dbqbUser != null) {
                                final DBChatroomAgentProfiles dbChatroomAgentProfiles = DatabaseManager.getInstance(currentActivity).getChatroomAgentProfilesByUserId((long) dbqbUser.getUserID());
                                AgentProfiles agentProfile = null;
                                if (dbChatroomAgentProfiles != null) {
                                    agentProfile = new Gson().fromJson(dbChatroomAgentProfiles.getChatroomAgentProfiles(), AgentProfiles.class);
                                    AgentProfiles finalAgentProfile = agentProfile;
                                    imageURL = finalAgentProfile.agentPhotoURL;
                                }
                            }
                            title = (String) message.getProperty("senderName");
                            if (ApplicationSingleton.this.incomingMessageCount > 1) {
                                content = StringUtils.getByIdWithVars(currentActivity, R.string.chat_lobby__group_unread_message, "you have %s unread message", "" + ApplicationSingleton.this.incomingMessageCount);
                            } else {
                                if (dbqbChatMessage.getBody().equals("image")) {
                                    content = getString(R.string.sign_up__photo);
                                } else {
                                    content = CryptLib.decryptedQBText(dbqbChatMessage);
                                }
                            }
                            final Toast toast = new Toast(currentActivity);
                            LayoutInflater inflater = currentActivity.getLayoutInflater();
                            View layout = inflater.inflate(R.layout.notification_chat_message,
                                    null);
                            ImageView imageView = (ImageView) layout.findViewById(R.id.agentImageView);
                            TextView nameTextView = (TextView) layout.findViewById(R.id.nameTextView);
                            TextView messageTextView = (TextView) layout.findViewById(R.id.messageTextView);

                            if (title != null && title.length() > 0) {
                                nameTextView.setText(title);
                            } else {
                                nameTextView.setVisibility(View.GONE);
                            }
                            if (content != null && content.length() > 0) {
                                messageTextView.setText(content);
                            } else {
                                messageTextView.setVisibility(View.GONE);
                            }

                            if (imageURL != null && imageURL.length() > 0) {
                                Picasso.with(currentActivity).load(imageURL)
                                        .error(R.drawable.default_profile)
                                        .placeholder(R.drawable.default_profile)
                                        .into(imageView);
                            } else {
                                imageView.setVisibility(View.GONE);
                            }

                            toast.setDuration(Toast.LENGTH_LONG);
                            toast.setView(layout);
                            toast.setGravity(Gravity.TOP | Gravity.FILL_HORIZONTAL, 0, 0);
                            toast.show();
                            ApplicationSingleton.this.incomingMessageCount = 0;
                            ApplicationSingleton.this.showChatNotiHandler = null;
                        }
                    }, TimeUtils.ONE_SECOND * 2);
                }
            }
        });
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate");

        if (BuildConfig.FLAVOR.equals("prod")) {
            //do PROD stuff
            Constants.isDevelopment = false;
        } else {
            //otherwise
            Constants.isDevelopment = true;
        }


        LocalStorageHelper.initSecurePreferences(getBaseContext());
        SharedPreferences prefs = getSharedPreferences(Constants.PREF_REAL_NETWORK, MODE_PRIVATE);
        String realJson = prefs.getString(Constants.PREF_REAL_NETWORK_JSON, null);
        if (realJson != null) {
            //is first beta vervion
            LocalStorageHelper.setRealNetworkJson(getBaseContext(), realJson);
            prefs.edit().clear().commit();
        }

        registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
                                               @Override
                                               public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
                                                   Locale locale = LocalStorageHelper.getCurrentLocale(activity);
                                                   Log.w(TAG, "activity = " + activity);
                                                   Log.w(TAG, "locale = " + locale);
                                                   if (locale != null) {
                                                       AppUtil.setLocale(activity, locale);
                                                   }
                                               }

                                               @Override
                                               public void onActivityStarted(final Activity activity) {
                                               }

                                               @Override
                                               public void onActivityResumed(final Activity activity) {
                                                   currentActivity = activity;
                                                   if (needForceUpdate) {
                                                       showUpdateAlert(activity, needForceUpdate, updateURL);
                                                   }
                                                   if (applicationDidEnterBackground) {
                                                       applicationDidEnterBackground = false;
                                                       RequestVersion requestVersion = new RequestVersion();
                                                       requestVersion.callVersionApi(activity, new ApiCallback() {
                                                           @Override
                                                           public void success(ApiResponse apiResponse) {
                                                               try {
                                                                   ResponseVersion response = new Gson().fromJson(apiResponse.jsonContent, ResponseVersion.class);
                                                                   if (response.ver.forceUpdate == 1) {
                                                                       needForceUpdate = true;
                                                                   } else {
                                                                       needForceUpdate = false;
                                                                   }
                                                                   SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
                                                                   String previousLatestVersionKey = "previousLatestVersion";
                                                                   float currentVersion = Float.parseFloat(BuildConfig.VERSION_NAME);
                                                                   float playstoreVersion = Float.parseFloat(response.ver.androidVer);
                                                                   float previousLatestVersion = sharedPref.getFloat(previousLatestVersionKey, currentVersion);
                                                                   if (needForceUpdate || playstoreVersion > previousLatestVersion) {
                                                                       sharedPref.edit().putFloat(previousLatestVersionKey, playstoreVersion).commit();
                                                                       updateURL = response.url;
                                                                       showUpdateAlert(activity, needForceUpdate, updateURL);
                                                                   }
                                                               } catch (Exception e) {
                                                                   Log.e("e", e.getLocalizedMessage());

                                                               }
                                                           }

                                                           @Override
                                                           public void failure(String errorMsg) {
                                                               Log.e("error", errorMsg);
                                                           }
                                                       });

                                                   }

                                                   checkLangToforceRTLSupported();

                                                   checkIsInvited();

                                               }

                                               @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
                                               private void checkLangToforceRTLSupported() {
                                                   if (getString(R.string.force_rtl).equalsIgnoreCase("true") &&
                                                           currentActivity.getClass() != CreatePostPhotosActivity.class) {
                                                       Configuration config = getResources().getConfiguration();
                                                       if (config.getLayoutDirection() != View.LAYOUT_DIRECTION_RTL) {
                                                           AppUtil.forceRTLIfSupported(currentActivity);
                                                       }
                                                   }

                                               }


                                               @Override
                                               public void onActivityPaused(Activity activity) {
                                               }

                                               @Override
                                               public void onActivityStopped(Activity activity) {

                                                   boolean isForegroud = AppUtil.isAppOnForeground(activity);
                                                   Log.w(TAG, "onActivityStopped foregroud = " + isForegroud);

                                                   if (!isForegroud && activity.getClass() != IntroActivity.class
                                                           && activity.getClass() != SignUpInfoActivity.class) {
                                                       ChatSessionUtil.logoutQBuser(activity);
                                                       isBackgroundBefore = true;
                                                       Log.w(TAG, "onActivityStopped activity = " + activity);
                                                   }

                                               }

                                               @Override
                                               public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
//                Log.w(TAG, "onActivitySaveInstanceState activity = " + activity.getClass().getSimpleName());
                                               }

                                               @Override
                                               public void onActivityDestroyed(Activity activity) {
                                                   Log.w(TAG, "onActivityDestroyed activity = " + activity.getClass().getSimpleName());
                                               }
                                           }

        );
        instance = this;

        // Initialise Facebook SDK
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        //  QuickBlox SDK
        QBSettings.getInstance().setLogLevel(Constants.isDevelopment ? LogLevel.DEBUG : LogLevel.NOTHING).fastConfigInit(AppConfig.getQbAppId(),
                AppConfig.getQbAuthKey(), AppConfig.getQbAuthSecret());

        if (AppConfig.IS_DEV)

        {
            //for DEV only
        } else

        {
            //for PROD only
            QBSettings.getInstance().setContentBucketName(AppConfig.getQbContentBucket());
            QBSettings.getInstance().setServerApiDomain(AppConfig.getQbApiEndpoint());
            QBSettings.getInstance().setChatServerDomain(AppConfig.getQbChatEndpoint());
            QBSettings.getInstance().setTurnServerDomain(AppConfig.getQbTurnEndpoint());
        }

//        TwitterAuthConfig authConfig = new TwitterAuthConfig(getResources().getString(R.string.twitter_consumer_key), getResources().getString(R.string.twitter_consumer_secret));
////        Fabric.with(this, new Twitter(authConfig));
//        final Fabric fabric = new Fabric.Builder(this)
//                .kits(new Twitter(authConfig))
//                .logger(new DefaultLogger(Log.DEBUG))
//                .debuggable(true)
//                .build();
//        Fabric.with(fabric);


        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(GCMIntentService.NOTIFICATION_ID);

        if (BuildConfig.FLAVOR.equals("prod"))

        {
            Branch.getAutoInstance(this);
        } else

        {
            Branch.getAutoTestInstance(this);
        }

    }


    @Override
    public void onTerminate() {
        super.onTerminate();
        Log.d(TAG, "onTerminate");
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public int getAppVersion() {
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    public void showErrorToast(String message) {
        Log.e("Toast Error", message);
//        Context context = getApplicationContext();
//        CharSequence text = message;
//        int duration = Toast.LENGTH_LONG;
//        Toast toast = Toast.makeText(context, text, duration);
//        toast.show();
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        if (level == ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN) {
            applicationDidEnterBackground = true;
        }
        Log.e("onTrimMemory", "level:" + level);
    }

    private void showUpdateAlert(final Activity activity, Boolean forceUpdate, final String updateURL) {
        if (updateURL != null) {
            AlertDialog.Builder updateDialogBuilder = new AlertDialog.Builder(activity).setMessage(R.string.app_update__alert).setCancelable(false);
            if (!forceUpdate) {
                updateDialogBuilder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
            }
            updateDialogBuilder.setPositiveButton(R.string.common__update, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(updateURL));
                    activity.startActivity(browserIntent);
                }
            });

            if (updateDialog == null || !updateDialog.isShowing()) {
                updateDialog = updateDialogBuilder.show();
            }

        }
    }
}
