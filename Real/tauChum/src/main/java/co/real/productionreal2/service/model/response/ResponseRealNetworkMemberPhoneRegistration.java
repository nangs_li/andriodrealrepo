package co.real.productionreal2.service.model.response;

import com.google.gson.annotations.SerializedName;

public class ResponseRealNetworkMemberPhoneRegistration extends ResponseBase {
    @SerializedName("Content")
    public Content content;

    public class Content {
        @SerializedName("UserID")
        public String userId;
        @SerializedName("FirstName")
        public String firstName;
        @SerializedName("LastName")
        public String lastName;
        @SerializedName("AccessToken")
        public String accessToken;
        @SerializedName("Email")
        public String email = ""; //unused
    }
}
