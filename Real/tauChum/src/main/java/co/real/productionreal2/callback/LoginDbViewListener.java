package co.real.productionreal2.callback;

import co.real.productionreal2.service.model.response.ResponseLoginSocial;

/**
 * Created by hohojo on 17/7/2015.
 */
public interface LoginDbViewListener {
    public void updateLoginDbAndToMainTabActivity(ResponseLoginSocial.Content content);
    public void loginFail(String failMsg);
    public void loginCancel();
}
