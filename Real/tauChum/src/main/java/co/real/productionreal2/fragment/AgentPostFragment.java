package co.real.productionreal2.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import co.real.productionreal2.CoreData;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.MainActivityTabBase;
import co.real.productionreal2.adapter.AgentPostPagerAdapter;
import co.real.productionreal2.callback.FragmentViewChangeListener;
import co.real.productionreal2.model.AgentListing;
import co.real.productionreal2.model.SystemSetting;
import co.real.productionreal2.service.model.request.RequestListing;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;
import co.real.productionreal2.util.ImageUtil;
import widget.RoundedImageView;
import widget.slidinguppanel.SlidingUpPanelLayout;

/**
 * Created by hohojo on 29/10/2015.
 */
public class AgentPostFragment extends Fragment{
    private static final String TAG ="AgentPostFragment";
    private ResponseLoginSocial.Content agentContent;
    private FragmentViewChangeListener fragmentViewChangeListener;
    @InjectView(R.id.coverImageView)
    ImageView coverImageView;
    @InjectView(R.id.agentImageView)
    RoundedImageView agentImageView;
    @InjectView(R.id.nameTextView)
    TextView nameTextView;
    @InjectView(R.id.followingCountTextView)
    TextView followingCountTextView;
    @InjectView(R.id.previewDetailViewPager)
    ViewPager previewDetailViewPager;
    @InjectView(R.id.slidingUpLayout)
    SlidingUpPanelLayout slidingUpLayout;
    @InjectView(R.id.rootView)
    LinearLayout rootView;


    private AgentPostPagerAdapter agentPostPagerAdapter;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            fragmentViewChangeListener = (FragmentViewChangeListener) activity;
        } catch (ClassCastException e) {
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        fragmentViewChangeListener = null;
    }

    public static AgentPostFragment newInstance() {
        Bundle args = new Bundle();
        AgentPostFragment fragment = new AgentPostFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_agent_post, container, false);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }



    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
        initSlidingUpLayout();
    }

    private void initSlidingUpLayout() {

        slidingUpLayout.setPanelHeight(ImageUtil.getScreenHeight(getActivity()) / 3);
        slidingUpLayout.setPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
                Log.i(TAG, "onPanelSlide, offset " + slideOffset);
            }

            @Override
            public void onPanelExpanded(View panel) {
                Log.i(TAG, "onPanelExpanded");
            }

            @Override
            public void onPanelCollapsed(View panel) {
                Log.i(TAG, "onPanelCollapsed");
            }

            @Override
            public void onPanelAnchored(View panel) {
                Log.i(TAG, "onPanelAnchored");
            }

            @Override
            public void onPanelHidden(View panel) {
                Log.i(TAG, "onPanelHidden");
            }
        });
    }

    private void initView() {
        rootView.setPadding(0, MainActivityTabBase.titleHeight, 0, 0);
        final ResponseLoginSocial.Content agentContent = CoreData.getUserContent(getActivity());

//        agentContent.agentProfile.agentListing.photos.get(0)
//        coverImageView.setImageBitmap(BitmapFactory.decodeFile(baseCreatePost.getPhotoPathList().get(0)));
//        for (AgentListing.Photo photo: agentContent.agentProfile.agentListing.photos) {
//            photo.position
//        }
        if (agentContent == null)
            return;
        if (agentContent.agentProfile == null)
            return;
        if (agentContent.agentProfile.agentListing == null)
            return;
        if (agentContent.agentProfile.agentListing.photos == null)
            return;
        if (agentContent.agentProfile.agentListing.photos.size() < 1)
            return;
        if (agentContent.agentProfile.agentListing.photos.get(0).url == null)
            return;
        fragmentViewChangeListener.changeTabTitle(this);

        Picasso.with(getActivity())
                .load(agentContent.photoUrl)
                .error(R.drawable.default_profile)
                .placeholder(R.drawable.default_profile)
                .into(agentImageView);
        Picasso.with(getActivity())
                .load(agentContent.agentProfile.agentListing.photos.get(0).url)
                .error(R.drawable.default_profile)
                .placeholder(R.drawable.default_profile)
                .into(coverImageView);

        if (agentContent == null)
            return;
        if (agentContent.agentProfile == null)
            return;
        if (agentContent.agentProfile.agentListing == null)
            return;
        if (agentContent.agentProfile.agentListing.addressInputs == null)
            return;
        String street = agentContent.agentProfile.agentListing.addressInputs.get(0).street;
//        addressTextView.setText(street);
        nameTextView.setText(agentContent.memName);
        followingCountTextView.setText(String.valueOf(agentContent.followingCount));

        agentPostPagerAdapter = new AgentPostPagerAdapter(getActivity().getSupportFragmentManager());
        ArrayList<Integer> langIndexList = getLangIndexList(agentContent.agentProfile.agentListing);
        agentPostPagerAdapter.setLangIndexList(langIndexList);
        agentPostPagerAdapter.setLangNameList(getLangStringList(langIndexList, agentContent));
        previewDetailViewPager.setAdapter(agentPostPagerAdapter);
        ViewGroup.LayoutParams viewpagerParams = previewDetailViewPager
                .getLayoutParams();
        viewpagerParams.height = ImageUtil.getScreenHeight(getActivity()) * 3 / 4;
        previewDetailViewPager.setLayoutParams(viewpagerParams);
        previewDetailViewPager.post(new Runnable() {
            @Override
            public void run() {
                previewDetailViewPager.setCurrentItem(previewDetailViewPager.getCurrentItem());
            }
        });

    }

    private ArrayList<Integer>  getLangIndexList(AgentListing agentListing) {
        ArrayList<Integer> langIndexList = new ArrayList<>();
        for (RequestListing.Reason reason : agentListing.reasons) {
            langIndexList.add(reason.languageIndex);
        }
        return langIndexList;
    }

    private ArrayList<String> getLangStringList(ArrayList<Integer> langIndexList, ResponseLoginSocial.Content userContent) {
        ArrayList<String> langStringList = new ArrayList<>();

        for (Integer langIndex : langIndexList) {
            for (SystemSetting.SystemLanguage systemLanguage : userContent.systemSetting.sysLangList) {
                if (systemLanguage.index == langIndex) {
                    langStringList.add(systemLanguage.lang);
                }
            }
        }
        return langStringList;
    }
}
