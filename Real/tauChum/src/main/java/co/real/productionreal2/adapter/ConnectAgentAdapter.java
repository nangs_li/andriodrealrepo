package co.real.productionreal2.adapter;

import android.content.Context;
import android.content.res.ColorStateList;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import co.real.productionreal2.Constants;
import co.real.productionreal2.R;
import co.real.productionreal2.dao.account.DBPhoneBook;
import co.real.productionreal2.daomanager.DatabaseManager;
import co.real.productionreal2.service.model.ReMatchContact;
import co.real.productionreal2.view.FollowButton;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;
import widget.RoundedImageView;

/**
 * Created by kelvinsun on 3/6/16.
 */
public class ConnectAgentAdapter extends BaseAdapter implements StickyListHeadersAdapter, Filterable {

    private List<Object> connectAgentList = new ArrayList<>();
    private LayoutInflater inflater;
    protected Context mContext;
    protected ConnectAgentAdapterListener mListener;
    private int inviteCheckedCount = 0;
    private List<String> existingAgentPhoneNoList = new ArrayList<>();
    private List<Integer> existingAgentIdList = new ArrayList<>();
    private List<DBPhoneBook> invitePhoneNoList = new ArrayList<>();

    private List<Object> filteredContactList = new ArrayList<>();

    public static interface ConnectAgentAdapterListener {

        public ArrayList<Object> getConnectAgentList();

        public void onFollowAll(List<Integer> existingAgentIdList);

        public void onFollowAgent(ReMatchContact agent);

        public void onInviteAgent(List<DBPhoneBook> invitePhoneNoList);
    }

    public ConnectAgentAdapter(Context context, List<Object> connectAgentList, ConnectAgentAdapterListener mListener) {
        inflater = LayoutInflater.from(context);
        this.mContext = context;
        this.connectAgentList = connectAgentList;
        for (Object object : connectAgentList) {
            if (object instanceof ReMatchContact) {
                this.existingAgentPhoneNoList.add(((ReMatchContact) object).phoneNumber);
            }
        }

        Iterator<Object> iterator = connectAgentList.iterator();
        while (iterator.hasNext()) {
            Object value = iterator.next();
            if (value instanceof DBPhoneBook) {
                for (String duplicatedNo : existingAgentPhoneNoList) {
                    if (duplicatedNo.equals(((DBPhoneBook) value).getPhoneNumber())) {
                        iterator.remove();
                        break;
                    }
                }
            }
        }
        this.filteredContactList = connectAgentList;
        Log.d("ConnectAgentAdapter", "existingAgentPhoneNoList: " + existingAgentPhoneNoList.size());
        this.mListener = mListener;
        Log.d("ConnectAgentAdapter", "ConnectAgentAdapter: " + connectAgentList.size());
    }

    @Override
    public int getCount() {
        return filteredContactList.size();
    }

    @Override
    public Object getItem(int position) {
        return filteredContactList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        MatchedAgentViewHolder views = new MatchedAgentViewHolder();

        final Object obj = filteredContactList.get(position);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_item_following, parent, false);
            // find view to cache
            views.llWhole = (LinearLayout) convertView.findViewById(R.id.ll_whole_cell);
            views.tvName = (TextView) convertView.findViewById(R.id.tv_following_name);
            views.tvRegion = (TextView) convertView.findViewById(R.id.tv_following_region);
            views.rlImage = (RelativeLayout) convertView.findViewById(R.id.rl_following_image);
            views.ivImage = (RoundedImageView) convertView.findViewById(R.id.iv_following_image);
            views.btnFollower = (FollowButton) convertView.findViewById(R.id.btn_to_follow);
            views.checkOption = (CheckedTextView) convertView.findViewById(R.id.checked_option);
            views.divider = (View) convertView.findViewById(R.id.line_horizontal);
            convertView.setTag(views);
        } else {
            views = (MatchedAgentViewHolder) convertView.getTag();
        }


        //set style
        views.tvName.setTextColor(mContext.getResources().getColor(R.color.white));
        views.tvRegion.setTextColor(mContext.getResources().getColor(R.color.dark_grey));


        if (obj instanceof ReMatchContact) {
            String name = ((ReMatchContact) obj).name;
            String region = ((ReMatchContact) obj).location;
            String imageUrl = ((ReMatchContact) obj).photoURL;

            views.tvName.setText(name);
            views.tvRegion.setText(region);
            Picasso.with(mContext).load(imageUrl).into(views.ivImage);

            views.rlImage.setVisibility(View.VISIBLE);
            views.btnFollower.setVisibility(View.VISIBLE);
            views.btnFollower.setEnabled(true);
            views.checkOption.setVisibility(View.GONE);

            //update following status
            if (((ReMatchContact) obj).isFollowing == 1) {
                views.ivImage.setImageBorder(Constants.STYLE_TYPE.AGENT_FOLLOWED);
            } else {
                views.ivImage.setImageBorder(Constants.STYLE_TYPE.AGENT_UNFOLLOWED);
            }

            if (((ReMatchContact) obj).isFollowing == 1) {
                views.btnFollower.setStyle(Constants.STYLE_TYPE.AGENT_FOLLOWED);
            } else {
                views.btnFollower.setStyle(Constants.STYLE_TYPE.AGENT_UNFOLLOWED);
            }


            final MatchedAgentViewHolder finalViews = views;
            views.btnFollower.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onFollowAgent((ReMatchContact) obj);
                    finalViews.btnFollower.playAnimation();
                    if (((ReMatchContact) obj).isFollowing == 1) {
                        finalViews.btnFollower.setStyle(Constants.STYLE_TYPE.AGENT_FOLLOWED);
                    } else {
                        finalViews.btnFollower.setStyle(Constants.STYLE_TYPE.AGENT_UNFOLLOWED);
                    }
                }
            });
            //disable click event
            views.llWhole.setOnClickListener(null);

        } else if (obj instanceof DBPhoneBook) {

            String name = ((DBPhoneBook) obj).getName();
            final String phoneNo = ((DBPhoneBook) obj).getCountryCode() + " " + ((DBPhoneBook) obj).getPhoneNumber();

            views.tvName.setText(name);
            views.tvRegion.setText(phoneNo);
            views.rlImage.setVisibility(View.GONE);
            views.btnFollower.setVisibility(View.GONE);
            views.checkOption.setVisibility(View.VISIBLE);


            Log.d("getHasSent","getHasSent: "+((DBPhoneBook) obj).getHasSent());
            views.checkOption.setCheckMarkDrawable(((DBPhoneBook) obj).getHasSent() ?
                    R.drawable.single_choice_selector_yellow_with_flag : R.drawable.single_choice_selector_yellow);


            views.llWhole.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (((DBPhoneBook) obj).getHasChecked()) {
                        if (inviteCheckedCount > 0) {
                            inviteCheckedCount--;
                            invitePhoneNoList.remove((obj));
                        }
                        ((DBPhoneBook) obj).setHasChecked(false);
                    } else {
                        inviteCheckedCount++;
                        invitePhoneNoList.add(((DBPhoneBook) obj));
                        ((DBPhoneBook) obj).setHasChecked(true);
                    }
                    DatabaseManager.getInstance(mContext).updatePhoneBook(((DBPhoneBook) obj));
                    notifyDataSetChanged();
                }
            });
            views.checkOption.setChecked(((DBPhoneBook) obj).getHasChecked() == null ? false : ((DBPhoneBook) obj).getHasChecked());
        } else {
            //disable click event
            views.llWhole.setOnClickListener(null);
        }


        return convertView;
    }

    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {
        final HeaderViewHolder holder;
        if (convertView == null) {
            holder = new HeaderViewHolder();
            convertView = inflater.inflate(R.layout.list_header_connect_agent, parent, false);
            holder.tvHeader = (TextView) convertView.findViewById(R.id.tv_header);
            holder.divider = (View) convertView.findViewById(R.id.view_divider);
            holder.bnAction = (Button) convertView.findViewById(R.id.bn_action);
            convertView.setTag(holder);
        } else {
            holder = (HeaderViewHolder) convertView.getTag();
        }
        //set header text as first char in name
//        String headerText = "" + countries[position].subSequence(0, 1).charAt(0);
        String actionName = "";
        String sectionHeader = "";
        int sectionColor = mContext.getResources().getColor(R.color.holo_blue);
        int bnColor = R.color.holo_blue;
        int bnDrawable = R.drawable.bg_btn_action_blue;

        Object obj = filteredContactList.get(position);
        if (obj instanceof ReMatchContact) {
            sectionHeader = mContext.getString(R.string.connectagent__agents_you_may_know);
            ;
            sectionColor = mContext.getResources().getColor(R.color.holo_blue);
            bnColor = R.color.holo_blue;
            bnDrawable = R.drawable.bg_btn_action_blue;
            actionName = mContext.getString(R.string.connectagent__follow_all);
            //to determine the action button status
            boolean isButtonEnable = false;
            for (Object object : connectAgentList) {
                if (object instanceof ReMatchContact) {
                    if (((ReMatchContact) object).isFollowing == 0) {
                        isButtonEnable = true;
                        break;
                    }
                }
            }
            holder.bnAction.setEnabled(isButtonEnable);

            holder.bnAction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    existingAgentIdList = new ArrayList<Integer>();
                    for (Object object : mListener.getConnectAgentList()) {
                        if (object instanceof ReMatchContact) {
                            existingAgentIdList.add(((ReMatchContact) object).memberID);
                            //update following status
                            ((ReMatchContact) object).isFollowing = 1;
                            holder.bnAction.setEnabled(false);
                        }
                    }
                    mListener.onFollowAll(existingAgentIdList);
                }
            });

        } else if (obj instanceof DBPhoneBook) {
            sectionHeader = mContext.getString(R.string.connectagent__favourite_agents);
            sectionColor = mContext.getResources().getColor(R.color.invite_yellow);
            bnColor = R.color.invite_yellow;
            bnDrawable = R.drawable.bg_btn_action_yellow;

            int inviteCheckedCount=DatabaseManager.getInstance(mContext).getCheckedPhoneNoCount();
            if (inviteCheckedCount > 0) {
                holder.bnAction.setEnabled(true);
                actionName = String.format(mContext.getString(R.string.connectagent__invite) , inviteCheckedCount);
            } else {
                holder.bnAction.setEnabled(false);
                actionName = mContext.getString(R.string.common__invite);
            }

            holder.bnAction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    invitePhoneNoList=DatabaseManager.getInstance(mContext).getCheckedPhoneItem();
                    mListener.onInviteAgent(invitePhoneNoList);
                    //reset invite list
//                    if (invitePhoneNoList!=null) {
//                        invitePhoneNoList.clear();
//                    }
//                    notifyDataSetChanged();
                }
            });
        } else {
            //disable click event
            holder.bnAction.setOnClickListener(null);
        }

        int[][] states = new int[][]{
                new int[]{android.R.attr.state_pressed}, // pressed
                new int[]{android.R.attr.state_enabled}, // enable
                new int[]{}
        };
        int[] colors = new int[]{
                mContext.getResources().getColor(R.color.dark_grey), // grey
                mContext.getResources().getColor(bnColor), // active color
                mContext.getResources().getColor(R.color.dark_grey)  // grey
        };
        ColorStateList list = new ColorStateList(states, colors);

        //set style
        holder.tvHeader.setTextColor(sectionColor);
        holder.divider.setBackgroundColor(sectionColor);
        holder.bnAction.setTextColor(list);
        holder.bnAction.setBackgroundResource(bnDrawable);
        holder.tvHeader.setText(sectionHeader);
        holder.bnAction.setText(actionName);

        return convertView;
    }

    @Override
    public long getHeaderId(int position) {
        //return the first character of the country as ID because this is what headers are based upon
        long headerId = 0;
        Object obj = filteredContactList.get(position);
        if (obj instanceof ReMatchContact) {
            headerId = 0;
        } else if (obj instanceof DBPhoneBook) {
            headerId = 1;
        }
        return headerId;
    }

    /**
     * Filter result with key word
     *
     * @return
     */
    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

                filteredContactList = (List<Object>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                FilterResults results = new FilterResults();
                ArrayList<Object> FilteredArrayNames = new ArrayList<Object>();

                // perform your search here using the searchConstraint String.
                constraint = constraint.toString().toLowerCase();
                for (int i = 0; i < mListener.getConnectAgentList().size(); i++) {
                    Object obj = mListener.getConnectAgentList().get(i);
                    String dataNames = null;
                    if (obj instanceof ReMatchContact) {
                        dataNames = ((ReMatchContact) obj).name;
                    } else if (obj instanceof DBPhoneBook) {
                        dataNames = ((DBPhoneBook) obj).getName();
                    }
                    if (dataNames != null) {
                        if (dataNames.toLowerCase().contains(constraint.toString())) {
                            FilteredArrayNames.add(obj);
                        }
                    }
                }

                results.count = FilteredArrayNames.size();
                results.values = FilteredArrayNames;
                Log.e("VALUES", results.values.toString());

                return results;
            }
        };

        return filter;
    }


    class HeaderViewHolder {
        TextView tvHeader;
        Button bnAction;
        View divider;
    }

    class MatchedAgentViewHolder {
        LinearLayout llWhole;
        TextView tvName;
        TextView tvRegion;
        RoundedImageView ivImage;
        RelativeLayout rlImage;
        FollowButton btnFollower;
        CheckedTextView checkOption;
        View divider;
    }

}
