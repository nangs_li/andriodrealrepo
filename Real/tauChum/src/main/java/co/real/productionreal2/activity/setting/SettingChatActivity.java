package co.real.productionreal2.activity.setting;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.CheckedTextView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import co.real.productionreal2.Constants;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.BaseActivity;
import co.real.productionreal2.callback.DialogCallback;
import co.real.productionreal2.chat.ChatDbViewController;
import co.real.productionreal2.chat.core.PrivateChatImpl;
import co.real.productionreal2.dao.account.DBLogInfo;
import co.real.productionreal2.dao.account.DBQBChatDialog;
import co.real.productionreal2.daomanager.DatabaseManager;
import co.real.productionreal2.model.QBUserCustomData;
import co.real.productionreal2.model.UserQBUser;
import co.real.productionreal2.util.StringUtils;
import co.real.productionreal2.view.Dialog;

/**
 * Created by alexhung on 15/4/16.
 */
public class SettingChatActivity extends BaseActivity {
    @InjectView(R.id.headerLayout)
    View headerLayout;

    @InjectView(R.id.photoAutoDownloadLayout)
    View photoAutoDownloadLayout;
    @InjectView(R.id.showLastSeenLayout)
    View showLastSeenLayout;
    @InjectView(R.id.showReadReciptsLayout)
    View showReadReciptsLayout;

    private static final String TAG = "SettingChatActivity";

    CheckedTextView photoAutoDownalodCheckedTextView;
    CheckedTextView lastSeenCheckedTextView;
    CheckedTextView readReciptsCheckedTextView;


    private DBLogInfo dbLogInfo;
    private ProgressDialog ringProgressDialog = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_chat);
        ButterKnife.inject(this);
        dbLogInfo = DatabaseManager.getInstance(this).getLogInfo();
        initView();
        getUserQBUserAndUpdateSwitches();
    }

    private void getUserQBUserAndUpdateSwitches() {
        QBUsers.getUser(dbLogInfo.getQbUserId(), new QBEntityCallback<QBUser>() {
            @Override
            public void onSuccess(QBUser qbUser, Bundle bundle) {
                UserQBUser.qbUser = qbUser;
                updateSwitches();
                cancelProgressDialog();
            }

            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(List<String> list) {
                cancelProgressDialog();

                if (Constants.isDevelopment)
                    Log.d(TAG,"getUserByQb error: "+ list);
            }
        });
    }

    private void updateSwitches() {
        QBUserCustomData qbUserCustomData = new Gson().fromJson(UserQBUser.qbUser.getCustomData(), QBUserCustomData.class);
        if (qbUserCustomData == null) {
            Dialog.feedbackDialog(this, true, getString(R.string.common__privacy), getString(R.string.common_google_play_services_network_error_text)
                    , new DialogCallback() {
                        @Override
                        public void yes() {
                            finish();
                        }
                    }).show();
        } else {
            if (qbUserCustomData.chatRoomShowLastSeen)
                lastSeenCheckedTextView.setChecked(true);
            else
                lastSeenCheckedTextView.setChecked(false);

            if (qbUserCustomData.chatRoomShowReadReceipts)
                readReciptsCheckedTextView.setChecked(true);
            else
                readReciptsCheckedTextView.setChecked(false);
        }

        if (dbLogInfo.getPhotoAutoDownload() == null)
            dbLogInfo.setPhotoAutoDownload(false);

        photoAutoDownalodCheckedTextView.setChecked(dbLogInfo.getPhotoAutoDownload());
    }

    private void showProgressDialog() {
        if (ringProgressDialog != null && ringProgressDialog.isShowing())
            return;
        ringProgressDialog = ProgressDialog.show(this, "", StringUtils.getById(SettingChatActivity.this,R.string.alert__Initializing,"Loading..."));
    }

    private void cancelProgressDialog() {
        if (ringProgressDialog != null) {
            ringProgressDialog.dismiss();
            ringProgressDialog.cancel();
            ringProgressDialog = null;
        }
    }

    private void updateEnableStatusToQB(final boolean isEnable) {
        if (UserQBUser.qbUser == null) {
            //retry later
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    updateEnableStatusToQB(isEnable);
                }
            }, 500);
        } else {
            Gson gson = new Gson();
            QBUserCustomData qbUserCustomData = gson.fromJson(UserQBUser.qbUser.getCustomData(), QBUserCustomData.class);
            if (isEnable == false) {
                qbUserCustomData.chatRoomShowLastSeen = false;
            } else {
                qbUserCustomData.chatRoomShowLastSeen = true;
            }
            UserQBUser.qbUser.setCustomData(gson.toJson(qbUserCustomData));
            QBUsers.updateUser(UserQBUser.qbUser, new QBEntityCallbackImpl<QBUser>() {
                @Override
                public void onSuccess(QBUser user, Bundle args) {
                    UserQBUser.qbUser = user;
                    dbLogInfo.setEnableStatus(isEnable);
                    DatabaseManager.getInstance(SettingChatActivity.this).updateLogInfo(dbLogInfo);
                    sendSystemMsgToAllChatUser();
                    Log.d(TAG,"setChatroomshowlastseen success");
                }

                @Override
                public void onError(List<String> errors) {
                    Log.d(TAG,"setChatroomshowlastseen error: " + errors);

                }
            });
        }

    }

    private void updateEnableReadToQB(final boolean isEnable) {
        if (UserQBUser.qbUser == null) {
            //retry later
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    // Do something after 5s = 5000ms
                    updateEnableReadToQB(isEnable);
                }
            }, 500);
        } else {
            Gson gson = new Gson();
            QBUserCustomData qbUserCustomData = gson.fromJson(UserQBUser.qbUser.getCustomData(), QBUserCustomData.class);
            if (isEnable == false) {
                qbUserCustomData.chatRoomShowReadReceipts = false;
            } else {
                qbUserCustomData.chatRoomShowReadReceipts = true;
            }

            UserQBUser.qbUser.setCustomData(gson.toJson(qbUserCustomData));
            QBUsers.updateUser(UserQBUser.qbUser, new QBEntityCallbackImpl<QBUser>() {
                @Override
                public void onSuccess(QBUser user, Bundle args) {
                    UserQBUser.qbUser = user;
                    dbLogInfo.setEnableReadMsg(isEnable);
                    DatabaseManager.getInstance(SettingChatActivity.this).updateLogInfo(dbLogInfo);
                    sendSystemMsgToAllChatUser();
                    Log.d(TAG,"setChatroomshowreadreceipts success");
                }

                @Override
                public void onError(List<String> errors) {
                    Log.d(TAG,"setChatroomshowreadreceipts error:"+ errors);

                }
            });
        }
    }

    private void sendSystemMsgToAllChatUser() {
        List<DBQBChatDialog> dbqbChatDialogList = DatabaseManager.getInstance(this).listQBChatDialog();
        for (DBQBChatDialog dbqbChatDialog : dbqbChatDialogList) {
            if (dbqbChatDialog != null)
                PrivateChatImpl.getInstance(this).sendSystemMsgSetting((int) dbqbChatDialog.getQbUserID(), ChatDbViewController.SYSTEM_MSG_CHAT_PRIVACY_SETTING);
        }

    }


//    holder.checkedTextView.setText(language.nativeName);
//    Log.d("langIndexList", "langIndexList: " + language.toString());
//    if (getCurrentPos() == position)
//            holder.checkedTextView.setChecked(true);
//    else
//            holder.checkedTextView.setChecked(false);
//
//    //disable selected language option
//    Log.d("langIndexList", "langIndexList: test: " + sysLangList.get(position).nativeName + " " + sysLangList.get(position).isEnable);
//    if (!language.isEnable) {
//        holder.checkedTextView.setChecked(true);
//        holder.checkedTextView.setAlpha(.5f);
//        holder.checkedTextView.setClickable(false);
//    }
//}
    private void initView() {
        TextView titleTextView = (TextView) headerLayout.findViewById(R.id.titleTextView);
        titleTextView.setText(StringUtils.getById(this,R.string.setting__chats, "Chats"));

        photoAutoDownloadLayout.setBackgroundResource(R.color.setting_spacing_blue);
        showReadReciptsLayout.setBackgroundResource(R.color.setting_spacing_blue);
        showLastSeenLayout.setBackgroundResource(R.color.setting_spacing_blue);
        int padding = getResources().getDimensionPixelOffset(R.dimen.common_inset);
        photoAutoDownloadLayout.setPadding(padding,0,padding,0);
        showReadReciptsLayout.setPadding(padding,0,padding,0);
        showLastSeenLayout.setPadding(padding,0,padding,0);
        photoAutoDownalodCheckedTextView = (CheckedTextView) photoAutoDownloadLayout.findViewById(R.id.checkedTextView);

        lastSeenCheckedTextView = (CheckedTextView) showLastSeenLayout.findViewById(R.id.checkedTextView);
        readReciptsCheckedTextView = (CheckedTextView) showReadReciptsLayout.findViewById(R.id.checkedTextView);

        photoAutoDownloadLayout.findViewById(R.id.dividerImageView).setVisibility(View.INVISIBLE);
        showReadReciptsLayout.findViewById(R.id.dividerImageView).setVisibility(View.INVISIBLE);

        photoAutoDownalodCheckedTextView.setClickable(true);
        lastSeenCheckedTextView.setClickable(true);
        readReciptsCheckedTextView.setClickable(true);

        photoAutoDownalodCheckedTextView.setText(R.string.chat_setting__save_incoming_media);
        lastSeenCheckedTextView.setText(R.string.chat_privacy__show_last_seen);
        readReciptsCheckedTextView.setText(R.string.chat_privacy__read_receipts);
        showProgressDialog();

        photoAutoDownalodCheckedTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                photoAutoDownalodCheckedTextView.toggle();
                if (photoAutoDownalodCheckedTextView.isChecked()){
                    dbLogInfo.setPhotoAutoDownload(true);
                } else {
                    dbLogInfo.setPhotoAutoDownload(false);
                }
                DatabaseManager.getInstance(SettingChatActivity.this).updateLogInfo(dbLogInfo);
            }
        });
        lastSeenCheckedTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lastSeenCheckedTextView.toggle();
                updateEnableStatusToQB(lastSeenCheckedTextView.isChecked());
            }
        });

        readReciptsCheckedTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                readReciptsCheckedTextView.toggle();
                updateEnableReadToQB(readReciptsCheckedTextView.isChecked());
            }
        });

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
