package co.real.productionreal2.service.model.response;

import com.google.gson.annotations.SerializedName;

public class ResponseBase {

	@SerializedName("ErrorCode")
	public int errorCode;

	@SerializedName("ErrorMsg")
	public String errorMsg;

	@SerializedName("APIName")
	public String apiName;
}
