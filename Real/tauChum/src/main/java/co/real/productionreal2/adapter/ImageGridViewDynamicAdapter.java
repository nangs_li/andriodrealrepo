package co.real.productionreal2.adapter;

/**
 * Author: alex askerov
 * Date: 9/9/13
 * Time: 10:52 PM
 */

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;

import org.askerov.dynamicgrid.BaseDynamicGridAdapter;
import org.askerov.dynamicgrid.DynamicGridView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import co.real.productionreal2.Constants;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.createpost.CreatePostPhotosActivity;
import me.iwf.photopicker.utils.PhotoPickerIntent;

/**
 * Author: alex askerov
 * Date: 9/7/13
 * Time: 10:56 PM
 */
public class ImageGridViewDynamicAdapter extends BaseDynamicGridAdapter {
    private DynamicGridView imageGridView;
    public boolean showEditView = false;

    public ImageGridViewDynamicAdapter(Context context, List<String> items, DynamicGridView imageGridView, int columnCount) {
        super(context, items, columnCount);
        this.imageGridView = imageGridView;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        CheeseViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.create_post_grid_item, null);
            holder = new CheeseViewHolder(convertView);
            convertView.setTag(holder);

        } else {
            holder = (CheeseViewHolder) convertView.getTag();
        }

        holder.coverPhotoRelativeLayout.setVisibility(View.GONE);
        holder.addPhotoBtn.setVisibility(View.GONE);
        holder.deleteBtn.setVisibility(View.GONE);
        holder.image.setVisibility(View.INVISIBLE);
        holder.addPhotoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PhotoPickerIntent intent = new PhotoPickerIntent(getContext());
                intent.setPhotoCount(Constants.MAX_SELECT_PHOTO);
                ((Activity) getContext()).startActivityForResult(intent, CreatePostPhotosActivity.SELECT_PHOTO);
            }
        });
        holder.deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<Object> items = new ArrayList<Object>();
                items.addAll(getItems());
//                items.remove(position);
                items.remove(position);
                set(items);
                notifyDataSetChanged();
                notifyDataSetInvalidated();

            }
        });
//
//        if (position == 0 && getItems().size() > 0) {
//            // first item and image list number is not 0
//            holder.image.setVisibility(View.VISIBLE);
//            holder.coverPhotoRelativeLayout.setVisibility(View.VISIBLE);
//            Picasso.with(convertView.getContext()).load(new File((String) getItem(position))).centerCrop().fit().into(holder.image);
//        } else {
        int mPos;

        Log.d("getItems().size()", " " + getItems().size() + " v.s. " + getCount());
        Log.d("getItems().size()", "getItems().size(): position: " + position);


        mPos = position;

        if (position >= getItems().size() || getItems().size() == 0) {
            holder.addPhotoBtn.setVisibility(View.VISIBLE);
        } else {
            //image item
            holder.image.setVisibility(View.VISIBLE);
            Picasso.with(convertView.getContext()).load(new File(String.valueOf(getItem(mPos)))).centerCrop().fit().into(holder.image);
            if (showEditView)
                holder.deleteBtn.setVisibility(View.VISIBLE);
            else
                holder.deleteBtn.setVisibility(View.GONE);

            Log.d("getItems().size()", "getItems().size(): getItem: " + String.valueOf(getItem(mPos)));
        }



//        }


//        if (position == getCount() - 1 && getCount() != getItems().size()) {
//            //last item
//            holder.coverPhotoRelativeLayout.setVisibility(View.GONE);
//            holder.addPhotoBtn.setVisibility(View.VISIBLE);
//            holder.image.setVisibility(View.GONE);
//            holder.deleteBtn.setVisibility(View.GONE);
//            holder.addPhotoBtn.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    PhotoPickerIntent intent = new PhotoPickerIntent(getContext());
//                    intent.setPhotoCount(Constants.MAX_SELECT_PHOTO);
//                    ((Activity) getContext()).startActivityForResult(intent, CreatePostPhotosActivity.SELECT_PHOTO);
//                }
//            });
//        } else {
//            holder.addPhotoBtn.setVisibility(View.GONE);
//            holder.image.setVisibility(View.VISIBLE);
//            if (position == 0) {
//                holder.coverPhotoRelativeLayout.setVisibility(View.VISIBLE);
//                holder.deleteBtn.setVisibility(View.GONE);
//
//            } else {
//                holder.coverPhotoRelativeLayout.setVisibility(View.GONE);
//                if (imageGridView.isEditMode()) {
//                    holder.deleteBtn.setVisibility(View.VISIBLE);
//                } else {
//                    holder.deleteBtn.setVisibility(View.GONE);
//                }
//            }
//
//            Log.w("ImageGridViewAdapter", "getItems().size(): " + getItems().size() + " getCount = " + getCount() + " position: " + position + " getItem = " + getItem(position));
//            if (TextUtils.isEmpty((String) getItem(position))) {
//                holder.image.setImageResource(R.drawable.ic_launcher);
//            } else {
//                Picasso.with(convertView.getContext()).load(new File((String) getItem(position))).centerCrop().fit().into(holder.image);
//                holder.deleteBtn.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        List<Object> items = new ArrayList<Object>();
//                        items.addAll(getItems());
//                        items.remove(position);
//                        set(items);
//                        notifyDataSetChanged();
//                    }
//                });
//            }
//        }
        return convertView;
    }

    public List<String> getPhotosPath() {
        List<String> photoPathList = new ArrayList<>();
        Log.d("getPhotosPath", "savePhotoPath: " + getItems().toString());
//        for (Object object : getItems()) {
//            photoPathList.add(object != null ? ((AgentListing.Photo)object): null);
//        }
        photoPathList.addAll((List<String>)(Object)getItems());
        return photoPathList;
    }

    private class CheeseViewHolder {
        private ImageView image;
        private ImageButton deleteBtn;
        private ImageButton addPhotoBtn;
        private RelativeLayout coverPhotoRelativeLayout;

        private CheeseViewHolder(View view) {
            image = (ImageView) view.findViewById(R.id.item_img);
            deleteBtn = (ImageButton) view.findViewById(R.id.item_delete_btn);
            addPhotoBtn = (ImageButton) view.findViewById(R.id.item_add_btn);
            coverPhotoRelativeLayout = (RelativeLayout) view.findViewById(R.id.coverPhotoRelativeLayout);

        }

    }


    public void updateItems(ArrayList<String> photoList) {
        this.set(photoList);
    }
}