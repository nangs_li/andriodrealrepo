package co.real.productionreal2.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.ndk.CrashlyticsNdk;
import com.google.gson.Gson;
import com.mixpanel.android.mpmetrics.MixpanelAPI;

import org.json.JSONException;
import org.json.JSONObject;

import co.real.productionreal2.ApplicationSingleton;
import co.real.productionreal2.BuildConfig;
import co.real.productionreal2.Constants;
import co.real.productionreal2.CoreData;
import co.real.productionreal2.activity.login.IntroActivity;
import co.real.productionreal2.common.FetchAgentProfileHelper;
import co.real.productionreal2.common.LocalStorageHelper;
import co.real.productionreal2.config.AppConfig;
import co.real.productionreal2.daomanager.DatabaseManager;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;
import co.real.productionreal2.util.AppUtil;
import io.branch.referral.Branch;
import io.branch.referral.BranchError;
import io.branch.referral.Defines;
import io.fabric.sdk.android.Fabric;

public class SplashActivity extends BaseActivity {

    /**
     * Duration of wait
     **/
    private final int SPLASH_DISPLAY_LENGTH = 2000;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        if (BuildConfig.FLAVOR.equals("prod"))
            Fabric.with(this, new Crashlytics(), new CrashlyticsNdk());
        Log.e("", "edwin SplashActivity onCreate");

        Branch branch = Branch.getInstance(getApplicationContext());
        branch.initSession(new Branch.BranchReferralInitListener() {
            @Override
            public void onInitFinished(JSONObject referringParams, BranchError error) {

                if (error != null) {
//                    Toast.makeText(SplashActivity.this, "Init branchError: " + error, Toast.LENGTH_LONG).show();
                } else {
                    if (Constants.isDevelopment)
                        Log.d("SplashActivity", "branch init success");
                }

                if (referringParams != null) {

                    // Mixpanel
                    boolean isFreshInstall = false;

                    try {
                        isFreshInstall = referringParams.getBoolean(Defines.Jsonkey.IsFirstSession.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    if (isFreshInstall) {
                        MixpanelAPI mixpanel = MixpanelAPI.getInstance(getApplicationContext(), AppConfig.getMixpanelToken());
                        mixpanel.track("Installed App");
                    }

                    SharedPreferences.Editor editor = getSharedPreferences(Constants.PREF_BRANCH, MODE_PRIVATE).edit();
                    String json = new Gson().toJson(referringParams);
                    editor.putString(Constants.PREF_BRANCH_OBJECT_JSON, json);
                    editor.commit();

                    ApplicationSingleton.getInstance().checkIsInvited();
                }
            }
        }, this.getIntent().getData(), this);

        gotoLaunchMode();
    }

    private void gotoLaunchMode() {
        if (!isTaskRoot()) {
            finish();
        } else {
            MixpanelAPI mixpanel = MixpanelAPI.getInstance(this, AppConfig.getMixpanelToken());
            mixpanel.track("Opened App");

//            DBUtil.copyConFigDB(this);
            DatabaseManager databaseManager = DatabaseManager.getInstance(this);
            if (LocalStorageHelper.getAccessToken(this)!="") {
                ResponseLoginSocial.Content content = CoreData.getUserContent(this);

                // migration flow for old user
                try{
                    if(LocalStorageHelper.getRegedPhoneNumber(this).equals("")){
                        Intent intent = new Intent(this, IntroActivity.class);
                        intent.putExtra(Constants.EXTRA_IS_OLD_USER,true);
                        startActivity(intent);
                        finish();
                        return;
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

                AppUtil.goToMainTabActivity(this, content);
                FetchAgentProfileHelper.getInstance().startFetching();
            } else {
                goToIntroActivity();
            }

//            ConfigDataBaseManager configDataBaseManager = ConfigDataBaseManager.getInstance(this);
//
//            if (configDataBaseManager.listMain().size() > 0) {
//
//                if (databaseManager.hasLogInfo() &&
//                        configDataBaseManager.getCurrentDbMain(configDataBaseManager.getCurrentMemberId()).getStatus()) {
//                    ResponseLoginSocial.Content content = new Gson().fromJson(databaseManager.getLogInfo().getLogInfo(), ResponseLoginSocial.Content.class);
//                    AppUtil.goToMainTabActivity(this, content);
//                } else {
//                    goToIntroActivity();
//                }
//            } else {
//                goToIntroActivity();
//            }
        }

    }

    private void goToIntroActivity() {
        Intent mainIntent = new Intent(SplashActivity.this, IntroActivity.class);
        startActivity(mainIntent);
        finish();
    }

}
