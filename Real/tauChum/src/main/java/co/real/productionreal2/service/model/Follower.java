package co.real.productionreal2.service.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by kelvinsun on 19/2/16.
 */
public class Follower implements Parcelable {
    @SerializedName("MemberType")
    public int memberType;
    @SerializedName("MemberID")
    public int memberID;
    @SerializedName("MemberName")
    public String memberName;
    @SerializedName("AgentPhotoURL")
    public String agentPhotoURL;
    @SerializedName("AgentLicenseNumber")
    public String agentLicenseNumber;
    @SerializedName("Location")
    public String location;
    @SerializedName("FollowDate")
    public String followDate;
    @SerializedName("ListingID")
    public int listingID;
    @SerializedName("IsFollowing")
    public int isFollowing;

    public Follower() {
    }


    public Follower(int memberID, String memberName, String agentPhotoURL,String location, int isFollowing) {
        this.memberID = memberID;
        this.memberName = memberName;
        this.agentPhotoURL = agentPhotoURL;
        this.location = location;
        this.isFollowing = isFollowing;
    }

    public Follower(Parcel source) {
        memberType = source.readInt();
        memberID = source.readInt();
        memberName = source.readString();
        agentPhotoURL = source.readString();
        agentLicenseNumber = source.readString();
        location = source.readString();
        followDate = source.readString();
        listingID = source.readInt();
        isFollowing = source.readInt();

    }


    public static final Parcelable.Creator<Follower> CREATOR = new Parcelable.Creator<Follower>() {
        @Override
        public Follower createFromParcel(Parcel in) {
            return new Follower(in);
        }

        @Override
        public Follower[] newArray(int size) {
            return new Follower[size];
        }
    };


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(memberType);
        dest.writeInt(memberID);
        dest.writeString(memberName);
        dest.writeString(agentPhotoURL);
        dest.writeString(agentLicenseNumber);
        dest.writeString(location);
        dest.writeString(followDate);
        dest.writeInt(listingID);
        dest.writeInt(isFollowing);
    }
}
