package co.real.productionreal2.callback;

/**
 * Created by hohojo on 19/10/2015.
 */
public interface CreateListingApiCallback {
    public void progress();
    public void failure(String errorMsg);
}