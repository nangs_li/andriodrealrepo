package co.real.productionreal2.fragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import co.real.productionreal2.CoreData;
import co.real.productionreal2.Navigation;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.MainActivityTabBase;
import co.real.productionreal2.activity.createpost.BaseCreatePost;
import co.real.productionreal2.activity.createpost.CreatePostPropertyActivity;
import co.real.productionreal2.activity.viral.InviteToFollowActivity;
import co.real.productionreal2.adapter.ActivityLogAdapter;
import co.real.productionreal2.adapter.FollowingAdapter;
import co.real.productionreal2.adapter.StoryAdapter;
import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.callback.FragmentViewChangeListener;
import co.real.productionreal2.chat.pushnotifications.Consts;
import co.real.productionreal2.common.LocalStorageHelper;
import co.real.productionreal2.config.AppConfig;
import co.real.productionreal2.contianer.MeTabContainer;
import co.real.productionreal2.model.AgentListing;
import co.real.productionreal2.model.CurrentFollower;
import co.real.productionreal2.newsfeed.NewsFeedProfileFragment;
import co.real.productionreal2.service.model.ActivityLog;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.Follower;
import co.real.productionreal2.service.model.request.RequestActivityLog;
import co.real.productionreal2.service.model.request.RequestFollowAgent;
import co.real.productionreal2.service.model.request.RequestFollowInfo;
import co.real.productionreal2.service.model.request.RequestFollowingList;
import co.real.productionreal2.service.model.request.RequestListing;
import co.real.productionreal2.service.model.response.ResponseActivityLog;
import co.real.productionreal2.service.model.response.ResponseFollowInfo;
import co.real.productionreal2.service.model.response.ResponseFollowingList;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;
import co.real.productionreal2.util.DisplayUtil;
import co.real.productionreal2.util.ImageUtil;
import co.real.productionreal2.util.NetworkUtil;
import co.real.productionreal2.view.BlurBuilder;
import co.real.productionreal2.view.FollowerCountSectionView;
import co.real.productionreal2.view.TabMenu;
import co.real.productionreal2.view.TitleBar;
import lib.blurbehind.OnBlurCompleteListener;
import widget.EnbleableSwipeViewPager;
import widget.RoundedImageView;

;

/**
 * Created by hohojo on 29/10/2015.
 */

public class CreateUpdatePostFragment extends Fragment implements View.OnClickListener, ActivityLogAdapter.HeaderListExpandableAdapterListener,
        MainActivityTabBase.TabOnClickListener, FollowingAdapter.FollowingAdapterListener, StoryAdapter.StoryAdapterListener {
    private static final String TAG = "CreateUpdatePostFrag";
    private FragmentViewChangeListener fragmentViewChangeListener;
    @InjectView(R.id.rootImageView)
    ImageView rootImageView;
    @InjectView(R.id.view_follower_count)
    FollowerCountSectionView followerCountView;
    @InjectView(R.id.view_following_count)
    FollowerCountSectionView followingCountView;
    @InjectView(R.id.completeRateTextView)
    TextView completeRateTextView;
    @InjectView(R.id.nameTextView)
    TextView nameTextView;
    @InjectView(R.id.agentNameTextView)
    TextView agentNameTextView;
    @InjectView(R.id.noPositingLinearLayout)
    LinearLayout noPositingLinearLayout;
    @InjectView(R.id.agentImageView)
    RoundedImageView agentImageView;
    @InjectView(R.id.completeProgressBar)
    ProgressBar completeProgressBar;
    @InjectView(R.id.maskView)
    View maskView;
    @InjectView(R.id.createPostBtn)
    public Button createPostBtn;
    @InjectView(R.id.updatePostBtn)
    Button updatePostBtn;
    @InjectView(R.id.progressBarRelativeLayout)
    RelativeLayout progressBarRelativeLayout;
    //    @InjectView(R.id.swipeImageView)
//    ImageView swipeImageView;
    @InjectView(R.id.contentRelativeLayout)
    RelativeLayout contentRelativeLayout;
    @InjectView(R.id.ll_activity_log)
    LinearLayout llActivityLog;
    @InjectView(R.id.tv_activity_log_header)
    TextView tvActivityLogHeader;
    @InjectView(R.id.tv_activity_log_header_right)
    TextView tvActivityLogHeaderRight;
    @InjectView(R.id.view_no_posting)
    LinearLayout viewNoPosting;
    @InjectView(R.id.view_update_now)
    LinearLayout viewUpdateNow;
    @InjectView(R.id.lv_activity_log)
    ExpandableListView lvActivityLog;
    @InjectView(R.id.ll_activity_log_mid)
    LinearLayout llActivityLogMid;
    @InjectView(R.id.ll_listview_dropper)
    LinearLayout llDropper;
    @InjectView(R.id.ll_no_activity_log)
    LinearLayout llEmptyActivityLog;
    @InjectView(R.id.tv_empty_log_msg)
    TextView tvEmptyLogMsg;
    @InjectView(R.id.activity_log_swipe_refresh_layout)
    SwipeRefreshLayout activityLogSwipeLayout;
    @InjectView(R.id.following_list_swipe_refresh_layout)
    SwipeRefreshLayout followingListSwipeLayout;
    @InjectView(R.id.lv_following_list)
    ListView lvFollowingList;

    @InjectView(R.id.lv_story_list)
    ListView lvStoryList;

    private EnbleableSwipeViewPager agentProfileViewPager;

    public static final int RESULT_OK = 1000;
    public static final int CONTINUE_POST = 10001;

    Handler handler = new Handler();

    private TitleBar mTitleBar;

    private ResponseLoginSocial.Content agentContent;

    private ActivityLogAdapter activityLogAdapter;
    private List<ActivityLog> activityLogList;

    private FollowingAdapter followingAdapter;
    private ArrayList<Follower> followingList;

    private StoryAdapter storyAdapter;
    private ArrayList<AgentListing> agentListingHistory;

    private int followerCount, followingCount;

    private int requestCode;
    private int resultCode;
    private int PAGE_NO = 1;
    private int PAGE_NO_FOLLOW = 1;
    public PANEL_TYPE panelType = PANEL_TYPE.DEFAULT;

    @Override
    public ArrayList<AgentListing> getStoryList() {
        return agentListingHistory;
    }

    @Override
    public void onCurrentStoryClick() {
        agentProfileViewPager.setCurrentItem(0);
    }

    public enum PANEL_TYPE {DEFAULT, ACTIVITY_LOG, FOLLOWING_LIST}


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            fragmentViewChangeListener = (FragmentViewChangeListener) activity;
        } catch (ClassCastException e) {
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        fragmentViewChangeListener = null;
    }

    public static CreateUpdatePostFragment newInstance(int requestCode, int resultCode, EnbleableSwipeViewPager agentProfileViewPager) {
        Bundle args = new Bundle();
        args.putInt(MeTabContainer.REQUEST_CODE, requestCode);
        args.putInt(MeTabContainer.RESULT_CODE, resultCode);
        CreateUpdatePostFragment fragment = new CreateUpdatePostFragment();
        fragment.setViewPager(agentProfileViewPager);
        fragment.setArguments(args);
        return fragment;
    }

    public static CreateUpdatePostFragment newInstance(int requestCode, int resultCode) {
        Bundle args = new Bundle();
        args.putInt(MeTabContainer.REQUEST_CODE, requestCode);
        args.putInt(MeTabContainer.RESULT_CODE, resultCode);
        CreateUpdatePostFragment fragment = new CreateUpdatePostFragment();
        fragment.setArguments(args);
        return fragment;
    }

    void setViewPager(EnbleableSwipeViewPager pager) {
        this.agentProfileViewPager = pager;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

//    private void updateView(BusSelectedEvent.SelectMeEvent event) {
//        swipeImageView.setImageBitmap(event.bitmap);
//    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        agentContent = CoreData.getUserContent(getActivity());
    }

    private void addListener() {
        followerCountView.setOnClickListener(this);
        followingCountView.setOnClickListener(this);
        //add invite to follow btn click event
        mTitleBar.getInviteFollowBtn().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //invite to follow

                // Mixpanel
                MixpanelAPI mixpanel = MixpanelAPI.getInstance(getActivity(), AppConfig.getMixpanelToken());
                mixpanel.track("View Invite to Follow @ Me");

                InviteToFollowActivity.start(getActivity());
            }
        });
        lvActivityLog.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            int previousGroup = -1;

            @Override
            public void onGroupExpand(int groupPosition) {
                if (groupPosition != previousGroup)
                    lvActivityLog.collapseGroup(previousGroup);
                previousGroup = groupPosition;

            }
        });

        lvActivityLog.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                Log.d("setOnGroupClickListener", "setOnGroupClickListener" + ((ActivityLog) activityLogAdapter.getGroup(groupPosition)).logType);
                if (((ActivityLog) activityLogAdapter.getGroup(groupPosition)).logType == 2)
                    return true;// This way the expander cannot be collapsed
                else
                    return false;
            }
        });

        //setup refresh list for following list
        followingListSwipeLayout.setColorSchemeResources(R.color.newsfeed_text_nuber_blue);
        followingListSwipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //online mode
                if (NetworkUtil.isConnected(getActivity())) {
                    PAGE_NO_FOLLOW = 1;
                    followingList.clear();
                    fetchFollowingList(PAGE_NO_FOLLOW);
                } else {
                    stopRefresh();
                }
            }
        });


        activityLogSwipeLayout.setColorSchemeResources(R.color.newsfeed_text_nuber_blue);
        activityLogSwipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //online mode
                if (NetworkUtil.isConnected(getActivity())) {
                    PAGE_NO = 1;
                    fetchActivityLog(PAGE_NO);
                } else {
                    stopRefresh();
                }
            }
        });

        llDropper.setOnTouchListener(new View.OnTouchListener() {
            private int _yDelta;
            RelativeLayout.LayoutParams layoutParamsOriginal = (RelativeLayout.LayoutParams) llActivityLog.getLayoutParams();

            @Override
            public boolean onTouch(View view, MotionEvent event) {
                final int Y = (int) event.getRawY();
                int displayH = DisplayUtil.getDisplayHeight(getActivity());

                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_DOWN:
                        _yDelta = Y;
                        break;
                    case MotionEvent.ACTION_UP:
                        if (layoutParamsOriginal.height > 500) {
                            layoutParamsOriginal.height = _yDelta;
                            llActivityLog.setLayoutParams(layoutParamsOriginal);
                        } else {
                            closePanel();
                        }
                        break;
                    case MotionEvent.ACTION_POINTER_DOWN:
                        break;
                    case MotionEvent.ACTION_POINTER_UP:
                        break;
                    case MotionEvent.ACTION_MOVE:
                        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) llActivityLog.getLayoutParams();
                        //to decide if close or keep panel open
                        layoutParams.height = Y - displayH / 3;
                        if (layoutParams.height > 0)
                            llActivityLog.setLayoutParams(layoutParams);
                        break;
                }
                llActivityLog.invalidate();
                return true;
            }
        });

        lvActivityLog.setOnScrollListener(new EndlessScrollListener() {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                // TODO Auto-generated method stub
                PAGE_NO++;
                fetchActivityLog(PAGE_NO);
            }
        });

        lvFollowingList.setOnScrollListener(new EndlessScrollListener() {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                // TODO Auto-generated method stub
                PAGE_NO_FOLLOW++;
                fetchFollowingList(PAGE_NO_FOLLOW);
            }
        });
    }

    public void stopRefresh() {
        activityLogSwipeLayout.setRefreshing(false);
        followingListSwipeLayout.setRefreshing(false);
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_update_post, container, false);
        ButterKnife.inject(this, view);

        ((MainActivityTabBase) getActivity()).tabOnClickListener = this;

        return view;
    }

    private void initData() {
        mTitleBar = ((MainActivityTabBase) getActivity()).getTitleBar();
        activityLogList = LocalStorageHelper.defaultHelper(getActivity()).getActivityLogList(getActivity());
        followingList = LocalStorageHelper.defaultHelper(getActivity()).getMeFollowingList(getActivity());
        followerCount = LocalStorageHelper.defaultHelper(getActivity()).getFollowerCount();
        followingCount = LocalStorageHelper.defaultHelper(getActivity()).getFollowingCount();
        agentListingHistory = new ArrayList<>();
        agentListingHistory.add(agentContent.agentProfile.agentListing);  //current listing
        agentListingHistory.addAll((ArrayList) agentContent.agentProfile.agentListingHistoryList); //previous listing
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getArguments() != null) {
            requestCode = getArguments().getInt(MeTabContainer.REQUEST_CODE);
            resultCode = getArguments().getInt(MeTabContainer.RESULT_CODE);
        }
        Log.d("onViewCreated", "onViewCreated requestCode" + requestCode + " resultCode: " + resultCode);
        initData();
        addListener();

        initView();
        updateFollowInfo();
        PAGE_NO = 1;
//        fetchActivityLog(PAGE_NO);
//        fetchFollowingList(PAGE_NO_FOLLOW);

//        newsFeedProfileFragment.update(agentContent.agentProfile, 0);
    }

    @Override
    public void onResume() {
        Log.v("","edwinaaa onResume 0");
        super.onResume();
        Log.v("","edwinaaa onResume 1");
        createPostBtn.setEnabled(true);
        updatePostBtn.setEnabled(true);
        Log.v("","edwinaaa onResume 2");
        agentContent = CoreData.getUserContent(getActivity());
        Log.v("","edwinaaa onResume 3");
        initView();
        Log.v("","edwinaaa onResume 4");
        Log.d("OnResume", "onResume CreateUpdatePost" + agentContent.agentProfile.toString());

    }

    private int getCompleteRate() {
        int completeRate = 0;
        if (agentContent == null)
            return 0;
        if (agentContent.agentProfile == null)
            return 0;

        if (agentContent.agentProfile.agentExpList != null && agentContent.agentProfile.agentExpList.size() > 0) {
            completeRate = completeRate + 20;
        }
        if (agentContent.agentProfile.agentLicenseNumber != null && agentContent.agentProfile.agentLicenseNumber.trim().length() > 0) {
            completeRate = completeRate + 20;
        }
        if (agentContent.agentProfile.agentLanguageList != null && agentContent.agentProfile.agentLanguageList.size() > 0) {
            completeRate = completeRate + 20;
        }
        if (agentContent.agentProfile.agentSpecialtyList != null && agentContent.agentProfile.agentSpecialtyList.size() > 0) {
            completeRate = completeRate + 20;
        }
        if (agentContent.agentProfile.agentPastClosingList != null && agentContent.agentProfile.agentPastClosingList.size() > 0) {
            completeRate = completeRate + 20;
        }
        Log.d(TAG, "onResume getCompleteRate: " + completeRate);
        return completeRate;
    }


    private void initView() {

        activityLogAdapter = new ActivityLogAdapter(getActivity(), activityLogList, CreateUpdatePostFragment.this);
        lvActivityLog.setAdapter(activityLogAdapter);

        followingAdapter = new FollowingAdapter(getActivity(), followingList, CreateUpdatePostFragment.this);
        lvFollowingList.setAdapter(followingAdapter);

        storyAdapter = new StoryAdapter(getActivity(), CreateUpdatePostFragment.this);
        lvStoryList.setAdapter(storyAdapter);

        contentRelativeLayout.setPadding(0, MainActivityTabBase.tabHeight, 0, 0);
        updatePostBtn.setEnabled(true);

        Log.d(TAG, "initView photo");

        Picasso.with(getActivity())
                .load(agentContent.photoUrl)
                .error(R.drawable.default_profile)
                .placeholder(R.drawable.default_profile)
                .into(agentImageView);
        Log.d(TAG, "initView photo 1");
        if (agentContent.agentProfile == null || agentContent.agentProfile.agentListing == null ||
                agentContent.agentProfile.agentListing.photos == null ||
                agentContent.agentProfile.agentListing.photos.size() == 0)
            Picasso.with(getActivity()).load(agentContent.photoUrl).into(target);
        else
            Picasso.with(getActivity()).load(agentContent.agentProfile.agentListing.photos.get(0).url).into(target);
        Log.d(TAG, "initView photo 2");
        int completeRate = getCompleteRate();
        completeProgressBar.setProgress(completeRate);
        completeRateTextView.setText("" + completeRate + "%");

        // Mixpanel
        MixpanelAPI mixpanel = MixpanelAPI.getInstance(getActivity(), AppConfig.getMixpanelToken());
        mixpanel.getPeople().set("Profile complete %", "" + completeRate);

        nameTextView.setText(agentContent.memName);
        agentNameTextView.setText(agentContent.memName);

        completeProgressBar.setVisibility(completeRate == 100 ? View.GONE : View.VISIBLE);
        progressBarRelativeLayout.setVisibility(completeRate == 100 ? View.GONE : View.VISIBLE);


        followerCountView.setDesc(R.string.common__followers);
        followingCountView.setDesc(R.string.common__following);
//        followerCountView.setCount(agentContent.followerCount);
//        followingCountView.setCount(agentContent.followingCount);

        //handle for non-agent
        viewNoPosting.setVisibility(View.VISIBLE);
        viewUpdateNow.setVisibility(View.GONE);

        //update create btn text
        int createBtnStrId;
        if (requestCode == CONTINUE_POST && resultCode == getActivity().RESULT_OK) {
            createBtnStrId = R.string.me__continue_my_posting;
        } else {
            createBtnStrId = R.string.me__create_posting_now;
        }
        createPostBtn.setText(createBtnStrId);

        if (agentContent == null)
            return;
        if (agentContent.agentProfile == null)
            return;
        if (agentContent.agentProfile.agentListing == null)
            return;
        if (agentContent.agentProfile.agentListing.listingID != 0) {
            //created post
            viewNoPosting.setVisibility(View.GONE);
            viewUpdateNow.setVisibility(View.VISIBLE);

            agentNameTextView.setVisibility(View.GONE);
            int updateBtnStrId;
            if (requestCode == CONTINUE_POST && resultCode == getActivity().RESULT_OK) {
                updateBtnStrId = R.string.me__continue_my_posting;
            } else {
                updateBtnStrId = R.string.me__btn_update_now;
            }
            updatePostBtn.setText(updateBtnStrId);
//            updatePostBtn.setCompoundDrawablesWithIntrinsicBounds(null, null, getResources().getDrawable(R.drawable.btn_setting_detail), null);
//            updatePostBtn.setBackground(null);
            noPositingLinearLayout.setVisibility(View.GONE);

            if (agentContent.agentProfile.agentListing.addressInputs.size() > 0) {
                RequestListing.AddressUserInput addressInput = agentContent.agentProfile.agentListing.addressInputs.get(0);
                Log.w(TAG, "addressInputs = " + addressInput);
                //set address input
//                addressTextView.setText(addressInput.street);
////                for (AgentListing.GoogleAddress googleAddress : agentContent.agentProfile.agentListing.googleAddresses) {
//                if (addressInput.aptSuite == null)
//                    apartmentTextView.setVisibility(View.GONE);
//                else
//                    apartmentTextView.setText(addressInput.aptSuite);

//                    break;
//                    if (getResources().getConfiguration().locale.toString().equalsIgnoreCase(googleAddress.lang)) {
//                        addressTextView.setText(googleAddress.name);
//                        if (googleAddress.address == null)
//                            apartmentTextView.setVisibility(View.GONE);
//                        else
//                            apartmentTextView.setText(googleAddress.address);
//                        break;
//                    }
//                }

//                if (agentContent.agentProfile.agentListing.addressInputs.get(0).aptSuite == null) {
//                    apartmentTextView.setVisibility(View.GONE);
//                }
            }

//            String imageName = AppUtil.getPropertyTypeImageName(agentContent.agentProfile.agentListing.propertyType,
//                    agentContent.agentProfile.agentListing.spaceType);
//            apartmentImageView.setImageResource(getActivity().getResources().getIdentifier(imageName + "_on", "drawable", getActivity().getPackageName()));

        }


    }

    Bitmap rootBitmap;
    private Target target = new Target() {
        @Override
        public void onBitmapLoaded(final  Bitmap bitmap, Picasso.LoadedFrom from) {

            new Thread() {
                @Override
                public void run() {
                    try {
                        rootBitmap = BlurBuilder.blurBitmap(getActivity(), bitmap);
//                        rootBitmap = BlurBuilder.reduceBrightness(rootBitmap);
                        handler.post(new Runnable() {
                            public void run() {
                                rootImageView.setImageBitmap(rootBitmap);
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }.start();
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {

        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {

        }

    };


    private void fetchActivityLog(final int pageNo) {
        if (activityLogList == null)
            return;
//        if (activityLogList.size() < 1 || LocalStorageHelper.defaultHelper(getActivity()).getNewCount(TabMenu.TAB_SECTION.ME) > 0 || PAGE_NO > 1) {

        RequestActivityLog requestActivityLog = new RequestActivityLog(agentContent.memId, LocalStorageHelper.getAccessToken(getContext()), pageNo);

        requestActivityLog.callActivityLogApi(getActivity(), new ApiCallback() {
            @Override
            public void success(ApiResponse apiResponse) {
                Log.w(TAG, "responseGson = " + apiResponse.jsonContent);

                List<ActivityLog> mActivityLogList = new Gson().fromJson(
                        apiResponse.jsonContent,
                        ResponseActivityLog.class).content.activityLog;
                Log.w(TAG, "activityLogList size = " + mActivityLogList.size());

                if (pageNo == 1) {
                    activityLogList = new ArrayList<ActivityLog>();
                }

                //add new record to existing list
                activityLogList.addAll(mActivityLogList);

                if (pageNo == 1) {
                    if (activityLogList.size() < 1) {
                        updateActivityLogUI();
                        stopRefresh();
                        return;
                    }
                    LocalStorageHelper.defaultHelper(getActivity()).saveActivityLogListToLocalStorage((ArrayList) activityLogList);
                    activityLogAdapter = new ActivityLogAdapter(getActivity(), activityLogList, CreateUpdatePostFragment.this);
                    lvActivityLog.setAdapter(activityLogAdapter);
                }
                Log.d(TAG, "responseGson = " + activityLogList.toString());
                Log.w(TAG, "activityLogList size = after: " + activityLogList.size());
                updateActivityLogUI();


                LocalStorageHelper.defaultHelper(getActivity()).saveNewCountToLocalStorage(0, TabMenu.TAB_SECTION.ME);
                stopRefresh();

            }

            @Override
            public void failure(String errorMsg) {
                Log.w(TAG, "errorMsg = " + errorMsg);
            }
        });

//        } else {
//            updateActivityLogUI();
//            stopRefresh();
//        }


    }


    private void updateActivityLogUI() {
//        getActivity().runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
        // show empty message
        Log.d("updateActivityLogUI", "updateActivityLogUI: " + activityLogList.size());
        Log.d("updateActivityLogUI", "updateActivityLogUI: " + agentContent.agentProfile);
        Log.d("updateActivityLogUI", "updateActivityLogUI: " + agentContent.agentProfile.agentListing);
        llEmptyActivityLog.setVisibility(activityLogList.size() > 0 ? View.GONE : View.VISIBLE);
        int noRecordStrId;

        if (agentContent.agentProfile != null) {
            if (agentContent.agentProfile.agentListing == null)
                noRecordStrId = R.string.me__no_record_desc;
            else
                noRecordStrId = R.string.me__no_record_title;

            tvEmptyLogMsg.setText(noRecordStrId);
        }

        tvEmptyLogMsg.setVisibility(View.VISIBLE);
//        tvActivityLogHeader.setText(activityLogList.size() > 1 ? String.format(getString(R.string.activity_log_activity_title), 1) : "");

        if (activityLogAdapter != null)
            activityLogAdapter.notifyDataSetChanged();

        recevieNewActivityLog();
//            }
//        });

    }


    private void updateFollowingUI() {
        // show empty message
        Log.d("updateFollowingUI", "updateFollowingUI: " + followingList.size());
        llEmptyActivityLog.setVisibility(followingList.size() > 0 ? View.GONE : View.VISIBLE);
        tvEmptyLogMsg.setText(R.string.me__no_record_title);

        int noRecordStrId;
        noRecordStrId = R.string.me__no_record_title;

        tvEmptyLogMsg.setText(noRecordStrId);

        tvEmptyLogMsg.setVisibility(View.VISIBLE);

        if (followingAdapter != null)
            followingAdapter.notifyDataSetChanged();

    }

    @OnClick(R.id.agentImageView)
    public void profileImgClick() {
        Log.d("updateFollowingUI", "agentProfileViewPager.getChildCount(): " + agentProfileViewPager.getChildCount());
        agentProfileViewPager.setCurrentItem(agentProfileViewPager.getChildCount() - 1);
    }


    @OnClick(R.id.updatePostBtn)
    public void updatePostBtnClick(View view) {
        // Mixpanel
        MixpanelAPI mixpanel = MixpanelAPI.getInstance(getActivity(), AppConfig.getMixpanelToken());
        mixpanel.track("Attempted to Update Posting");
        updatePostBtn.setEnabled(false);

        ImageUtil.getInstance().prepareBlurImageFromActivity(getActivity(), CreatePostPropertyActivity.class, new OnBlurCompleteListener() {
            @Override
            public void onBlurComplete() {
                Intent intent = new Intent(getActivity(), CreatePostPropertyActivity.class);
                Navigation.presentIntentForResult(getActivity(), intent, CONTINUE_POST);

            }
        });

    }

    @OnClick(R.id.createPostBtn)
    public void createPostBtnClick(View view) {
        // Mixpanel
        MixpanelAPI mixpanel = MixpanelAPI.getInstance(getActivity(), AppConfig.getMixpanelToken());
        mixpanel.track("Attempted to Create First Posting");
        createPostBtn.setEnabled(false);

        ImageUtil.getInstance().prepareBlurImageFromActivity(getActivity(), CreatePostPropertyActivity.class, new OnBlurCompleteListener() {
            @Override
            public void onBlurComplete() {
                Intent intent = new Intent(getActivity(), CreatePostPropertyActivity.class);
                Navigation.presentIntentForResult(getActivity(), intent, CONTINUE_POST);
            }
        });

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult: " + requestCode + " , " + resultCode);
        if (resultCode == RESULT_OK && BaseCreatePost.getInstance(getActivity()).getCreatePostRequest().addressInputs.size() > 0) {
            updatePostBtn.setText(R.string.me__continue_my_posting);
        } else {
            updatePostBtn.setText(R.string.me__btn_update_now);
        }
    }

    @Override
    public void onClick(View v) {
        if (v == followerCountView) {

            // Mixpanel
            MixpanelAPI mixpanel = MixpanelAPI.getInstance(getActivity(), AppConfig.getMixpanelToken());
            mixpanel.track("Viewed Followers Activity");
            openPanel(PANEL_TYPE.ACTIVITY_LOG);

            //online mode
            if (NetworkUtil.isConnected(getActivity())) {
                PAGE_NO = 1;
                activityLogList = new ArrayList<>();
                fetchActivityLog(PAGE_NO);
            } else {
                NetworkUtil.showNoNetworkMsg(getActivity());
                stopRefresh();
            }
//            //update UI
//            updateActivityLogUI();
        } else if (v == followingCountView) {
            openPanel(PANEL_TYPE.FOLLOWING_LIST);

            //online mode
            if (NetworkUtil.isConnected(getActivity())) {
                PAGE_NO_FOLLOW = 1;
                followingList.clear();
                fetchFollowingList(PAGE_NO_FOLLOW);
            } else {
                NetworkUtil.showNoNetworkMsg(getActivity());
                stopRefresh();
            }
//            //update UI
//            updateFollowingUI();
        }
    }


    private void openPanel(PANEL_TYPE mPanelType) {
        if (mPanelType == panelType && (followerCountView.isSelected() || followingCountView.isSelected())) {
            closePanel();
            return;
        }

        switch (mPanelType) {
            case ACTIVITY_LOG:
                if (llActivityLog.getVisibility() != View.VISIBLE) {
                    ImageUtil.expand(llActivityLog);
                }
                tvActivityLogHeader.setSelected(true);
                tvActivityLogHeader.setText(R.string.me__activity);
                followerCountView.setViewSelected(true);
                followingCountView.setViewSelected(false);
                //remove new count on tab widget on Me section
                readNewActivitylog();
                followingListSwipeLayout.setVisibility(View.INVISIBLE);
                activityLogSwipeLayout.setVisibility(View.VISIBLE);
                panelType = PANEL_TYPE.ACTIVITY_LOG;
                break;
            case FOLLOWING_LIST:
                if (llActivityLog.getVisibility() != View.VISIBLE) {
                    ImageUtil.expand(llActivityLog);
                }
                tvActivityLogHeader.setSelected(true);
                tvActivityLogHeader.setText(R.string.common__following);
                followerCountView.setViewSelected(false);
                followingCountView.setViewSelected(true);
                followingListSwipeLayout.setVisibility(View.VISIBLE);
                activityLogSwipeLayout.setVisibility(View.INVISIBLE);
                panelType = PANEL_TYPE.FOLLOWING_LIST;
                break;
            default:
                break;
        }
    }

    private void closePanel() {
        ImageUtil.collapse(llActivityLog);
        followerCountView.setViewSelected(false);
        followingCountView.setViewSelected(false);
        tvActivityLogHeader.setSelected(false);
    }

    private void readNewActivitylog() {
        LocalStorageHelper.defaultHelper(getActivity()).saveNewCountToLocalStorage(0, TabMenu.TAB_SECTION.ME);
        // notify about new count
        Intent intentNewCountPush = new Intent(Consts.ME_NEW_COUNT_EVENT);
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intentNewCountPush);
    }

    private void recevieNewActivityLog() {
        followerCountView.showAlert(LocalStorageHelper.defaultHelper(getActivity()).getNewCount(TabMenu.TAB_SECTION.ME) > 0);
    }

    @Override
    public void selectionItem(int position) {
//        lvActivityLog.smoothScrollToPosition(position);
    }

    @Override
    public void singleTap() {
        //refresh followInfo
        updateFollowInfo();
        if (followingCountView.isSelected()) {
            //online mode
            if (NetworkUtil.isConnected(getActivity())) {
                PAGE_NO_FOLLOW = 1;
                followingList.clear();
                fetchFollowingList(PAGE_NO_FOLLOW);
            } else {
                stopRefresh();
            }
        }
    }

    @Override
    public void doubleTap() {
        //refresh followInfo
        updateFollowInfo();
        followerCountView.performClick();
    }

    public abstract class EndlessScrollListener implements AbsListView.OnScrollListener {
        // The minimum amount of items to have below your current scroll position
        // before loading more.
        private int visibleThreshold = 5;
        // The current offset index of data you have loaded
        private int currentPage = 0;
        // The total number of items in the dataset after the last load
        private int previousTotalItemCount = 0;
        // True if we are still waiting for the last set of data to load.
        private boolean loading = true;
        // Sets the starting page index
        private int startingPageIndex = 0;

        public EndlessScrollListener() {
        }

        public EndlessScrollListener(int visibleThreshold) {
            this.visibleThreshold = visibleThreshold;
        }

        public EndlessScrollListener(int visibleThreshold, int startPage) {
            this.visibleThreshold = visibleThreshold;
            this.startingPageIndex = startPage;
            this.currentPage = startPage;
        }

        // This happens many times a second during a scroll, so be wary of the code you place here.
        // We are given a few useful parameters to help us work out if we need to load some more data,
        // but first we check if we are waiting for the previous load to finish.
        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            // If the total item count is zero and the previous isn't, assume the
            // list is invalidated and should be reset back to initial state
            if (totalItemCount < previousTotalItemCount) {
                this.currentPage = this.startingPageIndex;
                this.previousTotalItemCount = totalItemCount;
                if (totalItemCount == 0) {
                    this.loading = true;
                }
            }

            // If it’s still loading, we check to see if the dataset count has
            // changed, if so we conclude it has finished loading and update the current page
            // number and total item count.
            if (loading && (totalItemCount > previousTotalItemCount)) {
                loading = false;
                previousTotalItemCount = totalItemCount;
                currentPage++;
            }

            // If it isn’t currently loading, we check to see if we have breached
            // the visibleThreshold and need to reload more data.
            // If we do need to reload some more data, we execute onLoadMore to fetch the data.
            if (!loading && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {
                onLoadMore(currentPage + 1, totalItemCount);
                loading = true;
            }
        }

        // Defines the process for actually loading more data based on page
        public abstract void onLoadMore(int page, int totalItemsCount);

        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
            // Don't take any action on changed
        }

    }

    /**
     * get following list
     *
     * @param pageNo
     */
    private void fetchFollowingList(final int pageNo) {
        if (followingList == null)
            return;
        Log.w(TAG, "responseGson = " + "fetchFollowingList: " + pageNo);
        if (followingList.size() < 1 || PAGE_NO_FOLLOW > 1) {
            RequestFollowingList requestFollowingList = new RequestFollowingList(agentContent.memId, LocalStorageHelper.getAccessToken(getContext()), pageNo);
            requestFollowingList.callFollowingListApi(getActivity(), new ApiCallback() {
                @Override
                public void success(ApiResponse apiResponse) {
                    Log.w(TAG, "responseGson = " + apiResponse.jsonContent);

                    if (pageNo == 1) {
                        followingList = new ArrayList<Follower>();
                    }
                    //add new record to existing list
                    followingList.addAll(new Gson().fromJson(
                            apiResponse.jsonContent,
                            ResponseFollowingList.class).content.followers);

                    if (pageNo == 1) {
                        if (followingList.size() < 1) {
                            llEmptyActivityLog.setVisibility(View.VISIBLE);
                            tvEmptyLogMsg.setText(R.string.me__no_record_title);
                            tvEmptyLogMsg.setVisibility(View.VISIBLE);
                            stopRefresh();
                            return;
                        } else {
                            llEmptyActivityLog.setVisibility(View.GONE);
                        }
//                    LocalStorageHelper.defaultHelper(getActivity()).saveActivityLogListToLocalStorage((ArrayList) activityLogList);
                        followingAdapter = new FollowingAdapter(getActivity(), followingList, CreateUpdatePostFragment.this);
                        lvFollowingList.setAdapter(followingAdapter);
                    }
                    LocalStorageHelper.defaultHelper(getActivity()).saveMeFollowingListToLocalStorage(followingList);
                    stopRefresh();

                }

                @Override
                public void failure(String errorMsg) {
                    Log.w(TAG, "errorMsg = " + errorMsg);
                    stopRefresh();
                }
            });
        }
    }


    @Override
    public ArrayList<Follower> getFollowingList() {
        return followingList;
    }

    @Override
    public void onFollowItem(final Follower mFollower) {
        Log.d("toFollow", "toFollow agentContent: " + agentContent.toString());
        Log.d("toFollow", "toFollow mAgentProfile: " + mFollower.memberName + mFollower.isFollowing);

        ResponseLoginSocial.Content content = CoreData.getUserContent(getActivity());
        final RequestFollowAgent requestFollowAgent = new RequestFollowAgent(agentContent.agentProfile.memberID,  LocalStorageHelper.getAccessToken(getContext()));
        requestFollowAgent.followingListingId = mFollower.listingID;
        requestFollowAgent.followingMemId = mFollower.memberID;

        if (mFollower.isFollowing == 0) {
            // follow count +1
            followAgent(requestFollowAgent, mFollower);

        } else if (mFollower.isFollowing == 1) {

            FragmentManager fm = getChildFragmentManager();
            NewsFeedProfileFragment.MyAlertDialogFragment editNameDialog = NewsFeedProfileFragment.MyAlertDialogFragment
                    .newInstance(mFollower.memberName,

                            new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    // follow count -1
                                    unfollowAgent(requestFollowAgent, mFollower);
                                    dialog.dismiss();
                                }
                            });
            editNameDialog.show(fm, "fragment_edit_name");

        } else {
            // u are ureself
        }
    }


    private void followAgent(RequestFollowAgent followAgent, final Follower mFollower) {

        followAgent.callFollowApi(getActivity(), new ApiCallback() {
            @Override
            public void success(ApiResponse apiResponse) {
                Log.d("toFollow", "toFollow follow: " + mFollower.memberName + " " + mFollower.isFollowing);
//                        mAgentProfile.isFollowing = 1;
                mFollower.isFollowing = (mFollower.isFollowing == 0 ? 1 : 0);
                followingAdapter.notifyDataSetChanged();
                updateFollowInfo();

                CurrentFollower currentFollower = new CurrentFollower(agentContent.memId, mFollower.memberID,
                        agentContent.qbid, null, true);
                currentFollower.updateCurrentFollowingList(getActivity());

                //add local following people list
                LocalStorageHelper.addAgentProfileListToLocalStorage(getActivity(), mFollower.memberID);
            }

            @Override
            public void failure(String errorMsg) {
                Log.d("toFollow", "toFollow follow: " + errorMsg.toString());
            }
        });
    }

    private void unfollowAgent(RequestFollowAgent unfollowAgent, final Follower mFollower) {

        unfollowAgent.callUnfollowApi(getActivity(), new ApiCallback() {

            @Override
            public void success(ApiResponse apiResponse) {
                Log.d("toFollow", "toFollow unfollow: " + mFollower.memberName + " " + mFollower.isFollowing);
                mFollower.isFollowing = (mFollower.isFollowing == 0 ? 1 : 0);
                followingAdapter.notifyDataSetChanged();
//                        mAgentProfile.isFollowing = 0;
                updateFollowInfo();

                CurrentFollower currentFollower = new CurrentFollower(agentContent.memId, mFollower.memberID,
                        agentContent.qbid, null, false);
                currentFollower.updateCurrentFollowingList(getActivity());

                //add local following people list
                LocalStorageHelper.removeAgentProfileListToLocalStorage(getActivity(), mFollower.memberID);
            }

            @Override
            public void failure(String errorMsg) {

            }
        });
    }

    /**
     * get followers & following count
     */
    private void updateFollowInfo() {
        //offline mode
        if (!NetworkUtil.isConnected(getActivity())) {
            followerCountView.setCount(followerCount);
            followingCountView.setCount(followingCount);
            return;
        }

        RequestFollowInfo requestFollowInfo = new RequestFollowInfo(agentContent.memId, LocalStorageHelper.getAccessToken(getContext()));
        requestFollowInfo.callFollowInfoApi(getActivity(), new ApiCallback() {
            @Override
            public void success(ApiResponse apiResponse) {
                Log.d(TAG, "responseGson = " + apiResponse.jsonContent);
                ResponseFollowInfo.Content followInfo = new Gson().fromJson(apiResponse.jsonContent, ResponseFollowInfo.class).content;
                followerCountView.setCount(followInfo.followCount);
                followingCountView.setCount(followInfo.followingCount);
                LocalStorageHelper.saveFollowerCountToLocalStorage(followInfo.followCount);
            }

            @Override
            public void failure(String errorMsg) {
                Log.w(TAG, "errorMsg = " + errorMsg);
            }
        });
    }
}
