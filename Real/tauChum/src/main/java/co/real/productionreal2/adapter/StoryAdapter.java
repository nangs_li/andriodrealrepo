package co.real.productionreal2.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import co.real.productionreal2.R;
import co.real.productionreal2.model.AgentListing;
import co.real.productionreal2.util.AppUtil;
import co.real.productionreal2.util.TimeUtils;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by kelvinsun on 12/4/16.
 */
public class StoryAdapter extends BaseAdapter {

    public final static class StoryView {
        protected LinearLayout llHeader;
        protected TextView tvAddress;
        protected TextView tvAddressLong;
        protected ImageView ivStory;
        protected View divider;
        protected TextView tvHeader;
        protected TextView tvSince;

    }

    protected LayoutInflater mInflater;
    protected Context mContext;
    protected StoryAdapterListener mListener;

    public static interface StoryAdapterListener {
        public ArrayList<AgentListing> getStoryList();

        public void onCurrentStoryClick();
    }

    public StoryAdapter(Context context, StoryAdapterListener listener) {
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mListener = listener;

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return mListener.getStoryList().size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
//        return mProductList.get(position);
        return mListener.getStoryList().get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view;
        view = newView(parent, position);
        bindView(view, position);
        return view;
    }

    protected View newView(final ViewGroup parent, final int position) {
        if (mInflater == null) {
            mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        View view;
        view = mInflater.inflate(R.layout.list_item_story, parent, false);
        cacheView(view, position);

        return view;
    }

    protected void cacheView(View view, final int position) {
        final StoryView views = new StoryView();
        // find view to cache
        views.llHeader = (LinearLayout) view.findViewById(R.id.ll_header);
        views.ivStory = (ImageView) view.findViewById(R.id.iv_story);
        views.tvAddress = (TextView) view.findViewById(R.id.tv_story_address);
        views.tvAddressLong = (TextView) view.findViewById(R.id.tv_story_address_long);
        views.tvHeader = (TextView) view.findViewById(R.id.tv_story_header);
        views.tvSince = (TextView) view.findViewById(R.id.tv_story_since);
        views.divider = (View) view.findViewById(R.id.line_horizontal);


        final AgentListing mAgentListing = mListener.getStoryList().get(position);

        //style control
        views.llHeader.setVisibility(position < 2 ? View.VISIBLE : View.GONE);
        views.tvSince.setVisibility(position < 1 ? View.VISIBLE : View.GONE);
        if (position == 0) {
            views.tvHeader.setTextColor(mContext.getResources().getColor(R.color.white));
            views.tvAddress.setTextColor(mContext.getResources().getColor(R.color.white));
            views.tvAddressLong.setTextColor(mContext.getResources().getColor(R.color.white));
            views.divider.setBackgroundColor(mContext.getResources().getColor(R.color.dark_grey));
            views.tvHeader.setText(mContext.getResources().getString(R.string.me__current_story));

            //get time since

            Date today = new Date();
            ArrayList<Integer> diffDateList = null;
            String diffStr;
            Date createDate;
            try {
                createDate = TimeUtils.getDateFromString(mAgentListing.creationDateString);
            } catch (ParseException e) {
                e.printStackTrace();
                createDate = today;
            }
            diffStr = TimeUtils.timeDifferenceFromNow(createDate.getTime(),mContext);
            views.tvSince.setText(diffStr);
        } else if (position == 1) {
            views.tvHeader.setTextColor(mContext.getResources().getColor(R.color.dark_grey));
            views.tvAddress.setTextColor(mContext.getResources().getColor(R.color.dark_grey));
            views.tvAddressLong.setTextColor(mContext.getResources().getColor(R.color.dark_grey));
            views.divider.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.dash_line_horizontal_light));
            views.tvHeader.setText(mContext.getResources().getString(R.string.me__previous_storys));
        } else {
            views.tvHeader.setTextColor(mContext.getResources().getColor(R.color.dark_grey));
            views.tvAddress.setTextColor(mContext.getResources().getColor(R.color.dark_grey));
            views.tvAddressLong.setTextColor(mContext.getResources().getColor(R.color.dark_grey));
            views.divider.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.dash_line_horizontal_light));
        }

        String imageName = AppUtil.getPropertyTypeImageName(mAgentListing.propertyType,
                mAgentListing.spaceType);
        views.ivStory.setImageResource(mContext.getResources().getIdentifier(imageName + (position > 0 ? "_off" : "_on"), "drawable", mContext.getPackageName()));

        views.tvAddress.setText(mAgentListing.googleAddresses.get(0).name);
        views.tvAddressLong.setText(mAgentListing.googleAddresses.get(0).address);

//        views.tvRegion.setText(mFollower.location);
//        Picasso.with(mContext).load(mFollower.agentPhotoURL)
//                .transform(new RoundedTransformation()).into(views.ivImage);

        view.setTag(views);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position == 0)
                    mListener.onCurrentStoryClick();
            }
        });
    }

    protected void bindView(View view, final int position) {
        final AgentListing agentListing = (AgentListing) getItem(position);

//        final FollowingView cachedView = (FollowingView) view.getTag();
//        if (follower.isFollowing == 1) {
//            cachedView.btnFollower.setStyle(Constants.STYLE_TYPE.AGENT_FOLLOWED);
//        } else {
//            cachedView.btnFollower.setStyle(Constants.STYLE_TYPE.AGENT_UNFOLLOWED);
//        }
    }


    private ArrayList<Integer> calDiff(Date startDate, Date endDate) {

        long different = endDate.getTime() - startDate.getTime();
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;
        long weeksInMilli = daysInMilli * 7;

        long elapsedWeeks = different / weeksInMilli;
        different = different % weeksInMilli;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;
        ArrayList<Integer> calDiffList = new ArrayList<>();
        calDiffList.add((int) elapsedWeeks);
        calDiffList.add((int) elapsedDays);
        calDiffList.add((int) elapsedHours);
        return calDiffList;
    }

}