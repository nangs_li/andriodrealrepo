package co.real.productionreal2.activity.setting;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.mixpanel.android.mpmetrics.MixpanelAPI;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import co.real.productionreal2.Constants;
import co.real.productionreal2.CoreData;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.BaseActivity;
import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.callback.DialogCallback;
import co.real.productionreal2.common.LocalStorageHelper;
import co.real.productionreal2.config.AppConfig;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.request.RequestRealNetworkChangeEmail;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;
import co.real.productionreal2.service.model.response.ResponseRealNetworkMemReg;
import co.real.productionreal2.util.CryptLib;
import co.real.productionreal2.util.ValidUtil;
import co.real.productionreal2.view.Dialog;

;

public class ChangeEmailSettingActivity extends BaseActivity {
    @InjectView(R.id.headerLayout)
    View headerLayout;
    @InjectView(R.id.currentEmailTextView)
    TextView currentEmailTextView;
    @InjectView(R.id.newEmailEditText)
    EditText newEmailEditText;
    @InjectView(R.id.pwEditText)
    EditText pwEditText;

    private ResponseLoginSocial.Content userContent;
    private ResponseRealNetworkMemReg realLoginContent;
    private ProgressDialog ringProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_email_setting);
        ButterKnife.inject(this);

        pwEditText.setTypeface(Typeface.DEFAULT);

        userContent = CoreData.getUserContent(this);
        SharedPreferences prefs = getSharedPreferences(Constants.PREF_REAL_NETWORK, MODE_PRIVATE);
        String realJson = LocalStorageHelper.getRealNetworkJson(this);
        ResponseRealNetworkMemReg realNetworkMemReg = new Gson().fromJson(realJson, ResponseRealNetworkMemReg.class);
        if (realNetworkMemReg != null && realNetworkMemReg.content != null) {
            realLoginContent = realNetworkMemReg;
            initView();
        } else {
            finish();
        }

    }

    private void initView() {
        TextView titleTextView = (TextView) headerLayout.findViewById(R.id.titleTextView);
        titleTextView.setText(R.string.change_email__title);

        if (realLoginContent == null)
            return;
        currentEmailTextView.setText(realLoginContent.content.email);

    }

    @OnClick(R.id.changeEmailBtn)
    public void changeEmailBtnClick(View view) {
        if (pwEditText.getText().toString().isEmpty()) {
            Dialog.normalDialog(this,getString(R.string.error_message__passowrd_cannot)).show();
            return;
        } else if (!ValidUtil.isValidPassword(pwEditText.getText().toString())) {
            Dialog.pwNotValidDialog(this).show();
            return;
        } else if (!ValidUtil.isValidEmail(newEmailEditText.getText().toString())) {
            Dialog.emailNotValidDialog(this).show();
            return;
        } else {
            if (ringProgressDialog != null && ringProgressDialog.isShowing())
                return;
            ringProgressDialog = ProgressDialog.show(this, "",
                    getResources().getString(R.string.alert__Initializing));
            final String newEmail = newEmailEditText.getText().toString();
            RequestRealNetworkChangeEmail realNetworkChangeEmail = new RequestRealNetworkChangeEmail(newEmail,
                    realLoginContent.content.userId,
                    realLoginContent.content.accessToken,
                    CryptLib.md5(pwEditText.getText().toString()));
            realNetworkChangeEmail.callRealNetworkChangeEmailApi(this, new ApiCallback() {
                @Override
                public void success(ApiResponse apiResponse) {
                    if (ringProgressDialog != null) {
                        ringProgressDialog.dismiss();
                        ringProgressDialog.cancel();
                        ringProgressDialog = null;
                    }

                    currentEmailTextView.setText(newEmail);
                    newEmailEditText.setText("");
                    pwEditText.setText("");
                    SharedPreferences.Editor editor = getSharedPreferences(Constants.PREF_REAL_NETWORK, MODE_PRIVATE).edit();
                    realLoginContent.content.email = newEmail;
                    String json = new Gson().toJson(realLoginContent, ResponseRealNetworkMemReg.class);
                    LocalStorageHelper.setRealNetworkJson(ChangeEmailSettingActivity.this,json);
                    // Mixpanel
                    MixpanelAPI mixpanel = MixpanelAPI.getInstance(ChangeEmailSettingActivity.this, AppConfig.getMixpanelToken());
                    JSONObject properties = new JSONObject();
                    try {
                        properties.put("Old Email", realLoginContent.content.email);
                        properties.put("New Email", newEmail);
                        mixpanel.track("Changed Email Address", properties);
                        mixpanel.getPeople().set("$email", newEmail);
                    } catch (JSONException e) {

                    }

                    Dialog.feedbackDialog(ChangeEmailSettingActivity.this, true, "",
                            getString(R.string.error_message__change_email_success),
                            new DialogCallback() {
                                @Override
                                public void yes() {

                                    onBackPressed();
                                }
                            }).show();
                }

                @Override
                public void failure(String errorMsg) {
                    if (errorMsg != "") {
                        Dialog.normalDialog(ChangeEmailSettingActivity.this, errorMsg).show();
                    }
                    if (ringProgressDialog != null) {
                        ringProgressDialog.dismiss();
                        ringProgressDialog.cancel();
                        ringProgressDialog = null;
                    }
                }
            });
        }

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
