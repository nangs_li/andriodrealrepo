package co.real.productionreal2.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.mixpanel.android.mpmetrics.MixpanelAPI;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import co.real.productionreal2.Constants;
import co.real.productionreal2.CoreData;
import co.real.productionreal2.Navigation;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.MainActivityTabBase;
import co.real.productionreal2.activity.createpost.CreatePostPropertyActivity;
import co.real.productionreal2.activity.viral.InviteToFollowActivity;
import co.real.productionreal2.activity.profile.EditAgentProfileActivity;
import co.real.productionreal2.adapter.AgentProfilePagerAdapter;
import co.real.productionreal2.callback.FragmentViewChangeListener;
import co.real.productionreal2.config.AppConfig;
import co.real.productionreal2.contianer.MeTabContainer;
import co.real.productionreal2.newsfeed.NewsFeedDetailFragment;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;
import co.real.productionreal2.util.ImageUtil;
import co.real.productionreal2.view.TitleBar;
import lib.blurbehind.OnBlurCompleteListener;
import widget.EnbleableSwipeViewPager;
import widget.viewpagerindicator.CirclePageIndicator;

;

/**
 * Created by hohojo on 5/10/2015.
 */
public class AgentProfileFragment extends Fragment {
    private static final String TAG = "AgentProfileFragment";
    private FragmentViewChangeListener fragmentViewChangeListener;
    @InjectView(R.id.notCompleteAgentProfileViewPager)
    EnbleableSwipeViewPager agentProfileViewPager;
    @InjectView(R.id.maskView)
    View maskView;
    @InjectView(R.id.rootRelativeLayout)
    RelativeLayout rootRelativeLayout;
    @InjectView(R.id.indicator)
    CirclePageIndicator mIndicator;

    public LinearLayout pagerIndicator;

    private AgentProfilePagerAdapter agentProfilePagerAdapter;
    private static String PAGE_NO = "pageNo";
    private int pageNo;
    private TitleBar mTitleBar;
    private AgentProfileFragment fragment;
    ResponseLoginSocial.Content userContent;

    private int requestCode;
    private int resultCode;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
        fragmentViewChangeListener.changeTabTitle(this);
        agentProfilePagerAdapter.notifyDataSetChanged();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            fragmentViewChangeListener = (FragmentViewChangeListener) activity;
        } catch (ClassCastException e) {
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        fragmentViewChangeListener = null;
    }

    public static AgentProfileFragment newInstance(int pageNo, int requestCode, int resultCode) {
        Bundle args = new Bundle();
        args.putInt(PAGE_NO, pageNo);
        args.putInt(MeTabContainer.REQUEST_CODE, requestCode);
        args.putInt(MeTabContainer.RESULT_CODE, resultCode);
        AgentProfileFragment fragment = new AgentProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Log.w(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        Log.v("","edwin onCreate11");
        userContent = CoreData.getUserContent(getActivity());
        Log.v("","edwin onCreate1122");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.w(TAG, "onCreateView");
        View view = inflater.inflate(R.layout.fragment_agent_profile, container, false);
        ButterKnife.inject(this, view);


        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getArguments() != null) {
            requestCode = getArguments().getInt(MeTabContainer.REQUEST_CODE);
            resultCode = getArguments().getInt(MeTabContainer.RESULT_CODE);
        }

        pagerIndicator = (LinearLayout) view.findViewById(R.id.llMePagerIndicator);
//        initView();
    }


    @Override
    public void onResume() {
        Log.w(TAG, "onResume" + userContent.agentProfile.toString());
        super.onResume();
    }

    @Override
    public void onStart() {
        Log.w(TAG, "onStart");
        super.onStart();
    }

    @Override
    public void onStop() {
        Log.w(TAG, "onStop");
        super.onStop();
    }


    private void initView() {
        //init title bar
        mTitleBar = ((MainActivityTabBase) getActivity()).getTitleBar();

        agentProfilePagerAdapter = new AgentProfilePagerAdapter(getChildFragmentManager(), userContent, getFragments());
        agentProfileViewPager.setAdapter(agentProfilePagerAdapter);
//        agentProfileIndictor.setViewPager(notCompleteAgentProfileViewPager);
        agentProfileViewPager.setCurrentItem((Integer) getArguments().get(PAGE_NO));
        agentProfileViewPager.setOffscreenPageLimit(2);
        agentProfileViewPager.setSwipeable(true);
        mIndicator.setViewPager(agentProfileViewPager);
        agentProfileViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                if (position == AgentProfilePagerAdapter.CREATE_POST_OR_PREVIEW) {
                    mTitleBar.setupTitleBar(TitleBar.TAB_TYPE.ME_PREVIEW);
                    fragmentViewChangeListener.hideTabBar();
                    if (userContent != null && userContent.agentProfile != null && userContent.agentProfile.agentListing != null
                            && userContent.agentProfile.agentListing.googleAddresses != null
                            && userContent.agentProfile.agentListing.googleAddresses.get(0) != null)
                        mTitleBar.setTitle(userContent.agentProfile.agentListing.googleAddresses.get(0).name);
                } else if (position == AgentProfilePagerAdapter.EDIT_PROFILE) {
                    //Edit profile page
                    mTitleBar.setupTitleBar(TitleBar.TAB_TYPE.ME_PROFILE);
                    fragmentViewChangeListener.hideTabBar();
                    mTitleBar.getEditProfileBtn().setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            // Mixpanel
                            MixpanelAPI mixpanel = MixpanelAPI.getInstance(getActivity(), AppConfig.getMixpanelToken());
                            mixpanel.track("Edited Profile");


                            ImageUtil.getInstance().prepareBlurImageFromActivity(getActivity(), EditAgentProfileActivity.class, new OnBlurCompleteListener() {
                                @Override
                                public void onBlurComplete() {
                                    Intent intent = new Intent(getActivity(), EditAgentProfileActivity.class);
                                    Navigation.presentIntent(getActivity(),intent);
                                }
                            });

                        }
                    });
                } else {
                    //me home
                    mTitleBar.setupTitleBar(TitleBar.TAB_TYPE.ME_HOME_AGENT);
                    fragmentViewChangeListener.openTabBar();
                    mTitleBar.getInviteFollowBtn().setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            // Mixpanel
                            MixpanelAPI mixpanel = MixpanelAPI.getInstance(getActivity(), AppConfig.getMixpanelToken());
                            mixpanel.track("View Invite to Follow @ Me");

                            InviteToFollowActivity.start(getActivity());
                        }
                    });
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                Log.w(TAG, "onPageScrollStateChanged state = " + state + " currentItem: " + agentProfileViewPager.getCurrentItem());
                if (state == ViewPager.SCROLL_STATE_DRAGGING) {
                    if (agentProfileViewPager.getCurrentItem() == AgentProfilePagerAdapter.ME_HOME) {
                        fragmentViewChangeListener.hideTabBar();
                    }
                } else if (state == ViewPager.SCROLL_STATE_SETTLING) {
                    if (agentProfileViewPager.getCurrentItem() == AgentProfilePagerAdapter.CREATE_POST_OR_PREVIEW) {
                        if (userContent.memeberType == Constants.MEM_TYPE_USER && userContent.agentProfile.agentListing == null) {


                            ImageUtil.getInstance().prepareBlurImageFromActivity(getActivity(), CreatePostPropertyActivity.class, new OnBlurCompleteListener() {
                                @Override
                                public void onBlurComplete() {
                                    Intent intent = new Intent(getActivity(), CreatePostPropertyActivity.class);
                                    Navigation.presentIntentForResult(getActivity(),intent,Constants.ME_TO_CREATE_POST);
                                }
                            });

                        }
                    } else if (agentProfileViewPager.getCurrentItem() == AgentProfilePagerAdapter.ME_HOME) {
                        fragmentViewChangeListener.openTabBar();
                    }
                }
            }
        });
    }

    private List<Fragment> getFragments() {
        List<Fragment> fList = new ArrayList<Fragment>();
        ViewAgentProfileFragment viewAgentProfileFragment = ViewAgentProfileFragment.newInstance(agentProfileViewPager);
        CreateUpdatePostFragment createUpdatePostFragment = CreateUpdatePostFragment.newInstance(requestCode, resultCode,agentProfileViewPager);
        ToCreatePostFragment toCreatePostFragment = ToCreatePostFragment.newInstance();
        NewsFeedDetailFragment newsFeedDetailFragment = NewsFeedDetailFragment.newInstance("Fragment 3", agentProfileViewPager,  pagerIndicator,true);
        CoreData.isAgentProfileUpdate = true;

        fList.add(viewAgentProfileFragment);
        fList.add(createUpdatePostFragment);
        fList.add(toCreatePostFragment);
        fList.add(newsFeedDetailFragment);
        return fList;
    }

    public void gotoPage(int page){
        agentProfileViewPager.setCurrentItem(page);
    }
}

