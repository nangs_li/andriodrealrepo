package co.real.productionreal2.activity.setting;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;
import android.widget.ListView;
import android.widget.TextView;

import com.mixpanel.android.mpmetrics.MixpanelAPI;

import java.util.List;
import java.util.Locale;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import co.real.productionreal2.CoreData;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.BaseActivity;
import co.real.productionreal2.activity.MainActivityTabBase;
import co.real.productionreal2.common.LocalStorageHelper;
import co.real.productionreal2.config.AppConfig;
import co.real.productionreal2.model.SystemSetting;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;
import co.real.productionreal2.util.AppUtil;

;

public class ChangeLangSettingActivity extends BaseActivity {
    @InjectView(R.id.lvLang)
    ListView lvLang;
    @InjectView(R.id.headerLayout)
    View headerLayout;

    private ResponseLoginSocial.Content userContent;
    private LangAdapter langAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_lang);
        ButterKnife.inject(this);
        userContent = CoreData.getUserContent(this);

        initView();

    }

    private int getInitLangPos() {
        for (int i = 0; i < userContent.systemSetting.sysLangList.size(); i++) {
            if (AppUtil.getCurrentLangIndex(this) == userContent.systemSetting.sysLangList.get(i).index)
                return i;
        }
        return 0;
    }

    private void initView() {
        TextView titleTextView = (TextView) headerLayout.findViewById(R.id.titleTextView);
        titleTextView.setText(R.string.common__languages);


        langAdapter = new LangAdapter(this, userContent.systemSetting.sysLangList, getInitLangPos());

        lvLang.setAdapter(langAdapter);
        lvLang.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Mixpanel
                MixpanelAPI mixpanel = MixpanelAPI.getInstance(ChangeLangSettingActivity.this, AppConfig.getMixpanelToken());
                Locale locale = LocalStorageHelper.getCurrentLocale(ChangeLangSettingActivity.this);
                mixpanel.getPeople().set("Language", locale.getLanguage());

                langAdapter.setSelectedPos(position);

//                Intent intent = new Intent(ChangeLangSettingActivity.this, MainActivityTabBase.class);
//                intent.putExtra(Constants.EXTRA_SELECT_TAB, Constants.SELECT_SETTING);
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(intent);
//                finish();

//                setResult(RESULT_OK);

            }
        });

    }

    private class LangAdapter extends BaseAdapter {
        private ViewHolder holder;
        private List<SystemSetting.SystemLanguage> sysLangList;
        private Context context;
        private int selectedPos;


        public LangAdapter(Context context, List<SystemSetting.SystemLanguage> sysLangList, int initLangPos) {
            this.context = context;
            this.sysLangList = sysLangList;
            selectedPos = initLangPos;
        }

        public int getSelectedPos() {
            return selectedPos;
        }

        public void setSelectedPos(int selectedPos) {
            this.selectedPos = selectedPos;
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return sysLangList.size();
        }

        @Override
        public Object getItem(int position) {
            return sysLangList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.list_item_single_choice, null);
                holder = createViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.checkedTextView.setText(sysLangList.get(position).nativeName);
            if (position == selectedPos)
                holder.checkedTextView.setChecked(true);
            else
                holder.checkedTextView.setChecked(false);
            return convertView;
        }

        private ViewHolder createViewHolder(View v) {
            ViewHolder holder = new ViewHolder();
            holder.checkedTextView = (CheckedTextView) v.findViewById(R.id.checkedTextView);
            return holder;
        }


    }

    private static class ViewHolder {
        public CheckedTextView checkedTextView;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @OnClick(R.id.nextBtn)
    public void nextBtnClick(View view) {
        if (userContent.systemSetting.sysLangList.get(langAdapter.getSelectedPos()).code.equalsIgnoreCase("zh-CHT")) {
            AppUtil.setLocale(ChangeLangSettingActivity.this, Locale.TRADITIONAL_CHINESE);
        } else if (userContent.systemSetting.sysLangList.get(langAdapter.getSelectedPos()).code.equalsIgnoreCase("zh-CHS")) {
            AppUtil.setLocale(ChangeLangSettingActivity.this, Locale.SIMPLIFIED_CHINESE);
        } else {
            AppUtil.setLocale(ChangeLangSettingActivity.this, userContent.systemSetting.sysLangList.get(langAdapter.getSelectedPos()).code);
        }
        Intent intent = new Intent(ChangeLangSettingActivity.this, MainActivityTabBase.class);
//        intent.putExtra(Constants.EXTRA_SELECT_TAB, Constants.SELECT_SETTING);
        intent.putExtra("isNeedRestart",true);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);

        startActivity(intent);
//        finish();

        Log.d("ApplyChange","ApplyChange: index: "+AppUtil.getCurrentLangIndex(this));
        Log.d("ApplyChange","ApplyChange: locale: "+LocalStorageHelper.getCurrentLocale(this).toString());

    }
}
