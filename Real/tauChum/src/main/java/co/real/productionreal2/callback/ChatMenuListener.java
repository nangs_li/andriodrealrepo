package co.real.productionreal2.callback;

/**
 * Created by hohojo on 20/11/2015.
 */
public interface ChatMenuListener {
    public void chatDelClick();
    public void inviteToChatClick();
    //    public void agentSearchClick();
    public void filterLocalAgentSearch(String filterText);
    public void filterApiAgentSearch(String filterText);

}
