package co.real.productionreal2.activity.viral;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import butterknife.ButterKnife;
import co.real.productionreal2.Navigation;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.BaseActivity;
import co.real.productionreal2.fragment.BroadcastFragment;
import co.real.productionreal2.fragment.ChangePhoneNoIntroFragment;
import co.real.productionreal2.fragment.FindYourAgentFragment;
import co.real.productionreal2.util.AppUtil;
import co.real.productionreal2.util.ImageUtil;
import lib.blurbehind.OnBlurCompleteListener;

/**
 * Created by kelvinsun on 31/5/16.
 */
public class ConnectAgentActivity extends BaseActivity {

    int mStackLevel = 1;
    private boolean canReadContact = true;
    private ProgressDialog progressDialog;

    public enum ConnectAgentActivityType {
        NORMAL(0),
        BROADCAST(1);

        private final int value;

        private ConnectAgentActivityType(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    public static void start(final Context context) {

        ImageUtil.getInstance().prepareBlurImageFromActivity((Activity) context, ConnectAgentActivity.class, new OnBlurCompleteListener() {
            @Override
            public void onBlurComplete() {
                Intent intent = new Intent(context, ConnectAgentActivity.class);
                String action = String.valueOf(0);
                intent.setAction(action);
                Navigation.presentIntent((Activity) context, intent);
            }
        });
    }


    public static void start(final Context context, final ConnectAgentActivityType type) {

        ImageUtil.getInstance().prepareBlurImageFromActivity((Activity) context, ConnectAgentActivity.class, new OnBlurCompleteListener() {
            @Override
            public void onBlurComplete() {
                Intent intent = new Intent(context, ConnectAgentActivity.class);
                String action = String.valueOf(type.getValue());
                intent.setAction(action);
                Navigation.presentIntentForResult((Activity) context, intent, type.getValue());
            }
        });

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_agent);

//        AppUtil.setBlurBehind(this);
        ButterKnife.inject(this);
        if (canReadContact) {
            Fragment newFragment;
            if (activityType()== ConnectAgentActivityType.NORMAL.getValue()) {
                newFragment = FindYourAgentFragment.newInstance(mStackLevel);
            }else{
                newFragment = BroadcastFragment.newInstance(mStackLevel);
            }
            showDefaultAgentList(newFragment);
        } else {
            //cant read local contact
        }
    }

    void showDefaultAgentList(Fragment fragment) {

        // Instantiate a new fragment.
//        Fragment newFragment = FindYourAgentFragment.newInstance(mStackLevel);

        // Add the fragment to the activity, pushing this transaction
        // on to the back stack.
        FragmentTransaction ft = getFragmentManager().beginTransaction();
//        ft.setCustomAnimations(R.animator.fragment_slide_left_enter,
//                R.animator.fragment_slide_left_exit,
//                R.animator.fragment_slide_right_enter,
//                R.animator.fragment_slide_right_exit);
        ft.replace(R.id.fl_main_container, fragment);
        ft.commit();
    }

    private int activityType() {

        int typeInt = Integer.parseInt(getIntent().getAction());
        return typeInt;
    }

    public void showLoadingDialog() {
        if(progressDialog==null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage(getString(R.string.alert__Initializing));
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
        }
        if (!progressDialog.isShowing())
            progressDialog.show();

    }

    public void dismissLoadingDialog() {
        if (progressDialog!=null && progressDialog.isShowing()) {
            progressDialog.cancel();
        }
    }



}
