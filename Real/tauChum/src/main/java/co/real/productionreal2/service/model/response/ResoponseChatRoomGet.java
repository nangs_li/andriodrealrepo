package co.real.productionreal2.service.model.response;

import com.google.gson.annotations.SerializedName;
import co.real.productionreal2.service.model.ChatRoom;

import java.util.List;

/**
 * Created by hohojo on 18/11/2015.
 */
public class ResoponseChatRoomGet extends ResponseBase{
    @SerializedName("Content")
    public Content content;

    public class Content {
        @SerializedName("TotalCount")
        public int totalCount;

        @SerializedName("RecordCount")
        public int recordCount;

        @SerializedName("ChatRoom")
        public List<ChatRoom> chatRoomList;
    }
}
