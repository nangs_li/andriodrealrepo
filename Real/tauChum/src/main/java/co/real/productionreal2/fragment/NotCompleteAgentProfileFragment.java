package co.real.productionreal2.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.mixpanel.android.mpmetrics.MixpanelAPI;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import co.real.productionreal2.CoreData;
import co.real.productionreal2.Navigation;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.MainActivityTabBase;
import co.real.productionreal2.activity.profile.EditAgentProfileActivity;
import co.real.productionreal2.adapter.NotCompleteAgentProfileAdapter;
import co.real.productionreal2.callback.FragmentViewChangeListener;
import co.real.productionreal2.config.AppConfig;
import co.real.productionreal2.contianer.MeTabContainer;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;
import co.real.productionreal2.util.ImageUtil;
import co.real.productionreal2.view.TitleBar;
import lib.blurbehind.OnBlurCompleteListener;
import widget.EnbleableSwipeViewPager;
import widget.viewpagerindicator.CirclePageIndicator;

;

/**
 * Created by hohojo on 9/3/2016.
 */
public class NotCompleteAgentProfileFragment extends Fragment {
    private static final String TAG = "NotCompleteAgentProfileFragment ";
    private FragmentViewChangeListener fragmentViewChangeListener;
    @InjectView(R.id.notCompleteAgentProfileViewPager)
    EnbleableSwipeViewPager notCompleteAgentProfileViewPager;
    @InjectView(R.id.maskView)
    View maskView;
    @InjectView(R.id.rootRelativeLayout)
    RelativeLayout rootRelativeLayout;
    @InjectView(R.id.indicator)
    CirclePageIndicator mIndicator;

//    private NewsFeedDetailFragment newsFeedDetailFragment;

    public static LinearLayout llMePagerIndicator;

    private NotCompleteAgentProfileAdapter notCompleteAgentProfileAdapter;
    private byte[] backgroundBitmapBytes;

    private static String PAGE_NO = "pageNo";
    private int pageNo;
    private TitleBar mTitleBar;
    ResponseLoginSocial.Content userContent;

    private int requestCode;
    private int resultCode;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        notCompleteAgentProfileViewPager.setSwipeable(true);
        initView();
        fragmentViewChangeListener.changeTabTitle(this);
        notCompleteAgentProfileAdapter.notifyDataSetChanged();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            fragmentViewChangeListener = (FragmentViewChangeListener) activity;
        } catch (ClassCastException e) {
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        fragmentViewChangeListener = null;
    }

    public static NotCompleteAgentProfileFragment newInstance(int pageNo, int requestCode, int resultCode) {
        Bundle args = new Bundle();
        args.putInt(PAGE_NO, pageNo);
        args.putInt(MeTabContainer.REQUEST_CODE, requestCode);
        args.putInt(MeTabContainer.RESULT_CODE, resultCode);
        NotCompleteAgentProfileFragment fragment = new NotCompleteAgentProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userContent = CoreData.getUserContent(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_not_complete_agent_profile, container, false);
        ButterKnife.inject(this, view);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getArguments() != null) {
            requestCode = getArguments().getInt(MeTabContainer.REQUEST_CODE);
            resultCode = getArguments().getInt(MeTabContainer.RESULT_CODE);
        }

        llMePagerIndicator = (LinearLayout) view.findViewById(R.id.llMePagerIndicator);
//        initView();
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }


    private void viewProfileTitleBarSetting() {
        mTitleBar.setupTitleBar(TitleBar.TAB_TYPE.ME_PROFILE);
        mTitleBar.getEditProfileBtn().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Mixpanel
                MixpanelAPI mixpanel = MixpanelAPI.getInstance(getActivity(), AppConfig.getMixpanelToken());
                mixpanel.track("Edited Profile");
                ImageUtil.getInstance().prepareBlurImageFromActivity(getActivity(), EditAgentProfileActivity.class, new OnBlurCompleteListener() {
                    @Override
                    public void onBlurComplete() {
                        Intent intent = new Intent(getActivity(), EditAgentProfileActivity.class);
                        Navigation.presentIntent(getActivity(),intent);
                    }
                });

            }
        });
    }

    private void homeTitleBarSetting() {
        mTitleBar.setupTitleBar(TitleBar.TAB_TYPE.ME_HOME_AGENT);
        mTitleBar.getInviteFollowBtn().setVisibility(View.GONE);
    }

    private void initView() {
        //init title bar
        mTitleBar = ((MainActivityTabBase) getActivity()).getTitleBar();
        notCompleteAgentProfileAdapter = new NotCompleteAgentProfileAdapter(getChildFragmentManager(), userContent, getFragments());
        notCompleteAgentProfileViewPager.setAdapter(notCompleteAgentProfileAdapter);

        //setup dummy indicator for view pager
        NotCompleteAgentProfileAdapter notCompleteAgentProfileAdapterDummy = new NotCompleteAgentProfileAdapter(getChildFragmentManager(), userContent, getDummyFragments());
        ViewPager notCompleteAgentProfileViewPagerDummy=new ViewPager(getActivity());
        notCompleteAgentProfileViewPagerDummy.setAdapter(notCompleteAgentProfileAdapterDummy);
        mIndicator.setViewPager(notCompleteAgentProfileViewPagerDummy);
        notCompleteAgentProfileViewPager.setCurrentItem(0);
        mIndicator.setCurrentItem(1);

        notCompleteAgentProfileViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            private static final float thresholdOffset = 0.5f;
            private boolean scrollStarted, checkDirection;

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                Log.d("onPageScrolled", "onPageScrolled: " + position + " " + positionOffset);
            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    fragmentViewChangeListener.openTabBar();
                    homeTitleBarSetting();
                    mIndicator.setCurrentItem(1);
                } else if (position==1) {
                    fragmentViewChangeListener.hideTabBar();
                    viewProfileTitleBarSetting();
                    mIndicator.setCurrentItem(2);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

    }

    public CreateUpdatePostFragment createUpdatePostFragment;

    private List<Fragment> getFragments() {
        List<Fragment> fList = new ArrayList<Fragment>();
        createUpdatePostFragment = CreateUpdatePostFragment.newInstance(requestCode, resultCode,notCompleteAgentProfileViewPager);
        ViewAgentProfileFragment viewAgentProfileFragment = ViewAgentProfileFragment.newInstance(notCompleteAgentProfileViewPager);

        fList.add(createUpdatePostFragment);
        fList.add(viewAgentProfileFragment);
        return fList;
    }


    //a dummy collection for indicator
    private List<Fragment> getDummyFragments() {
        List<Fragment> fList = new ArrayList<Fragment>();
        Fragment dummyFragment1 = new Fragment();
        Fragment dummyFragment2 = new Fragment();
        Fragment dummyFragment3 = new Fragment();

        fList.add(dummyFragment1);
        fList.add(dummyFragment2);
        fList.add(dummyFragment3);
        return fList;
    }

    private int getCompleteRate() {
        int completeRate = 0;
        if (userContent == null)
            return 0;
        if (userContent.agentProfile == null)
            return 0;

        if (userContent.agentProfile.agentExpList != null && userContent.agentProfile.agentExpList.size() > 0) {
            completeRate = completeRate + 20;
        }
        if (userContent.agentProfile.agentLicenseNumber != null && !userContent.agentProfile.agentLicenseNumber.isEmpty()) {
            completeRate = completeRate + 20;
        }
        if (userContent.agentProfile.agentLanguageList != null && userContent.agentProfile.agentLanguageList.size() > 0) {
            completeRate = completeRate + 20;
        }
        if (userContent.agentProfile.agentSpecialtyList != null && userContent.agentProfile.agentSpecialtyList.size() > 0) {
            completeRate = completeRate + 20;
        }
        if (userContent.agentProfile.agentPastClosingList != null && userContent.agentProfile.agentPastClosingList.size() > 0) {
            completeRate = completeRate + 20;
        }
        return completeRate;
    }
}
