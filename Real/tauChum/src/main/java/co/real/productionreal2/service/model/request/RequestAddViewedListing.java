package co.real.productionreal2.service.model.request;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.model.InputRequest;
import co.real.productionreal2.service.OperationService;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.response.ResponseChatRoomGet;
import co.real.productionreal2.view.Dialog;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by kelvinsun on 11/1/16.
 */
public class RequestAddViewedListing extends RequestBase {
    public RequestAddViewedListing(int memberID, String accessToken, String uniqueKey, int viewedMemberID, RequestAddressSearch.GoogleAddressPack googleAddress) {
        super(memberID, accessToken);
        this.viewedMemberID = viewedMemberID;
        this.googleAddress = googleAddress;

    }

    @SerializedName("ViewedMemberID")
    public int viewedMemberID;
    @SerializedName("GoogleAddressPack")
    public RequestAddressSearch.GoogleAddressPack googleAddress;

    public void callViewedListingApi(final Context context, final ApiCallback callback) {
        String json = new Gson().toJson(this);
        Log.d("callViewedListingApi", "callViewedListingApi: " + json);
        final OperationService weatherService = new OperationService(context);
        weatherService.getCoffeeService().addViewedListing(new InputRequest(json), new Callback<ApiResponse>() {
            @Override
            public void success(ApiResponse apiResponse, Response response) {
                ResponseChatRoomGet responseGson = new Gson().fromJson(apiResponse.jsonContent, ResponseChatRoomGet.class);
                if (responseGson.errorCode == 4) {
                    Dialog.accessErrorForceLogoutDialog(context).show();
                } else if (responseGson.errorCode == 26) {
                    Dialog.suspendedErrorForceLogoutDialog(context).show();
                } else if (responseGson.errorCode != 0 && responseGson.errorCode != 20 && responseGson.errorCode != 21 &&
                        responseGson.errorCode != 22 && responseGson.errorCode != 23) {
                    callback.failure(responseGson.errorMsg);
                } else {
                    callback.success(apiResponse);
                }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    @Override
    public String toString() {
        return "RequestAddViewedListing{" +
                "viewedMemberID=" + viewedMemberID +
                ", googleAddress='" + googleAddress + '\'' +
                '}';
    }
}
