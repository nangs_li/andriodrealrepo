package co.real.productionreal2.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import co.real.productionreal2.Constants;
import co.real.productionreal2.CoreData;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.createpost.BaseCreatePost;
import co.real.productionreal2.service.model.request.RequestListing;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;
import co.real.productionreal2.util.AppUtil;
import co.real.productionreal2.util.ImageUtil;

/**
 * Created by hohojo on 23/10/2015.
 */
public class PreviewPagerItemFragment extends Fragment {
    private static final String TAG = "PreviewPagerItemFragment";

    @InjectView(R.id.addressDetailTextView)
    TextView addressDetailTextView;
    @InjectView(R.id.propertyTypeImageView)
    ImageView propertyTypeImageView;
    @InjectView(R.id.propertyTypeTextView)
    TextView propertyTypeTextView;
    @InjectView(R.id.priceTextView)
    TextView priceTextView;
    @InjectView(R.id.sizeTextView)
    TextView sizeTextView;
    @InjectView(R.id.bedroomCountTextView)
    TextView bedroomCountTextView;
    @InjectView(R.id.bathroomCountTextView)
    TextView bathroomCountTextView;
    @InjectView(R.id.reason1ImageView)
    ImageView reason1ImageView;
    @InjectView(R.id.reason2ImageView)
    ImageView reason2ImageView;
    @InjectView(R.id.reason3ImageView)
    ImageView reason3ImageView;
    @InjectView(R.id.reason1TextView)
    TextView reason1TextView;
    @InjectView(R.id.reason2TextView)
    TextView reason2TextView;
    @InjectView(R.id.reason3TextView)
    TextView reason3TextView;
    @InjectView(R.id.reason2LinearLayout)
    LinearLayout reason2LinearLayout;
    @InjectView(R.id.reason3LinearLayout)
    LinearLayout reason3LinearLayout;
    ResponseLoginSocial.Content agentContent;

    private int langIndex;

    public static PreviewPagerItemFragment newInstance(Integer langIndex) {
        Bundle args = new Bundle();
        args.putInt(Constants.EXTRA_LANG_INDEX, langIndex);
        PreviewPagerItemFragment fragment = new PreviewPagerItemFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        agentContent = CoreData.getUserContent(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        langIndex = getArguments().getInt(Constants.EXTRA_LANG_INDEX, 0);
        View view = inflater.inflate(R.layout.preview_detail_item, container,
                false);
        ButterKnife.inject(this, view);
        initView();
        return view;
    }

    private boolean imagePathIsValid(String imagePath) {
        if (imagePath != null && !imagePath.isEmpty()) {
            return true;
        }
        return false;
    }

    private void initView() {
        BaseCreatePost baseCreatePost = BaseCreatePost.getInstance(getActivity());
        priceTextView.setText(baseCreatePost.getCreatePostRequest().currency + " " + String.valueOf(baseCreatePost.getCreatePostRequest().propertyPrice));
        sizeTextView.setText(String.valueOf(baseCreatePost.getCreatePostRequest().propertySize) + " " + baseCreatePost.getCreatePostRequest().sizeUnit);
        bedroomCountTextView.setText(String.valueOf(String.valueOf(baseCreatePost.getCreatePostRequest().bedroomCount)));
        bathroomCountTextView.setText(String.valueOf(String.valueOf(baseCreatePost.getCreatePostRequest().bathroomCount)));
        String propertyTypeImageName = AppUtil.getPropertyTypeImageName(baseCreatePost.getCreatePostRequest().propertyType,
                baseCreatePost.getCreatePostRequest().spaceType);
        String propertyTypeName = AppUtil.getPropertyTypeHashMap(getActivity(), agentContent.systemSetting.propertyTypeList).get(baseCreatePost.getCreatePostRequest().propertyType+"," + baseCreatePost.getCreatePostRequest().spaceType);
        propertyTypeImageView.setImageResource(getResources().getIdentifier(propertyTypeImageName, "drawable", getActivity().getPackageName()));
        propertyTypeTextView.setText(propertyTypeName);
        for (RequestListing.Reason reason : baseCreatePost.getCreatePostRequest().reasons) {
            if (reason.languageIndex == langIndex) {
                if (reason.reason2 != null && !reason.reason2.isEmpty()) {
                    reason2LinearLayout.setVisibility(View.VISIBLE);
                }
                if (reason.reason3 != null && !reason.reason3.isEmpty()) {
                    reason3LinearLayout.setVisibility(View.VISIBLE);
                }
                reason1TextView.setText(String.valueOf(reason.reason1));
                reason2TextView.setText(String.valueOf(reason.reason2));
                reason3TextView.setText(String.valueOf(reason.reason3));
            }
        }

        for (BaseCreatePost.ReasonsDespImage reasonsDespImage : baseCreatePost.getReasonsDespImageList()) {
            if (reasonsDespImage.langIndex == langIndex) {
                if (imagePathIsValid(reasonsDespImage.reason1ImagePath)) {
                    reason1ImageView.setImageBitmap(ImageUtil.decodeSampledBitmapFromResource(reasonsDespImage.reason1ImagePath));
                }
                if (imagePathIsValid(reasonsDespImage.reason2ImagePath)) {
                    reason2ImageView.setImageBitmap(ImageUtil.decodeSampledBitmapFromResource(reasonsDespImage.reason2ImagePath));
                }
                if (imagePathIsValid(reasonsDespImage.reason3ImagePath)) {
                    reason3ImageView.setImageBitmap(ImageUtil.decodeSampledBitmapFromResource(reasonsDespImage.reason3ImagePath));
                }
            }

        }

        if (BaseCreatePost.getInstance(getActivity()) == null)
            return;
        if (BaseCreatePost.getInstance(getActivity()).getCreatePostRequest() == null)
            return;
        if (BaseCreatePost.getInstance(getActivity()).getCreatePostRequest().googleAddressList == null)
            return;
        if (BaseCreatePost.getInstance(getActivity()).getCreatePostRequest().googleAddressList.size()==0)
            return;
        if (BaseCreatePost.getInstance(getActivity()).getCreatePostRequest().googleAddressList.get(0) == null)
            return;
        if (BaseCreatePost.getInstance(getActivity()).getCreatePostRequest().googleAddressList.get(0).address == null)
            return;

        String street = "";
        street = BaseCreatePost.getInstance(getActivity()).getCreatePostRequest().googleAddressList.get(0).address.formattedAddress;
        addressDetailTextView.setText(street);


    }
}
