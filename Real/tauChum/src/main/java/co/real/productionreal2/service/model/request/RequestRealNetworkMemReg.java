package co.real.productionreal2.service.model.request;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import co.real.productionreal2.Constants;
import co.real.productionreal2.R;
import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.model.InputRequest;
import co.real.productionreal2.service.RealNetworkService;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.response.ResponseRealNetworkMemReg;
import co.real.productionreal2.util.AppUtil;
import co.real.productionreal2.util.CryptLib;
import co.real.productionreal2.util.NetworkUtil;
import co.real.productionreal2.view.Dialog;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by hohojo on 14/12/2015.
 */
public class RequestRealNetworkMemReg {
    public RequestRealNetworkMemReg(String email,
                                    String pwMD5,
                                    String firstName,
                                    String lastName) {
        this.email = email;
        this.pwMD5 = pwMD5;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @SerializedName("Email")
    public String email;
    @SerializedName("PasswordMD5")
    public String pwMD5;
    @SerializedName("FirstName")
    public String firstName;
    @SerializedName("LastName")
    public String lastName;
    @SerializedName("Lang")
    public String lang;

    public void callRealNetworkMemRegApi(final Context context, final ApiCallback callback) {
        this.lang = AppUtil.getCurrentLangISO2Code(context);
        String json = new Gson().toJson(this);
        String encryptJson;
        if (Constants.enableEncryption) {
            encryptJson = CryptLib.encrypt(json);
            Log.w("RealNetworkMemRegApi", "json = " + json);
        } else {
            encryptJson = json;
        }
        final RealNetworkService apiService = new RealNetworkService(context);
        apiService.getCoffeeService().memReg(new InputRequest(encryptJson), new Callback<ApiResponse>() {
            @Override
            public void success(ApiResponse apiResponse, Response response) {
                String decryptJson;
                if (Constants.enableEncryption) {
                    decryptJson = CryptLib.decrypt(apiResponse.jsonContent);
                    Log.w("callRealNetworkLoginApi", "decryptJson = " + decryptJson);
                } else {
                    decryptJson = apiResponse.jsonContent;
                }
                ResponseRealNetworkMemReg responseGson = new Gson().fromJson(decryptJson, ResponseRealNetworkMemReg.class);
                if (responseGson.errorCode == 4) {
                    callback.failure(responseGson.errorMsg);
                } else if (responseGson.errorCode == 26) {
                    Dialog.suspendedErrorForceLogoutDialog(context).show();
                } else if (responseGson.errorCode != 0 && responseGson.errorCode != 20 && responseGson.errorCode != 21 &&
                        responseGson.errorCode != 22 && responseGson.errorCode != 23) {
                    callback.failure(context.getString(R.string.login__wrong_password));
                } else {
                    callback.success(apiResponse);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                callback.failure("");
                NetworkUtil.showNoNetworkMsg(context);
            }
        });

    }
}
