package co.real.productionreal2.service.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import co.real.productionreal2.service.model.Follower;
import co.real.productionreal2.service.model.ReMatchContact;

/**
 * Created by kelvinsun on 19/2/16.
 */
public class ResponseMatchContact extends ResponseBase {
    @SerializedName("Content")
    public Content content;

    public class Content {

        @SerializedName("Page")
        public int page;

        @SerializedName("TotalCount")
        public int totalCount;

        @SerializedName("RecordCount")
        public int recordCount;

        @SerializedName("MatchList")
        public List<ReMatchContact> reMatchContactList;


    }
}
