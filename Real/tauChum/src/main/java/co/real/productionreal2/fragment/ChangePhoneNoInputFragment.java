package co.real.productionreal2.fragment;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import co.real.productionreal2.CoreData;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.setting.ChangePhoneNumberActivity;
import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.callback.DialogCallback;
import co.real.productionreal2.common.LocalStorageHelper;
import co.real.productionreal2.model.SystemSetting;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.request.RequestPhoneNumberChange;
import co.real.productionreal2.service.model.response.ResponseChangePhoneNum;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;
import co.real.productionreal2.util.AppUtil;
import co.real.productionreal2.util.ValidUtil;
import co.real.productionreal2.view.Dialog;

/**
 * Created by kelvinsun on 15/6/16.
 */
public class ChangePhoneNoInputFragment extends Fragment {

    private static final String TAG = "ChangePhoneNoInputFrag";

    @InjectView(R.id.oldCountryCodeBtn)
    Button bnOldCountryCode;
    @InjectView(R.id.oldPhoneEditText)
    EditText etOldPhoneNo;
    @InjectView(R.id.newCountryCodeBtn)
    Button bnNewCountryCode;
    @InjectView(R.id.newPhoneEditText)
    EditText etNewPhoneNo;
    @InjectView(R.id.verifyNewPhoneNoBtn)
    Button bnDone;

    private ResponseLoginSocial.Content userContent;
    private AlertDialog mConfirmDialog;
    private final int DEFAULT_COUNTRY_CODE = 852;
    private int selectedPos = -1;
    private int countryCode;

    private ArrayList<Integer> countryCodeList = new ArrayList<>();
    private ArrayList<Integer> filterCountryCodeList;
    private ArrayAdapter<Integer> arrayAdapter;
    private AlertDialog.Builder dialogBuilder;

    RequestPhoneNumberChange requestPhoneNumberChange;

    /**
     * Create a new instance of CountingFragment, providing "num"
     * as an argument.
     */
    public static ChangePhoneNoInputFragment newInstance() {
        ChangePhoneNoInputFragment f = new ChangePhoneNoInputFragment();
        // Supply num input as an argument.
        Bundle args = new Bundle();
        f.setArguments(args);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_change_phone_no_input, container, false);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        userContent = CoreData.getUserContent(getActivity());
        initView();
        addListener();
    }

    private void addListener() {
        etOldPhoneNo.addTextChangedListener(textWatcher);
        etNewPhoneNo.addTextChangedListener(textWatcher);

        etNewPhoneNo.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    verifyNewPhoneNoClick(v);
                }
                return false;
            }
        });
    }

    TextWatcher textWatcher=new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
          updateButtonStatus();
        }
    };

    private void updateButtonStatus(){
        bnDone.setEnabled(etNewPhoneNo.getText().toString().length()>0 && etOldPhoneNo.getText().toString().length()>0);
    }

    public void initView() {
        String countryIso = AppUtil.getCurrentLangISO2Code(getActivity());
        Log.w(TAG, "countryIso = " + countryIso);
        countryCode = getCountryCode(countryIso);
        bnOldCountryCode.setText("+" + countryCode);
        bnNewCountryCode.setText("+" + countryCode);
    }

    private int getCountryCode(String countryIso) {
        int i = 0;
        for (SystemSetting.CountryCodeInfo countryCodeInfo : userContent.systemSetting.smsCountryCodeInfoList) {
            Log.w(TAG, "countryCodeInfo.countryCode = " + countryCodeInfo.countryShortName + " " + countryCodeInfo.countryCode + " " + countryIso);
            if (countryCodeInfo.countryShortName.equalsIgnoreCase(countryIso)) {
                return Integer.valueOf(countryCodeInfo.countryCode);
            }
            i++;
        }
        return DEFAULT_COUNTRY_CODE;
    }

    private ArrayList<Integer> filterDeputeCountryCodeList(ArrayList<Integer> countryCodeList) {
        Set<Integer> hs = new HashSet<>();
        hs.addAll(countryCodeList);
        countryCodeList.clear();
        countryCodeList.addAll(hs);
        Collections.sort(countryCodeList);
        return countryCodeList;
    }


    @OnClick(R.id.oldCountryCodeBtn)
    public void oldCountryCodeBtnClick(View view) {
        countryCodeList = new ArrayList<>();
        for (SystemSetting.CountryCodeInfo countryCodeInfo : userContent.systemSetting.smsCountryCodeInfoList) {
            countryCodeList.add(Integer.parseInt(countryCodeInfo.countryCode));
        }
        filterCountryCodeList = filterDeputeCountryCodeList(countryCodeList);
        arrayAdapter = new ArrayAdapter<>(
                getActivity(),
                android.R.layout.simple_list_item_checked,
                countryCodeList
        );

        dialogBuilder = new AlertDialog.Builder(getActivity())
                .setSingleChoiceItems(arrayAdapter, selectedPos, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        bnOldCountryCode.setText("+" + filterCountryCodeList.get(i));
                        selectedPos = i;
                        dialogInterface.dismiss();
                    }
                });

        //init value
        if (selectedPos == -1) {
            int i = 0;
            for (int index : filterCountryCodeList) {
                if (index == countryCode) {
                    selectedPos = i;
                }
                i++;
            }
        }

        mConfirmDialog = dialogBuilder.create();
        mConfirmDialog.show();
    }

    @OnClick(R.id.newCountryCodeBtn)
    public void newCountryCodeBtnClick(View view) {
        countryCodeList = new ArrayList<>();
        for (SystemSetting.CountryCodeInfo countryCodeInfo : userContent.systemSetting.smsCountryCodeInfoList) {
            countryCodeList.add(Integer.parseInt(countryCodeInfo.countryCode));
        }

        filterCountryCodeList = filterDeputeCountryCodeList(countryCodeList);
        arrayAdapter = new ArrayAdapter<>(
                getActivity(),
                android.R.layout.simple_list_item_checked,
                countryCodeList
        );

        dialogBuilder = new AlertDialog.Builder(getActivity())
                .setSingleChoiceItems(arrayAdapter, selectedPos, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        bnNewCountryCode.setText("+" + filterCountryCodeList.get(i));
                        selectedPos = i;
                        dialogInterface.dismiss();
                    }
                });

        //init value
        if (selectedPos == -1) {
            int i = 0;
            for (int index : filterCountryCodeList) {
                if (index == countryCode) {
                    selectedPos = i;
                }
                i++;
            }
        }

        mConfirmDialog = dialogBuilder.create();
        mConfirmDialog.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() instanceof ChangePhoneNumberActivity) {
            ((ChangePhoneNumberActivity) getActivity()).updateTitle(R.string.change_phone_no__title);
        }
        updateButtonStatus();
    }


    @OnClick(R.id.verifyNewPhoneNoBtn)
    public void verifyNewPhoneNoClick(View view) {


        final int memId = CoreData.getUserContent(getActivity()).memId;
        final String accessToken = LocalStorageHelper.getAccessToken(getActivity());
        final String countryCode = bnNewCountryCode.getText().toString();
        final String phoneNumber = etNewPhoneNo.getText().toString();
        final String countryCodeOld = bnOldCountryCode.getText().toString();
        final String phoneNumberOld = etOldPhoneNo.getText().toString();
        final String locale = AppUtil.getCurrentLangCode(getActivity());

        String dialogTitle = getString(R.string.error_message__number_err_title);

        if (phoneNumber.isEmpty() || phoneNumberOld.isEmpty()) {
            AppUtil.showAlertDialog(getActivity(), getString(R.string.error_message__empty_phone_num));
        } else if (phoneNumber.equals(phoneNumberOld) && countryCode.equals(countryCodeOld)) {
            AppUtil.showAlertDialog(getActivity(), getString(R.string.error_message__phone_num_duplicated));
        } else if (!ValidUtil.isValidPhoneNumber(countryCodeOld, phoneNumberOld)) {
            AppUtil.showAlertDialog(getActivity(), dialogTitle, String.format(getString(R.string.error_message__invalid_phone_num_format),countryCodeOld+" "+phoneNumberOld));
        } else if (!ValidUtil.isValidPhoneNumber(countryCode, phoneNumber)) {
            AppUtil.showAlertDialog(getActivity(), dialogTitle, String.format(getString(R.string.error_message__invalid_phone_num_format),countryCode+" "+phoneNumber));
        } else {
            String phoneNoWithCountryCode = countryCode + " " + phoneNumber;
            Dialog.confirmChangePhoneNoDialog(getActivity(), phoneNoWithCountryCode, new DialogCallback() {
                @Override
                public void yes() {
                    requestPhoneNumberChange = new RequestPhoneNumberChange(memId, accessToken,
                            countryCode, phoneNumber, countryCodeOld, phoneNumberOld, locale);
                    requestPhoneNumberChange.callPhoneNumberChangeApi(getActivity(), new ApiCallback() {
                        @Override
                        public void success(ApiResponse apiResponse) {
                            AppUtil.showToast(getActivity(), "success");
                            ///TODO: go to SMS verify fragment
                            //Testing only
                            ResponseChangePhoneNum responseChangePhoneNum = new Gson().fromJson(apiResponse.jsonContent, ResponseChangePhoneNum.class);
                            long regId = responseChangePhoneNum.content.phoneRegId;
                            if (getActivity() instanceof ChangePhoneNumberActivity) {
                                ChangePhoneNoSmsVerifyFragment changePhoneNoSmsVerifyFragment =
                                        ChangePhoneNoSmsVerifyFragment.newInstance(countryCodeOld,phoneNumberOld,countryCode, phoneNumber, regId);
                                ((ChangePhoneNumberActivity) getActivity()).pushFragment(changePhoneNoSmsVerifyFragment,false);
                            }
                        }

                        @Override
                        public void failure(String errorMsg) {
                           AppUtil.showAlertDialog(getActivity(),errorMsg);
                        }
                    });
                }
            }).show();
        }

    }
}
