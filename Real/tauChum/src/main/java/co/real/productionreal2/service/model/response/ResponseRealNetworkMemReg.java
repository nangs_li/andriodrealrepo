package co.real.productionreal2.service.model.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by hohojo on 15/12/2015.
 */
public class ResponseRealNetworkMemReg extends ResponseBase {
    @SerializedName("Content")
    public Content content;

    public class Content {
        @SerializedName("UserID")
        public String userId;
        @SerializedName("FirstName")
        public String firstName;
        @SerializedName("LastName")
        public String lastName;
        @SerializedName("Email")
        public String email;
        @SerializedName("AccessToken")
        public String accessToken;
    }
}
