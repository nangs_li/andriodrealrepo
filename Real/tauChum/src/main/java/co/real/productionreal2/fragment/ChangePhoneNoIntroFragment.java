package co.real.productionreal2.fragment;

import android.os.Bundle;
import android.app.Fragment;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.setting.ChangePhoneNumberActivity;


/**
 * Created by kelvinsun on 15/6/16.
 */
public class ChangePhoneNoIntroFragment extends Fragment{

    @InjectView(R.id.bn_next)
    Button bnNext;

    ChangePhoneNoInputFragment changePhoneNoInputFragment;

    private static final String TAG = "ChangePhoneNoIntroFrag";

    /**
     * Create a new instance of CountingFragment, providing "num"
     * as an argument.
     */
    public static ChangePhoneNoIntroFragment newInstance() {
        ChangePhoneNoIntroFragment f = new ChangePhoneNoIntroFragment();
        // Supply num input as an argument.
        Bundle args = new Bundle();
        f.setArguments(args);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_change_phone_no_intro, container, false);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initView();
    }

    public void initView() {

    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() instanceof ChangePhoneNumberActivity) {
            ((ChangePhoneNumberActivity)getActivity()).updateTitle(R.string.change_phone_no__title);
        }
    }

    @OnClick(R.id.bn_next)
    public void verifyNewPhoneNoClick(View view) {
        if (getActivity() instanceof ChangePhoneNumberActivity) {
            if (changePhoneNoInputFragment==null) {
                changePhoneNoInputFragment = ChangePhoneNoInputFragment.newInstance();
            }
            ((ChangePhoneNumberActivity) getActivity()).pushFragment(changePhoneNoInputFragment,true);
        }
    }

}
