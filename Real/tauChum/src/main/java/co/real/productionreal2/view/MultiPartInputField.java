package co.real.productionreal2.view;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import co.real.productionreal2.R;
import co.real.productionreal2.dao.account.DBLogInfo;
import co.real.productionreal2.daomanager.DatabaseManager;
import co.real.productionreal2.model.SystemSetting;
import co.real.productionreal2.util.AppUtil;
import co.real.productionreal2.util.FileUtil;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by kelvinsun on 24/12/15.
 */
public class MultiPartInputField extends LinearLayout implements View.OnClickListener {

    private TextView tvCurrency;
    private TextView tvSizeUnit;
    private TextView tvApproximation;
    private EditText etDigit;

    private Context context;

    //to define the appearance input field
    private FIELD_TYPE fieldType;

    public enum FIELD_TYPE {DEFAULT, PRICE, SIZE}

    //0=ABOVE , 1=AROUND, 2=BELOW
    private int currentApproximateValue = 0;
    private AlertDialog mConfirmDialog;

    private final List<String> currencyArrayList = FileUtil.getSystemSetting(getContext()).obtainCurrencyNameList();
    //declare language index
    List<String> metricNameList;
    List<Integer> metricIndexList;
    List<SystemSetting.Metric> metricList;

    private int selectedMetric = -1;
    private int selectedCurrency = 0;
    private int langIndex = 0;



    private DBLogInfo dbLogInfo;

    //    private String[] sizeArray = getResources().getStringArray(R.array.unit_array);
//    private String[] currencyArray = getResources().getStringArray(R.array.currency_array);
    private String[] approximationArray;

    public MultiPartInputField(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MultiPartInputField(Context context) {
        super(context);
        this.context = context;
    }

    //setup type
    public void initType(FIELD_TYPE fieldType, int selectedCurrency, int mSelectedMetric, boolean isShowApprox) {
        langIndex = AppUtil.getCurrentLangIndex(getContext());
        metricNameList = FileUtil.getSystemSetting(getContext()).obtainMetricNameList(langIndex);
        metricIndexList = FileUtil.getSystemSetting(getContext()).obtainMetricIndexList(langIndex);
        dbLogInfo = DatabaseManager.getInstance(getContext()).getLogInfo();
        //set the metric preference
        int defaultMetic = AppUtil.getMeticIndexFromPosition(metricIndexList, 0);
        if (mSelectedMetric == -1) {
            if (dbLogInfo != null)
                selectedMetric = dbLogInfo.getMetricIndex() == null ? defaultMetic : dbLogInfo.getMetricIndex();
        } else {
            selectedMetric = mSelectedMetric;
        }
        Log.d("mSelectedMetric", "mSelectedMetric: " + mSelectedMetric);
        this.fieldType=fieldType;
        //price input field
        if (fieldType == FIELD_TYPE.PRICE) {
            tvCurrency.setVisibility(View.VISIBLE);
            tvSizeUnit.setVisibility(View.GONE);
            tvCurrency.setText(currencyArrayList.get(selectedCurrency));
            //metric input field
        } else if (fieldType == FIELD_TYPE.SIZE) {
            tvCurrency.setVisibility(View.GONE);
            tvSizeUnit.setVisibility(View.VISIBLE);
            if (getSelectedMetric(selectedMetric) != null)
                tvSizeUnit.setText(getSelectedMetric(selectedMetric).nativeName);
        } else if (fieldType == FIELD_TYPE.DEFAULT) {
            //default
            tvCurrency.setVisibility(View.VISIBLE);
            tvSizeUnit.setVisibility(View.GONE);
            tvApproximation.setVisibility(View.GONE);
        }
        if (!isShowApprox) {
            tvApproximation.setVisibility(View.GONE);
        }
    }

    private SystemSetting.Metric getSelectedMetric(int index) {
        metricList = FileUtil.getSystemSetting(getContext()).obtainMetricList(langIndex);
        for (SystemSetting.Metric metric : metricList) {
            if (metric.index == index)
                return metric;
        }
        return metricList.get(0);
    }


    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        // find views
        tvCurrency = (TextView) findViewById(R.id.tv_currency);
        tvSizeUnit = (TextView) findViewById(R.id.tv_size_unit);
        tvApproximation = (TextView) findViewById(R.id.tv_approximation);
        etDigit = (EditText) findViewById(R.id.et_digit);


        etDigit.setText("0");
        etDigit.setSelection(etDigit.length());
        addListener();
    }

    private void addListener() {
        tvApproximation.setOnClickListener(this);
        tvSizeUnit.setOnClickListener(this);
        tvCurrency.setOnClickListener(this);

        etDigit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//                if (count==0){
//                    etDigit.setSelection(etDigit.length());
//                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                etDigit.removeTextChangedListener(this);

                if (s.toString() != "" && s.toString().length() > 1) {
                    try {
                        int inilen, endlen;
                        inilen = etDigit.getText().length();
                        int cp = etDigit.getSelectionStart();

                        if (fieldType == FIELD_TYPE.PRICE)
                            etDigit.setText(AppUtil.formatAmount(Double.parseDouble(s.toString().replaceAll(",", ""))));
                        else
                            try {
                                etDigit.setText(AppUtil.formatAmount(Integer.parseInt(s.toString().replaceAll(",", ""))));
                            } catch (NumberFormatException e) {
                                etDigit.setText(AppUtil.formatAmount(Integer.MAX_VALUE));
                            }

                        endlen = etDigit.getText().length();
                        int sel = (cp + (endlen - inilen));
//                        if (sel > 0 && sel <= etDigit.getText().length()) {
//                            Log.w("afterTextChanged", "sel = " + sel);
//                            etDigit.setSelection(sel);
//                        } else {
//                            // place cursor at the end?
                        Log.w("afterTextChanged", "place cursor at the end? " + etDigit.getText().length());
                        etDigit.setSelection(etDigit.getText().length());
//                        }
                    } catch (NumberFormatException nfe) {
                        // do nothing?
                    }
                } else {
                    //set ZERO for empty case
//                    etDigit.setText("0");
                }

                etDigit.addTextChangedListener(this);
            }
        });
    }


    private void openSizeUnitDialog() {
        Log.d("mSelectedMetric", "mSelectedMetric: before: " + selectedMetric);
        final ArrayAdapter<String> metricList = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_checked, metricNameList);
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext())
                .setSingleChoiceItems(metricList, AppUtil.getMeticPositionFromIndex(metricIndexList, selectedMetric), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        tvSizeUnit.setText(metricNameList.get(i));
//                        SearchFilterSetting.getInstance().setSizeUnitType(selectedMetric);
                        selectedMetric = AppUtil.getMeticIndexFromPosition(metricIndexList, i);
                        dialogInterface.dismiss();

                        Log.d("mSelectedMetric", "mSelectedMetric: after: " + selectedMetric);
                    }
                });

//        mConfirmDialog.setTitle(R.string.spoken_lang);
        mConfirmDialog = dialogBuilder.create();
        mConfirmDialog.show();
    }

    private void openCurrencyDialog() {

        final ArrayAdapter<String> currencyList = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_checked, currencyArrayList);
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext())
                .setSingleChoiceItems(currencyList, selectedCurrency, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        tvCurrency.setText(currencyArrayList.get(i));
                        selectedCurrency = i;
//                        SearchFilterSetting.getInstance().setCurrencyUnit(selectedCurrency);
                        dialogInterface.dismiss();
                    }
                });

        mConfirmDialog = dialogBuilder.create();
        mConfirmDialog.show();
    }


    @Override
    public void onClick(View v) {
        if (v == tvApproximation) {
            int listSize = 3;
            //move forwards
            int newIndex = (currentApproximateValue >= listSize - 1 ? 0 : currentApproximateValue + 1);
            String approximateVaule;
            switch (newIndex) {
                case 0:
                    approximateVaule = getContext().getResources().getString(R.string.search_filter__above);
                    break;
                case 1:
                    approximateVaule = getContext().getResources().getString(R.string.search_filter__around);
                    break;
                case 2:
                    approximateVaule = getContext().getResources().getString(R.string.search_filter__below);
                    break;
                default:
                    approximateVaule = getContext().getResources().getString(R.string.search_filter__above);
                    break;
            }

            tvApproximation.setText(approximateVaule);
            currentApproximateValue = newIndex;
        } else if (v == tvSizeUnit) {
            openSizeUnitDialog();
        } else if (v == tvCurrency) {
            openCurrencyDialog();
        }
    }


    public void reset() {
        etDigit.setText("0");
        etDigit.setSelection(etDigit.length());
        if (metricNameList != null)
            tvSizeUnit.setText(metricNameList.get(0));
        if (currencyArrayList != null)
            tvCurrency.setText(currencyArrayList.get(0));
        currentApproximateValue = 0;
        tvApproximation.setText(getContext().getResources().getString(R.string.search_filter__above));
    }


    public TextView getTvCurrency() {
        return tvCurrency;
    }

    public TextView getTvSizeUnit() {
        return tvSizeUnit;
    }

    public TextView getTvApproximation() {
        return tvApproximation;
    }

    public EditText getEtDigit() {
        return etDigit;
    }

    public void setValue(BigDecimal value) {
        etDigit.setText(value == null ? "0" : value.toString());
        etDigit.setSelection(etDigit.length());
    }

    public void setValue(int value) {
        etDigit.setText(value == 0 ? "0" : String.valueOf(value));
        etDigit.setSelection(etDigit.length());
    }

    public int getValue() {
        if (etDigit.getText().toString().length() > 0)
            try {
                return Integer.parseInt(etDigit.getText().toString().replace(",", ""));
            } catch (NumberFormatException e) {
                return Integer.MAX_VALUE;
            }

        else
            return 0;
    }

    public BigDecimal getValueDecimal() {
        Log.d("getValueDecimal", "getValueDecimal: " + etDigit.getText().toString().replace(",", "").trim());
        if (etDigit.getText().toString().length() > 1)
            return BigDecimal.valueOf(Double.parseDouble(etDigit.getText().toString().replace(",", "")));
        else
            return BigDecimal.ZERO;
    }


    public int getMinValue() {
        switch (currentApproximateValue) {
            //ABOVE
            case 0:
                return getValue();
            //AROUND
            case 1:
                return (int) (getValue() * 0.8);
            //BELOW
            case 2:
                return 0;
            default:
                return 0;
        }
    }

    public int getMaxValue() {
        switch (currentApproximateValue) {
            //ABOVE
            case 0:
                return Integer.MAX_VALUE;
            //AROUND
            case 1:
                return (int) (getValue() * 1.2);
            //BELOW
            case 2:
                return getValue();
            default:
                return Integer.MAX_VALUE;
        }
    }

    public BigDecimal getMaxValueDecimal() {
        switch (currentApproximateValue) {
            //ABOVE
            case 0:
                //Max value
                return new BigDecimal("79028162514264337593543950330");
            case 1:
                return (BigDecimal.valueOf(getValue() * 1.2));
            //BELOW
            case 2:
                return getValueDecimal();
            default:
                return BigDecimal.valueOf(Double.MAX_VALUE);
        }
    }


    public int getSelectedMetric() {
        return selectedMetric;
    }

    public void setSelectedMetric(int selectedMetric) {

        this.selectedMetric = selectedMetric;
    }

    public void setSizeUnitStr(String SizeUnitStr) {
        if (SizeUnitStr == "" || SizeUnitStr == null)
            return;
        tvSizeUnit.setText(SizeUnitStr);
    }

    public String getSizeUnitStr() {
        return tvSizeUnit.getText().toString();
    }

    public int getSelectedCurrency() {
        return selectedCurrency;
    }

    public void setSelectedCurrency(int selectedCurrency) {
        this.selectedCurrency = selectedCurrency;
    }

    public void setSelectedCurrencyValue(String selectedCurrencyStr) {
        if (selectedCurrencyStr == "" || selectedCurrencyStr == null)
            return;
        tvCurrency.setText(selectedCurrencyStr);
    }

    public String getSelectedCurrencyStr() {
        return tvCurrency.getText().toString();
    }

    public int getCurrentApproximateValue() {
        return currentApproximateValue;
    }

    public void setCurrentApproximateValue(int currentApproximateValue) {
        this.currentApproximateValue = currentApproximateValue;
    }

}
