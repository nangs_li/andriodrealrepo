package co.real.productionreal2.login;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.plus.People;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.google.android.gms.plus.model.people.PersonBuffer;
import co.real.productionreal2.service.AdminService;
import co.real.productionreal2.service.model.request.RequestSocialLogin;
import co.real.productionreal2.util.FileUtil;

import java.io.IOException;

/**
 * Created by hohojo on 24/8/2015.
 */
public class GoogleplusLoginController  extends BaseLoginController{
    /* Client used to interact with Google APIs. */
    private static final String TAG = "GoogleplusLoginController";
    private GoogleApiClient mGoogleApiClient;
    private static GoogleplusLoginController instance;
    private boolean mIsResolving = false;/* Is there a ConnectionResult resolution in progress? */
    private boolean mShouldResolve = false;/* Should we automatically resolve ConnectionResults when possible? */
    private boolean underUserRecoverableAuthException = false;

    private static final int STATE_DEFAULT = 0;
    private static final int STATE_SIGN_IN = 1;
    private static final int STATE_IN_PROGRESS = 2;
    private static final int RC_SIGN_IN = 0;

    protected static final int REQUEST_CODE_TOKEN_AUTH = 0;
    public static final String SOCIAL_BACKEND = "google-oauth2";

    private static final String SCOPES_LOGIN = Scopes.PLUS_LOGIN + " " + Scopes.PROFILE + " " + "email";
    private static final String ACTIVITIES_LOGIN = "http://schemas.google.com/AddActivity";

    // We use mSignInProgress to track whether user has clicked sign in.
    // mSignInProgress can be one of three values:
    //
    //       STATE_DEFAULT: The default state of the application before the user
    //                      has clicked 'sign in', or after they have clicked
    //                      'sign out'.  In this state we will not attempt to
    //                      resolve sign in errors and so will display our
    //                      Activity in a signed out state.
    //       STATE_SIGN_IN: This state indicates that the user has clicked 'sign
    //                      in', so resolve successive errors preventing sign in
    //                      until the user has successfully authorized an account
    //                      for our app.
    //   STATE_IN_PROGRESS: This state indicates that we have started an intent to
    //                      resolve an error, and so we should not start further
    //                      intents until the current intent completes.
    private int mSignInProgress;

    private LoginAsyncTask loginAsyncTask;


    public GoogleplusLoginController(Context context) {
        super(context);
        loginAsyncTask = new LoginAsyncTask();
        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(connectionCallbacks)
                .addOnConnectionFailedListener(onConnectionFailedListener)
                .addApi(Plus.API)
                .addScope(new Scope(Scopes.PROFILE))
                .addScope(Plus.SCOPE_PLUS_LOGIN)
                .build();
    }

    public static GoogleplusLoginController getInstance(Context context) {
        if (instance == null) {
            instance = new GoogleplusLoginController(context);
        }
        return instance;
    }

    public void connectGoogleApiClient() {
        mShouldResolve = true;
        mGoogleApiClient.connect();
    }

    public void disconnectGoogleApiClient() {
        mGoogleApiClient.disconnect();
    }


    private GoogleApiClient.ConnectionCallbacks connectionCallbacks = new GoogleApiClient.ConnectionCallbacks() {
        @Override
        public void onConnected(Bundle connectionHint) {
            Plus.PeopleApi.loadVisible(mGoogleApiClient, null).setResultCallback(resultResultCallback);
            Log.d(TAG, "onConnected:" + connectionHint);
            mShouldResolve = false;
            mSignInProgress = STATE_DEFAULT;
        }

        @Override
        public void onConnectionSuspended(int i) {
            connectGoogleApiClient();
        }
    };

    private GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener = new GoogleApiClient.OnConnectionFailedListener() {
        @Override
        public void onConnectionFailed(ConnectionResult connectionResult) {
            Log.d(TAG, "onConnectionFailed: " + connectionResult);
            if (!mIsResolving && mShouldResolve) {
                if (connectionResult.hasResolution()) {
                    try {
                        connectionResult.startResolutionForResult((Activity) context, RC_SIGN_IN);
                        mIsResolving = true;
                    } catch (IntentSender.SendIntentException e) {
                        Log.e(TAG, "Could not resolve ConnectionResult.", e);
                        mIsResolving = false;
                        mGoogleApiClient.connect();
                    }
                } else {
                    Log.w(TAG, "showErrorDialog");
                }
            } else {
                // Show the signed-out UI
                Log.w(TAG, "showSignedOutUI");
            }
        }
    };

    private ResultCallback<People.LoadPeopleResult> resultResultCallback = new ResultCallback<People.LoadPeopleResult>() {
        @Override
        public void onResult(People.LoadPeopleResult peopleData) {
            String displayName, personUrl;
            if (peopleData.getStatus().getStatusCode() == CommonStatusCodes.SERVICE_INVALID) {
                PersonBuffer personBuffer = peopleData.getPersonBuffer();
                try {
                    int count = personBuffer.getCount();
                    for (int i = 0; i < count; i++) {
                        Log.d(TAG, "Display name: " + personBuffer.get(i).getDisplayName());
                    }
                } finally {
                    personBuffer.close();
                }
            } else {
                Log.e(TAG, "Error requesting visible circles: " + peopleData.getStatus());
            }

            loginAsyncTask.execute();

        }
    };

    private class LoginAsyncTask extends AsyncTask<Void, Void, RequestSocialLogin>{
        @Override
        protected RequestSocialLogin doInBackground(Void... params) {
            String token = null;

            try {
                Bundle appActivities = new Bundle();
                appActivities.putString(
                        GoogleAuthUtil.KEY_REQUEST_VISIBLE_ACTIVITIES,
                        ACTIVITIES_LOGIN);

                //service account
                token = GoogleAuthUtil.getToken(context,
                        Plus.AccountApi.getAccountName(mGoogleApiClient),
                        "oauth2:" + SCOPES_LOGIN
                );
            } catch (IOException transientEx) {
                // Network or server error, try later
                Log.e(TAG, transientEx.toString());
            } catch (UserRecoverableAuthException e) {
                // Recover (with e.getIntent())
                Log.e(TAG, e.toString() + "  UserRecoverableAuthException");
                underUserRecoverableAuthException = true;
                Intent recover = e.getIntent();
                ((Activity) context).startActivityForResult(recover, REQUEST_CODE_TOKEN_AUTH);
            } catch (GoogleAuthException authEx) {
                // The call is not ever expected to succeed
                // assuming you have already verified that
                // Google Play services is installed.
                Log.e(TAG, authEx.toString());
            }


            if (token == null) {
                return null;
            }
            Log.i(TAG, "Access token retrieved:" + token);


            RequestSocialLogin socialRequest = null;
            try {
                String accountName = Plus.AccountApi.getAccountName(mGoogleApiClient);


                String accountID;
                accountID = GoogleAuthUtil.getAccountId(context, accountName);

                Person currentPerson = Plus.PeopleApi
                        .getCurrentPerson(mGoogleApiClient);
                socialRequest = new RequestSocialLogin(
                        AdminService.DEVICE_TYPE_ANDROID,
                        FileUtil.getDeviceId(context),
                        AdminService.SOCIAL_GOOGLE_AUTH2,
                        currentPerson.getDisplayName(),
                        accountID,
                        accountName,
                        currentPerson.getImage().getUrl(),
                        token);
                Log.i(TAG, "Google Plus accountName:  " + accountName);
                Log.i(TAG, "Google Plus accountID: " + accountID);
                Log.i(TAG, "Google Plus getDisplayName: " + currentPerson.getDisplayName());
            } catch (GoogleAuthException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return socialRequest;
        }

        @Override
        protected void onPostExecute(RequestSocialLogin socialRequest) {
            loginAsyncTask.cancel(false);

            if (socialRequest == null) {
//                Toast.makeText(context, "google login failed", Toast.LENGTH_LONG).show();
            } else {
//                        proceedButton.setVisibility(View.VISIBLE);
                socialRequest.callLoginSocialApi(context);
            }
        }

    };


}

//public class GoogleplusLoginController{}
