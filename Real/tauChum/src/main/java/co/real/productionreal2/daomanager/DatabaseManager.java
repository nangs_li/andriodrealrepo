package co.real.productionreal2.daomanager;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import com.google.gson.Gson;
import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.chat.model.QBDialog;
import com.quickblox.users.model.QBUser;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import co.real.productionreal2.Constants;
import co.real.productionreal2.chat.ChatDbViewController;
import co.real.productionreal2.chat.core.ChatService;
import co.real.productionreal2.common.LocalStorageHelper;
import co.real.productionreal2.dao.account.DBChatroomAgentProfiles;
import co.real.productionreal2.dao.account.DBChatroomAgentProfilesDao;
import co.real.productionreal2.dao.account.DBLogInfo;
import co.real.productionreal2.dao.account.DBLogInfoDao;
import co.real.productionreal2.dao.account.DBPhoneBook;
import co.real.productionreal2.dao.account.DBPhoneBookDao;
import co.real.productionreal2.dao.account.DBQBChatDialog;
import co.real.productionreal2.dao.account.DBQBChatDialogDao;
import co.real.productionreal2.dao.account.DBQBChatMessage;
import co.real.productionreal2.dao.account.DBQBChatMessageDao;
import co.real.productionreal2.dao.account.DBQBUser;
import co.real.productionreal2.dao.account.DBQBUserDao;
import co.real.productionreal2.dao.account.DaoMaster;
import co.real.productionreal2.dao.account.DaoSession;
import co.real.productionreal2.model.QBUserCustomData;
import co.real.productionreal2.service.model.AgentProfiles;
import co.real.productionreal2.service.model.ChatRoom;
import co.real.productionreal2.service.model.ChatRoomMember;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;
import co.real.productionreal2.util.CryptLib;
import co.real.productionreal2.util.QBUtil;
import co.real.productionreal2.util.TimeUtils;
import de.greenrobot.dao.async.AsyncOperation;
import de.greenrobot.dao.async.AsyncOperationListener;
import de.greenrobot.dao.async.AsyncSession;
import de.greenrobot.dao.query.Query;
import de.greenrobot.dao.query.QueryBuilder;
import de.greenrobot.dao.query.WhereCondition;

/**
 * @author Octa
 */
public class DatabaseManager implements AsyncOperationListener {

    // Variables
    private static final String TAG = DatabaseManager.class.getCanonicalName();

    private static DatabaseManager instance;
    private Context context;
    private DaoMaster.OpenHelper mHelper;
    private SQLiteDatabase database;
    private DaoMaster daoMaster;
    private DaoSession daoSession;
    private AsyncSession asyncSession;
    private List<AsyncOperation> completedOperations;
    private DBLogInfo logInfo = null;
    private Integer currentqbUserId = -1;

    // Override Methods

    @Override
    public void onAsyncOperationCompleted(AsyncOperation operation) {
        completedOperations.add(operation);
    }

    // Initialization Methods

    public DatabaseManager(final Context context) {
        this.context = context;
        mHelper = new DaoMaster.OpenHelper(this.context, "user-database", null) {
            @Override
            public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
                int currentVersion = oldVersion;

                if (currentVersion < 2) {
                    db.execSQL("ALTER TABLE DBQBCHAT_MESSAGE ADD IS_ENCRYPTED integer default 0");
                    currentVersion = 2;
                }

                if (currentVersion < 3) {
                    db.execSQL("ALTER TABLE DBQBCHAT_DIALOG ADD IS_ENCRYPTED integer default 0");
                    currentVersion = 3;
                }
                if (currentVersion < 5) {
                     String SQL_CREATE_ENTRIES =
                            "CREATE TABLE " + "'DBPHONE_BOOK' (" + //
                                    "'_id' INTEGER PRIMARY KEY AUTOINCREMENT ," + // 0: id
                                    "'PHONE_NUMBER' INTEGER UNIQUE ," + // 1: phoneNumber
                                    "'COUNTRY_CODE' TEXT," + // 2: countryCode
                                    "'NAME' TEXT," + // 3: name
                                    "'HAS_SENT' INTEGER," + // 4: hasSent
                                    "'HAS_CHECKED' INTEGER);"; // 5: hasChecked
                    db.execSQL(SQL_CREATE_ENTRIES);
                    currentVersion = 5;
                }
            }
        };
        completedOperations = new CopyOnWriteArrayList<AsyncOperation>();
        intitDBLogInfo();
    }

    public static DatabaseManager getInstance(Context context) {
        if (instance == null) {
            initDatabaseManager(context);
        }

        return instance;
    }

    public static void initDatabaseManager(Context context) {
        instance = new DatabaseManager(context);
    }

    // General Methods

    public void openReadableDb() throws SQLiteException {
        database = mHelper.getReadableDatabase();
        daoMaster = new DaoMaster(database);
        daoSession = daoMaster.newSession();
        asyncSession = daoSession.startAsyncSession();
        asyncSession.setListener(this);
    }

    public void openWritableDb() throws SQLiteException {
        database = mHelper.getWritableDatabase();
        daoMaster = new DaoMaster(database);
        daoSession = daoMaster.newSession();
        asyncSession = daoSession.startAsyncSession();
        asyncSession.setListener(this);
    }

    public synchronized void dropDatabase() {
        try {
            openWritableDb();
            DaoMaster.dropAllTables(database, true); // drops all tables
            mHelper.onCreate(database);              // creates the tables
            asyncSession.deleteAll(DBChatroomAgentProfiles.class);
//            asyncSession.deleteAll(DBDidDeliverMessage.class);
//            asyncSession.deleteAll(DBDidReadMessage.class);
            asyncSession.deleteAll(DBLogInfo.class);
            asyncSession.deleteAll(DBQBChatDialog.class);
            asyncSession.deleteAll(DBQBChatMessage.class);
            asyncSession.deleteAll(DBQBUser.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getDbPath() {
        openWritableDb();
        return database.getPath();
    }

    // Chat Dialog Methods - Read

    public synchronized DBQBChatDialog getDBQBChatDialogByDialogId(String dialogId) {
        if (dialogId != null) {
            List<DBQBChatDialog> dbqbChatDialogList = daoSession.getDBQBChatDialogDao().queryBuilder().where(DBQBChatDialogDao.Properties.DialogID.eq(dialogId)).list();
            if (dbqbChatDialogList.size() > 0) {
                return dbqbChatDialogList.get(0);
            }
        }
        return null;
    }

    public synchronized DBQBChatDialog insertQBChatDialog(DBQBChatDialog qBChatDialog) {
        try {
            if (qBChatDialog != null) {
                openWritableDb();
                DBQBChatDialogDao qbChatDialogDao = daoSession.getDBQBChatDialogDao();
                qbChatDialogDao.insert(qBChatDialog);
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return qBChatDialog;
    }

    public synchronized List<DBQBChatDialog> listQBChatDialog() {
        List<DBQBChatDialog> qBChatDialogs = null;
        try {
            openReadableDb();

            QueryBuilder qb = daoSession.getDBQBChatDialogDao().queryBuilder();
            qBChatDialogs = qb.orderDesc(DBQBChatDialogDao.Properties.LastMessageDate).list();
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return qBChatDialogs;
    }

    public synchronized DBQBChatDialog getQBChatDialogByDialogId(String dialogId) {
        DBQBChatDialog qBChatDialog = null;
        try {
            openReadableDb();
            DBQBChatDialogDao dao = daoSession.getDBQBChatDialogDao();
            qBChatDialog = dao.queryBuilder().where(DBQBChatDialogDao.Properties.DialogID.eq(dialogId)).list().get(0);
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return qBChatDialog;
    }

    public synchronized DBQBChatDialog getQBChatDialogByQbId(int qBId) {
        DBQBChatDialog qBChatDialog = null;
        try {
            openReadableDb();
            DBQBChatDialogDao dao = daoSession.getDBQBChatDialogDao();
            qBChatDialog = dao.queryBuilder().where(DBQBChatDialogDao.Properties.QbUserID.eq(qBId)).list().get(0);
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return qBChatDialog;
    }

    public String getDialogIdByMsgId(String chatMessageId) {
        DBQBChatMessage dbqbChatMessage = getQBChatMessageByMessageId(chatMessageId);
        return dbqbChatMessage.getChatDialogID();
    }

    public synchronized int getUnreadDialogCount() {
        String sql = "SELECT DISTINCT " + DBQBChatMessageDao.Properties.ChatDialogID.columnName +
                " FROM " + DBQBChatMessageDao.TABLENAME +
                " WHERE MESSAGE_TYPE = 'receive'" +
                " AND MESSAGE_CATEGORY != 'agentListing'" +
                " AND RECEIVE_STATUS != 'sentRead'";
        ArrayList<String> result = new ArrayList<>();
        Cursor c = daoSession.getDatabase().rawQuery(sql, null);
        if (c.moveToFirst()) {
            do {
                result.add(c.getString(0));
            } while (c.moveToNext());
        }
        c.close();
        if (result == null)
            return 0;
        return result.size();
    }

    public synchronized String getChatroomLastListingId(String dialogId) {
        DBQBChatMessage qBChatMessage = null;
        try {
            openReadableDb();
            DBQBChatMessageDao dao = daoSession.getDBQBChatMessageDao();

            List<DBQBChatMessage> dbqbChatMessageList = dao.queryBuilder()
                    .where(DBQBChatMessageDao.Properties.MessageCategory.eq(ChatDbViewController.MSG_TYPE_AGENTLISTING),
                            DBQBChatMessageDao.Properties.ChatDialogID.eq(dialogId))
                    .orderDesc(DBQBChatMessageDao.Properties.DateSent).limit(1).list();
            if (dbqbChatMessageList == null)
                return null;
            if (dbqbChatMessageList.size() < 1)
                return null;

            qBChatMessage = dbqbChatMessageList.get(0);
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (qBChatMessage == null)
            return null;
        return qBChatMessage.getBindingAgentListingId();

    }

    // Chat Dialog Methods - Write

    public synchronized void insertOrUpdateQBChatDialog(DBQBChatDialog dbqbChatDialog) {
        if (getDBQBChatDialogByDialogId("" + dbqbChatDialog.getDialogID()) != null) {
            updateQBChatDialog(dbqbChatDialog);
        } else {
            insertQBChatDialog(dbqbChatDialog);
        }

    }

    public synchronized void saveQBChatDialogsToDB(Collection<QBDialog> dialogs) {
        if (dialogs != null) {
            for (QBDialog dialog : dialogs) {
                try {
                    insertOrUpdateQBChatDialog(dialog);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public synchronized void insertOrUpdateQBChatDialog(QBDialog qbDialog) {
        DBQBChatDialog mDBQBChatDialog = getDBQBChatDialogByDialogId("" + qbDialog.getDialogId());

        if (mDBQBChatDialog != null) {
            if (mDBQBChatDialog.getLastMessageText() == null) {
                mDBQBChatDialog.setLastMessageText(qbDialog.getLastMessage());
            }
            if (mDBQBChatDialog.getLastMessageDate() == null) {
                mDBQBChatDialog.setLastMessageDate(qbDialog.getLastMessageDateSent() + "");
            }
            mDBQBChatDialog.setQBChatDialog(new Gson().toJson(qbDialog));
            updateQBChatDialog(mDBQBChatDialog);
        } else {
            mDBQBChatDialog = new DBQBChatDialog();
            if (qbDialog.getDialogId() != null) {
                mDBQBChatDialog.setDialogID(qbDialog.getDialogId());
            }
//            if(qbDialog.getUserId() != null) {
//                mDBQBChatDialog.setQbUserID(qbDialog.getUserId());
//            }
            if (qbDialog.getOccupants() != null) {
                for (Integer occupantsID : qbDialog.getOccupants()) {
                    if (!occupantsID.equals(currentqbUserId)) {
//                        mDBQBChatDialog.setMemberID(occupantsID);
                        mDBQBChatDialog.setQbUserID(occupantsID);
                        break;
                    }
                }
            }
            mDBQBChatDialog.setLastMessageText(qbDialog.getLastMessage());
            mDBQBChatDialog.setLastMessageDate(qbDialog.getLastMessageDateSent() + "");
            mDBQBChatDialog.setQBChatDialog(new Gson().toJson(qbDialog));
            mDBQBChatDialog.setExited(0);
            mDBQBChatDialog.setExitedOpponent(0);
            mDBQBChatDialog.setDeleted(0);
            mDBQBChatDialog.setDeletedOpponent(0);
            mDBQBChatDialog.setBlocked(0);
            mDBQBChatDialog.setBlockedOpponent(0);
            mDBQBChatDialog.setMuted(0);
            mDBQBChatDialog.setMutedOpponent(0);
            insertQBChatDialog(mDBQBChatDialog);
        }

    }

    public synchronized void updateQBChatDialog(DBQBChatDialog qBChatDialog) {
        try {
            if (qBChatDialog != null) {
                openWritableDb();
                daoSession.update(qBChatDialog);
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized void insertOrReplaceQBChatDialog(DBQBChatDialog qBChatDialog) {
        try {
            if (qBChatDialog != null) {
                openWritableDb();
                daoSession.insertOrReplace(qBChatDialog);
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized boolean deleteQBChatDialogById(Long qBChatDialogId) {
        try {
            openWritableDb();
            DBQBChatDialogDao dao = daoSession.getDBQBChatDialogDao();
            dao.deleteByKey(qBChatDialogId);
            daoSession.clear();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    void updateDBQBChatDialogByDBQBChatMessage(DBQBChatMessage qBChatMessage) {
        DBQBChatDialog mDBQBChatDialog = getDBQBChatDialogByDialogId("" + qBChatMessage.getChatDialogID());
        if (mDBQBChatDialog != null) {
            String lastMessage = qBChatMessage.getBody();
            mDBQBChatDialog.setLastMessageText(lastMessage);
            if (qBChatMessage.getIsEncrypted() != null && qBChatMessage.getIsEncrypted()) {
                mDBQBChatDialog.setIsEncrypted(true);
            } else {
                mDBQBChatDialog.setIsEncrypted(false);
            }
            mDBQBChatDialog.setLastMessageDate(qBChatMessage.getDateSent());
            updateQBChatDialog(mDBQBChatDialog);
        }
    }

    public synchronized void deleteAllQBChatDialog() {
        try {
            openWritableDb();
            DBQBChatDialogDao dao = daoSession.getDBQBChatDialogDao();
            dao.deleteAll();
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized void updateDialogLastMsgInfo(String qbDialogId) {
        DBQBChatMessage dbqbChatMessage = getLastestQBChatMessage(qbDialogId);
        DBQBChatDialog dbqbChatDialog = getQBChatDialogByDialogId(qbDialogId);
        if (dbqbChatMessage != null &&
                dbqbChatDialog != null) {
            String lastMessage = dbqbChatMessage.getBody();
            dbqbChatDialog.setLastMessageText(lastMessage);
            if (dbqbChatMessage.getIsEncrypted() != null && dbqbChatMessage.getIsEncrypted()) {
                dbqbChatDialog.setIsEncrypted(true);
            } else {
                dbqbChatDialog.setIsEncrypted(false);
            }
            dbqbChatDialog.setLastMessageDate(dbqbChatMessage.getDateSent());
            dbqbChatDialog.setLastMessageUserID(dbqbChatMessage.getSenderID());
            updateQBChatDialog(dbqbChatDialog);
        }
    }

    public synchronized void updateDialogNeedInit(String qbDialogId, boolean needInit) {
        DBQBChatDialog dbqbChatDialog = getQBChatDialogByDialogId(qbDialogId);
        dbqbChatDialog.setNeedInit(needInit);
        updateQBChatDialog(dbqbChatDialog);
    }

    // Agent Profile Methods - Read

    public synchronized DBChatroomAgentProfiles getChatroomAgentProfilesByUserId(Long userId) {
        DBChatroomAgentProfiles chatroomAgentProfiles = null;
        try {
            openReadableDb();
            DBChatroomAgentProfilesDao dao = daoSession.getDBChatroomAgentProfilesDao();
            List<DBChatroomAgentProfiles> DBChatroomAgentProfilesList = dao.queryBuilder().where(DBChatroomAgentProfilesDao.Properties.QBID.eq(userId)).list();
            if(DBChatroomAgentProfilesList != null && DBChatroomAgentProfilesList.size()>0){
                chatroomAgentProfiles = DBChatroomAgentProfilesList.get(0);
            }
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return chatroomAgentProfiles;
    }

    public synchronized DBChatroomAgentProfiles getChatroomAgentProfilesByMemId(int memId) {
        DBChatroomAgentProfiles chatroomAgentProfiles = null;
        try {
            openReadableDb();
            DBChatroomAgentProfilesDao dao = daoSession.getDBChatroomAgentProfilesDao();
            chatroomAgentProfiles = dao.queryBuilder().where(DBChatroomAgentProfilesDao.Properties.MemberID.eq(memId)).list().get(0);
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return chatroomAgentProfiles;
    }

    public synchronized Long haveRecordChatroomAgentProfiles(String memberId) {
        List<DBChatroomAgentProfiles> dbChatroomAgentProfilesList = daoSession.getDBChatroomAgentProfilesDao().queryBuilder().where(DBChatroomAgentProfilesDao.Properties.MemberID.eq(memberId)).list();
        if (dbChatroomAgentProfilesList.size() > 0) {
            return dbChatroomAgentProfilesList.get(0).getId();
        }
        return Long.valueOf(-1);
    }

    public synchronized String[] getExpireAgentProfileQBIDList() {
        try {
            String sql = "SELECT * " +
                    " FROM " + DBChatroomAgentProfilesDao.TABLENAME +
                    " WHERE " + DBChatroomAgentProfilesDao.Properties.LatUpdateTime.columnName + " < " + (System.currentTimeMillis() - (Constants.AGENT_PROFILE_EXPIRE_TIME)) +
                    " LIMIT " + Constants.AGENT_PROFILE_UPDATE_PER_TIME;
            Cursor cursor = daoSession.getDatabase().rawQuery(sql, null);
            List<DBChatroomAgentProfiles> agentProfileList = daoSession.getDBChatroomAgentProfilesDao().loadAllDeepFromCursor(cursor);
            String[] expireAgentProfileQBIDList = new String[agentProfileList.size()];
            for (int i = 0; i < agentProfileList.size(); i++) {
                expireAgentProfileQBIDList[i] = agentProfileList.get(i).getQBID() + "";
            }
            return expireAgentProfileQBIDList;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    // Agent Profile Methods - Write

    public synchronized void updateChatroomAgentProfilesIsFollowing(DBChatroomAgentProfiles chatroomAgentProfiles) {
        try {
            if (chatroomAgentProfiles != null) {
                openWritableDb();
//                daoSession.update(chatroomAgentProfiles);
                Query query = daoSession.getDBChatroomAgentProfilesDao().queryBuilder().where(DBChatroomAgentProfilesDao.Properties.MemberID.eq(chatroomAgentProfiles.getMemberID())).build();
                List<DBChatroomAgentProfiles> dbChatroomAgentProfilesList = query.list();
                Log.d("dbChatroomAgentProfiles", "@@@ updateChatroomAgentProfilesIsFollowing: " + dbChatroomAgentProfilesList.size() + " " + dbChatroomAgentProfilesList);
                if (dbChatroomAgentProfilesList != null) {
                    for (DBChatroomAgentProfiles p : dbChatroomAgentProfilesList) {
                        p.setIsFollowing(chatroomAgentProfiles.getIsFollowing());
                        p.update();
                    }
                }
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized void insertOrReplaceChatroomAgentProfiles(DBChatroomAgentProfiles chatroomAgentProfiles) {
        try {
            if (chatroomAgentProfiles != null) {
                openWritableDb();
                daoSession.insertOrReplace(chatroomAgentProfiles);
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized void insertOrUpdateChatroomAgentProfiles(DBChatroomAgentProfiles dbChatroomAgentProfiles) {
        if (haveRecordChatroomAgentProfiles("" + dbChatroomAgentProfiles.getMemberID()) != -1) {
            updateChatroomAgentProfilesIsFollowing(dbChatroomAgentProfiles);
            Log.w(TAG, "updateChatroomAgentProfilesIsFollowing dbChatroomAgentProfiles.getDialogID() = " + dbChatroomAgentProfiles.getMemberID());
        } else {
            insertOrReplaceChatroomAgentProfiles(dbChatroomAgentProfiles);
            Log.w(TAG, "insertChatroomAgentProfiles dbChatroomAgentProfiles.getDialogID() = " + dbChatroomAgentProfiles.getMemberID());
        }
    }

    public synchronized void saveAgentProfilesToDB(Collection<AgentProfiles> agentProfiles) {
        if (agentProfiles != null) {
            for (AgentProfiles agentProfile : agentProfiles) {
                try {
                    insertOrUpdateAgentProfile(agentProfile);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public synchronized void insertOrUpdateAgentProfile(AgentProfiles agentProfile) {
        DBChatroomAgentProfiles mDBChatroomAgentProfiles = getChatroomAgentProfilesByMemId(agentProfile.memberID);

        if (mDBChatroomAgentProfiles != null) {
            mDBChatroomAgentProfiles.setChatroomAgentProfiles(new Gson().toJson(agentProfile));
            mDBChatroomAgentProfiles.setFollowerCount(agentProfile.followerCount);
            mDBChatroomAgentProfiles.setFollowingCount(agentProfile.followingCount);
            mDBChatroomAgentProfiles.setLatUpdateTime(System.currentTimeMillis());
            mDBChatroomAgentProfiles.setIsAgent(agentProfile.agentListing != null);

            updateDBChatroomAgentProfiles(mDBChatroomAgentProfiles);
        } else {
            mDBChatroomAgentProfiles = new DBChatroomAgentProfiles();
            mDBChatroomAgentProfiles.setMemberID(agentProfile.memberID);
            mDBChatroomAgentProfiles.setChatroomAgentProfiles(new Gson().toJson(agentProfile));
            mDBChatroomAgentProfiles.setQBID(agentProfile.qBID);
            mDBChatroomAgentProfiles.setFollowerCount(agentProfile.followerCount);
            mDBChatroomAgentProfiles.setFollowingCount(agentProfile.followingCount);
            mDBChatroomAgentProfiles.setIsFollowing(agentProfile.isFollowing);
            mDBChatroomAgentProfiles.setLatUpdateTime(System.currentTimeMillis());
            mDBChatroomAgentProfiles.setIsAgent(agentProfile.agentListing != null);

            insertDBChatroomAgentProfiles(mDBChatroomAgentProfiles);
        }

    }

    public synchronized void insertDBChatroomAgentProfiles(DBChatroomAgentProfiles dbChatroomAgentProfiles) {
        try {
            if (dbChatroomAgentProfiles != null) {
                openWritableDb();
                DBChatroomAgentProfilesDao dao = daoSession.getDBChatroomAgentProfilesDao();
                dao.insert(dbChatroomAgentProfiles);
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized void updateDBChatroomAgentProfiles(DBChatroomAgentProfiles dbChatroomAgentProfiles) {
        try {
            if (dbChatroomAgentProfiles != null) {
                openWritableDb();
                DBChatroomAgentProfilesDao dao = daoSession.getDBChatroomAgentProfilesDao();
                dao.update(dbChatroomAgentProfiles);
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized void deleteAllChatroomAgentProfiles() {
        try {
            openWritableDb();
            DBChatroomAgentProfilesDao dao = daoSession.getDBChatroomAgentProfilesDao();
            dao.deleteAll();
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Log Methods - Read

    public synchronized DBLogInfo getLogInfo() {
        if (logInfo == null) {
            intitDBLogInfo();
        }
        return logInfo;
    }

    public synchronized void intitDBLogInfo() {
        logInfo = null;
        try {
            openReadableDb();
            DBLogInfoDao dao = daoSession.getDBLogInfoDao();
            logInfo = dao.loadAll().get(0);
            currentqbUserId = logInfo.getQbUserId();
            daoSession.clear();
        } catch (Exception e) {
            if(Constants.isDevelopment) {
                e.printStackTrace();
            }
        }
    }

    public boolean hasLogInfo() {
        try {
            openReadableDb();
            DBLogInfoDao dao = daoSession.getDBLogInfoDao();
            if (dao.loadAll().size() > 0) {
                daoSession.clear();
                return true;
//                logInfo = dao.loadAll().get(0);
            }
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public synchronized boolean isUserEnableReadMsgTick() {
        if (getLogInfo().getEnableReadMsg() != null) {
            if (getLogInfo().getEnableReadMsg() == false)
                return false;
        }
        return true;
    }

    // Log Methods - Write

    public synchronized void updateLogInfo(DBLogInfo logInfo) {
        try {
            if (logInfo != null) {
                openWritableDb();
                daoSession.update(logInfo);
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized void insertOrReplaceLogInfo(DBLogInfo logInfo) {
        try {
            if (logInfo != null) {
                openWritableDb();
                daoSession.insertOrReplace(logInfo);
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized boolean deleteLogInfoById(Long logInfoId) {
        try {
            openWritableDb();
            DBLogInfoDao dao = daoSession.getDBLogInfoDao();
            dao.deleteByKey(logInfoId);
            daoSession.clear();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public synchronized void deleteAllLogInfo() {
        try {
//            ApplicationSingleton.getInstance().showErrorToast("Deleting All Login Info:\n" + Log.getStackTraceString(new Exception()));
            openWritableDb();
            DBLogInfoDao dao = daoSession.getDBLogInfoDao();
            dao.deleteAll();
            daoSession.clear();
            logInfo = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Chat Room Settings Methods - Read

    public synchronized boolean isSetEnableReadMsgTick(QBUser opponentQBUser) {
        if (opponentQBUser.getCustomData() != null) {
            Log.w(TAG, "isSetEnableReadMsgTick opponentQBUser.getCustomData() = " + opponentQBUser.getCustomData());
            QBUserCustomData qbUserCustomData = new Gson().fromJson(opponentQBUser.getCustomData(), QBUserCustomData.class);
            Log.w(TAG, "isSetEnableReadMsgTick chatRoomShowReadReceipts = " + qbUserCustomData.chatRoomShowReadReceipts);
            if (!qbUserCustomData.chatRoomShowReadReceipts) {
                return false;
            }
            if (getLogInfo().getEnableReadMsg() != null) {
                if (getLogInfo().getEnableReadMsg() == false)
                    return false;
            }
        }
        return true;
    }

    // Chat Room Settings Methods - Write

    public synchronized void saveQBChatRoomsSettingToDB(Collection<ChatRoom> chatRooms) {
        if (chatRooms != null) {
            for (ChatRoom chatRoom : chatRooms) {
                try {
                    insertOrUpdateChatRoomsSetting(chatRoom);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public synchronized void insertOrUpdateChatRoomsSetting(ChatRoom chatRoom) {
        DBQBChatDialog mDBQBChatDialog = getDBQBChatDialogByDialogId("" + chatRoom.dialogId);

        if (mDBQBChatDialog != null) {
            if (chatRoom.chatRoomMemList != null) {
                for (ChatRoomMember chatRoomMem : chatRoom.chatRoomMemList) {
                    if (chatRoomMem.qbId.equals(String.valueOf(currentqbUserId))) {
                        mDBQBChatDialog.setExited(chatRoomMem.exited);
                        mDBQBChatDialog.setDeleted(chatRoomMem.deleted);
                        mDBQBChatDialog.setBlocked(chatRoomMem.blocked);
                        mDBQBChatDialog.setMuted(chatRoomMem.muted);
                    } else {
                        mDBQBChatDialog.setExitedOpponent(chatRoomMem.exited);
                        mDBQBChatDialog.setDeletedOpponent(chatRoomMem.deleted);
                        mDBQBChatDialog.setBlockedOpponent(chatRoomMem.blocked);
                        mDBQBChatDialog.setMutedOpponent(chatRoomMem.muted);

                        mDBQBChatDialog.setQbUserID(Long.parseLong(chatRoomMem.qbId));
                    }
                }
            }

            updateQBChatDialog(mDBQBChatDialog);
        } else {
            mDBQBChatDialog = new DBQBChatDialog();
            if (chatRoom.dialogId != null) {
                mDBQBChatDialog.setDialogID(chatRoom.dialogId);
            }
            if (chatRoom.dialogId != null) {
                mDBQBChatDialog.setDialogID(chatRoom.dialogId);
            }
            if (chatRoom.chatRoomMemList != null) {
                for (ChatRoomMember chatRoomMem : chatRoom.chatRoomMemList) {
                    if (chatRoomMem.qbId.equals(String.valueOf(currentqbUserId))) {
                        mDBQBChatDialog.setExited(chatRoomMem.exited);
                        mDBQBChatDialog.setDeleted(chatRoomMem.deleted);
                        mDBQBChatDialog.setBlocked(chatRoomMem.blocked);
                        mDBQBChatDialog.setMuted(chatRoomMem.muted);
                    } else {
                        mDBQBChatDialog.setExitedOpponent(chatRoomMem.exited);
                        mDBQBChatDialog.setDeletedOpponent(chatRoomMem.deleted);
                        mDBQBChatDialog.setBlockedOpponent(chatRoomMem.blocked);
                        mDBQBChatDialog.setMutedOpponent(chatRoomMem.muted);

                        mDBQBChatDialog.setQbUserID(Long.parseLong(chatRoomMem.qbId));
                    }
                }


            }

            insertQBChatDialog(mDBQBChatDialog);
        }

    }

    // Messages Methods - Read

    public synchronized List<DBQBChatMessage> listQbMessageByDialogId(String dialogId) {
        List qBChatMessagelist = daoSession.getDBQBChatMessageDao().queryBuilder()
                .where(DBQBChatMessageDao.Properties.ChatDialogID.eq(dialogId))
                .orderAsc(DBQBChatMessageDao.Properties.Id)
                .list();
        return qBChatMessagelist;
    }

    public synchronized List<DBQBChatMessage> listPhotoQbMessageByDialogId(String dialogId) {
        List qBChatMessagelist = daoSession.getDBQBChatMessageDao().queryBuilder()
                .where(DBQBChatMessageDao.Properties.ChatDialogID.eq(dialogId),
                        DBQBChatMessageDao.Properties.MessageCategory.eq(ChatDbViewController.MSG_TYPE_IMAGE)
                )
                .orderAsc(DBQBChatMessageDao.Properties.Id)
                .list();

        return qBChatMessagelist;
    }

    public synchronized List<DBQBChatMessage> listUnreadFirstMsg(String dialogId) {
        List qBChatMessagelist = daoSession.getDBQBChatMessageDao().queryBuilder()
                .where(DBQBChatMessageDao.Properties.ChatDialogID.eq(dialogId),
                        DBQBChatMessageDao.Properties.IsUnreadFirstMessage.eq(true))
                .orderAsc(DBQBChatMessageDao.Properties.Id)
                .list();
        return qBChatMessagelist;
    }

    private DBQBChatMessage getPreviousListingMsg(List<DBQBChatMessage> dbqbListingMessageList, int currentListingId) {
        Log.v("", "edwincc getPreviousListingMsg dbqbListingMessageList.size() " + dbqbListingMessageList.size());
        for (int i = 0; i < dbqbListingMessageList.size(); i++) {
            if (dbqbListingMessageList.get(i).getBindingAgentListingId().equalsIgnoreCase("" + currentListingId)) {
                if (i + 1 >= dbqbListingMessageList.size())
                    return null;
                else
                    return dbqbListingMessageList.get(i + 1);
            }

        }
        return null;
    }

    public synchronized DBQBChatMessage getPreviousListingMsg(String dialogId, int currentListingId, long currentListingDatabaseId) {
        List qBChatMessagelist;

        if (currentListingDatabaseId != -1l) {
            qBChatMessagelist = daoSession.getDBQBChatMessageDao().queryBuilder()
                    .where(DBQBChatMessageDao.Properties.ChatDialogID.eq(dialogId),
                            DBQBChatMessageDao.Properties.Id.le(currentListingDatabaseId),
                            DBQBChatMessageDao.Properties.MessageCategory.eq(ChatDbViewController.MSG_TYPE_AGENTLISTING))
                    .orderDesc(DBQBChatMessageDao.Properties.Id)
                    .list();
        } else {
            qBChatMessagelist = daoSession.getDBQBChatMessageDao().queryBuilder()
                    .where(DBQBChatMessageDao.Properties.ChatDialogID.eq(dialogId),
                            DBQBChatMessageDao.Properties.MessageCategory.eq(ChatDbViewController.MSG_TYPE_AGENTLISTING))
                    .orderDesc(DBQBChatMessageDao.Properties.Id)
                    .list();
        }

        if (qBChatMessagelist == null)
            return null;
        Log.v("", "edwincc DBQBChatMessage getPreviousListingMsg dialogId " + dialogId + " " + " qBChatMessagelist.size() " + qBChatMessagelist.size());
        if (qBChatMessagelist.size() <= 0)
            return null;

        DBQBChatMessage previousListingMsg = getPreviousListingMsg(qBChatMessagelist, currentListingId);
        return previousListingMsg;
    }

    public synchronized List<DBQBChatMessage> listCurrentQbMessages(String dialogId, int currentAgentListingId, long currentListingDatabaseId) {
        Log.v("", "edwincc DBQBChatMessage listCurrentQbMessages dialogId " + dialogId + " currentAgentListingId " + currentAgentListingId + " currentListingDatabaseId " + currentListingDatabaseId);
//        String SQL_DISTINCT_ENAME = "SELECT * FROM "+DBQBChatMessageDao.TABLENAME + " WHERE "+DBQBChatMessageDao.Properties.ChatDialogID.columnName+" = "
        if (currentAgentListingId != -1) {
            DBQBChatMessage dbqbListingMessage;
            if (currentListingDatabaseId != -1l) {
                dbqbListingMessage = daoSession.getDBQBChatMessageDao().queryBuilder()
                        .where(DBQBChatMessageDao.Properties.ChatDialogID.eq(dialogId),
                                DBQBChatMessageDao.Properties.MessageCategory.eq(ChatDbViewController.MSG_TYPE_AGENTLISTING),
                                DBQBChatMessageDao.Properties.Id.le(currentListingDatabaseId),
                                DBQBChatMessageDao.Properties.BindingAgentListingId.eq(currentAgentListingId))
                        .orderDesc(DBQBChatMessageDao.Properties.Id)
                        .list().get(0);
            } else {
                dbqbListingMessage = daoSession.getDBQBChatMessageDao().queryBuilder()
                        .where(DBQBChatMessageDao.Properties.ChatDialogID.eq(dialogId),
                                DBQBChatMessageDao.Properties.MessageCategory.eq(ChatDbViewController.MSG_TYPE_AGENTLISTING),
                                DBQBChatMessageDao.Properties.BindingAgentListingId.eq(currentAgentListingId))
                        .orderDesc(DBQBChatMessageDao.Properties.Id)
                        .list().get(0);
            }
//            List qBChatMessagelist = daoSession.getDBQBChatMessageDao().queryBuilder()
//                    .where(DBQBChatMessageDao.Properties.ChatDialogID.eq(dialogId),
//                            DBQBChatMessageDao.Properties.Id.ge(dbqbListingMessage.getId()),
//                            DBQBChatMessageDao.Properties.IsDeleted.notEq(1))
//                    .orderDesc(DBQBChatMessageDao.Properties.Id)
//                    .list();
            Log.v("", "edwincc DBQBChatMessage listCurrentQbMessages dbqbListingMessage " + dbqbListingMessage.getId() + " currentAgentListingId " + currentAgentListingId);
            QueryBuilder qb = daoSession.getDBQBChatMessageDao().queryBuilder();
            qb.where(DBQBChatMessageDao.Properties.ChatDialogID.eq(dialogId),
                    qb.or(DBQBChatMessageDao.Properties.IsDeleted.isNull(),
                            DBQBChatMessageDao.Properties.IsDeleted.eq(false)),
                    DBQBChatMessageDao.Properties.Id.ge(dbqbListingMessage.getId()));
            List qBChatMessagelist = qb.orderAsc(DBQBChatMessageDao.Properties.Id).list();
            return qBChatMessagelist;
        } else {
            QueryBuilder qb = daoSession.getDBQBChatMessageDao().queryBuilder();
            qb.where(DBQBChatMessageDao.Properties.ChatDialogID.eq(dialogId),
                    qb.or(DBQBChatMessageDao.Properties.IsDeleted.isNull(), DBQBChatMessageDao.Properties.IsDeleted.eq(false)));
            List qBChatMessagelist = qb.orderAsc(DBQBChatMessageDao.Properties.Id).list();

            return qBChatMessagelist;
        }
    }

    public synchronized Long haveRecordQBChatMessage(String msgId) {
        List<DBQBChatMessage> qBChatMessagelist = daoSession.getDBQBChatMessageDao().queryBuilder().where(DBQBChatMessageDao.Properties.MessageID.eq(msgId)).list();
        if (qBChatMessagelist.size() > 0) {
            return qBChatMessagelist.get(0).getId();
        }
        return Long.valueOf(-1);
    }

    public synchronized List<DBQBChatMessage> listUnReadQBChatMessageByDialogId(String dialogId) {
        List qBChatMessagelist = daoSession.getDBQBChatMessageDao().queryBuilder()
                .where(DBQBChatMessageDao.Properties.ChatDialogID.eq(dialogId),
                        DBQBChatMessageDao.Properties.MessageType.eq(ChatDbViewController.SEND),
                        DBQBChatMessageDao.Properties.SendStatus.eq(ChatDbViewController.SENT_USER)
                )
                .orderAsc(DBQBChatMessageDao.Properties.DateSent)
                .list();
        return qBChatMessagelist;
    }

    public synchronized List<DBQBChatMessage> listUnsentReadQBChatMessageByDialogId(String dialogId) {
//        WhereCondition whereCondition = daoSession.getDBQBChatMessageDao().queryBuilder().or(
//                DBQBChatMessageDao.Properties.ReceiveStatus.notEq(ChatDbViewController.SENT_READ),
//                DBQBChatMessageDao.Properties.ReceiveStatus.isNull());

        List qBChatMessagelist = daoSession.getDBQBChatMessageDao().queryBuilder()
                .where(DBQBChatMessageDao.Properties.ChatDialogID.eq(dialogId),
                        DBQBChatMessageDao.Properties.MessageCategory.notEq(ChatDbViewController.MSG_TYPE_AGENTLISTING),
                        DBQBChatMessageDao.Properties.MessageType.eq(ChatDbViewController.RECEIVE),
                        DBQBChatMessageDao.Properties.ReceiveStatus.eq(ChatDbViewController.RECIEVED)
                )
                .orderDesc(DBQBChatMessageDao.Properties.DateSent)
                .list();
        return qBChatMessagelist;
    }

    public synchronized List<DBQBChatMessage> listUnsentReadQBChatMessageByDialogId(String dialogId, String dateSent) {
        List qBChatMessagelist = daoSession.getDBQBChatMessageDao().queryBuilder()
//                .where(
//                        DBQBChatMessageDao.Properties.MessageType.eq(ChatDbViewController.RECEIVE),
//                        DBQBChatMessageDao.Properties.ReceiveStatus.notEq(ChatDbViewController.SENT_READ),
//                        DBQBChatMessageDao.Properties.ChatDialogID.eq(dialogId),
//                        DBQBChatMessageDao.Properties.DateSent.lt(dateSent))
                .where(
                        DBQBChatMessageDao.Properties.MessageType.eq(ChatDbViewController.RECEIVE),
                        DBQBChatMessageDao.Properties.ChatDialogID.eq(dialogId),
                        DBQBChatMessageDao.Properties.DateSent.lt(dateSent))
                .orderAsc(DBQBChatMessageDao.Properties.DateSent)
                .limit(5)
                .list();
        return qBChatMessagelist;
    }

    public synchronized List<DBQBChatMessage> listUnSentUserQBChatMessageByDialogId(String dialogId) {
        WhereCondition whereCondition = daoSession.getDBQBChatMessageDao().queryBuilder().or(
                DBQBChatMessageDao.Properties.SendStatus.eq(ChatDbViewController.NEW),
                DBQBChatMessageDao.Properties.SendStatus.isNull());
        List qBChatMessagelist = daoSession.getDBQBChatMessageDao().queryBuilder()
                .where(whereCondition,
                        DBQBChatMessageDao.Properties.MessageType.eq(ChatDbViewController.SEND),
                        DBQBChatMessageDao.Properties.ChatDialogID.eq(dialogId))
                .orderAsc(DBQBChatMessageDao.Properties.DateSent)
                .list();
        return qBChatMessagelist;
    }

    public synchronized DBQBChatMessage getQBChatMessageById(Long qBChatMessageId) {
        DBQBChatMessage qBChatMessage = null;
        try {
            openReadableDb();
            DBQBChatMessageDao dao = daoSession.getDBQBChatMessageDao();
            qBChatMessage = dao.load(qBChatMessageId);
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return qBChatMessage;
    }

    public synchronized DBQBChatMessage getQBChatMessageByMessageId(String messageId) {
        DBQBChatMessage qBChatMessage = null;
        try {
            openReadableDb();
            DBQBChatMessageDao dao = daoSession.getDBQBChatMessageDao();
            List<DBQBChatMessage> qBChatMessageList = dao.queryBuilder().where(DBQBChatMessageDao.Properties.MessageID.eq(messageId)).list();
            if (qBChatMessageList.size() > 0) {
                qBChatMessage = qBChatMessageList.get(0);
            }
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return qBChatMessage;
    }

    public synchronized DBQBChatMessage getQBChatMessageByPhotoName(String photoName) {
        DBQBChatMessage qBChatMessage = null;
        try {
            openReadableDb();
            DBQBChatMessageDao dao = daoSession.getDBQBChatMessageDao();
            qBChatMessage = dao.queryBuilder().where(DBQBChatMessageDao.Properties.PhotoName.eq(photoName)).list().get(0);
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return qBChatMessage;
    }

    public synchronized DBQBChatMessage getLastestQBChatMessage(String dialogId) {
        DBQBChatMessage qBChatMessage = null;
        try {
            openReadableDb();
            DBQBChatMessageDao dao = daoSession.getDBQBChatMessageDao();
//            if (dao.queryBuilder().orderDesc(DBQBChatMessageDao.Properties.DateSent).limit(1).list() != null) {
//                qBChatMessage = dao.queryBuilder()
//                        .where(DBQBChatMessageDao.Properties.DateSent.eq(dialogId))
//                        .orderDesc(DBQBChatMessageDao.Properties.DateSent).limit(1).list().get(0);
//            }

            qBChatMessage = dao.queryBuilder()
                    .where(DBQBChatMessageDao.Properties.ChatDialogID.eq(dialogId))
                    .orderDesc(DBQBChatMessageDao.Properties.MessageID).limit(1).list().get(0);
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return qBChatMessage;

    }

    public synchronized DBQBChatMessage getLastestOpponentQBChatMessage(String dialogId, int opponentId) {
        DBQBChatMessage qBChatMessage = null;
        try {
            openReadableDb();
            DBQBChatMessageDao dao = daoSession.getDBQBChatMessageDao();
            qBChatMessage = dao.queryBuilder()
                    .where(DBQBChatMessageDao.Properties.ChatDialogID.eq(dialogId),
                            DBQBChatMessageDao.Properties.SenderID.eq(opponentId))
                    .orderDesc(DBQBChatMessageDao.Properties.MessageID).limit(1).list().get(0);
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return qBChatMessage;

    }

    // Messages Methods - Write

    // For new record
    public DBQBChatMessage chatMessageToDbQbChatMessage(QBChatMessage chatMessage, Gson gson) {
        DBQBChatMessage dbqbChatMessage = new DBQBChatMessage();
        return chatMessageToDbQbChatMessage(dbqbChatMessage, chatMessage, gson);
    }

    // For update record
    public DBQBChatMessage chatMessageToDbQbChatMessage(DBQBChatMessage dbqbChatMessage, QBChatMessage chatMessage, Gson gson) {
        dbqbChatMessage.setMessageID(chatMessage.getId());
        dbqbChatMessage.setMemberID(getLogInfo().getMemberID());
        dbqbChatMessage.setBody(chatMessage.getBody());
        dbqbChatMessage.setChatDialogID(chatMessage.getDialogId());
        String currentDateTime = TimeUtils.getCurrenDateTimeString();
        Long qbChatMsgDate = chatMessage.getDateSent();
        if (qbChatMsgDate == 0 &&
                chatMessage.getProperty(ChatDbViewController.PROPERTY_DATE_SENT) != null) {
            qbChatMsgDate = Double.valueOf(chatMessage.getProperty(ChatDbViewController.PROPERTY_DATE_SENT).toString()).longValue();
        }

        if (qbChatMsgDate == 0 && (dbqbChatMessage.getDateSent() == null || dbqbChatMessage.getDateSent() == "0")) {
            Long dateSent;
            if (getLastestQBChatMessage(chatMessage.getDialogId()) != null) {
                Log.w(TAG, "getDataSent = " + getLastestQBChatMessage(chatMessage.getDialogId()).getDateSent());
                dateSent = Long.parseLong(getLastestQBChatMessage(chatMessage.getDialogId()).getDateSent()) + 1;
            } else {
                Log.w(TAG, "currentDateTime = " + currentDateTime);
                dateSent = Long.parseLong(currentDateTime);
            }
            dbqbChatMessage.setDateSent("" + dateSent);

        } else {
            dbqbChatMessage.setDateSent("" + qbChatMsgDate);
        }
        dbqbChatMessage.setInsertDBDateTime(currentDateTime);
        dbqbChatMessage.setQBChatMessage(gson.toJson(chatMessage));
        dbqbChatMessage.setSenderID(chatMessage.getSenderId());
        dbqbChatMessage.setRecipientID(chatMessage.getRecipientId());

        boolean isSendMsg = chatMessage.getSenderId() == null
                || chatMessage.getSenderId().equals(getLogInfo().getQbUserId());


        if (isSendMsg) {
            dbqbChatMessage.setMessageType(ChatDbViewController.SEND);
            if (dbqbChatMessage.getSendStatus() != null)
                dbqbChatMessage.setSendStatus(dbqbChatMessage.getSendStatus());
            else {
                //1st time
                if (!chatMessage.isRead())
                    dbqbChatMessage.setSendStatus(ChatDbViewController.SENT);
                else
                    dbqbChatMessage.setSendStatus(ChatDbViewController.READ);
            }

        } else {
            dbqbChatMessage.setMessageType(ChatDbViewController.RECEIVE);
            if (dbqbChatMessage.getReceiveStatus() != null)
                dbqbChatMessage.setReceiveStatus(dbqbChatMessage.getReceiveStatus());
            else {
                //1st time
                if (!chatMessage.isRead())
                    dbqbChatMessage.setReceiveStatus(ChatDbViewController.RECIEVED);
                else
                    dbqbChatMessage.setReceiveStatus(ChatDbViewController.SENT_READ);

            }
//            }
        }

        // set msg category
        if (chatMessage.getProperty(ChatDbViewController.PROPERTY_MSG_TYPE) != null) {
            dbqbChatMessage.setMessageCategory(chatMessage.getProperty(ChatDbViewController.PROPERTY_MSG_TYPE).toString());
        }

        if (chatMessage.getProperty(ChatDbViewController.PROPERTY_PHOTO_URL) != null) {
            //is photo
            dbqbChatMessage.setPhotoURL(chatMessage.getProperty(ChatDbViewController.PROPERTY_PHOTO_URL).toString());
            Log.w(TAG, "PhotoURL = " + chatMessage.getProperty(ChatDbViewController.PROPERTY_PHOTO_URL).toString());
        }

        if (chatMessage.getProperty(ChatDbViewController.PROPERTY_ENABLE_READ_TICK) == null) {
            dbqbChatMessage.setEnableReadMsg(true);
        } else {
            dbqbChatMessage.setEnableReadMsg(chatMessage.getProperty(ChatDbViewController.PROPERTY_ENABLE_READ_TICK).toString().equalsIgnoreCase("true") ? true : false);
        }

        if (chatMessage.getProperty(ChatDbViewController.PROPERTY_BINDING_AGENT_LISTING_ID) != null) {
            dbqbChatMessage.setBindingAgentListingId(chatMessage.getProperty(ChatDbViewController.PROPERTY_BINDING_AGENT_LISTING_ID).toString());
        }

        CryptLib.encryptDBQBMessage(dbqbChatMessage);

        return dbqbChatMessage;
    }

    // Insert or update message into DB
    public void addUpdateMsgDbAction(QBUser opponentQBUser, QBChatMessage chatMessage, String status) {
        if (!isSetEnableReadMsgTick(opponentQBUser))
            chatMessage.setProperty(ChatDbViewController.PROPERTY_ENABLE_READ_TICK, "false");
        else
            chatMessage.setProperty(ChatDbViewController.PROPERTY_ENABLE_READ_TICK, "true");
        addUpdateMsgDbAction(chatMessage, status, false);
    }

    public void addUpdateMsgDbAction(QBChatMessage chatMessage, String status, boolean isUnReadFirstMsg) {
        Gson gson = new Gson();
        DBQBChatMessage dbqbChatMessage = chatMessageToDbQbChatMessage(chatMessage, gson);
        DBQBChatMessage oldDbqbChatMessage = getQBChatMessageByMessageId(dbqbChatMessage.getMessageID());

        if (oldDbqbChatMessage == null) {
            dbqbChatMessage.setIsUnreadFirstMessage(isUnReadFirstMsg);
        } else {
            //have record
//            dbqbChatMessage.setIsUnreadFirstMessage(oldDbqbChatMessage.getIsUnreadFirstMessage());
            dbqbChatMessage = oldDbqbChatMessage;
        }

        if (dbqbChatMessage.getMessageType().equalsIgnoreCase(ChatDbViewController.SEND))
            dbqbChatMessage.setSendStatus(status);
        else
            dbqbChatMessage.setReceiveStatus(status);

        insertOrUpdateMsgToDb(dbqbChatMessage, chatMessage.getId());

    }

    public synchronized void markDeleteAllDialogMessages(String dialogId) {

        DBQBChatMessageDao dao = daoSession.getDBQBChatMessageDao();
        List<DBQBChatMessage> markDeleteChatMessageList = dao.queryBuilder().where(DBQBChatMessageDao.Properties.ChatDialogID.eq(dialogId)).list();
        for (DBQBChatMessage markDeleteChatMessage : markDeleteChatMessageList) {
            markDeleteChatMessage.setIsDeleted(true);
            updateQBChatMessage(markDeleteChatMessage);
        }


    }

    public synchronized void deleteAllDialogMessages(String dialogId) {
        List<DBQBChatMessage> dbqbChatMessageList = daoSession.getDBQBChatMessageDao().queryBuilder().
                where(DBQBChatMessageDao.Properties.ChatDialogID.eq(dialogId)).list();
        Log.w(TAG, "deleteAllDialogMessages dbqbChatMessageList: " + dbqbChatMessageList.size());
        if (dbqbChatMessageList == null ||
                dbqbChatMessageList.size() == 0)
            return;

        try {
            openWritableDb();
            daoSession.getDBQBChatMessageDao().deleteInTx(dbqbChatMessageList);
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized void resetUnReadMsg(String qbDialogId) {
        List<DBQBChatMessage> dbqbChatMessageList = listUnreadFirstMsg(qbDialogId);
        for (DBQBChatMessage dbqbChatMessage : dbqbChatMessageList) {
            dbqbChatMessage.setIsUnreadFirstMessage(false);
            updateQBChatMessage(dbqbChatMessage);
        }
    }

    public synchronized DBQBChatMessage insertQBChatMessage(DBQBChatMessage qBChatMessage) {
        try {
            if (qBChatMessage != null) {
                openWritableDb();
                DBQBChatMessageDao qbChatMessageDao = daoSession.getDBQBChatMessageDao();
                qbChatMessageDao.insert(qBChatMessage);
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return qBChatMessage;
    }

    public synchronized void updateQBChatMessage(DBQBChatMessage qBChatMessage) {
        try {
            if (qBChatMessage != null) {

                CryptLib.encryptDBQBMessage(qBChatMessage);
                openWritableDb();
                daoSession.update(qBChatMessage);
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized void insertOrReplaceQBChatMessage(DBQBChatMessage qBChatMessage) {
        try {
            if (qBChatMessage != null) {

                CryptLib.encryptDBQBMessage(qBChatMessage);
                openWritableDb();
                daoSession.insertOrReplace(qBChatMessage);
                daoSession.clear();
                updateDBQBChatDialogByDBQBChatMessage(qBChatMessage);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized boolean deleteQBChatMessageById(Long qBChatMessageId) {
        try {
            openWritableDb();
            DBQBChatMessageDao dao = daoSession.getDBQBChatMessageDao();
            dao.deleteByKey(qBChatMessageId);
            daoSession.clear();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    //update msg status
    public void updateMsgStatusDbAction(String chatMessageId, String status) {
        DBQBChatMessage dbqbChatMessage = getQBChatMessageByMessageId(chatMessageId);

        if (dbqbChatMessage == null)
            return;
        if (dbqbChatMessage.getMessageType() == null)
            return;
        if (status.equalsIgnoreCase(ChatDbViewController.READ)) {
            List<DBQBChatMessage> unreadMsgList = listUnReadQBChatMessageByDialogId(dbqbChatMessage.getChatDialogID());
            if (unreadMsgList != null && unreadMsgList.size() > 0) {
                for (DBQBChatMessage unreadMsg : unreadMsgList) {
                    if (dbqbChatMessage.getMessageType().equalsIgnoreCase(ChatDbViewController.SEND)) {
                        unreadMsg.setSendStatus(ChatDbViewController.READ);
                        updateQBChatMessage(unreadMsg);
                    }

                }
            }
        }
        if (dbqbChatMessage.getMessageType().equalsIgnoreCase(ChatDbViewController.SEND))
            dbqbChatMessage.setSendStatus(status);
        else
            dbqbChatMessage.setReceiveStatus(status);

        updateQBChatMessage(dbqbChatMessage);

    }

    // insert or update msg
    public void insertOrUpdateMsgToDb(DBQBChatMessage dbqbChatMessage, String chatMessageId) {

        Long updateMsgId = haveRecordQBChatMessage(chatMessageId);
        if (haveRecordQBChatMessage(chatMessageId) != -1) {
            //update msg
            dbqbChatMessage.setId(updateMsgId);
            updateQBChatMessage(dbqbChatMessage);
        } else {
            // insert msg
            //sendReadMessage status
            insertOrReplaceQBChatMessage(dbqbChatMessage);
            Log.w(TAG, "dbqbChatMessage.getReceiveStatus() = " + dbqbChatMessage.getReceiveStatus());
            Log.w(TAG, "dbqbChatMessage.getSendStatus() = " + dbqbChatMessage.getSendStatus());
        }
    }

    // Users Methods - Read

    public synchronized Long haveRecordQBUser(String qbId) {
        List<DBQBUser> dbqbUserList = daoSession.getDBQBUserDao().queryBuilder().where(DBQBUserDao.Properties.UserID.eq(qbId)).list();
        if (dbqbUserList.size() > 0) {
            return dbqbUserList.get(0).getId();
        }
        return Long.valueOf(-1);
    }

    public synchronized DBQBUser getDbQBUserByQBId(int qbId) {
        List<DBQBUser> dbqbUserList = daoSession.getDBQBUserDao().queryBuilder().where(DBQBUserDao.Properties.UserID.eq(qbId + "")).list();
        if (dbqbUserList.size() > 0) {
            return dbqbUserList.get(0);
        }
        return null;
    }

    public synchronized List<DBQBUser> listQBUser() {
        List<DBQBUser> qBUsers = null;
        try {
            openReadableDb();
            DBQBUserDao qBUserDao = daoSession.getDBQBUserDao();
            qBUsers = qBUserDao.loadAll();

            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return qBUsers;
    }

    public synchronized DBQBUser getQBUserById(Long qBUserId) {
        DBQBUser qBUser = null;
        try {
            openReadableDb();
            DBQBUserDao dao = daoSession.getDBQBUserDao();
//            qBUser = dao.loadDeep(qBUserId);
            qBUser = dao.load(qBUserId);
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return qBUser;
    }

    public synchronized Integer getMemberIDByQBUserId(String qBUserId) {
        DBQBUser qBUser = getDbQBUserByQBId(Integer.parseInt(qBUserId));
        if (qBUser != null) {
            return qBUser.getMemberID();
        }
        return -1;
    }

    public synchronized List<DBQBUser> listQBUserByName(String name) {
        String[] nameArray = name.split(" ");
//        List qBUserlist = daoSession.getDBQBUserDao().queryBuilder()
//                .where(DBQBUserDao.Properties.Name.like("%" + name + "%"))
//                .list();

        String queryWhereString = "";
        for (int i = 0; i < nameArray.length; i++) {
            if (i == 0) {
                queryWhereString = DBQBUserDao.Properties.Name.columnName + " LIKE '%" + nameArray[i] + "%'";
            } else {
                queryWhereString = queryWhereString + " OR " +
                        DBQBUserDao.Properties.Name.columnName + " LIKE '%" + nameArray[i] + "%'";
            }

        }

//        String sql = "SELECT * FROM " + DBQBUserDao.TABLENAME + " WHERE (iS_HIDDEN = 0 or is_hidden is null) and ("+queryWhereString+")";
        String sql = "SELECT * FROM " + " (SELECT  * FROM `DBQBUSER` where (iS_HIDDEN = 0 or is_hidden is null) and (" + queryWhereString + ")" +
                ") A, 'DBQBCHAT_DIALOG' B where A.USER_ID = B.QB_USER_ID order by B.Last_message_date desc";
        Cursor cursor = daoSession.getDatabase().rawQuery(sql, null);
        List<DBQBUser> qBUserlist = daoSession.getDBQBUserDao().loadAllDeepFromCursor(cursor);

        return qBUserlist;
    }

    public synchronized DBQBUser getQBUserByUserId(Long userId) {
        DBQBUser qBUser = null;
        try {
            openReadableDb();
            DBQBUserDao dao = daoSession.getDBQBUserDao();
            qBUser = dao.queryBuilder().where(DBQBUserDao.Properties.UserID.eq(userId)).list().get(0);
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return qBUser;
    }

    public synchronized DBQBUser getQBUserByMemberId(int memberId) {
        DBQBUser qBUser = null;
        try {
            openReadableDb();
            DBQBUserDao dao = daoSession.getDBQBUserDao();
            qBUser = dao.queryBuilder().where(DBQBUserDao.Properties.MemberID.eq(memberId)).list().get(0);
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return qBUser;
    }

    // Users Methods - Write

    public synchronized void insertOrUpdateQBUser(DBQBUser qBUser) {
        if (haveRecordQBUser("" + qBUser.getUserID()) != -1) {
            updateQBUser(qBUser);
            Log.w(TAG, "updateQBUser qBUser.getUserID() = " + qBUser.getUserID());
        } else {
            insertQBUser(qBUser);
            Log.w(TAG, "insertQBUser qBUser.getUserID() = " + qBUser.getUserID());
        }
    }

    public synchronized void saveQBUsersToDB(Collection<QBUser> users) {
        if (users != null) {
            for (QBUser user : users) {
                try {
                    insertOrUpdateQBUser(user);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public synchronized void insertOrUpdateQBUser(QBUser user) {
        DBQBUser mDBQBUser = getDbQBUserByQBId(user.getId());

        if (mDBQBUser != null) {
            mDBQBUser.setName(user.getFullName());
            mDBQBUser.setQBUser(new Gson().toJson(user, QBUser.class));

            updateQBUser(mDBQBUser);
        } else {
            mDBQBUser = new DBQBUser();
            mDBQBUser.setUserID(user.getId());
            try {
                mDBQBUser.setMemberID(Integer.parseInt(user.getExternalId()));
            } catch (Exception e) {
            }
            mDBQBUser.setName(user.getFullName());
            mDBQBUser.setQBUser(new Gson().toJson(user, QBUser.class));

            insertQBUser(mDBQBUser);
        }
    }

    public synchronized DBQBUser insertQBUser(DBQBUser qBUser) {
        try {
            if (qBUser != null) {
                openWritableDb();
                DBQBUserDao qbUserDao = daoSession.getDBQBUserDao();
                qbUserDao.insert(qBUser);
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return qBUser;
    }

    public synchronized void updateQBUser(DBQBUser qBUser) {
        try {
            if (qBUser != null) {
                openWritableDb();
                daoSession.update(qBUser);
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized void insertOrReplaceQBUser(DBQBUser qBUser) {
        try {
            if (qBUser != null) {
                openWritableDb();
                daoSession.insertOrReplace(qBUser);
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized void deleteAllQBUser() {
        try {
            openWritableDb();
            DBQBUserDao dao = daoSession.getDBQBUserDao();
            dao.deleteAll();
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // DB Actions Methods

    public void dbActionForLogin(ResponseLoginSocial.Content content) {
         /*
        * update DB: LoginInfo */
        DBLogInfo dbLogInfo = new DBLogInfo();
//        deleteAllLogInfo();
        dbLogInfo.setMemberID(content.memId);
        LocalStorageHelper.setAccessToken(context,content.session);
        try {
            LocalStorageHelper.setRegedPhoneAeraCode(context,content.countryCode);
            LocalStorageHelper.setRegedPhoneNumber(context,content.phoneNumber);
        }catch (Exception e){
            e.printStackTrace();
        }
        content.session = "";
        content.qbPwd = "";

        dbLogInfo.setLogInfo(new Gson().toJson(content));
        //Hardcode qbid to Integer
        if (content.qbid != null) {
            if (!content.qbid.equalsIgnoreCase("") || !content.qbid.equalsIgnoreCase("null"))
                try {
                    dbLogInfo.setQbUserId(Integer.parseInt(content.qbid));
                } catch (NumberFormatException e) {
                    Log.e(TAG, "error = " + e);
                }

        }
        insertOrReplaceLogInfo(dbLogInfo);
    }

    public void dbActionForChatLobby(QBDialog dialog,
                                     boolean isAgent,
                                     AgentProfiles userProfile,
                                     String currentQbId,
                                     ChatRoomMember userChatRoomMem,
                                     ChatRoomMember opponentChatRoomMem) {
        /*
        * update DB: QbChatDialog, QBUser, ChatroomAgentProfiles*/
        Integer qbId = QBUtil.getOpponentIDForPrivateDialog(dialog, currentQbId);

        dbActionQBChatDialog(dialog, userProfile, qbId, userChatRoomMem, opponentChatRoomMem);
        dbActionQBUser(userProfile, qbId);
        dbActionChatroomAgentProfiles(isAgent, userProfile, qbId);
    }

    private void dbActionChatroomAgentProfiles(boolean isAgent, AgentProfiles userProfile, Integer qbId) {
        DBChatroomAgentProfiles dbChatroomAgentProfiles = new DBChatroomAgentProfiles();
        Integer memId = 0;
        if (userProfile != null) {
            memId = userProfile.memberID;
            dbChatroomAgentProfiles.setFollowerCount(userProfile.followerCount);
            dbChatroomAgentProfiles.setFollowingCount(userProfile.followingCount);
            dbChatroomAgentProfiles.setIsFollowing(userProfile.isFollowing);
        }

        dbChatroomAgentProfiles.setIsAgent(isAgent);
        dbChatroomAgentProfiles.setMemberID(memId);
        dbChatroomAgentProfiles.setQbUserID(qbId);
        dbChatroomAgentProfiles.setChatroomAgentProfiles(new Gson().toJson(userProfile));
        insertOrUpdateChatroomAgentProfiles(dbChatroomAgentProfiles);
        insertOrReplaceChatroomAgentProfiles(dbChatroomAgentProfiles);
    }

    private void dbActionQBUser(AgentProfiles userProfile, Integer qbId) {
        DBQBUser dbqbUser = new DBQBUser();
//        Integer opponentID = ChatService.getInstance()
//                .getOpponentIDForPrivateDialog(dialog);
        Integer memId = 0;
        if (userProfile != null) {
            memId = userProfile.memberID;
        }

        QBUser user = ChatService.getInstance().getDialogsUsers()
                .get(qbId);
        Gson gson = new Gson();
//        if (needInsertOrUpdateQbUser(user)) {
        dbqbUser.setMemberID(memId);
//        dbqbUser.setIsAgent(isAgent);
        dbqbUser.setUserID(qbId);
        dbqbUser.setQBUser(gson.toJson(user));
        if (user != null)
            dbqbUser.setName(user.getFullName());
//        insertOrReplaceQBUser(dbqbUser);
        insertOrUpdateQBUser(dbqbUser);
//        }

    }

    private void dbActionQBChatDialog(QBDialog dialog,
                                      AgentProfiles userProfile,
                                      Integer qbId,
                                      ChatRoomMember userChatRoomMem,
                                      ChatRoomMember opponentChatRoomMem) {
        Integer memId = 0;
        if (userProfile != null) {
            memId = userProfile.memberID;
        }
        DBQBChatDialog dbqbChatDialog = new DBQBChatDialog();
        dbqbChatDialog.setDialogID(dialog.getDialogId());
        dbqbChatDialog.setQbUserID(qbId);
        dbqbChatDialog.setLastMessageDate("" + dialog.getLastMessageDateSent());
        dbqbChatDialog.setLastMessageText(dialog.getLastMessage());
        dbqbChatDialog.setLastMessageUserID(qbId);
        dbqbChatDialog.setQBChatDialog(new Gson().toJson(dialog));
//        dbqbChatDialog.setIsAgent(isAgent);
        dbqbChatDialog.setNeedInit(false);


        if (userChatRoomMem != null) {
            dbqbChatDialog.setBlocked(userChatRoomMem.blocked);
            dbqbChatDialog.setDeleted(userChatRoomMem.deleted);
            dbqbChatDialog.setExited(userChatRoomMem.exited);
            dbqbChatDialog.setMuted(userChatRoomMem.muted);
        }
        if (opponentChatRoomMem != null) {
            dbqbChatDialog.setBlockedOpponent(opponentChatRoomMem.blocked);
            dbqbChatDialog.setDeletedOpponent(opponentChatRoomMem.deleted);
            dbqbChatDialog.setExitedOpponent(opponentChatRoomMem.exited);
            dbqbChatDialog.setMutedOpponent(opponentChatRoomMem.muted);
        }

        dbqbChatDialog.setUnReadMessageCount(dbqbChatDialog.getUnReadMessageCount());
        insertOrUpdateQBChatDialog(dbqbChatDialog);
//        this.insertOrReplaceQBChatDialog(dbqbChatDialog);
    }


    /**
     * Phone book
     */
    public synchronized List<DBPhoneBook> listAllContacts() {
        List qbPhoneBookList = daoSession.getDBPhoneBookDao().queryBuilder()
                .orderAsc(DBPhoneBookDao.Properties.Name)
                .list();
        return qbPhoneBookList;
    }

    public DBPhoneBook getPhonebookItem(String phoneNo) {
        List qbPhoneBookList = daoSession.getDBPhoneBookDao().queryBuilder()
                .where(DBPhoneBookDao.Properties.PhoneNumber.eq(phoneNo))
                .orderAsc(DBPhoneBookDao.Properties.Name)
                .limit(1)
                .list();
        return (DBPhoneBook)qbPhoneBookList.get(0);
    }

    public int getCheckedPhoneNoCount(){
        List qbPhoneBookList = daoSession.getDBPhoneBookDao().queryBuilder()
                .where(DBPhoneBookDao.Properties.HasChecked.eq(true))
                .list();
        return qbPhoneBookList.size();
    }

    public List<DBPhoneBook> getCheckedPhoneItem(){
        List<DBPhoneBook> qbPhoneBookList = daoSession.getDBPhoneBookDao().queryBuilder()
                .where(DBPhoneBookDao.Properties.HasChecked.eq(true))
                .list();
        return qbPhoneBookList;
    }


    public void insertPhoneBook(List<DBPhoneBook> phoneBookList){
        // [C]reate
        DBPhoneBookDao dbPhoneBookDao=daoSession.getDBPhoneBookDao();
//        if (dbPhoneBookDao!=null) {
//            dbPhoneBookDao.deleteAll();
//        }
        Log.d("DBPhoneBook","insertPhoneBook: "+dbPhoneBookDao);

        for (DBPhoneBook item:phoneBookList) {
            DBPhoneBook phoneBook;
            try {
                // Try to find the Example-Object if it is already existing.
                phoneBook = dbPhoneBookDao.queryBuilder().where(DBPhoneBookDao.Properties.PhoneNumber.eq(item.getPhoneNumber())).unique();
            } catch (Exception ex) {
                phoneBook = null;
            }
            if (phoneBook == null) {
                phoneBook = new DBPhoneBook();
                phoneBook.setCountryCode(item.getCountryCode());
                phoneBook.setPhoneNumber(item.getPhoneNumber());
                phoneBook.setName(item.getName());
                phoneBook.setHasChecked(item.getHasChecked()==null?false:item.getHasChecked());
                phoneBook.setHasSent(item.getHasSent()==null?false:item.getHasSent());
                dbPhoneBookDao.insertOrReplace(phoneBook);
            }else{
                updatePhoneBook(phoneBook);
            }
        }
    }

    public void updatePhoneBook(DBPhoneBook item){
        // [U]pdate
        DBPhoneBookDao dbPhoneBookDao=daoSession.getDBPhoneBookDao();
//        DBPhoneBook phoneBook = getPhonebookItem(item.phoneNo);
//        phoneBook.setHasSent(item.isHasSent());
//        phoneBook.setHasChecked(item.getStatus()== RePhonebookItem.STATUS.CHECKED);
        dbPhoneBookDao.update(item);
        Log.d("DBPhoneBook","updatePhoneBook: "+item);
    }

    public List<DBPhoneBook> readPhoneBook(){
        // [U]pdate
        DBPhoneBookDao dbPhoneBookDao=daoSession.getDBPhoneBookDao();
       return dbPhoneBookDao.loadAll();
    }

    public void daletePhoneBook(){
        // [D]elete
        DBPhoneBookDao dbPhoneBookDao=daoSession.getDBPhoneBookDao();
        dbPhoneBookDao.deleteAll();
    }
}
