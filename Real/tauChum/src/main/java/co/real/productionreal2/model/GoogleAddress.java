package co.real.productionreal2.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GoogleAddress {

	
	@SerializedName("address_components")
	public List<AddressComponent> googleAddressList;
	
	@SerializedName("formatted_address")
	public String formattedAddress; 
	
	@SerializedName("geometry")
	public Geometry geometry; 
	
	@SerializedName("place_id")
	public String placeId;


	@Override
	public String toString() {
		return "GoogleAddress{" +
				"googleAddressList=" + googleAddressList +
				", formattedAddress='" + formattedAddress + '\'' +
				", geometry=" + geometry +
				", placeId='" + placeId + '\'' +
				'}';
	}

	public class AddressComponent {
    	
    	@SerializedName("long_name")
    	public String longName;
    	
    	@SerializedName("short_name")
    	public String shortName; 
    	
    	@SerializedName("types")
    	public List<String> types; 

    }
	
  
    public class Geometry {
    	
    	@SerializedName("bounds")
    	public CoordRect bounds; 
    	
    	@SerializedName("location")
    	public LatLng location; 
    	
    	@SerializedName("location_type")
    	public String locationType;
    	
    	@SerializedName("viewport")
    	public CoordRect viewport; 
    	
    }
    
    public class CoordRect {
    
    	@SerializedName("northeast")
    	public LatLng northeast; 
    	
    	@SerializedName("southwest")
    	public LatLng southwest; 
    }
    
    
    public class LatLng{
    	
    	@SerializedName("lat")
    	public double lat; 
    	@SerializedName("lng")
    	public double lng; 
    }


    
}
