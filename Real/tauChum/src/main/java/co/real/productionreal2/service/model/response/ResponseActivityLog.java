package co.real.productionreal2.service.model.response;

import com.google.gson.annotations.SerializedName;
import co.real.productionreal2.service.model.ActivityLog;

import java.util.List;

/**
 * Created by kelvinsun on 15/1/16.
 */
public class ResponseActivityLog extends ResponseBase {


    @SerializedName("Content")
    public Content content;

    public class Content {

        @SerializedName("Log")
        public List<ActivityLog> activityLog;

        @SerializedName("TotalCount")
        public int totalCount;

        @SerializedName("RecordCount")
        public int recordCount;



    }


}