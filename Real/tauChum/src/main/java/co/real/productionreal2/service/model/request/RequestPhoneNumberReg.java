package co.real.productionreal2.service.model.request;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.model.InputRequest;
import co.real.productionreal2.service.AdminService;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.response.ResponseCreateListing;
import co.real.productionreal2.util.NetworkUtil;
import co.real.productionreal2.view.Dialog;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by hohojo on 18/11/2015.
 */
public class RequestPhoneNumberReg extends RequestBase {
    public RequestPhoneNumberReg(int memberID, String accessToken) {
        super(memberID, accessToken);

    }

    @SerializedName("CountryCode")
    public String countryCode;
    @SerializedName("PhoneNumber")
    public String phoneNumber;
    @SerializedName("Lang")
    public String lang;

    public void callPhoneNumberRegApi(final Context context, final ApiCallback callback) {
        String json = new Gson().toJson(this);
        Log.d("callPhoneNumberRegApi", "callPhoneNumberRegApi = " + json);
        final AdminService weatherService = new AdminService(context);
        weatherService.getCoffeeService().phoneNumberReg(
                new InputRequest(json),
                new retrofit.Callback<ApiResponse>() {
                    @Override
                    public void failure(RetrofitError error) {
                        callback.failure("");
                        NetworkUtil.showNoNetworkMsg(context);
                    }

                    @Override
                    public void success(ApiResponse apiResponse, Response response) {
                        ResponseCreateListing responseGson = new Gson().fromJson(apiResponse.jsonContent, ResponseCreateListing.class);
                        Log.w("callAgentListingAdd", "responseGson = " + responseGson);
                        if (responseGson.errorCode == 4) {
                            Dialog.accessErrorForceLogoutDialog(context).show();
                        } else if (responseGson.errorCode == 26) {
                            Dialog.suspendedErrorForceLogoutDialog(context).show();
                        } else if (responseGson.errorCode != 0 && responseGson.errorCode != 20 && responseGson.errorCode != 21 &&
                                responseGson.errorCode != 22 && responseGson.errorCode != 23) {
                            callback.failure(responseGson.errorMsg);
                        } else {
                            //listing photos
                            callback.success(apiResponse);
                        }
                    }
                }
        );
    }

}
