package co.real.productionreal2.service;

import android.content.Context;

import co.real.productionreal2.Constants;
import co.real.productionreal2.model.InputRequest;
import co.real.productionreal2.service.model.ApiResponse;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by kelvinsun on 2/3/16.
 */
public class MapService extends BaseService{

    public final Map mapService;

    public MapService(Context context) {
        RequestInterceptor requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestInterceptor.RequestFacade request) {
                request.addHeader("Accept", "application/json");
                request.addHeader("Content-type", "application/json");

            }
        };


        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(WEB_SERVICE_BASE_URL)
                .setRequestInterceptor(requestInterceptor)
                .setLogLevel(Constants.isDevelopment? RestAdapter.LogLevel.FULL : RestAdapter.LogLevel.NONE)
                .build();

        mapService = restAdapter.create(Map.class);
    }

    public interface Map {

        @POST("/Map.asmx/GoogleMapPlaceAPIAutoComplete")
        public void googleMapPlaceAPIAutoComplete(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);

        @POST("/Map.asmx/GoogleMapPlaceAPIGeocodeByPlaceID")
        public void googleMapPlaceAPIGeocodeByPlaceID(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);

        @POST("/Map.asmx/GoogleMapPlaceAPIGeocodeBySearchAddress")
        public void googleMapPlaceAPIGeocodeBySearchAddress(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);

    }


    public Map getMapService() {
        return mapService;
    }


}
