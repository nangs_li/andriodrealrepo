package co.real.productionreal2.activity.chat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.nineoldandroids.view.ViewHelper;
import com.nineoldandroids.view.ViewPropertyAnimator;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.exception.QBChatException;
import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.chat.model.QBDialog;
import com.quickblox.content.QBContent;
import com.quickblox.content.model.QBFile;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.core.helper.FileHelper;
import com.quickblox.core.request.QBRequestGetBuilder;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import co.real.productionreal2.Constants;
import co.real.productionreal2.CoreData;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.BaseActivity;
import co.real.productionreal2.activity.MainActivityTabBase;
import co.real.productionreal2.adapter.ChatAdapter;
import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.callback.DialogCallback;
import co.real.productionreal2.callback.UpdateChatViewListener;
import co.real.productionreal2.chat.ChatDbViewController;
import co.real.productionreal2.chat.core.ChatService;
import co.real.productionreal2.chat.core.PrivateChatImpl;
import co.real.productionreal2.common.LocalStorageHelper;
import co.real.productionreal2.config.AppConfig;
import co.real.productionreal2.dao.account.DBChatroomAgentProfiles;
import co.real.productionreal2.dao.account.DBLogInfo;
import co.real.productionreal2.dao.account.DBQBChatDialog;
import co.real.productionreal2.dao.account.DBQBChatMessage;
import co.real.productionreal2.dao.account.DBQBUser;
import co.real.productionreal2.daomanager.DatabaseManager;
import co.real.productionreal2.model.AgentListing;
import co.real.productionreal2.model.QBUserCustomData;
import co.real.productionreal2.model.UserQBUser;
import co.real.productionreal2.service.BaseService;
import co.real.productionreal2.service.model.AgentProfiles;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.ChatRoomMember;
import co.real.productionreal2.service.model.request.RequestAgentListingGetList;
import co.real.productionreal2.service.model.request.RequestChatRoomBlock;
import co.real.productionreal2.service.model.request.RequestChatRoomCreate;
import co.real.productionreal2.service.model.request.RequestChatRoomExit;
import co.real.productionreal2.service.model.request.RequestChatRoomGet;
import co.real.productionreal2.service.model.request.RequestChatRoomMute;
import co.real.productionreal2.service.model.response.ResponseAgentListingGetList;
import co.real.productionreal2.service.model.response.ResponseChatRoomCreate;
import co.real.productionreal2.service.model.response.ResponseChatRoomGet;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;
import co.real.productionreal2.util.AppUtil;
import co.real.productionreal2.util.DBUtil;
import co.real.productionreal2.util.FileUtil;
import co.real.productionreal2.util.ImageUtil;
import co.real.productionreal2.util.NetworkUtil;
import co.real.productionreal2.util.PermissionUtil;
import co.real.productionreal2.util.RoundedTransformation;
import co.real.productionreal2.util.TimeUtils;
import co.real.productionreal2.view.Dialog;
import io.branch.indexing.BranchUniversalObject;
import me.iwf.photopicker.PhotoPickerActivity;
import me.iwf.photopicker.utils.PhotoPickerIntent;
import widget.RoundedImageView;
import widget.observablescrollview.ObservableListView;
import widget.observablescrollview.ObservableScrollViewCallbacks;
import widget.observablescrollview.ScrollState;

public class ChatActivity extends BaseActivity {

    private static final String TAG = ChatActivity.class.getSimpleName();

    public static Context context;
    //    public static final String EXTRA_DIALOG = "dialog";
//    public static final String EXTRA_USER = "user";
    public static final String EXTRA_DIALOG_ID = "dialogId";
    public static final String EXTRA_USER_ID = "userId";
    public static final String EXTRA_MEM_ID = "memId";
    public static final String EXTRA_HAS_HISTORY = "has_history";
    public static final String EXTRA_IS_FORM_CHATLOBBY= "isFormChatLobby";
    public static final String EXTRA_BINDING_AGENT_LISTING_ID = "bindingAgentListingId";
    public static boolean activityIsRunning = false;
    private static final int SELECT_PHOTO = 0;

    @InjectView(R.id.messageEdit)
    EditText messageEditText;
    @InjectView(R.id.messagesListView)
    ObservableListView msgListView;
    @InjectView(R.id.chatSendButton)
    ImageButton chatSendButton;
    @InjectView(R.id.addPhotoBtn)
    ImageButton addPhotoBtn;
    @InjectView(R.id.companionLabel)
    TextView companionLabel;
    @InjectView(R.id.typeStatusLabel)
    TextView typeStatusLabel;
    @InjectView(R.id.chatImageView)
    RoundedImageView chatImageView;
    @InjectView(R.id.rlAgentListing)
    RelativeLayout rlAgentListing;
    @InjectView(R.id.chatEditLinearLayout)
    LinearLayout chatEditLinearLayout;
    @InjectView(R.id.muteTextView)
    TextView muteTextView;
    @InjectView(R.id.blockTextView)
    TextView blockTextView;
    @InjectView(R.id.exitChatBtn)
    Button exitChatBtn;
    @InjectView(R.id.exitMaskRelativeLayout)
    RelativeLayout exitMaskRelativeLayout;
    @InjectView(R.id.sendMsgLinearLayout)
    LinearLayout sendMsgLinearLayout;
    @InjectView(R.id.blockRelativeLayout)
    RelativeLayout blockRelativeLayout;
    @InjectView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @InjectView(R.id.ivListing)
    ImageView ivListing;
    @InjectView(R.id.propertyTypeTextView)
    TextView propertyTypeTextView;
    @InjectView(R.id.listingTitleTextView)
    TextView listingTitleTextView;
    @InjectView(R.id.listingSubtitleTextView)
    TextView listingSubtitleTextView;

    @InjectView(R.id.chatEditBtn)
    ImageButton chatEditBtn;

    @InjectView(R.id.maskView)
    View maskView;

    private int mBaseTranslationY;
    private int listViewScrollOldTop;
    private int listViewScrollOldFirstVisibleItem;
    private boolean listViewIsScrollingUp = false;

    private ChatAdapter chatAdapter;

//    private boolean isTimerRunning = false;

    private ChatDbViewController chatDbViewController;
    private Handler getOpponentUserHandler;
    private Handler checkLoggedInHandler = new Handler();

    // for listview load more
//    private static final int showMsgNo = 20;
//    private int limitMsgNo = 1;
//    private boolean listShowTop = false;
    private int currentMsgsNo;
    private int noOfMsgsAdded;
    private int currentListingId = -1;
    private Long currentListingDatabaseId = -1l;
    private int currentListingLineNo;
    private ResponseLoginSocial.Content userContent;

    private AgentProfiles opponentAgentProfile;
    private QBUser opponentQBUser;
    private int opponentQBID = -1;
    private QBDialog qbDialog;
    private String qbDialogId;
    private int opponentMemId;
    private int bindingAgentListingId;
    private boolean hasHistory;
//    private PrivateChatImpl privateChatImpl;

    boolean isFormLobby = false;
    boolean ishaveBlocked = true;

    public String getDialogId(){
        if(qbDialogId != null) {
            return qbDialogId;
        }else{
            return "";
        }
    }

    public int getOpponentQBID(){
        return opponentQBID;
    }

    public static void start(Context context, Bundle bundle) {
        Intent intent = new Intent(context, ChatActivity.class);
        intent.putExtras(bundle);
        context.startActivity(intent);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        isFormLobby = getIntent().getBooleanExtra(EXTRA_IS_FORM_CHATLOBBY,false);

        userContent = CoreData.getUserContent(context);

        context = this;
        ButterKnife.inject(this);
        chatDbViewController = ChatDbViewController.getInstance(this);
        chatDbViewController.setUpdateChatViewListener(updateChatViewListener);

        chatAdapter = new ChatAdapter(this);


        setIntentExtra(getIntent());
        initView();

        getOpponentUserHandler = new Handler();

        if (qbDialog != null) {
            // first time in chat room, get latest listingid
            if (DatabaseManager.getInstance(this).getChatroomLastListingId(qbDialog.getDialogId()) != null) {
                String listingId = DatabaseManager.getInstance(this).getChatroomLastListingId(qbDialog.getDialogId());
                if (listingId != null)
                    currentListingId = Integer.parseInt(DatabaseManager.getInstance(this).getChatroomLastListingId(qbDialog.getDialogId()));
            }
        }
        Log.i(TAG, "currentListingId = " + currentListingId);

    }

    private void setIntentExtra(Intent intent) {
        opponentQBID = Integer.parseInt(intent.getStringExtra(EXTRA_USER_ID));
        qbDialogId = intent.getStringExtra(EXTRA_DIALOG_ID);
        opponentMemId = intent.getIntExtra(EXTRA_MEM_ID, 0);
        hasHistory = intent.getBooleanExtra(EXTRA_HAS_HISTORY, false);
//        isfollowing = intent.getBooleanExtra(EXTRA_IS_FOLLOWING, false);

        bindingAgentListingId = intent.getIntExtra(EXTRA_BINDING_AGENT_LISTING_ID, -1);

        if (qbDialogId == null) {
            //first time, get opponent QBUser
            getOpponentQBUserFromQB();
        } else {
            // come from chatlobby, not first time
            DBQBUser dbqbUser = DatabaseManager.getInstance(this).getQBUserByUserId((long) opponentQBID);
            try{
                DBChatroomAgentProfiles dbChatroomAgentProfiles = DatabaseManager.getInstance(this).getChatroomAgentProfilesByUserId((long) opponentQBID);
                opponentAgentProfile = new Gson().fromJson(dbChatroomAgentProfiles.getChatroomAgentProfiles(), AgentProfiles.class);
            }catch (Exception e){

            }
            if (dbqbUser.getQBUser() != null) {
                Gson gson=  new GsonBuilder().setDateFormat("MMM dd, yyyy HH:mm:ss").create();
                opponentQBUser = gson.fromJson(dbqbUser.getQBUser(), QBUser.class);
                DBQBChatDialog dbqbChatDialog = DatabaseManager.getInstance(this).getQBChatDialogByDialogId(qbDialogId);
                qbDialog = gson.fromJson(dbqbChatDialog.getQBChatDialog(), QBDialog.class);
                chatAdapter.setDialogId(qbDialogId);
            }
        }

    }

    private boolean isFollowing() {
        DBChatroomAgentProfiles dbChatroomAgentProfiles = DatabaseManager.getInstance(this).getChatroomAgentProfilesByMemId(opponentMemId);
        if (dbChatroomAgentProfiles == null)
            return false;
        return  dbChatroomAgentProfiles.getIsFollowing()==1?true:false;
    }

    private void checkEnableSwipeRefreshlayout() {
        int totalMsgSize = DatabaseManager.getInstance(ChatActivity.this).listQbMessageByDialogId(qbDialogId).size();
        if (currentMsgsNo >= totalMsgSize) {
            swipeRefreshLayout.setRefreshing(false);
            swipeRefreshLayout.setEnabled(false);
        } else {
            swipeRefreshLayout.setEnabled(true);
        }
    }

    private void initView() {
        swipeRefreshLayout.setColorSchemeResources(R.color.newsfeed_text_nuber_blue);
        swipeRefreshLayout.setEnabled(false);

        initOpponentQBUserView(opponentQBUser);
        initChatSettingView();

        messageEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                Log.w(TAG, "sendIsTypingNotification");
                if (NetworkUtil.isConnected(ChatActivity.this)) {
                    if (getPrivateChatImpl() == null)
                        return;
                    if (QBChatService.getInstance().isLoggedIn())
                        getPrivateChatImpl().sendIsTypingNotification();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        msgListView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                msgListView.getViewTreeObserver().removeOnPreDrawListener(this);
                return false;
            }
        });

        msgListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {
                checkEnableSwipeRefreshlayout();

            }

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                View view = absListView.getChildAt(0);
                int top = (view == null) ? 0 : view.getTop();
                if (firstVisibleItem == listViewScrollOldFirstVisibleItem) {
                    if (top > listViewScrollOldTop) {
                        listViewIsScrollingUp = true;
                    } else if (top < listViewScrollOldTop) {
                        listViewIsScrollingUp = false;
                    }
                } else {
                    if (firstVisibleItem < listViewScrollOldFirstVisibleItem) {
                        listViewIsScrollingUp = true;
                    } else {
                        listViewIsScrollingUp = false;
                    }
                }

                listViewScrollOldTop = top;
                listViewScrollOldFirstVisibleItem = firstVisibleItem;

                if (firstVisibleItem == 0) {
                    ViewPropertyAnimator.animate(rlAgentListing).cancel();
                    ViewHelper.setTranslationY(rlAgentListing, -rlAgentListing.getHeight());
                }

                // first visible item reach  listing item
                if (chatAdapter.getListingMessageLineNoMap().containsKey(firstVisibleItem)) {
                    ViewPropertyAnimator.animate(rlAgentListing).cancel();
                    ViewHelper.setTranslationY(rlAgentListing, -rlAgentListing.getHeight());
                    DBQBChatMessage dbqbListingMessage = null;
                    int noOfItem = chatAdapter.getListingMessageLineNoMap().get(firstVisibleItem);
                    if (listViewIsScrollingUp) {
                        if (noOfItem > 0) {
                            dbqbListingMessage = chatAdapter.getListingMessageList().get(noOfItem - 1);
                            setCurrentListingLineNo(chatAdapter.getListingMessageLineNoList().get(noOfItem - 1));
                        } else {
                            dbqbListingMessage = chatAdapter.getListingMessageList().get(noOfItem);
                            setCurrentListingLineNo(chatAdapter.getListingMessageLineNoList().get(noOfItem));
                        }
                    } else {
                        dbqbListingMessage = chatAdapter.getListingMessageList().get(noOfItem);
                        setCurrentListingLineNo(chatAdapter.getListingMessageLineNoList().get(noOfItem));
                    }

                    //set Listing Title Bar View
                    if (dbqbListingMessage == null)
                        return;
                    if (dbqbListingMessage.getAgentListing() == null) {
                        callAgentListingGetListAndSetView(dbqbListingMessage);
                    } else {
                        AgentListing agentListing = new Gson().fromJson(dbqbListingMessage.getAgentListing(), AgentListing.class);
                        setListingTitleBarView(agentListing);
                    }
                }
            }

        });

        msgListView.setScrollViewCallbacks(new ObservableScrollViewCallbacks() {
            @Override
            public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
                if (dragging&& chatAdapter.getListingMessageLineNoMap().size() > 0) {
                    rlAgentListing.setVisibility(View.VISIBLE);
//                    if (currentListingId == -1)
//                        rlAgentListing.setVisibility(View.GONE);
//                    else
//                        rlAgentListing.setVisibility(View.VISIBLE);

                    int toolbarHeight = rlAgentListing.getHeight();
                    if (firstScroll) {
                        mBaseTranslationY = scrollY;
                    }
                    float currentHeaderTranslationY = ViewHelper.getTranslationY(rlAgentListing);
                    float headerTranslationY = currentHeaderTranslationY + mBaseTranslationY - scrollY;
                    if (headerTranslationY < -toolbarHeight)
                        headerTranslationY = -toolbarHeight;
                    else if (headerTranslationY > 0)
                        headerTranslationY = 0;
                    ViewPropertyAnimator.animate(rlAgentListing).cancel();
                    ViewHelper.setTranslationY(rlAgentListing, headerTranslationY);
                }
            }

            @Override
            public void onDownMotionEvent() {

            }

            @Override
            public void onUpOrCancelMotionEvent(ScrollState scrollState) {

            }
        });
        msgListView.setAdapter(chatAdapter);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Log.v("","edwincc swipeRefreshLayout onRefresh qbDialogId "+qbDialogId+" "+currentListingId);
                DBQBChatMessage dbqbListingMessage = DatabaseManager.getInstance(ChatActivity.this).getPreviousListingMsg(qbDialogId, currentListingId, currentListingDatabaseId);
                if (dbqbListingMessage != null) {
                    currentListingId = Integer.parseInt(dbqbListingMessage.getBindingAgentListingId());
                    currentListingDatabaseId = dbqbListingMessage.getId();
                }else {
                    currentListingId = -1;
                    currentListingDatabaseId = -1l;
                }

                fetchCurrentMsgs(false);
                swipeRefreshLayout.setRefreshing(false);
            }
        });

        //first time not show Listing title

    }

    public int getCurrentListingLineNo() {
        return currentListingLineNo;
    }

    public void setCurrentListingLineNo(int currentListingLineNo) {
        Log.w(TAG, "setCurrentListingLineNo currentListingLineNo = " + currentListingLineNo);
        this.currentListingLineNo = currentListingLineNo;
    }

    private void setListingTitleBarView(AgentListing agentListing) {
        String imageName = AppUtil.getPropertyTypeImageName(agentListing.propertyType, agentListing.spaceType);
        ivListing.setImageResource(context.getResources().getIdentifier(imageName, "drawable", context.getPackageName()));
        String propertyTypeName = AppUtil.getPropertyTypeHashMap(context, userContent.systemSetting.propertyTypeList).get(agentListing.propertyType + "," + agentListing.spaceType);
        propertyTypeTextView.setText(propertyTypeName);
        listingTitleTextView.setText(agentListing.googleAddresses.get(0).name);
        listingSubtitleTextView.setText(agentListing.googleAddresses.get(0).location);
    }

    private void callAgentListingGetListAndSetView(final DBQBChatMessage dbqbChatMessage) {
        ArrayList<Integer> listingIdList = new ArrayList<>();
        listingIdList.add(Integer.parseInt(dbqbChatMessage.getBindingAgentListingId()));
        RequestAgentListingGetList request = new RequestAgentListingGetList(userContent.memId, LocalStorageHelper.getAccessToken(getBaseContext()), listingIdList);
        request.callAgentListingGetListApi(context, new ApiCallback() {
            @Override
            public void success(ApiResponse apiResponse) {
                ResponseAgentListingGetList responseGson = new Gson().fromJson(apiResponse.jsonContent, ResponseAgentListingGetList.class);
                if (responseGson.content.agentListingsList != null && responseGson.content.agentListingsList.size() > 0) {
                    AgentListing agentListing = responseGson.content.agentListingsList.get(0);
                    dbqbChatMessage.setAgentListing(new Gson().toJson(agentListing));
                    DatabaseManager.getInstance(context).updateQBChatMessage(dbqbChatMessage);
                    setListingTitleBarView(agentListing);
                }
            }

            @Override
            public void failure(String errorMsg) {

            }
        });
    }


    @Override
    protected void onStart() {
        super.onStart();
        activityIsRunning = true;
    }

    private void callGetChatRoomApi() {
        final RequestChatRoomGet chatRoomGet = new RequestChatRoomGet(userContent.memId, LocalStorageHelper.getAccessToken(getBaseContext()));
        if (qbDialogId == null)
            return;
        chatRoomGet.dialogId = qbDialogId;

        chatRoomGet.callChatRoomGetApi(this, new ApiCallback() {
            @Override
            public void success(ApiResponse apiResponse) {
                ResponseChatRoomGet responseGson = new Gson().fromJson(apiResponse.jsonContent, ResponseChatRoomGet.class);
                DBQBChatDialog dbqbChatDialog = DatabaseManager.getInstance(context).getQBChatDialogByDialogId(chatRoomGet.dialogId);
                if (responseGson.content.chatRoom == null) {
                    if (Constants.isDevelopment)
                        Toast.makeText(context, "response chatroom = null", Toast.LENGTH_LONG).show();
                    return;
                }
                if (responseGson.content.chatRoom.size() < 1) {
                    if (Constants.isDevelopment)
                        Toast.makeText(context, "response chatroom size < 1", Toast.LENGTH_LONG).show();
                    return;
                }
                if (dbqbChatDialog == null) {
                    if (Constants.isDevelopment)
                        Toast.makeText(context, "dbqbChatDialog = null", Toast.LENGTH_LONG).show();
                    return;
                }

                for (ChatRoomMember chatRoomMem : responseGson.content.chatRoom.get(0).chatRoomMemList) {
                    if (chatRoomMem.qbId.equalsIgnoreCase(userContent.qbid)) {
                        dbqbChatDialog.setDeleted(chatRoomMem.deleted);
                        dbqbChatDialog.setExited(chatRoomMem.exited);
                        dbqbChatDialog.setBlocked(chatRoomMem.blocked);
                        dbqbChatDialog.setMuted(chatRoomMem.muted);
                    } else {
                        dbqbChatDialog.setDeletedOpponent(chatRoomMem.deleted);
                        dbqbChatDialog.setExitedOpponent(chatRoomMem.exited);
                        dbqbChatDialog.setBlockedOpponent(chatRoomMem.blocked);
                        dbqbChatDialog.setMutedOpponent(chatRoomMem.muted);
                    }
                }
                DatabaseManager.getInstance(ChatActivity.this).updateQBChatDialog(dbqbChatDialog);
                initChatSettingView();
                chatSendButton.setEnabled(true);
            }

            @Override
            public void failure(String errorMsg) {
                if (Constants.isDevelopment)
                Toast.makeText(context, "callChatRoomGetApi fail: " + errorMsg, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Init chat if the session is active
        //
        checkLoginChatAndInit();
        callGetChatRoomApi();
    }

    private void checkLoginChatAndInit() {
        if (!QBChatService.getInstance().isLoggedIn()) {
            final DBLogInfo dbLogInfo = DatabaseManager.getInstance(this).getLogInfo();
            final QBUser qbUser = UserQBUser.getNewQBUser(this);
            chatSendButton.setEnabled(false);
            addPhotoBtn.setEnabled(false);
//            if (dbLogInfo == null || dbLogInfo.getQbUserId() == null) {
//                ChatService.initIfNeed(ChatActivity.this);
//                ChatService.getInstance().signUp(qbUser, new QBEntityCallbackImpl() {
//                    @Override
//                    public void onSuccess() {
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                UserQBUser.qbUser = qbUser;
//                                initChatAndGetDialogQBMsgsAndDbAction();
//                                firstCreateDialogForInviteToChat();
//                            }
//                        });
//
//                    }
//
//                    @Override
//                    public void onError(List errors) {
//                        Log.w(TAG, "signUp errors " + errors);
//                    }
//                });
//            } else {
            ChatService.initIfNeed(ChatActivity.this);
            ChatService.getInstance().login(qbUser, new QBEntityCallbackImpl() {
                @Override
                public void onSuccess() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            UserQBUser.qbUser = qbUser;
                            initChatAndGetDialogQBMsgsAndDbAction();
                            firstCreateDialogForInviteToChat();
                            Log.w(TAG, "login success " + qbUser.getId());
                        }
                    });
                }

                @Override
                public void onError(List errors) {
                    Log.w(TAG, "login errors " + errors);
                    checkLoggedInHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            checkLoginChatAndInit();
                        }
                    }, 500);
                }
            });
//            }
        } else {
            chatSendButton.setEnabled(true);
            addPhotoBtn.setEnabled(true);
            initChatAndGetDialogQBMsgsAndDbAction();
            firstCreateDialogForInviteToChat();

        }
    }

    private void firstCreateDialogForInviteToChat() {
        if (checkIsInvitedToChat() && qbDialog == null)
            getPrivateChatImpl().firstTimeCreateDialog(opponentQBID, null, ChatDbViewController.getInstance(this).INVITE_TO_CHAT);

        SharedPreferences.Editor editor = getSharedPreferences(Constants.PREF_BRANCH, MODE_PRIVATE).edit();
        editor.clear();
        editor.commit();

    }

    private boolean checkIsInvitedToChat() {
        SharedPreferences prefs = getSharedPreferences(Constants.PREF_BRANCH, MODE_PRIVATE);
        if (prefs == null)
            return false;
        String branchJson = prefs.getString(Constants.PREF_BRANCH_OBJECT_JSON, null);
        if (branchJson == null || branchJson.isEmpty())
            return false;
        BranchUniversalObject branchUniversalObject = new Gson().fromJson(branchJson, BranchUniversalObject.class);
        if (branchUniversalObject == null)
            return false;
        if (branchUniversalObject.getMetadata() == null)
            return false;
        if (branchUniversalObject.getMetadata().containsKey(Constants.BRANCH_ACTION)) {
            if (branchUniversalObject.getMetadata().get(Constants.BRANCH_ACTION).equalsIgnoreCase(Constants.BRANCH_ACTION_CHAT))
                return true;
        }
        return false;
    }


    @Override
    protected void onStop() {
        super.onStop();
        activityIsRunning = false;
        if (qbDialogId == null)
            return;
        DatabaseManager.getInstance(this).resetUnReadMsg(qbDialogId);
        stopRepeatingGetOpponentQBUserFromQB();
        DatabaseManager.getInstance(this).updateDialogLastMsgInfo(qbDialogId);
    }

    private void initOpponentQBUserView(QBUser opponentQBUser) {
        if (opponentQBUser == null)
            return;

        if (companionLabel.getText().toString() != null
                && !companionLabel.getText().toString().isEmpty()) {
            //// its already init user view
            return;
        }

//        QBUserCustomData customData = new Gson().fromJson(opponentQBUser.getCustomData(), QBUserCustomData.class);

        String memberName = "";
        String agentPhotoURL = "";
        if(CoreData.opponentAgentProfile != null){
            opponentAgentProfile = CoreData.opponentAgentProfile;
        }
        if(opponentAgentProfile != null){
            memberName = opponentAgentProfile.memberName;
            agentPhotoURL = opponentAgentProfile.agentPhotoURL;
        }else{
            memberName = opponentQBUser.getFullName();
            agentPhotoURL = opponentQBUser.getWebsite();
        }

        companionLabel.setText(memberName);
        Picasso.with(this).load(agentPhotoURL)
                .error(R.drawable.default_profile)
                .placeholder(R.drawable.default_profile)
                .transform(new RoundedTransformation()).into(chatImageView);

        drawChatImageViewBorder();

        if (opponentQBUser!=null)
        chatAdapter.setOpponentName(opponentQBUser.getFullName());
    }

    private void setChatImageViewAndEditBtnByStyleType(int styleType) {
        if (styleType == Constants.MEM_TYPE_CUSTOMER_SERVICE) {
            chatImageView.setImageBorder(Constants.STYLE_TYPE.CUSTOMER_SERVICE);
            chatEditBtn.setVisibility(View.GONE);
        } else if (styleType == Constants.MEM_TYPE_SUPER) {
            chatImageView.setImageBorder(Constants.STYLE_TYPE.ROBOT);
            chatEditBtn.setVisibility(View.GONE);
        } else {
            chatImageView.setImageBorder(Constants.STYLE_TYPE.AGENT_UNFOLLOWED);
        }
    }

    private void drawChatImageViewBorder() {
        if (isFollowing()) {
            chatImageView.setImageBorder(Constants.STYLE_TYPE.AGENT_FOLLOWED);
        } else {
            DBChatroomAgentProfiles dbChatroomAgentProfiles = DatabaseManager.getInstance(this).getChatroomAgentProfilesByMemId(opponentMemId);
            if (dbChatroomAgentProfiles == null)
                return;
            AgentProfiles agentProfile = new Gson().fromJson(dbChatroomAgentProfiles.getChatroomAgentProfiles(), AgentProfiles.class);
            if (agentProfile == null)
                return;
            setChatImageViewAndEditBtnByStyleType(agentProfile.memberType);
        }
    }

    private void initChatSettingView() {
        DBQBChatDialog dbqbChatDialog = DatabaseManager.getInstance(this).getQBChatDialogByDialogId(qbDialogId);
        if (dbqbChatDialog != null) {
            if (dbqbChatDialog.getMutedOpponent() == null || dbqbChatDialog.getMutedOpponent() == 0)
                muteTextView.setText(R.string.chatroom__no);
            else
                muteTextView.setText(R.string.chatroom__yes);

            if (dbqbChatDialog.getBlockedOpponent() == null || dbqbChatDialog.getBlockedOpponent() == 0)
                blockTextView.setText(R.string.chatroom__block_this_contact);
            else
                blockTextView.setText(R.string.chatroom__unblock_this_contact);

            if (dbqbChatDialog.getExited() == null || dbqbChatDialog.getExited() == 0)
                exitChatBtn.setText("exit chat");
            else
                exitChatBtn.setText("unexit chat");


            if (((dbqbChatDialog.getBlocked() != null)&&(dbqbChatDialog.getBlocked() == 1)) || ((dbqbChatDialog.getBlockedOpponent() != null)&&(dbqbChatDialog.getBlockedOpponent() == 1))) {
                ishaveBlocked = true;
            } else {
                ishaveBlocked = false;

            }

        }

    }


    private void initChatAndGetDialogQBMsgsAndDbAction() {
        fetchCurrentMsgs(true);
        fetchLastMsgDbActionAndSendReadToQb();
        fetchUnsentReadMsgDbActionAndToQb();
        fetchUnSentMsgDbActionAndToQb();
        if (NetworkUtil.isConnected(this)) {
            setPrivateChatImpl();

            Log.w(TAG, "qbPrivateChatManager = " + QBChatService.getInstance().getPrivateChatManager());
            chatSendButton.setEnabled(true);
            addPhotoBtn.setEnabled(true);
            Log.w(TAG, "get newest Dialog messages");
            if (qbDialog == null)
                return;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    getDialogMessagesFromQBAndDbAction(qbDialog);
                }
            });

            getOpponentQBUserFromQB();
            startRepeatingGetOpponentQBUserFromQB();
        } else {
            chatSendButton.setEnabled(false);
            addPhotoBtn.setEnabled(false);
        }
    }

    private void fetchCurrentMsgs(boolean needScrollDown) {

        if (qbDialog == null)
            return;
        if (qbDialog.getDialogId() == null)
            return;

        Log.v("","edwincc fetchCurrentMsgs currentListingId "+currentListingId+" qbDialog.getDialogId() "+qbDialog.getDialogId());
        List<DBQBChatMessage> dbqbChatMessageList = DatabaseManager.getInstance(this).listCurrentQbMessages("" + qbDialog.getDialogId(), currentListingId, currentListingDatabaseId);
        Log.i(TAG, "dbqbChatMessageList = " + dbqbChatMessageList.size());

        if (dbqbChatMessageList != null && dbqbChatMessageList.size() != 0)
            noOfMsgsAdded = dbqbChatMessageList.size() - currentMsgsNo;
        Log.v("","edwincc fetchCurrentMsgs dbqbChatMessageList.size() "+dbqbChatMessageList.size() +" currentMsgsNo "+currentMsgsNo +" noOfMsgsAdded "+ noOfMsgsAdded);
        if (noOfMsgsAdded > 0) {
            currentMsgsNo = dbqbChatMessageList.size();
        }

        checkEnableSwipeRefreshlayout();

        updateChatViewListener.updateChatView(dbqbChatMessageList, needScrollDown);
    }

    Runnable getOpponentUserChecker = new Runnable() {
        @Override
        public void run() {
            getOpponentQBUserFromQB();
            getOpponentUserHandler.postDelayed(getOpponentUserChecker, Constants.CHAT_GET_OPPONENT_USER_TIMER);
        }
    };

    private void startRepeatingGetOpponentQBUserFromQB() {
        getOpponentUserChecker.run();
    }

    private void stopRepeatingGetOpponentQBUserFromQB() {
        getOpponentUserHandler.removeCallbacks(getOpponentUserChecker);
    }

    private void getOpponentQBUserFromQB() {
        QBUsers.getUser(opponentQBID, new QBEntityCallback<QBUser>() {
            @Override
            public void onSuccess(final QBUser qbUser, Bundle bundle) {
                ChatActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        DBQBUser dbqbUser = DatabaseManager.getInstance(ChatActivity.this).getQBUserByUserId((long) opponentQBID);
                        if (dbqbUser != null) {
                            dbqbUser.setQBUser(new Gson().toJson(qbUser));
                            DatabaseManager.getInstance(ChatActivity.this).insertOrUpdateQBUser(dbqbUser);
                        }

                        opponentQBUser = qbUser;
                        initOpponentQBUserView(opponentQBUser);
                        String textContent = getOpponentOnlineStatus(qbUser);
                        updateChatViewListener.statusAndTypingTextViewOnChange(Color.WHITE, textContent);
                    }
                });

            }

            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(List<String> list) {
//                Toast.makeText(ChatActivity.this, "getUser error: " + list, Toast.LENGTH_LONG).show();
                Log.w(TAG, "getUser fail: " + list);
            }
        });

    }

    @OnClick(R.id.rlAgentListing)
    public void rlAgentListingClick(View view) {
        Log.w(TAG, "rlAgentListingClick getCurrentListingLineNo = " + getCurrentListingLineNo());
        msgListView.setSelection(getCurrentListingLineNo());
    }

    @OnClick(R.id.clearChatBtn)
    public void clearChatBtnClick(View view) {

        Dialog.clearChatDialog(this, new DialogCallback() {

            @Override
            public void yes() {

                DBQBUser dbqbUser = DatabaseManager.getInstance(context).getQBUserByMemberId(opponentMemId);
                if (dbqbUser != null) {
                    dbqbUser.setIsClear(true);
                    DatabaseManager.getInstance(context).insertOrUpdateQBUser(dbqbUser);
                }
                DatabaseManager.getInstance(context).markDeleteAllDialogMessages(qbDialogId);
                fetchCurrentMsgs(true);
            }
        }).show();
    }


    @OnClick(R.id.blockRelativeLayout)
    public void blockRelativeLayoutClick(View view) {
        //block user to QB
        final RequestChatRoomBlock chatRoomBlock = new RequestChatRoomBlock(userContent.memId, LocalStorageHelper.getAccessToken(getBaseContext()));
        chatRoomBlock.dialogId = qbDialogId;
        DBQBChatDialog dbqbChatDialog = DatabaseManager.getInstance(ChatActivity.this).getQBChatDialogByDialogId(qbDialogId);


        if (dbqbChatDialog.getBlockedOpponent() == null || dbqbChatDialog.getBlockedOpponent() == 0) {
            Dialog.blockChatDialog(this, opponentQBUser.getFullName(), new DialogCallback() {
                @Override
                public void yes() {
                    chatRoomBlock.blockStatus = 1;
                    blockSomeone(chatRoomBlock);
                }
            }).show();
        } else {
            Dialog.unblockChatDialog(this, opponentQBUser.getFullName(), new DialogCallback() {
                @Override
                public void yes() {
                    chatRoomBlock.blockStatus = 0;
                    blockSomeone(chatRoomBlock);
                }
            }).show();
        }

    }

    private void blockSomeone(final RequestChatRoomBlock chatRoomBlock ){
        chatRoomBlock.callChatRoomBlockApi(this, new ApiCallback() {
            @Override
            public void success(ApiResponse apiResponse) {
                // Mixpanel
                MixpanelAPI mixpanel = MixpanelAPI.getInstance(ChatActivity.this, AppConfig.getMixpanelToken());
                JSONObject properties = new JSONObject();
                try {
                    properties.put("Blocked member ID", opponentMemId);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mixpanel.track("Blocked Contact", properties);

                getPrivateChatImpl().sendSystemMsgSetting(opponentQBID, ChatDbViewController.SYSTEM_MSG_CHAT_SETTING);
                DBQBChatDialog dbqbChatDialog = DatabaseManager.getInstance(ChatActivity.this).getQBChatDialogByDialogId(qbDialogId);
                dbqbChatDialog.setBlockedOpponent(chatRoomBlock.blockStatus);
                DatabaseManager.getInstance(ChatActivity.this).updateQBChatDialog(dbqbChatDialog);
                initChatSettingView();
                UserQBUser.updateUserQBUser(ChatActivity.this);
            }

            @Override
            public void failure(String errorMsg) {
                if (Constants.isDevelopment)
                    Toast.makeText(ChatActivity.this, "callChatRoomBlockApi fail: " + errorMsg, Toast.LENGTH_LONG).show();
            }
        });
    }

    @OnClick(R.id.muteRelativeLayout)
    public void muteRelativeLayoutClick(View view) {
        final RequestChatRoomMute chatRoomMute = new RequestChatRoomMute(userContent.memId, LocalStorageHelper.getAccessToken(getBaseContext()));
        chatRoomMute.dialogId = qbDialogId;
        DBQBChatDialog dbqbChatDialog = DatabaseManager.getInstance(ChatActivity.this).getQBChatDialogByDialogId(qbDialogId);
        if (dbqbChatDialog.getMutedOpponent() == null || dbqbChatDialog.getMutedOpponent() == 0) {
            chatRoomMute.muteStatus = 1;
        } else {
            chatRoomMute.muteStatus = 0;
        }
        chatRoomMute.callChatRoomMuteApi(this, new ApiCallback() {
            @Override
            public void success(ApiResponse apiResponse) {
                // Mixpanel
                MixpanelAPI mixpanel = MixpanelAPI.getInstance(ChatActivity.this, AppConfig.getMixpanelToken());
                JSONObject properties = new JSONObject();
                try {
                    if (chatRoomMute.muteStatus == 0) {
                        properties.put("Muted", "NO");
                    } else {
                        properties.put("Muted", "YES");
                    }
                    properties.put("Muted member ID", opponentMemId);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mixpanel.track("Muted Chat", properties);

                getPrivateChatImpl().sendSystemMsgSetting(opponentQBID, ChatDbViewController.SYSTEM_MSG_CHAT_SETTING);
                DBQBChatDialog dbqbChatDialog = DatabaseManager.getInstance(ChatActivity.this).getQBChatDialogByDialogId(qbDialogId);
                dbqbChatDialog.setMutedOpponent(chatRoomMute.muteStatus);
                DatabaseManager.getInstance(ChatActivity.this).updateQBChatDialog(dbqbChatDialog);

                initChatSettingView();
                UserQBUser.updateUserQBUser(ChatActivity.this);
            }

            @Override
            public void failure(String errorMsg) {
                if (Constants.isDevelopment)
                Toast.makeText(ChatActivity.this, "callChatRoomMuteApi fail: " + errorMsg, Toast.LENGTH_LONG).show();
            }
        });
    }


    @OnClick(R.id.exitChatBtn)
    public void exitChatBtnClick(View view) {
        final RequestChatRoomExit chatRoomExit = new RequestChatRoomExit(userContent.memId, LocalStorageHelper.getAccessToken(getBaseContext()));
        chatRoomExit.dialogId = qbDialogId;
        DBQBChatDialog dbqbChatDialog = DatabaseManager.getInstance(this).getQBChatDialogByDialogId(qbDialogId);
        if (dbqbChatDialog.getExited() == null || dbqbChatDialog.getExited() == 0) {
            chatRoomExit.exitStatus = 1;
        } else {
            chatRoomExit.exitStatus = 0;
        }

        chatRoomExit.callChatRoomExitApi(this, new ApiCallback() {
            @Override
            public void success(ApiResponse apiResponse) {
                getPrivateChatImpl().sendSystemMsgSetting(opponentQBID, ChatDbViewController.SYSTEM_MSG_CHAT_SETTING);
                DBQBChatDialog dbqbChatDialog = DatabaseManager.getInstance(ChatActivity.this).getQBChatDialogByDialogId(qbDialogId);
                dbqbChatDialog.setExited(chatRoomExit.exitStatus);
                DatabaseManager.getInstance(ChatActivity.this).updateQBChatDialog(dbqbChatDialog);
                initChatSettingView();

            }

            @Override
            public void failure(String errorMsg) {
                if (Constants.isDevelopment)
                Toast.makeText(ChatActivity.this, "callChatRoomExitApi fail: " + errorMsg, Toast.LENGTH_LONG).show();
            }
        });
    }


    @OnClick(R.id.maskView)
    public void maskViewClick(View view) {
        ImageUtil.collapse(chatEditLinearLayout);
        maskView.setVisibility(View.GONE);
    }

    @OnClick(R.id.chatEditBtn)
    public void chatEditBtnClick(View view) {
        if (qbDialog == null)
            return;
        if (chatEditLinearLayout.getVisibility() == View.GONE) {
            ImageUtil.expand(chatEditLinearLayout);
            maskView.setVisibility(View.VISIBLE);
            ImageUtil.hideKeyboard(this, view);
        } else {
            ImageUtil.collapse(chatEditLinearLayout);
            maskView.setVisibility(View.GONE);
        }

    }

    private boolean textIsEmpty(String msg) {
        return msg.trim().length() == 0;
    }

    @OnClick(R.id.chatSendButton)
    public void chatSendBtnClick(View view) {
        String msgText = messageEditText.getText().toString();
        if (textIsEmpty(msgText))
            return;
        DBQBChatDialog dbqbChatDialog = DatabaseManager.getInstance(ChatActivity.this).getQBChatDialogByDialogId(qbDialogId);
        if (dbqbChatDialog != null &&
                dbqbChatDialog.getBlockedOpponent() != null) {
            if (dbqbChatDialog.getBlockedOpponent() == 1) {
                        blockRelativeLayout.performClick();
                return;
            }
        }
        sendMsgStart(msgText, ChatDbViewController.TEXT);
        messageEditText.setText("");

        if (qbDialogId != null) {
            DatabaseManager.getInstance(this).resetUnReadMsg(qbDialogId);
        }

        DBQBUser dbqbUser = DatabaseManager.getInstance(context).getQBUserByMemberId(opponentMemId);
        if (dbqbUser != null) {
            //reset Clear user
            boolean needUpdate = false;
            if (dbqbUser.getIsClear() != null && dbqbUser.getIsClear()) {
                dbqbUser.setIsClear(false);
                needUpdate = true;
            }
            //reset Hidden user
            if (dbqbUser.getIsHidden() != null && dbqbUser.getIsHidden()) {
                dbqbUser.setIsHidden(false);
                needUpdate = true;
            }

            if (needUpdate) {
                DatabaseManager.getInstance(context).insertOrUpdateQBUser(dbqbUser);
            }
        }
        chatAdapter.notifyDataSetChanged();
    }


    @OnClick(R.id.addPhotoBtn)
    public void addPhotoBtnClick(View view) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    PermissionUtil.MY_PERMISSIONS_REQUEST_EXTERNAL_STORAGE);
            return;
        }

        if (isBlockedOpponent()) {
            Dialog.unblockChatDialog(this, opponentQBUser.getFullName(), new DialogCallback() {
                @Override
                public void yes() {
                    blockRelativeLayout.performClick();
                }
            }).show();
            return;
        }
        PhotoPickerIntent intent = new PhotoPickerIntent(ChatActivity.this);
        intent.setPhotoCount(Constants.MAX_SELECT_PHOTO_ON_CHATROOM);
        startActivityForResult(intent, SELECT_PHOTO);
//        isToPhotoPicker = true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        chatAdapter.clear();
        chatAdapter = null;
        CoreData.opponentAgentProfile = null;
//        resetLimitMsgNo();
    }


    @Override
    public void onBackPressed() {
        DBUtil.copyDB(this);
        if (getPrivateChatImpl() != null)
            getPrivateChatImpl().release();
        //to check if it is root
        if (!hasHistory) {
            Intent intent = new Intent(this, MainActivityTabBase.class);
            intent.putExtra(Constants.EXTRA_SELECT_TAB, Constants.SELECT_CHAT);

            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        List<String> photos = null;
        try {
            if (resultCode == RESULT_OK && requestCode == SELECT_PHOTO) {
                if (data != null)
                    photos = data.getStringArrayListExtra(PhotoPickerActivity.KEY_SELECTED_PHOTOS);
                if (photos != null) {
                    final List<String> photosList = photos;
                    if (qbDialog == null) {
                        if(PrivateChatImpl.getInstance(this).getPrivateChatManager() == null){
                            return;
                        }
                        PrivateChatImpl.getInstance(this).getPrivateChatManager().createDialog(opponentQBID, new QBEntityCallback<QBDialog>() {
                            @Override
                            public void onSuccess(QBDialog qbDialog, Bundle bundle) {
                                Log.i("","edwinzzz 1");
                                ChatActivity.this.qbDialog = qbDialog;
                                qbDialogId = qbDialog.getDialogId();
                                chatAdapter.setDialogId(qbDialogId);
                                DatabaseManager.getInstance(context).insertOrUpdateQBChatDialog(qbDialog);
                                ChatService.getInstance().getDialogById(qbDialogId, new BaseService.BaseServiceCallBack() {
                                    @Override
                                    public void onSuccess(Object var1) {

                                    }

                                    @Override
                                    public void onError(BaseService.RealServiceError serviceError) {

                                    }
                                });

                                Log.i("","edwinzzz 2");
                                sendPhotos(photosList);
                                Log.i("","edwinzzz 3");
                            }

                            @Override
                            public void onSuccess() {

                            }

                            @Override
                            public void onError(List<String> list) {
                                Log.e(TAG, "firstTimeCreateDialog on error: " + list);
                            }
                        });
                    } else {
                        sendPhotos(photosList);
                    }
                }
            }
        } catch (Exception e) {
            Toast.makeText(this, R.string.chatroom_error_message__incompatible_error, Toast.LENGTH_SHORT).show(); //TODO: general error for some not supporting image
        }
    }

    void sendPhotos(List<String> photos){
        for (int i = 0; i < photos.size(); i++) {
            try {
                Log.w("","edwinzzz +photos "+i);
                sendMsgStart(photos.get(i), ChatDbViewController.PIC);
            } catch (Exception e) {
                Log.e("","edwinzzz photos e.printStackTrace() ");
                e.printStackTrace();
                Toast.makeText(this, R.string.chatroom_error_message__incompatible_error, Toast.LENGTH_SHORT).show(); //TODO: general error for some not supporting image
            }
        }
    }

    //////////////////////////////////////////////////////////////////////////////////// get opponent online status  ////////////////////////////////////////////////////////////////////////////////////
    private String getOpponentOnlineStatus(QBUser qbUser) {

        String textContent = "";
        long currentTime = System.currentTimeMillis();
        long userLastRequestAtTime = qbUser.getLastRequestAt().getTime();
        Log.w("ChatSessionUtil", "chat online diff: " + (currentTime - userLastRequestAtTime));
        if ((currentTime - userLastRequestAtTime) < Constants.CHAT_ONLINE_STATUS_TOLERANCE) {
            textContent = getString(R.string.chatroom__online);
        } else {
            if (!isDisplayStatusAndTyping()) {
                textContent = "";
            } else {
                // user is offline now

                textContent = "" + TimeUtils.getRelativeTimeSpan(userLastRequestAtTime, currentTime, ChatActivity.this, true);
                Log.w("Timeutils", "dateTime qbUser.getLastRequestAt() = " + qbUser.getLastRequestAt());
            }
        }

        return textContent;
    }
    //////////////////////////////////////////////////////////////////////////////////// get opponent online status  ////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////listView ////////////////////////////////////////////////////////////////////////////////////
    private void keepListViewPosition() {
        msgListView.post(new Runnable() {
            @Override
            public void run() {
                Log.i(TAG, "noOfMsgsAdded: " + noOfMsgsAdded);
                msgListView.setSelection(noOfMsgsAdded - 1);
            }
        });
    }

    private void scrolldownListView() {
        msgListView.post(new Runnable() {
            @Override
            public void run() {
                msgListView.setSelection(msgListView.getCount() - 1);
            }
        });
    }

    //    private void addLimitMsgNo() {
//        if (qbDialog == null)
//            return;
//        if (qbDialog.getDialogId() == null)
//            return;
//        int maxLimitMsgNo = DatabaseManager.getInstance(context).listQbMessageByDialogId(qbDialog.getDialogId()).size() / showMsgNo;
//        if (limitMsgNo <= maxLimitMsgNo) {
//            limitMsgNo++;
//            if (limitMsgNo >= maxLimitMsgNo)
//                setListShowTop(true);
//        }
//    }
//
//    private boolean isListShowTop() {
//        Log.w(TAG, "limitMsgNo = " + limitMsgNo);
//        if (limitMsgNo == 1) {
//            return false;
//        }
//        return listShowTop;
//    }
//
//    private boolean listIsAtTop() {
//        if (msgListView.getChildCount() == 0) return true;
//        return msgListView.getChildAt(0).getTop() == 0;
//    }
//
//    private void setListShowTop(boolean listShowTop) {
//        this.listShowTop = listShowTop;
//    }
//
//    private void resetLimitMsgNo() {
//        limitMsgNo = 1;
//        setListShowTop(false);
//    }
    ////////////////////////////////////////////////////////////////////////////////////listView ////////////////////////////////////////////////////////////////////////////////////


    ////////////////////////////////// insert dialog and user to db//////////////////////////////////
    private void insertDialogAndUserToDB(QBDialog qbDialog, String qbDialogId, int opponentQBID, QBUser qbUser, int opponentMemId) {
        insertDialogToDB(qbDialog, qbDialogId, opponentQBID, opponentMemId);
        insertUserToDB(qbUser);
    }

    private void insertUserToDB(QBUser qbUser) {
        DBQBUser dbqbUser = new DBQBUser();
        dbqbUser.setQBUser(new Gson().toJson(qbUser));
        dbqbUser.setUserID(qbUser.getId());
        dbqbUser.setMemberID(opponentMemId);
        dbqbUser.setName(qbUser.getFullName());
        DatabaseManager.getInstance(this).insertOrUpdateQBUser(dbqbUser);
    }

    private void insertDialogToDB(QBDialog qbDialog, String qbDialogId, int opponentQBID, int opponentMemId) {
        DBQBChatDialog dbqbChatDialog = new DBQBChatDialog();
        dbqbChatDialog.setDialogID(qbDialogId);
        dbqbChatDialog.setQbUserID(opponentQBID);
        dbqbChatDialog.setQBChatDialog(new Gson().toJson(qbDialog));
        dbqbChatDialog.setExited(0);
        dbqbChatDialog.setExitedOpponent(0);
        dbqbChatDialog.setDeleted(0);
        dbqbChatDialog.setDeletedOpponent(0);
        dbqbChatDialog.setBlocked(0);
        dbqbChatDialog.setBlockedOpponent(0);
        dbqbChatDialog.setMuted(0);
        dbqbChatDialog.setMutedOpponent(0);
//        DatabaseManager.getInstance(this).dbActionQBChatDialog(dbqbChatDialog);
        DatabaseManager.getInstance(this).insertOrUpdateQBChatDialog(dbqbChatDialog);
    }
    ////////////////////////////////// insert dialog and user to db//////////////////////////////////


    private boolean chatNeedInit() {
        if (qbDialogId == null || qbDialogId.isEmpty() ||
                DatabaseManager.getInstance(this).getQBChatDialogByDialogId(qbDialogId) == null ||
                DatabaseManager.getInstance(this).getQBChatDialogByDialogId(qbDialogId).getNeedInit() == null ||
                !DatabaseManager.getInstance(this).getQBChatDialogByDialogId(qbDialogId).getNeedInit())
            return false;
        return true;
    }

    //////////////////////////////////get msgs from QB then insert or update in db////////////////////////////////////
    //insert DB from getting dialog new messages
    private void getDialogMessagesFromQBAndDbAction(QBDialog dialog) {
        QBRequestGetBuilder customObjectRequestBuilder = new QBRequestGetBuilder();
        final boolean chatNeedInit = chatNeedInit();
        customObjectRequestBuilder.setPagesLimit(100);
        if (qbDialogId == null)
            return;

        if (DatabaseManager.getInstance(this).getLastestQBChatMessage(qbDialogId) != null &&
                !chatNeedInit) {
            //have message before and don't need to init
            customObjectRequestBuilder.gte("date_sent", DatabaseManager.getInstance(this).getLastestQBChatMessage(qbDialogId).getDateSent());
        }
        customObjectRequestBuilder.sortAsc("_id");
        QBChatService.getDialogMessages(dialog, customObjectRequestBuilder,
                new QBEntityCallbackImpl<ArrayList<QBChatMessage>>() {
                    @Override
                    public void onSuccess(final ArrayList<QBChatMessage> messages,
                                          Bundle args) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Gson gson = new Gson();
                                if (chatNeedInit)
                                    DatabaseManager.getInstance(ChatActivity.this).deleteAllDialogMessages(qbDialogId);

                                for (int i = 0; i < messages.size(); i++) {
                                    String chatMessageId = messages.get(i).getId();
                                    Long updateMsgId = DatabaseManager.getInstance(ChatActivity.this).haveRecordQBChatMessage(chatMessageId);

                                    if (updateMsgId != -1) {
                                        //update msg
                                        DBQBChatMessage dbqbChatMessage = DatabaseManager.getInstance(ChatActivity.this).getQBChatMessageById(updateMsgId);
                                        dbqbChatMessage = DatabaseManager.getInstance(ChatActivity.this).chatMessageToDbQbChatMessage(dbqbChatMessage, messages.get(i), gson);
                                        dbqbChatMessage.setIsEncrypted(false);
                                        DatabaseManager.getInstance(ChatActivity.this).insertOrUpdateMsgToDb(dbqbChatMessage, dbqbChatMessage.getMessageID());
                                    } else {
                                        //insert
                                        DatabaseManager.getInstance(ChatActivity.this).insertOrUpdateMsgToDb(
                                                DatabaseManager.getInstance(ChatActivity.this).chatMessageToDbQbChatMessage(messages.get(i), gson), messages.get(i).getId());
                                    }

                                }
                                DatabaseManager.getInstance(ChatActivity.this).updateDialogNeedInit(qbDialogId, false);

                                fetchCurrentMsgs(true);
                                fetchLastMsgDbActionAndSendReadToQb();
                                fetchUnsentReadMsgDbActionAndToQb();
                                fetchUnSentMsgDbActionAndToQb();

                            }
                        });
                        Log.w(TAG, "getDialogMessages success: " + messages);

                    }

                    @Override
                    public void onError(final List<String> errors) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (Constants.isDevelopment)
                                Toast.makeText(context, "load chat history errors: " + errors, Toast.LENGTH_SHORT).show();
                            }
                        });

                    }
                });
    }


    ////////////////////////////////////get msgs from QB then insert or update in db////////////////////////////////////

    //////////resend all unread or unSentUser and to QB//////////
    //////////resend unread //////////
    private void fetchUnsentReadMsgDbActionAndToQb() {
        if(qbDialogId!=null) {
            List<DBQBChatMessage> dbqbChatMessages = DatabaseManager.getInstance(this).listUnsentReadQBChatMessageByDialogId(qbDialogId);
            Log.w(TAG, "fetchUnsentReadMsgDbActionAndToQb size = " + dbqbChatMessages.size());
            for (int i = 0; i < dbqbChatMessages.size(); i++) {
                sendReadMessageStatusAndDbAction(new Gson().fromJson(dbqbChatMessages.get(i).getQBChatMessage(), QBChatMessage.class));
            }
        }
    }

    private void fetchLastMsgDbActionAndSendReadToQb() {
        if(qbDialogId!=null) {
            DBQBChatMessage dbqbChatMessage = DatabaseManager.getInstance(this).getLastestOpponentQBChatMessage(qbDialogId, opponentQBID);
            Log.w(TAG, "dbqbChatMessage = " + dbqbChatMessage);
            if (dbqbChatMessage != null)
                sendReadMessageStatusAndDbAction(new Gson().fromJson(dbqbChatMessage.getQBChatMessage(), QBChatMessage.class));
        }

    }

    //////////resend unSentUser //////////
    private void fetchUnSentMsgDbActionAndToQb() {

        if(qbDialogId!=null) {
            List<DBQBChatMessage> dbqbChatMessages = DatabaseManager.getInstance(this).listUnSentUserQBChatMessageByDialogId(qbDialogId);
            Log.w(TAG, "listUnSentUserQBChatMessageByDialogId = " + dbqbChatMessages.size());
            Gson gson = new Gson();
            for (int i = 0; i < dbqbChatMessages.size(); i++) {
                QBChatMessage qbChatMessage = gson.fromJson(dbqbChatMessages.get(i).getQBChatMessage(), QBChatMessage.class);
                Log.w(TAG, "fetchUnSentMsgDbActionAndToQb sendMessageToQbAndDbAction");
                sendMessageToQbAndDbAction(qbChatMessage);
            }
        }
    }
    //////////resend all unread or unSentUser and to QB//////////

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////msgs status ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    public void sendReadMessageStatusAndDbAction(QBChatMessage chatMessage) {
        Log.w(TAG, "sendReadMessageStatusAndDbAction chatMessage = " + chatMessage);

        try {
            if (getPrivateChatImpl() == null) {
                setPrivateChatImpl();
                sendReadMessageStatusAndDbAction(chatMessage);
            } else {
                if (getPrivateChatImpl() == null)
                    return;
                getPrivateChatImpl().readMessage(chatMessage);
                if (QBChatService.getInstance().isLoggedIn()) {
                    String dialogId = DatabaseManager.getInstance(ChatActivity.this).getDialogIdByMsgId(chatMessage.getId());
                    if (dialogId == null)
                        return;
                    QBChatService.getInstance().markMessagesAsRead(dialogId, null, new QBEntityCallbackImpl<Void>() {
                        @Override
                        public void onSuccess() {
                            Log.w(TAG, "markMessagesAsRead success ");
                        }

                        @Override
                        public void onError(List errors) {
                            Log.w(TAG, "markMessagesAsRead errors " + errors);
                        }
                    });
                }
            }

            if(qbDialogId!=null) {
                //set unread before msg to read
                List<DBQBChatMessage> unreadDbQbChatMsgList = DatabaseManager.getInstance(this).listUnsentReadQBChatMessageByDialogId(qbDialogId);
                if (unreadDbQbChatMsgList != null && unreadDbQbChatMsgList.size() > 0) {
                    for (DBQBChatMessage dbqbChatMessage : unreadDbQbChatMsgList) {
                        long dateSent = chatMessage.getDateSent();
                        if (chatMessage.getDateSent() == 0) {
                            dateSent = Double.valueOf(chatMessage.getProperty(ChatDbViewController.PROPERTY_DATE_SENT).toString()).longValue();
                        }
                        Log.w(TAG, "dateSent = " + dateSent);
                        Log.w(TAG, "dateSent dbqbChatMessage.getDateSent() = " + dbqbChatMessage.getDateSent());
                        if (dateSent <= Long.parseLong(dbqbChatMessage.getDateSent()))
                            DatabaseManager.getInstance(this).updateMsgStatusDbAction(dbqbChatMessage.getMessageID(), ChatDbViewController.SENT_READ);
                    }
                }
            }
            fetchCurrentMsgs(true);
        } catch (XMPPException e) {
            e.printStackTrace();
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////msgs status ////////////////////////////////////////.////////////////////////////////////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// PrivateChatImpl////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public void setPrivateChatImpl() {
        PrivateChatImpl.getInstance(this).initPrivateChatIfnNeed(opponentQBID);
    }

    public PrivateChatImpl getPrivateChatImpl() {
        return PrivateChatImpl.getInstance(this);
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// PrivateChatImpl////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////new or receive msgs////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //insert Photo msg into DB and to QB
    private void addPhotoMsgDbActionAndToQb(QBChatMessage chatMessage, File fileInDevice, String status) {
        Gson gson = new Gson();
        DBQBChatMessage dbqbChatMessage = DatabaseManager.getInstance(ChatActivity.this).chatMessageToDbQbChatMessage(chatMessage, gson);
        if (dbqbChatMessage.getMessageType().equalsIgnoreCase(ChatDbViewController.SEND))
            dbqbChatMessage.setSendStatus(status);
        else
            dbqbChatMessage.setReceiveStatus(status);

        dbqbChatMessage.setPhotoLocalPath(fileInDevice.getAbsolutePath());
        dbqbChatMessage.setPhotoName(fileInDevice.getName());
        DatabaseManager.getInstance(this).insertOrUpdateMsgToDb(dbqbChatMessage, chatMessage.getId());
        fetchCurrentMsgs(true);
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////new or receive msgs////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////privacy Read and status////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    public boolean isDisplayStatusAndTyping() {
        Log.w(TAG, "opponentQBUser = " + opponentQBUser);
        Log.w(TAG, "getCustomData() = " + opponentQBUser.getCustomData());
        if(opponentAgentProfile!=null && opponentAgentProfile.memberType == 5){
            return false;
        }
        if (opponentQBUser.getCustomData() != null) {
            QBUserCustomData qbUserCustomData = new Gson().fromJson(opponentQBUser.getCustomData(), QBUserCustomData.class);
            if (!qbUserCustomData.chatRoomShowLastSeen) {
                return false;
            }
            if (DatabaseManager.getInstance(this).getLogInfo() != null &&
                    DatabaseManager.getInstance(this).getLogInfo().getEnableStatus() != null) {
                if (DatabaseManager.getInstance(this).getLogInfo().getEnableStatus() == false)
                    return false;
            }
        }
        return true;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////privacy Read and status////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////send message////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private void sendMsgStart(String msgTextOrPhotoPath, int type) {
        Log.v("","edwinzz sendMsgStart msgTextOrPhotoPath "+msgTextOrPhotoPath+" type "+type);
        // Mixpanel
        MixpanelAPI mixpanel = MixpanelAPI.getInstance(this, AppConfig.getMixpanelToken());

        JSONObject properties = new JSONObject();
        try {
            properties.put("Receiver member ID", opponentMemId);
            properties.put("Receiver name", opponentAgentProfile.memberName);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mixpanel.track("Sent Chat", properties);

        properties = new JSONObject();
        try {
            properties.put("Sender member ID", userContent.memId);
            properties.put("distinct_id", opponentMemId);
            properties.put("Sender name", userContent.memName);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mixpanel.track("Received Chat", properties);

        if (qbDialog == null) {
            getPrivateChatImpl().firstTimeCreateDialog(opponentQBID, msgTextOrPhotoPath, type);
        } else {
            ////////////////////////////////////////////////////////////////////agent listing message if need////////////////////////////////////////////////////////////////////
            if (!isFormLobby && bindingAgentListingId != -1) {
                String lastListingId = DatabaseManager.getInstance(this).getChatroomLastListingId(qbDialog.getDialogId());
                if (lastListingId == null || (lastListingId != null && bindingAgentListingId != Integer.parseInt(lastListingId))) {
                    QBChatMessage agentListingChatMessage = new QBChatMessage();
                    agentListingChatMessage.setProperty(ChatDbViewController.PROPERTY_SAVE_TO_HISTORY, "1");
                    agentListingChatMessage.setDialogId(qbDialog.getDialogId());
                    agentListingChatMessage.setSenderId(Integer.parseInt(userContent.qbid));
                    agentListingChatMessage.setRecipientId(opponentQBID);
                    agentListingChatMessage.setBody("");
                    agentListingChatMessage.setMarkable(true);
                    agentListingChatMessage.setDateSent(System.currentTimeMillis()/1000l);
                    if (userContent != null && userContent.memName != null)
                        agentListingChatMessage.setProperty(ChatDbViewController.PROPERTY_SENDER_NAME, userContent.memName);

                    agentListingChatMessage.setProperty(ChatDbViewController.PROPERTY_MSG_TYPE, ChatDbViewController.MSG_TYPE_AGENTLISTING);
                    agentListingChatMessage.setProperty(ChatDbViewController.PROPERTY_BINDING_AGENT_LISTING_ID, "" + bindingAgentListingId);
                    DatabaseManager.getInstance(this).addUpdateMsgDbAction(opponentQBUser, agentListingChatMessage, ChatDbViewController.NEW);
                    sendMessageToQbAndDbAction(agentListingChatMessage);
                }
            }

            QBChatMessage chatMessage = new QBChatMessage();
//            if (dbqbChatDialog.getMutedOpponent() == null || dbqbChatDialog.getMutedOpponent() == 0)
            chatMessage.setProperty(ChatDbViewController.PROPERTY_SAVE_TO_HISTORY, "1");
            chatMessage.setDialogId(qbDialog.getDialogId());
            chatMessage.setSenderId(Integer.parseInt(userContent.qbid));
            chatMessage.setRecipientId(opponentQBID);
            chatMessage.setMarkable(true);
            chatMessage.setDateSent(System.currentTimeMillis()/1000l);
            if (bindingAgentListingId != -1)
                chatMessage.setProperty(ChatDbViewController.PROPERTY_BINDING_AGENT_LISTING_ID, "" + bindingAgentListingId);
            else
                chatMessage.setProperty(ChatDbViewController.PROPERTY_BINDING_AGENT_LISTING_ID, "" + DatabaseManager.getInstance(this).getChatroomLastListingId(qbDialogId));
            if (userContent != null && userContent.memName != null)
                chatMessage.setProperty(ChatDbViewController.PROPERTY_SENDER_NAME, userContent.memName);

            if (type == ChatDbViewController.TEXT) {
                chatMessage.setProperty(ChatDbViewController.PROPERTY_MSG_TYPE, ChatDbViewController.MSG_TYPE_TEXT);
                chatMessage.setBody(msgTextOrPhotoPath);
                if (isBlocked()) {
                    DatabaseManager.getInstance(this).addUpdateMsgDbAction(opponentQBUser, chatMessage, ChatDbViewController.BLOCKED);
                    fetchCurrentMsgs(true);
                } else {
                    DatabaseManager.getInstance(this).addUpdateMsgDbAction(opponentQBUser, chatMessage, ChatDbViewController.NEW);
                    fetchCurrentMsgs(true);
                    sendMessageToQbAndDbAction(chatMessage);
                }
            } else if (type == ChatDbViewController.PIC) {
                chatMessage.setProperty(ChatDbViewController.PROPERTY_MSG_TYPE, ChatDbViewController.MSG_TYPE_IMAGE);
                chatMessage.setBody("image");
                sendPhotosToQBAndDbAction(msgTextOrPhotoPath, chatMessage);
            }
        }
    }

    private boolean isBlocked() {
        DBQBChatDialog dbqbChatDialog = DatabaseManager.getInstance(ChatActivity.this).getQBChatDialogByDialogId(qbDialogId);
        if (dbqbChatDialog == null || dbqbChatDialog.getBlocked() == null)
            return false;

        return dbqbChatDialog.getBlocked() == 1 ? true : false;
    }

    private boolean isBlockedOpponent() {
        DBQBChatDialog dbqbChatDialog = DatabaseManager.getInstance(ChatActivity.this).getQBChatDialogByDialogId(qbDialogId);
        if (dbqbChatDialog == null || dbqbChatDialog.getBlockedOpponent() == null)
            return false;
        return dbqbChatDialog.getBlockedOpponent() == 1 ? true : false;
    }

    //////////send msg//////////
    private void sendMessageToQbAndDbAction(QBChatMessage chatMessage) {
        try {
            Log.w(TAG, "getPrivateChatImpl = " + getPrivateChatImpl());
            Log.w(TAG, "chatMessage = " + chatMessage);
            if (getPrivateChatImpl() == null) {
                setPrivateChatImpl();
                sendMessageToQbAndDbAction(chatMessage);
            } else {
                getPrivateChatImpl().sendMessage(chatMessage);
                DatabaseManager.getInstance(this).addUpdateMsgDbAction(opponentQBUser, chatMessage, ChatDbViewController.SENT);
                fetchCurrentMsgs(true);
            }

        } catch (XMPPException e) {
            e.printStackTrace();
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
        }

    }

    //////////send msg//////////
    //////////send photo//////////
    public void sendPhotosToQBAndDbAction(String photo, QBChatMessage chatMessage) {
        String filePath = photo;
        File filePhoto = new File(filePath);
        Bitmap resizeBitmap = ImageUtil.resizeBitmap(FileUtil.fileToBitmap(filePhoto));
        File fileInDevice = new File(FileUtil.getAppSubFolder(Constants.SENT_FOLDER_NAME),
                chatMessage.getDialogId() + "-" + chatMessage.getId() + ".jpg");
        try {
//            FileUtil.convertBitmapToFile(fileInDevice, resizeBitmap);
            ImageUtil.getCompressImageFilePath(filePath, fileInDevice.getAbsolutePath(), ImageUtil.normalImageType);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //if blocked, don't send to QB
        if (isBlocked()) {
            addPhotoMsgDbActionAndToQb(chatMessage, fileInDevice, ChatDbViewController.BLOCKED);
            fetchCurrentMsgs(true);
            return;
        }
        //display local photo in list
        addPhotoMsgDbActionAndToQb(chatMessage, fileInDevice, ChatDbViewController.CREATE_PHOTO);

        Log.w(TAG, "enter DB Photo");

        File fileForUpload = null;
        try {
            Log.w(TAG, "filePhoto.getName() = " + fileInDevice.getName());
            Log.w(TAG, "filePhoto.getParent() = " + fileInDevice.getParent());
            InputStream is = new FileInputStream(fileInDevice);
            fileForUpload = FileHelper.getFileInputStream(is, fileInDevice.getName(), null);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Boolean fileIsPublic = true;
        QBContent.uploadFileTask(fileForUpload, fileIsPublic, null, new QBEntityCallbackImpl<QBFile>() {
            @Override
            public void onSuccess(QBFile file, Bundle params) {
                Log.w(TAG, "file.getName() = " + file.getName());
                Log.w(TAG, "file.getPublicUrl() = " + file.getPublicUrl());
                Gson gson = new Gson();
                //update chatMessage photoURL
                DBQBChatMessage dbqbChatMessage = DatabaseManager.getInstance(ChatActivity.this).getQBChatMessageByPhotoName(file.getName());
                dbqbChatMessage.setPhotoURL(file.getPublicUrl());
                QBChatMessage chatMessage = gson.fromJson(dbqbChatMessage.getQBChatMessage(), QBChatMessage.class);
                chatMessage.setProperty(ChatDbViewController.PROPERTY_PHOTO_URL, file.getPublicUrl());
                if (!DatabaseManager.getInstance(ChatActivity.this).isSetEnableReadMsgTick(opponentQBUser))
                    chatMessage.setProperty(ChatDbViewController.PROPERTY_ENABLE_READ_TICK, "false");
                dbqbChatMessage.setQBChatMessage(gson.toJson(chatMessage));
                dbqbChatMessage.setSendStatus(ChatDbViewController.NEW);
                DatabaseManager.getInstance(ChatActivity.this).insertOrUpdateMsgToDb(dbqbChatMessage, dbqbChatMessage.getMessageID());
                // send a message
                sendMessageToQbAndDbAction(chatMessage);

                if (Constants.isDevelopment)
                Toast.makeText(context, "upload file task onSuccess ", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(List<String> errors) {
                // error
                if (Constants.isDevelopment)
                Toast.makeText(context, "upload file task error: " + errors, Toast.LENGTH_SHORT).show();
                Log.e(TAG, "upload file task error: " + errors);
            }
        });

    }
    //////////send photo//////////

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////send message////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //////////////////////////download image to disk//////////////////////////
    private void downloadImageToDisk(final QBChatMessage chatMessage) {
        String photoUrl = (String) chatMessage.getProperty(ChatDbViewController.PROPERTY_PHOTO_URL);
        final String photoName = chatMessage.getDialogId() + "-" + chatMessage.getId() + ".jpg";
        final File fileInDevice = new File(FileUtil.getAppSubFolder(Constants.RECEIVE_FOLDER_NAME),
                photoName);
        Target downloadImageTarget = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                try {
                    Log.w(TAG, "onBitmapLoaded chatMessage: " + new Gson().toJson(chatMessage));
                    FileUtil.convertBitmapToFile(fileInDevice, bitmap);
                    DBQBChatMessage dbqbChatMessage = DatabaseManager.getInstance(ChatActivity.this).getQBChatMessageByMessageId(chatMessage.getId());
                    dbqbChatMessage.setPhotoLocalPath(fileInDevice.getAbsolutePath());
                    dbqbChatMessage.setPhotoName(photoName);
                    DatabaseManager.getInstance(ChatActivity.this).updateQBChatMessage(dbqbChatMessage);
                    fetchCurrentMsgs(false);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
                if (Constants.isDevelopment)
                Toast.makeText(context, "errorDrawable download: " + errorDrawable, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }

        };
        Picasso.with(context).load(photoUrl).into(downloadImageTarget);
    }
    //////////////////////////download image to disk//////////////////////////

    //////////////////////////////////////////////////////////// update chat view listener, all listener in this////////////////////////////////////////////////////////////
    private UpdateChatViewListener updateChatViewListener = new UpdateChatViewListener() {

        @Override
        public void updateChatView(final List<DBQBChatMessage> dbqbChatMessageList, final boolean needScrolldown) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
//                        ChatAdapter.getInstance(ChatActivity.this).setChatMessages(dbqbChatMessageList);
                    if (chatAdapter == null)
                        return;
                    chatAdapter.setChatMessages(dbqbChatMessageList);
                    if (needScrolldown) {
                        scrolldownListView();
                    } else {
                        keepListViewPosition();
                    }
                }
            });
        }

        @Override
        public void statusAndTypingTextViewOnChange(final int textColor, final String textContent) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
//                    typeStatusLabel.setTextColor(textColor);
                    typeStatusLabel.setText(textContent);
                    if(ishaveBlocked || textContent==null || textContent.equals("")){
                        typeStatusLabel.setVisibility(View.GONE);
                    }else{
                        typeStatusLabel.setVisibility(View.VISIBLE);
                    }

                }
            });
        }

        @Override
        public void firstCreateDialogSuccess(final QBDialog qbDialog, final String msgTextOrPhotoPath, final int type) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.v("","edwinzz firstCreateDialogSuccess");
                    ChatActivity.this.qbDialog = qbDialog;
                    qbDialogId = qbDialog.getDialogId();
                    chatAdapter.setDialogId(qbDialogId);
                    initChatAndGetDialogQBMsgsAndDbAction();

                    if (opponentQBUser == null) {
                        checkLoggedInHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                firstCreateDialogSuccess(qbDialog, msgTextOrPhotoPath, type);
                            }
                        }, 500);
                    } else {
                        if (msgTextOrPhotoPath != null)
                            sendMsgStart(msgTextOrPhotoPath, type);
                        qbDialog.setLastMessageDateSent(TimeUtils.getCurrentDateTime());
                        if (type == ChatDbViewController.TEXT) {
                            qbDialog.setLastMessage(msgTextOrPhotoPath);
                        }

                        insertDialogAndUserToDB(qbDialog, qbDialogId, opponentQBID, opponentQBUser, opponentMemId);

                        RequestChatRoomCreate requestChatRoomCreate = new RequestChatRoomCreate(userContent.memId, LocalStorageHelper.getAccessToken(getBaseContext()));
                        requestChatRoomCreate.dialogId = qbDialogId;
                        requestChatRoomCreate.toMemId = opponentMemId;

                        requestChatRoomCreate.callChatRoomCreateApi(ChatActivity.this, new ApiCallback() {
                            @Override
                            public void success(ApiResponse apiResponse) {
                                ResponseChatRoomCreate responseGson = new Gson().fromJson(apiResponse.jsonContent, ResponseChatRoomCreate.class);
                                if (Constants.isDevelopment)
                                    Toast.makeText(ChatActivity.this, "callChatRoomCreateApi success: " + responseGson.content.chatRoomId, Toast.LENGTH_LONG).show();

                            }

                            @Override
                            public void failure(String errorMsg) {
                                if (Constants.isDevelopment)
                                Toast.makeText(ChatActivity.this, "callChatRoomCreateApi fail: " + errorMsg, Toast.LENGTH_LONG).show();
                            }
                        });

                    }
                }
            });


        }

        @Override
        public void receivedMsg(final QBChatMessage chatMessage) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (qbDialogId != null &&
                            chatMessage.getDialogId().equalsIgnoreCase(qbDialogId)) {
                        fetchCurrentMsgs(true);
                        sendReadMessageStatusAndDbAction(chatMessage);
                    }

                    //todo download photo if autodownload
                    if (chatMessage.getProperty(ChatDbViewController.PROPERTY_MSG_TYPE).toString()
                            .equalsIgnoreCase(ChatDbViewController.MSG_TYPE_IMAGE)) {
                        DBLogInfo dbLogInfo = DatabaseManager.getInstance(ChatActivity.this).getLogInfo();
                        if (dbLogInfo == null)
                            return;
                        if (dbLogInfo.getPhotoAutoDownload() == null)
                            return;
                        if (chatMessage == null)
                            return;
                        if (chatMessage.getProperty(ChatDbViewController.PROPERTY_PHOTO_URL) == null)
                            return;

                        if (dbLogInfo.getPhotoAutoDownload())
                            downloadImageToDisk(chatMessage);

                    }

                }
            });


        }

        @Override
        public void processError(final QBChatMessage originChatMessage, QBChatException error) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    DatabaseManager.getInstance(ChatActivity.this).updateMsgStatusDbAction(originChatMessage.getId(), ChatDbViewController.ERROR);
                    fetchCurrentMsgs(true);
                }
            });

        }

        @Override
        public void processMessageDelivered(final String messageID, final String dialogId) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    DatabaseManager.getInstance(ChatActivity.this).updateMsgStatusDbAction(messageID, ChatDbViewController.SENT_USER);
                    if (qbDialogId != null && qbDialogId.equalsIgnoreCase(dialogId))
                        fetchCurrentMsgs(true);
                }
            });

        }

        @Override
        public void processMessageRead(final String messageID, final String dialogId) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    DatabaseManager.getInstance(ChatActivity.this).updateMsgStatusDbAction(messageID, ChatDbViewController.READ);
                    if (qbDialogId != null && qbDialogId.equalsIgnoreCase(dialogId))
                        fetchCurrentMsgs(true);
                }
            });
        }

        @Override
        public void connected(XMPPConnection connection) {
            Log.w(TAG, "connected");
        }

        @Override
        public void authenticated(XMPPConnection connection) {
            Log.w(TAG, "authenticated");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    chatSendButton.setEnabled(true);
                    addPhotoBtn.setEnabled(true);
                }
            });

        }

        @Override
        public void connectionClosed() {
            Log.w(TAG, "connectionClosed");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    chatSendButton.setEnabled(false);
                    addPhotoBtn.setEnabled(false);
                }
            });

        }

        @Override
        public void connectionClosedOnError(final Exception e) {
            Log.w(TAG, "connectionClosedOnError: " + e);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    chatSendButton.setEnabled(false);
                    addPhotoBtn.setEnabled(false);
                }
            });
            if (Constants.isDevelopment)
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(ChatActivity.this, "connectionClosedOnError: " + e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

        }

        @Override
        public void reconnectingIn(final int seconds) {
            Log.w(TAG, "reconnectingIn " + seconds);
            if (Constants.isDevelopment)
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(ChatActivity.this, "reconnectingIn " + seconds + "s", Toast.LENGTH_SHORT).show();
                    }
                });

        }

        @Override
        public void reconnectionSuccessful() {
            Log.w(TAG, "reconnectionSuccessful ");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    chatSendButton.setEnabled(true);
                    addPhotoBtn.setEnabled(true);
                    initChatAndGetDialogQBMsgsAndDbAction();
                }
            });

        }

        @Override
        public void reconnectionFailed(final Exception error) {
            Log.w(TAG, "reconnectionFailed: " + error);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    chatSendButton.setEnabled(false);
                    addPhotoBtn.setEnabled(false);
                    if (Constants.isDevelopment)
                        Toast.makeText(ChatActivity.this, "connect error: " + error.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            });
        }

        @Override
        public void processSystemMessageChatSetting(QBChatMessage qbChatMessage, DBQBChatDialog dbqbChatDialog) {
            DatabaseManager.getInstance(ChatActivity.this).updateQBChatDialog(dbqbChatDialog);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    initChatSettingView();
                }
            });
        }

        @Override
        public void processSystemMessageError(final QBChatException e, QBChatMessage qbChatMessage) {
            if (Constants.isDevelopment)
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(ChatActivity.this, "processSystemMessageError: " + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                    }
                });
        }

        @Override
        public void processSystemMessageChatPrivacy(QBChatMessage qbChatMessage, QBUser qbUser) {
            if (qbChatMessage.getSenderId() == opponentQBID) {
                opponentQBUser = qbUser;
                initOpponentQBUserView(opponentQBUser);
                String textContent = getOpponentOnlineStatus(qbUser);
                updateChatViewListener.statusAndTypingTextViewOnChange(Color.WHITE, textContent);
            }
        }
    };


}
