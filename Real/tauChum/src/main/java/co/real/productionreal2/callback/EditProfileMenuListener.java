package co.real.productionreal2.callback;

/**
 * Created by hohojo on 12/1/2016.
 */
public interface EditProfileMenuListener {
    public void editProfileBtnClick();
}
