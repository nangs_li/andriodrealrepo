package co.real.productionreal2.service.model.request;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.model.InputRequest;
import co.real.productionreal2.service.OperationService;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.response.ResponseNewsFeed;
import co.real.productionreal2.util.NetworkUtil;
import co.real.productionreal2.view.Dialog;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by kelvinsun on 27/1/16.
 */
public class RequestAddressSearchRetrieve extends RequestBase {
    public RequestAddressSearchRetrieve(int memId,
                                        String session,
                                        int languageIndex,
                                        List<Integer> retrieveList) {
        super(memId, session);
        this.languageIndex = languageIndex;
        this.retrieveList = retrieveList;
    }

    @SerializedName("LanguageIndex")
    public int languageIndex;

    @SerializedName("RetrieveList")
    public List<Integer> retrieveList;


    public void callAddressSearchRetrieveAgentProfile(final Context context, final ApiCallback callback) {
        String json = new Gson().toJson(this);
        Log.d("callAddressSearchRe", "callAddressSearchRe = " + json);
        final OperationService weatherService = new OperationService(context);
        weatherService.getCoffeeService().addressSearchRetrieveAgentProfile(new InputRequest(json), new Callback<ApiResponse>() {
            @Override
            public void success(ApiResponse apiResponse, Response response) {
                ResponseNewsFeed responseGson = new Gson().fromJson(apiResponse.jsonContent, ResponseNewsFeed.class);
                if (responseGson.errorCode == 4) {
                    Dialog.accessErrorForceLogoutDialog(context).show();
                } else if (responseGson.errorCode == 26) {
                    Dialog.suspendedErrorForceLogoutDialog(context).show();
                } else if (responseGson.errorCode != 0 && responseGson.errorCode != 20 && responseGson.errorCode != 21 &&
                        responseGson.errorCode != 22 && responseGson.errorCode != 23) {
                    callback.failure(responseGson.errorMsg);
                } else {
                    callback.success(apiResponse);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                callback.failure("");
                NetworkUtil.showNoNetworkMsg(context);
            }
        });
    }

    /**
     *  errorStatus=1 Not NetWork Connection
     errorStatus=2 Real Server Not Reponse
     errorStatus=3 checkAccessError Other User Login This Account
     errorStatus=4 Not Record Find
     errorStatus=10 Google Server Not Reponse
     errorStatus=11 Google Return Empty Result
     */
}

