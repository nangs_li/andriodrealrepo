package co.real.productionreal2.activity.setting;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import co.real.productionreal2.R;
import co.real.productionreal2.activity.BaseActivity;
import co.real.productionreal2.util.AppUtil;

import butterknife.ButterKnife;
import butterknife.InjectView;
import co.real.productionreal2.R;

public class AboutSettingActivity extends BaseActivity {
    @InjectView(R.id.headerLayout)
    View headerLayout;
    @InjectView(R.id.tv_app_version)
    TextView tvAppVersion;
    @InjectView(R.id.tv_build_version)
    TextView tvBuildVersion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_setting);
        ButterKnife.inject(this);
        initView();
    }

    private void initView() {
        TextView titleTextView = (TextView)headerLayout.findViewById(R.id.titleTextView);
        titleTextView.setText(R.string.common__about_this_version);

        tvAppVersion.setText(AppUtil.getAppVersion(this));
        tvBuildVersion.setText(AppUtil.getBuildVersion(this));
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}
