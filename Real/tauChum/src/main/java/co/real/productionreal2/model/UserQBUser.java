package co.real.productionreal2.model;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;

import java.util.List;

import co.real.productionreal2.Constants;
import co.real.productionreal2.CoreData;
import co.real.productionreal2.common.LocalStorageHelper;
import co.real.productionreal2.dao.account.DBLogInfo;
import co.real.productionreal2.dao.account.DBQBChatDialog;
import co.real.productionreal2.daomanager.DatabaseManager;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;

/**
 * Created by hohojo on 16/11/2015.
 */
public class UserQBUser {
    public static QBUser qbUser;
    public static boolean isLogin = false;

    public static QBUser getNewQBUser(Context context) {
        DBLogInfo dbLogInfo = DatabaseManager.getInstance(context).getLogInfo();
        QBUser qbUser = new QBUser();
        if (dbLogInfo != null)
            if (dbLogInfo.getLogInfo() != null) {
                ResponseLoginSocial.Content userContent = new Gson().fromJson(dbLogInfo.getLogInfo(), ResponseLoginSocial.Content.class);
                String str = "" + userContent.memId;
                String fullName = userContent.memName;
//                String qbpw = "" +userContent.qbPwd;
                String qbpw = LocalStorageHelper.getQBpw(context);

                String formatted = ("0000000000" + str).substring(str.length());
                qbUser.setLogin(formatted);
//                qbUser.setPassword(formatted);
                qbUser.setPassword(qbpw);

                qbUser.setWebsite(userContent.photoUrl);
                qbUser.setExternalId("" + userContent.memId);

                QBUserCustomData qbUserCustomData = new QBUserCustomData();
                qbUserCustomData.myPersonalPhotoUrl = userContent.photoUrl;
                qbUserCustomData.chatRoomShowLastSeen = true;
                qbUserCustomData.chatRoomShowReadReceipts = true;

                qbUserCustomData.memberID = userContent.memId+"";
                qbUserCustomData.agentListingID = "";
                qbUserCustomData.lastUpdateTimeStamp = System.currentTimeMillis()+"";
                qbUserCustomData.agentListing = "";

                Log.d("UserQBUser", "UserQBUser: " + userContent.toString());
//                if (userContent.agentProfile != null) {
//                    qbUserCustomData.agentListing = new Gson().toJson(userContent.agentProfile.agentListing);
//                    if (userContent.agentProfile.agentListing != null) {
//                        qbUserCustomData.agentListingId = "" + userContent.agentProfile.agentListing.listingID;
//                        qbUserCustomData.agentProfileId = "" + userContent.agentProfile.memberID;
//                    }
//                }
                qbUserCustomData.photoUrl = userContent.photoUrl;
                qbUserCustomData.chatVersion = "" + Constants.chatVersion;

                String customData = new Gson().toJson(qbUserCustomData);
                qbUser.setCustomData(customData);
                qbUser.setFullName(fullName);
            }
        return qbUser;
    }

    public static QBUser updateUserQBUser(final Context context) {
        if (DatabaseManager.getInstance(context) == null)
            return null;
        if (DatabaseManager.getInstance(context).getLogInfo() == null)
            return null;
        if (DatabaseManager.getInstance(context).getLogInfo().getLogInfo() == null)
            return null;

        final ResponseLoginSocial.Content userContent = CoreData.getUserContent(context);
        QBUsers.getUser(Integer.parseInt(userContent.qbid), new QBEntityCallback<QBUser>() {
            @Override
            public void onSuccess(QBUser qbUser, Bundle bundle) {
                UserQBUser.qbUser = qbUser;
                QBUserCustomData qbUserCustomData;
                if (qbUser.getCustomData() == null || qbUser.getCustomData().isEmpty()) {
                    qbUserCustomData = new QBUserCustomData();
                } else {
                    qbUserCustomData = new Gson().fromJson(qbUser.getCustomData(), QBUserCustomData.class);
                }
                qbUser.setWebsite(userContent.photoUrl);
                qbUser.setExternalId("" + userContent.memId);

                qbUserCustomData.memberID = userContent.memId+"";
                qbUserCustomData.agentListingID = "";
                qbUserCustomData.lastUpdateTimeStamp = System.currentTimeMillis()+"";
                qbUserCustomData.agentListing = "";

                Log.d("UserQBUser", "UserQBUser: " + userContent.toString());
//                if (userContent.agentProfile != null) {
//                    qbUserCustomData.agentListing = new Gson().toJson(userContent.agentProfile.agentListing);
//                    if (userContent.agentProfile.agentListing != null) {
//                        qbUserCustomData.agentListingId = "" + userContent.agentProfile.agentListing.listingID;
//                        qbUserCustomData.agentProfileId = "" + userContent.agentProfile.memberID;
//                    }
//                }
                qbUserCustomData.photoUrl = userContent.photoUrl;
                qbUserCustomData.chatVersion = "" + Constants.chatVersion;
                qbUserCustomData.myPersonalPhotoUrl = userContent.photoUrl;

                List<DBQBChatDialog> dbqbChatDialogList = DatabaseManager.getInstance(context).listQBChatDialog();
                String blockedOppoenentUser = "";
                String mutedOppoenentUser = "";
                if (dbqbChatDialogList != null) {
                    for (DBQBChatDialog dbqbChatDialog : dbqbChatDialogList) {
                        if (dbqbChatDialog.getBlockedOpponent() != null &&
                                dbqbChatDialog.getBlockedOpponent() == 1) {
                            if (blockedOppoenentUser.isEmpty())
                                blockedOppoenentUser = "" + dbqbChatDialog.getQbUserID();
                            else
                                blockedOppoenentUser = blockedOppoenentUser + ", " + dbqbChatDialog.getQbUserID();
                        }
                        if (dbqbChatDialog.getMutedOpponent() != null &&
                                dbqbChatDialog.getMutedOpponent() == 1) {
                            if (mutedOppoenentUser.isEmpty())
                                mutedOppoenentUser = "" + dbqbChatDialog.getQbUserID();
                            else
                                mutedOppoenentUser = mutedOppoenentUser + ", " + dbqbChatDialog.getQbUserID();
                        }
                    }
                }

                qbUserCustomData.block = blockedOppoenentUser;
                qbUserCustomData.mute = mutedOppoenentUser;
                String customData = new Gson().toJson(qbUserCustomData);
                qbUser.setCustomData(customData);
                QBUsers.updateUser(qbUser, new QBEntityCallbackImpl<QBUser>() {
                    @Override
                    public void onSuccess(QBUser user, Bundle args) {
                        UserQBUser.qbUser = user;
                    }

                    @Override
                    public void onError(List<String> errors) {
                        Toast.makeText(context, "setChatroomshowlastseen error: " + errors, Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(List<String> list) {
            }
        });


        return qbUser;
    }


}
