package co.real.productionreal2.service.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AgentSpecialty implements   Serializable,Parcelable {


	/**
	 *
	 */
	private static final long serialVersionUID = -3226106735510644913L;

	@SerializedName("specialty")
	public String specialty = "";

	public int order;

	public AgentSpecialty(String specialty, int order) {
		super();
		this.specialty = specialty;
		this.order =  order;
	}
		
	public AgentSpecialty(Parcel in) {
		specialty = in.readString();
		order =  in.readInt();
	}
	
	public static final  Parcelable.Creator<AgentSpecialty> CREATOR = new Parcelable.Creator<AgentSpecialty>() {
        @Override
        public AgentSpecialty createFromParcel(Parcel in) {
            return new AgentSpecialty(in);
        }

        @Override
        public AgentSpecialty[] newArray(int size) {
            return new AgentSpecialty[size];
        }
    };

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(specialty);
		dest.writeInt(order);
	}
}
