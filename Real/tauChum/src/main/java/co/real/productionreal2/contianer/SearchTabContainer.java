package co.real.productionreal2.contianer;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import co.real.productionreal2.R;
import co.real.productionreal2.BaseContainerFragment;
import co.real.productionreal2.newsfeed.AddressSearchFragment;
import co.real.productionreal2.newsfeed.NewsFeedFragment;

/**
 * Created by kelvinsun on 22/12/15.
 */
public class SearchTabContainer extends BaseContainerFragment {

    boolean needInit = true;

    public void fragmentBecameVisible() {
        if(needInit){
            initView();
            needInit = false;
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.container_fragment, null);

//		ViewGroup.MarginLayoutParams marginParams = new ViewGroup.MarginLayoutParams(rootView.getLayoutParams());
//		marginParams.setMargins(0, MainActivityTabBase.tabHeight, 0, 0);
//		FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(marginParams);
//		rootView.setLayoutParams(layoutParams);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void initView() {
        replaceFragment(new AddressSearchFragment(), false);

    }
}
