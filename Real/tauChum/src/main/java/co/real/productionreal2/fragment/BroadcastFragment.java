package co.real.productionreal2.fragment;

import android.app.Fragment;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import co.real.productionreal2.Constants;
import co.real.productionreal2.CoreData;
import co.real.productionreal2.R;
import co.real.productionreal2.adapter.PhoneBookAdapter;
import co.real.productionreal2.dao.account.DBPhoneBook;
import co.real.productionreal2.daomanager.DatabaseManager;
import co.real.productionreal2.util.PhoneBookUtil;
import co.real.productionreal2.view.Dialog;
import io.branch.indexing.BranchUniversalObject;
import io.branch.referral.Branch;
import io.branch.referral.BranchError;
import io.branch.referral.util.LinkProperties;


/**
 * Created by kelvinsun on 31/5/16.
 */
public class BroadcastFragment extends Fragment implements PhoneBookAdapter.PhoneBookAdapterListener {

    @InjectView(R.id.ll_connect_agent_header)
    LinearLayout llConnectAgentHeader;
    @InjectView(R.id.tv_title)
    TextView tvTitle;
    @InjectView(R.id.et_search_agent)
    EditText etSearchAgent;
    @InjectView(R.id.lv_phone_book_item)
    ListView lvPhoneBook;
    @InjectView(R.id.pb_find_agent)
    ProgressBar spinner;
    @InjectView(R.id.inviteBtn)
    Button bnInvite;
    @InjectView(R.id.tv_empty_placeholder)
    TextView tvEmptyMsg;
    @InjectView(R.id.otherBtn)
    Button bnOther;

    private static final int REQUEST_CODE_VIRAL = 100;
    private PhoneBookAdapter connectAgentAdapter;
    private ArrayList<DBPhoneBook> connectAgentList = new ArrayList<>();
    private List<DBPhoneBook> mInvitePhoneNoList = new ArrayList<>();

    private static final int AGENT_SHOW_LIMIT = 3;
    int mNum;

    /**
     * Create a new instance of CountingFragment, providing "num"
     * as an argument.
     */
    public static BroadcastFragment newInstance(int num) {
        BroadcastFragment f = new BroadcastFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putInt("num", num);
        f.setArguments(args);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_find_your_agent, container, false);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.inject(getActivity());

        initView();
        addListener();
    }


    private void addListener() {
        etSearchAgent.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                if (connectAgentAdapter != null) {
                    connectAgentAdapter.getFilter().filter(cs);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void afterTextChanged(Editable arg0) {
            }
        });
    }

    private void initView() {
        lvPhoneBook.setVisibility(View.VISIBLE);
        llConnectAgentHeader.setVisibility(View.GONE);
        tvTitle.setVisibility(View.VISIBLE);
        tvTitle.setText(Html.fromHtml(getString(R.string.connectagent__title)));
        bnInvite.setVisibility(View.VISIBLE);

        int[][] states = new int[][]{
                new int[]{android.R.attr.state_pressed}, // pressed
                new int[]{android.R.attr.state_enabled}, // enable
                new int[]{}
        };
        int[] colors = new int[]{
                getActivity().getResources().getColor(R.color.dark_grey), // grey
                getActivity().getResources().getColor(R.color.invite_yellow), // active color
                getActivity().getResources().getColor(R.color.dark_grey)  // grey
        };
        ColorStateList list = new ColorStateList(states, colors);
        bnOther.setTextColor(list);
        bnOther.setVisibility(View.VISIBLE);

        spinner.setVisibility(View.GONE);

        setupAgentList();
    }


    private void setupAgentList() {
        //combine list
        connectAgentList = new ArrayList<>();
        connectAgentList.addAll(getLocalPhoneBookList());
        
        if (connectAgentList.isEmpty()){
            updatePhoneBookList();
            connectAgentList=(ArrayList)getLocalPhoneBookList();
        }

        connectAgentAdapter = new PhoneBookAdapter(getActivity(), connectAgentList, BroadcastFragment.this);
        lvPhoneBook.setAdapter(connectAgentAdapter);
    }


    private void updatePhoneBookList() {
        ArrayList<DBPhoneBook> phoneBookList = PhoneBookUtil.loadPhoneBookFromLocal(getActivity());
        DatabaseManager.getInstance(getActivity()).insertPhoneBook(phoneBookList);
    }

    private List<DBPhoneBook> getLocalPhoneBookList() {
        List<DBPhoneBook> phoneBookList = DatabaseManager.getInstance(getActivity()).readPhoneBook();
        return phoneBookList;
    }

    @Override
    public ArrayList<DBPhoneBook> getPhoneBookList() {
        return connectAgentList;
    }

    @Override
    public void onSelectItem(List<DBPhoneBook> invitePhoneNoList) {
        mInvitePhoneNoList = invitePhoneNoList;
        updateBtnText();
    }

    @Override
    public void searchDone(int size) {
        Log.d("ConnectAgentAdapter", "ConnectAgentAdapter2: " + size);
        tvEmptyMsg.setVisibility(size == 0 ? View.VISIBLE : View.GONE);
    }

    private void updateBtnText() {
        int selectedCount = DatabaseManager.getInstance(getActivity()).getCheckedPhoneNoCount();
        String btnTxt = String.format(getString(R.string.broadcast__send_invitations), selectedCount);
        bnInvite.setText(btnTxt);
    }


    private void sendSMS() {
        BranchUniversalObject branchUniversalObject = genBranchUniObj();
        LinkProperties linkProps = genLinkProps();
        branchUniversalObject.generateShortUrl(getActivity(), linkProps, new Branch.BranchLinkCreateListener() {
            @Override
            public void onLinkCreate(String url, BranchError error) {
                if (error != null) {
                    Dialog.normalDialog(getActivity(), "Branch.io gen shortUrl error: " + error);
                } else {
                    //parse data to list String
                    final List<String> phoneNoList = new ArrayList<>();
                    for (DBPhoneBook phoneItem : mInvitePhoneNoList) {
                        String phoneNo = phoneItem.getCountryCode() + " " + phoneItem.getPhoneNumber();
                        phoneNoList.add(phoneNo);
                        //update flag
                        phoneItem.setHasSent(true);
                        phoneItem.setHasChecked(false);
                        DatabaseManager.getInstance(getActivity()).updatePhoneBook(phoneItem);
                    }
                    connectAgentAdapter.notifyDataSetChanged();
                    if (mInvitePhoneNoList != null) {
                        mInvitePhoneNoList.clear();
                    }
                    updateBtnText();
                    //remove teh blanket at the beginning and the end
                    String phoneNoListStr = phoneNoList.toString().replace("[", "").replace("]", "");
                    Uri uri = Uri.parse("smsto:" + phoneNoListStr);
                    Intent it = new Intent(Intent.ACTION_SENDTO, uri);
                    it.putExtra("sms_body", getString(R.string.invite_to_download__content) + " " + url);
                    startActivityForResult(it, REQUEST_CODE_VIRAL);
                }
            }
        });
    }

    private BranchUniversalObject genBranchUniObj() {
        BranchUniversalObject branchUniObj = new BranchUniversalObject()
                .setTitle("Real")
                .setContentDescription("Someone invite you to Invite in Real")
                .setContentIndexingMode(BranchUniversalObject.CONTENT_INDEX_MODE.PUBLIC)
                .addContentMetadata(Constants.BRANCH_MEM_ID, "" + CoreData.getUserContent(getActivity()).memId)
                .addContentMetadata(Constants.BRANCH_MEM_NAME, CoreData.getUserContent(getActivity()).memName)
                .addContentMetadata(Constants.BRANCH_ACTION, Constants.BRANCH_ACTION_NONE);

        return branchUniObj;
    }

    private LinkProperties genLinkProps() {
        LinkProperties linkProperties = new LinkProperties()
                .setChannel(Constants.BRANCH_CHANNEL_DOWNLOAD)
                .setFeature("invite");
        return linkProperties;
    }

    @OnClick(R.id.inviteBtn)
    public void sendInvite() {
        sendSMS();
    }


    @OnClick(R.id.otherBtn)
    public void otherInviteMethod() {
        genShortUrl();
    }


    private void genShortUrl() {
        if (CoreData.getUserContent(getActivity()) == null)
            return;
        BranchUniversalObject branchUniversalObject = genBranchUniObj();
        LinkProperties linkProps = genLinkProps();
        branchUniversalObject.generateShortUrl(getActivity(), linkProps, new Branch.BranchLinkCreateListener() {
            @Override
            public void onLinkCreate(String url, BranchError error) {
                if (error != null) {
                    Dialog.normalDialog(getActivity(), "Branch.io gen shortUrl error: " + error);
                } else {
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_SEND);
                    intent.setType("text/plain");
                    intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.invite_to_follow__subject));
                    intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.invite_to_follow__content) + " " + url);
                    startActivityForResult(Intent.createChooser(intent, getString(R.string.common__invite_to_follow)), REQUEST_CODE_VIRAL);
                }
            }
        });
    }

    @OnClick(R.id.closeBtn)
    public void finishActivity() {
        getActivity().finish();
    }

    @Override
    public void onResume() {
        super.onResume();
        updateBtnText();
    }

}
