package co.real.productionreal2.chat;

import java.util.List;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.Toast;

import co.real.productionreal2.chat.core.ApplicationSessionStateCallback;
import co.real.productionreal2.chat.core.ChatService;
import co.real.productionreal2.model.UserQBUser;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.users.model.QBUser;

public class BaseChatFragment extends Fragment implements
		ApplicationSessionStateCallback {
	private static final String TAG = "BaseChatFragment";

	private static final String USER_LOGIN_KEY = "USER_LOGIN_KEY";
	private static final String USER_PASSWORD_KEY = "USER_PASSWORD_KEY";

	private boolean sessionActive = false;
	private boolean needToRecreateSession = false;

	private ProgressDialog progressDialog;
	private final Handler handler = new Handler();

	public boolean isSessionActive() {
		return sessionActive;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// 'initialised' will be true if it's the 1st start of the app or if the
		// app's process was killed by OS(or user)
		//
		boolean initialised = ChatService.initIfNeed(getActivity());
		if (initialised && savedInstanceState != null) {
			needToRecreateSession = true;
		} else {
			sessionActive = true;
		}
	}
	
	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		if (needToRecreateSession) {
			needToRecreateSession = false;

			Log.d(TAG, "Need to restore chat connection");

//			QBUser user = new QBUser();
//			user.setLogin(savedInstanceState.getString(USER_LOGIN_KEY));
//			user.setPassword(savedInstanceState.getString(USER_PASSWORD_KEY));
//
//			savedInstanceState.remove(USER_LOGIN_KEY);
//			savedInstanceState.remove(USER_PASSWORD_KEY);

			recreateSession(UserQBUser.getNewQBUser(getActivity()));
		}
	}


	private void recreateSession(QBUser user) {
		sessionActive = false;
		this.onStartSessionRecreation();

		showProgressDialog();

		// Restoring Chat session
		//
		if (user == null)
			user = UserQBUser.getNewQBUser(getActivity());

		final QBUser finalUser = user;
		ChatService.initIfNeed(getActivity());
		ChatService.getInstance().login(user, new QBEntityCallbackImpl() {
			@Override
			public void onSuccess() {
				Log.d(TAG, "Chat login onSuccess");

				progressDialog.dismiss();
				progressDialog = null;

				sessionActive = true;
				BaseChatFragment.this.onFinishSessionRecreation(true);
			}

			@Override
			public void onError(List errors) {

				Log.d(TAG, "Chat login onError: " + errors);

				Toast toast = Toast
						.makeText(
								getActivity(),
								"Error in the recreate session request, trying again in 1 seconds.. Check you internet connection.",
								Toast.LENGTH_SHORT);
				toast.show();

				// try again
				handler.postDelayed(new Runnable() {
					@Override
					public void run() {
						recreateSession(finalUser);
					}
				}, 1000);

				BaseChatFragment.this.onFinishSessionRecreation(false);
			}
		});
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		ChatService.initIfNeed(getActivity());
		QBUser currentUser = ChatService.getInstance().getCurrentUser();
		if (currentUser != null) {
			outState.putString(USER_LOGIN_KEY, currentUser.getLogin());
			outState.putString(USER_PASSWORD_KEY, currentUser.getPassword());
		}

		// Always call the superclass so it can save the view hierarchy state
		super.onSaveInstanceState(outState);
	}

	private void showProgressDialog() {
		if (progressDialog == null) {
			progressDialog = new ProgressDialog(getActivity());
			progressDialog.setTitle("Loading");
			progressDialog.setMessage("Restoring chat session...");
			progressDialog.setProgressStyle(progressDialog.STYLE_SPINNER);
		}
		progressDialog.show();
	}

	//
	// ApplicationSessionStateCallback
	//

	@Override
	public void onStartSessionRecreation() {
	}

	@Override
	public void onFinishSessionRecreation(boolean success) {
	}

}
