package co.real.productionreal2;

import android.app.Activity;
import android.content.Intent;

import co.real.productionreal2.R;

/**
 * Created by alexhung on 15/4/16.
 */
public class Navigation {
    public static String NavigationTypeKey = "NavigationTypeKey";
    public static enum NavigationType{
      None,Push,Present,Fade
    }

    public static NavigationType getNavigationTypeFromIntent(Intent indent){
        if (indent.hasExtra(NavigationTypeKey)){
            int typeIndex = indent.getIntExtra(NavigationTypeKey,0);
            return NavigationType.values()[typeIndex];
        }else{
            return NavigationType.None;
        }
    }

    public static void pushIntent(Activity fromActivity,Intent intent){
        intent.putExtra(NavigationTypeKey,NavigationType.Push.ordinal());
        fromActivity.startActivity(intent);
        fromActivity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
    public static void pushIntentForResult(Activity fromActivity,Intent intent,int requestCode){
        intent.putExtra(NavigationTypeKey,NavigationType.Push.ordinal());
        fromActivity.startActivityForResult(intent,requestCode);
        fromActivity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public static void fadeIntent(Activity fromActivity,Intent intent){
        intent.putExtra(NavigationTypeKey,NavigationType.Fade.ordinal());
        fromActivity.startActivity(intent);
        fromActivity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
    public static void fadeIntentForResult(Activity fromActivity,Intent intent,int requestCode){
        intent.putExtra(NavigationTypeKey,NavigationType.Fade.ordinal());
        fromActivity.startActivityForResult(intent,requestCode);
        fromActivity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }


    public static void presentIntent(Activity fromActivity,Intent intent){
        intent.putExtra(NavigationTypeKey,NavigationType.Present.ordinal());
        fromActivity.startActivity(intent);
        fromActivity.overridePendingTransition(R.anim.slide_in_up, R.anim.fade_out);
    }

    public static void presentIntentForResult(Activity fromActivity, Intent intent, int requestCode){
        intent.putExtra(NavigationTypeKey,NavigationType.Present.ordinal());
        fromActivity.startActivityForResult(intent,requestCode);
        fromActivity.overridePendingTransition(R.anim.slide_in_up, R.anim.fade_out);
    }

}
