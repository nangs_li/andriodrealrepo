package co.real.productionreal2.service.model.request;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.callback.CreateListingApiCallback;
import co.real.productionreal2.model.GoogleAddress;
import co.real.productionreal2.model.InputRequest;
import co.real.productionreal2.service.AdminService;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.response.ResponseCreateListing;
import co.real.productionreal2.service.model.response.ResponseFileUpload;
import co.real.productionreal2.util.FileUtil;
import co.real.productionreal2.util.NetworkUtil;
import co.real.productionreal2.view.Dialog;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

public class RequestListing extends RequestBase {

    public RequestListing(int memId, String session) {
        super(memId, session);
        reasons = new ArrayList<Reason>();
        addressInputs = new ArrayList<AddressUserInput>();
        googleAddressList = new ArrayList<GoogleAddressPack>();
    }

    @SerializedName("SloganIndex")
    public int sloganIndex = -1; // 1

    @SerializedName("PropertyType")
    public int propertyType = -1; // 1

    @SerializedName("SpaceType")
    public int spaceType = -1; // 1

    @SerializedName("PropertyPrice")
    public BigDecimal propertyPrice; // 100000000

    @SerializedName("CurrencyUnit")
    public String currency; // HKD

    @SerializedName("PropertySize")
    public int propertySize; // 1000

    public String sizeUnit; // ft

    @SerializedName("SizeUnitType")
    public int sizeUnitType = 0;

    @SerializedName("BedroomCount")
    public int bedroomCount;

    @SerializedName("BathroomCount")
    public int bathroomCount;

    @SerializedName("PropertyPriceFormattedForRoman")
    public String propertyPriceFormattedForRoman;

    @SerializedName("PropertyPriceFormattedForChinese")
    public String propertyPriceFormattedForChinese;

    @SerializedName("Reasons")
    public List<Reason> reasons;

    @SerializedName("AddressUserInput")
    public List<AddressUserInput> addressInputs;

    @SerializedName("GoogleAddressPack")
    public List<GoogleAddressPack> googleAddressList;

    public static class Reason implements Parcelable {

        @SerializedName("SloganIndex")
        public int sloganIndex;

        @SerializedName("LanguageIndex")
        public int languageIndex; // 100000000

        @SerializedName("Reason1")
        public String reason1;

        @SerializedName("Reason2")
        public String reason2;

        @SerializedName("Reason3")
        public String reason3;

        @SerializedName("MediaURL1")
        public String mediaURL1;

        @SerializedName("MediaURL2")
        public String mediaURL2;

        @SerializedName("MediaURL3")
        public String mediaURL3;

        public Reason(Parcel source) {
            sloganIndex = source.readInt();
            languageIndex = source.readInt();
            reason1 = source.readString();
            reason2 = source.readString();
            reason3 = source.readString();
        }

        public Reason() {
            // TODO Auto-generated constructor stub
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(sloganIndex);
            dest.writeInt(languageIndex);
            dest.writeString(reason1);
            dest.writeString(reason2);
            dest.writeString(reason3);
        }

        public final static Parcelable.Creator<Reason> CREATOR = new Creator<Reason>() {
            public Reason createFromParcel(Parcel source) {
                return new Reason(source);
            }

            @Override
            public Reason[] newArray(int size) {
                return new Reason[size];
            }
        };
    }

    public static class AddressUserInput implements Parcelable {

        @SerializedName("LanguageIndex")
        public int languageIndex; // 100000000

        @SerializedName("Street")
        public String street;

        @SerializedName("AptSuite")
        public String aptSuite;

        @SerializedName("Unit")
        public String unit;

        public AddressUserInput(Parcel source) {
            languageIndex = source.readInt();
            street = source.readString();
            aptSuite = source.readString();
            unit = source.readString();
        }

        public AddressUserInput() {
            // TODO Auto-generated constructor stub
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(languageIndex);
            dest.writeString(street);
            dest.writeString(aptSuite);
            dest.writeString(unit);
        }

        public final static Parcelable.Creator<AddressUserInput> CREATOR = new Creator<AddressUserInput>() {
            public AddressUserInput createFromParcel(Parcel source) {
                return new AddressUserInput(source);
            }

            @Override
            public AddressUserInput[] newArray(int size) {
                return new AddressUserInput[size];
            }
        };

        @Override
        public String toString() {
            return "AddressUserInput{" +
                    "languageIndex=" + languageIndex +
                    ", street='" + street + '\'' +
                    ", aptSuite='" + aptSuite + '\'' +
                    ", unit='" + unit + '\'' +
                    '}';
        }
    }

    public class GoogleAddressPack {

        @SerializedName("GoogleAddressLang")
        public String lang; // 100000000

        @SerializedName("GoogleAddress")
        public GoogleAddress address;

        @Override
        public String toString() {
            return "GoogleAddressPack{" +
                    "lang='" + lang + '\'' +
                    ", address=" + address.toString() +
                    '}';
        }
    }

    private void fileUpload(AdminService weatherService, String filePath, int position, int fileType, int listingId, int langIndex, final CreateListingApiCallback callback) {
        weatherService.getCoffeeService().fileUpload(
                new TypedFile("multipart/form-data", new File(filePath)),
                memId,
                session,
                listingId,
                position,
                FileUtil.getFileExt(filePath),
                fileType,
                langIndex,
                new retrofit.Callback<ApiResponse>() {

                    @Override
                    public void success(ApiResponse apiResponse, Response response) {
                        ResponseFileUpload responseGson = new Gson().fromJson(apiResponse.jsonContent, ResponseFileUpload.class);
                        if (responseGson.errorCode == 4) {
//                            Dialog.accessErrorForceLogoutDialog(context).show();
                        } else if (responseGson.errorCode != 0 && responseGson.errorCode != 20 && responseGson.errorCode != 21 &&
                                responseGson.errorCode != 22 && responseGson.errorCode != 23) {
                            callback.failure(responseGson.errorMsg);
                        } else {
                            callback.progress();
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        callback.failure(error.getMessage());
                    }
                });
    }

    public void callAgentListingAddAndUploadFileApi(final Context context, final ApiCallback callback) {
        this.sizeUnit = "";
        String json = new Gson().toJson(this);
        Log.d("callAgentListingAdd", "callAgentListingAdd: " + json);
        final AdminService weatherService = new AdminService(context);
        weatherService.getCoffeeService().agentListingAdd(
                new InputRequest(json),
                new retrofit.Callback<ApiResponse>() {
                    @Override
                    public void failure(RetrofitError error) {
                        callback.failure("");
                        NetworkUtil.showNoNetworkMsg(context);
                    }

                    @Override
                    public void success(ApiResponse apiResponse, Response response) {
                        ResponseCreateListing responseGson = new Gson().fromJson(apiResponse.jsonContent, ResponseCreateListing.class);
                        Log.w("callAgentListingAdd", "responseGson = " + responseGson);
                        if (responseGson.errorCode == 4) {
                            Dialog.accessErrorForceLogoutDialog(context).show();
                        } else if (responseGson.errorCode == 26) {
                            Dialog.suspendedErrorForceLogoutDialog(context).show();
                        } else if (responseGson.errorCode != 0 && responseGson.errorCode != 20 && responseGson.errorCode != 21 &&
                                responseGson.errorCode != 22 && responseGson.errorCode != 23) {
                            callback.failure(responseGson.errorMsg);
                        } else {
                            //listing photos
                            callback.success(apiResponse);
                        }
                    }
                }
        );
    }

    @Override
    public String toString() {
        return "RequestListing{" +
                "sloganIndex=" + sloganIndex +
                ", propertyType=" + propertyType +
                ", spaceType=" + spaceType +
                ", propertyPrice=" + propertyPrice +
                ", currency='" + currency + '\'' +
                ", propertySize=" + propertySize +
                ", sizeUnit='" + sizeUnit + '\'' +
                ", sizeUnitType=" + sizeUnitType +
                ", bedroomCount=" + bedroomCount +
                ", bathroomCount=" + bathroomCount +
                ", propertyPriceFormattedForRoman='" + propertyPriceFormattedForRoman + '\'' +
                ", propertyPriceFormattedForChinese='" + propertyPriceFormattedForChinese + '\'' +
                ", reasons=" + reasons +
                ", addressInputs=" + addressInputs +
                ", googleAddressList=" + googleAddressList +
                '}';
    }

}
