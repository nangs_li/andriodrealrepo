package co.real.productionreal2.view;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.ScrollView;

public class NonFocusingScrollView extends ScrollView {

	  public NonFocusingScrollView(Context context) {
	    super(context);
	  }

	  public NonFocusingScrollView(Context context, AttributeSet attrs) {
	    super(context, attrs);
	  }

	  public NonFocusingScrollView(Context context, AttributeSet attrs, int defStyle) {
	    super(context, attrs, defStyle);
	  }

	  @Override
	  protected boolean onRequestFocusInDescendants(int direction, Rect previouslyFocusedRect) {
	    return true;
	  }
//	  public boolean onInterceptTouchEvent(MotionEvent p_event) {
//		    /*if (p_event.getAction() == MotionEvent.ACTION_MOVE) {
//		        return true;
//		    }*/
//		    return super.onInterceptTouchEvent(p_event);
//		}
//
//		@Override
//		public boolean onTouchEvent(MotionEvent p_event) {
//		    if (p_event.getAction() == MotionEvent.ACTION_MOVE && getParent() != null) {
//		        getParent().requestDisallowInterceptTouchEvent(true);
//		    }
//		    return super.onTouchEvent(p_event);
//		}
	}