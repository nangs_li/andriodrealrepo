package co.real.productionreal2.service.model;

import java.util.List;

import com.google.gson.annotations.SerializedName;
import co.real.productionreal2.model.GooglePlace;

public class ApiResponsePlace {

	@SerializedName("predictions")
	public List<GooglePlace> googlePlaces;
	
	@SerializedName("status")
	public String status;  //ZERO_RESULTS
	
}
