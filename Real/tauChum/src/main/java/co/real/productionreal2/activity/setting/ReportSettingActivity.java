package co.real.productionreal2.activity.setting;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import co.real.productionreal2.CoreData;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.BaseActivity;
import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.common.LocalStorageHelper;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.request.RequestReportProblem;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;
import co.real.productionreal2.util.AppUtil;
import co.real.productionreal2.util.StringUtils;

public class ReportSettingActivity extends BaseActivity {
    @InjectView(R.id.headerLayout)
    View headerLayout;
    @InjectView(R.id.reportEditText)
    EditText reportEditText;
    @InjectView(R.id.reportBtn)
    Button btnSubmit;

    private ResponseLoginSocial.Content userContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_setting);
        ButterKnife.inject(this);

        userContent = CoreData.getUserContent(this);

        initView();
    }

    private void initView() {
        TextView titleTextView = (TextView) headerLayout.findViewById(R.id.titleTextView);
        titleTextView.setText(StringUtils.getById(this, R.string.setting__feedback, "Feedback"));
        btnSubmit.setEnabled(!reportEditText.getText().toString().equals(""));

        reportEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                btnSubmit.setEnabled(s.length() > 0);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @OnClick(R.id.reportBtn)
    public void reportBtnClick(View view) {

        RequestReportProblem reportProblem = new RequestReportProblem(userContent.memId, LocalStorageHelper.getAccessToken(getBaseContext()));
        reportProblem.problemType = 1;
        reportProblem.desc = reportEditText.getText().toString();
        Log.d("callReportApi", "callReportApi: " + reportProblem.toString());
        reportProblem.callReportProblemApi(this, new ApiCallback() {
            @Override
            public void success(ApiResponse apiResponse) {
                AppUtil.showToast(ReportSettingActivity.this, getString(R.string.invite_to_chat__success));
                finish();
            }

            @Override
            public void failure(String errorMsg) {
                AppUtil.showToast(ReportSettingActivity.this, getString(R.string.error_message__please_try_again_later));
            }
        });


    }


}
