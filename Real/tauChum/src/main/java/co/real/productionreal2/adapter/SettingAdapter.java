package co.real.productionreal2.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import co.real.productionreal2.R;
import co.real.productionreal2.model.SettingItem;

import java.util.List;

/**
 * Created by alexhung on 15/4/16.
 */
public class SettingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<SettingItem> items;

    public class SectionRowViewHolder extends RecyclerView.ViewHolder {
        public TextView titleTextView;
        public TextView valueTextView;
        public ImageView dividerImageView;

        public SectionRowViewHolder(View view) {
            super(view);
            titleTextView = (TextView) view.findViewById(R.id.titleTextView);
            valueTextView = (TextView) view.findViewById(R.id.valueTextView);
            dividerImageView = (ImageView) view.findViewById(R.id.dividerImageView);

        }
    }


    public class SectionSpacingViewHolder extends RecyclerView.ViewHolder {
        public SectionSpacingViewHolder(View view) {
            super(view);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType) {
            case 0:
                View itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.setting_row_item, parent, false);
                return new SectionRowViewHolder(itemView);
            default:
                View sectionSpacingView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.activity_setting_section_spacing, parent, false);
                return new SectionSpacingViewHolder(sectionSpacingView);
        }

    }

    @Override
    public int getItemViewType(int position) {
        SettingItem item = items.get(position);
        if (item.itemType != SettingItem.SettingItemType.SETTING_TYPE_SECTION_SPACING) {
            return 0;
        } else {
            return 1;
        }
    }

    public SettingAdapter(List<SettingItem> titles) {
        this.items = titles;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        SettingItem item = items.get(position);
        if (item.itemType != SettingItem.SettingItemType.SETTING_TYPE_SECTION_SPACING) {
            SectionRowViewHolder rowViewHolder = (SectionRowViewHolder) holder;
            String title = item.title;
            String value = item.value;
            if (title != null) {
                rowViewHolder.titleTextView.setText(title);
            }
            if (value != null) {
                rowViewHolder.valueTextView.setText(value);
            }
            rowViewHolder.itemView.setBackgroundResource(item.backgroundColorID);
            if (item.showDivider){
                rowViewHolder.dividerImageView.setVisibility(View.VISIBLE);
            }else {
                rowViewHolder.dividerImageView.setVisibility(View.INVISIBLE);
            }
        } else {
            SectionSpacingViewHolder spacingViewHolder = (SectionSpacingViewHolder) holder;
            spacingViewHolder.itemView.setBackgroundResource(item.backgroundColorID);
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}
