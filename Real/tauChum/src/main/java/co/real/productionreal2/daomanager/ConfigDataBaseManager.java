package co.real.productionreal2.daomanager;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;


import co.real.productionreal2.dao.config.DBMain;
import co.real.productionreal2.dao.config.DBMainDao;
import co.real.productionreal2.dao.config.DaoMaster;
import co.real.productionreal2.dao.config.DaoSession;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import de.greenrobot.dao.async.AsyncOperation;
import de.greenrobot.dao.async.AsyncOperationListener;
import de.greenrobot.dao.async.AsyncSession;

/**
 * Created by hohojo on 20/7/2015.
 */
public class ConfigDataBaseManager implements AsyncOperationListener {
    private static ConfigDataBaseManager instance;
    /**
     * The Android Activity reference for access to DatabaseManager.
     */
    private Context context;
    private DaoMaster.DevOpenHelper mHelper;
    private SQLiteDatabase database;
    private DaoMaster daoMaster;
    private DaoSession daoSession;
    private AsyncSession asyncSession;
    private List<AsyncOperation> completedOperations;

    public static ConfigDataBaseManager getInstance(Context context) {

        if (instance == null) {
            instance = new ConfigDataBaseManager(context);
        }

        return instance;
    }

    public ConfigDataBaseManager(final Context context) {
        this.context = context;
        mHelper = new DaoMaster.DevOpenHelper(this.context, "Config-database", null);
        completedOperations = new CopyOnWriteArrayList<AsyncOperation>();
    }


    @Override
    public void onAsyncOperationCompleted(AsyncOperation operation) {
        completedOperations.add(operation);
    }

    private void assertWaitForCompletion1Sec() {
        asyncSession.waitForCompletion(1000);
        asyncSession.isCompleted();
    }

    /**
     * Query for readable DB
     */
    public void openReadableDb() throws SQLiteException {
        database = mHelper.getReadableDatabase();
        daoMaster = new DaoMaster(database);
        daoSession = daoMaster.newSession();
        asyncSession = daoSession.startAsyncSession();
        asyncSession.setListener(this);
    }

    /**
     * Query for writable DB
     */
    public void openWritableDb() throws SQLiteException {
        database = mHelper.getWritableDatabase();
        daoMaster = new DaoMaster(database);
        daoSession = daoMaster.newSession();
        asyncSession = daoSession.startAsyncSession();
        asyncSession.setListener(this);
    }

    public String getDbPath() {
        openWritableDb();
        return database.getPath();
    }

    public void closeDbConnections() {
        if (daoSession != null) {
            daoSession.clear();
            daoSession = null;
        }
        if (database != null && database.isOpen()) {
            database.close();
        }
        if (mHelper != null) {
            mHelper.close();
            mHelper = null;
        }
        if (instance != null) {
            instance = null;
        }
    }

    public synchronized void dropDatabase() {
        try {
            openWritableDb();
            DaoMaster.dropAllTables(database, true); // drops all tables
            mHelper.onCreate(database);              // creates the tables
            asyncSession.deleteAll(DBMain.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void dbActionMain(int memberId) {
        DBMain dbMain = new DBMain();
        if (needInsertMain(memberId)) {
            dbMain.setMemberID(memberId);
            dbMain.setStatus(true);
            insertMain(dbMain);
        }
    }

    private boolean needInsertMain(int memberId) {
        for (int i = 0; i < listMain().size(); i++) {
            if (memberId == listMain().get(i).getMemberID()) {
                return false;
            }
        }
        return true;
    }

    public synchronized void insertMain(DBMain main) {
        try {
            if (main != null) {
                openWritableDb();
                daoSession.insertOrReplace(main);
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized void updateMain(DBMain main) {
        try {
            if (main != null) {
                openWritableDb();
                daoSession.update(main);
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized List<DBMain> listMain() {
        List<DBMain> mains = null;
        try {
            openReadableDb();
            DBMainDao mainDao = daoSession.getDBMainDao();
            mains = mainDao.loadAll();

            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mains;
    }

    public synchronized DBMain getCurrentDbMain(int memId) {
        DBMain dbMain = null;
        try {
            openReadableDb();
            DBMainDao mainDao = daoSession.getDBMainDao();
            dbMain = mainDao.queryBuilder().where(DBMainDao.Properties.MemberID.eq(memId)).list().get(0);

            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dbMain;
    }



    public int getCurrentMemberId() {
        List<DBMain> mains = listMain();
        for (int i = 0; i < listMain().size(); i++) {
            if (mains.get(i).getStatus() == true) {
                return mains.get(i).getMemberID();
            }
        }
        return -1;
    }

//    public boolean isFollowing(int memId, int followingMemId) {
//        DBCurrentFollower currentFollower = null;
//        try {
//            openReadableDb();
//            DBCurrentFollowerDao dao = daoSession.getDBCurrentFollowerDao();
//            currentFollower = dao.queryBuilder().where(DBCurrentFollowerDao.Properties.MemberID.eq(memId),
//                    DBCurrentFollowerDao.Properties.FollowingMemberID.eq(followingMemId)).list().get(0);
//            daoSession.clear();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        boolean isFollowing = currentFollower == null ? false : true;
//        return isFollowing;
//    }


}
