package co.real.productionreal2.common;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.securepreferences.SecurePreferences;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import co.real.productionreal2.Constants;
import co.real.productionreal2.CoreData;
import co.real.productionreal2.activity.MainActivityTabBase;
import co.real.productionreal2.dao.account.DBChatroomAgentProfiles;
import co.real.productionreal2.dao.account.DBPhoneBook;
import co.real.productionreal2.daomanager.DatabaseManager;
import co.real.productionreal2.model.CurrentFollower;
import co.real.productionreal2.service.model.ActivityLog;
import co.real.productionreal2.service.model.AgentProfiles;
import co.real.productionreal2.service.model.Follower;
import co.real.productionreal2.service.model.ReMatchContact;
import co.real.productionreal2.service.model.request.RequestAddressSearch;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;
import co.real.productionreal2.setting.HistorySearchSetting;
import co.real.productionreal2.util.AppUtil;
import co.real.productionreal2.view.TabMenu;

/**
 * Created by kelvinsun on 21/1/16.
 */
public class LocalStorageHelper {
    private static final String TAG = "LocalStorageHelper";

    public static final String SHAREPREFERENCE_LATEST_FIVE_SEARCH_RESULT_LIST_KEY = "SHAREPREFERENCE_LATEST_FIVE_SEARCH_RESULT_LIST_KEY";
    public static final String SHAREPREFERENCE_LATEST_SEARCH_COUNT_KEY = "SHAREPREFERENCE_LATEST_SEARCH_COUNT_KEY";
    public static final String SHAREPREFERENCE_AGENT_PROFILE_LIST_KEY = "SHAREPREFERENCE_AGENT_PROFILE_LIST_KEY";
    public static final String SHAREPREFERENCE_AGENT_PROFILE_KEY = "SHAREPREFERENCE_AGENT_PROFILE_KEY";
    public static final String SHAREPREFERENCE_LATEST_ADDRESS_SEARCH_KEY = "SHAREPREFERENCE_LATEST_ADDRESS_SEARCH_KEY";

    public static final String SHAREPREFERENCE_CURRENT_SEARCH_COUNT_KEY = "SHAREPREFERENCE_CURRENT_SEARCH_COUNT_KEY";
    public static final String SHAREPREFERENCE_CURRENT_ADDRESS_SEARCH_NAME_KEY = "SHAREPREFERENCE_CURRENT_ADDRESS_SEARCH_NAME_KEY";

    public static final String SHAREPREFERENCE_ME_FOLLOWING_LIST_KEY = "SHAREPREFERENCE_ME_FOLLOWING_LIST_KEY";
    public static final String SHAREPREFERENCE_CURRENT_FOLLOWING_LIST_KEY = "SHAREPREFERENCE_CURRENT_FOLLOWING_LIST_KEY";
    public static final String SHAREPREFERENCE_ACTIVITY_LOG_LIST_KEY = "SHAREPREFERENCE_ACTIVITY_LOG_LIST_KEY";

    public static final String SHAREPREFERENCE_NEW_COUNT_KEY = "SHAREPREFERENCE_NEW_COUNT_KEY";
    public static final String SHAREPREFERENCE_FOLLOWER_COUNT_KEY = "SHAREPREFERENCE_FOLLOWER_COUNT_KEY";
    public static final String SHAREPREFERENCE_FOLLOWING_COUNT_KEY = "SHAREPREFERENCE_FOLLOWING_COUNT_KEY";

    public static final String SHAREPREFERENCE_LATEST_TAB_SECTION_KEY = "SHAREPREFERENCE_LATEST_TAB_SECTION_KEY";
    public static final String SHAREPREFERENCE_CURRENT_LOCALE_KEY = "SHAREPREFERENCE_CURRENT_LOCALE_KEY";

    public static final String SHAREPREFERENCE_PROPERTY_TYPE_KEY = "SHAREPREFERENCE_PROPERTY_TYPE_KEY";

    public static final String SHAREPREFERENCE_LANGINDEX = "SHAREPREFERENCE_LANGINDEX";

    public static final String SHAREPREFERENCE_CURRENTUSERID = "SHAREPREFERENCE_CURRENTUSERID";
    public static final String SHAREPREFERENCE_QBPW = "SHAREPREFERENCE_QBPW";
    public static final String SHAREPREFERENCE_ACCESSTOKEN = "SHAREPREFERENCE_ACCESSTOKEN";


    public static final String SHAREPREFERENCE_PHONEREGED = "SHAREPREFERENCE_PHONEREGED";
    public static final String SHAREPREFERENCE_REGED_PHONE_AREA_CODE = "SHAREPREFERENCE_REGED_PHONE_AREA_CODE";
    public static final String SHAREPREFERENCE_REGED_PHONE_NUMBER = "SHAREPREFERENCE_REGED_PHONE_NUMBER";

    public static final String SHAREPREFERENCE_CONNECT_AGENT_LIST_KEY = "SHAREPREFERENCE_CONNECT_AGENT_LIST_KEY";
    public static final String SHAREPREFERENCE_PHONE_BOOK_LIST_KEY = "SHAREPREFERENCE_PHONE_BOOK_LIST_KEY";


    public static Context mContext = null;
    static LocalStorageHelper sharedHelper = null;

    private static SharedPreferences securePreferences;

    static public synchronized LocalStorageHelper defaultHelper(Context context) {
        if (sharedHelper == null) {
            sharedHelper = new LocalStorageHelper(context);
        }

        mContext = context;

        return sharedHelper;
    }

    public LocalStorageHelper(Context context) {
        mContext = context;
    }

    /**
     * Local cache for the latest search results
     *
     * @return
     */
    public static ArrayList<RequestAddressSearch> getLatestSearchResultList(Context context) {
        SharedPreferences mPrefs = context.getSharedPreferences(SHAREPREFERENCE_LATEST_FIVE_SEARCH_RESULT_LIST_KEY, Context.MODE_PRIVATE);
        ArrayList<RequestAddressSearch> latestSearchResultsArrayList = new ArrayList<>();
        String serializedString = mPrefs.getString(SHAREPREFERENCE_LATEST_FIVE_SEARCH_RESULT_LIST_KEY, "");

        if (serializedString.length() > 0) {
            Gson gson = new Gson();
            Type type = new TypeToken<ArrayList<RequestAddressSearch>>() {
            }.getType();
            latestSearchResultsArrayList = gson.fromJson(serializedString, type);
        }
        return latestSearchResultsArrayList;
    }


    public static void saveLatestSearchResultListToLocalStorage(List<RequestAddressSearch> latestSearchResultsArrayList) {
        if (mContext == null)
            return;
        SharedPreferences mPrefs = mContext.getSharedPreferences(SHAREPREFERENCE_LATEST_FIVE_SEARCH_RESULT_LIST_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(latestSearchResultsArrayList);
        editor.putString(SHAREPREFERENCE_LATEST_FIVE_SEARCH_RESULT_LIST_KEY, json);
        editor.commit();
    }

    /**
     * Local cache for the agent profile list
     *
     * @param context
     * @return
     */
    public static List<AgentProfiles> getLocalAgentProfileList(Context context, boolean isAllFolloing) {
        List<AgentProfiles> agentProfilesArrayList=getLocalAgentProfileList(context, MainActivityTabBase.CURRENT_SECTION.NEWSFEED);
        List<AgentProfiles> agentProfilesFollowingArrayList = new ArrayList<AgentProfiles>();
        for (AgentProfiles agentProfiles : agentProfilesArrayList) {
            if (agentProfiles.isFollowing == 1) {
                agentProfilesFollowingArrayList.add(agentProfiles);
            }
        }
        return agentProfilesFollowingArrayList;
    }

    public static List<AgentProfiles> getLocalAgentProfileList(Context context ,MainActivityTabBase.CURRENT_SECTION SECTION) {
        SharedPreferences mPrefs = (context).getSharedPreferences(SHAREPREFERENCE_AGENT_PROFILE_LIST_KEY + "_" + SECTION.ordinal(), Context.MODE_PRIVATE);
        List<AgentProfiles> agentProfilesArrayList = new ArrayList<AgentProfiles>();
        String serializedString = mPrefs.getString(SHAREPREFERENCE_AGENT_PROFILE_LIST_KEY + "_" + SECTION.ordinal(), "");

        if (serializedString.length() > 0) {
            Gson gson = new Gson();
            Type type = new TypeToken<ArrayList<AgentProfiles>>() {
            }.getType();
            agentProfilesArrayList = gson.fromJson(serializedString, type);
        }
        return agentProfilesArrayList;
    }


    public static void saveLocalAgentProfileListToLocalStorage(Context context, List<AgentProfiles> agentProfilesArrayList, MainActivityTabBase.CURRENT_SECTION SECTION) {
        SharedPreferences mPrefs = ((Activity) context).getSharedPreferences(SHAREPREFERENCE_AGENT_PROFILE_LIST_KEY + "_" + SECTION.ordinal(), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(agentProfilesArrayList);
        editor.putString(SHAREPREFERENCE_AGENT_PROFILE_LIST_KEY + "_" + SECTION.ordinal(), json);
        editor.commit();
    }

    public static void addAgentProfileListToLocalStorage(Context context, AgentProfiles agentProfiles) {

        List<AgentProfiles> agentProfilesSearchArrayList = getLocalAgentProfileList(context, MainActivityTabBase.CURRENT_SECTION.ADDRESS_SEARCH);
        for (AgentProfiles mSearchAgentProfiles : agentProfilesSearchArrayList) {
            if (mSearchAgentProfiles.memberID == agentProfiles.memberID) {
                mSearchAgentProfiles.isFollowing = 1;
                mSearchAgentProfiles.followerCount+=1;
                break;
            }
        }
        saveLocalAgentProfileListToLocalStorage(context, agentProfilesSearchArrayList, MainActivityTabBase.CURRENT_SECTION.ADDRESS_SEARCH);


        List<AgentProfiles> agentProfilesArrayList = getLocalAgentProfileList(context, MainActivityTabBase.CURRENT_SECTION.NEWSFEED);
        boolean isExist = false;
        for (AgentProfiles mAgentProfiles : agentProfilesArrayList) {
            if (mAgentProfiles.memberID == agentProfiles.memberID) {
                isExist = true;
                mAgentProfiles.isFollowing = 1;
                mAgentProfiles.followerCount+=1;
                break;
            }
        }
        if (!isExist) {
            agentProfiles.isFollowing=1;
            agentProfiles.followerCount+=1;
            agentProfilesArrayList.add(agentProfiles);
        }
        //sort by creationDate
        Collections.sort(agentProfilesArrayList, new AgentProfiles.CompPosition());
        saveLocalAgentProfileListToLocalStorage(context, agentProfilesArrayList, MainActivityTabBase.CURRENT_SECTION.NEWSFEED);

        //update database for QB
        updateDataBaseIsFollowing(context, agentProfiles.memberID, true);
    }

    public static void addAgentProfileListToLocalStorage(Context context, int memId) {
        List<AgentProfiles> agentProfilesArrayList = getLocalAgentProfileList(context, MainActivityTabBase.CURRENT_SECTION.NEWSFEED);
        for (AgentProfiles agentProfiles : agentProfilesArrayList)
            if (agentProfiles.memberID == memId) {
                agentProfiles.isFollowing = 1;
                agentProfiles.followerCount+=1;
                break;
            }
        //sort by creationDate
        Collections.sort(agentProfilesArrayList, new AgentProfiles.CompPosition());
        saveLocalAgentProfileListToLocalStorage(context, agentProfilesArrayList, MainActivityTabBase.CURRENT_SECTION.NEWSFEED);
    }

    public static void removeAgentProfileListToLocalStorage(Context context, int memId) {

        List<AgentProfiles> agentProfilesSearchArrayList = getLocalAgentProfileList(context, MainActivityTabBase.CURRENT_SECTION.ADDRESS_SEARCH);
        for (AgentProfiles mSearchAgentProfiles : agentProfilesSearchArrayList) {
            if (mSearchAgentProfiles.memberID == memId) {
                mSearchAgentProfiles.isFollowing = 0;
                mSearchAgentProfiles.followerCount=mSearchAgentProfiles.followerCount-1;
                break;
            }
        }
        saveLocalAgentProfileListToLocalStorage(context, agentProfilesSearchArrayList, MainActivityTabBase.CURRENT_SECTION.ADDRESS_SEARCH);

        List<AgentProfiles> agentProfilesArrayList = getLocalAgentProfileList(context, MainActivityTabBase.CURRENT_SECTION.NEWSFEED);
        for (AgentProfiles mAgentProfile : agentProfilesArrayList)
            if (mAgentProfile.memberID == memId) {
                mAgentProfile.isFollowing = 0;
                mAgentProfile.followerCount=mAgentProfile.followerCount-1;
                //update database for QB
                updateDataBaseIsFollowing(context,mAgentProfile.memberID , false);
                break;
            }
        saveLocalAgentProfileListToLocalStorage(context, agentProfilesArrayList, MainActivityTabBase.CURRENT_SECTION.NEWSFEED);


    }

    public static boolean checkIfAgentIsFollowing(Context context, int agentProfileId) {
        List<AgentProfiles> agentProfilesArrayList = getLocalAgentProfileList(context, true);
        for (AgentProfiles mAgentProfiles : agentProfilesArrayList)
            if (mAgentProfiles.memberID == agentProfileId) {
                return true;
            }
        return false;
    }


    private static void updateDataBaseIsFollowing(Context context, int memId, boolean isFollowing) {
        //update database for QB
        DBChatroomAgentProfiles dbChatroomAgentProfiles = DatabaseManager.getInstance(context).getChatroomAgentProfilesByMemId(memId);
        if (dbChatroomAgentProfiles != null)
        {
            dbChatroomAgentProfiles.setIsFollowing(isFollowing ? 1 : 0);
            DatabaseManager.getInstance(context).updateChatroomAgentProfilesIsFollowing(dbChatroomAgentProfiles);
        }
    }

    /**
     * Local cache for the latest address search
     *
     * @param context
     * @return
     */
    public static String getLatestAddressSearch(Context context) {
        SharedPreferences mPrefs = (context).getSharedPreferences(SHAREPREFERENCE_LATEST_ADDRESS_SEARCH_KEY, Context.MODE_PRIVATE);
        return mPrefs.getString(SHAREPREFERENCE_LATEST_ADDRESS_SEARCH_KEY, "");
    }


    public static void saveLatestAddressSearchToLocalStorage(Context context, String latestAddressName) {
        SharedPreferences mPrefs = ((Activity) context).getSharedPreferences(SHAREPREFERENCE_LATEST_ADDRESS_SEARCH_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putString(SHAREPREFERENCE_LATEST_ADDRESS_SEARCH_KEY, latestAddressName);
        editor.commit();
    }

    /**
     * Local cache for the current address search
     *
     * @param context
     * @return
     */
    public static RequestAddressSearch getCurrentAddressSearch(Context context) {
        SharedPreferences mPrefs = (context).getSharedPreferences(SHAREPREFERENCE_CURRENT_ADDRESS_SEARCH_NAME_KEY, Context.MODE_PRIVATE);
        String serializedString = mPrefs.getString(SHAREPREFERENCE_CURRENT_ADDRESS_SEARCH_NAME_KEY, "");
        Gson gson = new Gson();
        Type type = new TypeToken<RequestAddressSearch>() {
        }.getType();
        RequestAddressSearch requestAddressSearch = gson.fromJson(serializedString, type);
        return requestAddressSearch;
    }


    public static void saveCurrentAddressSearchToLocalStorage(Context context, RequestAddressSearch currentAddressName) {
        SharedPreferences mPrefs = ((Activity) context).getSharedPreferences(SHAREPREFERENCE_CURRENT_ADDRESS_SEARCH_NAME_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        // Convert the object to a JSON string
        prefsEditor.clear().commit();
        String json = new Gson().toJson(currentAddressName);
        prefsEditor.putString(SHAREPREFERENCE_CURRENT_ADDRESS_SEARCH_NAME_KEY, json).apply();
        prefsEditor.commit();
    }

    /**
     * Local cache for current search count
     *
     * @return
     */
    public static int getCurrentSearchCount() {
        SharedPreferences mPrefs = mContext.getSharedPreferences(SHAREPREFERENCE_CURRENT_SEARCH_COUNT_KEY, Context.MODE_PRIVATE);
        return mPrefs.getInt(SHAREPREFERENCE_CURRENT_SEARCH_COUNT_KEY, 0);
    }

    public static void saveCurrentSearchCountToLocalStorage(int totalCount) {
        SharedPreferences mPrefs = mContext.getSharedPreferences(SHAREPREFERENCE_CURRENT_SEARCH_COUNT_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        prefsEditor.clear().commit();
        prefsEditor.putInt(SHAREPREFERENCE_CURRENT_SEARCH_COUNT_KEY, totalCount).apply();
        prefsEditor.commit();
    }

    /**
     * Local cache for the agent profile list
     *
     * @param context
     * @return
     */
    public static Locale getCurrentLocale(Context context) {
//        return AppUtil.getCurrentLangLocale(context);
        SharedPreferences mPrefs = (context).getSharedPreferences(SHAREPREFERENCE_CURRENT_LOCALE_KEY, Context.MODE_PRIVATE);
        String localeKeyJson = mPrefs.getString(SHAREPREFERENCE_CURRENT_LOCALE_KEY, "");
        if (localeKeyJson.isEmpty())
            return null;
        Gson gson = new Gson();
        Locale currentLocale = gson.fromJson(localeKeyJson, Locale.class);

        return currentLocale;
    }


    public static void saveCurrentLocaleToLocalStorage(Context context, Locale locale) {
        SharedPreferences mPrefs = ((Activity) context).getSharedPreferences(SHAREPREFERENCE_CURRENT_LOCALE_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(locale);
        editor.putString(SHAREPREFERENCE_CURRENT_LOCALE_KEY, json);
        editor.commit();

        CoreData.langIndex = AppUtil.getLangIndexFromLocale(context, context.getResources().getConfiguration().locale);
        LocalStorageHelper.setLangIndex(context, CoreData.langIndex);
    }


    /**
     * Local cache for an agent profile
     *
     * @param mContext
     * @return
     */
    public static AgentProfiles getLocalAgentProfiles(Context mContext, MainActivityTabBase.CURRENT_SECTION SECTION) {
        SharedPreferences mPrefs = ((Activity) mContext).getSharedPreferences(SHAREPREFERENCE_AGENT_PROFILE_KEY + "_" + SECTION.ordinal(), Context.MODE_PRIVATE);
        String serializedString = mPrefs.getString(SHAREPREFERENCE_AGENT_PROFILE_KEY + "_" + SECTION.ordinal(), "");
        Gson gson = new Gson();
        Type type = new TypeToken<AgentProfiles>() {
        }.getType();
        AgentProfiles agentProfiles = gson.fromJson(serializedString, type);
        return agentProfiles;
    }


    public static void saveAgentProfilesToLocalStorage(Context context, AgentProfiles agentProfiles,  MainActivityTabBase.CURRENT_SECTION SECTION) {
        SharedPreferences mPrefs = ((Activity) context).getSharedPreferences(SHAREPREFERENCE_AGENT_PROFILE_KEY + "_" + SECTION.ordinal(), Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        // Convert the object to a JSON string
        prefsEditor.clear().commit();
        String json = new Gson().toJson(agentProfiles);
        prefsEditor.putString(SHAREPREFERENCE_AGENT_PROFILE_KEY + "_" + SECTION.ordinal(), json).apply();
        prefsEditor.commit();
    }


    /**
     * Local cache for the activity log
     *
     * @return
     */
    public static ArrayList<ActivityLog> getActivityLogList(Context context) {
        SharedPreferences mPrefs = context.getSharedPreferences(SHAREPREFERENCE_ACTIVITY_LOG_LIST_KEY, Context.MODE_PRIVATE);
        ArrayList<ActivityLog> activityLogArrayList = new ArrayList<>();
        String serializedString = mPrefs.getString(SHAREPREFERENCE_ACTIVITY_LOG_LIST_KEY, "");

        if (serializedString.length() > 0) {
            Gson gson = new Gson();
            Type type = new TypeToken<ArrayList<ActivityLog>>() {
            }.getType();
            activityLogArrayList = gson.fromJson(serializedString, type);
        }
        return activityLogArrayList;
    }


    public static void saveActivityLogListToLocalStorage(ArrayList<ActivityLog> activityLogArrayList) {
        if (mContext == null)
            return;
        SharedPreferences mPrefs = mContext.getSharedPreferences(SHAREPREFERENCE_ACTIVITY_LOG_LIST_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(activityLogArrayList);
        editor.putString(SHAREPREFERENCE_ACTIVITY_LOG_LIST_KEY, json);
        editor.commit();
    }


    /**
     * Local cache for current following list
     *
     * @return
     */
    public static ArrayList<CurrentFollower> getCurrentFollowerList(Context context) {
        SharedPreferences mPrefs = context.getSharedPreferences(SHAREPREFERENCE_CURRENT_FOLLOWING_LIST_KEY, Context.MODE_PRIVATE);
        ArrayList<CurrentFollower> followerList = new ArrayList<>();
        String serializedString = mPrefs.getString(SHAREPREFERENCE_CURRENT_FOLLOWING_LIST_KEY, "");

        if (serializedString.length() > 0) {
            Gson gson = new Gson();
            Type type = new TypeToken<ArrayList<CurrentFollower>>() {
            }.getType();
            followerList = gson.fromJson(serializedString, type);
        }
        return followerList;
    }


    public static void saveCurrentFollowingListToLocalStorage(ArrayList<CurrentFollower> followingList) {
        if (mContext == null)
            return;
        SharedPreferences mPrefs = mContext.getSharedPreferences(SHAREPREFERENCE_CURRENT_FOLLOWING_LIST_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(followingList);
        editor.putString(SHAREPREFERENCE_CURRENT_FOLLOWING_LIST_KEY, json);
        editor.commit();
    }


    /**
     * Local cache for the following list in me page
     *
     * @return
     */
    public static ArrayList<Follower> getMeFollowingList(Context context) {
        SharedPreferences mPrefs = context.getSharedPreferences(SHAREPREFERENCE_ME_FOLLOWING_LIST_KEY, Context.MODE_PRIVATE);
        ArrayList<Follower> followingList = new ArrayList<>();
        String serializedString = mPrefs.getString(SHAREPREFERENCE_ME_FOLLOWING_LIST_KEY, "");

        if (serializedString.length() > 0) {
            Gson gson = new Gson();
            Type type = new TypeToken<ArrayList<Follower>>() {
            }.getType();
            followingList = gson.fromJson(serializedString, type);
        }
        return followingList;
    }
//

    public static void saveMeFollowingListToLocalStorage(ArrayList<Follower> followingList) {
        if (mContext == null)
            return;
        SharedPreferences mPrefs = mContext.getSharedPreferences(SHAREPREFERENCE_ME_FOLLOWING_LIST_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(followingList);
        editor.putString(SHAREPREFERENCE_ME_FOLLOWING_LIST_KEY, json);
        editor.commit();
    }


    /**
     * Local cache for tab alert
     *
     * @return
     */
    public static int getNewCount(TabMenu.TAB_SECTION section) {
        SharedPreferences mPrefs = mContext.getSharedPreferences(SHAREPREFERENCE_NEW_COUNT_KEY + "_" + section.toString(), Context.MODE_PRIVATE);
        return mPrefs.getInt(SHAREPREFERENCE_NEW_COUNT_KEY + "_" + section.toString(), 0);
    }

    public static void saveNewCountToLocalStorage(int newCount, TabMenu.TAB_SECTION section) {
        SharedPreferences mPrefs = mContext.getSharedPreferences(SHAREPREFERENCE_NEW_COUNT_KEY + "_" + section.toString(), Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        prefsEditor.clear().commit();
        prefsEditor.putInt(SHAREPREFERENCE_NEW_COUNT_KEY + "_" + section.toString(), newCount).apply();
        prefsEditor.commit();
    }

    /**
     * Local cache for follower count
     *
     * @return
     */
    public static int getFollowerCount() {
        SharedPreferences mPrefs = mContext.getSharedPreferences(SHAREPREFERENCE_FOLLOWER_COUNT_KEY, Context.MODE_PRIVATE);
        return mPrefs.getInt(SHAREPREFERENCE_FOLLOWER_COUNT_KEY, 0);
    }

    public static void saveFollowerCountToLocalStorage(int count) {
        SharedPreferences mPrefs = mContext.getSharedPreferences(SHAREPREFERENCE_FOLLOWER_COUNT_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        prefsEditor.clear().commit();
        prefsEditor.putInt(SHAREPREFERENCE_FOLLOWER_COUNT_KEY, count).apply();
        prefsEditor.commit();
    }

    /**
     * Local cache for following count
     *
     * @return
     */
    public static int getFollowingCount() {
        SharedPreferences mPrefs = mContext.getSharedPreferences(SHAREPREFERENCE_FOLLOWING_COUNT_KEY, Context.MODE_PRIVATE);
        return mPrefs.getInt(SHAREPREFERENCE_FOLLOWING_COUNT_KEY, 0);
    }

    public static void saveFollowingCountToLocalStorage(int count) {
        SharedPreferences mPrefs = mContext.getSharedPreferences(SHAREPREFERENCE_FOLLOWING_COUNT_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        prefsEditor.clear().commit();
        prefsEditor.putInt(SHAREPREFERENCE_FOLLOWING_COUNT_KEY, count).apply();
        prefsEditor.commit();
    }


    /**
     * Local cache for latest tab section
     *
     * @return
     */
    public static int getLatestTabSectionIndex() {
        SharedPreferences mPrefs = mContext.getSharedPreferences(SHAREPREFERENCE_LATEST_TAB_SECTION_KEY, Context.MODE_PRIVATE);
        return mPrefs.getInt(SHAREPREFERENCE_LATEST_TAB_SECTION_KEY, 0);
    }

    public static void saveLatestTabSectionIndexToLocalStorage(int newCount) {
        SharedPreferences mPrefs = mContext.getSharedPreferences(SHAREPREFERENCE_LATEST_TAB_SECTION_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        prefsEditor.clear().commit();
        prefsEditor.putInt(SHAREPREFERENCE_LATEST_TAB_SECTION_KEY, newCount).apply();
        prefsEditor.commit();
    }

    /**
     * Local cache for latest search count
     *
     * @return
     */
    public static int getLatestSearchCount() {
        SharedPreferences mPrefs = mContext.getSharedPreferences(SHAREPREFERENCE_LATEST_SEARCH_COUNT_KEY, Context.MODE_PRIVATE);
        return mPrefs.getInt(SHAREPREFERENCE_LATEST_SEARCH_COUNT_KEY, 0);
    }

    public static void saveLatestSearchCountToLocalStorage(int totalCount) {
        SharedPreferences mPrefs = mContext.getSharedPreferences(SHAREPREFERENCE_LATEST_SEARCH_COUNT_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        prefsEditor.clear().commit();
        prefsEditor.putInt(SHAREPREFERENCE_LATEST_SEARCH_COUNT_KEY, totalCount).apply();
        prefsEditor.commit();
    }

    /**
     * save property type in hashmap
     */
    public static void savePropertyTypeHashMap(Context context, HashMap<String, String> inputMap) {
        SharedPreferences pSharedPref = context.getSharedPreferences(SHAREPREFERENCE_PROPERTY_TYPE_KEY, Context.MODE_PRIVATE);
        if (pSharedPref != null) {
            JSONObject jsonObject = new JSONObject(inputMap);
            String jsonString = jsonObject.toString();
            SharedPreferences.Editor editor = pSharedPref.edit();
            editor.remove(SHAREPREFERENCE_PROPERTY_TYPE_KEY).commit();
            editor.putString(SHAREPREFERENCE_PROPERTY_TYPE_KEY, jsonString);
            editor.commit();
        }
    }

    public static HashMap<String, String> loadPropertyTypeHashMap(Context context) {
        HashMap<String, String> outputMap = new HashMap<String, String>();
        SharedPreferences pSharedPref = context.getSharedPreferences(SHAREPREFERENCE_PROPERTY_TYPE_KEY, Context.MODE_PRIVATE);
        try {
            if (pSharedPref != null) {
                String jsonString = pSharedPref.getString(SHAREPREFERENCE_PROPERTY_TYPE_KEY, (new JSONObject()).toString());
                JSONObject jsonObject = new JSONObject(jsonString);
                Iterator<String> keysItr = jsonObject.keys();
                while (keysItr.hasNext()) {
                    String key = keysItr.next();
                    String value = (String) jsonObject.get(key);
                    outputMap.put(key, value);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return outputMap;
    }



//    /**
//     * Local cache for the invited phone number list
//     *
//     * @return
//     */
//    public static List<String> getInvitedPhoneNoList(Context context) {
//        SharedPreferences mPrefs = context.getSharedPreferences(SHAREPREFERENCE_INVITED_PHONE_NO_LIST_KEY, Context.MODE_PRIVATE);
//        List<String> invitedPhoneNoList = new ArrayList<>();
//        String serializedString = mPrefs.getString(SHAREPREFERENCE_INVITED_PHONE_NO_LIST_KEY, "");
//
//        if (serializedString.length() > 0) {
//            Gson gson = new Gson();
//            Type type = new TypeToken<ArrayList<String>>() {
//            }.getType();
//            invitedPhoneNoList = gson.fromJson(serializedString, type);
//        }
//        return invitedPhoneNoList;
//    }
//
//
//    public static void saveInvitedPhoneNoListToLocalStorage(List<String> invitedPhoneNoList) {
//        if (mContext == null)
//            return;
//        SharedPreferences mPrefs = mContext.getSharedPreferences(SHAREPREFERENCE_INVITED_PHONE_NO_LIST_KEY, Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor = mPrefs.edit();
//        Gson gson = new Gson();
//        String json = gson.toJson(invitedPhoneNoList);
//        editor.putString(SHAREPREFERENCE_INVITED_PHONE_NO_LIST_KEY, json);
//        editor.commit();
//    }


    /**
     *  Local cache for the invited phone number list
     *
     * @return
     */
    public static ArrayList<ReMatchContact> getConnectAgentList(Context context) {
        SharedPreferences mPrefs = context.getSharedPreferences(SHAREPREFERENCE_CONNECT_AGENT_LIST_KEY, Context.MODE_PRIVATE);
        ArrayList<ReMatchContact> agentList = new ArrayList<>();
        String serializedString = mPrefs.getString(SHAREPREFERENCE_CONNECT_AGENT_LIST_KEY, "");

        if (serializedString.length() > 0) {
            Gson gson = new Gson();
            Type type = new TypeToken<ArrayList<ReMatchContact>>() {
            }.getType();
            agentList = gson.fromJson(serializedString, type);
        }
        return agentList;
    }
//

    public static void saveConnectAgentListToLocalStorage(Context context,ArrayList<ReMatchContact> agentList) {
        if (context == null)
            return;
        SharedPreferences mPrefs = context.getSharedPreferences(SHAREPREFERENCE_CONNECT_AGENT_LIST_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(agentList);
        editor.putString(SHAREPREFERENCE_CONNECT_AGENT_LIST_KEY, json);
        editor.commit();
    }

    /**
     *  Local cache for the phone book list
     *
     * @return
     */
    public static ArrayList<DBPhoneBook> getPhoneBookList(Context context) {
        SharedPreferences mPrefs = context.getSharedPreferences(SHAREPREFERENCE_PHONE_BOOK_LIST_KEY, Context.MODE_PRIVATE);
        ArrayList<DBPhoneBook> agentList = new ArrayList<>();
        String serializedString = mPrefs.getString(SHAREPREFERENCE_PHONE_BOOK_LIST_KEY, "");

        if (serializedString.length() > 0) {
            Gson gson = new Gson();
            Type type = new TypeToken<ArrayList<DBPhoneBook>>() {
            }.getType();
            agentList = gson.fromJson(serializedString, type);
        }
        return agentList;
    }
//

    public static void savePhonebookListToLocalStorage(Context context,List<DBPhoneBook> agentList) {
        if (context == null)
            return;
        SharedPreferences mPrefs = context.getSharedPreferences(SHAREPREFERENCE_PHONE_BOOK_LIST_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(agentList);
        editor.putString(SHAREPREFERENCE_PHONE_BOOK_LIST_KEY, json);
        editor.commit();
    }



    public static void setLangIndex(Context context, int i) {
        SharedPreferences mPrefs = context.getSharedPreferences(SHAREPREFERENCE_LANGINDEX, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putInt(SHAREPREFERENCE_LANGINDEX, i);
        editor.commit();
    }

    public static int getLangIndex(Context context) {
        SharedPreferences mPrefs = context.getSharedPreferences(SHAREPREFERENCE_LANGINDEX, Context.MODE_PRIVATE);
        return mPrefs.getInt(SHAREPREFERENCE_LANGINDEX, -1);
    }

    public static void setCurrentUserID(Context context, int id) {
        getSecurePreferences(context).edit().putInt(SHAREPREFERENCE_CURRENTUSERID, id).commit();
    }

    public static int getCurrentUserID(Context context) {
        return getSecurePreferences(context).getInt(SHAREPREFERENCE_CURRENTUSERID, -1);
    }

    public static void setQBpw(Context context, String pw) {
        getSecurePreferences(context).edit().putString(SHAREPREFERENCE_QBPW, pw).commit();
    }

    public static String getQBpw(Context context) {
        return getSecurePreferences(context).getString(SHAREPREFERENCE_QBPW, "");
    }

    public static void setAccessToken(Context context, String pw) {
        getSecurePreferences(context).edit().putString(SHAREPREFERENCE_ACCESSTOKEN, pw).commit();
    }

    public static String getAccessToken(Context context) {
        return getSecurePreferences(context).getString(SHAREPREFERENCE_ACCESSTOKEN, "");
    }

    public static void setRealNetworkJson(Context context, String jsonString) {
        getSecurePreferences(context).edit().putString(Constants.PREF_REAL_NETWORK_JSON, jsonString).commit();
    }

    public static String getRealNetworkJson(Context context) {
        return getSecurePreferences(context).getString(Constants.PREF_REAL_NETWORK_JSON, "");
    }


    public static void setRegedPhoneAeraCode(Context context, String phoneNumber) {
        getSecurePreferences(context).edit().putString(SHAREPREFERENCE_REGED_PHONE_AREA_CODE, phoneNumber).commit();
    }

    public static String getRegedPhoneAeraCode(Context context) {
        return getSecurePreferences(context).getString(SHAREPREFERENCE_REGED_PHONE_AREA_CODE, "");
    }

    public static void setRegedPhoneNumber(Context context, String phoneNumber) {
        getSecurePreferences(context).edit().putString(SHAREPREFERENCE_REGED_PHONE_NUMBER, phoneNumber).commit();
    }

    public static String getRegedPhoneNumber(Context context) {
        return getSecurePreferences(context).getString(SHAREPREFERENCE_REGED_PHONE_NUMBER, "");
    }


    public static void initSecurePreferences(Context context){
        securePreferences = new SecurePreferences(context, "u_pw", "s_user_prefs.xml");
    }

    private static SharedPreferences getSecurePreferences(Context context){
        if(securePreferences == null){
            initSecurePreferences(context);
        }
        return securePreferences;
    }

    public static void setUpByLogin(Context context, ResponseLoginSocial.Content content) {
        setCurrentUserID(context, content.memId);
        setQBpw(context, content.qbPwd);
    }

    public static void removeAll() {

        saveNewCountToLocalStorage(0, TabMenu.TAB_SECTION.NEWSFEED);
        saveNewCountToLocalStorage(0, TabMenu.TAB_SECTION.ME);

        //clear CoreData
        CoreData.isAgentProfileUpdate = false;
        CoreData.langIndex = -1;

        //clear latest five search result from memory
        HistorySearchSetting.getInstance().resetValues();
        //clear SharedPreferences
        mContext.getSharedPreferences(SHAREPREFERENCE_LATEST_FIVE_SEARCH_RESULT_LIST_KEY, Context.MODE_PRIVATE).edit().clear().commit();
        mContext.getSharedPreferences(SHAREPREFERENCE_LATEST_ADDRESS_SEARCH_KEY, Context.MODE_PRIVATE).edit().clear().commit();
        mContext.getSharedPreferences(SHAREPREFERENCE_LATEST_SEARCH_COUNT_KEY, Context.MODE_PRIVATE).edit().clear().commit();
        mContext.getSharedPreferences(SHAREPREFERENCE_CURRENT_ADDRESS_SEARCH_NAME_KEY, Context.MODE_PRIVATE).edit().clear().commit();
        mContext.getSharedPreferences(SHAREPREFERENCE_CURRENT_SEARCH_COUNT_KEY, Context.MODE_PRIVATE).edit().clear().commit();
        mContext.getSharedPreferences(SHAREPREFERENCE_ACTIVITY_LOG_LIST_KEY, Context.MODE_PRIVATE).edit().clear().commit();
        mContext.getSharedPreferences(SHAREPREFERENCE_AGENT_PROFILE_KEY + "_0", Context.MODE_PRIVATE).edit().clear().commit();
        mContext.getSharedPreferences(SHAREPREFERENCE_AGENT_PROFILE_KEY + "_1", Context.MODE_PRIVATE).edit().clear().commit();
        mContext.getSharedPreferences(SHAREPREFERENCE_AGENT_PROFILE_LIST_KEY + "_0", Context.MODE_PRIVATE).edit().clear().commit();
        mContext.getSharedPreferences(SHAREPREFERENCE_AGENT_PROFILE_LIST_KEY + "_1", Context.MODE_PRIVATE).edit().clear().commit();
        mContext.getSharedPreferences(SHAREPREFERENCE_AGENT_PROFILE_KEY, Context.MODE_PRIVATE).edit().clear().commit();
        mContext.getSharedPreferences(SHAREPREFERENCE_ME_FOLLOWING_LIST_KEY, Context.MODE_PRIVATE).edit().clear().commit();
        mContext.getSharedPreferences(SHAREPREFERENCE_FOLLOWER_COUNT_KEY, Context.MODE_PRIVATE).edit().clear().commit();
        mContext.getSharedPreferences(SHAREPREFERENCE_FOLLOWING_COUNT_KEY, Context.MODE_PRIVATE).edit().clear().commit();
        mContext.getSharedPreferences(Constants.PREF_REAL_NETWORK, Context.MODE_PRIVATE).edit().clear().commit();
        mContext.getSharedPreferences(SHAREPREFERENCE_LANGINDEX, Context.MODE_PRIVATE).edit().clear().commit();
        mContext.getSharedPreferences(SHAREPREFERENCE_CONNECT_AGENT_LIST_KEY, Context.MODE_PRIVATE).edit().clear().commit();
        mContext.getSharedPreferences(SHAREPREFERENCE_PHONE_BOOK_LIST_KEY,Context.MODE_PRIVATE).edit().clear().commit();

        getSecurePreferences(mContext).edit().clear().commit();

    }
}
