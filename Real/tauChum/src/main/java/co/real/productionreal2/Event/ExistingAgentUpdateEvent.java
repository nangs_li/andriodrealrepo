package co.real.productionreal2.Event;

import java.util.ArrayList;

import co.real.productionreal2.service.model.Follower;
import co.real.productionreal2.service.model.ReMatchContact;

/**
 * Created by kelvinsun on 31/5/16.
 */
public class ExistingAgentUpdateEvent {
        public final ArrayList<ReMatchContact> agentList;

        public ExistingAgentUpdateEvent(ArrayList<ReMatchContact> agentList) {
            this.agentList = agentList;
        }
}
