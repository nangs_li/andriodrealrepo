package co.real.productionreal2.util;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

/**
 * Created by kelvinsun on 27/4/16.
 */
public class PermissionUtil {

    public static final int MY_PERMISSIONS_REQUEST_READ_PHONE_STATUS = 10;
    public static final int MY_PERMISSIONS_REQUEST_READ_PHONE_STATUS_FB=11;
    public static final int MY_PERMISSIONS_REQUEST_EXTERNAL_STORAGE=9;
    public static final int MY_PERMISSIONS_REQUEST_READ_CONTACT=8;

    public static void checkPhoneStatusPermission(Context context) {
        //request permission for OS > 6.0
        if (ContextCompat.checkSelfPermission(context,
                Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            // No explanation needed, we can request the permission.
            ActivityCompat.requestPermissions((Activity)context,
                    new String[]{Manifest.permission.READ_PHONE_STATE},
                    MY_PERMISSIONS_REQUEST_READ_PHONE_STATUS);

            // MY_PERMISSIONS_REQUEST_READ_PHONE_STATUS is an
            // app-defined int constant. The callback method gets the
            // result of the request.
            return;
        }
    }
}
