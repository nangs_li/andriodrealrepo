package co.real.productionreal2.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import co.real.productionreal2.R;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by hohojo on 6/10/2015.
 */
public class ToCreatePostFragment extends Fragment {
    private static final String TAG = "ToCreatePostFragment";
    @InjectView(R.id.apartmentBtn)
    Button apartmentBtn;
    @InjectView(R.id.unitBtn)
    Button unitBtn;
    @InjectView(R.id.townhouseBtn)
    Button townhouseBtn;
    @InjectView(R.id.loftBtn)
    Button loftBtn;
    @InjectView(R.id.houseboatBtn)
    Button houseboatBtn;
    @InjectView(R.id.hotelBtn)
    Button hotelBtn;

    int[] offButtonDrawableId = {R.drawable.apartment_off, R.drawable.unit_off, R.drawable.townhouse_off,
            R.drawable.loft_off, R.drawable.houseboat_off, R.drawable.hotel_off};

    Button[] buttons;

    public static ToCreatePostFragment newInstance() {
        Bundle args = new Bundle();
        ToCreatePostFragment fragment = new ToCreatePostFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.w(TAG, "onCreateView");
        View view = inflater.inflate(R.layout.to_create_post_fragment, container, false);
        ButterKnife.inject(this, view);
        initView();
        return view;
    }
    private void initView() {
        buttons = new Button[]{apartmentBtn, unitBtn, townhouseBtn, loftBtn, houseboatBtn, hotelBtn};
        offAllBtn();
    }

    private void offAllBtn() {
        for (int i = 0; i < buttons.length; i++) {
            buttons[i].setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(offButtonDrawableId[i]), null, null);
            buttons[i].setTextColor(getResources().getColor(R.color.dark_grey));
        }
    }

    @Override
    public void onResume() {
        Log.w(TAG, "onResume");
        super.onResume();
    }
}
