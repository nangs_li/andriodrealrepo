package co.real.productionreal2.activity.createpost;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import co.real.productionreal2.Constants;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.BaseActivity;

public class CreatePostSelectCountryActivity extends BaseActivity {
    @InjectView(R.id.rootImageView)
    ImageView rootImageView;
    @InjectView(R.id.lvCountry)
    ListView lvCountry;
    private SelectCountryAdapter selectCountryAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_post_select_country);
        ButterKnife.inject(this);
        initView();
    }

    private void initView() {
        Intent intent = getIntent();
        if (intent.getStringArrayListExtra(Constants.EXTRA_CREATE_POST_GOOGLE_ADDRESS_LIST) != null) {
            ArrayList<String> formattedAddressList = intent.getStringArrayListExtra(Constants.EXTRA_CREATE_POST_GOOGLE_ADDRESS_LIST);
            selectCountryAdapter = new SelectCountryAdapter(this, formattedAddressList);
            lvCountry.setAdapter(selectCountryAdapter);
            lvCountry.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent  = new Intent();
                    intent.putExtra(Constants.EXTRA_CREATE_POST_GOOGLE_ADDRESS_SELECT_INDEX, position);
                    setResult(RESULT_OK, intent);
                    finish();
                }
            });
        }

    }

    @OnClick(R.id.backBtn)
    public void backBtnClick(View view) {
        exitActivity();
    }

    @OnClick(R.id.closeBtn)
    public void closeBtnClick(View view) {
        exitActivity();
    }

    private void exitActivity() {
        setResult(RESULT_CANCELED);
        finish();
    }

    private class SelectCountryAdapter extends BaseAdapter {
        private ViewHolder holder;
        List<String> formattedAddressList;
        private Context context;

        public SelectCountryAdapter(Context context, List<String> formattedAddressList) {
            this.context = context;
            this.formattedAddressList = formattedAddressList;
        }


        @Override
        public int getCount() {
            return formattedAddressList.size();
        }

        @Override
        public Object getItem(int position) {
            return formattedAddressList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.list_item_single_choice, null);
                holder = createViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.checkedTextView.setText(formattedAddressList.get(position));
            return convertView;
        }

        private ViewHolder createViewHolder(View v) {
            ViewHolder holder = new ViewHolder();
            holder.checkedTextView = (CheckedTextView) v.findViewById(R.id.checkedTextView);
            return holder;
        }
    }

    private static class ViewHolder {
        public CheckedTextView checkedTextView;
    }
}
