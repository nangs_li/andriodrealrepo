package co.real.productionreal2.dao.config;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT. Enable "keep" sections if you want to edit. 
/**
 * Entity mapped to table DBCURRENT_FOLLOWER.
 */
public class DBCurrentFollower {

    private Long id;
    private Integer MemberID;
    private Integer FollowingMemberID;
    private Boolean IsFollowing;

    public DBCurrentFollower() {
    }

    public DBCurrentFollower(Long id) {
        this.id = id;
    }

    public DBCurrentFollower(Long id, Integer MemberID, Integer FollowingMemberID, Boolean IsFollowing) {
        this.id = id;
        this.MemberID = MemberID;
        this.FollowingMemberID = FollowingMemberID;
        this.IsFollowing = IsFollowing;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getMemberID() {
        return MemberID;
    }

    public void setMemberID(Integer MemberID) {
        this.MemberID = MemberID;
    }

    public Integer getFollowingMemberID() {
        return FollowingMemberID;
    }

    public void setFollowingMemberID(Integer FollowingMemberID) {
        this.FollowingMemberID = FollowingMemberID;
    }

    public Boolean getIsFollowing() {
        return IsFollowing;
    }

    public void setIsFollowing(Boolean IsFollowing) {
        this.IsFollowing = IsFollowing;
    }

}
