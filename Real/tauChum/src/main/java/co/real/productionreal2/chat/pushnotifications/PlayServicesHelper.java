package co.real.productionreal2.chat.pushnotifications;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;

import co.real.productionreal2.ApplicationSingleton;
import co.real.productionreal2.CoreData;
import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.common.LocalStorageHelper;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.request.RequestUpdateDeviceToken;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;

public class PlayServicesHelper {

	private static final String PROPERTY_APP_VERSION = "appVersion";
	private static final String PROPERTY_REG_ID = "registration_id";
	private static final String TAG = "PlayServicesHelper";
	private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

	private GoogleCloudMessaging googleCloudMessaging;
	private Activity activity;
	private String regId;
	private static PlayServicesHelper instance;
	private static boolean isAPINeedUpdate = true;
	private static boolean isREALNeedUpdate = true;


	public static PlayServicesHelper getInstance(Context context) {
		if (instance == null) {
			instance = new PlayServicesHelper((Activity) context);
		}

		return instance;
	}

	public static void clear(){

		instance = null;
		isAPINeedUpdate = true;
		isREALNeedUpdate = true;
	}

	public PlayServicesHelper(Activity activity) {
		this.activity = activity;
		checkPlayService();
	}

	private void checkPlayService() {
		// Check device for Play Services APK. If check succeeds, proceed with
		// GCM registration.
		if (checkPlayServices()) {
			googleCloudMessaging = GoogleCloudMessaging.getInstance(activity);
			regId = getRegistrationId();

//			if (regId.isEmpty()) {
				registerInBackground();
//			}
		} else {
			Log.i(TAG, "No valid Google Play Services APK found.");
		}

		Log.w(TAG, "regId = " + regId);
	}

	/**
	 * Check the device to make sure it has the Google Play Services APK. If it
	 * doesn't, display a dialog that allows users to download the APK from the
	 * Google Play Store or enable it in the device's system settings.
	 */
	public boolean checkPlayServices() {
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(activity);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
//				GooglePlayServicesUtil.getErrorDialog(resultCode, activity,
//						PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else {
				Log.i(TAG, "This device is not supported.");
				activity.finish();
			}
			return false;
		}
		return true;
	}

	/**
	 * Gets the current registration ID for application on GCM service.
	 * <p/>
	 * If result is empty, the app needs to register.
	 * 
	 * @return registration ID, or empty string if there is no existing
	 *         registration ID.
	 */
	private String getRegistrationId() {
		final SharedPreferences prefs = getGCMPreferences();
		String registrationId = prefs.getString(PROPERTY_REG_ID, "");
		if (registrationId.isEmpty()) {
			Log.i(TAG, "Registration not found.");
			return "";
		}
		// Check if app was updated; if so, it must clear the registration ID
		// since the existing regID is not guaranteed to work with the new
		// app version.
		int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION,
				Integer.MIN_VALUE);
		int currentVersion = ApplicationSingleton.getInstance().getAppVersion();
		if (registeredVersion != currentVersion) {
			Log.i(TAG, "App version changed.");
			return "";
		}
		return registrationId;
	}

	/**
	 * Registers the application with GCM servers asynchronously.
	 * <p/>
	 * Stores the registration ID and app versionCode in the application's
	 * shared preferences.
	 */
	private void registerInBackground() {
		Log.w(TAG, "registerInBackground");
		new AsyncTask<Void, Void, String>() {
			@Override
			protected String doInBackground(Void... params) {
				String msg = "";
				try {
					if (googleCloudMessaging == null) {
						googleCloudMessaging = GoogleCloudMessaging
								.getInstance(activity);
					}
					regId = googleCloudMessaging
							.register(Consts.PROJECT_NUMBER);
					msg = "Device registered, registration ID=" + regId;
					Log.w(TAG, "registerInBackground msg: "+msg);
					CoreData.regId = regId;

					// You should send the registration ID to your server over
					// HTTP, so it
					// can use GCM/HTTP or CCS to send messages to your app.
//					Handler h = new Handler(activity.getMainLooper());
//					h.post(new Runnable() {
//						@Override
//						public void run() {
//							ChatSessionUtil.subscribeToPushNotifications(activity, regId);
//						}
//					});

					// For this demo: we don't need to send it because the
					// device will send
					// upstream messages to a server that echo back the message
					// using the
					// 'from' address in the message.

					// Persist the regID - no need to register again.
					storeRegistrationId(regId);
					uploadDeviceToken(regId);
				} catch (IOException ex) {
					msg = "Error :" + ex.getMessage();
					// If there is an error, don't just keep trying to register.
					// Require the user to click a button again, or perform
					// exponential back-off.
				}
				return msg;
			}

			@Override
			protected void onPostExecute(String msg) {
				Log.i(TAG, msg + "\n");
			}
		}.execute(null, null, null);
	}

	/**
	 * Update device token to server
	 * @param regId
	 */
	private void uploadDeviceToken(String regId) {
		Log.w("","GCMTesting uploadDeviceToken regId "+regId);

		try {
			ResponseLoginSocial.Content agentContent = CoreData.getUserContent(instance.activity);

			RequestUpdateDeviceToken requestActivityLog = new RequestUpdateDeviceToken(agentContent.memId, LocalStorageHelper.getAccessToken(instance.activity), regId);
			requestActivityLog.callUpdateDeviceTokenApi( instance.activity, new ApiCallback() {
				@Override
				public void success(ApiResponse apiResponse) {
					isAPINeedUpdate = false;
					Log.w("", "GCMTesting uploadDeviceToken apiResponse " + apiResponse.jsonContent);
					Log.d(TAG, "responseGson = " + apiResponse.jsonContent);
				}

				@Override
				public void failure(String errorMsg) {
					Log.w("", "GCMTesting uploadDeviceToken errorMsg " + errorMsg);
					Log.w(TAG, "errorMsg = " + errorMsg);
				}
			});
		}catch (Exception e){
			Log.w("","uploadDeviceToken error");
		}
	}

	/**
	 * @return Application's {@code SharedPreferences}.
	 */
	private SharedPreferences getGCMPreferences() {
		// This sample app persists the registration ID in shared preferences,
		// but
		// how you store the regID in your app is up to you.
		return activity.getSharedPreferences(activity.getPackageName(),
				Context.MODE_PRIVATE);
	}


	/**
	 * Stores the registration ID and app versionCode in the application's
	 * {@code SharedPreferences}.
	 * 
	 * @param regId
	 *            registration ID
	 */
	private void storeRegistrationId(String regId) {
		final SharedPreferences prefs = getGCMPreferences();
		int appVersion = ApplicationSingleton.getInstance().getAppVersion();
		Log.i(TAG, "Saving regId on app version " + appVersion);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(PROPERTY_REG_ID, regId);
		editor.putInt(PROPERTY_APP_VERSION, appVersion);
		editor.commit();
	}
}