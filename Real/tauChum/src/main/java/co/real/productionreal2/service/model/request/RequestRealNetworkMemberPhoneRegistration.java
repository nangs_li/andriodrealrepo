package co.real.productionreal2.service.model.request;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import co.real.productionreal2.Constants;
import co.real.productionreal2.R;
import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.model.InputRequest;
import co.real.productionreal2.service.RealNetworkService;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.response.ResponseRealNetworkMemberPhoneRegistration;
import co.real.productionreal2.util.AppUtil;
import co.real.productionreal2.util.CryptLib;
import co.real.productionreal2.util.NetworkUtil;
import co.real.productionreal2.view.Dialog;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class RequestRealNetworkMemberPhoneRegistration {
    public RequestRealNetworkMemberPhoneRegistration(
                                                     int phoneRegID,
                                                     String pINInput,
                                                     String firstName,
                                                     String lastName
                                                     ) {
        this.phoneRegID = phoneRegID;
        this.pINInput = pINInput;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @SerializedName("UniqueKey")
    public String uniqueKey;
    @SerializedName("PhoneRegID")
    public int phoneRegID;
    @SerializedName("PINInput")
    public String pINInput;
    @SerializedName("FirstName")
    public String firstName;
    @SerializedName("LastName")
    public String lastName;
    @SerializedName("Lang")
    public String lang;

    public void callRealNetworkMemberPhoneRegistrationApi(final Context context, final ApiCallback callback) {
        this.uniqueKey = AppUtil.getUniqueKey();
        this.lang = AppUtil.getCurrentLangISO2Code(context);

        String json = new Gson().toJson(this);
        String encryptJson;
        if (Constants.enableEncryption) {
            encryptJson = CryptLib.encrypt(json);
            Log.w("RealNetworkMemberPhoneRegistrationApi", "json = " + json);
        } else {
            encryptJson = json;
        }
        final RealNetworkService apiService = new RealNetworkService(context);
        apiService.getCoffeeService().memPhoneReg(new InputRequest(encryptJson), new Callback<ApiResponse>() {
            @Override
            public void success(ApiResponse apiResponse, Response response) {
                String decryptJson;
                if (Constants.enableEncryption) {
                    decryptJson = CryptLib.decrypt(apiResponse.jsonContent);
                    Log.w("callRealNetworkMemberPhoneRegistrationApi", "decryptJson = " + decryptJson);
                } else {
                    decryptJson = apiResponse.jsonContent;
                }
                ResponseRealNetworkMemberPhoneRegistration responseGson = new Gson().fromJson(decryptJson, ResponseRealNetworkMemberPhoneRegistration.class);
                if (responseGson.errorCode == 4) {
                    callback.failure(responseGson.errorMsg);
                } else if (responseGson.errorCode == 26) {
                    Dialog.suspendedErrorForceLogoutDialog(context).show();
                } else if (responseGson.errorCode != 0 && responseGson.errorCode != 20 && responseGson.errorCode != 21 &&
                        responseGson.errorCode != 22 && responseGson.errorCode != 23) {
                    callback.failure(context.getString(R.string.login__wrong_password));
                } else {
                    callback.success(apiResponse);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                callback.failure("");
                NetworkUtil.showNoNetworkMsg(context);
            }
        });

    }
}
