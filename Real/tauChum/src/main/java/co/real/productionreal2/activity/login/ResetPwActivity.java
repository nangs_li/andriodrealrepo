package co.real.productionreal2.activity.login;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import co.real.productionreal2.R;
import co.real.productionreal2.activity.BaseActivity;
import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.callback.DialogCallback;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.request.RequestRealNetworkSendResetPwEmail;
import co.real.productionreal2.util.AppUtil;
import co.real.productionreal2.util.ValidUtil;
import co.real.productionreal2.view.Dialog;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class ResetPwActivity extends BaseActivity {
    @InjectView(R.id.emailEditText)
    EditText emailEditText;
    private ProgressDialog ringProgressDialog;
    @InjectView(R.id.tv_reset_pw_title)
    TextView tvResetPwTitle;
    @InjectView(R.id.errorTextView)
    TextView errorTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_pw);

        ButterKnife.inject(this);

        errorTextView.setVisibility(View.INVISIBLE);

        tvResetPwTitle.setText(Html.fromHtml(getString(R.string.sign_up__resetpassword)));
        emailEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    pressDone();
                }
                return false;
            }
        });

        emailEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                errorTextView.setVisibility(View.INVISIBLE);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }


    @OnClick(R.id.rlSubmit)
    public void submitBtnClick(View view) {
        pressDone();

    }

    void pressDone() {
        if (emailEditText.getText().toString().isEmpty()) {
//            Dialog.noEmailDialog(this).show();
            errorTextView.setText(R.string.error_message__email_cannot_be);
            errorTextView.setVisibility(View.VISIBLE);
        } else if (!ValidUtil.isValidEmail(emailEditText.getText().toString())) {
//            Dialog.emailNotValidDialog(this).show();
            errorTextView.setText(R.string.error_message__please_enter_valid);
            errorTextView.setVisibility(View.VISIBLE);
        } else {
            errorTextView.setVisibility(View.INVISIBLE);
            if (ringProgressDialog != null && ringProgressDialog.isShowing())
                return;
//            ringProgressDialog = ProgressDialog.show(this, getResources().getString(R.string.common__please_wait),
//                    "");
            RequestRealNetworkSendResetPwEmail requestRealNetworkResetPw = new RequestRealNetworkSendResetPwEmail(emailEditText.getText().toString());
            requestRealNetworkResetPw.callRealNetworkSendResetPasswordEmail(this, new ApiCallback() {
                @Override
                public void success(ApiResponse apiResponse) {
                    Dialog.feedbackDialog(ResetPwActivity.this, true,
                            getString(R.string.error_message__reset_email_sent),
                            getString(R.string.error_message__check_email_follow_instructions),
                            new DialogCallback() {
                                @Override
                                public void yes() {
                                    onBackPressed();
                                }
                            }).show();
                }

                @Override
                public void failure(String errorMsg) {
                    if (errorMsg.trim()!="")
                    AppUtil.showDialog(ResetPwActivity.this, errorMsg, "", false);
                    if (ringProgressDialog != null) {
                        ringProgressDialog.dismiss();
                        ringProgressDialog.cancel();
                        ringProgressDialog = null;
                    }

                }
            });

        }

    }

}
