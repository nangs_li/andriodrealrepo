package co.real.productionreal2.fragment;

import android.Manifest;
import android.app.Fragment;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import co.real.productionreal2.Constants;
import co.real.productionreal2.CoreData;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.viral.ConnectAgentActivity;
import co.real.productionreal2.adapter.ConnectAgentAdapter;
import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.callback.DialogCallback;
import co.real.productionreal2.common.LocalStorageHelper;
import co.real.productionreal2.dao.account.DBPhoneBook;
import co.real.productionreal2.daomanager.DatabaseManager;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.ReMatchContact;
import co.real.productionreal2.service.model.ReUploadContact;
import co.real.productionreal2.service.model.request.RequestFollowAgent;
import co.real.productionreal2.service.model.request.RequestMatchContact;
import co.real.productionreal2.service.model.response.ResponseMatchContact;
import co.real.productionreal2.util.APIUtil;
import co.real.productionreal2.util.FileUtil;
import co.real.productionreal2.util.NetworkUtil;
import co.real.productionreal2.util.PermissionUtil;
import co.real.productionreal2.util.PhoneBookUtil;
import co.real.productionreal2.view.Dialog;
import io.branch.indexing.BranchUniversalObject;
import io.branch.referral.Branch;
import io.branch.referral.BranchError;
import io.branch.referral.util.LinkProperties;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;


/**
 * Created by kelvinsun on 31/5/16.
 */
public class FindYourAgentFragment extends Fragment implements ConnectAgentAdapter.ConnectAgentAdapterListener {

    @InjectView(R.id.tv_find_your_agent_title)
    TextView tvFindAgentTitle;
    @InjectView(R.id.et_search_agent)
    EditText etSearchAgent;
    @InjectView(R.id.lv_suggested_agents)
    StickyListHeadersListView lvAgents;
    @InjectView(R.id.pb_find_agent)
    ProgressBar spinner;
    @InjectView(R.id.inviteBtn)
    Button bnInvite;
    @InjectView(R.id.tv_empty_placeholder)
    TextView tvEmptyMsg;

    private static final int REQUEST_CODE_VIRAL = 100;
    private ConnectAgentAdapter connectAgentAdapter;
    private ArrayList<Object> connectAgentList = new ArrayList<>();
    private ArrayList<ReMatchContact> existingAgentList = new ArrayList<>();
    private ArrayList<ReUploadContact> agentList = new ArrayList<>();

    private InsertDBAsyncTask insertDBAsyncTask = new InsertDBAsyncTask();
    private FetchingLocalContactsAsyncTask fetchingLocalContactsAsyncTask = new FetchingLocalContactsAsyncTask();
    private UploadLocalContactsAsyncTask uploadLocalContactsAsyncTask = new UploadLocalContactsAsyncTask();
    private UploadInviteListAsyncTask uploadInviteListAsyncTask = new UploadInviteListAsyncTask();

    private static final int AGENT_SHOW_LIMIT = 3;
    int mNum;

    /**
     * Create a new instance of CountingFragment, providing "num"
     * as an argument.
     */
    public static FindYourAgentFragment newInstance(int num) {
        FindYourAgentFragment f = new FindYourAgentFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putInt("num", num);
        f.setArguments(args);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_find_your_agent, container, false);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.inject(getActivity());

        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) {
            // No explanation needed, we can request the permission.
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.READ_CONTACTS},
                    PermissionUtil.MY_PERMISSIONS_REQUEST_READ_CONTACT);
        } else {
            setupAgentList(null);
            if (NetworkUtil.isConnected(getActivity())) {
                //call api to upload ContactList
                fetchingLocalContactsAsyncTask = new FetchingLocalContactsAsyncTask();
                fetchingLocalContactsAsyncTask.execute();
            } else {
                //nothing
            }
        }
        initView();
        addListener();

    }

    private void addListener() {
        etSearchAgent.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                if (connectAgentAdapter != null) {
                    connectAgentAdapter.getFilter().filter(cs);
                }

                if (connectAgentAdapter != null) {
                    tvEmptyMsg.setVisibility(connectAgentAdapter.getCount() == 0 ? View.VISIBLE : View.GONE);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void afterTextChanged(Editable arg0) {
            }
        });

        lvAgents.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });
    }

    private void initView() {
        lvAgents.setVisibility(View.VISIBLE);
        tvFindAgentTitle.setText(Html.fromHtml(getString(R.string.setting__connect_agent)));
        bnInvite.setVisibility(View.GONE);

        if (connectAgentAdapter == null) {
            spinner.setVisibility(View.VISIBLE);
            spinner.invalidate();
        } else {
            spinner.setVisibility(View.GONE);
        }
    }

    private void setupAgentList(ArrayList<ReMatchContact> agentList) {

        if (agentList != null) {
            //combine list
            connectAgentList = new ArrayList<>();
            connectAgentList.addAll(agentList);

            //cache data
            LocalStorageHelper.defaultHelper(getActivity()).saveConnectAgentListToLocalStorage(getActivity(), agentList);

            insertDBAsyncTask = new InsertDBAsyncTask();
            insertDBAsyncTask.execute();
        } else {
            connectAgentList.addAll(LocalStorageHelper.getConnectAgentList(getActivity()));
            connectAgentList.addAll(DatabaseManager.getInstance(getActivity()).readPhoneBook());
            spinner.setVisibility(View.GONE);

            if (connectAgentList.size() > 0) {
                if (connectAgentAdapter == null) {
                    connectAgentAdapter = new ConnectAgentAdapter(getActivity(), connectAgentList, FindYourAgentFragment.this);
                    lvAgents.setAdapter(connectAgentAdapter);
                } else {
                    connectAgentAdapter.notifyDataSetChanged();
                }
            }
        }
    }


    private void updatePhoneBookList() {
        ArrayList<DBPhoneBook> phoneBookList = PhoneBookUtil.loadPhoneBookFromLocal(getActivity());
        DatabaseManager.getInstance(getActivity()).insertPhoneBook(phoneBookList);
    }

    @OnClick(R.id.closeBtn)
    public void finishActivity() {
        getActivity().finish();
    }

    @Override
    public ArrayList<Object> getConnectAgentList() {
        return connectAgentList;
    }

    @Override
    public void onFollowAll(List<Integer> existingAgentIdList) {
        final RequestFollowAgent requestFollowAgent = new RequestFollowAgent(CoreData.getUserContent(getActivity()).memId, LocalStorageHelper.getAccessToken(getActivity()));
        requestFollowAgent.toBatchRealMemberID = existingAgentIdList;
        followAllAgents(requestFollowAgent);
    }

    @Override
    public void onFollowAgent(final ReMatchContact agent) {
        final RequestFollowAgent requestFollowAgent = new RequestFollowAgent(CoreData.getUserContent(getActivity()).memId, LocalStorageHelper.getAccessToken(getActivity()));
        requestFollowAgent.followingMemId = agent.memberID;

        if (agent.isFollowing == 0) {
            // follow count +1
            followAgent(requestFollowAgent, agent);

        } else if (agent.isFollowing == 1) {
            Dialog.unfollowDialog(getActivity(), agent.name, new DialogCallback() {
                @Override
                public void yes() {
                    // follow count -1
                    unfollowAgent(requestFollowAgent, agent);
                }
            }).show();
        } else {
            // u are ureself
        }
    }

    private void followAllAgents(final RequestFollowAgent followAgent) {

        followAgent.callFollowApi(getActivity(), new ApiCallback() {
            @Override
            public void success(ApiResponse apiResponse) {
                Log.d("toFollow", "toFollow followAll: " + followAgent.toString());
                connectAgentAdapter.notifyDataSetChanged();
            }

            @Override
            public void failure(String errorMsg) {
                Log.d("toFollow", "toFollow followAll: " + errorMsg.toString());
            }
        });
    }

    private void followAgent(RequestFollowAgent followAgent, final ReMatchContact agent) {

        followAgent.callFollowApi(getActivity(), new ApiCallback() {
            @Override
            public void success(ApiResponse apiResponse) {
                Log.d("toFollow", "toFollow follow: " + agent.name + " " + agent.isFollowing);
                agent.isFollowing = (agent.isFollowing == 0 ? 1 : 0);
                connectAgentAdapter.notifyDataSetChanged();
            }

            @Override
            public void failure(String errorMsg) {
                Log.d("toFollow", "toFollow follow: " + errorMsg.toString());
            }
        });
    }

    private void unfollowAgent(RequestFollowAgent unfollowAgent, final ReMatchContact agent) {

        unfollowAgent.callUnfollowApi(getActivity(), new ApiCallback() {

            @Override
            public void success(ApiResponse apiResponse) {
                Log.d("toFollow", "toFollow unfollow: " + agent.name + " " + agent.isFollowing);
                agent.isFollowing = (agent.isFollowing == 0 ? 1 : 0);
                connectAgentAdapter.notifyDataSetChanged();
//                CurrentFollower currentFollower = new CurrentFollower(userContent.memId, mFollower.memberID,
//                        userContent.qbid, null, false);
//                currentFollower.updateCurrentFollowingList(getActivity());
//
//                //add local following people list
//                LocalStorageHelper.removeAgentProfileListToLocalStorage(getActivity(),mFollower.memberID);
            }

            @Override
            public void failure(String errorMsg) {

            }
        });
    }

    @Override
    public void onInviteAgent(List<DBPhoneBook> invitePhoneNoList) {
        sendSMS(invitePhoneNoList);
    }

    private void sendSMS(final List<DBPhoneBook> invitePhoneNoList) {

        if (invitePhoneNoList != null) {

            if (getActivity() instanceof ConnectAgentActivity) {
                ((ConnectAgentActivity) getActivity()).showLoadingDialog();
                //parse data to list String
                final List<String> phoneNoList = new ArrayList<>();
                final List<ReUploadContact> invitedList = new ArrayList<>();
                for (DBPhoneBook phoneItem : invitePhoneNoList) {
                    String phoneNo = phoneItem.getCountryCode() + " " + phoneItem.getPhoneNumber();
                    phoneNoList.add(phoneNo);
                    //update flag
                    phoneItem.setHasSent(true);
                    phoneItem.setHasChecked(false);
                    DatabaseManager.getInstance(getActivity()).updatePhoneBook(phoneItem);
                    Log.d("phoneItem", "phoneItem: " + phoneItem.getName() + " " + phoneItem.getPhoneNumber());
                    ReUploadContact reUploadContact = new ReUploadContact(phoneItem.getName(), phoneItem.getCountryCode(), String.valueOf(phoneItem.getPhoneNumber()), "");
                    invitedList.add(reUploadContact);
                }

                String contentStr = new Gson().toJson(new ReUploadContactList(invitedList));
                //write to text file
                FileUtil.writeJsonToFile(Constants.UPLOAD_INVITED_LIST_FILE_NAME, contentStr, Constants.UPLOAD_INVITED_LIST_FOLDER_NAME);

                uploadInviteListAsyncTask = new UploadInviteListAsyncTask();
                uploadInviteListAsyncTask.execute();

                BranchUniversalObject branchUniversalObject = genBranchUniObj();
                LinkProperties linkProps = genLinkProps();
                branchUniversalObject.generateShortUrl(getActivity(), linkProps, new Branch.BranchLinkCreateListener() {
                    @Override
                    public void onLinkCreate(String url, BranchError error) {
                        ((ConnectAgentActivity) getActivity()).dismissLoadingDialog();
                        if (error != null) {
                            Dialog.normalDialog(getActivity(), "Branch.io gen shortUrl error: " + error);
                        } else {
                            //update list cells status
                            connectAgentAdapter.notifyDataSetChanged();
                            //remove teh blanket at the beginning and the end
                            String phoneNoListStr = phoneNoList.toString().replace("[", "").replace("]", "");
                            Uri uri = Uri.parse("smsto:" + phoneNoListStr);
                            Intent it = new Intent(Intent.ACTION_SENDTO, uri);
                            it.putExtra("sms_body", getString(R.string.invite_to_download__content) + " " + url);
                            startActivityForResult(it, REQUEST_CODE_VIRAL);
                        }
                    }
                });
            }
        }
    }

    private BranchUniversalObject genBranchUniObj() {
        BranchUniversalObject branchUniObj = new BranchUniversalObject()
                .setTitle("Real")
                .setContentDescription("Someone invite you to Invite in Real")
                .setContentIndexingMode(BranchUniversalObject.CONTENT_INDEX_MODE.PUBLIC)
                .addContentMetadata(Constants.BRANCH_MEM_ID, "" + CoreData.getUserContent(getActivity()).memId)
                .addContentMetadata(Constants.BRANCH_MEM_NAME, CoreData.getUserContent(getActivity()).memName)
                .addContentMetadata(Constants.BRANCH_ACTION, Constants.BRANCH_ACTION_NONE);

        return branchUniObj;
    }

    private LinkProperties genLinkProps() {
        LinkProperties linkProperties = new LinkProperties()
                .setChannel(Constants.BRANCH_CHANNEL_DOWNLOAD)
                .setFeature("invite");
        return linkProperties;
    }


    private void uploadContactList() {

        //call api to upload ContactList
        uploadLocalContactsAsyncTask = new UploadLocalContactsAsyncTask();
        uploadLocalContactsAsyncTask.execute();
//        //success
//        fetchMatchContactList(1);
        //fail, retry?

    }

    class UploadInviteListAsyncTask extends AsyncTask<String, Integer, Integer> {
        File txtFile = new File(FileUtil.getAppSubFolder(Constants.UPLOAD_INVITED_LIST_FOLDER_NAME), Constants.UPLOAD_INVITED_LIST_FILE_NAME);

        protected Integer doInBackground(String... urls) {
            //upload invite list
            APIUtil.uploadTxtFileApi(getActivity(), CoreData.getUserContent(getActivity()), txtFile, 6);
            return null;
        }

        protected void onPostExecute(Integer result) {
            if (Constants.isDevelopment) {
                //remove file
                FileUtil.deleteFile(txtFile);
            }
            uploadInviteListAsyncTask = null;
        }
    }

    class InsertDBAsyncTask extends AsyncTask<String, Integer, Integer> {

        protected Integer doInBackground(String... urls) {
            updatePhoneBookList();
            return null;
        }

        protected void onPostExecute(Integer result) {
            List<DBPhoneBook> phoneBookList = new ArrayList<>();
            phoneBookList.addAll(DatabaseManager.getInstance(getActivity()).readPhoneBook());
            connectAgentList.addAll(phoneBookList);

            //cache data
//            LocalStorageHelper.defaultHelper(getActivity()).savePhonebookListToLocalStorage(getActivity(), phoneBookList);

            spinner.setVisibility(View.GONE);

            if (connectAgentAdapter == null) {
                connectAgentAdapter = new ConnectAgentAdapter(getActivity(), connectAgentList, FindYourAgentFragment.this);
                lvAgents.setAdapter(connectAgentAdapter);
            } else {
                connectAgentAdapter.notifyDataSetChanged();
            }

            insertDBAsyncTask = null;
        }
    }


    class UploadLocalContactsAsyncTask extends AsyncTask<String, Integer, Integer> {
        File txtFile = new File(FileUtil.getAppSubFolder(Constants.UPLOAD_CONTACT_LIST_FOLER_NAME), Constants.UPLOAD_CONTACT_LIST_FILE_NAME);

        protected Integer doInBackground(String... urls) {
            try {
                Log.d("doInBackground", "UploadLocalContactsAsyncTask START");
                APIUtil.uploadTxtFileApi(getActivity(), CoreData.getUserContent(getActivity()), txtFile, 5);
                return null;
            } catch (Exception e) {
                return null;
            }
        }

        protected void onPostExecute(Integer result) {
            // TODO: check this.exception
            fetchMatchContactList(1);
            if (Constants.isDevelopment) {
                //remove file
                FileUtil.deleteFile(txtFile);
            }
            uploadLocalContactsAsyncTask = null;
        }
    }

    private class FetchingLocalContactsAsyncTask extends AsyncTask<String, Integer, Integer> {

        @Override
        protected Integer doInBackground(String... param) {

            //load phone book
            agentList = PhoneBookUtil.loadUploadContactFromLocal(getActivity());
            //parse to json string to upload
            String contentStr = new Gson().toJson(new ReUploadContactList(agentList));
            //write to text file
            FileUtil.writeJsonToFile(Constants.UPLOAD_CONTACT_LIST_FILE_NAME, contentStr, Constants.UPLOAD_CONTACT_LIST_FOLER_NAME);

            return null;
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            //upload contact list
            uploadContactList();
            fetchingLocalContactsAsyncTask = null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
    }

    public class ReUploadContactList {
        @SerializedName("contactList")
        final Object ContactList;

        public ReUploadContactList(Object contactList) {
            this.ContactList = contactList;
        }

    }

    /**
     * get matched contact list
     *
     * @param pageNo
     */
    private void fetchMatchContactList(final int pageNo) {
        if (existingAgentList == null)
            return;
        Log.d("doInBackground", "UploadLocalContactsAsyncTask END");


        RequestMatchContact requestMatchContact = new RequestMatchContact(CoreData.getUserContent(getActivity()).memId, LocalStorageHelper.getAccessToken(getActivity()), pageNo);
        requestMatchContact.callMatchContactApi(getActivity(), new ApiCallback() {
            @Override
            public void success(ApiResponse apiResponse) {
                Log.w("fetchMatchContactList", "responseGson = " + apiResponse.jsonContent);

                ResponseMatchContact responseMatchContact = new Gson().fromJson(apiResponse.jsonContent, ResponseMatchContact.class);

                existingAgentList = new ArrayList<ReMatchContact>();
                //add new record to existing list
                existingAgentList.addAll(responseMatchContact.content.reMatchContactList);

                //success
                setupAgentList(existingAgentList);

                //fail, retry?

            }

            @Override
            public void failure(String errorMsg) {
                Log.w("fetchMatchContactList", "errorMsg = " + errorMsg);
                setupAgentList(null);
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PermissionUtil.MY_PERMISSIONS_REQUEST_READ_CONTACT: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    if (NetworkUtil.isConnected(getActivity())) {
                        //call api to upload ContactList
                        fetchingLocalContactsAsyncTask = new FetchingLocalContactsAsyncTask();
                        fetchingLocalContactsAsyncTask.execute();
                    } else {
                        setupAgentList(null);
                    }
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    spinner.setVisibility(View.GONE);
                    tvEmptyMsg.setVisibility(View.VISIBLE);
                }
                return;
            }
        }
    }


    @Override
    public void onResume() {
        super.onResume();
    }

//    // This method will be called when a SomeOtherEvent is posted
//    @Subscribe
//    public void handleExistingAgentResponse(ExistingAgentUpdateEvent event) {
//        if (event.agentList != null) {
//            setupAgentList(event.agentList);
//        }
//        spinner.setVisibility(View.GONE);
//
//        Log.d("handle", "handleExistingAgentResponse" + event);
//    }

    //    @Override
//    public void onStart() {
//        super.onStart();
//        EventBus.getDefault().register(this);
//    }
//
    @Override
    public void onStop() {
//        EventBus.getDefault().unregister(this);
        if (insertDBAsyncTask != null) {
            insertDBAsyncTask.cancel(true);
        }
        if (fetchingLocalContactsAsyncTask != null) {
            fetchingLocalContactsAsyncTask.cancel(true);
        }
        if (uploadLocalContactsAsyncTask != null) {
            uploadLocalContactsAsyncTask.cancel(true);
        }
        if (uploadInviteListAsyncTask != null) {
            uploadInviteListAsyncTask.cancel(true);
        }
        super.onStop();
    }

}
