package co.real.productionreal2.service;

import android.content.Context;

import co.real.productionreal2.Constants;
import co.real.productionreal2.model.InputRequest;
import co.real.productionreal2.service.model.ApiResponse;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.http.Body;
import retrofit.http.Headers;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.mime.TypedFile;

public class AdminService extends BaseService{


    public final static int DEVICE_TYPE_ANDROID = 2;

    public final static int SOCIAL_FACEBOOK = 1;
    public final static int SOCIAL_TWITTER = 2;
    public final static int SOCIAL_GOOGLE_AUTH2 = 3;
    public final static int SOCIAL_WEIBO = 4;
    public final static int SOCIAL_REAL = 5;
    public final static int SOCIAL_PHONE = 6; //RealNetwork/Phone


    // We are implementing against version 2.5 of the Open Weather Map web service.
    //    private static final String WEB_SERVICE_BASE_URL = "http://api.openweathermap.org/data/2.5";
//	public static final String WEB_SERVICE_BASE_URL = "http://52.24.198.180";//"http://192.168.1.138/";
//	public static final String WEB_SERVICE_BASE_URL = "http://192.168.1.138/";
    //	CookieHeaderProvider cookieHeaderProvider = new CookieHeaderProvider();

    public final CoffeeService coffeeService;


    public AdminService(Context context) {
        RequestInterceptor requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestInterceptor.RequestFacade request) {
                request.addHeader("Accept", "application/json");
                request.addHeader("Content-type", "application/json");

            }
        };

//        CookieManager cookieManager = new CookieManager(new PersistentCookieStore(context), CookiePolicy.ACCEPT_ALL);
//        OkHttpClient okHttpClient = new OkHttpClient();
//        okHttpClient.setCookieHandler(cookieManager);
//        Client client = new OkClient(okHttpClient);

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(WEB_SERVICE_BASE_URL)
                .setRequestInterceptor(requestInterceptor)
                .setLogLevel(Constants.isDevelopment? RestAdapter.LogLevel.FULL : RestAdapter.LogLevel.NONE)
//                .setConverter(new MixedConverter(new SimpleXMLConverter(), new GsonConverter(gson)))
//                .setClient(new InterceptingOkClient())
//                .setClient(client)
                .build();


        coffeeService = restAdapter.create(CoffeeService.class);
    }



    //CoffeeService.java
    public interface CoffeeService {
        @POST("/Admin.asmx/LoginBySocialNetwork")
        public void loginSocial(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);


        @POST("/Admin.asmx/MemberProfilePixUpdate")
        public void updateProfilePix(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);


        @POST("/Admin.asmx/AgentProfileRegistration")
        public void agentProfileReg(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);


        @POST("/Admin.asmx/AgentProfileGet")
        public void agentProfileGet(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);

        @POST("/Operation.asmx/AgentProfileGetList")
        public void agentProfileGetList(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);

        @POST("/Operation.asmx/AgentProfileGetList_EX")
        public void agentProfileGetListEx(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);

        @POST("/Admin.asmx/AgentProfileAgentLicenseUpdate")
        public void agentLicenseUpdate(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);

        ////////////////////////////

        @POST("/Admin.asmx/AgentProfileExperienceAdd")
        public void agentProfileExpAdd(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);

        @POST("/Admin.asmx/AgentProfileExperienceDelete")
        public void agentProfileExpDel(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);

        @POST("/Admin.asmx/AgentProfileLanguageAdd")
        public void agentProfileLangAdd(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);

        @POST("/Admin.asmx/AgentProfileLanguageDelete")
        public void agentProfileLangDel(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);

        @POST("/Admin.asmx/AgentProfilePastClosingAdd")
        public void agentProfileClosingAdd(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);

        @POST("/Admin.asmx/AgentProfilePastClosingDelete")
        public void agentProfileClosingDel(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);

        @POST("/Admin.asmx/AgentProfilePastClosingPositionUpdate")
        public void agentProfileClosingUpdatePos(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);


        @POST("/Admin.asmx/AgentProfileSpecialtyAdd")
        public void agentProfileSpecAdd(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);

        @POST("/Admin.asmx/AgentProfileSpecialtyDelete")
        public void agentProfileSpecDel(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);

        ////////////////////////////

        @POST("/Admin.asmx/AgentListingAdd")
        public void agentListingAdd(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);

        @POST("/Admin.asmx/AgentListingPhotoPositionUpdate")
        public void agentListingPhotoUpdate(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);

        @POST("/Admin.asmx/AgentListingPhotoDelete")
        public void agentListingPhotoDelete(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);

////////////////////////////


        //MemberID int
        //TypedString
        @Multipart
        @POST("/Admin.asmx/FileUpload")  //@POST("/posts/add.json")
        @Headers({
                "Accept: application/xml",
                "Accept-Encoding: gzip"
        })
        public void fileUpload(
                @Part("photo") TypedFile photo,
                @Part("MemberID") int memberId,
                @Part("AccessToken") String accessToken,
                @Part("ListingID") int listingId,
                @Part("Position") int position,
                @Part("FileExt") String ext,
                @Part("FileType") int type,
                @Part("LanguageIndex") int langIndex,
                retrofit.Callback<ApiResponse> callback);


        @POST("/Admin.asmx/MemberProfileQBIDUpdate")
        public void updateQBID(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);

        @POST("/Admin.asmx/MemberProfileQBApply")
        public void memberProfileQBApply(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);


        @POST("/Admin.asmx/PhoneNumberReg")
        public void phoneNumberReg(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);

        @POST("/Admin.asmx/PhoneNumberRegEX")
        public void phoneNumberRegEX(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);

        @POST("/Admin.asmx/PhoneNumberSMSVerify")
        public void phoneNumberSMSVerify(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);

        @POST("/Admin.asmx/PhoneNumberSMSVerifyEX")
        public void phoneNumberSMSVerifyEX(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);

        @POST("/Admin.asmx/PhoneNumberChange")
        public void phoneNumberChange(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);

        @POST("/Admin.asmx/PhoneNumberSMSVerifyForChangingPhone")
        public void phoneNumberSMSVerifyForChangingPhone(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);

        @POST("/Admin.asmx/CountryCodeGet")
        public void countryCodeGet(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);

        @POST("/Admin.asmx/PhoneNumberMigrate")
        public void phoneNumberMigrate(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);

        @POST("/Admin.asmx/PhoneNumberSMSVerifyForMigratingPhone")
        public void phoneNumberSMSVerifyForMigratingPhone(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);


    }



    public CoffeeService getCoffeeService() {
        return coffeeService;
    }


}
