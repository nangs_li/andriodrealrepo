package co.real.productionreal2.callback;

import co.real.productionreal2.service.model.ApiResponse;

/**
 * Created by hohojo on 23/9/2015.
 */
public interface ApiCallback {
    public void success(ApiResponse apiResponse);
    public void failure(String errorMsg);
}
