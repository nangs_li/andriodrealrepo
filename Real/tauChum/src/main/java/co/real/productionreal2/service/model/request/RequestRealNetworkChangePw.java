package co.real.productionreal2.service.model.request;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import co.real.productionreal2.R;
import co.real.productionreal2.Constants;
import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.model.InputRequest;
import co.real.productionreal2.service.RealNetworkService;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.response.ResponseRealNetworkMemReg;
import co.real.productionreal2.util.CryptLib;
import co.real.productionreal2.util.NetworkUtil;
import co.real.productionreal2.view.Dialog;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by hohojo on 15/12/2015.
 */
public class RequestRealNetworkChangePw {
    public RequestRealNetworkChangePw(String newPasswordMD5, String oldPasswordMD5, String userId, String accessToken) {
        this.newPasswordMD5 = newPasswordMD5;
        this.oldPasswordMD5 = oldPasswordMD5;
        this.userId = userId;
        this.accessToken = accessToken;
    }

    @SerializedName("NewPasswordMD5")
    public String newPasswordMD5;
    @SerializedName("OldPasswordMD5")
    public String oldPasswordMD5;
    @SerializedName("UserID")
    public String userId;
    @SerializedName("AccessToken")
    public String accessToken;

    public void callRealNetworkChangePwApi(final Context context, final ApiCallback callback) {
        String json = new Gson().toJson(this);
        String encryptJson;
        if (Constants.enableEncryption) {
            encryptJson = CryptLib.encrypt(json);
            Log.w("callRealNetworkChangePwApi", "json = " + json);
        } else {
            encryptJson = json;
        }
        final RealNetworkService apiService = new RealNetworkService(context);
        apiService.getCoffeeService().changePassword(new InputRequest(encryptJson), new Callback<ApiResponse>() {
            @Override
            public void success(ApiResponse apiResponse, Response response) {
                String decryptJson;
                if (Constants.enableEncryption) {
                    decryptJson = CryptLib.decrypt(apiResponse.jsonContent);
                    Log.w("callRealNetworkLoginApi", "decryptJson = " + decryptJson);
                } else {
                    decryptJson = apiResponse.jsonContent;
                }
                ResponseRealNetworkMemReg responseGson = new Gson().fromJson(decryptJson, ResponseRealNetworkMemReg.class);
                if (responseGson.errorCode == 4) {
                    callback.failure(context.getString(R.string.change_password__alert_message_old_password));
                } else if (responseGson.errorCode == 26) {
                    Dialog.suspendedErrorForceLogoutDialog(context).show();
                } else if (responseGson.errorCode != 0 && responseGson.errorCode != 20 && responseGson.errorCode != 21 &&
                        responseGson.errorCode != 22 && responseGson.errorCode != 23) {
                    callback.failure(responseGson.errorMsg);
                } else {
                    callback.success(apiResponse);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                callback.failure("");
                NetworkUtil.showNoNetworkMsg(context);
            }
        });
    }
}
