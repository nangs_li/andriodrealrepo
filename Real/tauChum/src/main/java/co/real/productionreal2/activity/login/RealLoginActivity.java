package co.real.productionreal2.activity.login;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.mixpanel.android.mpmetrics.MixpanelAPI;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import co.real.productionreal2.Navigation;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.BaseActivity;
import co.real.productionreal2.callback.LoginDbViewListener;
import co.real.productionreal2.common.LocalStorageHelper;
import co.real.productionreal2.config.AppConfig;
import co.real.productionreal2.daomanager.ConfigDataBaseManager;
import co.real.productionreal2.daomanager.DatabaseManager;
import co.real.productionreal2.login.FacebookLoginController;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;
import co.real.productionreal2.util.AppUtil;
import co.real.productionreal2.util.NetworkUtil;
import co.real.productionreal2.util.PermissionUtil;
import co.real.productionreal2.util.ValidUtil;
import co.real.productionreal2.view.Dialog;


public class RealLoginActivity extends BaseActivity implements LoginDbViewListener {
    private static final String TAG = "RealLoginActivity";
    @InjectView(R.id.userNameEditText)
    EditText userNameEditText;
    @InjectView(R.id.pwEditText)
    EditText pwEditText;
    @InjectView(R.id.tvInputHint)
    TextView tvInputHint;
    @InjectView(R.id.tv_login_title)
    TextView tvLoginTitle;

    private ProgressDialog ringProgressDialog;
    private FacebookLoginController facebookLoginController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_real_login);

        ButterKnife.inject(this);

        tvInputHint.setVisibility(View.INVISIBLE);

        tvLoginTitle.setText(Html.fromHtml(getString(R.string.login__login)));

        pwEditText.setTypeface(Typeface.DEFAULT);
        pwEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    pressDone();
                }
                return false;
            }
        });


        userNameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tvInputHint.setVisibility(View.INVISIBLE);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        pwEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tvInputHint.setVisibility(View.INVISIBLE);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    @OnClick(R.id.loginFbRelativeLayout)
    public void loginFbRelativeLayoutClick(View view) {

        if (NetworkUtil.isConnected(this)) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.READ_PHONE_STATE)
                    != PackageManager.PERMISSION_GRANTED) {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_PHONE_STATE},
                        PermissionUtil.MY_PERMISSIONS_REQUEST_READ_PHONE_STATUS_FB);

                // MY_PERMISSIONS_REQUEST_READ_PHONE_STATUS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
                return;
            }
            facebookLogin();
        } else {
            NetworkUtil.showNoNetworkMsg(this);
        }
    }

    @OnClick(R.id.loginBtn)
    public void loginBtnClick(View view) {

        // Mixpanel
        MixpanelAPI mixpanel = MixpanelAPI.getInstance(this, AppConfig.getMixpanelToken());
        mixpanel.track("Logged in with Email");

        pressDone();
    }

    void pressDone() {

        String userName = userNameEditText.getText().toString();
        String pw = pwEditText.getText().toString();
        if (userName.isEmpty()) {
//            Dialog.normalDialog(this,getString(R.string.error_message__email_cannot_be)).show();
            tvInputHint.setText(R.string.error_message__email_cannot_be);
            tvInputHint.setVisibility(View.VISIBLE);
            return;
        } else if (!ValidUtil.isValidEmail(userNameEditText.getText().toString())) {
//            Dialog.emailNotValidDialog(this).show();
            tvInputHint.setText(R.string.error_message__please_enter_valid);
            tvInputHint.setVisibility(View.VISIBLE);
            return;
        } else if (pw.isEmpty()) {
//            Dialog.normalDialog(this,getString(R.string.error_message__passowrd_cannot)).show();
            tvInputHint.setText(R.string.error_message__passowrd_cannot);
            tvInputHint.setVisibility(View.VISIBLE);
            return;
        } else if (!ValidUtil.isValidPassword(pwEditText.getText().toString())) {
            Dialog.pwNotValidDialog(this).show();
            tvInputHint.setText(R.string.error_message__password_must_contain);
            tvInputHint.setVisibility(View.VISIBLE);
            return;
        }

        tvInputHint.setVisibility(View.INVISIBLE);

        //request permission for OS > 6.0
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            // No explanation needed, we can request the permission.
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_PHONE_STATE},
                    PermissionUtil.MY_PERMISSIONS_REQUEST_READ_PHONE_STATUS);

            // MY_PERMISSIONS_REQUEST_READ_PHONE_STATUS is an
            // app-defined int constant. The callback method gets the
            // result of the request.
            return ;
        }

        showLoginRingProgressDialog();
//        RequestRealNetworkLogin realNetworkLogin = new RequestRealNetworkLogin(userName, CryptLib.md5(pw));
//        realNetworkLogin.callRealNetworkLoginApi(this, new ApiCallback() {
//            @Override
//            public void success(ApiResponse apiResponse) {
//                String decryptJson;
//                if (Constants.enableEncryption) {
//                    decryptJson = CryptLib.decrypt(apiResponse.jsonContent);
//                    Log.w("requestsocialLogin", "jsonContent = " + decryptJson);
//                } else {
//                    decryptJson = apiResponse.jsonContent;
//                }
//                ResponseRealNetworkMemLogin responseGson = new Gson().fromJson(decryptJson, ResponseRealNetworkMemLogin.class);
//
//                RequestSocialLogin socialLogin = new RequestSocialLogin(
//                        AdminService.DEVICE_TYPE_ANDROID,
//                        FileUtil.getDeviceId(RealLoginActivity.this),
//                        AdminService.SOCIAL_REAL,
//                        responseGson.content.firstName + " " + responseGson.content.lastName,
//                        responseGson.content.userId,
//                        responseGson.content.email,
//                        responseGson.content.photoURL,
//                        responseGson.content.accessToken
//                );
//
//                LocalStorageHelper.setRealNetworkJson(RealLoginActivity.this, decryptJson);
//
//                socialLogin.callLoginSocialApi(RealLoginActivity.this, new ApiCallback() {
//                    @Override
//                    public void success(ApiResponse apiResponse) {
//                        String decryptJson;
//                        if (Constants.enableEncryption) {
//                            decryptJson = CryptLib.decrypt(apiResponse.jsonContent);
//                            Log.w("requestsocialLogin", "jsonContent = " + decryptJson);
//                        } else {
//                            decryptJson = apiResponse.jsonContent;
//                        }
//                        final ResponseLoginSocial responseGson = new Gson().fromJson(decryptJson, ResponseLoginSocial.class);
//
//                        // Mixpanel
//                        MixpanelAPI mixpanel = MixpanelAPI.getInstance(RealLoginActivity.this, AppConfig.getMixpanelToken());
//                        mixpanel.identify("" + responseGson.content.memId);
//                        mixpanel.getPeople().identify(mixpanel.getDistinctId());
//                        Locale locale = AppUtil.getCurrentLangLocale(RealLoginActivity.this);
//                        mixpanel.getPeople().set("Language", locale.getLanguage());
//                        mixpanel.track("Verified Email Login");
//
//                        LocalStorageHelper.setUpByLogin(RealLoginActivity.this, responseGson.content);
//
//                        ConfigDataBaseManager configDataBaseManager = ConfigDataBaseManager.getInstance(RealLoginActivity.this);
//                        DatabaseManager.getInstance(RealLoginActivity.this).dbActionForLogin(responseGson.content);
//                        configDataBaseManager.dbActionMain(responseGson.content.memId);
//
//                        QBUser user = UserQBUser.getNewQBUser(RealLoginActivity.this);
//
//                        ChatSessionUtil.callQBLogin(RealLoginActivity.this, user, new ApiCallback() {
//                            @Override
//                            public void success(ApiResponse apiResponse) {
//                                PrivateChatImpl.getInstance(RealLoginActivity.this);
//
//                                ChatService.getInstance().getAllDialogs(new BaseService.BaseServiceCallBack() {
//                                    @Override
//                                    public void onSuccess(Object var1) {
//                                        hideLoginRingProgressDialog();
//                                        FetchAgentProfileHelper.getInstance().startFetching();
//                                        AppUtil.goToMainTabActivity(RealLoginActivity.this, responseGson.content);
//                                    }
//
//                                    @Override
//                                    public void onError(BaseService.RealServiceError serviceError) {
//                                        Log.d(TAG, "onError: ");
//                                    }
//                                });
//                            }
//
//                            @Override
//                            public void failure(String errorMsg) {
//                                Log.d(TAG, "failure: "+errorMsg);
//                                hideLoginRingProgressDialog();
//                            }
//                        });
//                    }
//
//                    @Override
//                    public void failure(String errorMsg) {
//                        hideLoginRingProgressDialog();
//                        if (errorMsg.trim() != "")
//                            Dialog.normalDialog(RealLoginActivity.this, getString(R.string.login__wrong_password)).show();
////                        AppUtil.showDialog(RealLoginActivity.this, "", errorMsg, false);
//                    }
//                });
//            }
//
//            @Override
//            public void failure(String errorMsg) {
//                hideLoginRingProgressDialog();
//                if (errorMsg.trim() != "")
//                    Dialog.normalDialog(RealLoginActivity.this, errorMsg).show();
////                AppUtil.showDialog(RealLoginActivity.this, errorMsg, "", false);
//            }
//        });
    }


    private void showLoginRingProgressDialog() {
        if (ringProgressDialog != null && ringProgressDialog.isShowing())
            return;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!isFinishing()) {
                    ringProgressDialog = ProgressDialog.show(RealLoginActivity.this, getResources().getString(R.string.alert__Initializing),
                            getResources().getString(R.string.common__login_in));
                }
            }
        });
    }

    private void hideLoginRingProgressDialog() {
        if (ringProgressDialog != null) {
            ringProgressDialog.dismiss();
            ringProgressDialog.cancel();
            ringProgressDialog = null;
        }
    }

    @OnClick(R.id.forgottenTextView)
    public void forgottenTextViewClick(View view) {
        Intent intent = new Intent(this, ResetPwActivity.class);
        Navigation.pushIntent(this, intent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i(TAG, "onActivityResult " + requestCode + " " + resultCode);
        if (resultCode == RESULT_CANCELED) {
            hideLoginRingProgressDialog();
        }
        switch (requestCode) {
            case FacebookLoginController.REQUEST_CODE:
                showLoginRingProgressDialog();
                facebookLoginController.onActivityResult(requestCode, resultCode, data);
                break;
//            default:
//                googleplusLoginController.connectGoogleApiClient();
//                break;

        }

    }

    private void facebookLogin(){
        // Mixpanel
        MixpanelAPI mixpanel = MixpanelAPI.getInstance(this, AppConfig.getMixpanelToken());
        mixpanel.track("Logged in with Facebook");

        facebookLoginController = new FacebookLoginController(this);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PermissionUtil.MY_PERMISSIONS_REQUEST_READ_PHONE_STATUS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    pressDone();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            case PermissionUtil.MY_PERMISSIONS_REQUEST_READ_PHONE_STATUS_FB: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    facebookLogin();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;

                // other 'case' lines to check for other
                // permissions this app might request
            }
        }
    }

    @Override
    public void updateLoginDbAndToMainTabActivity(ResponseLoginSocial.Content content) {
        LocalStorageHelper.setUpByLogin(this, content);

        ConfigDataBaseManager configDataBaseManager = ConfigDataBaseManager.getInstance(this);
        DatabaseManager.getInstance(this).dbActionForLogin(content);
        configDataBaseManager.dbActionMain(content.memId);

        hideLoginRingProgressDialog();
        AppUtil.goToMainTabActivity(this, content);
    }

    @Override
    public void loginFail(String failMsg) {
        Dialog.normalDialog(this, failMsg).show();
        hideLoginRingProgressDialog();
    }

    @Override
    public void loginCancel() {
        hideLoginRingProgressDialog();
    }
}
