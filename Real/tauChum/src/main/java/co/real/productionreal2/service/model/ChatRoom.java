package co.real.productionreal2.service.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hohojo on 18/11/2015.
 */
public class ChatRoom implements Parcelable {
    @SerializedName("ChatRoomID")
    public int chatRoomId;
    @SerializedName("DialogID")
    public String dialogId;
    @SerializedName("ChatRoomMember")
    public List<ChatRoomMember> chatRoomMemList;

    public ChatRoom(Parcel source) {
        chatRoomMemList = new ArrayList<>();
        source.readTypedList(chatRoomMemList, ChatRoomMember.CREATOR);
        dialogId = source.readString();
        chatRoomId = source.readInt();
    }


    public static final Creator<ChatRoom> CREATOR = new Creator<ChatRoom>() {
        @Override
        public ChatRoom createFromParcel(Parcel in) {
            return new ChatRoom(in);
        }

        @Override
        public ChatRoom[] newArray(int size) {
            return new ChatRoom[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(chatRoomMemList);
        dest.writeString(dialogId);
        dest.writeInt(chatRoomId);
    }
}
