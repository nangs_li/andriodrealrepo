package co.real.productionreal2.service.model.response;

import com.google.gson.annotations.SerializedName;
import co.real.productionreal2.model.AgentListing;

import java.util.List;

/**
 * Created by hohojo on 25/1/2016.
 */
public class ResponseAgentListingGetList extends ResponseBase {
    @SerializedName("Content")
    public Content content;

    public class Content {
        @SerializedName("TotalCount")
        public int totalCount;
        @SerializedName("RecordCount")
        public int recordCount;
        @SerializedName("AgentListings")
        public List<AgentListing> agentListingsList;
    }
}
