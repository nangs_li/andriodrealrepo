package co.real.productionreal2.util;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import co.real.productionreal2.Constants;
import co.real.productionreal2.model.SystemSetting;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;

/**
 * Created by hohojo on 10/8/2015.
 */
public class FileUtil {

    private static SystemSetting sysSetting = null;

    private static File createFolderIfNotExist(File basePath, String folderName) {
        File folder = null;
        try {
            folder = new File(basePath, folderName);
        } catch (NullPointerException exception) {
            Log.e("Folder creation", "cannot get the folder name");
            return folder;
        }
        try {
            //create the folder if it does not exist
            if (!folder.exists() && !folder.isDirectory()) {
                if (folder.mkdirs()) {
                    Log.i("Folder creation", "folder created:" + folder);
                } else {
                    Log.i("Folder creation", "folder exists:" + folder);
                }
            }
        } catch (SecurityException securityException) {
            Log.e("Folder creation", "access denied for the folder");
        }
        return folder;
    }

    public static boolean createFolders(File file) throws NullPointerException {
        File parentFile = file.getParentFile();
        return parentFile == null ? false : parentFile.mkdirs();
    }

    public static void convertBitmapToFile(File file, Bitmap bitmap) throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream); //compress ratio, e.g. 100 = no loss
        byte[] bitmapdata = outputStream.toByteArray();
        FileOutputStream fileOutputStream
                = new FileOutputStream(file);
        fileOutputStream.write(bitmapdata);
        fileOutputStream.close();
    }

    public static void convertBitmapToFile(String filePath, Bitmap bitmap) throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, outputStream); //compress ratio, e.g. 100 = no loss
        byte[] bitmapdata = outputStream.toByteArray();
        FileOutputStream fileOutputStream
                = new FileOutputStream(filePath.toString());
        fileOutputStream.write(bitmapdata);
        fileOutputStream.close();
    }


    public static Bitmap correctBitmapOrientation(String filePath, Bitmap bitmap) {
        ExifInterface exif;
        int angle = 0;
        try {
            exif = new ExifInterface(filePath);
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);
            Log.d("uri","correctBitmapOrientation: orientation: " +orientation);
            if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                angle = 90;
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                angle = 180;
            }else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                angle = 270;
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Matrix matrix1 = new Matrix();

        //set image rotation value to 45 degrees in matrix.
        matrix1.postRotate(angle);

        Log.d("BitmapOrientation", "correctBitmapOrientation: " + angle);

        //Create bitmap with new values.
        Bitmap photo = Bitmap.createBitmap(bitmap, 0, 0,
                bitmap.getWidth(), bitmap.getHeight(), matrix1, true);
        return photo;
    }

    public static File getAppFolder() {
        File appFolder = null;
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            appFolder = createFolderIfNotExist(Environment.getExternalStorageDirectory(),
                    Constants.APP_FOLDER_NAME);
            if (appFolder != null) {
                File noMediaFile = new File(appFolder, ".nomedia");
                try {
                    noMediaFile.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return appFolder;
    }

    public static File getAppSubFolder(String folderName) {
        File appFolder = getAppFolder();
        File appSubFolder = null;
        if (appFolder != null) {
            appSubFolder = createFolderIfNotExist(appFolder, folderName);
        }
        return appSubFolder;
    }

    public static Bitmap fileToBitmap(File file) {
        return BitmapFactory.decodeFile(file.getAbsolutePath());
    }

    public static String getDeviceId(Context context) {
        String deviceId;

        TelephonyManager mTelephony = (TelephonyManager) context
                .getSystemService(Context.TELEPHONY_SERVICE);
        if (mTelephony.getDeviceId() != null) {
            deviceId = mTelephony.getDeviceId(); // *** use for mobiles
        } else {
            deviceId = Settings.Secure.getString(context.getContentResolver(),
                    Settings.Secure.ANDROID_ID); // *** use for tablets
        }
        return deviceId;
    }

    public static void setSystemSetting(Context context, ResponseLoginSocial.Content content) {
        //http://www.vogella.com/tutorials/JavaSerialization/article.html
        // save the object to file
        FileOutputStream fos = null;
        ObjectOutputStream out = null;
        try {
            fos = context.openFileOutput("SYSTEM_SETTING", Context.MODE_PRIVATE);
            out = new ObjectOutputStream(fos);
            out.writeObject(content.systemSetting);

            out.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static SystemSetting getSystemSetting(Context context) {
        if(sysSetting == null){
            initSystemSetting(context);
        }
        return sysSetting;
    }

    public static void initSystemSetting(Context context) {
        // read the object from file
        FileInputStream fis = null;
        ObjectInputStream in = null;
        try {
            fis = context.openFileInput("SYSTEM_SETTING");
            in = new ObjectInputStream(fis);
            sysSetting = (SystemSetting) in.readObject();
            in.close();
        } catch (Exception ex) {
            if(Constants.isDevelopment) {
                ex.printStackTrace();
            }
        }
    }

    public static void writeJsonToFile(String fileName, String data,String folderName) {


        File root = Environment.getExternalStorageDirectory();
        File outDir = new File(root.getAbsolutePath() + File.separator + Constants.APP_FOLDER_NAME+ File.separator + folderName);

        // use a Unicode encoding
        Charset utf8 = Charset.forName("UTF-8");

        if (!outDir.isDirectory()) {
            outDir.mkdir();
        }
        try {
            if (!outDir.isDirectory()) {
                throw new IOException(
                        "Unable to create directory Real_DOC. Maybe the SD card is mounted?");
            }
            File outputFile = new File(outDir, fileName);
            OutputStream out = new FileOutputStream(outputFile);
            Writer writer = new OutputStreamWriter(out, utf8);
            writer.write("\ufeff");  //bom utf-8
            writer.write(data);
            writer.flush();
            writer.close();

        } catch (IOException e) {
            Log.w("IOException", e.getMessage(), e);
        }

    }


    public static String getJsonFileToString(String fileName) throws Exception {
        File root = Environment.getExternalStorageDirectory();
        File file = new File(root.getAbsolutePath() + File.separator + Constants.APP_DOC_FOLDER_NAME + File.separator + fileName);

        FileInputStream fin = new FileInputStream(file);
        String ret = convertStreamToString(fin);
        //Make sure you close all streams.
        fin.close();
        return ret;
    }

    public static String convertStreamToString(InputStream is) {

        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append((line + "\n"));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    public static String getRealPathFromURI(Context context, Uri contentURI) {
        String result;
        Cursor cursor = context.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    public static String getFileExt(String fileName) {
        String fileExt = fileName.substring((fileName.lastIndexOf(".") + 1), fileName.length());
        Log.w("getFileExt", "fileExt = " + fileExt);
        return fileExt;

    }

    public static void deleteFile(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles())
                deleteFile(child);

        fileOrDirectory.delete();
    }

    public static Document getDomElement(String xml) {
        Document doc = null;
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try {

            DocumentBuilder db = dbf.newDocumentBuilder();

            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(xml));
            doc = db.parse(is);

        } catch (ParserConfigurationException e) {
            Log.e("Error: ", e.getMessage());
            return null;
        } catch (SAXException e) {
            Log.e("Error: ", e.getMessage());
            return null;
        } catch (IOException e) {
            Log.e("Error: ", e.getMessage());
            return null;
        }
        // return DOM
        return doc;
    }

    public static String nodeToString(Node node) {
        StringWriter sw = new StringWriter();
        try {
            Transformer t = TransformerFactory.newInstance().newTransformer();
            t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            t.transform(new DOMSource(node), new StreamResult(sw));
        } catch (TransformerException te) {
            System.out.println("nodeToString Transformer Exception");
        }
        return sw.toString();
    }


    //get all space type
    public static List<SystemSetting.SpaceType> getAllSpaceType(Context context, int langIndex) {
        List<SystemSetting.PropertyType> propertyTypeArrayList = new ArrayList<>();
        propertyTypeArrayList = FileUtil.getSystemSetting(context).propertyTypeList;

        List<SystemSetting.SpaceType> allSpaceTypeArrayList = new ArrayList<>();

        //add all space type into a single list
        for (SystemSetting.PropertyType propertyType : propertyTypeArrayList) {
            for (SystemSetting.SpaceType spaceType : propertyType.spaceTypeList) {
                //add propertyType index into spaceType
                spaceType.propertyTypeIndex = propertyType.index;
                if (propertyType.langIndex == langIndex) {
                    allSpaceTypeArrayList.add(spaceType);
                }
            }
        }
        return allSpaceTypeArrayList;
    }


}
