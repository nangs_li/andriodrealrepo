package co.real.productionreal2.util;

import java.io.IOException;
import java.io.OutputStream;

import retrofit.mime.TypedOutput;

public class ByteArrayTypedOutput implements TypedOutput {

    private byte[] imageData;

    public ByteArrayTypedOutput(byte[] imageData){
        this.imageData = imageData;
    }

    @Override
    public String fileName() {
        return "filename";
    }

    @Override
    public String mimeType() {
        return "png";
    }

    @Override
    public long length() {
        return imageData.length;
    }

    @Override
    public void writeTo(OutputStream outputStream) throws IOException {
         outputStream.write(imageData);
    }
}