package co.real.productionreal2.activity.setting;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.mixpanel.android.mpmetrics.MixpanelAPI;

import java.util.UUID;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.BaseActivity;
import co.real.productionreal2.config.AppConfig; ;
import co.real.productionreal2.view.Dialog;

public class LogoutSettingActivity extends BaseActivity {
    @InjectView(R.id.headerLayout)
    View headerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logout_setting);
        ButterKnife.inject(this);
        initView();
    }

    private void initView() {
        TextView titleTextView = (TextView) headerLayout.findViewById(R.id.titleTextView);
        titleTextView.setText(R.string.common__log_out);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @OnClick(R.id.logoutBtn)
    public void logout(View view) {
        // Mixpanel
        MixpanelAPI mixpanel = MixpanelAPI.getInstance(this, AppConfig.getMixpanelToken());
        mixpanel.track("Logged Out");
        mixpanel.flush();

        mixpanel.reset();
        String newDistinctId = UUID.randomUUID().toString();
        mixpanel.identify(newDistinctId);
        mixpanel.getPeople().identify(mixpanel.getDistinctId());

        Dialog.settingLogoutDialog(this).show();
    }

}
