package co.real.productionreal2.service.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kelvinsun on 15/1/16.
 */
public class ActivityLog implements Parcelable{

    @SerializedName("ID")
    public int id;

    @SerializedName("MemberID")
    public int memberId;

    @SerializedName("CreationDate")
    public String createDate;

    @SerializedName("CreationDateString")
    public String createDateString;

    @SerializedName("LogType")
    public int logType;

    @SerializedName("BasedIn")
    public String baseIn;

    @SerializedName("DateOfJoin")
    public String dateOfJoin;

    @SerializedName("DateOfFollow")
    public String dateOfFollow;

    @SerializedName("DateOfJoinString")
    public String dateOfJoinString;

    @SerializedName("DateOfFollowString")
    public String dateOfFollowString;

    @SerializedName("FollowingCount")
    public int followingCount;

    @SerializedName("ActiveChatCount")
    public int activeChatCount;

    @SerializedName("Places")
    public List<ActivityPlace> activityPlaceList;

    public ActivityLog(){}

    public ActivityLog(Parcel in) {
        id	= in.readInt();
        memberId	= in.readInt();
        activityPlaceList = new ArrayList<>();
        in.readTypedList(activityPlaceList, ActivityPlace.CREATOR);
        createDate = in.readString();
        createDateString = in.readString();
        logType = in.readInt();
        baseIn = in.readString();
        dateOfJoin = in.readString();
        dateOfJoinString	= in.readString();
        dateOfFollowString	= in.readString();
        followingCount	= in.readInt();
        activeChatCount	= in.readInt();
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(memberId);
        dest.writeTypedList(activityPlaceList);
        dest.writeString(createDate);
        dest.writeString(createDateString);
        dest.writeInt(logType);
        dest.writeString(baseIn);
        dest.writeString(dateOfJoin);
        dest.writeString(dateOfJoinString);
        dest.writeString(dateOfFollowString);
        dest.writeInt(followingCount);
        dest.writeInt(activeChatCount);
    }


    public static final Parcelable.Creator<ActivityLog> CREATOR = new Parcelable.Creator<ActivityLog>() {
        @Override
        public ActivityLog createFromParcel(Parcel in) {
            return new ActivityLog(in);
        }

        @Override
        public ActivityLog[] newArray(int size) {
            return new ActivityLog[size];
        }
    };

    @Override
    public String toString() {
        return "ActivityLog{" +
                "id=" + id +
                ", memberId=" + memberId +
                ", createDate='" + createDate + '\'' +
                ", createDateString='" + createDateString + '\'' +
                ", logType=" + logType +
                ", baseIn='" + baseIn + '\'' +
                ", dateOfJoin='" + dateOfJoin + '\'' +
                ", dateOfFollow='" + dateOfFollow + '\'' +
                ", dateOfJoinString='" + dateOfJoinString + '\'' +
                ", dateOfFollowString='" + dateOfFollowString + '\'' +
                ", followingCount=" + followingCount +
                ", activeChatCount=" + activeChatCount +
                ", activityPlaceList=" + activityPlaceList +
                '}';
    }
}
