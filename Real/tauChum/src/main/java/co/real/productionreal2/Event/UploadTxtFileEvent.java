package co.real.productionreal2.Event;

import java.util.ArrayList;

import co.real.productionreal2.service.model.Follower;

/**
 * Created by kelvinsun on 2/6/16.
 */
public class UploadTxtFileEvent {
    public final boolean isSuccess;

    public UploadTxtFileEvent(boolean isSuccess) {
        this.isSuccess = isSuccess;
    }
}
