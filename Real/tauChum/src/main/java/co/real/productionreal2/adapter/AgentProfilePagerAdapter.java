package co.real.productionreal2.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import co.real.productionreal2.service.model.response.ResponseLoginSocial;

import java.util.List;

/**
 * Created by hohojo on 6/10/2015.
 */
public class AgentProfilePagerAdapter extends FragmentStatePagerAdapter {
    public static final int EDIT_PROFILE = 2;
    public static final int ME_HOME = 1;
    public static final int CREATE_POST_OR_PREVIEW = 0;
    private ResponseLoginSocial.Content userContent;
    private List<Fragment> fragments;

    public AgentProfilePagerAdapter(FragmentManager fm, ResponseLoginSocial.Content userContent, List<Fragment> fragments) {
        super(fm);
        this.userContent = userContent;
        this.fragments = fragments;
    }

//    @Override
//    public int getItemPosition(Object object) {
//        return POSITION_NONE;
//    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;

        switch (position) {
            case EDIT_PROFILE:
                //todo edit agent profile
//                fragment = ViewAgentProfileFragment.newInstance();
                fragment = fragments.get(0);
                break;
            case ME_HOME:
                fragment = fragments.get(1);
//                fragment = CreateUpdatePostFragment.newInstance(getCompleteRate(), newsFeedDetailFragment);
                break;
            case CREATE_POST_OR_PREVIEW:
                if (userContent == null || userContent.agentProfile == null ||
                        userContent.agentProfile.agentListing == null)
                    fragment = fragments.get(2);
                else {
                    fragment = fragments.get(3);
                }

                break;
        }
        return fragment;
    }

//    private int getCompleteRate() {
//        int completeRate = 0;
//        if (userContent == null)
//            return 0;
//        if (userContent.agentProfile == null)
//            return 0;
//
//        if (userContent.agentProfile.agentExpList != null && userContent.agentProfile.agentExpList.size() > 0) {
//            completeRate = completeRate + 20;
//        }
//        if (userContent.agentProfile.agentLicenseNumber != null && !userContent.agentProfile.agentLicenseNumber.isEmpty()) {
//            completeRate = completeRate + 20;
//        }
//        if (userContent.agentProfile.agentLanguageList != null && userContent.agentProfile.agentLanguageList.size() > 0) {
//            completeRate = completeRate + 20;
//        }
//        if (userContent.agentProfile.agentSpecialtyList != null && userContent.agentProfile.agentSpecialtyList.size() > 0) {
//            completeRate = completeRate + 20;
//        }
//        if (userContent.agentProfile.agentPastClosingList != null && userContent.agentProfile.agentPastClosingList.size() > 0) {
//            completeRate = completeRate + 20;
//        }
//        return completeRate;
//    }


    @Override
    public int getCount() {
        return 3;
    }
}
