package co.real.productionreal2.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;

import com.mixpanel.android.mpmetrics.MixpanelAPI;

import java.util.List;
import java.util.UUID;

import co.real.productionreal2.Navigation;
import co.real.productionreal2.adapter.SettingAdapter;
import co.real.productionreal2.config.AppConfig; ;
import co.real.productionreal2.model.SettingItem;
import co.real.productionreal2.util.RecyclerItemClickListener;

/**
 * Created by alexhung on 15/4/16.
 */
public class SettingSectionRecyclerView extends RecyclerView {
    SettingAdapter settingAdapter;
    List<SettingItem> settingItems;
    Activity context;

    public SettingSectionRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SettingSectionRecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void initAdapterWithItems(List<SettingItem> items, final Activity context) {
        this.context = context;
        this.settingItems = items;
        this.settingAdapter = new SettingAdapter(items);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        this.setLayoutManager(mLayoutManager);
        this.setItemAnimator(new DefaultItemAnimator());
        this.setAdapter(settingAdapter);
        this.addOnItemTouchListener(new RecyclerItemClickListener(context, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                SettingItem item = settingItems.get(position);
                if (item != null) {
                    SettingItem.SettingItemType type = item.itemType;
                    Class<?> targetClass = item.targetClass;
                    if (type == SettingItem.SettingItemType.SETTING_TYPE_LOGOUT){
                        MixpanelAPI mixpanel = MixpanelAPI.getInstance(context, AppConfig.getMixpanelToken());
                        mixpanel.track("Logged Out");
                        mixpanel.flush();

                        mixpanel.reset();
                        String newDistinctId = UUID.randomUUID().toString();
                        mixpanel.identify(newDistinctId);
                        mixpanel.getPeople().identify(mixpanel.getDistinctId());

                        Dialog.settingLogoutDialog(context).show();
                    }else if (type != SettingItem.SettingItemType.SETTING_TYPE_SECTION_SPACING && targetClass != null) {
                        Intent intent = new Intent(SettingSectionRecyclerView.this.context, targetClass);
                        Navigation.pushIntent(SettingSectionRecyclerView.this.context, intent);
                    }
                }
            }
        }));
    }


    public SettingSectionRecyclerView(Context context) {
        super(context);
    }
}
