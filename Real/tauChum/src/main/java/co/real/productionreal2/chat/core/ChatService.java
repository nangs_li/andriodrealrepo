package co.real.productionreal2.chat.core;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.google.gson.Gson;
import com.quickblox.auth.QBAuth;
import com.quickblox.auth.model.QBSession;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.model.QBDialog;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.core.request.QBPagedRequestBuilder;
import com.quickblox.core.request.QBRequestGetBuilder;
import com.quickblox.core.request.QueryRule;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;

import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import co.real.productionreal2.ApplicationSingleton;
import co.real.productionreal2.Constants;
import co.real.productionreal2.activity.MainActivityTabBase;
import co.real.productionreal2.activity.chat.ChatActivity;
import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.chat.ChatDbViewController;
import co.real.productionreal2.common.LocalStorageHelper;
import co.real.productionreal2.daomanager.DatabaseManager;
import co.real.productionreal2.model.UserQBUser;
import co.real.productionreal2.service.BaseService;
import co.real.productionreal2.service.model.AgentProfiles;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.ChatRoom;
import co.real.productionreal2.service.model.request.RequestAgentProfileGetListEx;
import co.real.productionreal2.service.model.request.RequestChatRoomGet;
import co.real.productionreal2.service.model.response.ResponseAgentProfileListEx;
import co.real.productionreal2.service.model.response.ResponseChatRoomGet;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;

public class ChatService extends BaseService{

    private static final String TAG = ChatService.class.getSimpleName();


    private static ChatService instance;
    private String connectStatus = CONNECTION_CLOSED;
    private int maxIdsPerRequest = 10;
    public static String CONNECT = "connected";
    public static String AUTHENTICATED = "authenticated";
    public static String CONNECTION_CLOSED = "connectionClosed";
    private Context baseContext = ApplicationSingleton.getInstance().getBaseContext();
    private HashMap<String,ArrayList<RequestAgentProfileGetListEx>> pendingAgentProfileRequestHashMap;
    private HashMap<String,ArrayList<RequestChatRoomGet>> pendingChatRoomGetRequestHashMap;
    private HashMap<String,ArrayList<String>> pendingDialogIdHashMap;
    public static synchronized ChatService getInstance() {
        if (instance == null) {
            instance = new ChatService();
        }
        return instance;
    }

    public static boolean initIfNeed(Context ctx) {
        if (!QBChatService.isInitialized()) {
            QBChatService.setDebugEnabled(true);
            QBChatService.init(ctx);
            Log.d(TAG, "Initialise QBChatService");

            return true;
        }
        Log.d(TAG, "Initialise QBChatService false");
        return false;
    }

    private QBChatService chatService;

    private ChatService() {
        chatService = QBChatService.getInstance();
        chatService.addConnectionListener(chatConnectionListener);
        pendingAgentProfileRequestHashMap = new HashMap<>();
        pendingChatRoomGetRequestHashMap = new HashMap<>();
        pendingDialogIdHashMap = new HashMap<>();
    }

    public void addConnectionListener(ConnectionListener listener) {
        chatService.addConnectionListener(listener);
    }

    public void removeConnectionListener(ConnectionListener listener) {
        chatService.removeConnectionListener(listener);
    }

    public void login(final QBUser user, final QBEntityCallback callback) {
        // Create REST API session
        //
        QBAuth.createSession(user, new QBEntityCallbackImpl<QBSession>() {
            @Override
            public void onSuccess(QBSession session, Bundle args) {
                user.setId(session.getUserId());

                // login to Chat
                //
                loginToChat(user, new QBEntityCallbackImpl() {

                    @Override
                    public void onSuccess() {
                        if (MainActivityTabBase.activityIsRunning)
                            UserQBUser.qbUser = user;
                        callback.onSuccess();
                    }

                    @Override
                    public void onError(List errors) {
                        callback.onError(errors);
                    }
                });
            }

            @Override
            public void onError(List<String> errors) {
                callback.onError(errors);
            }
        });
    }

    public void logout() {
        QBChatService.getInstance().logout(new QBEntityCallbackImpl() {
            @Override
            public void onSuccess() {
                Log.w(TAG, "logout success in ChatService");
            }

            @Override
            public void onError(List list) {
                Log.e(TAG, "logout fail in ChatService: " + list);
            }
        });
    }

    public void loginToChat(final QBUser user, final QBEntityCallback callback) {

        chatService.login(user, new QBEntityCallbackImpl() {
            @Override
            public void onSuccess() {

                // Start sending presences
                //
                try {
                    chatService.startAutoSendPresence(Constants.AUTO_PRESENCE_INTERVAL_IN_SECONDS);
                } catch (SmackException.NotLoggedInException e) {
                    e.printStackTrace();
                }

                callback.onSuccess();
                Log.w(TAG, "login success in ChatService");
            }

            @Override
            public void onError(List errors) {
                callback.onError(errors);
                Log.e(TAG, "login fail in ChatService: " + errors);
            }
        });
    }

    public void saveDialogs(Collection<QBDialog> dialogs){
        DatabaseManager.getInstance(baseContext).saveQBChatDialogsToDB(dialogs);
    }

    public void saveQBUsers(Collection<QBUser> users){
        DatabaseManager.getInstance(baseContext).saveQBUsersToDB(users);
    }

    public void saveAgentProfile(Collection<AgentProfiles> agentProfile){
        DatabaseManager.getInstance(baseContext).saveAgentProfilesToDB(agentProfile);
    }

    public void saveChatRoom(Collection<ChatRoom> chatRooms){
        DatabaseManager.getInstance(baseContext).saveQBChatRoomsSettingToDB(chatRooms);
    }

    public void getDialogById(final String dialogId, final BaseServiceCallBack callBack){
        ApplicationSingleton.getInstance().currentActivity.runOnUiThread(new Runnable() {
            public void run() {
                Log.v("", "edwin ChatService getDialogById ");
                final String uuid = generatePendingArray();
                QBRequestGetBuilder customObjectRequestBuilder = new QBRequestGetBuilder();
                customObjectRequestBuilder.setPagesLimit(1);
                customObjectRequestBuilder.addRule("_id", QueryRule.EQ, dialogId);
                customObjectRequestBuilder.setPagesLimit(1);
                QBChatService.getChatDialogs(null, customObjectRequestBuilder, new QBEntityCallbackImpl<ArrayList<QBDialog>>() {
                    @Override
                    public void onSuccess(final ArrayList<QBDialog> dialogs, Bundle args) {
                        Log.v("", "edwin ChatService getDialogById onSuccess dialogs size " + dialogs.size());
                        saveDialogs(dialogs);
                        // collect all occupants ids
                        //
                        List<Integer> usersIDs = new ArrayList<Integer>();
                        ArrayList<String> dialogIds = new ArrayList<String>();
                        for (QBDialog dialog : dialogs) {
                            dialogIds.add(dialog.getDialogId());
                            usersIDs.addAll(dialog.getOccupants());
                        }

                        setPendingDialogIdsByUUID(uuid, dialogIds);

                        // Get all occupants info
                        //
                        QBPagedRequestBuilder requestBuilder = new QBPagedRequestBuilder();
                        requestBuilder.setPage(1);
                        requestBuilder.setPerPage(usersIDs.size());
                        //
                        Log.v("", "edwin ChatService getUsersByIDs ");
                        QBUsers.getUsersByIDs(usersIDs, requestBuilder, new QBEntityCallbackImpl<ArrayList<QBUser>>() {
                            @Override
                            public void onSuccess(ArrayList<QBUser> users, Bundle params) {
                                Log.v("", "edwin ChatService getUsersByIDs onSuccess users size " + users.size());
                                saveQBUsers(users);
                                ArrayList<String> qbids = new ArrayList<String>();
                                for (QBUser user : users) {
                                    qbids.add("" + user.getId());
                                }
                                getAgentProfileByQBIDs(qbids, uuid, true, callBack);
                            }

                            @Override
                            public void onError(List<String> errors) {
                                Log.v("", "edwin ChatService getUsersByIDs onError  ");
                                RealServiceError error = new RealServiceError(99);
                                callBack.onError(error);
                            }

                        });
                    }

                    @Override
                    public void onError(List<String> errors) {
                        Log.v("", "edwin ChatService getDialogById onError  ");
                        RealServiceError error = new RealServiceError(99);
                        callBack.onError(error);
                    }
                });

            }
        });
    }
    private String generatePendingArray(){
        final String uuid = UUID.randomUUID().toString().replaceAll("-", "");
        ArrayList<RequestChatRoomGet> chatRoomGets = new ArrayList<>();
        ArrayList<RequestAgentProfileGetListEx> agentProfileGetLists = new ArrayList<>();
        ArrayList<String>  dialogIds = new ArrayList<>();
        pendingAgentProfileRequestHashMap.put(uuid,agentProfileGetLists);
        pendingChatRoomGetRequestHashMap.put(uuid,chatRoomGets);
        pendingDialogIdHashMap.put(uuid,dialogIds);
        return uuid;
    }

    private ArrayList<RequestAgentProfileGetListEx> getPendingAgentProfileRequestByUUID(String uuid){
       return pendingAgentProfileRequestHashMap.get(uuid);
    }

    private ArrayList<RequestChatRoomGet> getPendingChatRoomRequestByUUID(String uuid){
        return pendingChatRoomGetRequestHashMap.get(uuid);
    }

    private ArrayList<String > getPendingDialogIdsByUUID(String uuid){
        return pendingDialogIdHashMap.get(uuid);
    }

    private ArrayList<RequestAgentProfileGetListEx> setPendingAgentProfileRequestByUUID(String uuid,ArrayList<RequestAgentProfileGetListEx> requests){
        ArrayList<RequestAgentProfileGetListEx> pendingRequests = getPendingAgentProfileRequestByUUID(uuid);
        pendingRequests = new ArrayList<>(requests);
        pendingAgentProfileRequestHashMap.put(uuid,pendingRequests);
        return pendingRequests;
    }

    private ArrayList<RequestChatRoomGet> setPendingChatRoomRequestByUUID(String uuid,ArrayList<RequestChatRoomGet> requests){
        ArrayList<RequestChatRoomGet> pendingRequests = getPendingChatRoomRequestByUUID(uuid);
        pendingRequests = new ArrayList<>(requests);
        pendingChatRoomGetRequestHashMap.put(uuid,pendingRequests);
        return pendingRequests;
    }

    private ArrayList<String> setPendingDialogIdsByUUID(String uuid,ArrayList<String> dialogIDs){
        ArrayList<String> pendingIDs = getPendingDialogIdsByUUID(uuid);
        pendingIDs = new ArrayList<>(dialogIDs);
        pendingDialogIdHashMap.put(uuid,pendingIDs);
        return pendingIDs;
    }

    public void getAllDialogs(final BaseServiceCallBack callBack){
        final String uuid = generatePendingArray();
        QBRequestGetBuilder customObjectRequestBuilder = new QBRequestGetBuilder();
        customObjectRequestBuilder.setPagesLimit(100);
        customObjectRequestBuilder.sortAsc("last_message_date_sent");

        QBChatService.getChatDialogs(null, customObjectRequestBuilder, new QBEntityCallbackImpl<ArrayList<QBDialog>>() {
            @Override
            public void onSuccess(final ArrayList<QBDialog> dialogs, Bundle args) {

                saveDialogs(dialogs);
                // collect all occupants ids
                //
                List<Integer> usersIDs = new ArrayList<Integer>();
                ArrayList<String> pendingDialogIds = new ArrayList<String>();
                for (QBDialog dialog : dialogs) {
                    pendingDialogIds.add(dialog.getDialogId());
                    usersIDs.addAll(dialog.getOccupants());
                }

                setPendingDialogIdsByUUID(uuid,pendingDialogIds);

                // Get all occupants info
                //
                QBPagedRequestBuilder requestBuilder = new QBPagedRequestBuilder();
                requestBuilder.setPage(1);
                requestBuilder.setPerPage(usersIDs.size());
                //
                QBUsers.getUsersByIDs(usersIDs, requestBuilder, new QBEntityCallbackImpl<ArrayList<QBUser>>() {
                    @Override
                    public void onSuccess(ArrayList<QBUser> users, Bundle params) {
                        saveQBUsers(users);
                        ArrayList<String>qbids = new ArrayList<String>();
                        for (QBUser user:users) {
                            qbids.add(""+user.getId());
                        }
                        getAgentProfileByQBIDs(qbids,uuid,true,callBack);
                    }

                    @Override
                    public void onError(List<String> errors) {
                        RealServiceError error = new RealServiceError(99);
                        callBack.onError(error);
                    }

                });
            }

            @Override
            public void onError(List<String> errors) {
                RealServiceError error = new RealServiceError(99);
                callBack.onError(error);
            }
        });
    }

    public void getAgentProfileByQBIDs(Collection<String> qbids, final BaseServiceCallBack callBack){
        final String uuid = UUID.randomUUID().toString().replaceAll("-", "");
        getAgentProfileByQBIDs(qbids,uuid,false,callBack);
    }

    public void getAgentProfileByQBIDs(Collection<String> qbids, final String uuid, final Boolean getChatRoomStatus, final BaseServiceCallBack callBack){
        Log.v("","edwin ChatService getAgentProfileByQBIDs ");
        if(DatabaseManager.getInstance(baseContext).getLogInfo() == null|| DatabaseManager.getInstance(baseContext).getLogInfo().getLogInfo() == null){
            return;
        }
        ResponseLoginSocial.Content userContent = new Gson().fromJson(DatabaseManager.getInstance(baseContext).getLogInfo().getLogInfo(),
                ResponseLoginSocial.Content.class);
        ArrayList<String> tempIds = new ArrayList<>();
        ArrayList<RequestAgentProfileGetListEx> tempRequests = new ArrayList<>();
        for (String id :qbids) {
             if (tempIds.size()<maxIdsPerRequest){
                 tempIds.add(id);
             }else {
                 RequestAgentProfileGetListEx profileRequest = new RequestAgentProfileGetListEx(userContent.memId, LocalStorageHelper.getAccessToken(baseContext), tempIds);
                 tempRequests.add(profileRequest);
                 tempIds.clear();
                 tempIds.add(id);
             }
        }
        if (tempIds.size()>0){
            RequestAgentProfileGetListEx profileRequest = new RequestAgentProfileGetListEx(userContent.memId, LocalStorageHelper.getAccessToken(baseContext), tempIds);
            tempRequests.add(profileRequest);
        }
        setPendingAgentProfileRequestByUUID(uuid,tempRequests);

        for (final RequestAgentProfileGetListEx request : tempRequests){
            request.callAgentProfileGetListExApi(baseContext, new ApiCallback() {
                @Override
                public void success(ApiResponse apiResponse) {
                    getPendingAgentProfileRequestByUUID(uuid).remove(request);
                    ResponseAgentProfileListEx agentProfileListResponseEx = new Gson()
                            .fromJson(apiResponse.jsonContent,
                                    ResponseAgentProfileListEx.class);
                    int errorCode = agentProfileListResponseEx.errorCode;
                    RealServiceError serviceError = null;
                    if (errorCode != 0){
                        serviceError = new RealServiceError(errorCode);
                    }

                    if (serviceError != null){
                        callBack.onError(serviceError);
                    }else{
                        List<AgentProfiles> agentProfileList = agentProfileListResponseEx.content.profiles;
                        saveAgentProfile(agentProfileList);
                    }

                    if (getPendingAgentProfileRequestByUUID(uuid).size() == 0){
                        if (getChatRoomStatus) {
                            getChatRoomByDialogIds(getPendingDialogIdsByUUID(uuid), uuid, callBack);
                        }else{
                            callBack.onSuccess("");
                        }
                    }
                }

                @Override
                public void failure(String errorMsg) {
                    getPendingAgentProfileRequestByUUID(uuid).remove(request);
                    if (getPendingAgentProfileRequestByUUID(uuid).size() == 0){
                        if (getChatRoomStatus) {
                            getChatRoomByDialogIds(getPendingDialogIdsByUUID(uuid), uuid, callBack);
                        }else{
                            callBack.onSuccess("");
                        }
                    }
                }
            });
        }
    }

    public void getChatRoomByDialogIds(Collection<String>dialogIDs, final String uuid, final  BaseServiceCallBack callBack){
        if (DatabaseManager.getInstance(baseContext).getLogInfo()==null)
            return;
        ResponseLoginSocial.Content userContent = new Gson().fromJson(DatabaseManager.getInstance(baseContext).getLogInfo().getLogInfo(),
                ResponseLoginSocial.Content.class);

        ArrayList<String> tempIds = new ArrayList<>();
        ArrayList<RequestChatRoomGet> tempRequests = new ArrayList<>();
        for (String id :dialogIDs) {
            if (tempIds.size()<maxIdsPerRequest){
                tempIds.add(id);
            }else {
                RequestChatRoomGet profileRequest = new RequestChatRoomGet(userContent.memId, LocalStorageHelper.getAccessToken(baseContext), tempIds);
                tempRequests.add(profileRequest);
                tempIds.clear();
                tempIds.add(id);
            }
        }
        if (tempIds.size()>0){
            RequestChatRoomGet profileRequest = new RequestChatRoomGet(userContent.memId, LocalStorageHelper.getAccessToken(baseContext), tempIds);
            tempRequests.add(profileRequest);
        }
        setPendingChatRoomRequestByUUID(uuid,tempRequests);
        for (final RequestChatRoomGet request : tempRequests) {
                request.callChatRoomGetApi(baseContext, new ApiCallback() {
                    @Override
                    public void success(ApiResponse apiResponse) {
                        ResponseChatRoomGet responseChatRoomGet = new Gson().fromJson(apiResponse.jsonContent, ResponseChatRoomGet.class);
                        int errorCode = responseChatRoomGet.errorCode;
                        RealServiceError serviceError = null;
                        if (errorCode != 0){
                            serviceError = new RealServiceError(errorCode);
                        }

                        if (serviceError != null){
                            callBack.onError(serviceError);
                        }else{
                            saveChatRoom(responseChatRoomGet.content.chatRoom);
                        }
                        getPendingChatRoomRequestByUUID(uuid).remove(request);
                        if (getPendingChatRoomRequestByUUID(uuid).size() == 0){
                            callBack.onSuccess("");
                        }
                    }

                    @Override
                    public void failure(String errorMsg) {
                        getPendingChatRoomRequestByUUID(uuid).remove(request);
                        if (getPendingChatRoomRequestByUUID(uuid).size() == 0){
                            callBack.onSuccess("");
                        }
                    }
                });

        }
    }


    private Map<Integer, QBUser> dialogsUsers = new HashMap<Integer, QBUser>();

    public Map<Integer, QBUser> getDialogsUsers() {
        return dialogsUsers;
    }

    public void setDialogsUsers(List<QBUser> setUsers) {
        dialogsUsers.clear();

        for (QBUser user : setUsers) {
            dialogsUsers.put(user.getId(), user);
        }
    }

    public void addDialogsUsers(List<QBUser> newUsers) {
        for (QBUser user : newUsers) {
            dialogsUsers.put(user.getId(), user);
        }
    }

    public QBUser getCurrentUser() {
        return QBChatService.getInstance().getUser();
    }

    public Integer getOpponentIDForPrivateDialog(QBDialog dialog) {
        Integer opponentID = -1;
        for (Integer userID : dialog.getOccupants()) {
            if (!userID.equals(getCurrentUser().getId())) {
                opponentID = userID;
                break;
            }
        }
        return opponentID;
    }

    public String getConnectStatus() {
        return connectStatus;
    }

    public void setConnectStatus(String connectStatus) {
        this.connectStatus = connectStatus;
    }

    public static ConnectionListener chatConnectionListener = new ConnectionListener() {
        @Override
        public void connected(XMPPConnection connection) {
            Log.i(TAG, "connected");
            ChatService.getInstance().setConnectStatus(CONNECT);

            if (ChatActivity.activityIsRunning) {

                ChatDbViewController.getInstance(ChatActivity.context).connected(connection);
            }
        }

        @Override
        public void authenticated(XMPPConnection connection) {
            Log.i(TAG, "authenticated");
            ChatService.getInstance().setConnectStatus(AUTHENTICATED);

            if (ChatActivity.activityIsRunning) {

                ChatDbViewController.getInstance(ChatActivity.context).authenticated(connection);
            }
        }

        @Override
        public void connectionClosed() {
            Log.i(TAG, "connectionClosed");
            ChatService.getInstance().setConnectStatus(CONNECTION_CLOSED);

            if (ChatActivity.activityIsRunning) {

                ChatDbViewController.getInstance(ChatActivity.context).connectionClosed();
            }
        }

        @Override
        public void connectionClosedOnError(final Exception e) {
            Log.i(TAG, "connectionClosedOnError: " + e.getLocalizedMessage());
            ChatService.getInstance().setConnectStatus(CONNECTION_CLOSED);

            if (ChatActivity.activityIsRunning) {

                ChatDbViewController.getInstance(ChatActivity.context).connectionClosedOnError(e);
            }
        }

        @Override
        public void reconnectingIn(final int seconds) {

            if (seconds % 5 == 0) {
                Log.i(TAG, "reconnectingIn: " + seconds);
                ChatService.getInstance().setConnectStatus(CONNECTION_CLOSED);
            }

            if (ChatActivity.activityIsRunning) {

                ChatDbViewController.getInstance(ChatActivity.context).reconnectingIn(seconds);
            }
        }

        @Override
        public void reconnectionSuccessful() {
            Log.i(TAG, "reconnectionSuccessful");
            ChatService.getInstance().setConnectStatus(CONNECT);

            if (ChatActivity.activityIsRunning) {

                ChatDbViewController.getInstance(ChatActivity.context).reconnectionSuccessful();
            }
        }

        @Override
        public void reconnectionFailed(final Exception error) {
            Log.i(TAG, "reconnectionFailed: " + error.getLocalizedMessage());
            ChatService.getInstance().setConnectStatus(CONNECTION_CLOSED);

            if (ChatActivity.activityIsRunning) {

                ChatDbViewController.getInstance(ChatActivity.context).reconnectionFailed(error);
            }
        }
    };
}
