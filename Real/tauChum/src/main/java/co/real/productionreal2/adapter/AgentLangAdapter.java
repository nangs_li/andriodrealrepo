package co.real.productionreal2.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;

import co.real.productionreal2.R;
import co.real.productionreal2.model.SystemSetting;
import co.real.productionreal2.service.model.AgentLanguage;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hohojo on 16/9/2015.
 */
public class AgentLangAdapter extends BaseAdapter {
    private Context context;
    private List<SystemSetting.SpokenLanguage> spokenLangList;
    private List<AgentLanguage> agentSpokenLangList;
    private List<Boolean> spokenLangIsCheckedList;
    private ArrayList<String> updateAgentSpokenLangNativeNameList;


    public AgentLangAdapter(Context context, List<SystemSetting.SpokenLanguage> spokenLangList, List<AgentLanguage> agentSpokenLangList) {

        this.context = context;
        this.spokenLangList = spokenLangList;
        this.agentSpokenLangList = agentSpokenLangList;
        spokenLangIsCheckedList = new ArrayList<>();
        updateAgentSpokenLangNativeNameList = new ArrayList<>();

        for (int i = 0; i < spokenLangList.size(); i++) {
            spokenLangIsCheckedList.add(initSpokenLangIsChecked(spokenLangList.get(i)));
        }

    }

    private boolean initSpokenLangIsChecked(SystemSetting.SpokenLanguage spokenLanguage) {
        for (int i = 0; i < agentSpokenLangList.size(); i++) {
            if (spokenLanguage.index == agentSpokenLangList.get(i).langIndex)
                return true;
        }
        return false;
    }

    @Override
    public int getCount() {
        return spokenLangList.size();
    }

    @Override
    public Object getItem(int position) {
        return spokenLangList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        final ViewHolder holder;
        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.lang_list_item, null);
            holder = createViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

//        holder.langCheckedTextView.setText(spokenLangList.get(position).nativeName);
        final SystemSetting.SpokenLanguage spokenLanguage = spokenLangList.get(position);

        holder.langCheckedTextView.setText(spokenLanguage.nativeName);


        return convertView;
    }


    public void setSpokenLangIsCheckedList(List<Boolean> spokenLangIsCheckedList) {
        this.spokenLangIsCheckedList = spokenLangIsCheckedList;
    }


    public ArrayList<AgentLanguage> getUpdateAgentSpokenLangList() {
        ArrayList<AgentLanguage> updatedAgentSpokenLangList = new ArrayList<>();
        for (int i = 0; i < spokenLangIsCheckedList.size(); i++) {
            if (spokenLangIsCheckedList.get(i)) {
                AgentLanguage agentLang = new AgentLanguage();
                agentLang.langIndex = spokenLangList.get(i).index;
                agentLang.lang = spokenLangList.get(i).nativeName;
                updateAgentSpokenLangNativeNameList.add(spokenLangList.get(i).nativeName);
                updatedAgentSpokenLangList.add(agentLang);
            }
        }
        return updatedAgentSpokenLangList;
    }
    public ArrayList<String> getUpdateAgentSpokenLangNativeNameList() {
        return updateAgentSpokenLangNativeNameList;
    }

    public List<Boolean> getSpokenLangIsCheckedList() {
        return spokenLangIsCheckedList;
    }

    private ViewHolder createViewHolder(View v) {
        ViewHolder holder = new ViewHolder();
        holder.langCheckedTextView = (CheckedTextView) v.findViewById(R.id.langCheckedTextView);
        return holder;
    }


    private static class ViewHolder {
        public CheckedTextView langCheckedTextView;
    }
}

