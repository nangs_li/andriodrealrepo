package co.real.productionreal2.util;

import android.content.Context;

/**
 * Created by alexhung on 13/4/16.
 */
public class StringUtils {
    private StringUtils() {
    }

    public static String getById(Context context,int id,String defaultValue){
        try {
            return context.getString(id);
        }catch (Exception e){
            return defaultValue;
        }
    }

    public static String getByIdWithVars(Context context,int id,String defaultValue, Object... args){
        String format = StringUtils.getById(context,id,defaultValue);
        String string = "";
        try {
            string = String.format(format,args);
        }catch (Exception e) {
            e.printStackTrace();
            string = defaultValue;
        }
        return string;
    }

}
