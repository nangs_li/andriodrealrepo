package co.real.productionreal2.fragment;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.quickblox.chat.model.QBDialog;
import com.quickblox.users.model.QBUser;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import co.real.productionreal2.Constants;
import co.real.productionreal2.CoreData;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.MainActivityTabBase;
import co.real.productionreal2.activity.chat.ChatActivity;
import co.real.productionreal2.activity.viral.InviteToChatActivity;
import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.callback.ChatLobbyDbViewUpdateListener;
import co.real.productionreal2.callback.DialogCallback;
import co.real.productionreal2.callback.FragmentViewChangeListener;
import co.real.productionreal2.chat.BaseChatFragment;
import co.real.productionreal2.chat.ChatDbViewController;
import co.real.productionreal2.chat.ChatLobbyDbViewController;
import co.real.productionreal2.chat.core.ChatService;
import co.real.productionreal2.common.LocalStorageHelper;
import co.real.productionreal2.config.AppConfig;
import co.real.productionreal2.dao.account.DBChatroomAgentProfiles;
import co.real.productionreal2.dao.account.DBQBChatDialog;
import co.real.productionreal2.dao.account.DBQBUser;
import co.real.productionreal2.daomanager.DatabaseManager;
import co.real.productionreal2.model.AgentListing;
import co.real.productionreal2.model.CurrentFollower;
import co.real.productionreal2.newsfeed.HeaderRecyclerViewAdapterV2;
import co.real.productionreal2.newsfeed.NewsFeedDetailFragment;
import co.real.productionreal2.newsfeed.NewsFeedProfileFragment;
import co.real.productionreal2.service.BaseService;
import co.real.productionreal2.service.model.AgentProfiles;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.request.RequestAgentProfileGetListEx;
import co.real.productionreal2.service.model.request.RequestAgentSearch;
import co.real.productionreal2.service.model.request.RequestChatRoomGet;
import co.real.productionreal2.service.model.request.RequestFollowAgent;
import co.real.productionreal2.service.model.response.ResponseAgentProfileListEx;
import co.real.productionreal2.service.model.response.ResponseChatRoomGet;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;
import co.real.productionreal2.service.model.response.ResponseNewsFeed;
import co.real.productionreal2.util.AppUtil;
import co.real.productionreal2.util.CryptLib;
import co.real.productionreal2.util.QBUtil;
import co.real.productionreal2.util.TimeUtils;
import co.real.productionreal2.view.Dialog;
import co.real.productionreal2.view.FollowButton;
import co.real.productionreal2.view.TitleBar;
import widget.ComputeVerticalScrollOffAbleRecyclerView;
import widget.EnbleableSwipeViewPager;
import widget.InterceptableListener;
import widget.InterceptableRelativeLayout;
import widget.RoundedImageView;


public class ChatLobbyFragment extends BaseChatFragment implements ChatLobbyDbViewUpdateListener, MainActivityTabBase.ChatlobbyUpdateListener {
    private static final String TAG = "ChatLobbyFragment";

    private static ChatLobbyAdapter chatLobbyAdapter;
    @InjectView(R.id.chatLobbyListView)
    ComputeVerticalScrollOffAbleRecyclerView chatLobbyListView;
    @InjectView(R.id.chatLobbyProgressBar)
    ProgressBar chatLobbyProgressBar;

    private TitleBar mTitlebar;
    public static boolean fragmentIsRunning = false;
    public static Context context;
    public static ChatLobbyFragment chatLobbyFragment;
    private ResponseLoginSocial.Content userContent;
    public static String searchingModeText = "";
    /**
     * Remember the position of the selected item.
     */
    private static final String STATE_SELECTED_POSITION = "selected_navigation_drawer_position";

    /**
     * Per the design guidelines, you should show the drawer on launch until the
     * user manually expands it. This shared preference tracks this.
     */
    private static final String PREF_USER_LEARNED_DRAWER = "navigation_drawer_learned";

    private FragmentViewChangeListener fragmentViewChangeListener;
    private ChatLobbyDbViewUpdateListener chatLobbyDbViewUpdateListener;
    private boolean isAgentSearchingMode = false;

    /**
     * Helper component that ties the action bar to the navigation drawer.
     */

    private DrawerLayout mDrawerLayout;
    private View mFragmentContainerView;

    private int mCurrentSelectedPosition = 0;
    private boolean mFromSavedInstanceState;

    private int PAGE_NO = 1;
    private boolean hasPageEnd = false;
    int pastVisiblesItems, visibleItemCount, totalItemCount;

    private View mainView;


    List<AgentProfiles> agentProfileList;
    //    AgentProfiles[] userProfiles;
    private DatabaseManager databaseManager;
    private boolean isAgentSearchTimerRunning = false;
//    public static boolean ableToSwipe = false;

    private NewsFeedDetailFragment newsFeedDetailFragment;
    private NewsFeedProfileFragment newsFeedProfileFragment;

    LinearLayoutManager mLayoutManager;

    private ViewPager pager;
    private InterceptableRelativeLayout mInterceptableRelativeLayout;

    private LinearLayout newsfeed_moving_cover_layout;

    ArrayList<DBQBChatDialog> searchDialogList = new ArrayList<>();

    @InjectView(R.id.newsfeed_moving_cover_view1)
    View newsfeed_moving_cover_view1;
    @InjectView(R.id.newsfeed_moving_cover_view2)
    View newsfeed_moving_cover_view2;
    @InjectView(R.id.newsfeed_moving_cover_view3)
    View newsfeed_moving_cover_view3;

    int newsfeed_item_hight = 0;
    public boolean isShowing = false;
    public int viewPagerScrollState = ViewPager.SCROLL_STATE_IDLE;

    private RequestAgentSearch requestAgentSearch;


    public static ChatLobbyFragment newInstance(String message, InterceptableRelativeLayout mInterceptableRelativeLayout, ViewPager pager, NewsFeedDetailFragment newsFeedDetailFragment, NewsFeedProfileFragment newsFeedProfileFragment) {
        ChatLobbyFragment chatLobbyFragment = new ChatLobbyFragment();
        chatLobbyFragment.setNewsFeedDetailFragment(newsFeedDetailFragment);
        chatLobbyFragment.setNewsFeedProfileFragment(newsFeedProfileFragment);
        chatLobbyFragment.setInterceptableRelativeLayout(mInterceptableRelativeLayout);
        chatLobbyFragment.setViewPager(pager);
        Bundle args = new Bundle();
        chatLobbyFragment.setArguments(args);
        return chatLobbyFragment;

    }

    void setNewsFeedDetailFragment(NewsFeedDetailFragment newsFeedDetailFragment) {
        this.newsFeedDetailFragment = newsFeedDetailFragment;
    }

    void setNewsFeedProfileFragment(NewsFeedProfileFragment newsFeedProfileFragment) {
        this.newsFeedProfileFragment = newsFeedProfileFragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
        chatLobbyFragment = this;

        databaseManager = DatabaseManager.getInstance(getActivity());
        userContent = new Gson().fromJson(databaseManager.getLogInfo().getLogInfo(),
                ResponseLoginSocial.Content.class);


        if (savedInstanceState != null) {
            mCurrentSelectedPosition = savedInstanceState
                    .getInt(STATE_SELECTED_POSITION);
            mFromSavedInstanceState = true;
        }


        getDialogsAndAgentProfileList();

        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mAgentProfileDidUpdateReceiver,
                new IntentFilter(Constants.AGENT_PROFILE_DID_UPDATE));
    }

    private BroadcastReceiver mAgentProfileDidUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            Log.d(TAG, Constants.AGENT_PROFILE_DID_UPDATE);
        }
    };

    void setInterceptableRelativeLayout(InterceptableRelativeLayout view) {
        mInterceptableRelativeLayout = view;
    }

    void setViewPager(ViewPager pager) {
        this.pager = pager;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mainView = inflater.inflate(R.layout.fragment_chat_lobby, container,
                false);
        ButterKnife.inject(this, mainView);

        ChatLobbyDbViewController.initChatLobbyDbViewController(getActivity());
        buildListView(databaseManager.listQBChatDialog());

        mainView.setPadding(0, MainActivityTabBase.tabHeight, 0, 0);

        fragmentIsRunning = true;

        return mainView;

    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //init title bar
        mTitlebar = ((MainActivityTabBase) getActivity()).getTitleBar();
        chatLobbyListView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this.getActivity());
        chatLobbyListView.setLayoutManager(mLayoutManager);


        //attach listener
        ((MainActivityTabBase) getActivity()).chatlobbyUpdateListener = this;

        addListener();

    }

    private void addListener() {
        //add invite to chat btn click event
        mTitlebar.getInviteToChatBtn().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //invite to chat

                // Mixpanel
                MixpanelAPI mixpanel = MixpanelAPI.getInstance(getContext(), AppConfig.getMixpanelToken());
                mixpanel.track("View Invite to Chat @ Chat");

                InviteToChatActivity.start(getActivity());
            }
        });

        //add delete btn click event
        mTitlebar.getEditChatLobbyBtn().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //delete action
                chatLobbyAdapter.setIsDelMode(!chatLobbyAdapter.isDelMode());

            }
        });

        // agent search event
        mTitlebar.getAgentSearchBtn().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //agent search feature
                createSearchingModeLayout(searchingModeText);
            }
        });

        mTitlebar.getSearchAgentEditText().addTextChangedListener(new SearchWatcher("chat"));


        chatLobbyListView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dx == 0 && dy == 0) {
                    return;
                }
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    //Pagination applies to agent search ONLY
                    if (mTitlebar.getAgentSearchBtn().getVisibility() == View.GONE) {
                        if (!hasPageEnd) {
                            if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                                if (requestAgentSearch == null) {
                                    PAGE_NO++;
                                    Log.v("...", "Last Item Wow !" + PAGE_NO);
//                            Log.v("...", "Last Item Wow !" + getCurrentSection().name());
                                    //Do pagination.. i.e. fetch new data
                                    filterApiAgentSearch(mTitlebar.getSearchAgentEditText().getText().toString());
                                }

                            }
                        }
                    } else {
                        chatLobbyAdapter.showLoader(false);
                    }
                }
//                updateNormalViewListDisplay();
            }
        });

    }

    public void filterLocalAgentSearch(final String filterText) {
        if(!fragmentIsRunning){
            return;
        }
        searchingModeText = filterText;
        Log.v("", "edwinxx filterText " + filterText);
        if (filterText.isEmpty()) {
            chatLobbyAdapter.showFooter(false);
            chatLobbyAdapter.showLoader(false);
            final List<DBQBChatDialog> dbqbChatDialogList = DatabaseManager.getInstance(ChatLobbyFragment.this.getActivity()).listQBChatDialog();
            if(!fragmentIsRunning){
                return;
            }
            ChatLobbyFragment.this.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(chatLobbyAdapter!=null) {
                        chatLobbyAdapter.setFilterText("");
                        chatLobbyAdapter.setLocalSearchDialogs(dbqbChatDialogList);
                    }
                }
            });

        } else {

            List<DBQBUser> dbqbUserList = DatabaseManager.getInstance(getActivity()).listQBUserByName(filterText);
            searchDialogList = new ArrayList<>();
            for (DBQBUser dbqbUser : dbqbUserList) {
                searchDialogList.add(DatabaseManager.getInstance(getActivity()).getQBChatDialogByQbId(dbqbUser.getUserID()));
            }
            if(!fragmentIsRunning){
                return;
            }
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(chatLobbyAdapter!=null) {
                        chatLobbyAdapter.setFilterText(filterText);
                        chatLobbyAdapter.setLocalSearchDialogs(searchDialogList);
                    }
                }
            });

        }

    }

    List<AgentProfiles> api_agentProfiles = new ArrayList<AgentProfiles>();

    public void filterApiAgentSearch(String filterText) {
        requestAgentSearch = new RequestAgentSearch(userContent.memId,
                LocalStorageHelper.getAccessToken(getContext()),
                PAGE_NO,
                filterText);
        requestAgentSearch.callAgentSearchApi(getActivity(), new ApiCallback() {
            @Override
            public void success(ApiResponse apiResponse) {
                try {
                    TitleBar mTitleBar = ((MainActivityTabBase) getActivity()).getTitleBar();
                    Log.w(TAG, "getChatSearchLinearLayout: " + mTitleBar.getChatSearchLinearLayout().getVisibility());
                    Log.w(TAG, "edwinxxc getChatSearchLinearLayout: " + mTitleBar.getEtSearchField().getText().toString());

                    requestAgentSearch = null;
                    if (mTitleBar.getSearchAgentEditText().getText().toString().isEmpty()) {
                        return;
                    }
                    if (mTitleBar.getChatSearchLinearLayout().getVisibility() == View.GONE)
                        return;
                    ResponseNewsFeed responseGson = new Gson().fromJson(apiResponse.jsonContent, ResponseNewsFeed.class);
                    if (PAGE_NO == 1) {
                        api_agentProfiles.clear();
                    }
                    api_agentProfiles.addAll(responseGson.content.agentProfiles);
                    List<AgentProfiles> temp_agentProfiles = new ArrayList<AgentProfiles>();
                    temp_agentProfiles.addAll(api_agentProfiles);
                    filterApiAgent(searchDialogList, temp_agentProfiles);
                    Log.w(TAG, "getChatSearchLinearLayout: " + searchDialogList.size() + " " + temp_agentProfiles.size());
                    chatLobbyAdapter.setApiSearchDialogs(temp_agentProfiles);
                    //to trigger the loader
                    int resultCount = responseGson.content.totalCount;
                    //cater for few search results showing loader
                    hasPageEnd = (resultCount == 0 || resultCount < 20);
                    chatLobbyAdapter.showLoader(!hasPageEnd);
                    Log.w(TAG, "responseGson = " + responseGson.content.toString());
                } catch (Exception e) {
                    requestAgentSearch = null;
                }
            }

            @Override
            public void failure(String errorMsg) {
                Log.w(TAG, "errorMsg = " + errorMsg);
                requestAgentSearch = null;
            }
        });
    }

    void filterApiAgent(List<DBQBChatDialog> mysearchDialogList, List<AgentProfiles> mytemp_agentProfiles) {
        for (AgentProfiles mAgentProfiles : api_agentProfiles) {
            for (int i = 0; i < mysearchDialogList.size(); i++) {
                if (mAgentProfiles.qBID.equals(String.valueOf(mysearchDialogList.get(i).getQbUserID()))) {
                    mytemp_agentProfiles.remove(mAgentProfiles);
                    break;
                }
            }
        }
    }

    private void createSearchingModeLayout(String searchingText) {
        mTitlebar.getTvTitle().setVisibility(View.GONE);
        mTitlebar.getChatSearchLinearLayout().setVisibility(View.VISIBLE);
        mTitlebar.getAgentSearchBtn().setVisibility(View.GONE);
        mTitlebar.getSearchAgentEditText().setText(searchingText);
        mTitlebar.getSearchAgentEditText().requestFocus();
        mTitlebar.getAgentSearchCancelBtn().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtil.hideKeyboard(context, mTitlebar.getSearchAgentEditText());
                filterLocalAgentSearch("");
                mTitlebar.getSearchAgentEditText().setText("");
                mTitlebar.getChatSearchLinearLayout().setVisibility(View.GONE);
                mTitlebar.getAgentSearchBtn().setVisibility(View.VISIBLE);
                mTitlebar.getTvTitle().setVisibility(View.VISIBLE);
            }
        });
        AppUtil.showKeyboard(context, mTitlebar.getSearchAgentEditText());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mTitlebar.getSearchAgentEditText().setShowSoftInputOnFocus(true);
        }
    }

    @Override
    public void refreshLobby() {
        Log.w(TAG, "refreshLobby ");
        notifyDataSetChanged();
    }


    /**
     * Responsible for handling changes in search edit text.
     */
    private class SearchWatcher implements TextWatcher {
        SearchWatcher(String type) {
            this.type = type;
        }

        private String type;
        private String s;
        private long after;
        private Thread t;
        private Runnable runnable_EditTextWatcher = new Runnable() {
            @Override
            public void run() {
                while (true) {
                    if(!fragmentIsRunning){
                        break;
                    }
                    if (type.equalsIgnoreCase("chat")) {
                        if ((System.currentTimeMillis() - after) > 300 && mTitlebar.getChatSearchLinearLayout().getVisibility() == View.VISIBLE) {
//                            filterLocalAgentSearch(mTitlebar.getSearchAgentEditText().getText().toString());
                            Log.d("Debug_EditTEXT_watcher", "(System.currentTimeMillis()-after)>600 ->  " + (System.currentTimeMillis() - after) + " > " + s);
                            // get suggest places list
                            switch (type) {
                                case "chat":
                                    if (mTitlebar.getSearchAgentEditText().getText().length() > 1) {
                                        PAGE_NO = 1;
                                        hasPageEnd = false;
                                        filterApiAgentSearch(mTitlebar.getSearchAgentEditText().getText().toString());
                                    }
                                    break;
                            }

                            t = null;
                            break;
                        }
                    }

                }
            }
        };

        @Override
        public void beforeTextChanged(CharSequence c, int i, int i2, int i3) {

        }

        @Override
        public void onTextChanged(CharSequence c, int i, int i2, int i3) {
            s = c.toString();
        }

        @Override
        public void afterTextChanged(final Editable editable) {
            if (editable.toString().trim().length()>0) {
                filterLocalAgentSearch(mTitlebar.getSearchAgentEditText().getText().toString());
                after = System.currentTimeMillis();
                if (t == null) {
                    t = new Thread(runnable_EditTextWatcher);
                    t.start();
                }
            }
            chatLobbyAdapter.showFooter(editable.toString().trim().length() > 0);

        }
    }

    @Override
    public void onStartSessionRecreation() {

    }

    @Override
    public void onFinishSessionRecreation(final boolean success) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (success) {
                    getDialogsAndAgentProfileList();
                }
            }
        });
    }


    private void getDialogsAndAgentProfileList() {

        //UnUse?
//        if (DatabaseManager.getInstance(getActivity()).listQBChatDialog() == null ||
//                DatabaseManager.getInstance(getActivity()).listQBChatDialog().size() == 0) {
////            if (chatLobbyProgressBar != null)
////                chatLobbyProgressBar.setVisibility(View.VISIBLE);
//        }

        getDialogsFromQBAndGetAgentProfileListAndChatSetting();

    }

    private void getChatSettingAndDbAction(final ArrayList<QBDialog> dialogs, final String currentQbId, final List<AgentProfiles> userProfileList) {
        ResponseLoginSocial.Content userContent = new Gson().fromJson(DatabaseManager.getInstance(getActivity()).getLogInfo().getLogInfo(),
                ResponseLoginSocial.Content.class);
        RequestChatRoomGet chatRoomGet = new RequestChatRoomGet(userContent.memId, LocalStorageHelper.getAccessToken(getContext()));
        chatRoomGet.callChatRoomGetApi(getActivity(), new ApiCallback() {
            @Override
            public void success(ApiResponse apiResponse) {
                ResponseChatRoomGet responseChatRoomGet = new Gson().fromJson(apiResponse.jsonContent, ResponseChatRoomGet.class);
                ChatLobbyDbViewController.getInstance(getActivity()).updateDialogDb(dialogs, userProfileList, currentQbId, responseChatRoomGet);
                updateDialogView(databaseManager.listQBChatDialog());
            }

            @Override
            public void failure(String errorMsg) {
                if (Constants.isDevelopment)
                    Toast.makeText(getActivity(), "getChatSettingAndDbAction error: " + errorMsg, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getUserProfileListAndChatSetting(final ArrayList<QBDialog> dialogs) {
        final List<String> qbIds = new ArrayList<>();

        ResponseLoginSocial.Content userContent = new Gson().fromJson(DatabaseManager.getInstance(chatLobbyFragment.getActivity()).getLogInfo().getLogInfo(),
                ResponseLoginSocial.Content.class);
        final String currentQbId = userContent.qbid;
        for (int i = 0; i < dialogs.size(); i++) {
            qbIds.add(String.valueOf(QBUtil.getOpponentIDForPrivateDialog(dialogs.get(i), currentQbId)));
        }


        RequestAgentProfileGetListEx request = new RequestAgentProfileGetListEx(userContent.memId, LocalStorageHelper.getAccessToken(getContext()), qbIds);
        request.callAgentProfileGetListExApi(context, new ApiCallback() {
            @Override
            public void success(ApiResponse apiResponse) {
                ResponseAgentProfileListEx agentProfileListResponseEx = new Gson()
                        .fromJson(apiResponse.jsonContent,
                                ResponseAgentProfileListEx.class);

                if (agentProfileListResponseEx.errorCode == 0) {
                    List<AgentProfiles> agentProfileList = agentProfileListResponseEx.content.profiles;
                    //update profiles
                    getChatSettingAndDbAction(dialogs, currentQbId, agentProfileList);
                }
            }

            @Override
            public void failure(String errorMsg) {
                if (Constants.isDevelopment)
                    Toast.makeText(getActivity(),
                            "callAgentProfileGetListExApi: " + errorMsg,
                            Toast.LENGTH_LONG).show();
            }
        });

    }

    private void getDialogsFromQBAndGetAgentProfileListAndChatSetting() {
        chatLobbyFragment.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ChatService.initIfNeed(getActivity());
                ChatService.getInstance().getAllDialogs(new BaseService.BaseServiceCallBack() {
                    @Override
                    public void onSuccess(Object var1) {
                        updateDialogView(databaseManager.listQBChatDialog());
                        Log.e("GetDialogs", "getDialogs success");
                    }

                    @Override
                    public void onError(BaseService.RealServiceError serviceError) {
                        Log.e("GetDialogs", "getDialogs error");
                        if (chatLobbyProgressBar != null) {
                            chatLobbyProgressBar.setVisibility(View.GONE);
                        }
                        if (Constants.isDevelopment) {
                            Toast.makeText(getActivity(), "getDialogs errors: " + serviceError, Toast.LENGTH_LONG).show();
                        }
                    }
                });
//                ChatService.getInstance().getDialogs(new QBEntityCallbackImpl() {
//                    @Override
//                    public void onSuccess(Object object, Bundle bundle) {
//                        final ArrayList<QBDialog> dialogs = (ArrayList<QBDialog>) object;
//
//                        if (chatLobbyProgressBar != null) {
//                            chatLobbyProgressBar.setVisibility(View.GONE);
//                        }
//                        getUserProfileListAndChatSetting(dialogs);
//                    }
//
//                    @Override
//                    public void onError(List errors) {
//                        if (chatLobbyProgressBar != null) {
//                            chatLobbyProgressBar.setVisibility(View.GONE);
//                        }
//                        Toast.makeText(getActivity(), "getDialogs errors: " + errors, Toast.LENGTH_LONG).show();
//                    }
//                });
            }
        });
    }

    void buildListView(List<DBQBChatDialog> dialogs) {

        chatLobbyAdapter = new ChatLobbyAdapter(getActivity());
        chatLobbyAdapter.showLoader(false);
        chatLobbyAdapter.showFooter(false);
        chatLobbyAdapter.setLocalSearchDialogs(dialogs);

        if (chatLobbyListView != null) {
            chatLobbyListView.setAdapter(chatLobbyAdapter);


            final ViewTreeObserver vto = chatLobbyListView.getViewTreeObserver();
            vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                public void onGlobalLayout() {
                    if (mLayoutManager != null && mLayoutManager.getChildAt(0) != null) {
                        newsfeed_item_hight = mLayoutManager.getChildAt(0).getHeight();

//                    int[] locations = new int[2];
//                    mRecyclerView.getTop().getLocationInWindow(locations);
                        if (vto.isAlive()) {
                            // Unregister the listener to only call scrollToPosition once
                            vto.removeGlobalOnLayoutListener(this);
                            // Use vto.removeOnGlobalLayoutListener(this) on API16+ devices as
                            // removeGlobalOnLayoutListener is deprecated.
                            // They do the same thing, just a rename so your choice.
                        }

                        mInterceptableRelativeLayout.setInterceptableListener(new InterceptableListener() {

                            @Override
                            public boolean onInterceptTouchEvent(MotionEvent motionEvent) {

                                if (isShowing && viewPagerScrollState == ViewPager.SCROLL_STATE_IDLE) {
//                                Log.v("", "edwin mInterceptableRelativeLayout onInterceptTouchEvent " + motionEvent.getAction() + " x " + motionEvent.getX() + " y " + (motionEvent.getY() - MainActivityTabBase.tabHeight));
                                    if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                                        ((EnbleableSwipeViewPager) pager).setSwipeable(false);
                                        View child = chatLobbyListView.findChildViewUnder(motionEvent.getX(), (motionEvent.getY() - MainActivityTabBase.tabHeight));
                                        try {
                                            if (child != null) {
                                                //check item agent profile
                                                int idx = chatLobbyListView.getChildPosition(child);
                                                Log.i("", "edwin idx " + idx);
                                                AgentProfiles agentProfilesItem = chatLobbyAdapter.getSelectedAgentProfile(idx);
                                                Log.w("", "edwin agentProfilesItem " + agentProfilesItem.toString());
                                                Log.e("", "edwin agentProfilesItem.memberType " + agentProfilesItem.memberType + " " + (agentProfilesItem.agentListing == null));
                                                if (agentProfilesItem.memberType != 2 || agentProfilesItem.agentListing == null) {
                                                    return false;
                                                }

                                                ((EnbleableSwipeViewPager) pager).setSwipeable(true);
//                                            Log.d("", "edwin cover onInterceptTouchEvent idx " + idx + " " + (viewLocation[1]) + " " + coverLocation[1] + " " + MainActivityTabBase.tabHeight + " " + MainActivityTabBase.titleHeight + " " + mRecyclerView.getTop() + " " + newsfeed_item_hight + " " + child.getTop());
                                                int child_Top = child.getTop();

                                                newsfeed_moving_cover_view1.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (child_Top < 0 ? 0 : child_Top)));
                                                newsfeed_moving_cover_view2.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, newsfeed_item_hight + (child_Top > 0 ? 0 : child_Top)));

                                                String imageName = AppUtil.getPropertyTypeImageName(agentProfilesItem.agentListing.propertyType, agentProfilesItem.agentListing.spaceType);
                                                String propertyTypeName = AppUtil.getPropertyTypeHashMap(getActivity(), userContent.systemSetting.propertyTypeList).get(agentProfilesItem.agentListing.propertyType + "," + agentProfilesItem.agentListing.spaceType);
                                                newsFeedProfileFragment.update(agentProfilesItem, idx);
                                                newsFeedDetailFragment.update(agentProfilesItem, idx, imageName, propertyTypeName);

                                            }
                                        } catch (Exception e) {

                                        }

                                    }
                                }
                                return false;
                            }

                            @Override
                            public boolean onTouchEvent(MotionEvent motionEvent) {

//                            Log.v("","edwin mInterceptableRelativeLayout onTouchEvent "+motionEvent.getAction());
                                if (motionEvent.getAction() == MotionEvent.ACTION_UP || motionEvent.getAction() == MotionEvent.ACTION_CANCEL) {
                                    View child = chatLobbyListView.findChildViewUnder(motionEvent.getX(), motionEvent.getY());

                                    try {
                                        child.setAlpha(1f);
                                    } catch (Exception e) {

                                    }
                                    ((EnbleableSwipeViewPager) pager).setSwipeable(false);
                                }
                                return false;
                            }
                        });

                    }
                }
            });

        }
    }

//    void rebuildListView(List<DBQBChatDialog> dialogs) {
////        chatLobbyAdapter.resetDialogs(dialogs);
////        chatLobbyAdapter.setLocalSearchDialogs(dialogs);
//        filterLocalAgentSearch(mTitlebar.getSearchAgentEditText().getText().toString());
//    }

    void rebuildListView() {
        filterLocalAgentSearch(mTitlebar.getSearchAgentEditText().getText().toString());
    }

    public void setCoverViewAlpha(float alpha) {
        if (newsfeed_moving_cover_view1 != null) {
            newsfeed_moving_cover_view1.setAlpha(alpha);
            newsfeed_moving_cover_view3.setAlpha(alpha);
        }
    }

    public void notifyDataSetChanged() {
        if (chatLobbyAdapter != null) {
            chatLobbyAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
        fragmentIsRunning = false;
        context = null;
        ChatLobbyDbViewController.clear();
        chatLobbyFragment = null;
    }

    public boolean isDrawerOpen() {
        return mDrawerLayout != null
                && mDrawerLayout.isDrawerOpen(mFragmentContainerView);
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            fragmentViewChangeListener = (FragmentViewChangeListener) activity;
        } catch (ClassCastException e) {
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        fragmentViewChangeListener = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_SELECTED_POSITION, mCurrentSelectedPosition);
    }

    private ActionBar getActionBar() {
        return ((ActionBarActivity) getActivity()).getSupportActionBar();
    }

    /**
     * Callbacks interface that all activities using this fragment must
     * implement.
     */

    public int getDrawerWidth() {
        return mainView.getWidth();
    }


    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onResume() {
        super.onResume();
        Log.v("", "edwinxx onResume ");
        updateLobbyListView();

    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {

        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mAgentProfileDidUpdateReceiver);

        super.onDestroy();
        fragmentIsRunning = false;
        searchingModeText = "";
    }

    private void toChatActivity(int position, Object object) {
        String dialogId = null;
        String opponentID = null;
        Integer memId = null;
        int bindingAgentListingId = -1;
        Bundle bundle = new Bundle();
        AgentProfiles mAgentProfiles = null;

        if (object instanceof DBQBChatDialog) {
            // from local
            Gson gson=  new GsonBuilder().setDateFormat("MMM dd, yyyy HH:mm:ss").create();
            QBDialog selectedDialog = gson.fromJson(((DBQBChatDialog) object).getQBChatDialog(), QBDialog.class);
            opponentID = "" + QBUtil.getOpponentIDForPrivateDialog(selectedDialog, "" + databaseManager.getLogInfo().getQbUserId());
            dialogId = selectedDialog.getDialogId();
            memId = databaseManager.getMemberIDByQBUserId(opponentID);

            try {
                DBChatroomAgentProfiles mDBChatroomAgentProfiles = databaseManager.getChatroomAgentProfilesByMemId(memId);
                mAgentProfiles = new Gson().fromJson(mDBChatroomAgentProfiles.getChatroomAgentProfiles(), AgentProfiles.class);
                bindingAgentListingId = mAgentProfiles.agentListing.listingID;
                bundle.putBoolean(ChatActivity.EXTRA_IS_FORM_CHATLOBBY, true);
            } catch (Exception e) {
            }

        } else if (object instanceof AgentProfiles) {
            // from search api
            mAgentProfiles = (AgentProfiles) object;
            opponentID = mAgentProfiles.qBID;
            memId = mAgentProfiles.memberID;

            try {
                bindingAgentListingId = mAgentProfiles.agentListing.listingID;
            } catch (Exception e) {
            }
        }

        bundle.putString(ChatActivity.EXTRA_DIALOG_ID, dialogId);
        bundle.putString(ChatActivity.EXTRA_USER_ID, "" + opponentID);
        bundle.putInt(ChatActivity.EXTRA_MEM_ID, memId);
        bundle.putBoolean(ChatActivity.EXTRA_HAS_HISTORY, true);

        if (bindingAgentListingId != -1) {
            bundle.putInt(ChatActivity.EXTRA_BINDING_AGENT_LISTING_ID, bindingAgentListingId);
        }

        // Open chat activity
        //
        fragmentViewChangeListener.viewMovingEnd();
        CoreData.opponentAgentProfile = mAgentProfiles;
        ChatActivity.start(getActivity(), bundle);
    }

    @Override
    public void reloadDialogDbAndView(String dialogId) {
        if (DatabaseManager.getInstance(getActivity()).getQBChatDialogByDialogId(dialogId) != null) {
            updateLobbyListView();

        } else {
            getDialogsAndAgentProfileList();
        }

    }

    void updateLobbyListView() {
        if (chatLobbyFragment != null) {
            chatLobbyFragment.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    rebuildListView();
                }
            });
        }
    }


    @Override
    public void updateDialogView(List<DBQBChatDialog> dbqbChatDialogs) {
        // build list view
        rebuildListView();
    }


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ChatLobbyAdapter ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private class ChatLobbyAdapter extends HeaderRecyclerViewAdapterV2 {
        private List<Object> dataSource;
        private List<DBQBChatDialog> localSearchDataSource;
        private List<AgentProfiles> apiSearchDataSource;
        private LayoutInflater mInflater;
        private Context context;

        private int swipeItem = -1;
        private Bitmap listViewBitmap;
        //        private boolean isDragging;
        private Gson gson;
        private boolean isDelMode = false;
        private String filterText;
        private int pagerState;

        protected boolean showLoader = false;
        protected boolean showFooter = false;

        List<DBQBChatDialog> temp_dataSource = new ArrayList<DBQBChatDialog>();
        int currentlocalSearchDataSourceSize = 0;

        public ChatLobbyAdapter(Context context) {
            this.context = context;
            this.dataSource = new ArrayList<>();
            mInflater = LayoutInflater.from(context);
            gson = new Gson();
        }

        private List<DBQBChatDialog> addUnreadMsgCountInDbQbChatDialogs(List<DBQBChatDialog> dataSource) {
            for (int i = 0; i < dataSource.size(); i++)
                dataSource.get(i).setUnReadMessageCount(databaseManager.listUnsentReadQBChatMessageByDialogId(dataSource.get(i).getDialogID()).size());

            return dataSource;
        }


        public int getPagerState() {
            return pagerState;
        }

        public void setPagerState(int pagerState) {
            this.pagerState = pagerState;
            notifyDataSetChanged();
        }

        public void showLoader(boolean status) {
            showLoader = status;
        }

        public void showFooter(boolean status) {
                Log.v("showFooter", "showFooter "+status);
            showFooter = status;
        }


        public void setFilterText(String filterText) {
            this.filterText = filterText;
        }

        public String getFilterText() {
            return filterText;
        }


        public void setLocalSearchDialogs(List<DBQBChatDialog> dataSource) {
            Log.v("", "edwinxx setLocalSearchDialogs ");
            temp_dataSource.clear();

            for (DBQBChatDialog mDBQBChatDialog : dataSource) {
                DBQBUser dbqbUser = databaseManager.getQBUserByUserId(mDBQBChatDialog.getQbUserID());
                if (dbqbUser != null && (dbqbUser.getIsHidden() == null || !dbqbUser.getIsHidden())) {
                    temp_dataSource.add(mDBQBChatDialog);
                }
            }
            localSearchDataSource = addUnreadMsgCountInDbQbChatDialogs(temp_dataSource);
            if (this.dataSource != null)
                this.dataSource.clear();
            this.dataSource.addAll(localSearchDataSource);

            List<AgentProfiles> temp_agentProfiles = new ArrayList<AgentProfiles>();


            if (getFilterText() != null && !getFilterText().isEmpty()) {
                Log.v("", "edwinxxx " + getFilterText() + " ");
                temp_agentProfiles.addAll(api_agentProfiles);
                filterApiAgent(searchDialogList, temp_agentProfiles);

                if (temp_agentProfiles != null) {
                    for (AgentProfiles agentProfiles : temp_agentProfiles) {
                        this.dataSource.add(agentProfiles);
                    }
                }
            }
            currentlocalSearchDataSourceSize = localSearchDataSource.size();

            notifyDataSetChanged();

//            localSearchDataSource = addUnreadMsgCountInDbQbChatDialogs(dataSource);
////            for (DBQBChatDialog dbqbChatDialog : localSearchDataSource) {
////                this.dataSource.add(dbqbChatDialog);
////            }
//
//            this.dataSource.addAll(localSearchDataSource);
//            notifyDataSetChanged();
        }

        public void setApiSearchDialogs(List<AgentProfiles> agentProfileList) {
            if (this.dataSource == null)
                return;
            this.dataSource.clear();
            this.apiSearchDataSource = agentProfileList;
            for (DBQBChatDialog dbqbChatDialog : localSearchDataSource) {
                this.dataSource.add(dbqbChatDialog);
            }
            for (AgentProfiles agentProfiles : apiSearchDataSource) {
                this.dataSource.add(agentProfiles);
            }
            notifyDataSetChanged();
        }

        public boolean isDelMode() {
            return isDelMode;
        }

        public void setIsDelMode(boolean isDelMode) {
            this.isDelMode = isDelMode;
            notifyDataSetChanged();
        }


        public List<Object> getDataSource() {
            return dataSource;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }


        public void setSwipeItem(int swipeItem) {
            this.swipeItem = swipeItem;
        }

        public int getSwipeItem() {
            return swipeItem;
        }


        @Override
        public void onBindBasicItemView(RecyclerView.ViewHolder holder, final int position) {
            Log.w(TAG, "getView position = " + position);
            final ViewHolder viewHolder = (ViewHolder) holder;


            /////////////////////inviteToChatLinearLayout/////////////////////
//            if (position == getBasicItemCount() - 1 &&
//                    getFilterText() != null &&
//                    !getFilterText().isEmpty()) {
//                viewHolder.inviteToChatLinearLayout.setVisibility(View.VISIBLE);
//                viewHolder.inviteToChatTextView.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        InviteToChatActivity.start(getActivity());
//                    }
//                });
//            } else {
//            viewHolder.inviteToChatLinearLayout.setVisibility(View.GONE);
//            }

            final Object object = dataSource.get(position);

            viewHolder.itemRootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    toChatActivity(position, object);
                }
            });
            //check  is search from local or from api
            if (object instanceof DBQBChatDialog) {
                final DBQBChatDialog dbqbChatDialog = (DBQBChatDialog) object;
                if (dbqbChatDialog == null)
                    return;
//                final QBDialog dialog = gson.fromJson(dbqbChatDialog.getQBChatDialog(), QBDialog.class);
                final DBQBUser dbqbUser = databaseManager.getQBUserByUserId(dbqbChatDialog.getQbUserID());
                //Todo: get QBUser from Quickblox when it doest not exist in database
                if (dbqbUser == null)
                    return;
                Gson gson=  new GsonBuilder().setDateFormat("MMM dd, yyyy HH:mm:ss").create();
                QBUser qbUser = gson.fromJson(dbqbUser.getQBUser(), QBUser.class);
                final DBChatroomAgentProfiles dbChatroomAgentProfiles = databaseManager.getChatroomAgentProfilesByUserId(dbqbChatDialog.getQbUserID());
                AgentProfiles agentProfile = null;
                if (dbChatroomAgentProfiles != null)
                    agentProfile = gson.fromJson(dbChatroomAgentProfiles.getChatroomAgentProfiles(), AgentProfiles.class);
                final AgentProfiles finalAgentProfile = agentProfile;

                // get opponent name for private dialog
                if (qbUser == null)
                    return;

                viewHolder.editDelBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        setIsDelMode(false);
                        DBQBUser myDbqbUser = dbqbUser;
                        myDbqbUser.setIsHidden(true);
                        DatabaseManager.getInstance(context).insertOrUpdateQBUser(myDbqbUser);
                        setLocalSearchDialogs(DatabaseManager.getInstance(getActivity()).listQBChatDialog());
                    }
                });

                if (isDelMode()) {
                    viewHolder.editDelBtn.setVisibility(View.VISIBLE);
                } else {
                    viewHolder.editDelBtn.setVisibility(View.GONE);
                }

//                /////////////////////separatorLinearLayout/////////////////////
                if (localSearchDataSource.size() > 0 &&
                        position == localSearchDataSource.size() - 1 &&
                        getFilterText() != null &&
                        !getFilterText().isEmpty()) {
                    viewHolder.separatorLinearLayout.setVisibility(View.VISIBLE);
//                    viewHolder.agentSearchTextView.setText(getString(R.string.chat_lobby__other_agents, getFilterText()));
                } else {
                    viewHolder.separatorLinearLayout.setVisibility(View.GONE);
                }


                /////////////////////muteImageView/////////////////////
                if (dbqbChatDialog.getMutedOpponent() == null || dbqbChatDialog.getMutedOpponent() == 0)
                    viewHolder.muteImageView.setVisibility(View.GONE);
                else
                    viewHolder.muteImageView.setVisibility(View.VISIBLE);

                /////////////////////blockImageView/////////////////////
                if (dbqbChatDialog.getBlockedOpponent() == null || dbqbChatDialog.getBlockedOpponent() == 0)
                    viewHolder.blockImageView.setVisibility(View.GONE);
                else
                    viewHolder.blockImageView.setVisibility(View.VISIBLE);


//                if (dbqbChatDialog.getExited() == null || dbqbChatDialog.getExited() == 0)
//                    holder.itemRootView.setAlpha(1);
//                else
//                    holder.itemRootView.setAlpha(0.7f);

                /////////////////////chatImageView/////////////////////
                if (finalAgentProfile != null) {
                    Picasso.with(context).load(finalAgentProfile.agentPhotoURL)
                            .placeholder(R.drawable.default_profile)
                            .into(viewHolder.chatImageView);
                }

                /////////////////////nameTextView/////////////////////
                viewHolder.nameTextView.setText(qbUser.getFullName());

                if (dbqbUser.getIsClear() != null && dbqbUser.getIsClear()) {
                    viewHolder.dateTextView.setVisibility(View.GONE);
                    viewHolder.lastMsgTextView.setVisibility(View.GONE);
                } else {
                    viewHolder.dateTextView.setVisibility(View.VISIBLE);
                    viewHolder.lastMsgTextView.setVisibility(View.VISIBLE);
                    viewHolder.ivPropertyType.setVisibility(View.GONE);

                    /////////////////////dateTextView/////////////////////
                    if (dbqbChatDialog.getLastMessageDate() != null) {
                        String timeString = TimeUtils.getRelativeTimeSpan(Long.parseLong(dbqbChatDialog.getLastMessageDate()) * 1000, System.currentTimeMillis(), getActivity(), false);
                        viewHolder.dateTextView.setText(timeString);
                    }

                    /////////////////////lastMsgTextView/////////////////////
                    if (dbqbChatDialog.getLastMessageText() != null && !dbqbChatDialog.getLastMessageText().equals("image")) {
                        String text = dbqbChatDialog.getLastMessageText();
                        if (dbqbChatDialog.getIsEncrypted() != null && dbqbChatDialog.getIsEncrypted()) {
                            text = CryptLib.decrypt(text);
                        }
                        if (text.equalsIgnoreCase(ChatDbViewController.MSG_TYPE_RATE)) {
                            text = context.getString(R.string.chatroom__rate_message);
                        }
                        viewHolder.lastMsgTextView.setText(text);
                    } else {
                        viewHolder.lastMsgTextView.setText(getString(R.string.sign_up__photo));
                    }
                }

                /////////////////////unreadNoTextView/////////////////////
                if (dbqbChatDialog.getUnReadMessageCount() != null
                        && dbqbChatDialog.getUnReadMessageCount() > 0) {
                    viewHolder.unreadNoTextView.setVisibility(View.VISIBLE);
                    viewHolder.unreadNoTextView.setText("" + dbqbChatDialog.getUnReadMessageCount());
                } else {
                    viewHolder.unreadNoTextView.setVisibility(View.GONE);
                }

                if (dbChatroomAgentProfiles != null) {
                    if (finalAgentProfile.memberType == Constants.MEM_TYPE_CREATED_POST_AGENT) {
                        /////////////////////followLinearLayout/////////////////////
                        viewHolder.btnToFollow.setVisibility(View.VISIBLE);

                        if (dbChatroomAgentProfiles.getIsFollowing() != null) {

                            if (dbChatroomAgentProfiles.getIsFollowing() == 0) {
//                            if (LocalStorageHelper.checkIfAgentIsFollowing(context,dbChatroomAgentProfiles.getMemberID())){
                                viewHolder.chatImageView.setImageBorder(Constants.STYLE_TYPE.AGENT_UNFOLLOWED);
                                viewHolder.btnToFollow.setStyle(Constants.STYLE_TYPE.AGENT_UNFOLLOWED);
                            } else {
                                viewHolder.chatImageView.setImageBorder(Constants.STYLE_TYPE.AGENT_FOLLOWED);
                                viewHolder.btnToFollow.setStyle(Constants.STYLE_TYPE.AGENT_FOLLOWED);
                            }
                            viewHolder.btnToFollow.setIsFollowing(dbChatroomAgentProfiles.getIsFollowing());
                        }


                        viewHolder.btnToFollow.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                final RequestFollowAgent followAgent = new RequestFollowAgent(userContent.memId, LocalStorageHelper.getAccessToken(getContext()));
                                followAgent.followingMemId = dbChatroomAgentProfiles.getMemberID();
                                viewHolder.btnToFollow.playAnimation();
                                if (finalAgentProfile != null &&
                                        finalAgentProfile.agentListing != null)
                                    followAgent.followingListingId = finalAgentProfile.agentListing.listingID;
                                if (dbChatroomAgentProfiles.getIsFollowing() == 1) {
                                    Dialog.unfollowDialog(getActivity(), finalAgentProfile.memberName, new DialogCallback() {
                                        @Override
                                        public void yes() {
                                            followAgent.callUnfollowApi(getActivity(), new ApiCallback() {
                                                @Override
                                                public void success(ApiResponse apiResponse) {

                                                    CurrentFollower currentFollower = new CurrentFollower(userContent.memId, finalAgentProfile.memberID,
                                                            userContent.qbid, finalAgentProfile.qBID, false);
                                                    currentFollower.updateCurrentFollowingList(getActivity());

                                                    dbChatroomAgentProfiles.setIsFollowing(0);
                                                    viewHolder.btnToFollow.setStyle(Constants.STYLE_TYPE.AGENT_UNFOLLOWED);
                                                    viewHolder.chatImageView.setImageBorder(Constants.STYLE_TYPE.AGENT_UNFOLLOWED);
                                                    databaseManager.updateChatroomAgentProfilesIsFollowing(dbChatroomAgentProfiles);
                                                    notifyDataSetChanged();

                                                    //remove agentProfile from local following people list
                                                    LocalStorageHelper.removeAgentProfileListToLocalStorage(context, finalAgentProfile.memberID);
                                                    newsFeedProfileFragment.updateUI(true);
                                                }

                                                @Override
                                                public void failure(String errorMsg) {
                                                    AppUtil.showToast(getActivity(), getString(R.string.error_message__please_try_again_later));
                                                }
                                            });
                                        }
                                    }).show();
                                } else {
                                    followAgent.callFollowApi(getActivity(), new ApiCallback() {
                                        @Override
                                        public void success(ApiResponse apiResponse) {
//                                    getDialogsAndAgentProfileList();
                                            CurrentFollower currentFollower = new CurrentFollower(userContent.memId, finalAgentProfile.memberID,
                                                    userContent.qbid, finalAgentProfile.qBID, true);
                                            currentFollower.updateCurrentFollowingList(getActivity());

                                            dbChatroomAgentProfiles.setIsFollowing(1);
                                            viewHolder.btnToFollow.setStyle(Constants.STYLE_TYPE.AGENT_FOLLOWED);
                                            viewHolder.chatImageView.setImageBorder(Constants.STYLE_TYPE.AGENT_FOLLOWED);
                                            databaseManager.updateChatroomAgentProfilesIsFollowing(dbChatroomAgentProfiles);
                                            notifyDataSetChanged();


                                            //add local following people list
                                            LocalStorageHelper.addAgentProfileListToLocalStorage(context, finalAgentProfile);

                                            newsFeedProfileFragment.updateUI(true);
                                        }

                                        @Override
                                        public void failure(String errorMsg) {
                                            AppUtil.showToast(getActivity(), getString(R.string.error_message__please_try_again_later));
                                        }
                                    });
                                }
                            }
                        });
                    } else {
                        //not agent
                        viewHolder.btnToFollow.setVisibility(View.GONE);
                        if (finalAgentProfile.memberType == Constants.MEM_TYPE_CUSTOMER_SERVICE) {
                            //cs
                            viewHolder.chatImageView.setImageBorder(Constants.STYLE_TYPE.CUSTOMER_SERVICE);
                            viewHolder.btnToFollow.setStyle(Constants.STYLE_TYPE.CUSTOMER_SERVICE);
                            viewHolder.editDelBtn.setVisibility(View.GONE);
                        } else if (finalAgentProfile.memberType == Constants.MEM_TYPE_SUPER) {
                            //robot
                            viewHolder.chatImageView.setImageBorder(Constants.STYLE_TYPE.ROBOT);
                            viewHolder.btnToFollow.setStyle(Constants.STYLE_TYPE.ROBOT);

                            viewHolder.btnToFollow.setVisibility(View.VISIBLE);
                            viewHolder.editDelBtn.setVisibility(View.GONE);
                        } else {
                            // normal user
                            viewHolder.chatImageView.setImageBorder(Constants.STYLE_TYPE.USER);

                        }
                    }
                }
            } else {
                //from api search
                if (object instanceof AgentProfiles) {
                    final AgentProfiles agentProfiles = (AgentProfiles) object;
                    /////////////////////chatImageView/////////////////////
                    Picasso.with(context).load(agentProfiles.agentPhotoURL)
                            .placeholder(R.drawable.default_profile)
                            .into(viewHolder.chatImageView);
                    if (agentProfiles.isFollowing == 1) {
                        viewHolder.chatImageView.setImageBorder(Constants.STYLE_TYPE.AGENT_FOLLOWED);
                        viewHolder.btnToFollow.setStyle(Constants.STYLE_TYPE.AGENT_FOLLOWED);
                    } else {
                        viewHolder.chatImageView.setImageBorder(Constants.STYLE_TYPE.AGENT_UNFOLLOWED);
                        viewHolder.btnToFollow.setStyle(Constants.STYLE_TYPE.AGENT_UNFOLLOWED);
                    }

                    /////////////////////nameTextView/////////////////////
                    viewHolder.nameTextView.setText(agentProfiles.memberName);

                    /////////////////////dateTextView/////////////////////
                    /////////////////////followLinearLayout/////////////////////
                    Log.w(TAG, "dataSource = " + dataSource);
                    if (agentProfiles.agentListing != null &&
                            agentProfiles.agentListing.googleAddresses != null) {

                        //show property type thumbnail
                        int propertyType=agentProfiles.agentListing.propertyType;
                        int spaceType=agentProfiles.agentListing.spaceType;
                        String imageName=AppUtil.getPropertyTypeImageName(propertyType,spaceType);
                        viewHolder.ivPropertyType.setVisibility(View.VISIBLE);
                        viewHolder.ivPropertyType.setImageResource(context.getResources().getIdentifier(imageName+"_mini", "drawable", context.getPackageName()));

                        List<AgentListing.GoogleAddress> googleAddresses = agentProfiles.agentListing.googleAddresses;
                        viewHolder.dateTextView.setText(googleAddresses.get(0).address);
                        if (agentProfiles.isFollowing == 0) {
                            viewHolder.btnToFollow.setStyle(Constants.STYLE_TYPE.AGENT_UNFOLLOWED);
                            viewHolder.chatImageView.setImageBorder(Constants.STYLE_TYPE.AGENT_UNFOLLOWED);
                        } else {
                            viewHolder.btnToFollow.setStyle(Constants.STYLE_TYPE.AGENT_FOLLOWED);
                            viewHolder.chatImageView.setImageBorder(Constants.STYLE_TYPE.AGENT_FOLLOWED);
                        }
                        viewHolder.btnToFollow.setIsFollowing(agentProfiles.isFollowing);

                        viewHolder.btnToFollow.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                viewHolder.btnToFollow.playAnimation();
                                final RequestFollowAgent followAgent = new RequestFollowAgent(userContent.memId, LocalStorageHelper.getAccessToken(getContext()));
                                followAgent.followingMemId = agentProfiles.memberID;
                                followAgent.followingListingId = agentProfiles.agentListing.listingID;
                                if (agentProfiles.isFollowing == 1) {
                                    Dialog.unfollowDialog(getActivity(), agentProfiles.memberName, new DialogCallback() {
                                        @Override
                                        public void yes() {
                                            followAgent.callUnfollowApi(getActivity(), new ApiCallback() {
                                                @Override
                                                public void success(ApiResponse apiResponse) {
                                                    agentProfiles.isFollowing = 0;
                                                    dataSource.set(position, agentProfiles);
                                                    notifyDataSetChanged();

                                                    //remove agentProfile from local following people list
                                                    LocalStorageHelper.removeAgentProfileListToLocalStorage(context, agentProfiles.memberID);
                                                    newsFeedProfileFragment.updateUI(true);
                                                }

                                                @Override
                                                public void failure(String errorMsg) {
                                                    AppUtil.showToast(getActivity(), getString(R.string.error_message__please_try_again_later));
                                                }
                                            });
                                        }
                                    }).show();
                                } else {
                                    followAgent.callFollowApi(getActivity(), new ApiCallback() {
                                        @Override
                                        public void success(ApiResponse apiResponse) {
                                            agentProfiles.isFollowing = 1;
                                            dataSource.set(position, agentProfiles);
                                            notifyDataSetChanged();

                                            //add local following people list
                                            LocalStorageHelper.addAgentProfileListToLocalStorage(context, agentProfiles);
                                            newsFeedProfileFragment.updateUI(true);
                                        }

                                        @Override
                                        public void failure(String errorMsg) {
                                            AppUtil.showToast(getActivity(), getString(R.string.error_message__please_try_again_later));
                                        }
                                    });
                                }
                            }
                        });
                    } else {
                        viewHolder.dateTextView.setVisibility(View.INVISIBLE);
                        viewHolder.btnToFollow.setVisibility(View.GONE);
                    }

                    /////////////////////lastMsgTextView/////////////////////
                    viewHolder.lastMsgTextView.setVisibility(View.GONE);


                    /////////////////////unreadNoTextView/////////////////////
                    viewHolder.unreadNoTextView.setVisibility(View.GONE);

                    /////////////////////separatorLinearLayout/////////////////////
                    viewHolder.separatorLinearLayout.setVisibility(View.GONE);

                    /////////////////////muteImageView/////////////////////
                    viewHolder.muteImageView.setVisibility(View.GONE);

                    /////////////////////blockImageView/////////////////////
                    viewHolder.blockImageView.setVisibility(View.GONE);

                }
            }

        }

        @Override
        public boolean useHeader() {
            return false;
        }

        @Override
        public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent, int viewType) {
            return null;
        }

        @Override
        public void onBindHeaderView(RecyclerView.ViewHolder holder, int position) {

        }

        @Override
        public boolean useFooter() {
            return true;
        }

        @Override
        public RecyclerView.ViewHolder onCreateFooterViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.recycler_view_chats_footer, parent, false);
            Footer_ViewHolder viewHolder = new Footer_ViewHolder(v);
            return viewHolder;
        }

        @Override
        public void onBindFooterView(RecyclerView.ViewHolder holder, int position) {
            // Loader ViewHolder
            Log.d("onBindFooterView", "onBindFooterView " + holder.toString() + " " + position + showLoader);
            if (holder instanceof Footer_ViewHolder) {
                Footer_ViewHolder loaderViewHolder = (Footer_ViewHolder) holder;
                if (showLoader) {
                    loaderViewHolder.progressSpinner.setVisibility(View.VISIBLE);
                } else {
                    loaderViewHolder.progressSpinner.setVisibility(View.GONE);
                }

                if (showFooter) {
                    loaderViewHolder.inviteToChatLinearLayout.setVisibility(View.VISIBLE);
                    loaderViewHolder.inviteToChatTextView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            InviteToChatActivity.start(getActivity());
                        }
                    });
                } else {
                    loaderViewHolder.inviteToChatLinearLayout.setVisibility(View.GONE);
                }

                return;
            }
        }

        @Override
        public RecyclerView.ViewHolder onCreateBasicItemViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.chat_lobby_list_item, parent, false);
            ViewHolder viewHolder = new ViewHolder(v);
            return viewHolder;
        }

        @Override
        public int getBasicItemCount() {
            return dataSource.size();
        }

        @Override
        public int getBasicItemType(int position) {
            return 0;
        }

        public AgentProfiles getSelectedAgentProfile(int index) {
            AgentProfiles agentProfiles;
            Object data = dataSource.get(index);
            if (data instanceof DBQBChatDialog) {
                DBChatroomAgentProfiles dbChatroomAgentProfiles = DatabaseManager.getInstance(getActivity()).getChatroomAgentProfilesByUserId(((DBQBChatDialog) data).getQbUserID());
                if (dbChatroomAgentProfiles == null)
                    return null;
                agentProfiles = gson.fromJson(dbChatroomAgentProfiles.getChatroomAgentProfiles(), AgentProfiles.class);
                agentProfiles.setIsFollowing(dbChatroomAgentProfiles.getIsFollowing());
                return agentProfiles;
            } else {
                agentProfiles = (AgentProfiles) data;
                agentProfiles.memberType = Constants.MEM_TYPE_CREATED_POST_AGENT;
                return agentProfiles;
            }
        }


    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @InjectView(R.id.itemRootView)
        View itemRootView;
        @InjectView(R.id.btn_to_follow)
        FollowButton btnToFollow;
        @InjectView(R.id.chatImageView)
        RoundedImageView chatImageView;
        @InjectView(R.id.nameTextView)
        TextView nameTextView;
        @InjectView(R.id.lastMsgTextView)
        TextView lastMsgTextView;
        @InjectView(R.id.unreadNoTextView)
        TextView unreadNoTextView;
        @InjectView(R.id.dateTextView)
        TextView dateTextView;
        @InjectView(R.id.editDelBtn)
        ImageButton editDelBtn;
        @InjectView(R.id.muteImageView)
        ImageView muteImageView;
        @InjectView(R.id.blockImageView)
        ImageView blockImageView;
        @InjectView(R.id.separatorLinearLayout)
        LinearLayout separatorLinearLayout;
        @InjectView(R.id.agentSearchTextView)
        TextView agentSearchTextView;
        @InjectView(R.id.iv_propertyType)
        ImageView ivPropertyType;


        public ViewHolder(View view) {
            super(view);
            ButterKnife.inject(this, view);
        }
    }

    class Footer_ViewHolder extends RecyclerView.ViewHolder {

        ProgressBar progressSpinner;
        LinearLayout inviteToChatLinearLayout;
        TextView inviteToChatTextView;

        public Footer_ViewHolder(View itemView) {
            super(itemView);
            progressSpinner = (ProgressBar) itemView.findViewById(R.id.progress_spinner);
            inviteToChatLinearLayout = (LinearLayout) itemView.findViewById(R.id.inviteToChatLinearLayout);
            inviteToChatTextView = (TextView) itemView.findViewById(R.id.inviteToChatTextView);
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ChatLobbyAdapter ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
