package co.real.productionreal2.service.model.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by kelvinsun on 19/2/16.
 */
public class ResponseFollowInfo extends ResponseBase{
    @SerializedName("Content")
    public Content content;

    public class Content {
        @SerializedName("FollowerCount")
        public int followCount;
        @SerializedName("FollowingCount")
        public int followingCount;

        @Override
        public String toString() {
            return "Content{" +
                    "FollowerCount=" + followCount +
                    ", followingCount=" + followingCount +
                    '}';
        }
    }
}
