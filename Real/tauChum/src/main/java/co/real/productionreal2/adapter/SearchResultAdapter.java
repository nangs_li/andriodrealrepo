package co.real.productionreal2.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import co.real.productionreal2.R;
import co.real.productionreal2.model.GooglePlace;
import co.real.productionreal2.model.SystemSetting;
import co.real.productionreal2.service.model.request.RequestAddressSearch;

import java.util.ArrayList;

/**
 * Created by kelvinsun on 30/12/15.
 */
public class SearchResultAdapter extends ArrayAdapter<Object> {

    private Context context;
    protected SearchResultAdapterListener mListener;
    private ArrayList<Object> topFiveCities = new ArrayList<>();

    public static interface SearchResultAdapterListener {
        public void selectPlace(Object mPlace);
    }

    public SearchResultAdapter(Context context, ArrayList<Object> topFiveCities, SearchResultAdapterListener mListener) {
        super(context, 0, topFiveCities);
        this.context = context;
        this.mListener = mListener;
        this.topFiveCities = topFiveCities;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        final Object place = getItem(getCount() - position - 1);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.view_search_result_row, parent, false);
        }
        // Lookup view for data population
        TextView tvAddressName = (TextView) convertView.findViewById(R.id.tv_address_name);
        TextView tvNumOfAgent = (TextView) convertView.findViewById(R.id.tv_num_of_agent);
        // Populate the data into the template view using the data object
        if (place instanceof SystemSetting.TopFiveCities) {
            tvAddressName.setText(((SystemSetting.TopFiveCities) place).cityName);
            int count = ((SystemSetting.TopFiveCities) place).agentCount;
            String resultCountStr;
            if (count > 1000)
                resultCountStr = "1000+ " + context.getString(R.string.common__agents);
            else
                resultCountStr = ((SystemSetting.TopFiveCities) place).agentCount + " " + context.getString(R.string.common__agents);
            tvNumOfAgent.setText(resultCountStr);
            tvNumOfAgent.setVisibility(View.VISIBLE);
        } else if (place instanceof RequestAddressSearch) {
            tvAddressName.setText(((RequestAddressSearch) place).inputAddress);
            tvNumOfAgent.setText("");
            tvNumOfAgent.setVisibility(View.GONE);
        } else if (place instanceof GooglePlace) {
            tvAddressName.setText(((GooglePlace) place).description);
            tvNumOfAgent.setText("");
            tvNumOfAgent.setVisibility(View.GONE);
        }

        // Return the completed view to render on screen
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.selectPlace(place);
            }
        });

        return convertView;
    }

    @Override
    public int getCount() {
        return topFiveCities.size();
    }
}