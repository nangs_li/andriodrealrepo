package co.real.productionreal2.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import co.real.productionreal2.R;
import co.real.productionreal2.model.MenuEdit;

import java.util.ArrayList;

/**
 * Created by hohojo on 20/11/2015.
 */
public class MenuSpinnerAdapter extends BaseAdapter {
    private ArrayList<MenuEdit> editList;
    private Context context;

    public MenuSpinnerAdapter(Context context, ArrayList<MenuEdit> editList) {
        super();
        this.context = context;
        this.editList = editList;
    }

    public void updateMenuEditList(ArrayList<MenuEdit> editList) {
        editList.clear();
        this.editList = editList;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return editList.size();
    }

    @Override
    public Object getItem(int position) {
        return editList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        LayoutInflater vi = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = vi.inflate(R.layout.edit_spinner_item, null);
            holder = createViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.editImageView.setImageResource(editList.get(position).editImageRes);
        holder.editTextView.setText(editList.get(position).editString);
        return convertView;
    }

    private ViewHolder createViewHolder(View v) {
        ViewHolder holder = new ViewHolder();
        holder.editImageView = (ImageView) v.findViewById(R.id.editImageView);
        holder.editTextView = (TextView) v.findViewById(R.id.editTextView);
        return holder;
    }

    private static class ViewHolder {
        public ImageView editImageView;
        public TextView editTextView;
    }


}
