package co.real.productionreal2;

import com.quickblox.messages.model.QBEnvironment;

public class Constants {
    public enum STYLE_TYPE {USER, AGENT_FOLLOWED, AGENT_UNFOLLOWED, CUSTOMER_SERVICE, ROBOT, SUCCESS, DEFAULT}

    public static final int chatVersion = 1;
    public static boolean isDevelopment = true;
    public static final boolean enableEncryption = true;

    public static final int SEARCH_TAB = 0;
    public static final int NEWS_FEED_PAGE = 1;
    public static final int CHAT_TAB = 2;
    public static final int ME_TAB = 3;
    public static final int SETTING_TAB = 4;

    public static final int CREATE_POST_SELECT_COUNTRY = 30;
    public static final int CREATE_POST_ADD_LANG = 55;

    public static final int ME_VIEW_PROFILE = 40;
    public static final int ME_HOME = 41;
    public static final int ME_PREVIEW = 42;
    public static final int ME_TO_CREATE_POST = 43;
    public static final int ME_BE_AN_AGENT = 44;
    public static final int CHANGE_LANG = 45;

    public static final int SEARCH_FILTER = 0;
    public static final int INPUT_PHONE_NUMBER_REQUSTCODE = 1001;


    public static final String APP_FOLDER_NAME = "Real";
    public static final String APP_DOC_FOLDER_NAME = "Real_doc";
    public static final String SENT_FOLDER_NAME = "sent";
    public static final String RECEIVE_FOLDER_NAME = "receive";
    public static final String COVER_IMAGE_FOLDER_NAME = "cover_image";
    public static final String UPLOAD_FILE_FOLDER_NAME = "upload_file";
    public static final String UPLOAD_CONTACT_LIST_FOLER_NAME = "contact_list";
    public static final String UPLOAD_CONTACT_LIST_FILE_NAME = "real_contact_list.txt";
    public static final String UPLOAD_INVITED_LIST_FOLDER_NAME = "invited_list";
    public static final String UPLOAD_INVITED_LIST_FILE_NAME = "invited_list.txt";

    public static final int MEM_TYPE_USER = 1;
    public static final int MEM_TYPE_CREATED_POST_AGENT = 2;
    public static final int MEM_TYPE_CUSTOMER_SERVICE = 4;
    public static final int MEM_TYPE_SUPER = 5;


    public static final QBEnvironment QB_ENVIROMENT = QBEnvironment.PRODUCTION;
//	public static final QBEnvironment QB_ENVIROMENT = QBEnvironment.PRODUCTION;

    public static final String SET_COOKIE = "Set-Cookie";
    public static final String SET_COOKIE_CSRFTOKEN = "csrftoken";
    public static final String SET_COOKIE_SESSIONID = "sessionid";

    public static final String SHAREDPREF_NAME = "adsfdsafds";
    public static final String SHAREDPREF_SYS_LANG_INDEX = "SHAREDPREF_SYS_LANG_INDEX";
    public static final String PREF_REAL_NETWORK = "real_network";
    public static final String PREF_REAL_NETWORK_JSON = "real_network_json";
    public static final String PREF_BRANCH = "branch";
    public static final String PREF_BRANCH_OBJECT_JSON = "branch_object_json";

    public static final String CHAT_LOBBY_DATE_FORMATE = "yyyy/MM/dd";

    public static final int CHAT_IMAGE_MAX_SIZE = 50;

    public static final String BRANCH_QBID = "member_qbid";
    public static final String BRANCH_TITLE = "$og_title";
    public static final String BRANCH_DESC = "$og_description";

    public static final String BRANCH_CHANNEL = "~channel";
    public static final String BRANCH_CHANNEL_CHAT = "Invite To Chat";
    public static final String BRANCH_CHANNEL_FOLLOW = "Invite To Follow";
    public static final String BRANCH_CHANNEL_DOWNLOAD = "Invite To Download";
    public static final String BRANCH_CHANNEL_EDM = "eDM";

    public static final String BRANCH_FEATURE = "~feature";
    public static final String BRANCH_MEM_ID = "member_id";
    public static final String BRANCH_MEM_NAME = "member_name";
    public static final String BRANCH_MEM_IMAGE_URL = "member_image_url";
    public static final String BRANCH_AGENT_LISTING_ID = "agent_listing_id";

    // eDM only
    public static final String BRANCH_ORIGINAL_EMAIL = "email";
    public static final String BRANCH_COMPANY = "company";

    public static final String BRANCH_ACTION = "action";
    public static final String BRANCH_ACTION_CHAT = "DeepLinkActionChat";
    public static final String BRANCH_ACTION_FOLLOW = "DeepLinkActionFollow";
    public static final String BRANCH_ACTION_NONE = "DeepLinkActionNone";


    public static final String EXTRA_QBID = "qbid";
    public static final String EXTRA_SELECT_TAB = "selectTab";
    public static final String EXTRA_QB_USER_ID = "qbUserId";
    public static final String EXTRA_PAGE_NO = "pageNo";
    public static final String EXTRA_DIALOG_ID = "dialogId";
    public static final String EXTRA_PHOTO_NAME = "photoName";
    public static final String EXTRA_SCREEN_SHOT_BYTES = "screenShotBytes";
    public static final String EXTRA_REQUEST_AGENT_EXP = "RequestAgentExp";
    public static final String EXTRA_AGENT_EXP_EDIT_CONTENT = "agentExpEditContent";
    public static final String EXTRA_AGENT_EXP_IS_EDIT = "agentExpIsEdit";
    public static final String EXTRA_AGENT_EXP_EDIT_TAG_NO = "agentExpEditTagNo";
    public static final String EXTRA_SPOKEN_LANG = "spokenLang";
    public static final String EXTRA_AGENT_LANG = "agentLang";
    public static final String EXTRA_UPDATED_AGENT_LANG = "updatedAgentLang";
    public static final String EXTRA_UPDATED_AGENT_LANG_DEL_LIST = "updatedAgentLangDelList";
    public static final String EXTRA_UPDATED_AGENT_LANG_ADD_LIST = "updatedAgentLangAddList";
    public static final String EXTRA_UPDATED_AGENT_LANG_NATIVE_NAME = "updatedAgentLangNativeName";
    public static final String EXTRA_AGENT_SPEC_LIST = "agentSpecList";
    public static final String EXTRA_AGENT_SPEC_DEL_LIST = "agentSpecDelList";
    public static final String EXTRA_AGENT_SPEC_ADD_LIST = "agentSpecAddList";


    public static final String EXTRA_AGENT_PROPERTY_TYPE_LIST = "agenPropertyTypeList";
    public static final String EXTRA_AGENT_PAST_CLOSING = "agenPastClosing";

    public static final String EXTRA_SLOGAN = "slogan";
    public static final String EXTRA_SLOGAN_INDEX = "sloganIndex";
    public static final String EXTRA_LANG_INDEX = "langIndex";

    public static final String EXTRA_IS_ADD_LANG = "isAddLang";
    public static final String EXTRA_SIGN_UP_EMAIL = "signUpEmail";
    public static final String EXTRA_PHONE_REG_ID = "PhoneRegID";
    public static final String EXTRA_PIN_INPUT = "PINInput";
    public static final String EXTRA_CREATE_POST_GOOGLE_ADDRESS_LIST = "googleAddressList";
    public static final String EXTRA_CREATE_POST_GOOGLE_ADDRESS_SELECT_INDEX = "googleAddressSelectIndex";

    public static final String EXTRA_IS_OLD_USER = "isOldUser";
    public static final String EXTRA_COUNTRY_CODE = "CountryCode";
    public static final String EXTRA_PHONE_NUMBER = "PhoneNumber";

    public static final int AGENT_SEARCH_TYPING_TIMER = 300;
    public static final int CHAT_TYPING_TIMER = 1000;
    public static final int CHAT_ONLINE_STATUS_TIMER = 3000; // typing timer
    public static final int INVITE_TO_FOLLOW_TOAST_TIME = 10000;
    public static final int AUTO_PRESENCE_INTERVAL_IN_SECONDS = 30; //kQBPresenceTimerInterval
    public static final int CHAT_ONLINE_STATUS_TOLERANCE = 30000; //kQBCheckOnlineTimePeriod
    public static final int CHAT_GET_OPPONENT_USER_TIMER = 10000; //kQBRecipientTimerInterval
    private static final String onlineStatusDateFormat = "yyyy-MM-dd hh:mm:ss";
    public static final int MAX_SELECT_PHOTO = 10;
    public static final int MAX_SELECT_PHOTO_ON_CHATROOM = 5;

    public static final int SELECT_AGENT_SEARCH = 0;
    public static final int SELECT_NEWS = 1;
    public static final int SELECT_CHAT = 2;
    public static final int SELECT_PROFILE = 3;
    public static final int SELECT_SETTING = 4;

    public static final String chatroomAgentProfiles = "chatroomAgentProfiles";
    public static final String didDeliverMessage = "didDeliverMessage";
    public static final String didReadMessage = "didReadMessage";
    public static final String logInfo = "logInfo";
    public static final String qbChatDialog = "qbChatDialog";
    public static final String qbChatMessage = "qbChatMessage";
    public static final String qbUser = "qbUser";
    public static final String searchAgentProfileArray = "searchAgentProfileArray";

    public static final String[] SLOGAN_ARR = new String[]{
            "Amazing Potential",
            "The Only One",
            "Fire Sale",
            "Super High Yield",
            "Fine Call",
            "Nothing to lose",
            "With good investment potential",
            "The only opportunity in market",
            "Below market price",
            "High return / High yield",
            "Last call offer",
            "Immediate sale"
    };

    public static final int resizeChatImageWidth = 1280;
    public static final int resizeChatImageHeight = 1280;

    public static final int resizeImageWidth = 1024;
    public static final int resizeImageHeight = 1024;

    public static final int resizeImageSize = 200 * 1000;

    public static final int UPLOAD_LISTING_PHOTO = 1;
    public static final int UPLOAD_MEM_PROFILE_PHOTO = 2;
    public static final int UPLOAD_REASON_ID_PHOTO = 3;

    public static final float compressNormalImageWidth = 800f;
    public static final float compressIconImageWidth = 160f;

    public static final String RATE_URL = "https://bnc.lt/rating";

    public static final int AGENT_PROFILE_EXPIRE_TIME = 24 * 60 * 60 * 1 * 1000; // milliseconds
    public static final int AGENT_PROFILE_REFRESH_INTERVAL = 180 * 1000; // milliseconds
    public static final int AGENT_PROFILE_UPDATE_PER_TIME = 5;
    public static final String AGENT_PROFILE_DID_UPDATE = "AgentProfileDidUpdate";
}
