package co.real.productionreal2.activity.setting;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import butterknife.ButterKnife;
import butterknife.InjectView;
import co.real.productionreal2.Event.UpdateResendTimerEvent;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.BaseActivity;
import co.real.productionreal2.fragment.ChangePhoneNoIntroFragment;

//
public class ChangePhoneNumberActivity extends BaseActivity {
    private static final String TAG = "ChangePhoneNumberAc";

    public static final String COUNTRY_CODE_OLD="countryCodeOld";
    public static final String PHONE_NUM_OLD="phoneNumOld";
    public static final String COUNTRY_CODE="countryCode";
    public static final String PHONE_NUM="phoneNum";
    public static final String REG_ID="regId";

    @InjectView(R.id.headerLayout)
    View headerLayout;
    @InjectView(R.id.titleTextView)
    TextView tvTitle;

    CountDownTimer countDownTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_phone_no);

        ButterKnife.inject(this);
        initView();

        Fragment newFragment;
        newFragment = ChangePhoneNoIntroFragment.newInstance();
        showDefaultAgentList(newFragment);

    }

    private void initView() {

    }

    private void showDefaultAgentList(Fragment fragment) {

        // Instantiate a new fragment.
//        Fragment newFragment = FindYourAgentFragment.newInstance(mStackLevel);

        // Add the fragment to the activity, pushing this transaction
        // on to the back stack.
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.fl_main_container, fragment);
        ft.commit();
    }

    public void pushFragment(Fragment fragment,boolean isAllowBackStack) {

        // Add the fragment to the activity, pushing this transaction
        // on to the back stack.
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.animator.fragment_slide_left_enter,
                R.animator.fragment_slide_left_exit,
                R.animator.fragment_slide_right_enter,
                R.animator.fragment_slide_right_exit);
        ft.replace(R.id.fl_main_container, fragment);
        if (isAllowBackStack){
            ft.addToBackStack(fragment.getClass().getName());
        } else{
            getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
        ft.commit();
    }

    public void updateTitle(int titleId){
        //update title
        tvTitle.setText(titleId);
    }

    public void closeActivity(){
        finish();
    }


    @Override
    public void onBackPressed() {

        int backStackoCount = getFragmentManager().getBackStackEntryCount();
        Log.d("onBackPressed", "onBackPressed:" + backStackoCount);
        if (backStackoCount > 0) {
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }


    public void resendCountDown() {
        if (countDownTimer == null) {
            countDownTimer = new CountDownTimer(60000, 100) {
                @Override
                public void onTick(long millisUntilFinished) {
                    long min = millisUntilFinished / 60000;
                    long re = millisUntilFinished % 60000;
                    long sec = re / 1000;
                    EventBus.getDefault().post(new UpdateResendTimerEvent(min, sec));
                }

                @Override
                public void onFinish() {
                    countDownTimer=null;
                }
            };
            countDownTimer.start();
        }
    }

    @Override
    protected void onDestroy() {
        if (countDownTimer!= null) {
            countDownTimer.cancel();
        }
        super.onDestroy();
    }
}
