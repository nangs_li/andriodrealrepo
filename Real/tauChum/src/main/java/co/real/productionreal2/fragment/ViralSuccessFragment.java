package co.real.productionreal2.fragment;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.InjectView;
import co.real.productionreal2.Constants;
import co.real.productionreal2.R;
import co.real.productionreal2.service.model.ActivityLog;
import widget.RoundedImageView;

/**
 * Created by kelvinsun on 24/5/16.
 */
public class ViralSuccessFragment extends DialogFragment {

    ImageView ivMask;
    ImageView ivTick;
    RoundedImageView ivTickBorder;

    int mNum;
    Animation a;
    Handler handler;
    Runnable runnable;

    /**
     * Create a new instance of MyDialogFragment, providing "num"
     * as an argument.
     */
    public static ViralSuccessFragment newInstance(int num) {
        ViralSuccessFragment f = new ViralSuccessFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putInt("num", num);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mNum = getArguments().getInt("num");

        // Pick a style based on the num.
        int style = DialogFragment.STYLE_NO_TITLE;
        int theme = android.R.style.Theme_Holo;
        setStyle(style, theme);
    }


    @Override
    public void onStart() {
        super.onStart();

        Window window = getDialog().getWindow();
        window.setBackgroundDrawableResource(android.R.color.transparent);
        window.getAttributes().windowAnimations = R.style.DialogAnimation;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_success, container, false);

        ivMask = (ImageView) v.findViewById(R.id.iv_mask);
        ivTick = (ImageView) v.findViewById(R.id.iv_success);
        ivTickBorder = (RoundedImageView) v.findViewById(R.id.iv_success_border);
        showSuccessPopup();

        return v;
    }


    public void showLoadingDialog() {

        ivMask.setVisibility(View.VISIBLE);
        ivTick.setVisibility(View.VISIBLE);
        AlphaAnimation alphaAnimation = new AlphaAnimation((float) 0.1, (float) 1.0);
        alphaAnimation.setFillAfter(false);
        alphaAnimation.setDuration(1000);

        Animation a = AnimationUtils.loadAnimation(getActivity(), R.anim.zoom_in_out);
        a.reset();
        a.setDuration(1000);
        a.setFillAfter(false);
        ivTick.clearAnimation();
        ivTick.setAnimation(alphaAnimation);
        ivTick.startAnimation(a);

        //set tick color to green
        animateImageView(ivTick);
        animateImageView(ivTickBorder);

        ivTickBorder.setAnimation(alphaAnimation);
        ivTickBorder.clearAnimation();
        ivTickBorder.startAnimation(alphaAnimation);
        ivTickBorder.setImageBorder(Constants.STYLE_TYPE.SUCCESS);
        ivTickBorder.setVisibility(View.VISIBLE);


    }

    public void animateImageView(final ImageView v) {
        final int green = getResources().getColor(R.color.holo_green);

        final ValueAnimator colorAnim = ObjectAnimator.ofFloat(0f, 1f);
        colorAnim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float mul = (Float) animation.getAnimatedValue();
                int alphaGreen = adjustAlpha(green, mul);
                v.setColorFilter(alphaGreen, PorterDuff.Mode.SRC_ATOP);
                if (mul == 0.0) {
                    v.setColorFilter(null);
                }
            }
        });

        colorAnim.setDuration(1000);
        colorAnim.setRepeatMode(ValueAnimator.REVERSE);
        colorAnim.setRepeatCount(-1);
        colorAnim.start();

    }

    public int adjustAlpha(int color, float factor) {
        int alpha = Math.round(Color.alpha(color) * factor);
        int red = Color.red(color);
        int green = Color.green(color);
        int blue = Color.blue(color);
        return Color.argb(alpha, red, green, blue);
    }

    private void showSuccessPopup() {

        a = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out);
        //show success popup
        showLoadingDialog();

        handler = new Handler();
        runnable=new Runnable() {
            @Override
            public void run() {
                // Do something after 1s = 1000ms
                ivTick.clearAnimation();

                a.reset();
                a.setDuration(500);
                a.setFillAfter(false);
                ivTick.clearAnimation();
                ivTick.startAnimation(a);
                ivTick.setVisibility(View.GONE);

                ivTickBorder.clearAnimation();
                ivTickBorder.startAnimation(a);
                ivTickBorder.setVisibility(View.GONE);

                ivMask.clearAnimation();
                ivMask.startAnimation(a);
                ivMask.setVisibility(View.GONE);


                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        dismiss();
                    }
                }, 1000);
            }
        };
        handler.postDelayed(runnable, 1000);
    }

    @Override
    public void dismiss() {
        super.dismiss();
        handler.removeCallbacksAndMessages(null);
    }
}