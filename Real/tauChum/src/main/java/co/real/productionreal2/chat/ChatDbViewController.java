package co.real.productionreal2.chat;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.quickblox.chat.exception.QBChatException;
import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.chat.model.QBDialog;
import com.quickblox.users.model.QBUser;

import org.jivesoftware.smack.XMPPConnection;

import co.real.productionreal2.callback.UpdateChatViewListener;
import co.real.productionreal2.chat.core.PrivateChatImpl;
import co.real.productionreal2.dao.account.DBQBChatDialog;
import co.real.productionreal2.dao.account.DBQBUser;
import co.real.productionreal2.daomanager.DatabaseManager;
import co.real.productionreal2.model.QBUserCustomData;

public class ChatDbViewController {
    private static final String TAG = "ChatDbViewController";
    private Context context;
    private DatabaseManager databaseManager;
    private UpdateChatViewListener updateChatViewListener;
    public static final String SEND = "send", RECEIVE = "receive";
    public static final String CREATE_PHOTO = "createPhoto", NEW = "new", SENT = "sent", SENT_USER = "sentUser", READ = "read", BLOCKED = "blocked";
    public static final String RECIEVED = "received", SENT_READ = "sentRead", ERROR = "error";
    public static final String PROPERTY_SAVE_TO_HISTORY = "save_to_history";
    public static final String PROPERTY_PHOTO_URL = "photourl";
    public static final String PROPERTY_DATE_SENT = "date_sent";
    public static final String PROPERTY_MSG_TYPE = "messageType";
    public static final String PROPERTY_SENDER_NAME = "senderName";
    public static final String PROPERTY_BINDING_AGENT_LISTING_ID = "bindingAgentListingID";
    public static final String PROPERTY_SYSTEM_MSG_TYPE= "type";
    public static final String PROPERTY_SYSTEM_MSG_CONTENT= "content";
    public static final String SYSTEM_MSG_CHAT_SETTING = "SystemMessageTypeRefreshChatSetting";
    public static final String SYSTEM_MSG_CHAT_PRIVACY_SETTING = "SystemMessageTypeRefreshChatPrivacySetting";
    public static final String SYSTEM_MSG_ALERT = "SystemMessageTypeAlert";

    public static final String MSG_TYPE_TEXT = "text",MSG_TYPE_IMAGE = "image", MSG_TYPE_AUDIO = "audio",
            MSG_TYPE_VIDEO = "video", MSG_TYPE_UNKNOWN = "unknown", MSG_TYPE_AGENTLISTING = "agentListing", MSG_TYPE_RATE = "rate";
    public static final String PROPERTY_ENABLE_READ_TICK = "chatroomshowreadreceipts";
    public static final String PROPERTY_PHOTO_PATH = "photo_path";
    public static final String CALLBACK_SUCCESS = "success";
    public static final int TEXT = 0;
    public static final int PIC = 1;
    public static final int INVITE_TO_CHAT = -1;
    private static ChatDbViewController instance;
    private QBUser opponentQBUser;
    private QBDialog qbDialog;
    private int opponentID;
    private PrivateChatImpl privateChatImpl;
    private int limitMsgNo = 1;
    private static final int showMsgNo = 20;
    //    private boolean listShowTop = false;
    private String currentMsgOrPhotoPath;
    private int currentMsgType;

    public ChatDbViewController(Context context) {
        this.context = context;
        databaseManager = DatabaseManager.getInstance(context);
    }

    public static ChatDbViewController getInstance(Context context) {
        if (instance == null) {
            instance = new ChatDbViewController(context);
        }
        return instance;
    }

    public void setUpdateChatViewListener(UpdateChatViewListener updateChatViewListener) {
        this.updateChatViewListener = updateChatViewListener;
    }

    public void receivedMsg(QBChatMessage chatMessage) {
        updateChatViewListener.receivedMsg(chatMessage);
    }

    public void statusAndTypingTextViewOnChange(int textColor, String textContent) {
        updateChatViewListener.statusAndTypingTextViewOnChange(textColor, textContent);
    }

    public QBDialog getQbDialog() {
        return qbDialog;
    }

    public void setQbDialog(QBDialog qbDialog) {
        this.qbDialog = qbDialog;
    }

    public int getOpponentID() {
        return opponentID;
    }

    public void setOpponentID(int opponentID) {
        this.opponentID = opponentID;
    }

    public void setOpponentQBUserByQBId(Integer opponentID) {
        DBQBUser dbqbUser = DatabaseManager.getInstance(context).getQBUserByUserId(new Long(opponentID));
        QBUser qbUser = new Gson().fromJson(dbqbUser.getQBUser(), QBUser.class);
        setOpponentQBUser(qbUser);
    }

    public void setOpponentQBUser(QBUser opponentQBUser) {
        this.opponentQBUser = opponentQBUser;
    }

    public QBUser getOpponentQBUser() {
        return opponentQBUser;
    }

    public boolean isDisplayStatusAndTyping() {
        Log.w(TAG, "getCustomData() = " + getOpponentQBUser().getCustomData());
        if (getOpponentQBUser().getCustomData() != null) {
            QBUserCustomData qbUserCustomData = new Gson().fromJson(getOpponentQBUser().getCustomData(), QBUserCustomData.class);
            if (!qbUserCustomData.chatRoomShowLastSeen) {
                return false;
            }
            if (databaseManager.getLogInfo().getEnableStatus() != null) {
                if (databaseManager.getLogInfo().getEnableStatus() == false)
                    return false;
            }
        }
        return true;
    }

    public void firstCreateDialogSuccess(QBDialog qbDialog, String msgTextOrPhotoPath, int type) {
        updateChatViewListener.firstCreateDialogSuccess(qbDialog, msgTextOrPhotoPath, type);
    }

    public void connected(XMPPConnection connection) {
        updateChatViewListener.connected(connection);
    }

    public void authenticated(XMPPConnection connection) {
        updateChatViewListener.authenticated(connection);
    }

    public void connectionClosed() {
        updateChatViewListener.connectionClosed();
    }

    public void connectionClosedOnError(Exception e) {
        updateChatViewListener.connectionClosedOnError(e);
    }

    public void reconnectingIn(int seconds) {
        updateChatViewListener.reconnectingIn(seconds);
    }

    public void reconnectionSuccessful() {
        updateChatViewListener.reconnectionSuccessful();
    }

    public void reconnectionFailed(Exception error) {
        updateChatViewListener.reconnectionFailed(error);
    }


    public void processError(QBChatMessage originChatMessage, QBChatException error) {
        updateChatViewListener.processError(originChatMessage, error);
    }

    public void processMessageDelivered(String messageId, String dialogId, Integer userId) {
        updateChatViewListener.processMessageDelivered(messageId, dialogId);
    }

    public void processMessageRead(String messageId, String dialogId, Integer userId) {
        updateChatViewListener.processMessageRead(messageId, dialogId);
    }

    public void processSystemMessage(QBChatMessage qbChatMessage, DBQBChatDialog dbqbChatDialog) {
        updateChatViewListener.processSystemMessageChatSetting(qbChatMessage, dbqbChatDialog);
    }

    public void processSystemMessageError(QBChatException e, QBChatMessage qbChatMessage) {
        updateChatViewListener.processSystemMessageError(e, qbChatMessage);
    }

    public void processSystemMessageChatPrivacy(QBChatMessage qbChatMessage, QBUser qbUser) {
        updateChatViewListener.processSystemMessageChatPrivacy(qbChatMessage, qbUser);
    }
}
