package co.real.productionreal2.util;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.media.ExifInterface;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.view.inputmethod.InputMethodManager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import co.real.productionreal2.Constants;
import lib.blurbehind.OnBlurCompleteListener;
import lib.blurbehind.util.Blur;

public class ImageUtil {

    public static int contentHeight;
    public final static int normalImageType = 0;
    public final static int iconImageType = 1;
    private static ImageUtil mInstance;
    private int mAlpha = 100;
    private int mFilterColor = -1;
    public OnBlurCompleteListener blurCompleteListener;
    private Bitmap blurredBitmap;
    private BlurActivityTask blurActivityTask;
    private String blurredFromClass;
    public static ImageUtil getInstance() {
        if (mInstance == null) {
            mInstance = new ImageUtil();
        }
        return mInstance;
    }

    public  void clearBlurredBackground(Activity activity){

        if (blurredFromClass != null && blurredFromClass.equals(activity.getClass().getSimpleName())){
            stopBlurActivityTask();
            blurredBitmap = null;
            blurredFromClass = null;

        }
    }

    public static Bitmap loadBitmapFromView(View view) {

        if (view.getMeasuredHeight() <= 0) {
            view.measure(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            Bitmap b = Bitmap.createBitmap(view.getMeasuredWidth(),
                    view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
            Canvas c = new Canvas(b);
            view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
            view.draw(c);
            return b;
        } else {
            Bitmap bitmap = Bitmap.createBitmap(view.getLayoutParams().width,
                    view.getLayoutParams().height, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            view.layout(view.getLeft(), view.getTop(), view.getRight(),
                    view.getBottom());
            view.draw(canvas);
            return bitmap;
        }

    }

    public static int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier(
                "status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }


    public static Bitmap cropImage(Bitmap bitmap, Context context) {
        Bitmap cropBitmap = Bitmap.createBitmap(bitmap, 0, 0,
                bitmap.getWidth(), bitmap.getHeight());

        return cropBitmap;
    }


    public static Bitmap takeViewScreenShot(View v1, Context context) {
        Bitmap bitmap;
        v1.setDrawingCacheEnabled(true);
        bitmap = Bitmap.createBitmap(v1.getDrawingCache());
        v1.setDrawingCacheEnabled(false);
        cropImage(bitmap, context);
        return bitmap;

    }

    private void stopBlurActivityTask(){
        if (blurActivityTask != null && blurActivityTask.getStatus() != AsyncTask.Status.FINISHED) {
            blurActivityTask.cancel(true);
            blurActivityTask = null;
        }
    }
    public void prepareBlurImageFromActivity(Activity activity,Class toActivityClass,OnBlurCompleteListener blurCompleteListener){
        blurredFromClass = toActivityClass.getSimpleName();
        stopBlurActivityTask();
        blurActivityTask = new BlurActivityTask(activity,blurCompleteListener);
        blurActivityTask.execute();
    }

    public boolean hasBluredBackground(){
        return blurredBitmap != null;
    }

    public void applyBlurBackground(Activity activity){
        BitmapDrawable bd = new BitmapDrawable(activity.getResources(), blurredBitmap);
        bd.setAlpha(mAlpha);
        if (mFilterColor != -1) {
            bd.setColorFilter(mFilterColor, PorterDuff.Mode.DST_ATOP);
        }
        activity.getWindow().setBackgroundDrawable(bd);
    }



    private class BlurActivityTask extends AsyncTask<Void, Void, Void> {
        private Activity activity;
        private View decorView;
        private Bitmap image;
        private int mRadius = 25;
        private OnBlurCompleteListener blurCompleteListener;

        public BlurActivityTask(Activity activity,OnBlurCompleteListener blurCompleteListener) {
            this.activity = activity;
            this.blurCompleteListener = blurCompleteListener;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            decorView = activity.getWindow().getDecorView();
            decorView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_LOW);
            decorView.setDrawingCacheEnabled(true);
            decorView.buildDrawingCache();

            image = decorView.getDrawingCache();
        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.d("doInBackground","doInBackground: "+image);
            if (image != null) {
                ImageUtil.getInstance().blurredBitmap = Blur.apply(activity, image, mRadius);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            decorView.destroyDrawingCache();
            decorView.setDrawingCacheEnabled(false);

            activity = null;
            if (blurCompleteListener != null) {
                blurCompleteListener.onBlurComplete();
            }
        }
    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            // Log exception
            return null;
        }
    }

    public static String getExt(String filePath) {
        String filenameArray[] = filePath.split("\\.");
        String extension = filenameArray[filenameArray.length - 1];
        return extension;
    }

    public static int getScreenWidth(Context context) {
        WindowManager wm;
        wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        return width;
    }

    public static int getScreenHeight(Context context) {
        WindowManager wm;
        wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int height = size.y;
        return height;
    }

    public static Bitmap resizeBitmap(Bitmap bitmap) {
        Bitmap resizeBitmap = null;
        double resizeRate;
        if (bitmap.getWidth() < Constants.resizeChatImageWidth && bitmap.getHeight() < Constants.resizeChatImageHeight) {
            return bitmap;
        } else {
            if (bitmap.getWidth() > bitmap.getHeight()) {
                //resize base on width
                resizeRate = (double) Constants.resizeChatImageWidth / bitmap.getWidth();
            } else {
                resizeRate = (double) Constants.resizeChatImageHeight / bitmap.getHeight();
            }
            Log.w("resizeBitmap", "bitmap.getWidth() = " + bitmap.getWidth());
            Log.w("resizeBitmap", "bitmap.getHeight() = " + bitmap.getHeight());
            Log.w("resizeBitmap", "resizeRate = " + resizeRate);
            resizeBitmap = Bitmap.createScaledBitmap(bitmap, (int) (bitmap.getWidth() * resizeRate), (int) (bitmap.getHeight() * resizeRate), true);
        }
        return resizeBitmap;
    }


    public static Bitmap decodeSampledBitmapFromResource(String filePath) {

        //release ram memory to prevent OOM
        System.gc();
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
//        BitmapFactory.decodeResource(res, resId, options);
        BitmapFactory.decodeFile(filePath, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, Constants.resizeImageWidth, Constants.resizeImageHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
//        return BitmapFactory.decodeResource(res, resId, options);
        return BitmapFactory.decodeFile(filePath, options);
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and width
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will guarantee
            // a final image with both dimensions larger than or equal to the
            // requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }

        return inSampleSize;
    }

    public static int getViewHeight(View v) {
        v.measure(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        return v.getMeasuredHeight();
    }

    public static void expand(final View v) {
        v.measure(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        final int targetHeight = v.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? LayoutParams.WRAP_CONTENT
                        : (int) (targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
//        a.setDuration((int)(targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        a.setDuration(100);
        v.startAnimation(a);
    }

    public static void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
//        a.setDuration((int)(initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        a.setDuration(100);
        v.startAnimation(a);
    }


    public static void expandByScroll(View v, int scrollY) {
        v.measure(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        final int targetHeight = v.getMeasuredHeight();
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        if (scrollY < targetHeight) {
            v.getLayoutParams().height = targetHeight + scrollY;
            v.requestLayout();
        }


    }

    public static void collapseByScroll(View v, int scrollY) {
        final int initialHeight = v.getMeasuredHeight();
        if (initialHeight <= 0) {
            v.setVisibility(View.GONE);
        } else {
            v.getLayoutParams().height = scrollY - initialHeight;
            Log.w("collapseByScroll", "v.getLayoutParams().height  = " + v.getLayoutParams().height);
            v.requestLayout();
        }


    }

    public static void hideKeyboard(Context context, View view) {
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static int dpToPx(Context context, int dp) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }


    /**
     * image 90 degree rotated issue
     */
    public static Bitmap decodeFile(String path) {//you can provide file path here
        int orientation;
        try {
            if (path == null) {
                return null;
            }
            // decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            // Find the correct scale value. It should be the power of 2.
            final int REQUIRED_SIZE = 70;
            int width_tmp = o.outWidth, height_tmp = o.outHeight;
            int scale = 0;
            while (true) {
                if (width_tmp / 2 < REQUIRED_SIZE
                        || height_tmp / 2 < REQUIRED_SIZE)
                    break;
                width_tmp /= 2;
                height_tmp /= 2;
                scale++;
            }
            // decode with inSampleSize
            // First decode with inJustDecodeBounds=true to check dimensions
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            // Calculate inSampleSize
            options.inSampleSize = calculateInSampleSize(options, Constants.resizeImageWidth, Constants.resizeImageHeight);

            // Decode bitmap with inSampleSize set
            options.inJustDecodeBounds = false;
            Bitmap bm = BitmapFactory.decodeFile(path, options);
            Bitmap bitmap = bm;

            ExifInterface exif = new ExifInterface(path);

            orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);

            Log.e("ExifInteface .........", "rotation =" + orientation);

            //exif.setAttribute(ExifInterface.ORIENTATION_ROTATE_90, 90);

            Log.e("orientation", "" + orientation);
            Matrix m = new Matrix();

            if ((orientation == ExifInterface.ORIENTATION_ROTATE_180)) {
                m.postRotate(180);
                //m.postScale((float) bm.getWidth(), (float) bm.getHeight());
                // if(m.preRotate(90)){
                Log.e("in orientation", "" + orientation);
                bitmap = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), m, true);
                return bitmap;
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                m.postRotate(90);
                Log.e("in orientation", "" + orientation);
                bitmap = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), m, true);
                return bitmap;
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                m.postRotate(270);
                Log.e("in orientation", "" + orientation);
                bitmap = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), m, true);
                return bitmap;
            }
            return bitmap;
        } catch (Exception e) {
            return null;
        }
    }


    // type 0 = normalImageType
    // type 1 = iconImageType
    public static synchronized String getCompressImageFilePath(String originFilePath, String tempFilePath, int type) {

        try {

            if (tempFilePath == null) {
                tempFilePath = getTempFilePath();
            }


            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            Bitmap bmp = BitmapFactory.decodeFile(originFilePath, options);

            int actualHeight = options.outHeight;
            int actualWidth = options.outWidth;

            float maxHeight = Constants.compressNormalImageWidth;
            float maxWidth = Constants.compressNormalImageWidth;
            if (type == iconImageType) {
                maxHeight = Constants.compressIconImageWidth;
                maxWidth = Constants.compressIconImageWidth;
            }

            float imgRatio = actualWidth / actualHeight;
            float maxRatio = maxWidth / maxHeight;

            FileOutputStream out = null;

            if (actualHeight > maxHeight || actualWidth > maxWidth) {
                if (imgRatio < maxRatio) {
                    imgRatio = maxHeight / actualHeight;
                    actualWidth = (int) (imgRatio * actualWidth);
                    actualHeight = (int) maxHeight;
                } else if (imgRatio > maxRatio) {
                    imgRatio = maxWidth / actualWidth;
                    actualHeight = (int) (imgRatio * actualHeight);
                    actualWidth = (int) maxWidth;
                } else {
                    actualHeight = (int) maxHeight;
                    actualWidth = (int) maxWidth;

                }
            }

            options.inSampleSize = calculateInSampleSize2(options, actualWidth, actualHeight);
            options.inJustDecodeBounds = false;
            options.inDither = false;
            options.inPurgeable = true;
            options.inInputShareable = true;
            options.inTempStorage = new byte[16 * 1024];

            bmp = BitmapFactory.decodeFile(originFilePath, options);

            ExifInterface exif;
            exif = new ExifInterface(originFilePath);

            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
            } else if (orientation == 3) {
                matrix.postRotate(180);
            } else if (orientation == 8) {
                matrix.postRotate(270);
            }
            bmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true);

            out = new FileOutputStream(tempFilePath);
            bmp.compress(Bitmap.CompressFormat.JPEG, type == iconImageType ? 95 : 80, out);
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return tempFilePath;

    }

    public static String getTempFilePath() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "Real/Images");
        if (!file.exists()) {
            file.mkdirs();
        }
//        String uriSting = (file.getAbsolutePath() + "/"+ System.currentTimeMillis() + ".jpg");
        String uriSting = (file.getAbsolutePath() + "/" + "temp" + ".jpg");
        return uriSting;

    }

    public static int calculateInSampleSize2(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;

        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }

}
