package co.real.productionreal2.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import co.real.productionreal2.CoreData;
import co.real.productionreal2.Event.UpdateInputPhoneNumberEvent;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.login.IntroActivity;
import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.callback.DialogCallback;
import co.real.productionreal2.model.SystemSetting;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.request.RequestCountryCodeGet;
import co.real.productionreal2.service.model.request.RequestPhoneNumberMigrate;
import co.real.productionreal2.service.model.request.RequestPhoneNumberRegEX;
import co.real.productionreal2.service.model.response.ResponseCountryCodeGet;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;
import co.real.productionreal2.service.model.response.ResponsePhoneNumberMigrate;
import co.real.productionreal2.service.model.response.ResponsePhoneNumberRegEX;
import co.real.productionreal2.util.AppUtil;
import co.real.productionreal2.view.Dialog;


public class InputPhoneNoFragment extends BaseFragment {
    private static final String TAG = "InputPhoneNoFragment";

    @InjectView(R.id.phoneEditText)
    EditText phoneEditText;
    @InjectView(R.id.countryCodeBtn)
    Button countryCodeBtn;
    @InjectView(R.id.goToVerifyPageBtn)
    Button goToVerifyPageBtn;
    @InjectView(R.id.tv_sendEmail)
    TextView tv_sendEmail;

    IntroActivity introActivity;

    private ResponseLoginSocial.Content userContent;
    private AlertDialog mConfirmDialog;
    private final int DEFAULT_COUNTRY_CODE = 852;
    private int selectedPos = -1;
    private int countryCode;
    boolean isOldUser;
    String oldCountryCode;
    String newCountryCode;
    String oldPhoneNumber;
    String newPhoneNumber;
    int phoneRegID;
    int regType;

    RequestCountryCodeGet requestCountryCodeGet;
    List<ResponseCountryCodeGet.SMSCountryCode> sMSCountryCodeList;
    RequestPhoneNumberRegEX requestPhoneNumberRegEX;
    RequestPhoneNumberMigrate requestPhoneNumberMigrate;

    public static final InputPhoneNoFragment newInstance(IntroActivity introActivity) {
        InputPhoneNoFragment f = new InputPhoneNoFragment();
        Bundle bdl = new Bundle(1);
        f.setArguments(bdl);
        f.setIntroActivity(introActivity);

        return f;
    }

    void setIntroActivity(IntroActivity introActivity){
        this.introActivity = introActivity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_input_phone_number, container, false);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        userContent = CoreData.getUserContent(this.getContext());
        isOldUser = CoreData.isOldUser;
        initView();
        addListener();

        callCountryCodeGetApi();
    }


    public void initView() {
        tv_sendEmail.setText(Html.fromHtml("<u>"+"hfiudhdfihvosiduhvdoixl fsdvsd"+"</u>"));

        String countryIso = AppUtil.getCurrentLangISO2Code(activity);
        Log.w(TAG, "countryIso = " + countryIso);
        //TODO edwin
        countryCode = getCountryCode(countryIso);
        countryCodeBtn.setText("+" + countryCode);

        goToVerifyPageBtn.setEnabled(false);
    }

    void addListener(){
        phoneEditText.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start,int before, int count)
            {
                // TODO Auto-generated method stub
            }
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                if(s.length()>5)
                {
                    goToVerifyPageBtn.setEnabled(true);
                }else{
                    goToVerifyPageBtn.setEnabled(false);
                }
            }

        });
        phoneEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(event!=null){
                }
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    if(goToVerifyPageBtn.isEnabled()){
                        goToVerifyPageBtn.performClick();
                    }
                }
                return false;
            }
        });
    }

    private int getCountryCode(String countryIso) {
        try {
            for (SystemSetting.CountryCodeInfo countryCodeInfo : userContent.systemSetting.smsCountryCodeInfoList) {
                Log.w(TAG, "countryCodeInfo.countryCode = " + countryCodeInfo.countryShortName + " " + countryCodeInfo.countryCode + " " + countryIso);
                if (countryCodeInfo.countryShortName.equalsIgnoreCase(countryIso)) {
                    return Integer.valueOf(countryCodeInfo.countryCode);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return DEFAULT_COUNTRY_CODE;
    }

    private ArrayList<Integer> filterDeputeCountryCodeList(ArrayList<Integer> countryCodeList) {
        Set<Integer> hs = new HashSet<>();
        hs.addAll(countryCodeList);
        countryCodeList.clear();
        countryCodeList.addAll(hs);
        Collections.sort(countryCodeList);
        return countryCodeList;
    }


    @OnClick(R.id.countryCodeBtn)
    public void countryCodeBtnClick(View view) {
        ArrayList<Integer> countryCodeList = new ArrayList<>();
        for (ResponseCountryCodeGet.SMSCountryCode sMSCountryCode : sMSCountryCodeList) {
            countryCodeList.add(Integer.parseInt(sMSCountryCode.v));
        }

        final ArrayList<Integer> filterCountryCodeList = filterDeputeCountryCodeList(countryCodeList);

        final ArrayAdapter<Integer> arrayAdapter = new ArrayAdapter<>(
                activity,
                android.R.layout.simple_list_item_checked,
                countryCodeList
        );

        //init value
        if (selectedPos == -1) {
            int i = 0;
            for (int index : filterCountryCodeList) {
                if (index == countryCode) {
                    selectedPos = i;
                }
                i++;
            }
        }

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity)
                .setSingleChoiceItems(arrayAdapter, selectedPos, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        countryCodeBtn.setText("+" + filterCountryCodeList.get(i));
                        selectedPos = i;
                        dialogInterface.dismiss();
                    }
                });

        mConfirmDialog = dialogBuilder.create();
        mConfirmDialog.show();

    }

    @OnClick(R.id.tv_sendEmail)
    public void tv_sendEmailClick(View view) {

        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:"));
        intent.putExtra(Intent.EXTRA_EMAIL  , new String[]{"team@real.co"});
        try {
            if (intent.resolveActivity(activity.getPackageManager()) != null) {
                startActivity(intent);
            }
//            context.startActivity(Intent.createChooser(intent, "Send mail..."));
        } catch (Exception ex) {
        }

    }

    @OnClick(R.id.goToVerifyPageBtn)
    public void goToVerifyPageBtnClick(View view) {

        newCountryCode = countryCodeBtn.getText().toString();
        newPhoneNumber = phoneEditText.getText().toString();

        if(!newCountryCode.equals(oldCountryCode) || !newPhoneNumber.equals(oldPhoneNumber)) {
            showConfirDialog();
        }else{
            goToVerifyPage();
        }

    }

    void callCountryCodeGetApi(){
        launchLoadingDialog();
        requestCountryCodeGet = new RequestCountryCodeGet();

        requestCountryCodeGet.callCountryCodeGetApi(activity, new ApiCallback() {
            @Override
            public void success(ApiResponse apiResponse) {
                ResponseCountryCodeGet responseGson = new Gson().fromJson(apiResponse.jsonContent, ResponseCountryCodeGet.class);
                sMSCountryCodeList = responseGson.content.sMSCountryCodeList;

                cancelLoadingDialog();
            }

            @Override
            public void failure(String errorMsg) {
                AppUtil.showAlertDialog(activity, errorMsg);
                cancelLoadingDialog();
            }
        });
    }

    void showConfirDialog(){
        String phoneNoWithCountryCode = newCountryCode + " " + newPhoneNumber;
        Dialog.confirmChangePhoneNoDialog(getActivity(), phoneNoWithCountryCode, new DialogCallback() {
            @Override
            public void yes() {
                if(isOldUser) {
                    callPhoneNumberMigrateApi();
                }else{
                    callPhoneNumberRegEXApi();
                }
            }
        }).show();
    }

    void callPhoneNumberRegEXApi(){
        launchLoadingDialog();
        requestPhoneNumberRegEX = new RequestPhoneNumberRegEX();

        requestPhoneNumberRegEX.countryCode = newCountryCode;
        requestPhoneNumberRegEX.phoneNumber = newPhoneNumber;

        requestPhoneNumberRegEX.callPhoneNumberRegEXApi(activity, new ApiCallback() {
            @Override
            public void success(ApiResponse apiResponse) {
                ResponsePhoneNumberRegEX responseGson = new Gson().fromJson(apiResponse.jsonContent, ResponsePhoneNumberRegEX.class);
                phoneRegID = responseGson.content.phoneRegID;
                regType = responseGson.content.regType;

                sendUpdateInputPhoneNumberEvent();
                goToVerifyPage();
                cancelLoadingDialog();
            }

            @Override
            public void failure(String errorMsg) {
                AppUtil.showAlertDialog(activity, errorMsg);
                cancelLoadingDialog();
            }
        });
    }

    void callPhoneNumberMigrateApi(){
        launchLoadingDialog();
        requestPhoneNumberMigrate = new RequestPhoneNumberMigrate(24, "5f747107-348a-46c4-a7ee-5c7cd26def42",
                newCountryCode, newPhoneNumber, AppUtil.getCurrentLangCode(getActivity()));
        requestPhoneNumberMigrate.callPhoneNumberMigrateApi(getActivity(), new ApiCallback() {
                    @Override
                    public void success(ApiResponse apiResponse) {
                        ResponsePhoneNumberMigrate responseGson = new Gson().fromJson(apiResponse.jsonContent, ResponsePhoneNumberMigrate.class);
                        phoneRegID = responseGson.content.phoneRegId;

                        sendUpdateInputPhoneNumberEvent();
                        goToVerifyPage();
                        cancelLoadingDialog();
                    }

                    @Override
                    public void failure(String errorMsg) {
                        AppUtil.showAlertDialog(getActivity(),errorMsg);
                        cancelLoadingDialog();
                    }
                });
    }

    void sendUpdateInputPhoneNumberEvent(){
        EventBus.getDefault().post(new UpdateInputPhoneNumberEvent(newCountryCode, newPhoneNumber, phoneRegID, regType));
        oldCountryCode = newCountryCode;
        oldPhoneNumber = newPhoneNumber;

    }

    void goToVerifyPage(){
        introActivity.goToVerifyPage();
    }



}
