package co.real.productionreal2.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import java.util.Calendar;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import co.real.productionreal2.Constants;
import co.real.productionreal2.R;
import co.real.productionreal2.service.model.AgentExp;
import co.real.productionreal2.util.TimeUtils;
import co.real.productionreal2.view.Dialog;

/**
 * Created by hohojo on 11/9/2015.
 */
public class AgentExpActivity extends BaseActivity {
    @InjectView(R.id.rootView)
    View rootView;
    @InjectView(R.id.companyEditText)
    EditText companyEditText;
    @InjectView(R.id.titleEditText)
    EditText titleEditText;
    @InjectView(R.id.currentJobSwitch)
    Switch currentJobSwitch;
    @InjectView(R.id.expStartDateTextView)
    TextView expStartDateTextView;
    @InjectView(R.id.expEndDateTextView)
    TextView expEndDateTextView;
    @InjectView(R.id.saveBtn)
    Button saveBtn;
    @InjectView(R.id.endDateView)
    View endDateView;

    private Calendar startCalendar = Calendar.getInstance();
    private Calendar endCalendar = Calendar.getInstance();
    private AgentExp experience = new AgentExp();
    private boolean isEdit = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agent_exp);

        ButterKnife.inject(this);
        initView();
        addListener();

    }

    private void addListener() {

        currentJobSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    endDateView.setVisibility(View.GONE);
                } else {
                    endDateView.setVisibility(View.VISIBLE);
                }
                updateUI();
            }
        });

        expStartDateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Dialog().datePickerDialogWithoutDay(
                        AgentExpActivity.this,
                        startDate,
                        endCalendar.get(Calendar.YEAR),
                        endCalendar.get(Calendar.MONTH)
                ).show();
            }
        });

        expEndDateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Dialog().datePickerDialogWithoutDay(
                        AgentExpActivity.this,
                        endDate,
                        endCalendar.get(Calendar.YEAR),
                        endCalendar.get(Calendar.MONTH)
                ).show();
            }
        });

        companyEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                updateUI();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        titleEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                updateUI();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void initView() {
        Intent intent = getIntent();
        isEdit = intent.getBooleanExtra(Constants.EXTRA_AGENT_EXP_IS_EDIT, false);
        if (isEdit) {
            AgentExp agentExp = intent.getParcelableExtra(Constants.EXTRA_AGENT_EXP_EDIT_CONTENT);
            String startDateString = agentExp.startDate.substring(6, agentExp.startDate.length() - 2);
            String endDateString = agentExp.endDate.substring(6, agentExp.startDate.length() - 2);
            expStartDateTextView.setText(TimeUtils.getDateFromMilliSeconds(Long.parseLong(startDateString)));
            expEndDateTextView.setText(TimeUtils.getDateFromMilliSeconds(Long.parseLong(endDateString)));
            currentJobSwitch.setChecked(agentExp.isCurrentJob == 1 ? true : false);
            if (agentExp.isCurrentJob == 1) {
                endDateView.setVisibility(View.GONE);
            }
            companyEditText.setText(agentExp.company);
            titleEditText.setText(agentExp.title);
        }

        updateUI();

    }

    @OnClick(R.id.backBtn)
    public void closeBtn(View view) {
        setResult(RESULT_CANCELED);
        finish();
    }


    @OnClick(R.id.saveBtn)
    public void saveBtn(View view) {

        if (!dateIsValid(experience.startDate, experience.endDate)) {
            String errorDialogMsg = getString(R.string.add_experience__end_date_must_after_start_date);
            Dialog.normalDialog(this, errorDialogMsg).show();
        } else {

            Intent intent = new Intent();
            intent.putExtra(Constants.EXTRA_REQUEST_AGENT_EXP, experience);
            intent.putExtra(Constants.EXTRA_AGENT_EXP_IS_EDIT, isEdit);
            intent.putExtra(Constants.EXTRA_AGENT_EXP_EDIT_TAG_NO, getIntent().getIntExtra(Constants.EXTRA_AGENT_EXP_EDIT_TAG_NO, 0));
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    private void updateUI() {
        experience.company = companyEditText.getText().toString();
        experience.title = titleEditText.getText().toString();

        if (currentJobSwitch.isChecked()) {
            experience.isCurrentJob = 1;
            experience.startDate = TimeUtils.calendarToString(startCalendar, "yyyy-MM-dd");
            experience.startDateString = experience.startDate;
            experience.endDate = TimeUtils.currentDateString();
            experience.endDateString = experience.endDate;
        } else {
            experience.isCurrentJob = 0;
            experience.startDate = TimeUtils.calendarToString(startCalendar, "yyyy-MM-dd");
            experience.startDateString = experience.startDate;
            experience.endDate = TimeUtils.calendarToString(endCalendar, "yyyy-MM-dd");
            experience.endDateString = experience.endDate;
        }
        saveBtn.setEnabled(isExpDataValid(experience));
    }

    private boolean dateIsValid(String startDate, String endDate) {
        return (
                TimeUtils.stringToDate(TimeUtils.calendarToString(startCalendar, "yyyy-MM-dd")).before(TimeUtils.stringToDate(TimeUtils.calendarToString(endCalendar, "yyyy-MM-dd"))) ||
                        TimeUtils.stringToDate(TimeUtils.calendarToString(startCalendar, "yyyy-MM-dd")).equals(TimeUtils.stringToDate(TimeUtils.calendarToString(endCalendar, "yyyy-MM-dd")))
        );
    }


    private boolean isExpDataValid(AgentExp experience) {
        boolean isValid = true;
        if (experience.company == "" || experience.company.isEmpty()) {
            isValid = false;
        }
        if (experience.title == "" || experience.title.isEmpty()) {
            isValid = false;
        }
        if (expStartDateTextView.getText().toString().equalsIgnoreCase(getString(R.string.add_experience__MM_YY))) {
            isValid = false;
        }
        //not current position
        if (!currentJobSwitch.isChecked())
            if (expEndDateTextView.getText().toString().equalsIgnoreCase(getString(R.string.add_experience__MM_YY)))
                isValid = false;

        return isValid;
    }

    DatePickerDialog.OnDateSetListener startDate = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            startCalendar.set(Calendar.YEAR, year);
            startCalendar.set(Calendar.MONTH, monthOfYear);
            startCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            expStartDateTextView.setText(TimeUtils.calendarToString(startCalendar, getString(R.string.general_date_format_month_year)));
            updateUI();
        }

    };


    DatePickerDialog.OnDateSetListener endDate = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            endCalendar.set(Calendar.YEAR, year);
            endCalendar.set(Calendar.MONTH, monthOfYear);
            endCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            expEndDateTextView.setText(TimeUtils.calendarToString(endCalendar, getString(R.string.general_date_format_month_year)));
            updateUI();
        }

    };

}
