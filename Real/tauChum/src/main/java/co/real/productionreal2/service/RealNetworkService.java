package co.real.productionreal2.service;

import android.content.Context;

import co.real.productionreal2.Constants;
import co.real.productionreal2.model.InputRequest;
import co.real.productionreal2.service.model.ApiResponse;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by hohojo on 14/12/2015.
 */
public class RealNetworkService extends BaseService{

    //	public static final String WEB_SERVICE_BASE_URL = "http://52.24.198.180";//"http://192.168.1.138/";


    //	public static final String WEB_SERVICE_BASE_URL = "http://192.168.1.138/";
    //	CookieHeaderProvider cookieHeaderProvider = new CookieHeaderProvider();
    public final RealNetwork realNetworkService;

    public RealNetworkService(Context context) {
        RequestInterceptor requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestInterceptor.RequestFacade request) {
                request.addHeader("Accept", "application/json");
                request.addHeader("Content-type", "application/json");

            }
        };


        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(WEB_SERVICE_BASE_URL)
                .setRequestInterceptor(requestInterceptor)
                .setLogLevel(Constants.isDevelopment? RestAdapter.LogLevel.FULL : RestAdapter.LogLevel.NONE)
                .build();

        realNetworkService = restAdapter.create(RealNetwork.class);
    }

    public RealNetworkService(Context context, boolean dummy) {
        RequestInterceptor requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestInterceptor.RequestFacade request) {
                request.addHeader("Accept", "*/*");
                request.addHeader("Content-type", "multipart/form-data");

            }
        };

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(WEB_SERVICE_BASE_URL)
                .setRequestInterceptor(requestInterceptor)
                .setLogLevel(Constants.isDevelopment? RestAdapter.LogLevel.FULL : RestAdapter.LogLevel.NONE)
//                .setConverter(new MixedConverter(new SimpleXMLConverter(), new GsonConverter(gson)));
//                .setClient(new InterceptingOkClient())
//                .setClient(client)
                .build();

        realNetworkService = restAdapter.create(RealNetwork.class);
    }


    //RealNetwork.java
    public interface RealNetwork {
        @POST("/RealNetwork.asmx/RealNetworkSendResetPasswordEmail")
        public void resetPwEmail(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);


        @POST("/RealNetwork.asmx/RealNetworkChangePassword")
        public void changePassword(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);


        @POST("/RealNetwork.asmx/RealNetworkChangeEmail")
        public void changeEmail(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);

        @POST("/RealNetwork.asmx/RealNetworkMemberRegistration")
        public void memReg(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);

        @POST("/RealNetwork.asmx/RealNetworkMemberPhoneRegistration")
        public void memPhoneReg(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);

        @POST("/RealNetwork.asmx/RealNetworkMemberLogin")
        public void memLogin(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);

        @POST("/RealNetwork.asmx/RealNetworkCheckEmailAvailability")
        public void checkEmailAvailability(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);
        ////////////////////////////////////////////////


    }


    public RealNetwork getCoffeeService() {
        return realNetworkService;
    }

}
