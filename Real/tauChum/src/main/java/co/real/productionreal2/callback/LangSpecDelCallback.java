package co.real.productionreal2.callback;

/**
 * Created by hohojo on 15/9/2015.
 */
public interface LangSpecDelCallback {
    public void onLangSpecDel(int tagNo, int type);
}
