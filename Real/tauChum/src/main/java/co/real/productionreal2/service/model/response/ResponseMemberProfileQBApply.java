package co.real.productionreal2.service.model.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by kelvinsun on 19/2/16.
 */
public class ResponseMemberProfileQBApply extends ResponseBase{
    @SerializedName("Content")
    public Content content;

    public class Content {
        @SerializedName("QBID")
        public String QBID;
        @SerializedName("QBPwd")
        public String QBPwd;

        @Override
        public String toString() {
            return "Content{" +
                    "QBID=" + QBID +
                    ", QBPwd=" + QBPwd +
                    '}';
        }
    }
}
