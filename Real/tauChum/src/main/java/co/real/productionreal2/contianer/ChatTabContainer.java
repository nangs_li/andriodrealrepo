package co.real.productionreal2.contianer;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import co.real.productionreal2.BaseContainerFragment;
import co.real.productionreal2.R;
import co.real.productionreal2.chat.ChatMainDbViewController;
import co.real.productionreal2.common.LocalStorageHelper;
import co.real.productionreal2.fragment.ChatTabsFragment;
import co.real.productionreal2.fragment.ChatUnloginFragment;

public class ChatTabContainer extends BaseContainerFragment {
	boolean needInit = true;
	ChatTabsFragment myChatTabsFragment;

	public void fragmentBecameVisible() {
		if (!LocalStorageHelper.getQBpw(getContext()).equals("")) {
			if (needInit) {
				initView();
				needInit = false;
			}

		}else {
			ChatMainDbViewController.getInstance(ChatTabContainer.this.getContext()).callQBUserSignUpOrLogin();
			replaceFragment(ChatUnloginFragment.newInstance(), false);
			needInit = true;
		}

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.e("Ritesh", "Tab3");
		View rootView = inflater.inflate(R.layout.container_fragment, null);
//		ViewGroup.MarginLayoutParams marginParams = new ViewGroup.MarginLayoutParams(rootView.getLayoutParams());
//		marginParams.setMargins(0, MainActivityTabBase.tabHeight, 0, 0);
//		FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(marginParams);
//		rootView.setLayoutParams(layoutParams);

		myChatTabsFragment = ChatTabsFragment.newInstance();

//		fragmentBecameVisible();

		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}

	private void initView() {
		replaceFragment(myChatTabsFragment, false);

	}
}