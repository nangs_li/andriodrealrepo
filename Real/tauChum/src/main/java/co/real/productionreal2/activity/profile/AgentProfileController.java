package co.real.productionreal2.activity.profile;

import android.content.Context;

import co.real.productionreal2.service.model.AgentExp;

/**
 * Created by hohojo on 14/9/2015.
 */
public class AgentProfileController {
    private Context context;
    private static AgentProfileController instance;

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// Init controller////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public AgentProfileController(Context context) {
        this.context = context;
    }

    public static AgentProfileController getInstance(Context context) {
        if (instance == null) {
            instance = new AgentProfileController(context);
        }
        return instance;
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// Init controller////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public void stateReceiveNewAgentExp(AgentExp agentExp) {
        //todo  to data then view

    }


}
