package co.real.productionreal2.activity.createpost;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import co.real.productionreal2.CoreData;
import co.real.productionreal2.common.LocalStorageHelper;
import co.real.productionreal2.daomanager.DatabaseManager;
import co.real.productionreal2.model.AgentListing;
import co.real.productionreal2.service.model.AgentProfiles;
import co.real.productionreal2.service.model.request.RequestListing;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;

/**
 * Created by hohojo on 12/10/2015.
 */
public class BaseCreatePost {
    private static final String TAG = "BaseCreatePost";
    private AgentProfiles agentProfiles;
    private AgentListing createPostRequest;
    private static BaseCreatePost instance;
    private Context context;
    private byte[] backgroundBitmapBytes;
    private ArrayList<ReasonsDespImage> reasonsDespImageList;
    //    private ArrayList<CreatePost> createPostList;
    private ReasonsDespImage currentReasonsDespImage;
    private List<AgentListing.Photo> photoPathList;
    private String addressInputStr;

    public static BaseCreatePost getInstance(Context context) {

        if (instance == null) {
            instance = new BaseCreatePost(context);
        }

        return instance;
    }

    public void resetBaseCreatePost() {
        instance = null;
    }

    public BaseCreatePost(final Context context) {
        this.context = context;
        if (DatabaseManager.getInstance(context).getLogInfo() != null) {
            ResponseLoginSocial.Content content = CoreData.getUserContent(context);
            createPostRequest = new AgentListing(content.memId, LocalStorageHelper.getAccessToken(context));
            reasonsDespImageList = new ArrayList<>();
        }
//        createPostList = new ArrayList<>();
    }

    ////////////////////////////////////////////////////////////////  background image ////////////////////////////////////////////////////////////////

    public void setBackgroundBitmapBytes(byte[] backgroundBitmapBytes) {
        this.backgroundBitmapBytes = backgroundBitmapBytes;
    }
    ////////////////////////////////////////////////////////////////  background image ////////////////////////////////////////////////////////////////


//    public ArrayList<CreatePost> getCreatePostList() {
//        return createPostList;
//    }
//
//    public void setCreatePostList(ArrayList<CreatePost> createPostList) {
//        this.createPostList = createPostList;
//    }

    public AgentListing getCreatePostRequest() {
        return createPostRequest;
    }

    //    public CreatePost findCurrentCreatePost(int langIndex) {
//        for (CreatePost createPost:getCreatePostList()) {
//            if (createPost.langIndex == langIndex) {
//                return createPost;
//            }
//        }
//        ResponseLoginSocial.Content agentContent = new Gson().fromJson(DatabaseManager.getInstance(context).getLogInfo().getLogInfo(), ResponseLoginSocial.Content.class);
//
//        CreatePost createPost = new CreatePost(langIndex);
//        createPost.createPostRequest = new RequestListing(agentContent.memId, LocalStorageHelper.getAccessToken(getBaseContext()));
//
//        return createPost;
//    }
    ////////////////////////////////////////////////////////////////  PhotoPathList ////////////////////////////////////////////////////////////////
    public List<AgentListing.Photo> getPhotoPathList() {
        return photoPathList;
    }

    public void setPhotoPathList(List<AgentListing.Photo> photoPathList) {
        this.photoPathList = photoPathList;
    }
    ////////////////////////////////////////////////////////////////  PhotoPathList ////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////  3 Reasons ////////////////////////////////////////////////////////////////

    public void addReasonsToRequest() {
        RequestListing.Reason currReason = new RequestListing.Reason();
        currReason.languageIndex = getCurrentReasonsDespImage().langIndex;
        currReason.sloganIndex = getCurrentReasonsDespImage().sloganIndex;
        currReason.reason1 = getCurrentReasonsDespImage().reason1;
        currReason.reason2 = getCurrentReasonsDespImage().reason2;
        currReason.reason3 = getCurrentReasonsDespImage().reason3;
//        boolean isNeedAdd = true;
//        if (getCreatePostRequest().reasons.size() > 0) {
//            for (int i = 0; i < getCreatePostRequest().reasons.size(); i++) {
//                if (isNeedAdd) {
//                    if (getCreatePostRequest().reasons.get(i).languageIndex == getCurrentReasonsDespImage().langIndex) {
//                        getCreatePostRequest().reasons.set(i, currReason);
//                        isNeedAdd = false;
//                    } else {
//                        isNeedAdd = true;
//                    }
//                }
//            }
//        } else {
//            isNeedAdd = true;
//        }
//
//        if (isNeedAdd) {
        getCreatePostRequest().reasons.add(currReason);
//        }

        List<RequestListing.Reason> reasons = getCreatePostRequest().reasons;
        Log.w(TAG, "reasons = " + reasons);
    }

    public void replaceReasonsInRequest(int index) {
        RequestListing.Reason currReason = new RequestListing.Reason();
        currReason.languageIndex = getCurrentReasonsDespImage(index).langIndex;
        currReason.sloganIndex = getCurrentReasonsDespImage(index).sloganIndex;
        currReason.reason1 = getCurrentReasonsDespImage(index).reason1;
        currReason.reason2 = getCurrentReasonsDespImage(index).reason2;
        currReason.reason3 = getCurrentReasonsDespImage(index).reason3;
        for (int i = 0; i < getCreatePostRequest().reasons.size(); i++) {
            if (getCreatePostRequest().reasons.get(i).languageIndex == getCurrentReasonsDespImage().langIndex) {
                getCreatePostRequest().reasons.set(i, currReason);
            }
        }
    }

    public void formatReasonInRequest() {
        getCreatePostRequest().reasons.clear();

        for (int i = 0; i < reasonsDespImageList.size(); i++) {
            RequestListing.Reason currReason = new RequestListing.Reason();
            currReason.languageIndex = getCurrentReasonsDespImage(i).langIndex;
            currReason.sloganIndex = getCurrentReasonsDespImage(i).sloganIndex;
            currReason.reason1 = getCurrentReasonsDespImage(i).reason1;
            currReason.reason2 = getCurrentReasonsDespImage(i).reason2;
            currReason.reason3 = getCurrentReasonsDespImage(i).reason3;
            getCreatePostRequest().reasons.add(currReason);
        }

        Log.w(TAG, "formatReasonInRequest = " + getCreatePostRequest().reasons.size() + " " + getCreatePostRequest().reasons.toString());
    }

    ////////////////////////////////////////////////////////////////  3 Reasons ////////////////////////////////////////////////////////////////


    ////////////////////////////////////////////////////////////////  ReasonsDespImage ////////////////////////////////////////////////////////////////

    public ReasonsDespImage getCurrentReasonsDespImage() {
        return currentReasonsDespImage;
    }

    public ReasonsDespImage getCurrentReasonsDespImage(int index) {
        return reasonsDespImageList.get(index);
    }

    public void setCurrentReasonsDespImage(int index, ReasonsDespImage reasonsDespImage) {
        this.reasonsDespImageList.set(index, reasonsDespImage);
    }

    public void resetCurrentResonsDespImage() {
        currentReasonsDespImage = null;
    }

    public void initCurrentReasonsImage(int langIndex, int sloganIndex) {
        this.currentReasonsDespImage = new ReasonsDespImage(langIndex, sloganIndex);
    }

    public ReasonsDespImage getCurrentReasonsImage(int langIndex) {
        if (getReasonsDespImageList()!=null) {
            for (ReasonsDespImage reason : getReasonsDespImageList()) {
                if (reason.langIndex == langIndex) {
                    return reason;
                }
            }
            return currentReasonsDespImage;
        }else{
            return null;
        }
    }

    public void setCurrentReasonsImage(ReasonsDespImage reasonsDespImage) {
        this.currentReasonsDespImage = reasonsDespImage;
    }


    public ArrayList<ReasonsDespImage> getReasonsDespImageList() {
        return reasonsDespImageList;
    }

    public void setReasonsDespImageList(ArrayList<ReasonsDespImage> reasonsDespImageList) {
        this.reasonsDespImageList = reasonsDespImageList;
    }


    public String getAddressInputStr() {
        return addressInputStr;
    }

    public void setAddressInputStr(String addressInputStr) {
        this.addressInputStr = addressInputStr;
    }

    public int getUploadImageCount() {
        int count = 0;
        for (int i = 0; i < getReasonsDespImageList().size(); i++) {
            if (checkImagePathValid(getReasonsDespImageList().get(i).reason1ImagePath))
                count++;
            if (checkImagePathValid(getReasonsDespImageList().get(i).reason2ImagePath))
                count++;
            if (checkImagePathValid(getReasonsDespImageList().get(i).reason3ImagePath))
                count++;
        }
        count+=getPhotoPathList().size();
        return count;
    }

    private boolean checkImagePathValid(String imagePath) {
        if (imagePath == null || imagePath.isEmpty()) {
            return false;
        }
        return true;
    }
////////////////////////////////////////////////////////////////  ReasonsDespImage ////////////////////////////////////////////////////////////////


    public class ReasonsDespImage {
        ReasonsDespImage(int langIndex, int sloganIndex) {
            this.langIndex = langIndex;
            this.sloganIndex = sloganIndex;
        }

        public int langIndex;
        public int sloganIndex;
        public String reason1ImagePath;
        public String reason2ImagePath;
        public String reason3ImagePath;
        public String reason1="";
        public String reason2="";
        public String reason3="";
    }


}
