package co.real.productionreal2.chat.pushnotifications;

/**
 * Created by igorkhomenko on 4/28/15.
 */
public class Consts {
    // In GCM, the Sender ID is a project ID that you acquire from the API console
    public static final String PROJECT_NUMBER = "492248870979"; //ProjectID = real-973

    public static final String GCM_NOTIFICATION = "GCM Notification";
    public static final String GCM_DELETED_MESSAGE = "Deleted messages on server: ";
    public static final String GCM_INTENT_SERVICE = "GcmIntentService";
    public static final String GCM_SEND_ERROR = "Send error: ";
    public static final String GCM_RECEIVED = "Received: ";

    public static final String NEW_PUSH_EVENT = "new-push-event";

    public static final String ME_NEW_COUNT_EVENT = "ME_NEW_COUNT_EVENT";
    public static final String NEWSFEED_NEW_COUNT_EVENT = "NEWSFEED_NEW_COUNT_EVENT";

    public static final String GCM_KEY_ALERT = "alert";
    public static final String GCM_KEY_NEW_COUNT = "NewCount";
    public static final String GCM_KEY_PUSH_TYPE = "PushType";
    public static final String GCM_MESSAGE_VALUE = "message";
}
