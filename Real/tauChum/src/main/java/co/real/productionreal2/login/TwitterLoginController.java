package co.real.productionreal2.login;

//import com.twitter.sdk.android.core.Callback;
//import com.twitter.sdk.android.core.Result;
//import com.twitter.sdk.android.core.TwitterApiClient;
//import com.twitter.sdk.android.core.TwitterCore;
//import com.twitter.sdk.android.core.TwitterException;
//import com.twitter.sdk.android.core.TwitterSession;
//import com.twitter.sdk.android.core.identity.TwitterAuthClient;
//import com.twitter.sdk.android.core.identity.TwitterLoginButton;
//import com.twitter.sdk.android.core.models.User;

/**
 * Created by hohojo on 25/8/2015.
 */
//public class TwitterLoginController extends BaseLoginController {
//    private static final String TAG = "TwitterLoginController";
//    private static TwitterLoginController instance;
//    private TwitterLoginButton twitterLoginButton;
//    private Result<TwitterSession> twitterSessionResult;
//    private Result<User> twitterUserResult;
//    public static final int REQUEST_CODE = 140;
//
//    public TwitterLoginController(Context context, TwitterLoginButton twitterLoginButton) {
//        super(context);
//        this.twitterLoginButton = twitterLoginButton;
////        this.twitterLoginButton.setCallback(twitterSessionCallback);
//        this.twitterLoginButton.setCallback(new Callback<TwitterSession>() {
//            @Override
//            public void success(Result<TwitterSession> result) {
//                twitterSessionResult = result;
//                TwitterApiClient twitterApiClient = TwitterCore.getInstance().getApiClient();
//                twitterApiClient.getAccountService().verifyCredentials(false, false, new Callback<User>() {
//                    @Override
//                    public void success(Result<User> userResult) {
//                        Log.w(TAG, "User result = " + userResult);
//                        twitterUserResult = userResult;
//                        setRequestSocialLoginRegisterWithAccessToken(twitterUserResult, twitterSessionResult);
//                    }
//
//                    @Override
//                    public void failure(TwitterException e) {
//                        loginFail("twitterUserCallback fail: " + e.getMessage());
//                    }
//                });
//            }
//
//            @Override
//            public void failure(TwitterException e) {
//                loginFail("twitterSessionCallback fail: " + e.getMessage());
//            }
//        });
//    }
//
//    public static TwitterLoginController getInstance(Context context, TwitterLoginButton twitterLoginButton) {
//        if (instance == null) {
//            instance = new TwitterLoginController(context, twitterLoginButton);
//        }
//        return instance;
//    }
//
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        twitterLoginButton.onActivityResult(requestCode, resultCode, data);
////        final TwitterAuthClient twitterAuthClient = new TwitterAuthClient();
////        if (twitterAuthClient.getRequestCode() == requestCode) {
////            if (resultCode == Activity.RESULT_CANCELED)
////            twitterAuthClient.onActivityResult(requestCode, resultCode, data);
////        }
//    }
//
//
////    private Callback<TwitterSession> twitterSessionCallback = new Callback<TwitterSession>() {
////        @Override
////        public void success(Result<TwitterSession> result) {
////            twitterSessionResult = result;
////
////            TwitterApiClient twitterApiClient = TwitterCore.getInstance().getApiClient();
////            twitterApiClient.getAccountService().verifyCredentials(false, false, twitterUserCallback);
////
////        }
////
////        @Override
////        public void failure(TwitterException e) {
////            loginFail("twitterSessionCallback fail: "+e.getMessage());
////        }
////    };
//
//    private Callback<User> twitterUserCallback = new Callback<User>() {
//        @Override
//        public void success(Result<User> userResult) {
//            Log.w(TAG, "User result = " + userResult);
//            twitterUserResult = userResult;
//            setRequestSocialLoginRegisterWithAccessToken(twitterUserResult, twitterSessionResult);
////            TwitterAuthClient authClient = new TwitterAuthClient();
////            authClient.requestEmail(twitterSessionResult.data, twitterEmailCallback);
//        }
//
//        @Override
//        public void failure(TwitterException e) {
//            loginFail("twitterUserCallback fail: " + e.getMessage());
//        }
//    };
//
//    // get email
////    private Callback<String> twitterEmailCallback = new Callback<String>() {
////        @Override
////        public void success(Result<String> result) {
////            Log.w(TAG, "result = "+result);
////            Log.w(TAG, "mail = "+result.data);
////            setRequestSocialLoginRegisterWithAccessToken(twitterUserResult, twitterSessionResult, result.data);
////
////        }
////
////        @Override
////        public void failure(TwitterException e) {
////            Log.e(TAG, "email fail: "+e);
////            loginFail("twitterEmailCallback fail: " + e.getMessage());
////        }
////    };
//
//    private void setRequestSocialLoginRegisterWithAccessToken(Result<User> userResult, Result<TwitterSession> twitterSessionResult) {
//        Log.w(TAG, "twitterSessionResult = " + twitterSessionResult);
//
//        RequestSocialLogin socialRequest = new RequestSocialLogin(
//                AdminService.DEVICE_TYPE_ANDROID,
//                FileUtil.getDeviceId(context),
//                AdminService.SOCIAL_TWITTER,
//                userResult.data.name,
//                "" + twitterSessionResult.data.getUserId(),
//                userResult.data.email,
//                userResult.data.profileImageUrl,
//                twitterSessionResult.data.getAuthToken().token);
//        Log.w(TAG, "getName: " + userResult.data.name);
//        Log.w(TAG, "getId: " + twitterSessionResult.data.getUserId());
//        Log.w(TAG, "getEmail: " + userResult.data.email);
//        Log.w(TAG, "getProfilePictureUri: " + userResult.data.profileImageUrl);
//        socialRequest.callLoginSocialApi(context, socialRequest);
//    }
//
//}

public class TwitterLoginController {}
