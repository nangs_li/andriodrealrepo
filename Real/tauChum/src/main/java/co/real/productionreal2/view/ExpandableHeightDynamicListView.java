package co.real.productionreal2.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewGroup;

import com.nhaarman.listviewanimations.itemmanipulation.DynamicListView;

//https://gist.github.com/Alezhka/7926930
public class ExpandableHeightDynamicListView  extends DynamicListView  {
    public static int ANIM_INITIAL_DELAY_MILLIS = 300;
 
    boolean expanded = false;
 
    public ExpandableHeightDynamicListView(Context context) {
        super(context);
    }
 
    public ExpandableHeightDynamicListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
 
    public ExpandableHeightDynamicListView(Context context, AttributeSet attrs,
                                    int defStyle) {
        super(context, attrs, defStyle);
    }
 
    public boolean isExpanded() {
        return expanded;
    }
 
    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (isExpanded()) {
            // Calculate entire height by providing a very large height hint.
            // View.MEASURED_SIZE_MASK represents the largest height possible.
            int expandSpec = MeasureSpec.makeMeasureSpec(MEASURED_SIZE_MASK,
                    MeasureSpec.AT_MOST);
            super.onMeasure(widthMeasureSpec, expandSpec);
 
            ViewGroup.LayoutParams params = getLayoutParams();
            params.height = getMeasuredHeight();
        } else {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }
 
    public void setExpanded(boolean expanded) {
        this.expanded = expanded;
    }
    
    /**
     * http://stackoverflow.com/questions/16563947/android-scrollview-intercepts-clicks-should-not-do-that
     */
    
    @Override
    public boolean onInterceptTouchEvent(MotionEvent p_event) {
        /*if (p_event.getAction() == MotionEvent.ACTION_MOVE) {
            return true;
        }*/
        return super.onInterceptTouchEvent(p_event);
    }

  
	@Override
	public boolean performClick() {
		return super.performClick();
	}
	
	

	@Override
    public boolean onTouchEvent(MotionEvent p_event) {
		
		if (p_event.getAction() ==  MotionEvent.ACTION_UP){
//			Log.d("dynamiclistview", " MotionEvent.ACTION_UP"  );
			isDragging = false;
		} else if (p_event.getAction() ==  MotionEvent.ACTION_MOVE){
//			Log.d("dynamiclistview", " MotionEvent.ACTION_MOVE"  );

		} else if (p_event.getAction() ==  MotionEvent.ACTION_SCROLL){
//			Log.d("dynamiclistview", " MotionEvent.ACTION_SCROLL"  );
		} else {
//			Log.d("dynamiclistview",  p_event.getAction() + "" );
		} 
		
        if ( isDragging && p_event.getAction() == MotionEvent.ACTION_MOVE && getParent() != null) {
            getParent().requestDisallowInterceptTouchEvent(true);
        }
        return super.onTouchEvent(p_event);
    }

	
	private boolean isDragging = false;
	public void markDragging() {
		isDragging = true;
		
	}
}
