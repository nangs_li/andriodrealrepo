/*
 * Copyright 2014 Soichiro Kashima
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package co.real.productionreal2.newsfeed;

import android.database.MatrixCursor;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.AlphaAnimation;
import android.view.inputmethod.EditorInfo;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import co.real.productionreal2.Constants;
import co.real.productionreal2.CoreData;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.MainActivityTabBase;
import co.real.productionreal2.activity.filter.SearchFilterActivity;
import co.real.productionreal2.adapter.SearchResultAdapter;
import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.common.LocalStorageHelper;
import co.real.productionreal2.config.AppConfig;
import co.real.productionreal2.model.AgentListing;
import co.real.productionreal2.model.GoogleAddress;
import co.real.productionreal2.model.GooglePlace;
import co.real.productionreal2.model.SystemSetting;
import co.real.productionreal2.service.GoogleService;
import co.real.productionreal2.service.model.AgentProfiles;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.ApiResponseGeocode;
import co.real.productionreal2.service.model.ApiResponsePlace;
import co.real.productionreal2.service.model.request.RequestAddressSearch;
import co.real.productionreal2.service.model.request.RequestAddressSearchRetrieve;
import co.real.productionreal2.service.model.request.RequestGoogleMapGeocodeByAddress;
import co.real.productionreal2.service.model.request.RequestGoogleMapPlaceAutoComplete;
import co.real.productionreal2.service.model.request.RequestGoogleMapPlaceGeocodeByPlaceId;
import co.real.productionreal2.service.model.request.RequestListing;
import co.real.productionreal2.service.model.response.ResponseNewsFeed;
import co.real.productionreal2.setting.HistorySearchSetting;
import co.real.productionreal2.setting.SearchFilterSetting;
import co.real.productionreal2.util.AppUtil;
import co.real.productionreal2.util.GoogleGeoUtil;
import co.real.productionreal2.util.LocationUtil;
import co.real.productionreal2.util.NetworkUtil;
import co.real.productionreal2.view.TitleBar;
import retrofit.RetrofitError;
import retrofit.client.Response;
import widget.ComputeVerticalScrollOffAbleRecyclerView;
import widget.EnbleableSwipeViewPager;
import widget.InterceptableListener;
import widget.InterceptableRelativeLayout;

;


public class AddressSearchMidFragment extends BaseNewsFeedFragment implements MainActivityTabBase.SearchResultUpdateListener, SearchResultAdapter.SearchResultAdapterListener, NewsFeed_AgentProfilesAdapter.NewsFeed_AgentProfilesAdapterListener {

    private static final String TAG = "NewsFeedSearchFragment";

    @InjectView(R.id.noti_new_story)
    LinearLayout notiNewStory;
    @InjectView(R.id.btn_new_story)
    TextView tvNewStory;

    NewsFeedDetailFragment newsFeedDetailFragment;
    NewsFeedProfileFragment newsFeedProfileFragment;

    SwipeRefreshLayout mSwipeRefreshLayout;
    ComputeVerticalScrollOffAbleRecyclerView mRecyclerView;
    FrameLayout flEmptyView;
    FrameLayout flEmptyNewsFeedView;
    TextView tvEmptyResultDesc;
    LinearLayoutManager mLayoutManager;
    private NewsFeed_AgentProfilesAdapter searchAdapter;
    private HashMap<String, String> propertyTypeHashMap = new HashMap<>();

    int newsfeed_item_hight = 0;
    int newsfeed_item_header_height = 0;
    int newsfeed_item_thumbnail_height = 0;
    int newsfeed_item_bottom_height = 0;
    int newsfeed_item_shadow_height = 0;
    int newsfeed_item_end_height = 0;
    int newsfeed_icon_height;
    int newsfeed_icon_bottom_height = 0;

    int min_newsfeed_icon_height = 0;
    int my_newsfeed_icon_height = 0;
    int bottom_hight = 0;

    AlphaAnimation animationBeHide;
    AlphaAnimation animationBeVisiable;

    private RequestAddressSearchRetrieve requestAddressSearchRetrieve;

    private TitleBar mTitleBar;

    private boolean isMiniview = false;
    int topItemPostion = 0;
    ResponseNewsFeed responseGson;

    private int PAGE_NO = 1;
    private boolean hasPageEnd = false;
    private List<Integer> agentResultList = new ArrayList<>();

    private static List<AgentProfiles> agentProfilesList = new ArrayList<>();
    private List<SystemSetting.Slogan> sloganList = new ArrayList<>();
    private List<SystemSetting.PropertyType> propertyTypeList = new ArrayList<>();

    private Location mLastLocation;

    // Google client to interact with Google API
    private List<GooglePlace> googeplaceList;
    protected RequestAddressSearch addressSearch;
    private boolean isDefaultSearch = false;

    private SearchResultAdapter adapter;
    public ViewPager pager;
    private InterceptableRelativeLayout mInterceptableRelativeLayout;

    private View newsfeed_moving_cover_view1;
    private View newsfeed_moving_cover_view2;
    private View newsfeed_moving_cover_view3;

    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    public boolean isShowing = false;
    public int viewPagerScrollState = ViewPager.SCROLL_STATE_IDLE;

    private boolean IS_SUPPORT_GOOGLE_SERVICE;

    private String regionName;

    public static final String BUNDLE_NEWSFEED_TYPE = "BUNDLE_NEWSFEED_TYPE";
    public static final String EXTRA_MESSAGE = "EXTRA_MESSAGE";

    public static final AddressSearchMidFragment newInstance(String message, InterceptableRelativeLayout mInterceptableRelativeLayout, ViewPager pager, NewsFeedDetailFragment newsFeedDetailFragment, NewsFeedProfileFragment newsFeedProfileFragment, ResponseNewsFeed responseGson) {
        AddressSearchMidFragment f = new AddressSearchMidFragment();
        f.setNewsFeedDetailFragment(newsFeedDetailFragment);
        f.setNewsFeedProfileFragment(newsFeedProfileFragment);
        f.setInterceptableRelativeLayout(mInterceptableRelativeLayout);
        f.setViewPager(pager);
        f.setResponseGson(responseGson);
        Bundle bdl = new Bundle(1);
        bdl.putString(EXTRA_MESSAGE, message);
        f.setArguments(bdl);
        return f;
    }

    void setNewsFeedDetailFragment(NewsFeedDetailFragment newsFeedDetailFragment) {
        this.newsFeedDetailFragment = newsFeedDetailFragment;
    }

    void setNewsFeedProfileFragment(NewsFeedProfileFragment newsFeedProfileFragment) {
        this.newsFeedProfileFragment = newsFeedProfileFragment;
    }

    void setInterceptableRelativeLayout(InterceptableRelativeLayout view) {
        mInterceptableRelativeLayout = view;
    }

    void setViewPager(ViewPager pager) {
        this.pager = pager;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
//        String message = getArguments().getString(EXTRA_MESSAGE);

        animationBeHide = new AlphaAnimation(1.0f, 0.0f);
        animationBeHide.setDuration(500);
        animationBeHide.setStartOffset(0);

        animationBeVisiable = new AlphaAnimation(0.0f, 1.0f);
        animationBeVisiable.setDuration(500);
        animationBeVisiable.setStartOffset(0);

        View view = inflater.inflate(R.layout.fragment_newsfeed_search, container, false);

        view.setPadding(0, MainActivityTabBase.tabHeight, 0, 0);

        context = this.getContext();
        userContent = CoreData.getUserContent(context);
        propertyTypeHashMap = AppUtil.getPropertyTypeHashMap(getActivity(), userContent.systemSetting.propertyTypeList);

        ButterKnife.inject(this, view);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        ((MainActivityTabBase) getActivity()).searchResultUpdateListener = this;

        //init title bar
        initTitlebar();

        //initData
        initData();

        newsfeed_item_header_height = getResources().getDimensionPixelSize(R.dimen.newsfeed_item_header_height);
        newsfeed_item_thumbnail_height = getResources().getDimensionPixelSize(R.dimen.newsfeed_item_thumbnail_height);
        newsfeed_item_bottom_height = getResources().getDimensionPixelSize(R.dimen.newsfeed_item_bottom_height);
        newsfeed_item_shadow_height = getResources().getDimensionPixelSize(R.dimen.newsfeed_item_shadow_height);
        newsfeed_icon_height = getResources().getDimensionPixelSize(R.dimen.newsfeed_icon_height);
        newsfeed_icon_bottom_height = getResources().getDimensionPixelSize(R.dimen.newsfeed_icon_bottom_height);

        min_newsfeed_icon_height = (int) (newsfeed_icon_height / 2);
        bottom_hight = newsfeed_icon_height + newsfeed_item_shadow_height;

        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.newsfeed_swipe_refresh_layout);
        mRecyclerView = (ComputeVerticalScrollOffAbleRecyclerView) view.findViewById(R.id.recycler_view);
        flEmptyView = (FrameLayout) view.findViewById(R.id.fl_empty_view);
        flEmptyNewsFeedView = (FrameLayout) view.findViewById(R.id.fl_empty_newsfeed_view);
        tvEmptyResultDesc = (TextView) view.findViewById(R.id.tv_empty_result_desc);

        newsfeed_moving_cover_view1 = view.findViewById(R.id.newsfeed_moving_cover_view1);
        newsfeed_moving_cover_view2 = view.findViewById(R.id.newsfeed_moving_cover_view2);
        newsfeed_moving_cover_view3 = view.findViewById(R.id.newsfeed_moving_cover_view3);

        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this.getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                View itemView = mLayoutManager.getChildAt(0);
                myOnScrollStateChanged(itemView, newState);

                View itemView2 = mLayoutManager.getChildAt(1);
                if (itemView2 != null) {
                    myOnScrollStateChanged(itemView2, newState);
                }
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    topItemPostion = mLayoutManager.findFirstVisibleItemPosition();
                    preLoadDetailImage(topItemPostion);
                    int intNextPostion = topItemPostion + 1;
                    if (mLayoutManager.getItemCount() > 0) {
                        if (intNextPostion < mLayoutManager.getItemCount() - 1) {
                            preLoadDetailImage(intNextPostion);
                        }
                    }
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dx == 0 && dy == 0) {
                    return;
                }
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (!hasPageEnd) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
//                            loading = false;
                            Log.v("...", "Last Item Wow !" + PAGE_NO++);
                            Log.v("...", "Last Item Wow !" + getCurrentSection().name());
                            //Do pagination.. i.e. fetch new data
                            loadMoreSearchResult();
                        }
                    }
                }
                updateNormalViewListDisplay();
            }
        });

        mSwipeRefreshLayout.setColorSchemeResources(R.color.newsfeed_text_nuber_blue);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                agentProfilesList = new ArrayList<>();
                PAGE_NO = 1;
                hasPageEnd = false;
                isDefaultSearch = true;

                //refresh
                refreshResult();
            }
        });


        loadData();


    }

    private void refreshResult() {

        RequestAddressSearch requestAddressSearch = LocalStorageHelper.getCurrentAddressSearch(context);
        if (requestAddressSearch == null) {
            stopRefresh();
            return;
        }
        final String placeId = requestAddressSearch.googleAddress.address.placeId;
        if (IS_SUPPORT_GOOGLE_SERVICE) {
            final GoogleService weatherService =
                    new GoogleService(context);
            weatherService.getGeocodeService().getAddressPackGeocodeRequest(
                    placeId, GoogleService.API_KEY,
                    new retrofit.Callback<ApiResponseGeocode>() {
                        @Override
                        public void failure(RetrofitError error) {
                            Log.e("result", error.toString());
                        }

                        @Override
                        public void success(final ApiResponseGeocode apiResponse, Response response) {
                            Log.d(TAG, "Google service API 2" + placeId.toString());
                            processGeocodeByAddressResult(apiResponse);
                        }
                    }
            );
        } else {
            RequestGoogleMapGeocodeByAddress requestGoogleMapGeocodeByAddress = new RequestGoogleMapGeocodeByAddress(userContent.memId, LocalStorageHelper.getAccessToken(getContext()), AppUtil.getCurrentLangCode(context), mTitleBar.getEtSearchField().getText().toString());
            requestGoogleMapGeocodeByAddress.callGoogleMapPlaceAPIGeocodeBySearchAddress(getActivity(), new ApiCallback() {
                @Override
                public void success(ApiResponse apiResponse) {
                    try {
                        JSONObject jsonObject = new JSONObject(apiResponse.jsonContent.toString());
                        ApiResponseGeocode responseGson = new Gson().fromJson(jsonObject.optString("GoogleResult"), ApiResponseGeocode.class);
                        processGeocodeByAddressResult(responseGson);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void failure(String errorMsg) {
                    Log.e("result", errorMsg.toString());
                }
            });
        }
    }


    private void initData() {
        ArrayList<RequestAddressSearch> googlePlaceList = LocalStorageHelper.getLatestSearchResultList(getActivity());
        if (googlePlaceList.size() > 0)
            HistorySearchSetting.getInstance().setLatestFiveAddressSearchList(googlePlaceList);

        Log.d(TAG, "initData: " + googlePlaceList.size());
        IS_SUPPORT_GOOGLE_SERVICE = LocationUtil.isSupportedGoogleMap(context);
    }

    private void initTitlebar() {
        mTitleBar = ((MainActivityTabBase) getActivity()).getTitleBar();

        //used in newsfeed section only

        ArrayList<RequestAddressSearch> googlePlaceList = LocalStorageHelper.getLatestSearchResultList(getActivity());
        if (googlePlaceList.size() > 0) {
            mTitleBar.getEtSearchField().setText(LocalStorageHelper.getLatestAddressSearch(context));
        }
        mTitleBar.setResultCount(LocalStorageHelper.defaultHelper(getActivity()).getLatestSearchCount());
        mTitleBar.getEtSearchField().addTextChangedListener(new SearchWatcher());
        //add filter btn click event
        mTitleBar.getFilterBtn().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //show search filter fragment
                SearchFilterActivity.start(getActivity());
            }
        });

        mTitleBar.getLlSearchSection().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTitleBar.getEtSearchField().requestFocus();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    mTitleBar.getEtSearchField().setShowSoftInputOnFocus(true);
                }
            }
        });

        mTitleBar.getEtSearchField().setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    mTitleBar.getEtSearchField().clearFocus();
//                        performSearch(mTitleBar.getEtSearchField().getText().toString(), true);
//                        ((MainActivityTabBase) getActivity()).performSearch();
                    //clear previous search result
                    agentProfilesList.clear();
                    notifyDataSetChanged();

                    isDefaultSearch = true;
                    callSuggestGeoPlaceApi(mTitleBar.getEtSearchField().getText().toString());
                    return true;
                }
                return false;
            }
        });


        mTitleBar.getEtSearchField().setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                Log.d(TAG, "onFocus: " + v + " " + hasFocus);
                if (getActivity() == null)
                    return;
                if (hasFocus) {
                    String top5Search = "";
                    // Construct the data source
                    ArrayList<RequestAddressSearch> googlePlaceList = LocalStorageHelper.getLatestSearchResultList(getActivity());
                    if (googlePlaceList.size() == 0) {
                        //show the suggestion from API
                        ((MainActivityTabBase) getActivity()).placesList = new ArrayList<Object>();
                        ((MainActivityTabBase) getActivity()).placesList.addAll((ArrayList) userContent.systemSetting.topFiveCitiesList);
                        for (SystemSetting.TopFiveCities city : userContent.systemSetting.topFiveCitiesList) {
                            top5Search += city.cityName + " | ";
                        }
                    } else {
                        //show the search history
                        ((MainActivityTabBase) getActivity()).placesList = new ArrayList<Object>();
                        ((MainActivityTabBase) getActivity()).placesList.addAll(googlePlaceList);
                        for (RequestAddressSearch city : googlePlaceList) {
                            top5Search += city.inputAddress + " | ";
                        }
                    }
                    // Mixpanel
                    MixpanelAPI mixpanel = MixpanelAPI.getInstance(getActivity(), AppConfig.getMixpanelToken());
                    mixpanel.getPeople().set("Top 5 search", top5Search);
                    ((MainActivityTabBase) getActivity()).searchResultAdapter = new SearchResultAdapter(context, ((MainActivityTabBase) getActivity()).placesList, AddressSearchMidFragment.this);
                }
                ((MainActivityTabBase) getActivity()).showResultSuggestion(hasFocus);
            }
        });


    }

    @OnClick(R.id.btn_new_story)
    public void fetchNewStory() {

        Log.d("NewsfeedSearchFragment", "NewsfeedSearchFragment: fetchNewStory: " + " " + " " + PAGE_NO + " " + loading);
        agentProfilesList = new ArrayList<>();
        PAGE_NO = 1;
        isDefaultSearch = true;
        hasPageEnd = false;
        callApi(PAGE_NO);

        //fade out button
        tvNewStory.setVisibility(View.GONE);
//        Animation a = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out);
//        a.reset();
//        a.setDuration(2000);
//        notiNewStory.clearAnimation();
//        notiNewStory.startAnimation(a);
    }

    private void callApi(int pageNo) {
        Log.d("NewsfeedSearchFragment", "callApi" + hasPageEnd + " " + " " + " " + pageNo + " " + loading);
        Log.d("NewsfeedSearchFragment", "NewsfeedSearchFragment: fetchAPI: " + hasPageEnd + " " + " " + pageNo + " " + loading);

        //search section
        Log.d("NewsfeedSearchFragment", "getLatestSearchResultList: " + " " + LocalStorageHelper.getLatestSearchResultList(getActivity()).size() + " " + LocationUtil.getCountryIsoBySIM(getActivity()));
        if (LocalStorageHelper.getLatestSearchResultList(getActivity()).size() == 0) {
            //get data by the current region
            performSearch(LocationUtil.getCountryIsoBySIM(getActivity()), true);
        } else {
            //refresh current list
//                ((MainActivityTabBase) getActivity()).performSearch();
//                callGeoCodeApi(mTitleBar.getEtSearchField().getText().toString(), 0);
            callSuggestGeoPlaceApi(mTitleBar.getEtSearchField().getText().toString());

        }

    }


    private void myOnScrollStateChanged(View itemView, int newState) {
        // Hide the tv_bubble when is dragging
//        if (itemView != null) {
//
//            TextView tv_bubble = (TextView) itemView.findViewById(R.id.tv_bubble);
//            if (tv_bubble != null) {
//                if (newState == RecyclerView.SCROLL_STATE_DRAGGING) {
//                    tv_bubble.startAnimation(animationBeHide);
//                    tv_bubble.setVisibility(View.GONE);
//                } else if (newState == RecyclerView.SCROLL_STATE_IDLE) {
//                    tv_bubble.startAnimation(animationBeVisiable);
//                    tv_bubble.setVisibility(View.VISIBLE);
//                } else {
//                }
//            }
//        }
    }

    private void resetItem(int postion) {
        try {
            View itemView = mLayoutManager.getChildAt(postion);
            RelativeLayout rl_headerBar = (RelativeLayout) itemView.findViewById(R.id.rl_headerBar);
            ImageView img_icon = (ImageView) itemView.findViewById(R.id.img_icon);
            rl_headerBar.setTranslationY(0);

            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) img_icon.getLayoutParams();
            params.width = (int) (newsfeed_icon_height * 1f);
            params.height = (int) (newsfeed_icon_height * 1f);

            img_icon.setLayoutParams(params);
            img_icon.setTranslationY(0);

        } catch (Exception e) {
        }

    }

    private void updateNormalViewListDisplay() {
        if (!(getCurrentSection() == MainActivityTabBase.CURRENT_SECTION.NEWSFEED && isMiniview)) {
            my_newsfeed_icon_height = newsfeed_icon_height;
            topItemPostion = mLayoutManager.findFirstVisibleItemPosition();
            int moveY = mRecyclerView.computeVerticalScrollOffset() - newsfeed_item_hight * topItemPostion;

            View itemView = mLayoutManager.getChildAt(0);
            RelativeLayout rl_headerBar = (RelativeLayout) itemView.findViewById(R.id.rl_headerBar);
            ImageView img_icon = (ImageView) itemView.findViewById(R.id.img_icon);

            if (newsfeed_item_hight - moveY > newsfeed_item_end_height) {
                rl_headerBar.setTranslationY(moveY);
            } else {
                rl_headerBar.setTranslationY(newsfeed_item_hight - newsfeed_item_end_height);
                my_newsfeed_icon_height = Math.max(min_newsfeed_icon_height, Math.min(newsfeed_icon_height, (newsfeed_item_hight - newsfeed_icon_bottom_height - min_newsfeed_icon_height - moveY)));
            }

            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) img_icon.getLayoutParams();
            params.width = my_newsfeed_icon_height;
            params.height = my_newsfeed_icon_height;
            img_icon.setLayoutParams(params);

            if (newsfeed_item_hight - moveY > bottom_hight) {
                img_icon.setTranslationY(moveY);
            } else {
                img_icon.setTranslationY(newsfeed_item_hight - bottom_hight);
            }

            resetItem(1);
        }
    }

    private void loadMoreSearchResult() {

        searchAdapter.showLoader(true);
        searchAdapter.notifyDataSetChanged();

        List<Integer> loadResultList = new ArrayList<>();
        if (agentResultList.size() > 20) {
            loadResultList = agentResultList.subList(0, 19);
        } else {
            loadResultList = agentResultList;
        }

        agentProfilesList = LocalStorageHelper.getLocalAgentProfileList(context, getCurrentSection());

        Log.d(TAG, "loadResultList = " + loadResultList.size());

        if (loadResultList.size() == 0) {
            hasPageEnd = true;
            searchAdapter.showLoader(false);
            searchAdapter.notifyDataSetChanged();
            return;
        } else {
            hasPageEnd = false;
        }

        if (requestAddressSearchRetrieve == null) {
            requestAddressSearchRetrieve = new RequestAddressSearchRetrieve(userContent.memId, LocalStorageHelper.getAccessToken(getContext()), AppUtil.getCurrentLangIndex(getActivity()), loadResultList);
            requestAddressSearchRetrieve.callAddressSearchRetrieveAgentProfile(getActivity(), new ApiCallback() {

                        @Override
                        public void success(ApiResponse apiResponse) {
                            //parse Json to Object
                            ResponseNewsFeed responseGson = new Gson().fromJson(apiResponse.jsonContent, ResponseNewsFeed.class);
                            int recordCount = responseGson.content.recordCount;
                            Log.d(TAG, "googleAddressListJSON = " + apiResponse.jsonContent.toString());
                            if (agentResultList.size() >= recordCount) {
                                for (int i = (recordCount - 1); i >= 0; i--)
                                    agentResultList.remove(i);
                            }

                            Log.d(TAG, "loadMoreSearchResult: resultset: " + agentResultList);

                            if (responseGson != null)
                                agentProfilesList.addAll(responseGson.content.agentProfiles);

                            if (agentResultList.size() == 0) {
                                //rnd of the list
                                hasPageEnd = true;
                                searchAdapter.showLoader(false);
                                searchAdapter.notifyDataSetChanged();
                            } else {
                                hasPageEnd = false;
                                searchAdapter.showLoader(true);
                                searchAdapter.notifyDataSetChanged();
                            }
                            LocalStorageHelper.saveLocalAgentProfileListToLocalStorage(getActivity(), agentProfilesList, getCurrentSection());
                            searchAdapter.setAgentProfilesList(agentProfilesList);
                            searchAdapter.notifyDataSetChanged();

                            requestAddressSearchRetrieve=null;

                        }

                        @Override
                        public void failure(String errorMsg) {
                            requestAddressSearchRetrieve=null;
                            Log.e(TAG, "addressSearch error = " + errorMsg);
                        }

                    }

            );
        }

    }

    private void handleSearchResult(ApiResponse mApiResponse) {
        Log.d(TAG, "handleSearchResult responseGson: " + mApiResponse.jsonContent);

        if (mApiResponse != null) {
            ResponseNewsFeed responseGson = new Gson().fromJson(mApiResponse.jsonContent, ResponseNewsFeed.class);
            if (responseGson != null) {
                int totalCount = ((ResponseNewsFeed) responseGson).content.totalCount;
                //display empty view
                flEmptyView.setVisibility(totalCount > 0 ? View.GONE : View.VISIBLE);

                agentResultList = ((ResponseNewsFeed) responseGson).content.fullResultSet;
                Log.d(TAG, "handleSearchResult: resultset size: " + agentResultList.size());
                if (agentResultList.size() > 19) {
                    for (int i = 19; i >= 0; i--)
                        Log.d(TAG, "handleSearchResult: resultset: removeItem: " + agentResultList.remove(i));
                } else if (agentResultList.size() > 0 && agentProfilesList.size() <= 19) {
                    //remove the items if the list size is >1 && <19
                    for (int i = agentResultList.size() - 1; i >= 0; i--)
                        Log.d(TAG, "handleSearchResult: resultset: removeItem: " + agentResultList.remove(i));
                }
                Log.d(TAG, "handleSearchResult: resultset List: " + agentResultList);

                //cache latest search result
//                if (responseGson.content.totalCount > 0)
                LocalStorageHelper.saveLocalAgentProfileListToLocalStorage(context, responseGson.content.agentProfiles, getCurrentSection());

                updateView(responseGson, hasPageEnd);
            }
        }
        isDefaultSearch = false;
    }


    public void updateView(ResponseNewsFeed responseGson, boolean mHasPageEnd) {
        //update pageEnd status
        hasPageEnd = mHasPageEnd;

        this.responseGson = responseGson;


        Log.d(TAG, "updateView: result: " + responseGson + " " + getCurrentSection() + " " + LocalStorageHelper.getLocalAgentProfileList(getActivity(), true));

        if (responseGson != null && getCurrentSection() == MainActivityTabBase.CURRENT_SECTION.ADDRESS_SEARCH) {
            agentProfilesList = responseGson.content.agentProfiles;
        } else {
            agentProfilesList = LocalStorageHelper.getLocalAgentProfileList(getActivity(), getCurrentSection());
        }

        Log.d(TAG, "updateView: resultset: " + agentProfilesList.size());

        if (agentProfilesList == null)
            return;

        //show empty message
        flEmptyView.setVisibility(agentProfilesList.size() > 0 ? View.GONE : View.VISIBLE);


        //to init the Adapter in the beginning ONLY
        if (!mHasPageEnd || searchAdapter == null || agentProfilesList.size() == 0) {
            searchAdapter = new NewsFeed_AgentProfilesAdapter(context, agentProfilesList, userContent.systemSetting.sloganList, userContent.systemSetting.propertyTypeList, propertyTypeHashMap, this) {
                @Override
                public void updateItem(AgentProfiles agentProfilesItem, int position, String imageName, String propertyTypeName) {
                    newsFeedProfileFragment.update(agentProfilesItem, position);
                    newsFeedDetailFragment.update(agentProfilesItem, position, imageName, propertyTypeName);
//                NewsFeedFragment.agentProfile = agentProfilesItem;
                    LocalStorageHelper.saveAgentProfilesToLocalStorage(context, agentProfilesItem, getCurrentSection());
                    Log.d(TAG, "updateView: " + agentProfilesItem.toString());
                }

                @Override
                public void goToProfileFragment(AgentProfiles agentProfilesItem) {
                    LocalStorageHelper.saveAgentProfilesToLocalStorage(context, agentProfilesItem, getCurrentSection());
                    pager.setCurrentItem(2, true);
                }

                @Override
                public void goToDetailFragment(AgentProfiles agentProfilesItem) {
                    LocalStorageHelper.saveAgentProfilesToLocalStorage(context, agentProfilesItem, getCurrentSection());
                    pager.setCurrentItem(0, true);
                }
            };
//                searchAdapter.setType(newsFeedType, isMiniview);
            mRecyclerView.setAdapter(searchAdapter);
        } else {
            searchAdapter.notifyDataSetChanged();
        }
        final ViewTreeObserver vto = mRecyclerView.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                if (mLayoutManager != null && mLayoutManager.getChildAt(0) != null) {
                    newsfeed_item_hight = mLayoutManager.getChildAt(0).getHeight();
                    newsfeed_item_end_height = newsfeed_item_header_height + newsfeed_item_bottom_height + newsfeed_item_shadow_height;

//                    int[] locations = new int[2];
//                    mRecyclerView.getTop().getLocationInWindow(locations);
                    if (vto.isAlive()) {
                        // Unregister the listener to only call scrollToPosition once
                        vto.removeGlobalOnLayoutListener(this);
                        // Use vto.removeOnGlobalLayoutListener(this) on API16+ devices as
                        // removeGlobalOnLayoutListener is deprecated.
                        // They do the same thing, just a rename so your choice.
                    }

                    mInterceptableRelativeLayout.setInterceptableListener(new InterceptableListener() {

                        @Override
                        public boolean onInterceptTouchEvent(MotionEvent motionEvent) {

                            if (isShowing && viewPagerScrollState == ViewPager.SCROLL_STATE_IDLE) {
//                                Log.v("", "edwin mInterceptableRelativeLayout onInterceptTouchEvent " + motionEvent.getAction() + " x " + motionEvent.getX() + " y " + (motionEvent.getY() - MainActivityTabBase.tabHeight));
                                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                                    View child = mRecyclerView.findChildViewUnder(motionEvent.getX(), (motionEvent.getY() - MainActivityTabBase.tabHeight));
                                    try {
                                        if (child != null) {
                                            ((EnbleableSwipeViewPager) pager).setSwipeable(true);
//                                            int idx = mRecyclerView.getChildPosition(child);
//                                            Log.d("", "edwin cover onInterceptTouchEvent idx " + idx + " " + (viewLocation[1]) + " " + coverLocation[1] + " " + MainActivityTabBase.tabHeight + " " + MainActivityTabBase.titleHeight + " " + mRecyclerView.getTop() + " " + newsfeed_item_hight + " " + child.getTop());
                                            int child_Top = child.getTop();

                                            newsfeed_moving_cover_view1.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (child_Top < 0 ? 0 : child_Top)));
                                            newsfeed_moving_cover_view2.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, searchAdapter.isMiniview ? searchAdapter.getMinivewHeight() : newsfeed_item_hight + (child_Top > 0 ? 0 : child_Top)));
                                        }
                                    } catch (Exception e) {

                                    }

                                }
                            }
                            return false;
                        }

                        @Override
                        public boolean onTouchEvent(MotionEvent motionEvent) {

//                            Log.v("","edwin mInterceptableRelativeLayout onTouchEvent "+motionEvent.getAction());
                            if (motionEvent.getAction() == MotionEvent.ACTION_UP || motionEvent.getAction() == MotionEvent.ACTION_CANCEL) {
                                View child = mRecyclerView.findChildViewUnder(motionEvent.getX(), motionEvent.getY());

                                try {
                                    child.setAlpha(1f);
                                } catch (Exception e) {

                                }
                                ((EnbleableSwipeViewPager) pager).setSwipeable(false);
                            }
                            return false;
                        }
                    });

                }
            }
        });

        stopRefresh();

        for (int i = 0; i < agentProfilesList.size(); i++) {
            if (i == 2) {
                break;
            }
            preLoadDetailImage(i);
        }
        for (int i = 1; i < agentProfilesList.size(); i++) {
            AgentProfiles agentProfilesItem = agentProfilesList.get(i);
            Picasso.with(getActivity()).load(agentProfilesItem.agentPhotoURL).fetch();
            if (agentProfilesItem.agentListing.photos.size() > 0)
                Picasso.with(getActivity()).load(agentProfilesItem.agentListing.photos.get(0).url).fetch();
        }
    }

    public void setCoverViewAlpha(float alpha) {
        if (newsfeed_moving_cover_view1 != null) {
            newsfeed_moving_cover_view1.setAlpha(alpha);
            newsfeed_moving_cover_view3.setAlpha(alpha);
        }
    }

    void preLoadDetailImage(int i) {
        if (responseGson != null && i > 0 && i < responseGson.content.agentProfiles.size()) {
            AgentProfiles agentProfilesItem = responseGson.content.agentProfiles.get(i);
            Picasso.with(getActivity()).load(agentProfilesItem.agentPhotoURL).fetch();
            if (agentProfilesItem.agentListing.photos.size() > 0)
                Picasso.with(getActivity()).load(agentProfilesItem.agentListing.photos.get(0).url).fetch();

            for (RequestListing.Reason reason : agentProfilesItem.agentListing.reasons) {

                if (reason.mediaURL1 != null &&
                        !reason.mediaURL1.isEmpty()) {
                    Picasso.with(getActivity()).load(reason.mediaURL1).fetch();
                }

                if (reason.mediaURL2 != null &&
                        !reason.mediaURL2.isEmpty()) {
                    Picasso.with(getActivity()).load(reason.mediaURL2).fetch();
                }

                if (reason.mediaURL3 != null &&
                        !reason.mediaURL3.isEmpty()) {
                    Picasso.with(getActivity()).load(reason.mediaURL3).fetch();
                }
            }

            for (int photoIndex = 1; photoIndex < agentProfilesItem.agentListing.photos.size(); photoIndex++) {
                AgentListing.Photo photo = agentProfilesItem.agentListing.photos.get(photoIndex);
                Picasso.with(getActivity()).load(photo.url).fetch();
            }
        }
    }

    public void stopRefresh() {
        mSwipeRefreshLayout.setRefreshing(false);

        //hide loading
        ((MainActivityTabBase) getActivity()).hideLoadingDialog();
    }

    @Override
    public void fetchResult(String currentPlaceName) {
//        callGeoCodeApi(currentPlaceName, 0);
        agentProfilesList = new ArrayList<>();
        PAGE_NO = 1;
        isDefaultSearch = true;
        refreshResult();

    }

    @Override
    public void updateNewsfeed() {
        List<AgentProfiles> agentProfilesList = LocalStorageHelper.getLocalAgentProfileList(context, getCurrentSection());

        Log.d(TAG, "updateNewsfeed: child2:  " + getCurrentSection().name() + " " + searchAdapter);

        if (agentProfilesList != null && searchAdapter != null) {
            searchAdapter.setAgentProfilesList(agentProfilesList);
        }

        if (searchAdapter == null) {
            searchAdapter = new NewsFeed_AgentProfilesAdapter(context, agentProfilesList, userContent.systemSetting.sloganList, userContent.systemSetting.propertyTypeList, propertyTypeHashMap, this) {
                @Override
                public void updateItem(AgentProfiles agentProfilesItem, int position, String imageName, String propertyTypeName) {
                    newsFeedProfileFragment.update(agentProfilesItem, position);
                    newsFeedDetailFragment.update(agentProfilesItem, position, imageName, propertyTypeName);
//                NewsFeedFragment.agentProfile = agentProfilesItem;
                    LocalStorageHelper.saveAgentProfilesToLocalStorage(context, agentProfilesItem, getCurrentSection());
                    Log.d(TAG, "updateView: " + agentProfilesItem.toString());
                }

                @Override
                public void goToProfileFragment(AgentProfiles agentProfilesItem) {
                    LocalStorageHelper.saveAgentProfilesToLocalStorage(context, agentProfilesItem, getCurrentSection());
                    pager.setCurrentItem(2, true);
                }

                @Override
                public void goToDetailFragment(AgentProfiles agentProfilesItem) {
                    LocalStorageHelper.saveAgentProfilesToLocalStorage(context, agentProfilesItem, getCurrentSection());
                    pager.setCurrentItem(0, true);
                }
            };
        } else {
            notifyDataSetChanged();
        }

        if (agentProfilesList != null)
            Log.d(TAG, "updateNewsfeed: child2: toString:  " + agentProfilesList.toString());


//        flEmptyNewsFeedView.setVisibility(View.GONE);
//        flEmptyView.setVisibility(agentProfilesList.size() > 0 ? View.GONE : View.VISIBLE);

    }


    /**
     * handle the agent follower count change
     */
    public void updateAgentList(int memId, int newFollwerCount) {
        Log.d(TAG, "updateAgentList " + " " + newFollwerCount);
        int i = 0;
        for (AgentProfiles agentProfiles : agentProfilesList) {
            if (agentProfiles.memberID == memId) {
                agentProfilesList.get(i).followerCount = newFollwerCount;
                searchAdapter.notifyDataSetChanged();
            }
            i++;
        }
    }


    public void loadData() {
        Log.d(TAG, "onResume " + searchAdapter);
        Log.d(TAG, "onResume " + agentProfilesList.size() + " " + getCurrentSection());
        agentProfilesList = LocalStorageHelper.getLocalAgentProfileList(getActivity(), getCurrentSection());
        if (agentProfilesList.size() > 0) {
            updateView(responseGson, hasPageEnd);
        } else {
            callApi(PAGE_NO);
        }
        if (getCurrentSection() == MainActivityTabBase.CURRENT_SECTION.NEWSFEED)
            flEmptyNewsFeedView.setVisibility(agentProfilesList.size() > 0 ? View.GONE : View.VISIBLE);
//        else
//            flEmptyView.setVisibility(agentProfilesList.size() > 0 ? View.GONE : View.VISIBLE);
    }

    public void notifyDataSetChanged() {
        Log.d(TAG, "notifyDataSetChanged " + " " + searchAdapter);
        List<AgentProfiles> agentProfilesList = LocalStorageHelper.getLocalAgentProfileList(context, getCurrentSection());
        Log.d(TAG, "notifyDataSetChanged  " + getCurrentSection().name() + " " + searchAdapter);
        if (agentProfilesList != null && searchAdapter != null) {
            searchAdapter.setAgentProfilesList(agentProfilesList);
        }
        if (searchAdapter != null) {
            searchAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void selectPlace(Object mPlace) {
        String currentPlaceName = "";
        String placeId = "";
        isDefaultSearch = false;
        if (mPlace instanceof SystemSetting.TopFiveCities) {
            currentPlaceName = ((SystemSetting.TopFiveCities) mPlace).cityName;
            placeId = ((SystemSetting.TopFiveCities) mPlace).placeId;
        } else if (mPlace instanceof RequestAddressSearch) {
            currentPlaceName = ((RequestAddressSearch) mPlace).inputAddress;
            placeId = ((RequestAddressSearch) mPlace).googleAddress.address.placeId;
        } else if (mPlace instanceof GooglePlace) {
            currentPlaceName = ((GooglePlace) mPlace).description;
            placeId = ((GooglePlace) mPlace).placeId;
        }

        //set search bar text with the corresponding selection
        mTitleBar.getEtSearchField().setText(currentPlaceName);


        callGeoCodeApi(placeId, -1);
        ((MainActivityTabBase) getActivity()).updateSearchView(currentPlaceName);
    }

    private void callGeoCodeApi(final String placeId, int position) {

        if (position == 0) {
            //set title address input field
            if (googeplaceList != null && googeplaceList.size() > 0 && LocalStorageHelper.getCurrentAddressSearch(context) != null) {
                mTitleBar.getEtSearchField().setText(googeplaceList.get(0).description);
            }
        }

        if (IS_SUPPORT_GOOGLE_SERVICE) {
            final GoogleService weatherService =
                    new GoogleService(context);
            weatherService.getGeocodeService().getAddressPackGeocodeRequest(
                    placeId, GoogleService.API_KEY,
                    new retrofit.Callback<ApiResponseGeocode>() {
                        @Override
                        public void failure(RetrofitError error) {
                            Log.e("result", error.toString());
                            NetworkUtil.showNoNetworkMsg(context);
                        }

                        @Override
                        public void success(final ApiResponseGeocode apiResponse, Response response) {
                            Log.d(TAG, "Google service API 2" + placeId.toString());
                            processGeocodeByAddressResult(apiResponse);
                        }
                    }
            );
        } else {
            RequestGoogleMapGeocodeByAddress requestGoogleMapGeocodeByAddress = new RequestGoogleMapGeocodeByAddress(userContent.memId, LocalStorageHelper.getAccessToken(getContext()), AppUtil.getCurrentLangCode(context), mTitleBar.getEtSearchField().getText().toString());
            requestGoogleMapGeocodeByAddress.callGoogleMapPlaceAPIGeocodeBySearchAddress(getActivity(), new ApiCallback() {
                @Override
                public void success(ApiResponse apiResponse) {
                    try {
                        JSONObject jsonObject = new JSONObject(apiResponse.jsonContent.toString());
                        ApiResponseGeocode responseGson = new Gson().fromJson(jsonObject.optString("GoogleResult"), ApiResponseGeocode.class);
                        processGeocodeByAddressResult(responseGson);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void failure(String errorMsg) {
                    Log.e("result", errorMsg.toString());
                }
            });
        }
    }

    private void processGeocodeByAddressResult(final ApiResponseGeocode apiResponse) {
        Log.d(TAG, "processGeocodeByAddressResult : " + apiResponse.status.equals("OK") + " " + apiResponse.googleAddress.size());
        if (apiResponse.status.equals("OK") && apiResponse.googleAddress.size() > 0) {
            callPlaceApi(apiResponse.googleAddress.get(0));
        } else {
            //do nothing
//            Dialog.noReturnAddressDialog(getActivity()).show();
        }
    }

    public void setResponseGson(ResponseNewsFeed responseGson) {
        this.responseGson = responseGson;
    }

    @Override
    public ArrayList<AgentProfiles> getAgentProfileList() {
        return (ArrayList) LocalStorageHelper.getLocalAgentProfileList(getActivity(), getCurrentSection());
    }


    /**
     * Responsible for handling changes in search edit text.
     */
    private class SearchWatcher implements TextWatcher {

        private String s;
        private long after;
        private Thread t;
        private Runnable runnable_EditTextWatcher = new Runnable() {
            @Override
            public void run() {
                while (true) {
                    if ((System.currentTimeMillis() - after) > 600) {
                        Log.d(TAG, "(System.currentTimeMillis()-after)>600 ->  " + (System.currentTimeMillis() - after) + " > " + s);
                        // get suggest places list
                        if (mTitleBar.getEtSearchField().getText().length() > 0 && isDefaultSearch == false) {
                            performSearch(mTitleBar.getEtSearchField().getText().toString(), false);
                        }
                        t = null;
                        break;
                    }
                }
            }
        };

        @Override
        public void beforeTextChanged(CharSequence c, int i, int i2, int i3) {

        }

        @Override
        public void onTextChanged(CharSequence c, int i, int i2, int i3) {
            s = c.toString();
        }

        @Override
        public void afterTextChanged(final Editable editable) {
            Log.d(TAG, "afterTextChanged" + editable.toString());
            after = System.currentTimeMillis();
            if (t == null) {
                t = new Thread(runnable_EditTextWatcher);
                t.start();
            }
        }


    }

    public void performSearch(String regionName, boolean doSearch) {
        isDefaultSearch = doSearch;
        //do search
        callSuggestGeoPlaceApi(regionName);
    }


    private void callSuggestGeoPlaceApi(final String search) {
        if (IS_SUPPORT_GOOGLE_SERVICE) {
            final GoogleService weatherService = new GoogleService(context);
            weatherService.getGeocodeService().geocodeRequest(search, 20000000,
                    GoogleService.API_KEY,

                    new retrofit.Callback<ApiResponsePlace>() {
                        @Override
                        public void success(ApiResponsePlace apiResponse,
                                            Response response) {
                            Log.d(TAG, "Google service API 1" + apiResponse.googlePlaces.toString());
                            processSuggestionPlaceResults(apiResponse, search);
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            Log.e("result", error.toString());
                            NetworkUtil.showNoNetworkMsg(context);
                        }
                    }
            );
        } else {
            RequestGoogleMapPlaceAutoComplete requestGoogleMapPlaceAutoComplete = new RequestGoogleMapPlaceAutoComplete(userContent.memId, LocalStorageHelper.getAccessToken(getContext()), search);
            requestGoogleMapPlaceAutoComplete.callGoogleMapPlaceAPIAutoComplete(getActivity(), new ApiCallback() {
                @Override
                public void success(ApiResponse apiResponse) {
                    try {
                        JSONObject jsonObject = new JSONObject(apiResponse.jsonContent.toString());
                        ApiResponsePlace responseGson = new Gson().fromJson(jsonObject.optString("GoogleResult"), ApiResponsePlace.class);
                        processSuggestionPlaceResults(responseGson, search);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void failure(String errorMsg) {
                    Log.e("result", errorMsg.toString());
                }
            });
        }
    }

    private void processSuggestionPlaceResults(ApiResponsePlace apiResponse, final String search) {
        if ("OK".equals(apiResponse.status)) {

            googeplaceList = apiResponse.googlePlaces;

            //init request param
            addressSearch = new RequestAddressSearch(
                    userContent.memId,
                    LocalStorageHelper.getAccessToken(getContext()), AppUtil.getCurrentLangIndex(getActivity()));

            Log.d(TAG, "isDefaultSearch: " + isDefaultSearch);
            if (isDefaultSearch) {
                //do search by selecting the first place

                callGeoCodeApi(googeplaceList.get(0).placeId, 0);

//                if (AppConfigSetting.defaultHelper(context).isFreshLogin()) {
////                    AppConfigSetting.defaultHelper(context).saveIsFreshLogin(false);
//                } else {
//                    //save to local disk for latest result reference
//                    HistorySearchSetting.getInstance().addRecord((GooglePlace) apiResponse.googlePlaces.get(0));
//                    LocalStorageHelper.defaultHelper(getActivity()).saveLatestSearchResultListToLocalStorage(HistorySearchSetting.getInstance().getLatestFiveAddressSearchListInReverse());
//                }

            } else {
                Log.d(TAG, "googeplaceList: " + googeplaceList.toString());
                if (((MainActivityTabBase) getActivity()).placesList != null) {
                    ((MainActivityTabBase) getActivity()).placesList.clear();
                }
                ((MainActivityTabBase) getActivity()).placesList.addAll(googeplaceList);
                if (((MainActivityTabBase) getActivity()).searchResultAdapter != null)
                    ((MainActivityTabBase) getActivity()).searchResultAdapter.notifyDataSetChanged();

                final MatrixCursor c = new MatrixCursor(
                        new String[]{BaseColumns._ID, "cityName"});
                for (int i = 0; i < apiResponse.googlePlaces.size(); i++) {
                    c.addRow(new Object[]{i,
                            googeplaceList.get(i).description});
                }
            }

        } else {
            Log.d(TAG, "not ok");
        }
    }


    private void callPlaceApi(final GoogleAddress googleAddress) {
        String countryShortName = GoogleGeoUtil.getTypeFromGoogleAddress(googleAddress);
        final ArrayList<String> countryLangList = new ArrayList<String>() {{
            add("en");
        }};
        ;
        if (countryLangList != null) {
            for (final String countryLang : countryLangList) {
                if (IS_SUPPORT_GOOGLE_SERVICE) {
                    final GoogleService weatherService =
                            new GoogleService(context);
                    weatherService.getGeocodeService().geocodeWithLangRequest(
                            googleAddress.placeId, GoogleService.API_KEY, countryLang,
                            new retrofit.Callback<ApiResponseGeocode>() {
                                @Override
                                public void failure(RetrofitError error) {
                                    Log.e("result", error.toString());
                                }

                                @Override
                                public void success(final ApiResponseGeocode apiResponse, Response response) {
                                    Log.d(TAG, "Google service API 3 -- " + countryLang);
                                    processGoogleGeoPlaceByIdResult(apiResponse, countryLang, countryLangList);
                                }
                            }
                    );
                } else {
                    RequestGoogleMapPlaceGeocodeByPlaceId requestGoogleMapPlcaeGeocodeByPlaceId = new RequestGoogleMapPlaceGeocodeByPlaceId(userContent.memId, LocalStorageHelper.getAccessToken(getContext()), AppUtil.getCurrentLangCode(context), googleAddress.placeId);
                    requestGoogleMapPlcaeGeocodeByPlaceId.callGoogleMapPlaceAPIGeocodeByPlaceId(getActivity(), new ApiCallback() {
                        @Override
                        public void success(ApiResponse apiResponse) {
                            try {
                                JSONObject jsonObject = new JSONObject(apiResponse.jsonContent.toString());
                                ApiResponseGeocode responseGson = new Gson().fromJson(jsonObject.optString("GoogleResult"), ApiResponseGeocode.class);
                                processGoogleGeoPlaceByIdResult(responseGson, countryLang, countryLangList);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void failure(String errorMsg) {
                            Log.e("result", errorMsg.toString());
                        }
                    });
                }

            }

        } else if (Constants.isDevelopment)
            Toast.makeText(context, "This is not valid place", Toast.LENGTH_LONG).show();
    }


    private void processGoogleGeoPlaceByIdResult(final ApiResponseGeocode apiResponse, String countryLang, ArrayList<String> countryLangList) {
        if (apiResponse.status.equals("OK") && apiResponse.googleAddress.size() > 0) {
            //call get agent profile api
            if (addressSearch == null) {
                //init request param
                addressSearch = new RequestAddressSearch(
                        userContent.memId,
                        LocalStorageHelper.getAccessToken(getContext()), AppUtil.getCurrentLangIndex(getActivity()));
            }
            NewsFeedFragment.googleAddressPack = addressSearch.new GoogleAddressPack();
            NewsFeedFragment.googleAddressPack.lang = AppUtil.getCurrentLangISO2Code(context);
            NewsFeedFragment.googleAddressPack.address = apiResponse.googleAddress.get(0);
            addressSearch.googleAddress = NewsFeedFragment.googleAddressPack;
            addressSearch.inputAddress = mTitleBar.getEtSearchField().getText().toString();

            boolean isFilterApplied = SearchFilterSetting.getInstance().getAlertCount() > 0 ? true : false;

            if (countryLang.equals(countryLangList.get(0)))
                searchAddress(addressSearch, isFilterApplied);
//                                    } else {
//                                        Toast.makeText(MainActivityTabBase.this, "Exceed daily query limit", Toast.LENGTH_LONG).show();
        } else {
            if (Constants.isDevelopment)
                Toast.makeText(context, "Please enter another place", Toast.LENGTH_LONG).show();
        }
    }


    private void searchAddress(final RequestAddressSearch addressSearch, final boolean isApplyFiltered) {
        //        //show loading
        if (!((MainActivityTabBase) getActivity()).isTutorialDialogFragmentVisible())
            ((MainActivityTabBase) getActivity()).showLoadingDialog();

        //set the empty result text
        String emptyResultDesc;
        if (isApplyFiltered)
            emptyResultDesc = getString(R.string.search_result_detail__filter_no_results_founds);
        else
            emptyResultDesc = getString(R.string.search_result_detail__no_results_founds);
        tvEmptyResultDesc.setText(emptyResultDesc);

        addressSearch.callSearchAddressApi(getActivity(), isApplyFiltered, new ApiCallback() {
                    @Override
                    public void success(ApiResponse apiResponse) {
                        //hide loading
                        ((MainActivityTabBase) getActivity()).hideLoadingDialog();

                        mRecyclerView.setVisibility(View.VISIBLE);
                        Log.d(TAG, "Google service API 4");

                        Log.d(TAG, "googleAddressListJSON = " + apiResponse.jsonContent.toString());
                        //reset loading for pagination
                        hasPageEnd = false;

                        int totalCount = 0;
                        try {
                            JSONObject jsonObject = new JSONObject(apiResponse.jsonContent);
                            totalCount = jsonObject.optJSONObject("Content").optInt("TotalCount");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Log.d(TAG, "searchAddress success = " + isDefaultSearch + " " + LocalStorageHelper.getLatestSearchResultList(getActivity()).size());
                        Log.d(TAG, "searchAddress success = " + addressSearch.languageIndex);
                        Log.d(TAG, "searchAddress success = " + addressSearch.googleAddress.address.googleAddressList.toString());
                        Log.d(TAG, "searchAddress success = " + addressSearch.googleAddress.address.formattedAddress.toString());
                        Log.d(TAG, "searchAddress success = " + addressSearch.inputAddress.toString());

                        if (!isDefaultSearch || LocalStorageHelper.getLatestSearchResultList(getActivity()).size() > 0) {

                            //save as search history
                            HistorySearchSetting.getInstance().addRecord(addressSearch);
                            //save to local disk for latest result reference
                            LocalStorageHelper.defaultHelper(getActivity()).saveLatestSearchResultListToLocalStorage(HistorySearchSetting.getInstance().getLatestFiveAddressSearchList());

                            //save the latest address name
                            if (totalCount > 0) {
                                LocalStorageHelper.saveLatestAddressSearchToLocalStorage(context, addressSearch.inputAddress);
                                LocalStorageHelper.defaultHelper(getActivity()).saveLatestSearchCountToLocalStorage(totalCount);
                            }

                            //save for current search address name
                            LocalStorageHelper.saveCurrentAddressSearchToLocalStorage(context, addressSearch);
                            LocalStorageHelper.defaultHelper(getActivity()).saveCurrentSearchCountToLocalStorage(totalCount);

                            mTitleBar.setResultCount(totalCount);
                            // Mixpanel
                            MixpanelAPI mixpanel = MixpanelAPI.getInstance(getActivity(), AppConfig.getMixpanelToken());
                            JSONObject json = new JSONObject();
                            try {
                                json.put("Search query", addressSearch.googleAddress.address);
                                if (isApplyFiltered) {
                                    json.put("Filter applied", "YES");
                                } else {
                                    json.put("Filter applied", "NO");
                                }
                                json.put("# of results returned", totalCount);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            mixpanel.track("Searched Area", json);

                            //enable the filter btn
                            mTitleBar.getFilterBtn().setEnabled(LocalStorageHelper.getLatestSearchResultList(getActivity()).size() > 0);

                        } else {
                            //search USA if no result in current country
                            if (totalCount < 20 && regionName != LocationUtil.DEFAULT_COUNTRY) {
                                regionName = LocationUtil.DEFAULT_COUNTRY;
                                performSearch(LocationUtil.DEFAULT_COUNTRY, true);
                                return;
                            }


                            if (CoreData.isNewUser|| addressSearch.inputAddress.equals("")) {
                                //do nothing
                            } else {
                                //save as search history
                                HistorySearchSetting.getInstance().addRecord(addressSearch);
                                //save to local disk for latest result reference
                                LocalStorageHelper.defaultHelper(getActivity()).saveLatestSearchResultListToLocalStorage(HistorySearchSetting.getInstance().getLatestFiveAddressSearchList());
                            }

                            //to verify if this is a fresh search
                            CoreData.isNewUser=false;

                            //hide total results
                            mTitleBar.setResultCount(0);
                        }

                        handleSearchResult(apiResponse);
                    }

                    @Override
                    public void failure(String errorMsg) {
                        //hide loading
                        mRecyclerView.setVisibility(View.GONE);
                        isDefaultSearch = false;
                        ((MainActivityTabBase) getActivity()).hideLoadingDialog();
                        mSwipeRefreshLayout.setRefreshing(false);
                        flEmptyView.setVisibility(View.VISIBLE);
                        agentProfilesList = new ArrayList<AgentProfiles>();
                        if (searchAdapter != null)
                            searchAdapter.notifyDataSetChanged();

                        //save as search history
                        HistorySearchSetting.getInstance().addRecord(addressSearch);
                        //save to local disk for latest result reference
                        LocalStorageHelper.defaultHelper(getActivity()).saveLatestSearchResultListToLocalStorage(HistorySearchSetting.getInstance().getLatestFiveAddressSearchList());


                        //save for current search address name
                        LocalStorageHelper.saveCurrentAddressSearchToLocalStorage(context, addressSearch);
                        LocalStorageHelper.defaultHelper(getActivity()).saveCurrentSearchCountToLocalStorage(0);


                        //update search field
                        mTitleBar.setResultCount(0);

                        Log.d(TAG, "addressSearch error = " + errorMsg);
                    }
                }

        );


        String json = new Gson().toJson(addressSearch);


        Log.d(TAG, "addressSearchInJSON: " + json);
    }


    private MainActivityTabBase.CURRENT_SECTION getCurrentSection() {
        return ((MainActivityTabBase) getActivity()).currentSection;
    }
}
