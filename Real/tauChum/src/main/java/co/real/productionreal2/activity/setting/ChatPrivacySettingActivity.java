package co.real.productionreal2.activity.setting;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.google.gson.Gson;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import co.real.productionreal2.Constants;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.BaseActivity;
import co.real.productionreal2.callback.DialogCallback;
import co.real.productionreal2.chat.ChatDbViewController;
import co.real.productionreal2.chat.core.PrivateChatImpl;
import co.real.productionreal2.dao.account.DBLogInfo;
import co.real.productionreal2.dao.account.DBQBChatDialog;
import co.real.productionreal2.daomanager.DatabaseManager;
import co.real.productionreal2.model.QBUserCustomData;
import co.real.productionreal2.model.UserQBUser;
import co.real.productionreal2.view.Dialog;

/**
 * Created by hohojo on 11/11/2015.
 */
public class ChatPrivacySettingActivity extends BaseActivity {
    private static final String TAG = "ChatPrivacySettingAc";
    @InjectView(R.id.headerLayout)
    View headerLayout;
    @InjectView(R.id.showLastSeenSwitch)
    Switch showLastSeenSwitch;
    @InjectView(R.id.showReadReciptsSwitch)
    Switch showReadReciptsSwitch;
    private DBLogInfo dbLogInfo;

    private static ProgressDialog ringProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_privacy_setting);
        ButterKnife.inject(this);
        dbLogInfo = DatabaseManager.getInstance(this).getLogInfo();

        getUserQBUserAndUpdateSwitches();
        initView();
    }

    private void getUserQBUserAndUpdateSwitches() {
        QBUsers.getUser(dbLogInfo.getQbUserId(), new QBEntityCallback<QBUser>() {
            @Override
            public void onSuccess(QBUser qbUser, Bundle bundle) {
                UserQBUser.qbUser = qbUser;
                updateSwitches();
                cancelProgressDialog();
            }

            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(List<String> list) {
                cancelProgressDialog();

                if (Constants.isDevelopment)
                    Log.d(TAG,"getUserByQb error: "+ list);

            }
        });
    }

    private void showProgressDialog() {
        if (ringProgressDialog != null && ringProgressDialog.isShowing())
            return;
//        ringProgressDialog = ProgressDialog.show(this, getString(R.string.common__please_wait), "Getting Chat information");
    }

    private void cancelProgressDialog() {
        if (ringProgressDialog != null) {
            ringProgressDialog.dismiss();
            ringProgressDialog.cancel();
            ringProgressDialog = null;
        }
    }

    private void updateEnableStatusToQB(final boolean isEnable) {
        if (UserQBUser.qbUser == null) {
            //retry later
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    updateEnableStatusToQB(isEnable);
                }
            }, 500);
        } else {
            Gson gson = new Gson();
            QBUserCustomData qbUserCustomData = gson.fromJson(UserQBUser.qbUser.getCustomData(), QBUserCustomData.class);
            if (isEnable == false) {
                qbUserCustomData.chatRoomShowLastSeen = false;
            } else {
                qbUserCustomData.chatRoomShowLastSeen = true;
            }
            UserQBUser.qbUser.setCustomData(gson.toJson(qbUserCustomData));
            QBUsers.updateUser(UserQBUser.qbUser, new QBEntityCallbackImpl<QBUser>() {
                @Override
                public void onSuccess(QBUser user, Bundle args) {
                    UserQBUser.qbUser = user;
                    dbLogInfo.setEnableStatus(isEnable);
                    DatabaseManager.getInstance(ChatPrivacySettingActivity.this).updateLogInfo(dbLogInfo);
                    sendSystemMsgToAllChatUser();
                    Log.d(TAG,"setChatroomshowlastseen success");
                }

                @Override
                public void onError(List<String> errors) {
                    Log.d(TAG,"setChatroomshowlastseen error:"+errors);
                }
            });
        }

    }

    private void sendSystemMsgToAllChatUser() {
        List<DBQBChatDialog> dbqbChatDialogList = DatabaseManager.getInstance(this).listQBChatDialog();
        for (DBQBChatDialog dbqbChatDialog : dbqbChatDialogList) {
            if (dbqbChatDialog != null)
                PrivateChatImpl.getInstance(this).sendSystemMsgSetting((int) dbqbChatDialog.getQbUserID(), ChatDbViewController.SYSTEM_MSG_CHAT_PRIVACY_SETTING);
        }

    }

    private void updateEnableReadToQB(final boolean isEnable) {
        if (UserQBUser.qbUser == null) {
            //retry later
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    // Do something after 5s = 5000ms
                    updateEnableReadToQB(isEnable);
                }
            }, 500);
        } else {
            Gson gson = new Gson();
            QBUserCustomData qbUserCustomData = gson.fromJson(UserQBUser.qbUser.getCustomData(), QBUserCustomData.class);
            if (isEnable == false) {
                qbUserCustomData.chatRoomShowReadReceipts = false;
            } else {
                qbUserCustomData.chatRoomShowReadReceipts = true;
            }

            UserQBUser.qbUser.setCustomData(gson.toJson(qbUserCustomData));
            QBUsers.updateUser(UserQBUser.qbUser, new QBEntityCallbackImpl<QBUser>() {
                @Override
                public void onSuccess(QBUser user, Bundle args) {
                    UserQBUser.qbUser = user;
                    dbLogInfo.setEnableReadMsg(isEnable);
                    DatabaseManager.getInstance(ChatPrivacySettingActivity.this).updateLogInfo(dbLogInfo);
                    sendSystemMsgToAllChatUser();
                    Log.d(TAG,"setChatroomshowreadreceipts success");
                }

                @Override
                public void onError(List<String> errors) {
                    Log.d(TAG,"setChatroomshowreadreceipts error:"+ errors);
                }
            });
        }
    }

    private void initView() {
        TextView titleTextView = (TextView) headerLayout.findViewById(R.id.titleTextView);
        titleTextView.setText(R.string.common__privacy);

        showProgressDialog();

        showLastSeenSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                updateEnableStatusToQB(isChecked);
            }
        });
        showReadReciptsSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                updateEnableReadToQB(isChecked);
            }
        });


    }

    private void updateSwitches() {
        QBUserCustomData qbUserCustomData = new Gson().fromJson(UserQBUser.qbUser.getCustomData(), QBUserCustomData.class);
        if (qbUserCustomData == null) {
            Dialog.feedbackDialog(this, true, getString(R.string.common__privacy), getString(R.string.common_google_play_services_network_error_text)
                    , new DialogCallback() {
                @Override
                public void yes() {
                    finish();
                }
            }).show();
        } else {
            if (
                    qbUserCustomData.chatRoomShowLastSeen)
                showLastSeenSwitch.setChecked(true);
            else
                showLastSeenSwitch.setChecked(false);

            if (
                    qbUserCustomData.chatRoomShowReadReceipts)
                showReadReciptsSwitch.setChecked(true);
            else
                showReadReciptsSwitch.setChecked(false);
        }

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
