package co.real.productionreal2;

import android.graphics.Bitmap;

import co.real.productionreal2.model.AgentListing;
import co.real.productionreal2.service.model.AgentProfiles;

public class BusSelectedEvent {
    public static class SelectAgentEvent {

        public AgentProfiles agentProfiles;

        public SelectAgentEvent(AgentProfiles agentProfiles) {
            this.agentProfiles = agentProfiles;
        }

    }

    public static class SelectPropertyEvent {

        public AgentListing listing;

        public SelectPropertyEvent(AgentListing agentListing) {
            this.listing = agentListing;
        }

    }

    public static class ChatLobbyPagerStateEvent {
        public int selectedNo;
        public int pagerState;

        public ChatLobbyPagerStateEvent(int selectedNo, int pagerState) {
            this.selectedNo = selectedNo;
            this.pagerState = pagerState;
        }
    }

    public static class SelectMeEvent {
        public Bitmap bitmap;

        public SelectMeEvent(Bitmap bitmap) {
            this.bitmap = bitmap;
        }
    }


}
