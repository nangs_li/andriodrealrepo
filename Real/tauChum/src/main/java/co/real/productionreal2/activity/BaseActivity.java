package co.real.productionreal2.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.WindowManager;

import com.quickblox.chat.QBChatService;

import co.real.productionreal2.Navigation;
import co.real.productionreal2.R;
import co.real.productionreal2.util.ImageUtil;
import lib.blurbehind.OnBlurCompleteListener;

/**
 * Created by hohojo on 24/9/2015.
 */
public class BaseActivity extends FragmentActivity implements OnBlurCompleteListener{
    protected static final int REQUEST_STORAGE_READ_ACCESS_PERMISSION = 101;
    protected static final int REQUEST_STORAGE_WRITE_ACCESS_PERMISSION = 102;
    private boolean didApplyBlurredBackground = false;
    private Navigation.NavigationType navigationType;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!QBChatService.isInitialized()) {
            QBChatService.init(this);
        }
       this.navigationType = Navigation.getNavigationTypeFromIntent(getIntent());

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!QBChatService.isInitialized()) {
            QBChatService.init(this);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        ImageUtil.getInstance().blurCompleteListener = this;
        applyBlurredBackground();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onBlurComplete() {
        didApplyBlurredBackground = true;
        applyBlurredBackground();
    }

    private void applyBlurredBackground(){
        if (ImageUtil.getInstance().hasBluredBackground()){
            ImageUtil.getInstance().applyBlurBackground(this);
        }
    }

    public void finish() {
        super.finish();
        ImageUtil.getInstance().clearBlurredBackground(this);
        if (this.navigationType == Navigation.NavigationType.Push) {
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        } else if (this.navigationType == Navigation.NavigationType.Present) {
            overridePendingTransition(R.anim.fade_in, R.anim.slide_bottom_down);
        }else if (this.navigationType == Navigation.NavigationType.Fade){
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        }
    }

    public void launchLoadingDialog() {
        if(progressDialog==null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage(getString(R.string.alert__Initializing));
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
        }
        if (!progressDialog.isShowing())
            progressDialog.show();

    }

    public void cancelLoadingDialog() {
        if (progressDialog!=null && progressDialog.isShowing()) {
            progressDialog.cancel();
        }
    }



}
