package co.real.productionreal2.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import co.real.productionreal2.model.GoogleAddress;

import java.util.List;

/**
 * Created by hohojo on 14/10/2015.
 */
public class DialogPropertyAdapter extends BaseAdapter{
    private Context context;
    private List<GoogleAddress> googleAddressList;
    public DialogPropertyAdapter(Context context,  List<GoogleAddress> googleAddressList) {
        this.context = context;
        this.googleAddressList = googleAddressList;
    }
    @Override
    public int getCount() {
        return googleAddressList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        TextView textView = new TextView(context);
        textView.setTextAppearance(context, android.R.style.TextAppearance_Large);
        textView.setText(googleAddressList.get(position).formattedAddress);
        return textView;
    }




}
