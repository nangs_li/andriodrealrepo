package co.real.productionreal2.util;

import android.content.Context;
import android.view.Display;
import android.view.WindowManager;

/**
 * Created by kelvinsun on 5/1/16.
 */
public class DisplayUtil {

    public static int getDisplayHeight(Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        return display.getHeight();
    }

    public static int getDisplayWidth(Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        return display.getWidth(); // default height width of screen
    }
}
