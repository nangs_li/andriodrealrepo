package co.real.productionreal2.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import co.real.productionreal2.R;
import co.real.productionreal2.Constants;
import co.real.productionreal2.activity.createpost.BaseCreatePost;
import co.real.productionreal2.activity.createpost.CreatePost3ReasonsActivity;
import co.real.productionreal2.util.AppUtil;
import co.real.productionreal2.util.FileUtil;
import com.squareup.picasso.Picasso;

import java.io.File;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by hohojo on 13/10/2015.
 */
public class Reason2Fragment extends Fragment {
    @InjectView(R.id.docImageBtn)
    TextView docImageBtn;
    @InjectView(R.id.reasonsEditText)
    EditText reasonsEditText;
    @InjectView(R.id.top3resonsImageView)
    ImageView top3resonsImageView;
    //    private BaseCreatePost.Reasons reason;
    private BaseCreatePost baseCreatePost;
    private BaseCreatePost.ReasonsDespImage currentReason;
    private int langIndex;

    public static Reason2Fragment newInstance(int langIndex) {
        Bundle args = new Bundle();
        args.putInt(Constants.EXTRA_LANG_INDEX, langIndex);
        Reason2Fragment fragment = new Reason2Fragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        langIndex = getArguments().getInt(Constants.EXTRA_LANG_INDEX, 2);

        View view = inflater.inflate(R.layout.top_3_resons_pager_items, container,
                false);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        initView();
    }

    private void initView() {
        baseCreatePost = BaseCreatePost.getInstance(getActivity());
        
        if (baseCreatePost.getReasonsDespImageList().size()>1 && langIndex<baseCreatePost.getReasonsDespImageList().size()) {
            currentReason = baseCreatePost.getCurrentReasonsDespImage(langIndex);
        }else{
            currentReason = baseCreatePost.getCurrentReasonsDespImage();
        }
//        if (baseCreatePost.findCurrentCreatePost(langIndex) != null &&
//                baseCreatePost.findCurrentCreatePost(langIndex).reason1ImagePath != null &&
//                !baseCreatePost.findCurrentCreatePost(langIndex).reason1ImagePath.isEmpty()) {
//            top3resonsImageView.setVisibility(View.VISIBLE);
//            Bitmap bitmap = BitmapFactory.decodeFile(baseCreatePost.findCurrentCreatePost(langIndex).reason1ImagePath);
//            top3resonsImageView.setImageBitmap(bitmap);
//            docImageBtn.setVisibility(View.GONE);
//        } else {
//            top3resonsImageView.setVisibility(View.GONE);
//            docImageBtn.setVisibility(View.VISIBLE);
//        }
//        if (baseCreatePost.findCurrentCreatePost(langIndex) != null &&
//                baseCreatePost.findCurrentCreatePost(langIndex).createPostRequest.reasons.get(0).reason1 != null &&
//                !baseCreatePost.findCurrentCreatePost(langIndex).createPostRequest.reasons.get(0).reason1.isEmpty()) {
//            String currResonString = baseCreatePost.findCurrentCreatePost(langIndex).createPostRequest.reasons.get(0).reason1;
//            reasonsEditText.setText(currResonString);
//        }
        if (currentReason != null &&
                currentReason.reason2ImagePath != null &&
                !currentReason.reason2ImagePath.isEmpty()) {
            top3resonsImageView.setVisibility(View.VISIBLE);
            Picasso.with(getActivity())
                    .load(new File(currentReason.reason2ImagePath))
                    .placeholder(R.drawable.progress_animation)
                    .into(top3resonsImageView);
            docImageBtn.setVisibility(View.GONE);
        } else {
            top3resonsImageView.setVisibility(View.GONE);
            docImageBtn.setVisibility(View.VISIBLE);
        }
        if (currentReason != null &&
                currentReason.reason2 != null &&
                !currentReason.reason2.isEmpty()) {
            String currResonString = currentReason.reason2;
            reasonsEditText.setText(currResonString);
        }

        reasonsEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    currentReason.reason2 = reasonsEditText.getText().toString();

            }

            @Override
            public void afterTextChanged(Editable editable) {
                saveReason();
            }
        });

        reasonsEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    AppUtil.hideKeyboard(getActivity(), reasonsEditText);
                }
            }
        });

        docImageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intentToPickImage();
            }
        });

        top3resonsImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showReasonPhotoDialog();
            }
        });
    }

    private final void intentToPickImage() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        getActivity().startActivityForResult(photoPickerIntent, CreatePost3ReasonsActivity.SELECT_PHOTO_2);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == CreatePost3ReasonsActivity.SELECT_PHOTO_2 && resultCode == Activity.RESULT_OK) {
            Uri selectedImage = intent.getData();
            String filePath = FileUtil.getRealPathFromURI(getActivity(), selectedImage);
            Log.d("getImageUrl", "getImageUrlWithAuthority: "+filePath);
//            BaseCreatePost.getInstance(getActivity()).getReasons()[0].imagePath = filePath;
            if (filePath!=null && currentReason!=null) {
                currentReason.reason2ImagePath = filePath;
                initView();
            }else{
                AppUtil.showToast(getActivity(),getString(R.string.chatroom_error_message__incompatible_error));
            }

//            InputStream imageStream = null;
//            try {
//                imageStream = getActivity().getContentResolver().openInputStream(selectedImage);
//            } catch (FileNotFoundException e) {
//                e.printStackTrace();
//            }
//            Bitmap bitmap = BitmapFactory.decodeStream(imageStream);
//            top3resonsImageView.setImageBitmap(bitmap);


//            notifyDataSetChanged();
        }
    }

    private void showReasonPhotoDialog() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_reason_pic);
        dialog.show();

        Button btnTakePhoto = (Button) dialog.findViewById(R.id.photoLibRadioButton);
        btnTakePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
                intentToPickImage();

            }
        });

        Button btnChoosePhoto = (Button) dialog.findViewById(R.id.removePhotoRadioButton);
        btnChoosePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
                currentReason.reason2ImagePath = null;
                initView();
            }
        });
    }

    public void saveReason(){
        currentReason.reason2=reasonsEditText.getText().toString();
    }
}