package co.real.productionreal2.callback;

/**
 * Created by kelvinsun on 3/2/16.
 */
public interface DialogDualCallback {
    public void positive();
    public void negative();
}
