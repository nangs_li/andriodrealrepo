package co.real.productionreal2.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.android.gms.common.api.GoogleApiClient;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.quickblox.chat.QBChatService;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import co.real.productionreal2.Constants;
import co.real.productionreal2.CoreData;
import co.real.productionreal2.Navigation;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.MainActivityTabBase;
import co.real.productionreal2.activity.SplashActivity;
import co.real.productionreal2.activity.viral.ConnectAgentActivity;
import co.real.productionreal2.activity.viral.InviteToChatActivity;
import co.real.productionreal2.activity.viral.InviteToFollowActivity;
import co.real.productionreal2.activity.setting.AboutSettingActivity;
import co.real.productionreal2.activity.setting.ChangeEmailSettingActivity;
import co.real.productionreal2.activity.setting.ChangeLangSettingActivity;
import co.real.productionreal2.activity.setting.ChangePwSettingActivity;
import co.real.productionreal2.activity.setting.ChatPrivacySettingActivity;
import co.real.productionreal2.activity.setting.ChatSettingActivity;
import co.real.productionreal2.activity.setting.LogoutSettingActivity;
import co.real.productionreal2.activity.setting.MetricSettingActivity;
import co.real.productionreal2.activity.setting.PrivacySettingActivity;
import co.real.productionreal2.activity.setting.ReportSettingActivity;
import co.real.productionreal2.activity.setting.ResetPwSettingActivity;
import co.real.productionreal2.activity.setting.SettingAboutActivity;
import co.real.productionreal2.activity.setting.SettingChatActivity;
import co.real.productionreal2.activity.setting.SettingGeneralActivity;
import co.real.productionreal2.activity.setting.TNCSettingActivity;
import co.real.productionreal2.callback.UpdateSettingViewListener;
import co.real.productionreal2.common.LocalStorageHelper;
import co.real.productionreal2.config.AppConfig;
import co.real.productionreal2.daomanager.DatabaseManager;
import co.real.productionreal2.model.SettingItem;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;
import co.real.productionreal2.util.DBUtil;
import co.real.productionreal2.util.PermissionUtil;
import co.real.productionreal2.util.StringUtils;
import co.real.productionreal2.view.Dialog;
import co.real.productionreal2.view.SettingSectionRecyclerView;

;

public class SettingFragment extends Fragment implements UpdateSettingViewListener {

    private static final String TAG = "SettingFragment";
//    @InjectView(R.id.lang_setting)
//    TextView langSetting;
//    @InjectView(R.id.statusDisplaySwitch)
//    Switch statusDisplaySwitch;
//    @InjectView(R.id.readMsgDisplaySwitch)
//    Switch readMsgDisplaySwitch;

//    @InjectView(R.id.saveBtn)
//    Button saveBtn;

    public static String REQUEST_CODE = "REQUEST_CODE";
    public static String RESULT_CODE = "RESULT_CODE";
    public static boolean fragmentIsRunning = false;
    public static Context context;
    private GoogleApiClient mGoogleApiClient;


    @InjectView(R.id.rootView)
    View rootView;
    @InjectView(R.id.chatLayout)
    View chatLayout;
    @InjectView(R.id.chatPrivacyLayout)
    View chatPrivacyLayout;
    @InjectView(R.id.langLayout)
    View langLayout;
    @InjectView(R.id.metricLayout)
    View metricLayout;
    @InjectView(R.id.reportLayout)
    View reportLayout;
    @InjectView(R.id.privacyLayout)
    View privacyLayout;
    @InjectView(R.id.tncLayout)
    View tncLayout;
    @InjectView(R.id.aboutLayout)
    View aboutLayout;
    @InjectView(R.id.changeEmailLayout)
    View changeEmailLayout;
    @InjectView(R.id.changePwLayout)
    View changePwLayout;
    @InjectView(R.id.resetPwLayout)
    View resetPwLayout;
    @InjectView(R.id.logoutLayout)
    View logoutLayout;
    @InjectView(R.id.developmentLayout)
    View developmentLayout;

    @InjectView(R.id.broadcastBtn)
    Button inviteFollowBtn;

    @InjectView(R.id.inviteChatBtn)
    Button inviteChatBtn;

    @InjectView(R.id.connectAgentBtn)
    Button inviteDownloadBtn;

    @InjectView(R.id.settingRecyclerView)
    SettingSectionRecyclerView settingRecyclerView;
    List<SettingItem> settingItems;


    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentIsRunning = true;
        context = getActivity();

        View view = inflater.inflate(R.layout.fragment_setting, container, false);
        ButterKnife.inject(this, view);
        initView();
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void initData(){
        String settingGeneralTitle = StringUtils.getById(getActivity(),R.string.setting__general,"General");
        String settingChatTitle = StringUtils.getById(getActivity(),R.string.setting__chats,"Chats");
        String settingFeedbackTitle = StringUtils.getById(getActivity(),R.string.setting__feedback,"Feedback");
        String settingAboutTitle = StringUtils.getById(getActivity(),R.string.setting__about,"About");
        String settingLogoutTitle = StringUtils.getById(getActivity(),R.string.common__log_out,"Logout");

        settingItems = new ArrayList<>();
        settingItems.add(new SettingItem(SettingItem.SettingItemType.SETTING_TYPE_SECTION_SPACING,R.color.transparent));
        settingItems.add(new SettingItem(settingGeneralTitle,"",SettingGeneralActivity.class));
        settingItems.add(new SettingItem(settingChatTitle,"",SettingChatActivity.class));
        settingItems.add(new SettingItem(settingFeedbackTitle,"",ReportSettingActivity.class));
        settingItems.add(new SettingItem(settingAboutTitle,"",SettingAboutActivity.class));
//        settingItems.add(new SettingItem(settingLogoutTitle,"",null, SettingItem.SettingItemType.SETTING_TYPE_LOGOUT));

    }
    private void initRecyclerView(List<SettingItem> items){
        settingRecyclerView.initAdapterWithItems(items,getActivity());
    }

    private void initView() {
//        if (Constants.isDevelopment)
        developmentLayout.setVisibility(View.VISIBLE);
//        else
//            developmentLayout.setVisibility(View.GONE);
        initData();
        initRecyclerView(settingItems);



        if (!isRealNetworkLogin()) {
            changeEmailLayout.setVisibility(View.GONE);
            changePwLayout.setVisibility(View.GONE);
        }

        String inviteTo = "InviteTo";
        String chat = "Chat";
        String follow = "Follow";
        String download = "Download";

//        inviteChatBtn.setText(inviteStyleString(inviteTo, chat));
//        inviteDownloadBtn.setText(inviteStyleString(inviteTo, download));
//        inviteFollowBtn.setText(inviteStyleString(inviteTo, follow));
        inviteChatBtn.setText(Html.fromHtml(getString(R.string.setting__invite_to_chat)));
        inviteDownloadBtn.setText(Html.fromHtml(getString(R.string.connectagent__title)));
        inviteFollowBtn.setText(Html.fromHtml(getString(R.string.setting__broadcast)));

    }

    private SpannableString inviteStyleString(String white, String yellow) {
        SpannableString text;
        if (white != null && white.length() > 0 && yellow != null && yellow.length() > 0) {
            text = new SpannableString(white + yellow);
            text.setSpan(new ForegroundColorSpan(Color.WHITE), 0, white.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            text.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.invite_yellow)), white.length(), white.length() + yellow.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        } else {
            text = new SpannableString("");
        }

        return text;
    }

    private boolean isRealNetworkLogin() {
//        SharedPreferences prefs = getActivity().getSharedPreferences(Constants.PREF_REAL_NETWORK, getActivity().MODE_PRIVATE);
//        String realJson = prefs.getString(Constants.PREF_REAL_NETWORK_JSON, null);
        String realJson =  LocalStorageHelper.getRealNetworkJson(this.getActivity());
        if (realJson == null || realJson.equals(""))
            return false;

        return true;
    }

    private Drawable getInviteFollowDrawable(boolean isAgent) {
        int drawableId;
        if (isAgent)
            drawableId = R.drawable.ico_setting_invite_follow_on;
        else
            drawableId = R.drawable.ico_setting_invite_follow_off;


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return getResources().getDrawable(drawableId, getActivity().getTheme());
        } else {
            return getResources().getDrawable(drawableId);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
        fragmentIsRunning = false;
    }


    private void toSplashPage() {
        Intent splashIntent = new Intent(getActivity(), SplashActivity.class);
        splashIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(splashIntent);
        getActivity().finish();
    }

    private void deleteLoginInfoDB() {
        DatabaseManager.getInstance(getActivity()).deleteAllLogInfo();
    }

    @OnClick(R.id.logoutLayout)
    public void logoutLayoutClick(View view) {
        //Mixpanel
        MixpanelAPI mixpanel = MixpanelAPI.getInstance(context, AppConfig.getMixpanelToken());
        mixpanel.track("Attempted to Log Out");

        Intent intent = new Intent(getActivity(), LogoutSettingActivity.class);
        Navigation.pushIntent(getActivity(),intent);
    }

    @OnClick(R.id.chatLayout)
    public void chatLayoutClick(View view) {
        Intent intent = new Intent(getActivity(), ChatSettingActivity.class);
        Navigation.pushIntent(getActivity(),intent);
    }

    @OnClick(R.id.chatPrivacyLayout)
    public void chatPrivacyLayoutClick(View view) {
        if (!QBChatService.getInstance().isLoggedIn()) {
            Dialog.normalDialog(getActivity(),
                    getString(R.string.error_message__network_failure),
                    getString(R.string.error_message__please_try_again_later)).show();
        } else {
            Intent intent = new Intent(getActivity(), ChatPrivacySettingActivity.class);
            Navigation.pushIntent(getActivity(),intent);
        }
    }

    @OnClick(R.id.metricLayout)
    public void metricLayoutClick(View view) {
        Intent intent = new Intent(getActivity(), MetricSettingActivity.class);
        Navigation.pushIntent(getActivity(),intent);
    }

    @OnClick(R.id.reportLayout)
    public void reportLayoutClick(View view) {
        Intent intent = new Intent(getActivity(), ReportSettingActivity.class);
        Navigation.pushIntent(getActivity(),intent);
    }

    @OnClick(R.id.privacyLayout)
    public void privacyLayoutClick(View view) {
        Intent intent = new Intent(getActivity(), PrivacySettingActivity.class);
        Navigation.pushIntent(getActivity(),intent);
    }

    @OnClick(R.id.langLayout)
    public void langLayoutClick(View view) {
        Intent intent = new Intent(getActivity(), ChangeLangSettingActivity.class);
//        startActivity(intent);
        Navigation.pushIntentForResult(getActivity(),intent,Constants.CHANGE_LANG);

    }

    @OnClick(R.id.tncLayout)
    public void tncLayoutClick(View view) {
        Intent intent = new Intent(getActivity(), TNCSettingActivity.class);
        Navigation.pushIntent(getActivity(),intent);
    }

    @OnClick(R.id.aboutLayout)
    public void aboutLayoutClick(View view) {
        Intent intent = new Intent(getActivity(), AboutSettingActivity.class);
        Navigation.pushIntent(getActivity(),intent);
    }

    @OnClick(R.id.changeEmailLayout)
    public void changeEmailLayoutClick(View view) {

        // Mixpanel
        MixpanelAPI mixpanel = MixpanelAPI.getInstance(getActivity(), AppConfig.getMixpanelToken());
        mixpanel.track("Attempted to Change Email Address");

        Intent intent = new Intent(getActivity(), ChangeEmailSettingActivity.class);
        Navigation.pushIntent(getActivity(),intent);
    }

    @OnClick(R.id.changePwLayout)
    public void changePwLayoutClick(View view) {
        Intent intent = new Intent(getActivity(), ChangePwSettingActivity.class);
        Navigation.pushIntent(getActivity(),intent);
    }

    @OnClick(R.id.resetPwLayout)
    public void resetPwLayoutClick(View view) {
        Intent intent = new Intent(getActivity(), ResetPwSettingActivity.class);
        Navigation.pushIntent(getActivity(),intent);
    }


    @OnClick(R.id.copyDBBtn)
    public void copyDBBtnClick(View view) {
        DBUtil.copyDB(getActivity());
    }

    @OnClick(R.id.broadcastBtn)
    public void inviteFollowBtnClick(View view) {

        // Mixpanel
        MixpanelAPI mixpanel = MixpanelAPI.getInstance(getContext(), AppConfig.getMixpanelToken());
        mixpanel.track("View Invite to Follow @ Settings");

        ResponseLoginSocial.Content userContent = CoreData.getUserContent(getActivity());
        if (userContent.agentProfile != null && userContent.agentProfile.agentListing != null) {    // Agent with listing

            if (ContextCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.READ_CONTACTS)
                    != PackageManager.PERMISSION_GRANTED) {
                InviteToFollowActivity.start(getActivity(), InviteToFollowActivity.InviteToFollowActivityType.NORMAL);
            } else {
                //granted permission
                ConnectAgentActivity.start(getActivity(), ConnectAgentActivity.ConnectAgentActivityType.BROADCAST);
            }

        } else if (userContent.agentProfile != null) {  // Agent without listing

            InviteToFollowActivity.start(getActivity(), InviteToFollowActivity.InviteToFollowActivityType.REDIRECT_TO_CREATE_LISTING);

        } else {    // Non-agent

            InviteToFollowActivity.start(getActivity(), InviteToFollowActivity.InviteToFollowActivityType.REDIRECT_TO_BECOME_AN_AGENT);
        }
    }

    private void switchToMeTab() {

        if (getActivity() instanceof MainActivityTabBase) {

            MainActivityTabBase mainActivityTabBase = (MainActivityTabBase) getActivity();
            mainActivityTabBase.tabMenu.getMeTabIcon().setSelected(true);
            mainActivityTabBase.tabMenu.getSettingTabIcon().setSelected(false);
            mainActivityTabBase.onTabClicked(mainActivityTabBase.tabMenu.getMeTabIcon(), false);
        }
    }


    @OnClick(R.id.connectAgentBtn)
    public void inviteDownloadBtnClick(View view) {

        // Mixpanel
        MixpanelAPI mixpanel = MixpanelAPI.getInstance(getContext(), AppConfig.getMixpanelToken());
        mixpanel.track("View Invite to Download @ Settings");

        ConnectAgentActivity.start(getActivity(), ConnectAgentActivity.ConnectAgentActivityType.NORMAL);
    }

    @OnClick(R.id.inviteChatBtn)
    public void inviteChatBtnClick(View view) {

        // Mixpanel
        MixpanelAPI mixpanel = MixpanelAPI.getInstance(getContext(), AppConfig.getMixpanelToken());
        mixpanel.track("View Invite to Chat @ Settings");

        InviteToChatActivity.start(getActivity());
    }


    @Override
    public void settingViewIsReady() {
        if (SettingFragment.fragmentIsRunning) {
            ((Activity) SettingFragment.context).runOnUiThread(new Runnable() {
                @Override
                public void run() {
//                    initSetting();
                }
            });
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == InviteToFollowActivity.InviteToFollowActivityType.REDIRECT_TO_BECOME_AN_AGENT.getValue()) {

                redirectToBecomeAnAgent();

            } else if (requestCode == InviteToFollowActivity.InviteToFollowActivityType.REDIRECT_TO_CREATE_LISTING.getValue()) {

                redirectToCreateListing();
            }
        }
    }

    public void redirectToBecomeAnAgent() {

        this.switchToMeTab();

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                MainActivityTabBase mainActivityTabBase = (MainActivityTabBase) getActivity();
                mainActivityTabBase.mMeTabContainer.userProfileFragment.beAnAgentBtn.performClick();
            }
        }, 1000);
    }

    public void redirectToCreateListing() {

        this.switchToMeTab();

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                MainActivityTabBase mainActivityTabBase = (MainActivityTabBase) getActivity();
                mainActivityTabBase.mMeTabContainer.notCompleteAgentProfileFragment.createUpdatePostFragment.createPostBtn.performClick();
            }
        }, 1000);
    }
}
