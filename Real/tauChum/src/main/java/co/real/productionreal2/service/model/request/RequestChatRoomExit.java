package co.real.productionreal2.service.model.request;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.model.InputRequest;
import co.real.productionreal2.service.OperationService;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.response.ResponseChatRoomStatus;
import co.real.productionreal2.util.NetworkUtil;
import co.real.productionreal2.view.Dialog;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by hohojo on 18/11/2015.
 */
public class RequestChatRoomExit extends RequestBase {

    public RequestChatRoomExit(int memId, String session) {
        super(memId, session);
    }

    @SerializedName("DialogID")
    public String dialogId;

    @SerializedName("ExitStatus")
    public int exitStatus;

    public void callChatRoomExitApi(final Context context, final ApiCallback callback) {
        String json = new Gson().toJson(this);
        final OperationService weatherService = new OperationService(context);
        weatherService.getCoffeeService().chatRoomExit(new InputRequest(json), new Callback<ApiResponse>() {
            @Override
            public void success(ApiResponse apiResponse, Response response) {
                ResponseChatRoomStatus responseGson = new Gson().fromJson(apiResponse.jsonContent, ResponseChatRoomStatus.class);
                if (responseGson.errorCode == 4) {
                    Dialog.accessErrorForceLogoutDialog(context).show();
                } else if (responseGson.errorCode == 26) {
                    Dialog.suspendedErrorForceLogoutDialog(context).show();
                } else if (responseGson.errorCode != 0 && responseGson.errorCode != 20 && responseGson.errorCode != 21 &&
                        responseGson.errorCode != 22 && responseGson.errorCode != 23) {
                    callback.failure(responseGson.errorMsg);
                } else {
                    callback.success(apiResponse);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                callback.failure("");
                NetworkUtil.showNoNetworkMsg(context);
            }
        });
    }
}
