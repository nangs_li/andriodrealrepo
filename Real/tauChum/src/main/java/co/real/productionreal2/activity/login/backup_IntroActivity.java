package co.real.productionreal2.activity.login;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.Toast;
import android.widget.VideoView;

import com.mixpanel.android.mpmetrics.MixpanelAPI;

import java.util.Locale;

import butterknife.ButterKnife;
import butterknife.InjectView;
import co.real.productionreal2.Navigation;
import co.real.productionreal2.R;
import co.real.productionreal2.callback.LoginDbViewListener;
import co.real.productionreal2.common.LocalStorageHelper;
import co.real.productionreal2.config.AppConfig;
import co.real.productionreal2.daomanager.ConfigDataBaseManager;
import co.real.productionreal2.daomanager.DatabaseManager;
import co.real.productionreal2.login.FacebookLoginController;
import co.real.productionreal2.login.GoogleplusLoginController;
import co.real.productionreal2.login.TwitterLoginController;
import co.real.productionreal2.login.WeiboLoginController;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;
import co.real.productionreal2.util.AppUtil;
import co.real.productionreal2.util.DBUtil;

;

/**
 * Created by hohojo on 24/8/2015.
 */
public class backup_IntroActivity extends Activity implements LoginDbViewListener {
    private static final String TAG = "IntroActivity";
    //    @InjectView(R.id.twitterLoginBtn)
//    TwitterLoginButton twitterLoginBtn;
    @InjectView(R.id.fl_top_layer)
    FrameLayout flTopLayer;
    @InjectView(R.id.vv_bg)
    VideoView vvBg;
    private FacebookLoginController facebookLoginController;
    private GoogleplusLoginController googleplusLoginController;
    private WeiboLoginController weiboLoginController;
    private TwitterLoginController twitterLoginController;
    private static ProgressDialog ringProgressDialog;


    private boolean mIsResolving = false;/* Is there a ConnectionResult resolution in progress? */
    private boolean mShouldResolve = false;/* Should we automatically resolve ConnectionResults when possible? */
    /* Client used to interact with Google APIs. */
    private int mSignInProgress;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DBUtil.copyDB(this);
        setContentView(R.layout.activity_intro);

        ButterKnife.inject(this);

        //if no previous setting, load default language
        if (LocalStorageHelper.getCurrentLocale(this)==null)
        AppUtil.setLocale(this, Locale.getDefault());


    }

    @Override
    protected void onResume() {
        super.onResume();
        initView();
    }

    private void initView() {

        Uri uri= Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.real_for_android_1080p_lower);
        vvBg.setMediaController(null);
        vvBg.setVideoURI(uri);
        vvBg.start();

        //Video Loop
        vvBg.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                vvBg.start(); //need to make transition seamless.
            }
        });


        Animation a = AnimationUtils.loadAnimation(this, R.anim.fade_in);
        a.reset();
        a.setDuration(2500);
        flTopLayer.clearAnimation();
        flTopLayer.startAnimation(a);
    }


    public void signinClick(View view) {

        // Mixpanel
        MixpanelAPI mixpanel = MixpanelAPI.getInstance(this, AppConfig.getMixpanelToken());
        mixpanel.track("Attempted to Log In");

        goToSignIn();
    }

    public void registerClick(View view) {
        // Mixpanel
        MixpanelAPI mixpanel = MixpanelAPI.getInstance(this, AppConfig.getMixpanelToken());
        mixpanel.track("Attempted to Sign Up");

        goToRegister();
    }

    private void goToSignIn() {
        Intent intent = new Intent(this, RealLoginActivity.class);
        Navigation.pushIntent(this,intent);
    }

    private void goToRegister() {
        Intent intent = new Intent(this, SignUpActivity.class);
        Navigation.pushIntent(this,intent);
    }

    public void realClick(View view) {

    }

    private void showProgressDialog() {
//        loginProgressBar.setVisibility(View.VISIBLE);
//        if (ringProgressDialog == null)
//            return;
        if (ringProgressDialog != null && ringProgressDialog.isShowing())
            return;
        ringProgressDialog = ProgressDialog.show(this, getResources().getString(R.string.alert__Initializing), getResources().getString(R.string.common__signin));
    }

    @Override
    public void updateLoginDbAndToMainTabActivity(ResponseLoginSocial.Content content) {
        LocalStorageHelper.setUpByLogin(this, content);

        ConfigDataBaseManager configDataBaseManager = ConfigDataBaseManager.getInstance(this);
        DatabaseManager.getInstance(this).dbActionForLogin(content);

        configDataBaseManager.dbActionMain(content.memId);
//        loginProgressBar.setVisibility(View.GONE);
        if (ringProgressDialog != null) {
            ringProgressDialog.dismiss();
            ringProgressDialog.cancel();
            ringProgressDialog = null;
        }

        AppUtil.goToMainTabActivity(this, content);
    }

    @Override
    public void loginFail(String failMsg) {
        if (ringProgressDialog != null && ringProgressDialog.isShowing()) {
            ringProgressDialog.dismiss();
            ringProgressDialog = null;
        }
        Toast.makeText(getApplicationContext(), failMsg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void loginCancel() {
        if (ringProgressDialog != null && ringProgressDialog.isShowing()) {
            ringProgressDialog.dismiss();
            ringProgressDialog = null;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i(TAG, "onActivityResult " + requestCode + " " + resultCode);
        if (resultCode == RESULT_CANCELED) {
            if (ringProgressDialog != null) {
                ringProgressDialog.dismiss();
                ringProgressDialog.cancel();
                ringProgressDialog = null;
            }
        }
        switch (requestCode) {
            case FacebookLoginController.REQUEST_CODE:
                facebookLoginController.onActivityResult(requestCode, resultCode, data);
                break;
//            case TwitterLoginController.REQUEST_CODE:
//                twitterLoginController.onActivityResult(requestCode, resultCode, data);
//                break;
//            case WeiboLoginController.REQUEST_CODE:
//                weiboLoginController.onActivityResult(requestCode, resultCode, data);
//                break;
            default:
                googleplusLoginController.connectGoogleApiClient();
                break;

        }
//        if (resultCode == RESULT_OK) {
//            if (requestCode == FacebookLoginController.REQUEST_CODE)
//                facebookLoginController.onActivityResult(requestCode, resultCode, data);
//            else if (requestCode == TwitterLoginController.REQUEST_CODE)
//                twitterLoginBtn.onActivityResult(requestCode, resultCode, data);
//            else if (requestCode == WeiboLoginController.REQUEST_CODE)
//                WeiboLoginController.getInstance(this, weiboLoginBtn).onActivityResult(requestCode, resultCode, data);
//        } else {
//            if (ringProgressDialog != null) {
//                ringProgressDialog.dismiss();
//                ringProgressDialog.cancel();
//                ringProgressDialog = null;
//            }
//        }

    }

}
