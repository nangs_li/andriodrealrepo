package co.real.productionreal2.service.model.request;

import android.content.Context;
import android.os.Parcel;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.model.AgentListing;
import co.real.productionreal2.model.InputRequest;
import co.real.productionreal2.service.AdminService;
import co.real.productionreal2.service.model.AgentExp;
import co.real.productionreal2.service.model.AgentLanguage;
import co.real.productionreal2.service.model.AgentPastClosing;
import co.real.productionreal2.service.model.AgentSpecialty;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.response.ResponseBase;
import co.real.productionreal2.util.NetworkUtil;
import co.real.productionreal2.util.TimeUtils;
import co.real.productionreal2.view.Dialog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import retrofit.RetrofitError;
import retrofit.client.Response;

public class RequestAgentReg extends RequestBase {

    public RequestAgentReg(int memId, String session) {
        super(memId, session);
        agentLangList = new ArrayList<AgentLanguage>();
        agentExpList = new ArrayList<AgentExp>();
        agentPastCloseList = new ArrayList<AgentPastClosing>();
        agentSpecialityList = new ArrayList<AgentSpecialty>();
    }

    public RequestAgentReg(Parcel source) {

        super(0, null);
        agentLangList = new ArrayList<AgentLanguage>();
        agentExpList = new ArrayList<AgentExp>();
        agentPastCloseList = new ArrayList<AgentPastClosing>();
        agentSpecialityList = new ArrayList<AgentSpecialty>();

        source.readTypedList(agentLangList, AgentLanguage.CREATOR);
        source.readTypedList(agentExpList, AgentExp.CREATOR);
        source.readTypedList(agentPastCloseList, AgentPastClosing.CREATOR);
        source.readTypedList(agentSpecialityList, AgentSpecialty.CREATOR);


        agentListing = source.readParcelable(AgentListing.class.getClassLoader());
//		 followingMemId = source.readInt();
        agentLicNum = source.readString();


        agentName = source.readString();
        agentPhotoURL = source.readString();
        followerCount = source.readInt();

        followingCount = source.readInt();
    }


    @SerializedName("QBID")
    public String qbId = "";

    @SerializedName("MemberName")
    public String agentName = "";

    @SerializedName("AgentPhotoURL")
    public String agentPhotoURL = "";

    @SerializedName("followerCount")
    public int followerCount;

    @SerializedName("FollowingCount")
    public int followingCount;

    @SerializedName("IsFollowing")
    public int isFollowing = 0;


    @SerializedName("AgentLicenseNumber")
    public String agentLicNum;

    @SerializedName("AgentLanguage")
    public List<AgentLanguage> agentLangList;

    @SerializedName("AgentExperience")
    public List<AgentExp> agentExpList;

    @SerializedName("AgentPastClosing")
    public List<AgentPastClosing> agentPastCloseList;

    @SerializedName("AgentSpecialty")
    public List<AgentSpecialty> agentSpecialityList;

    @SerializedName("AgentListing")
    public AgentListing agentListing;


//    public static final Parcelable.Creator<RequestAgentReg> CREATOR = new Creator<RequestAgentReg>() {
//        public RequestAgentReg createFromParcel(Parcel source) {
//            return new RequestAgentReg(source);
//        }
//
//        @Override
//        public RequestAgentReg[] newArray(int size) {
//            return new RequestAgentReg[size];
//        }
//    };
//
//    @Override
//    public int describeContents() {
//        return 0;
//    }
//
//    @Override
//    public void writeToParcel(Parcel parcel, int flags) {
//        parcel.writeTypedList(agentLangList);
//        parcel.writeTypedList(agentExpList);
//        parcel.writeTypedList(agentPastCloseList);
//        parcel.writeTypedList(agentSpecialityList);
//
//        parcel.writeParcelable(agentListing, flags);
//
//
////		parcel.writeInt(followingMemId);
//        parcel.writeString(agentLicNum);
//
//        parcel.writeString(agentName);
//        parcel.writeString(agentPhotoURL);
//
//        parcel.writeInt(followerCount);
//        parcel.writeInt(followingCount);
//
//        /**
//         *  agentName = source.readString();
//         agentPhotoURL = source.readString();
//         followerCount = source.readInt();
//
//         followingCount = source.readInt();
//         */
//    }

    public void registerAgentService(final Context context, final RequestAgentReg agentRegRequest, final ApiCallback callback) {
        String json = new Gson().toJson(agentRegRequest);
        final AdminService weatherService = new AdminService(context);
        weatherService.getCoffeeService().agentProfileReg(
                new InputRequest(json),
                new retrofit.Callback<ApiResponse>() {
                    @Override
                    public void failure(RetrofitError error) {
                        registerAgentService(context, agentRegRequest, callback);
                        callback.failure("");
                        NetworkUtil.showNoNetworkMsg(context);
                    }

                    @Override
                    public void success(ApiResponse apiResponse, Response response) {
                        ResponseBase responseGson = new Gson().fromJson(apiResponse.jsonContent, ResponseBase.class);
                        if (responseGson.errorCode == 4) {
                            Dialog.accessErrorForceLogoutDialog(context).show();
                        } else if (responseGson.errorCode == 26) {
                            Dialog.suspendedErrorForceLogoutDialog(context).show();
                        } else if (responseGson.errorCode != 0 && responseGson.errorCode != 20 && responseGson.errorCode != 21 &&
                                responseGson.errorCode != 22 && responseGson.errorCode != 23) {
                            callback.failure(responseGson.errorMsg);
                        } else {
                            callback.success(apiResponse);
                        }
                        Log.w("RequestAgentReg", "apiResponse.jsonContent = " + apiResponse.jsonContent);
                        Log.w("RequestAgentReg", "responseGson = " + responseGson);

                    }

                }
        );

    }

    @Override
    public String toString() {
        return "RequestAgentReg{" +
                "qbId='" + qbId + '\'' +
                ", agentName='" + agentName + '\'' +
                ", agentPhotoURL='" + agentPhotoURL + '\'' +
                ", followerCount=" + followerCount +
                ", followingCount=" + followingCount +
                ", isFollowing=" + isFollowing +
                ", agentLicNum='" + agentLicNum + '\'' +
                ", agentLangList=" + agentLangList +
                ", agentExpList=" + agentExpList +
                ", agentPastCloseList=" + agentPastCloseList +
                ", agentSpecialityList=" + agentSpecialityList +
                ", agentListing=" + agentListing +
                '}';
    }

    public void sortAgentExps() {
        final Comparator<AgentExp> currentJobComparator = new Comparator<AgentExp>() {
            @Override
            public int compare(AgentExp lhs, AgentExp rhs) {
                int result = 0;
                if (rhs.isCurrentJob == lhs.isCurrentJob) {
                    result = 0;
                } else if (rhs.isCurrentJob > lhs.isCurrentJob) {
                    result = 1;
                } else {
                    result = -1;
                }
                return result;
            }
        };

        final Comparator<AgentExp> endDateComparator = new Comparator<AgentExp>() {
            @Override
            public int compare(AgentExp lhs, AgentExp rhs) {
                if (currentJobComparator.compare(lhs, rhs) != 0) {
                    return currentJobComparator.compare(lhs, rhs);
                }
                Date lshDate = TimeUtils.stringToDate(lhs.endDateString);
                Date rhsDate = TimeUtils.stringToDate(rhs.endDateString);
                SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
                if (fmt.format(lshDate).equals(fmt.format(rhsDate))) {
                    return 0;
                }
                int result = (rhsDate.after(lshDate)) ? 1 : -1;
                return result;
            }
        };
        final Comparator<AgentExp> startDateComparator = new Comparator<AgentExp>() {
            @Override
            public int compare(AgentExp lhs, AgentExp rhs) {
                if (currentJobComparator.compare(lhs, rhs) != 0) {
                    return currentJobComparator.compare(lhs, rhs);
                } else {
                    if (endDateComparator.compare(lhs, rhs) != 0) {
                        return endDateComparator.compare(lhs, rhs);
                    }
                }
                Date lshDate = TimeUtils.stringToDate(lhs.startDateString);
                Date rhsDate = TimeUtils.stringToDate(rhs.startDateString);
                SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
                if (fmt.format(lshDate).equals(fmt.format(rhsDate))) {
                    return 0;
                }
                int result = (rhsDate.after(lshDate)) ? 1 : -1;
                return result;
            }
        };

        ArrayList<Comparator> comparators = new ArrayList<>();
        comparators.add(currentJobComparator);
        comparators.add(startDateComparator);
        comparators.add(endDateComparator);
        for (Comparator<AgentExp> comparator : comparators) {
            Collections.sort(agentExpList, comparator);
            Log.e("sort", agentExpList.toString());
        }
    }
}
