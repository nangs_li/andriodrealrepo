package co.real.productionreal2.service.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import co.real.productionreal2.service.model.AgentProfiles;

public class ResponseChangePhoneNum extends ResponseBase{

	
	@SerializedName("Content")
    public Content content; 

    public class Content {
    	@SerializedName("PhoneRegID")
        public long phoneRegId;
    	
    }
	
    
    
	
}
