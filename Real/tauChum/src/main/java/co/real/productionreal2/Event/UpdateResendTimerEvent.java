package co.real.productionreal2.Event;

/**
 * Created by kelvinsun on 16/6/16.
 */
public class UpdateResendTimerEvent {
    public final long min;
    public final long sec;

    public UpdateResendTimerEvent(long min, long sec) {
        this.min = min;
        this.sec = sec;
    }
}
