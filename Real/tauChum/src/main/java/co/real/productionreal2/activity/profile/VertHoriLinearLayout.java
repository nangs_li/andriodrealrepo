package co.real.productionreal2.activity.profile;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import co.real.productionreal2.R;
import co.real.productionreal2.callback.LangSpecDelCallback;
import co.real.productionreal2.service.model.AgentLanguage;
import co.real.productionreal2.service.model.AgentSpecialty;

import java.util.ArrayList;

/**
 * Created by hohojo on 15/9/2015.
 */
public class VertHoriLinearLayout {
    private static final String TAG = "VertHoriLinearLayout";
    private int totalWidth;
    private ArrayList<String> contentList;
    private int langOrSpecType;
    private LinearLayout rootLinearLayout;
    private ArrayList<LinearLayout> horiLinearLayoutList;
    private Context context;

    private int currentUsedWidth = 0;
    private LangSpecDelCallback langSpecDelCallback;
    private LayoutInflater layoutInflater;

    public static final int PROFILE_LANG = 0;
    public static final int PROFILE_SPEC = 1;
    public static final int SPEC = 2;
    public static final int VIEW_PROFILE_LANG = 3;
    public static final int VIEW_PROFILE_SPEC = 4;
    private static final int marginSize = 5;

    public VertHoriLinearLayout(Context context, LinearLayout rootLinearLayout,
                                ArrayList<String> contentList, int totalWidth, int langOrSpecType, LangSpecDelCallback langSpecDelCallback) {
        this.context = context;
        this.rootLinearLayout = rootLinearLayout;
        this.contentList = contentList;
        this.totalWidth = totalWidth;
        this.langOrSpecType = langOrSpecType;
        this.langSpecDelCallback = langSpecDelCallback;
        Log.w(TAG, "totalWidth = " + totalWidth);

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        horiLinearLayoutList = new ArrayList<>();
        initCurrentHoriLinearLayout();
        notifyDataSetChanged();
    }

    public ArrayList<String> addViewAndGetContentList(String content) {
        contentList.add(content);
        notifyDataSetChanged();

        return contentList;
    }

    public ArrayList<String> removeViewAndGetContentList(int tagNum) {
        contentList.remove(tagNum);
        notifyDataSetChanged();

        return contentList;
    }

    private void resetLayoutAndCurrentUsedWidth() {
        rootLinearLayout.removeAllViews();
        for (int i = 0; i < horiLinearLayoutList.size(); i++) {
            horiLinearLayoutList.get(i).removeAllViews();
        }
        currentUsedWidth = 0;
    }

    private void notifyDataSetChanged() {
        resetLayoutAndCurrentUsedWidth();
        //
        for (int i = 0; i < contentList.size(); i++) {
            addViewThenUpdateCurrentUsedWidth(contentList.get(i), i);
        }

        for (int i = 0; i < horiLinearLayoutList.size(); i++) {
            rootLinearLayout.addView(horiLinearLayoutList.get(i));
        }
    }

    private void initCurrentHoriLinearLayout() {
        LinearLayout currentHoriLinearLayout = new LinearLayout(context);
        currentHoriLinearLayout.setOrientation(LinearLayout.HORIZONTAL);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);

        currentHoriLinearLayout.setLayoutParams(layoutParams);
        horiLinearLayoutList.add(currentHoriLinearLayout);
    }

    private void addViewThenUpdateCurrentUsedWidth(String content, final int tagNum) {

        LinearLayout newView = (LinearLayout) layoutInflater.inflate(R.layout.vert_hori_linearlayout_item, null);
        newView.setTag(tagNum);

        TextView contentTextView = (TextView) newView.findViewById(R.id.contentTextView);
        ImageButton deleteBtn = (ImageButton) newView.findViewById(R.id.deleteBtn);
        contentTextView.setText(content);

        if (langOrSpecType == VIEW_PROFILE_LANG || langOrSpecType == VIEW_PROFILE_SPEC)
            deleteBtn.setVisibility(View.GONE);
        else
            deleteBtn.setVisibility(View.VISIBLE);

        newView.measure(0, 0);
        int newViewWidth = newView.getMeasuredWidth();

        if (totalWidth < newViewWidth + currentUsedWidth) {
            initCurrentHoriLinearLayout();
            horiLinearLayoutList.get(horiLinearLayoutList.size() - 1).addView(newView);
            //update current used width
            currentUsedWidth = newViewWidth + marginSize;
            Log.w(TAG, "new line currentUsedWidth = " + currentUsedWidth);

        } else {
            horiLinearLayoutList.get(horiLinearLayoutList.size() - 1).addView(newView);
            //update current used width
            currentUsedWidth = currentUsedWidth + newViewWidth + marginSize;
            Log.w(TAG, "same line currentUsedWidth = " + currentUsedWidth);
        }

        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                langSpecDelCallback.onLangSpecDel(tagNum, langOrSpecType);
            }
        });
    }

    public static ArrayList<String> agentLangListToStringList(ArrayList<AgentLanguage> agentLangList) {
        ArrayList<String> agentLangStringList = new ArrayList<>();
        for (int i = 0; i < agentLangList.size(); i++) {
            agentLangStringList.add(agentLangList.get(i).lang);
        }
        return agentLangStringList;
    }


    public static ArrayList<String> agentSpecListToStringList(ArrayList<AgentSpecialty> agentSpecList) {
        ArrayList<String> agentSpecStringList = new ArrayList<>();
        for (int i = 0; i < agentSpecList.size(); i++) {
            agentSpecStringList.add(agentSpecList.get(i).specialty);
        }
        return agentSpecStringList;
    }

    public static ArrayList<AgentSpecialty> stringListToAgentSpecList(ArrayList<String> agentSpecStringList) {
        ArrayList<AgentSpecialty> agentSpecList = new ArrayList<>();
        for (int i = 0; i < agentSpecStringList.size(); i++) {
            agentSpecList.add(new AgentSpecialty(agentSpecStringList.get(i), 0));
        }
        return agentSpecList;
    }
}
