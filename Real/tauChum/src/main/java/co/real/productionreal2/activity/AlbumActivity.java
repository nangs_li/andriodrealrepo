package co.real.productionreal2.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import co.real.productionreal2.Constants;
import co.real.productionreal2.R;
import co.real.productionreal2.dao.account.DBQBChatMessage;
import co.real.productionreal2.daomanager.DatabaseManager;
import co.real.productionreal2.util.ImageUtil;
import me.iwf.photopicker.fragment.ImagePagerFragment;

/**
 * Created by hohojo on 12/8/2015.
 */
public class AlbumActivity extends BaseFragmentActivity {
    private ImagePagerFragment imagePagerFragment;
    @InjectView(R.id.rootView)
    LinearLayout rootView;
    @InjectView(R.id.container)
    View container;
//    @InjectView(R.id.photoNoTextView)
//    TextView photoNoTextView;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album);


        ButterKnife.inject(this);

        Intent intent = getIntent();
        String dialogId = intent.getStringExtra(Constants.EXTRA_DIALOG_ID);
        String photoName = intent.getStringExtra(Constants.EXTRA_PHOTO_NAME);

        List<String> photos = getPhotosByDialogId(dialogId);
//        int photoPosition = getPhotoPosition(photos, photoName);
//        photoNoTextView.setText(photoPosition + " of " + photos.size());

        int [] screenLocation = new int[2];
        container.getLocationOnScreen(screenLocation);
        ImagePagerFragment imagePagerFragment =
                ImagePagerFragment.newInstance(photos, getPhotoPosition(photos, photoName), screenLocation,
                        ImageUtil.getScreenWidth(this), ImageUtil.getScreenHeight(this));
        addImagePagerFragment(imagePagerFragment);
    }

    private List<String> getPhotosByDialogId(String dialogId) {
        List<DBQBChatMessage> dbqbChatMessageList = DatabaseManager.getInstance(this).listPhotoQbMessageByDialogId(dialogId);
        List<String> photos = new ArrayList<>();
        for (int j = 0; j < dbqbChatMessageList.size(); j++) {
            String photoPath = null;
            if (dbqbChatMessageList.get(j).getPhotoLocalPath() != null) {
                String filePath = dbqbChatMessageList.get(j).getPhotoLocalPath();
                File file = new File(filePath);
                if (file.exists())
                    photoPath = filePath;
            }

            if(photoPath == null && dbqbChatMessageList.get(j).getPhotoURL() != null){
                photoPath = dbqbChatMessageList.get(j).getPhotoURL();
            }

            if(photoPath != null || !photoPath.isEmpty()){
                photos.add(photoPath);
            }
        }
        return photos;
    }

    private int getPhotoPosition(List<String> photos, String photoName) {
        for (int i = 0; i < photos.size(); i++) {
            if (photos.get(i).equalsIgnoreCase(photoName)) {
                return i;
            }
        }
        return 0;
    }

    public void addImagePagerFragment(ImagePagerFragment imagePagerFragment) {
        this.imagePagerFragment = imagePagerFragment;
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, this.imagePagerFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onBackPressed() {
        finish();

    }
}
