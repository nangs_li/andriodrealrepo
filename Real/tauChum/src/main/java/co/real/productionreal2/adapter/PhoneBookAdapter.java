package co.real.productionreal2.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import co.real.productionreal2.R;
import co.real.productionreal2.dao.account.DBPhoneBook;
import co.real.productionreal2.daomanager.DatabaseManager;
import co.real.productionreal2.service.model.RePhonebookItem;
import co.real.productionreal2.view.FollowButton;
import widget.RoundedImageView;

/**
 * Created by kelvinsun on 7/6/16.
 */
public class PhoneBookAdapter extends BaseAdapter implements Filterable {
    private List<DBPhoneBook> phonebookItemList = new ArrayList<>();
    private LayoutInflater inflater;
    protected Context mContext;
    protected PhoneBookAdapterListener mListener;
    private int inviteCheckedCount = 0;
    private List<String> existingAgentPhoneNoList = new ArrayList<>();
    private List<Integer> existingAgentIdList = new ArrayList<>();
    private List<DBPhoneBook> invitePhoneNoList = new ArrayList<>();

    private List<DBPhoneBook> filteredContactList = new ArrayList<>();

    public static interface PhoneBookAdapterListener {

        public ArrayList<DBPhoneBook> getPhoneBookList();

        public void onSelectItem(List<DBPhoneBook> invitePhoneNoList);

        public void searchDone(int size);
    }

    public PhoneBookAdapter(Context context, List<DBPhoneBook> phonebookItemList, PhoneBookAdapterListener mListener) {

        inflater = LayoutInflater.from(context);
        this.mContext = context;
        this.phonebookItemList = phonebookItemList;
        this.filteredContactList = phonebookItemList;
        this.mListener = mListener;
        Log.d("ConnectAgentAdapter", "ConnectAgentAdapter: " + phonebookItemList.size());
    }


    @Override
    public int getCount() {
        return filteredContactList.size();
    }

    @Override
    public Object getItem(int position) {
        return filteredContactList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        final PhoneBookViewHolder holder;
        LayoutInflater layoutInflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_item_following,viewGroup,false);
            holder = createViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (PhoneBookViewHolder) convertView.getTag();
        }

        final DBPhoneBook phonebookItem = filteredContactList.get(position);

        String name = phonebookItem.getName();
        final String phoneNo = phonebookItem.getCountryCode() + " " + phonebookItem.getPhoneNumber();

        //set style
        holder.tvName.setTextColor(mContext.getResources().getColor(R.color.white));
        holder.tvRegion.setTextColor(mContext.getResources().getColor(R.color.dark_grey));

        holder.tvName.setText(name);
        holder.tvRegion.setText(phoneNo);
        holder.rlImage.setVisibility(View.GONE);
        holder.btnFollower.setVisibility(View.GONE);
        holder.checkOption.setVisibility(View.VISIBLE);

        holder.checkOption.setCheckMarkDrawable(phonebookItem.getHasSent() ?
                R.drawable.single_choice_selector_yellow_with_flag : R.drawable.single_choice_selector_yellow);


        holder.llWhole.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (phonebookItem.getHasChecked()) {
                    if (inviteCheckedCount > 0) {
                        inviteCheckedCount--;
                        invitePhoneNoList.remove(phonebookItem);
                    }
                    phonebookItem.setHasChecked(false);
                } else {
                    inviteCheckedCount++;
                    invitePhoneNoList.add(phonebookItem);
                    phonebookItem.setHasChecked(true);
                }
                DatabaseManager.getInstance(mContext).updatePhoneBook(phonebookItem);
                mListener.onSelectItem(invitePhoneNoList);
                notifyDataSetChanged();
            }
        });

        holder.checkOption.setChecked(phonebookItem.getHasChecked() == null ? false : phonebookItem.getHasChecked());


        return convertView;
    }


    private PhoneBookViewHolder createViewHolder(View v) {
        PhoneBookViewHolder holder = new PhoneBookViewHolder();
        holder.llWhole = (LinearLayout) v.findViewById(R.id.ll_whole_cell);
        holder.tvName = (TextView) v.findViewById(R.id.tv_following_name);
        holder.tvRegion = (TextView) v.findViewById(R.id.tv_following_region);
        holder.rlImage = (RelativeLayout) v.findViewById(R.id.rl_following_image);
        holder.ivImage = (RoundedImageView) v.findViewById(R.id.iv_following_image);
        holder.btnFollower = (FollowButton) v.findViewById(R.id.btn_to_follow);
        holder.checkOption = (CheckedTextView) v.findViewById(R.id.checked_option);
        holder.divider = (View) v.findViewById(R.id.line_horizontal);
        return holder;
    }

    class PhoneBookViewHolder {
        LinearLayout llWhole;
        TextView tvName;
        TextView tvRegion;
        RoundedImageView ivImage;
        RelativeLayout rlImage;
        FollowButton btnFollower;
        CheckedTextView checkOption;
        View divider;
    }


    /**
     * Filter result with key word
     *
     * @return
     */
    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

                filteredContactList = (List<DBPhoneBook>) results.values;
                notifyDataSetChanged();

                mListener.searchDone(filteredContactList.size());
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                FilterResults results = new FilterResults();
                ArrayList<DBPhoneBook> FilteredArrayNames = new ArrayList<DBPhoneBook>();

                // perform your search here using the searchConstraint String.
                constraint = constraint.toString().toLowerCase();
                for (int i = 0; i < mListener.getPhoneBookList().size(); i++) {
                    DBPhoneBook obj = mListener.getPhoneBookList().get(i);
                    String dataNames = null;
                    dataNames = obj.getName();
                    if (dataNames != null) {
                        if (dataNames.toLowerCase().contains(constraint.toString())) {
                            FilteredArrayNames.add(obj);
                        }
                    }
                }

                results.count = FilteredArrayNames.size();
                results.values = FilteredArrayNames;
                Log.e("VALUES", results.values.toString());

                return results;
            }
        };

        return filter;
    }
}