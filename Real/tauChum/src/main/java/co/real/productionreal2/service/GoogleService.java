package co.real.productionreal2.service;

import android.content.Context;

import co.real.productionreal2.Constants;
import co.real.productionreal2.config.AppConfig; ;
import co.real.productionreal2.service.model.ApiResponseGeocode;
import co.real.productionreal2.service.model.ApiResponseLangDetect;
import co.real.productionreal2.service.model.ApiResponsePlace;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;

public class GoogleService {


    public final GeocodeService geocodeService;
    public final TranslationService translationService;
    private static final String WEB_SERVICE_MAP_BASE_URL = AppConfig.getGoogleMapApiUrl();
    private static final String WEB_SERVICE_BASE_URL = AppConfig.getGoogleApiUrl();
    public static final String API_KEY = AppConfig.getGoogleApiKey();


    //AIzaSyANdE9cF543nn6yBiYDTUAuQaM5D-2vLtQ


    public GoogleService(Context context) {
        RequestInterceptor requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestInterceptor.RequestFacade request) {
                request.addHeader("Accept", "application/json");
                request.addHeader("Content-type", "application/json");
            }
        };

        RestAdapter restMapAdapter = new RestAdapter.Builder()
                .setEndpoint(WEB_SERVICE_MAP_BASE_URL)
                .setRequestInterceptor(requestInterceptor)
                .setLogLevel(Constants.isDevelopment? RestAdapter.LogLevel.FULL : RestAdapter.LogLevel.NONE)
                .build();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(WEB_SERVICE_BASE_URL)
                .setRequestInterceptor(requestInterceptor)
                .setLogLevel(Constants.isDevelopment? RestAdapter.LogLevel.FULL : RestAdapter.LogLevel.NONE)
                .build();

        geocodeService = restMapAdapter.create(GeocodeService.class);
        translationService = restAdapter.create(TranslationService.class);

    }

    public interface GeocodeService {

//		@FormUrlEncoded
//    	@POST("/maps/api/geocode/json")
//        public void geocodeRequest(
//        		@Field("address") String first, 
//        		@Field("sensor") Boolean sensor, 
//        		retrofit.Callback<ApiResponse> callback);

        //search without language
        @POST("/maps/api/geocode/json")  //@POST("/posts/add.json")
        public void geocodeRequest(
                @Query("address") String first,
                @Query("sensor") Boolean sensor,
                retrofit.Callback<ApiResponseGeocode> callback);

        //search with language
        @POST("/maps/api/geocode/json")  //@POST("/posts/add.json")
        public void geocodeRequest(
                @Query("address") String first,
                @Query("sensor") Boolean sensor,
                @Query("language") String lang,
                retrofit.Callback<ApiResponseGeocode> callback);

        //use received placeId to perform search
        @POST("/maps/api/geocode/json")  //@POST("/posts/add.json")
        public void geocodeWithLangRequest(
                @Query("place_id") String placeId,
                @Query("key") String key,
                @Query("language") String lang,
                retrofit.Callback<ApiResponseGeocode> callback);

        //autocomplete address
        @POST("/maps/api/place/autocomplete/json")  //@POST("/posts/add.json")
        public void geocodeRequest(
                @Query("input") String input,
                @Query("radius") int radius,
                @Query("key") String key,
                retrofit.Callback<ApiResponsePlace> callback);

        @POST("/maps/api/geocode/json")  //@POST("/posts/add.json")
        public void getAddressPackGeocodeRequest(
                @Query("place_id") String placeId,
                @Query("key") String key,
                retrofit.Callback<ApiResponseGeocode> callback);

    }


    public GeocodeService getGeocodeService() {
        return geocodeService;
    }


    /**
     * Translation
     */

    public interface TranslationService {

        /**
         * Google language detection
         * @param inputStr
         * @param key
         * @param callback
         */
        @GET("/language/translate/v2/detect")
        public void getLanguageDetection(
                @Query("q") String inputStr,
                @Query("key") String key,
                retrofit.Callback<ApiResponseLangDetect> callback);

    }


    public TranslationService getTranslationService() {
        return translationService;
    }


}
