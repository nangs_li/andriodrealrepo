package co.real.productionreal2.chat;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;

import co.real.productionreal2.Constants;
import co.real.productionreal2.CoreData;
import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.callback.MainChatViewListener;
import co.real.productionreal2.chat.core.PrivateChatImpl;
import co.real.productionreal2.common.LocalStorageHelper;
import co.real.productionreal2.dao.account.DBLogInfo;
import co.real.productionreal2.daomanager.DatabaseManager;
import co.real.productionreal2.model.UserQBUser;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.request.RequestMemberProfileQBApply;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;
import co.real.productionreal2.service.model.response.ResponseMemberProfileQBApply;
import com.quickblox.auth.model.QBSession;
import com.quickblox.chat.QBPrivateChat;
import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.users.model.QBUser;

/**
 * Created by hohojo on 18/8/2015.
 */
public class ChatMainDbViewController {
    private static final String TAG = "ChatMainDbViewController";
    private static ChatMainDbViewController instance;
    private MainChatViewListener mainChatViewListener;
    private Context context;
    public static final String CALLBACK_SUCCESS = "success";
    private QBSession session;

    public ChatMainDbViewController(Context context) {
        this.context = context;
        mainChatViewListener = (MainChatViewListener) context;

    }

    public void updateMainChatViewListener(Context context) {
        mainChatViewListener = (MainChatViewListener) context;
    }

    public static ChatMainDbViewController getInstance(Context context) {
        if (instance == null) {
            instance = new ChatMainDbViewController(context);
        }
        return instance;
    }


    public QBSession getSession() {
        return session;
    }

    public void setSession(QBSession session) {
        this.session = session;
    }

    public void updateViewByReceivedMsg(QBPrivateChat chat, QBChatMessage chatMessage) {
        mainChatViewListener.updateViewByReceivedMsg(chat, chatMessage);
    }


    public void callQBUserSignUpOrLogin() {
        QBUser qbUser = UserQBUser.getNewQBUser(context);

        DBLogInfo dbLogInfo = DatabaseManager.getInstance(context).getLogInfo();
        if (dbLogInfo == null || dbLogInfo.getQbUserId() == null) {
//            ChatSessionUtil.callQBSignUp(context, qbUser);
            callMemberProfileQBApply();
        } else {
            ChatSessionUtil.callQBLogin(context, qbUser);
        }
    }

    public void qbSignUpOrLoginCallback(final String callbackMsg) {
        if (callbackMsg.equalsIgnoreCase(CALLBACK_SUCCESS)) {
            ((Activity) context).runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    PrivateChatImpl.getInstance(context);
//                    setQbIdDbActionAndToServer();
                    UserQBUser.updateUserQBUser(context);
                }
            });
        } else {
            if (Constants.isDevelopment)
            ((Activity) context).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(context, callbackMsg, Toast.LENGTH_LONG).show();
                    Log.w(TAG, "qbSignUpOrLoginCallback fail: " + callbackMsg);
                }
            });
        }
    }

    public void callMemberProfileQBApply(){
        //callMemberProfileQBApplyApi
        try{
        final DBLogInfo dbLogInfo = DatabaseManager.getInstance(context).getLogInfo();
        final ResponseLoginSocial.Content content = new Gson().fromJson(dbLogInfo.getLogInfo(),
                ResponseLoginSocial.Content.class);
        RequestMemberProfileQBApply requestMemberProfileQBApply = new RequestMemberProfileQBApply(
                content.memId,
                LocalStorageHelper.getAccessToken(context));
        requestMemberProfileQBApply.callMemberProfileQBApplyApi(context, new ApiCallback() {
            @Override
            public void success(ApiResponse apiResponse) {
                ResponseMemberProfileQBApply responseGson = new Gson().fromJson(apiResponse.jsonContent, ResponseMemberProfileQBApply.class);
                content.qbid = ""+responseGson.content.QBID;
                content.qbPwd = ""+responseGson.content.QBPwd;
                CoreData.setUserContent(content);
                dbLogInfo.setLogInfo(new Gson().toJson(content));
                DatabaseManager.getInstance(context).updateLogInfo(dbLogInfo);
                LocalStorageHelper.setUpByLogin(context, content);
                //todo update QB information to DBLogInfo, then call login qb
                ChatSessionUtil.callQBLogin(context, UserQBUser.getNewQBUser(context));


            }

            @Override
            public void failure(String errorMsg) {
                if (Constants.isDevelopment)
                Toast.makeText(context, "callMemberProfileQBApplyApi fail: " + errorMsg, Toast.LENGTH_LONG).show();
            }
        });
        }catch (Exception e){
            Log.w("","callMemberProfileQBApply error");
        }
    }

//    private void setQbIdDbActionAndToServer() {
//        if (DatabaseManager.getInstance(context) == null)
//            return;
//        if (DatabaseManager.getInstance(context).getLogInfo() == null)
//            return;
//
//        //check is first sign inQB
//        DBLogInfo dbLogInfo = DatabaseManager.getInstance(context).getLogInfo();
//        if (dbLogInfo.getQbUserId() == null) {
//            Gson gson = new Gson();
//            String contentJson;
//            ResponseLoginSocial.Content content = gson.fromJson(dbLogInfo.getLogInfo(), ResponseLoginSocial.Content.class);
//            if (UserQBUser.qbUser == null)
//                return;
//
//            content.qbid = "" + UserQBUser.qbUser.getId();
//            contentJson = gson.toJson(content);
//            dbLogInfo.setLogInfo(contentJson);
//            dbLogInfo.setQbUserId(UserQBUser.qbUser.getId());
//            DatabaseManager.getInstance(context).updateLogInfo(dbLogInfo);
//
//            updateQBIdToServer();
////            //callMemberProfileQBApplyApi
////            RequestMemberProfileQBApply requestMemberProfileQBApply = new RequestMemberProfileQBApply(
////                    content.memId,
////                    content.session);
////            requestMemberProfileQBApply.callMemberProfileQBApplyApi( context, new ApiCallback() {
////                @Override
////                public void success(ApiResponse apiResponse) {
////                    ResponseMemberProfileQBApply responseGson = new Gson().fromJson(apiResponse.jsonContent, ResponseMemberProfileQBApply.class);
////                    if (responseGson.errorCode == 4) {
////                        Dialog.accessErrorForceLogoutDialog(context).show();
////                    } else if (responseGson.errorCode == 26) {
////                        Dialog.suspendedErrorForceLogoutDialog(context).show();
////                    } else if (responseGson.errorCode != 0 && responseGson.errorCode != 20 && responseGson.errorCode != 21 &&
////                            responseGson.errorCode != 22 && responseGson.errorCode != 23) {
////                        callback.failure(responseGson.errorMsg);
////                    } else {
////                        callback.success(apiResponse);
////                    }
////
////                }
////
////                @Override
////                public void failure(String errorMsg) {
////                    Toast.makeText(context, "callMemberProfileQBApplyApi fail: " + errorMsg, Toast.LENGTH_LONG).show();
////                }
////            });
//
//        }
//    }

    public void processMessageDelivered(String messageId, String dialogId, Integer userId) {
        mainChatViewListener.processMessageDelivered(messageId, dialogId, userId);
    }

    public void processMessageRead(String messageId, String dialogId, Integer userId) {
        mainChatViewListener.processMessageRead(messageId, dialogId, userId);
    }

    public void updateTabView() {
        mainChatViewListener.updateTabView();
    }


}
