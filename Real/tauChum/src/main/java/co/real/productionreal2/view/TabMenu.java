package co.real.productionreal2.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import co.real.productionreal2.R;

/**
 * Created by kelvinsun on 2/2/16.
 */
public class TabMenu extends LinearLayout {

    public static interface TabMenuListener {
        public void onTabClicked(View v, boolean isDoubleClick);
    }

    RelativeLayout rl_tab_search;
    RelativeLayout rl_tab_news;
    RelativeLayout rl_tab_chat;
    RelativeLayout rl_tab_me;
    RelativeLayout rl_tab_setting;
    ImageButton searchTabIcon;
    ImageButton newsTabIcon;
    ImageButton chatTabIcon;
    ImageButton meTabIcon;
    ImageButton settingTabIcon;
    TextView tvSearchCount;
    TextView tvNewsCount;
    TextView tvChatCount;
    TextView tvMeCount;
    TextView tvSettingCount;


    private boolean waitDouble = true;
    private static final int DOUBLE_CLICK_TIME = 350; //兩次單擊的時間間隔
    private OnClickListener doubleClickListener;

    private TabMenuListener mListener;

    public TAB_SECTION tabSection;

    public enum TAB_SECTION {SEARCH, NEWSFEED, CHAT, ME, SETTING}

    public void setListener(TabMenuListener listener) {
        mListener = listener;
    }

    public TabMenu(Context context) {
        super(context);
    }

    public TabMenu(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TabMenu(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }


    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        // find views
        rl_tab_search = (RelativeLayout) findViewById(R.id.rl_tab_search);
        rl_tab_news = (RelativeLayout) findViewById(R.id.rl_tab_news);
        rl_tab_chat = (RelativeLayout) findViewById(R.id.rl_tab_chat);
        rl_tab_me = (RelativeLayout) findViewById(R.id.rl_tab_me);
        rl_tab_setting = (RelativeLayout) findViewById(R.id.rl_tab_setting);

        searchTabIcon = (ImageButton) findViewById(R.id.img_tabtxt_tab_search);
        newsTabIcon = (ImageButton) findViewById(R.id.img_tabtxt_tab_news);
        chatTabIcon = (ImageButton) findViewById(R.id.img_tabtxt_tab_chat);
        meTabIcon = (ImageButton) findViewById(R.id.img_tabtxt_tab_me);
        settingTabIcon = (ImageButton) findViewById(R.id.img_tabtxt_tab_setting);

        tvSearchCount = (TextView) findViewById(R.id.noTextView_tab_search);
        tvNewsCount = (TextView) findViewById(R.id.noTextView_tab_news);
        tvChatCount = (TextView) findViewById(R.id.noTextView_tab_chat);
        tvMeCount = (TextView) findViewById(R.id.noTextView_tab_me);
        tvSettingCount = (TextView) findViewById(R.id.noTextView_tab_setting);

        doubleClickListener = new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                Log.d("tabMenu", "Click detect");
                clearAllTabsSelected();

                //single click
                Log.d("tabMenu", "Click single");
                mListener.onTabClicked(v, false);

                //handle double click
                if (waitDouble == true) {
                    waitDouble = false;
                    Thread thread = new Thread() {
                        @Override
                        public void run() {
                            try {
                                sleep(DOUBLE_CLICK_TIME);
                                if (waitDouble == false) {
                                    waitDouble = true;
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    };
                    thread.start();
                } else {
                    waitDouble = true;
                    //double click
                    Log.d("tabMenu", "Click double");
                    mListener.onTabClicked(v, true);
                }
                v.setSelected(true);
            }
        };

        searchTabIcon.setOnClickListener(doubleClickListener);
        newsTabIcon.setOnClickListener(doubleClickListener);
        chatTabIcon.setOnClickListener(doubleClickListener);
        meTabIcon.setOnClickListener(doubleClickListener);
        settingTabIcon.setOnClickListener(doubleClickListener);
    }

    public void setCount(int count, TAB_SECTION section) {
        switch (section) {
            case SEARCH:
                if (count > 0) {
                    tvSearchCount.setVisibility(View.VISIBLE);
                    tvSearchCount.setText("" + count);
                } else {
                    tvSearchCount.setVisibility(View.GONE);
                }
                break;
            case NEWSFEED:
                if (count > 0) {
                    tvNewsCount.setVisibility(View.VISIBLE);
                    tvNewsCount.setText("" + count);
                } else {
                    tvNewsCount.setVisibility(View.GONE);
                }
                break;
            case CHAT:
                if (count > 0) {
                    tvChatCount.setVisibility(View.VISIBLE);
                    tvChatCount.setText("" + count);
                } else {
                    tvChatCount.setVisibility(View.GONE);
                }
                break;
            case ME:
                if (count > 0) {
                    tvMeCount.setVisibility(View.VISIBLE);
                    tvMeCount.setText("" + count);
                } else {
                    tvMeCount.setVisibility(View.GONE);
                }
                break;
            case SETTING:
                if (count > 0) {
                    tvSettingCount.setVisibility(View.VISIBLE);
                    tvSettingCount.setText("" + count);
                } else {
                    tvSettingCount.setVisibility(View.GONE);
                }
                break;
            default:

        }
    }


    public RelativeLayout getRl_tab_search() {
        return rl_tab_search;
    }

    public RelativeLayout getRl_tab_news() {
        return rl_tab_news;
    }

    public RelativeLayout getRl_tab_chat() {
        return rl_tab_chat;
    }

    public RelativeLayout getRl_tab_me() {
        return rl_tab_me;
    }

    public RelativeLayout getRl_tab_setting() {
        return rl_tab_setting;
    }

    public ImageButton getSearchTabIcon() {
        return searchTabIcon;
    }

    public ImageButton getNewsTabIcon() {
        return newsTabIcon;
    }

    public ImageButton getChatTabIcon() {
        return chatTabIcon;
    }

    public ImageButton getMeTabIcon() {
        return meTabIcon;
    }

    public ImageButton getSettingTabIcon() {
        return settingTabIcon;
    }


    public void clearAllTabsSelected() {
        searchTabIcon.setSelected(false);
        newsTabIcon.setSelected(false);
        chatTabIcon.setSelected(false);
        meTabIcon.setSelected(false);
        settingTabIcon.setSelected(false);
    }


}