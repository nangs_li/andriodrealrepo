package co.real.productionreal2.service.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by kelvinsun on 1/6/16.
 */
public class ReUploadContact {
    @SerializedName("nm")
    public String name;
    @SerializedName("cc")
    public String countryCode;
    @SerializedName("pn")
    public String phoneNo;
    @SerializedName("em")
    public String email;

    public ReUploadContact(String name, String countryCode, String phoneNo, String email) {
        this.name = name;
        this.countryCode = countryCode;
        this.phoneNo = phoneNo;
        this.email = email;
    }
}
