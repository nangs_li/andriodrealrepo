package co.real.productionreal2.service.model;

import java.util.List;

import com.google.gson.annotations.SerializedName;
import co.real.productionreal2.model.GoogleAddress;

public class ApiResponseGeocode {

	
	@SerializedName("results")
	public List<GoogleAddress> googleAddress;
//	public String googleAddress;
	
	@SerializedName("status")
	public String status;
	
}
