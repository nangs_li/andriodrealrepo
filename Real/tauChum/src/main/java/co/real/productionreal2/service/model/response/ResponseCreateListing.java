package co.real.productionreal2.service.model.response;

import com.google.gson.annotations.SerializedName;

public class ResponseCreateListing extends ResponseBase {

	@SerializedName("Content")
    public Content content; 

    public class Content {
    	
    	@SerializedName("AgentListingID")
    	public int agentListingId;   	
    	
    }
	
	
}
