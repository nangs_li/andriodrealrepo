package co.real.productionreal2.activity.viral;

import android.Manifest;
import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mixpanel.android.mpmetrics.MixpanelAPI;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import co.real.productionreal2.Constants;
import co.real.productionreal2.CoreData;
import co.real.productionreal2.Navigation;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.BaseActivity;
import co.real.productionreal2.config.AppConfig;
import co.real.productionreal2.fragment.ViralSuccessFragment;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;
import co.real.productionreal2.util.AppUtil;
import co.real.productionreal2.util.ImageUtil;
import co.real.productionreal2.util.PermissionUtil;
import co.real.productionreal2.view.Dialog;
import io.branch.indexing.BranchUniversalObject;
import io.branch.referral.Branch;
import io.branch.referral.BranchError;
import io.branch.referral.util.LinkProperties;
import lib.blurbehind.OnBlurCompleteListener;


public class InviteToFollowActivity extends BaseActivity {

    @InjectView(R.id.ll_invite_to_follow)
    RelativeLayout llInviteToFollow;
    @InjectView(R.id.icon_invite_to_follow)
    ImageView inviteToFollowIcon;
    @InjectView(R.id.TextView_tnc)
    TextView TextView_tnc;
    @InjectView(R.id.closeBtn)
    ImageButton closeBtn;
    @InjectView(R.id.othersBtn)
    Button othersBtn;
    @InjectView(R.id.inviteToFollowBtn)
    Button inviteToFollowBtn;
    @InjectView(R.id.tv_inviteto_follow)
    TextView tvInviteToFollow;


    private static final int REQUEST_CODE_VIRAL = 100;

    public enum InviteToFollowActivityType {
        NORMAL(0),
        SHOW_SKIP(1),
        REDIRECT_TO_BECOME_AN_AGENT(2),
        REDIRECT_TO_CREATE_LISTING(3);

        private final int value;

        private InviteToFollowActivityType(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    private static final String TAG = "InviteToFollowActivity";
    private ResponseLoginSocial.Content userContent;

    Animation animationBSlideDown;

    final int ANIMATION_DURATION = 800;

    public static void start(final Context context) {

        InviteToFollowActivity.start(context, InviteToFollowActivityType.NORMAL);
    }

    public static void start(final Context context, final InviteToFollowActivityType type) {


        ImageUtil.getInstance().prepareBlurImageFromActivity((Activity) context, InviteToFollowActivity.class, new OnBlurCompleteListener() {
            @Override
            public void onBlurComplete() {
                Intent intent = new Intent(context, InviteToFollowActivity.class);
                String action = String.valueOf(type.getValue());
                intent.setAction(action);
                Navigation.presentIntentForResult((Activity) context, intent, type.getValue());
            }
        });

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_to_follow);
        AppUtil.setBlurBehind(this);

        userContent = CoreData.getUserContent(this);

        ButterKnife.inject(this);
        initView();

    }


    private void initView() {

        tvInviteToFollow.setText(Html.fromHtml(getString(R.string.setting__broadcast)));

        String htmlTNC = "<subtitle><yellow> " + getString(R.string.invite_to_follow__desc_1) + "</yellow></subtitle>"
                + "\n"
                + getString(R.string.invite_to_follow__desc_2)
                + "\n\n"
                + "<subtitle><yellow> " + getString(R.string.invite_to_follow__desc_3) + "</yellow></subtitle>"
                + "\n"
                + getString(R.string.invite_to_follow__desc_4)
                + "\n\n"
                + "<subtitle><yellow>" + getString(R.string.invite_to_follow__desc_5) + "</yellow></subtitle>"
                + "\n"
                + getString(R.string.invite_to_follow__desc_6);

        TextView_tnc.setText(Html.fromHtml(htmlTNC.replaceAll("\n", "<br>")
                .replaceAll("<subtitle>", "<font size=\"12\"><b>")
                .replaceAll("</subtitle>", "</b></font>")
                .replaceAll("<yellow>", "<font color=#" + Integer.toHexString(ContextCompat.getColor(this, R.color.invite_yellow) & 0x00ffffff) + ">")
                .replaceAll("</yellow>", "</font>")
        ));

        boolean showSkip = showSkip();
//        othersBtn.setVisibility(showSkip ? View.VISIBLE : View.GONE);
//        closeBtn.setVisibility(showSkip ? View.GONE : View.VISIBLE);

        // invite to follow page show logic after publish
        if (showSkip && CoreData.inviteToFollowShownCount > 0)
            CoreData.inviteToFollowShownCount--;

        othersBtn.setVisibility(View.GONE);

        if (activityType() == InviteToFollowActivityType.REDIRECT_TO_BECOME_AN_AGENT.getValue() || activityType() == InviteToFollowActivityType.REDIRECT_TO_CREATE_LISTING.getValue()) {
            inviteToFollowBtn.setText(getString(R.string.invite_to__create_first_story));
            othersBtn.setVisibility(View.GONE);
        }
    }

    private void genShortUrl() {
        if (userContent == null)
            return;
        if (userContent.agentProfile == null)
            return;
        if (userContent.agentProfile.agentListing == null)
            return;
        BranchUniversalObject branchUniversalObject = genBranchUniObj();
        LinkProperties linkProps = genLinkProps();
        branchUniversalObject.generateShortUrl(this, linkProps, new Branch.BranchLinkCreateListener() {
            @Override
            public void onLinkCreate(String url, BranchError error) {
                if (error != null) {
                    Dialog.normalDialog(InviteToFollowActivity.this, "Branch.io gen shortUrl error: " + error);
                } else {
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_SEND);
                    intent.setType("text/plain");
                    intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.invite_to_follow__subject));
                    intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.invite_to_follow__content) + " " + url);
                    startActivityForResult(Intent.createChooser(intent, getString(R.string.common__invite_to_follow)), REQUEST_CODE_VIRAL);
                }
            }
        });
    }

    private BranchUniversalObject genBranchUniObj() {
        BranchUniversalObject branchUniObj = new BranchUniversalObject()
                .setTitle("Real")
                .setContentDescription("Someone invite you to Invite in Real")
                .setContentIndexingMode(BranchUniversalObject.CONTENT_INDEX_MODE.PUBLIC)
                .addContentMetadata(Constants.BRANCH_MEM_ID, "" + userContent.memId)
                .addContentMetadata(Constants.BRANCH_MEM_NAME, userContent.memName)
                .addContentMetadata(Constants.BRANCH_ACTION, Constants.BRANCH_ACTION_FOLLOW)
                .addContentMetadata(Constants.BRANCH_MEM_IMAGE_URL, userContent.photoUrl)
                .addContentMetadata(Constants.BRANCH_AGENT_LISTING_ID, "" + userContent.agentProfile.agentListing.listingID);

        return branchUniObj;
    }

    private LinkProperties genLinkProps() {
        LinkProperties linkProperties = new LinkProperties()
                .setChannel(Constants.BRANCH_CHANNEL_FOLLOW)
                .setFeature("invite");
        return linkProperties;
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        if (showSkip()) {
            playAnimation();
        } else {
            finish();
        }
    }

    @OnClick(R.id.closeBtn)
    public void closeBtnClick(View view) {
        onBackPressed();
    }

    //finishing animation
    Animation.AnimationListener animationASlideDownListener = new Animation.AnimationListener() {

        @Override
        public void onAnimationStart(Animation animation) {
        }

        @Override
        public void onAnimationEnd(Animation animation) {
//            inviteToFollowIcon.startAnimation(animationBSlideDown);
            inviteToFollowIcon.setVisibility(View.INVISIBLE);
            llInviteToFollow.setVisibility(View.INVISIBLE);
            finish();
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
        }
    };

    @OnClick(R.id.othersBtn)
    public void skipBtnClick(View view) {
        if (activityType() == InviteToFollowActivityType.REDIRECT_TO_BECOME_AN_AGENT.getValue() || activityType() == InviteToFollowActivityType.REDIRECT_TO_CREATE_LISTING.getValue()) {

            setResult(RESULT_OK);
            finish();

        } else {
            // Mixpanel
            MixpanelAPI mixpanel = MixpanelAPI.getInstance(this, AppConfig.getMixpanelToken());
            mixpanel.track("Invite to Follow");

            genShortUrl();
        }
    }


    private void playAnimation() {
        animationBSlideDown = AnimationUtils.loadAnimation(this,
                R.anim.slide_up_anim);
        animationBSlideDown.setDuration(ANIMATION_DURATION);
        animationBSlideDown.setAnimationListener(animationASlideDownListener);
        inviteToFollowIcon.startAnimation(animationBSlideDown);

        Animation a = AnimationUtils.loadAnimation(this, R.anim.fade_out);
        a.reset();
        a.setDuration(ANIMATION_DURATION);
        llInviteToFollow.clearAnimation();
        llInviteToFollow.startAnimation(a);
    }

    @OnClick(R.id.inviteToFollowBtn)
    public void inviteToFollowBtnClick(View view) {

        if (activityType() == InviteToFollowActivityType.REDIRECT_TO_BECOME_AN_AGENT.getValue() || activityType() == InviteToFollowActivityType.REDIRECT_TO_CREATE_LISTING.getValue()) {

            setResult(RESULT_OK);
            finish();

        } else {
            // Mixpanel
            MixpanelAPI mixpanel = MixpanelAPI.getInstance(this, AppConfig.getMixpanelToken());
            mixpanel.track("Invite to Follow");

            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.READ_CONTACTS)
                    != PackageManager.PERMISSION_GRANTED) {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_CONTACTS},
                        PermissionUtil.MY_PERMISSIONS_REQUEST_READ_CONTACT);
            } else {
                ConnectAgentActivity.start(this, ConnectAgentActivity.ConnectAgentActivityType.BROADCAST);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PermissionUtil.MY_PERMISSIONS_REQUEST_READ_CONTACT: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    ConnectAgentActivity.start(this, ConnectAgentActivity.ConnectAgentActivityType.BROADCAST);
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    genShortUrl();
                }
                return;
            }
        }
    }


    private boolean showSkip() {

        boolean showSkip = false;

        if (activityType() == InviteToFollowActivityType.SHOW_SKIP.getValue()) {
            showSkip = true;
        }

        return showSkip;
    }

    private int activityType() {

        int typeInt = Integer.parseInt(getIntent().getAction());
        return typeInt;
    }

//

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == REQUEST_CODE_VIRAL) {
            showSuccessDialog();
        }
    }


    void showSuccessDialog() {
        int mStackLevel = 0;
        mStackLevel++;

        // DialogFragment.show() will take care of adding the fragment
        // in a transaction.  We also want to remove any currently showing
        // dialog, so make our own transaction and take care of that here.
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        DialogFragment newFragment = ViralSuccessFragment.newInstance(mStackLevel);
        newFragment.show(ft, "dialog");
    }
}


