package co.real.productionreal2.service.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by kelvinsun on 19/2/16.
 */
public class ReMatchContact implements Parcelable {
    @SerializedName("MemberID")
    public int memberID;
    @SerializedName("Name")
    public String name;
    @SerializedName("PhotoURL")
    public String photoURL;
    @SerializedName("Location")
    public String location;
    @SerializedName("CountryCode")
    public String countryCode;
    @SerializedName("PhoneNumber")
    public String phoneNumber;
    @SerializedName("ListingID")
    public int listingID;
    @SerializedName("IsFollowing")
    public int isFollowing=0;

    public ReMatchContact() {
    }

    public ReMatchContact(int memberID, String name, String photoURL, String location, String countryCode, String phoneNumber, int isFollowing) {
        this.memberID = memberID;
        this.name = name;
        this.photoURL = photoURL;
        this.location = location;
        this.countryCode = countryCode;
        this.phoneNumber = phoneNumber;
        this.listingID = listingID;
        this.isFollowing = isFollowing;
    }


    public ReMatchContact(Parcel source) {
        memberID = source.readInt();
        name = source.readString();
        photoURL = source.readString();
        location = source.readString();
        countryCode = source.readString();
        phoneNumber = source.readString();
        listingID = source.readInt();
        isFollowing = source.readInt();

    }


    public static final Creator<ReMatchContact> CREATOR = new Creator<ReMatchContact>() {
        @Override
        public ReMatchContact createFromParcel(Parcel in) {
            return new ReMatchContact(in);
        }

        @Override
        public ReMatchContact[] newArray(int size) {
            return new ReMatchContact[size];
        }
    };


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(memberID);
        dest.writeString(name);
        dest.writeString(photoURL);
        dest.writeString(location);
        dest.writeString(countryCode);
        dest.writeString(phoneNumber);
        dest.writeInt(listingID);
        dest.writeInt(isFollowing);
    }
}
