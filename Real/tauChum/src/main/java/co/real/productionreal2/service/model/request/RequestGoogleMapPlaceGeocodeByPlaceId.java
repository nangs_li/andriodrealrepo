package co.real.productionreal2.service.model.request;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.model.InputRequest;
import co.real.productionreal2.service.MapService;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.response.ResponseNewsFeed;
import co.real.productionreal2.view.Dialog;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by kelvinsun on 7/3/16.
 */
public class RequestGoogleMapPlaceGeocodeByPlaceId extends RequestBase {
    public RequestGoogleMapPlaceGeocodeByPlaceId(int memId,
                                                 String session,
                                                 String language,
                                                 String placeID) {
        super(memId, session);
        this.language = language;
        this.placeID=placeID;
    }

    @SerializedName("Language")
    public String language;

    @SerializedName("PlaceID")
    public String placeID;


    public void callGoogleMapPlaceAPIGeocodeByPlaceId(final Context context,  final ApiCallback callback) {
        String json = new Gson().toJson(this);
        Log.d("GooglePlaceAPIGeocode", "GooglePlaceAPIGeocode = " + json);
        final MapService mapService = new MapService(context);
        mapService.getMapService().googleMapPlaceAPIGeocodeByPlaceID(new InputRequest(json), new Callback<ApiResponse>() {
            @Override
            public void success(ApiResponse apiResponse, Response response) {
                ResponseNewsFeed responseGson = new Gson().fromJson(apiResponse.jsonContent, ResponseNewsFeed.class);
                if (responseGson.errorCode == 4) {
                    Dialog.accessErrorForceLogoutDialog(context).show();
                } else if (responseGson.errorCode == 26) {
                    Dialog.suspendedErrorForceLogoutDialog(context).show();
                } else if (responseGson.errorCode != 0 && responseGson.errorCode != 20 && responseGson.errorCode != 21 &&
                        responseGson.errorCode != 22 && responseGson.errorCode != 23) {
                    callback.failure(responseGson.errorMsg);
                } else {
                    callback.success(apiResponse);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                callback.failure(error.getMessage());
            }
        });
    }
}
