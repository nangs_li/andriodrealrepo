package co.real.productionreal2.activity.setting;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import co.real.productionreal2.Navigation;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.BaseActivity;
import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.callback.DialogCallback;
import co.real.productionreal2.common.LocalStorageHelper;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.request.RequestRealNetworkChangePw;
import co.real.productionreal2.service.model.response.ResponseRealNetworkMemLogin;
import co.real.productionreal2.util.CryptLib;
import co.real.productionreal2.view.Dialog;

public class ChangePwSettingActivity extends BaseActivity {
    @InjectView(R.id.headerLayout)
    View headerLayout;
    @InjectView(R.id.currentPwEditText)
    EditText currentPwEditText;
    @InjectView(R.id.newPwEditText)
    EditText newPwEditText;
    @InjectView(R.id.confirmPwEditText)
    EditText confirmPwEditText;
    @InjectView(R.id.resetPwLayout)
    View resetPwLayout;


    private ResponseRealNetworkMemLogin.Content realLoginContent;
    private ProgressDialog ringProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pw_setting);
        ButterKnife.inject(this);

        String realJson = LocalStorageHelper.getRealNetworkJson(this);
        ResponseRealNetworkMemLogin realNetworkMemLogin = new Gson().fromJson(realJson, ResponseRealNetworkMemLogin.class);
        realLoginContent = realNetworkMemLogin.content;

        initView();
    }

    private void initView() {
        TextView titleTextView = (TextView) headerLayout.findViewById(R.id.titleTextView);
        titleTextView.setText(R.string.change_password__title);
        resetPwLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChangePwSettingActivity.this, ResetPwSettingActivity.class);
                Navigation.pushIntent(ChangePwSettingActivity.this,intent);
            }
        });
    }

    @OnClick(R.id.changePwBtn)
    public void changePwBtnClick(View view) {
        if (currentPwEditText.getText().toString().isEmpty())
            Dialog.normalDialog(this, getString(R.string.change_password__title), getString(R.string.error_message__password_cannot_be_blank)).show();
        else if (newPwEditText.getText().toString().isEmpty() ||
                confirmPwEditText.getText().toString().isEmpty())
            Dialog.normalDialog(this, getString(R.string.change_password__title), getString(R.string.error_message__new_password_cannot)).show();
        else if (confirmPwEditText.getText().toString().isEmpty())
            Dialog.normalDialog(this, getString(R.string.change_password__title), getString(R.string.error_message__confirm_your_password)).show();
        else if (currentPwEditText.getText().toString().equals(newPwEditText.getText().toString()))
            Dialog.normalDialog(this, getString(R.string.change_password__title), getString(R.string.error_message__old_password_and_new)).show();
        else if (!confirmPwEditText.getText().toString().equals(newPwEditText.getText().toString()))
            Dialog.normalDialog(this, getString(R.string.change_password__title), getString(R.string.error_message__password_does_not_match)).show();
        else {
            if (ringProgressDialog != null && ringProgressDialog.isShowing())
                return;
            ringProgressDialog = ProgressDialog.show(this, getResources().getString(R.string.alert__Initializing),
                    getResources().getString(R.string.common__change));

            RequestRealNetworkChangePw realNetworkChangePw = new RequestRealNetworkChangePw(
                    CryptLib.md5(newPwEditText.getText().toString()),
                    CryptLib.md5(currentPwEditText.getText().toString()),
                    realLoginContent.userId, realLoginContent.accessToken);
            realNetworkChangePw.callRealNetworkChangePwApi(this, new ApiCallback() {
                @Override
                public void success(ApiResponse apiResponse) {
                    if (ringProgressDialog != null) {
                        ringProgressDialog.dismiss();
                        ringProgressDialog.cancel();
                        ringProgressDialog = null;
                    }
                    Dialog.feedbackDialog(ChangePwSettingActivity.this, true,
                            getString(R.string.change_password__title),
                            getString(R.string.error_message__change_password_success),
                            new DialogCallback() {
                                @Override
                                public void yes() {
                                    onBackPressed();
                                }
                            }).show();
                }

                @Override
                public void failure(String errorMsg) {
                    if (errorMsg!="") {
                        Dialog.normalDialog(ChangePwSettingActivity.this, errorMsg).show();
                    }
                    if (ringProgressDialog != null) {
                        ringProgressDialog.dismiss();
                        ringProgressDialog.cancel();
                        ringProgressDialog = null;
                    }
                }
            });
        }






    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
