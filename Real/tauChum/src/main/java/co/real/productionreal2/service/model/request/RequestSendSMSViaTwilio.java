package co.real.productionreal2.service.model.request;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.model.InputRequest;
import co.real.productionreal2.service.OperationService;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.response.ResponseBase;
import co.real.productionreal2.util.AppUtil;
import co.real.productionreal2.util.NetworkUtil;
import co.real.productionreal2.view.Dialog;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by hohojo on 4/1/2016.
 */
public class RequestSendSMSViaTwilio extends RequestBase {

    public RequestSendSMSViaTwilio(int memId,
                                   String session,
                                   String toCountryCode,
                                   String toPhoneNumber,
                                   String url) {
        super(memId, session);
        this.toCountryCode = toCountryCode;
        this.toPhoneNumber = toPhoneNumber;
        this.url = url;
    }

    @SerializedName("ToCountryCode")
    public String toCountryCode;
    @SerializedName("ToPhoneNumber")
    public String toPhoneNumber;
    @SerializedName("URL")
    public String url;
    @SerializedName("Lang")
    public String lang;

    public void callSendSMSViaTwilioApi(final Context context, final ApiCallback callback) {
        this.lang = AppUtil.getCurrentLangISO2Code(context);
        String json = new Gson().toJson(this);
        final OperationService weatherService = new OperationService(context);
        weatherService.getCoffeeService().sendSMSViaTwilio(new InputRequest(json), new Callback<ApiResponse>() {
            @Override
            public void success(ApiResponse apiResponse, Response response) {
                ResponseBase responseGson = new Gson().fromJson(apiResponse.jsonContent, ResponseBase.class);
                if (responseGson.errorCode == 4) {
                    Dialog.accessErrorForceLogoutDialog(context).show();
                } else if (responseGson.errorCode == 26) {
                    Dialog.suspendedErrorForceLogoutDialog(context).show();
                } else if (responseGson.errorCode != 0 && responseGson.errorCode != 20 && responseGson.errorCode != 21 &&
                        responseGson.errorCode != 22 && responseGson.errorCode != 23) {
                    callback.failure(responseGson.errorMsg);
                } else {
                    callback.success(apiResponse);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                callback.failure("");
                NetworkUtil.showNoNetworkMsg(context);
            }
        });
    }
}
