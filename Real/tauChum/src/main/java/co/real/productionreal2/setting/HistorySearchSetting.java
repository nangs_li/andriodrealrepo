package co.real.productionreal2.setting;

import android.util.Log;
import co.real.productionreal2.service.model.request.RequestAddressSearch;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by kelvinsun on 4/1/16.
 */
public class HistorySearchSetting {

    private static HistorySearchSetting historySearchSetting;
//    private static List<GooglePlace> latestFiveAddressSearchList = null;
    private static List<RequestAddressSearch> latestFiveAddressSearchList = null;
    private final int MAX_RECORD = 5;


    public static HistorySearchSetting getInstance() {
        if (historySearchSetting == null) {
            historySearchSetting = new HistorySearchSetting();
            latestFiveAddressSearchList = new ArrayList<>();
        }
        return historySearchSetting;
    }


    public void addRecord(RequestAddressSearch requestAddressSearch) {
        if (latestFiveAddressSearchList != null) {

            int position = checkIsExist(requestAddressSearch);
            Log.d("GooglePlace:", "GooglePlace: check position: " + position);
            if (position != -1) {
                //remove duplicated record
                latestFiveAddressSearchList.remove(position);
            }else if (latestFiveAddressSearchList.size() >= MAX_RECORD) {
                latestFiveAddressSearchList.remove(0);
            }

            latestFiveAddressSearchList.add(requestAddressSearch);


        }
    }

    private int checkIsExist(RequestAddressSearch mRequestAddressSearch) {
        int i = 0;
        for (RequestAddressSearch requestAddressSearch : latestFiveAddressSearchList) {
            Log.d("GooglePlace:", "GooglePlace: 1: " + requestAddressSearch.googleAddress + " <> " + mRequestAddressSearch.googleAddress);
            if (requestAddressSearch.googleAddress!=null && mRequestAddressSearch.googleAddress!=null) {
                Log.d("GooglePlace:", "GooglePlace: 1: " + requestAddressSearch.googleAddress.address.placeId + " <> " + mRequestAddressSearch.googleAddress.address.placeId.toString());
                if ((requestAddressSearch.googleAddress.address.placeId).equals(mRequestAddressSearch.googleAddress.address.placeId)) {
                    return i;
                }
                i++;
            }
        }
        return -1;
    }

    public static void setLatestFiveAddressSearchList(List<RequestAddressSearch> latestFiveAddressSearchList) {
        HistorySearchSetting.latestFiveAddressSearchList = latestFiveAddressSearchList;
    }

    public List<RequestAddressSearch> getLatestFiveAddressSearchList() {
        return latestFiveAddressSearchList;
    }

    public List<RequestAddressSearch> getLatestFiveAddressSearchListInReverse() {
        ArrayList<RequestAddressSearch> latestFiveAddressSearchListInReverse = new ArrayList<RequestAddressSearch>(latestFiveAddressSearchList);
        Collections.reverse(latestFiveAddressSearchListInReverse);
        return latestFiveAddressSearchListInReverse;
    }

    public void resetValues() {
        latestFiveAddressSearchList.clear();
    }
}
