package co.real.productionreal2.contianer;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.yalantis.ucrop.UCrop;

import co.real.productionreal2.BaseContainerFragment;
import co.real.productionreal2.Constants;
import co.real.productionreal2.CoreData;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.MainActivityTabBase;
import co.real.productionreal2.activity.viral.InviteToFollowActivity;
import co.real.productionreal2.daomanager.DatabaseManager;
import co.real.productionreal2.fragment.AgentProfileFragment;
import co.real.productionreal2.fragment.CreateUpdatePostFragment;
import co.real.productionreal2.fragment.NotCompleteAgentProfileFragment;
import co.real.productionreal2.fragment.UserProfileFragment;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;
import co.real.productionreal2.view.TitleBar;


public class MeTabContainer extends BaseContainerFragment {
    public static final String TAG = "MeTabContainer";
    public static String REQUEST_CODE = "REQUEST_CODE";
    public static String RESULT_CODE = "RESULT_CODE";
    public static String RESULT_DATA = "RESULT_DATA";
    private ResponseLoginSocial.Content userContent;
    private TitleBar mTitleBar;
    boolean needInit = true;
    public UserProfileFragment userProfileFragment;
    public NotCompleteAgentProfileFragment notCompleteAgentProfileFragment;

    public void fragmentBecameVisible() {
        if (needInit) {
            userContent = CoreData.getUserContent(getActivity());
            Log.d("onActivityCreated", "onActivityCreated MeTabProfile" + userContent);
            if (userContent.agentProfile != null &&
                    userContent.agentProfile.agentListing == null) {
                // init to home, not complete agent home is index 0
                initView(0, false, 0, 0,null);
            } else
                initView(1, false, 0, 0,null);

            needInit = false;
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.container_fragment, null);
        mTitleBar = ((MainActivityTabBase) getActivity()).getTitleBar();

        return rootView;

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult: " + requestCode + " , " + resultCode);
//        List<Fragment> fragments = getChildFragmentManager().getFragments();
//        if (fragments != null) {
//            for (Fragment fragment : fragments) {
//                fragment.onActivityResult(requestCode, resultCode, data);
//            }
//        }
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case Constants.ME_VIEW_PROFILE:
                case Constants.ME_BE_AN_AGENT:
                    initView(0, true, requestCode, resultCode,data);
                    break;
                case Constants.ME_TO_CREATE_POST:
                case CreateUpdatePostFragment.CONTINUE_POST:
                    initView(1, true, requestCode, resultCode,data);
                    break;
                default:
                    initView(1, true, requestCode, resultCode,data);
            }
        } else {
            //discards post
            if (requestCode != InviteToFollowActivity.InviteToFollowActivityType.REDIRECT_TO_BECOME_AN_AGENT.getValue())  // avoid change titlebar from setting section
                initView(1, true, requestCode, resultCode, data);
        }

    }


    private void initView(int pageNo, boolean changeTitle, int requestCode, int resultCode, Intent data) {
        userContent = CoreData.getUserContent(getActivity());
        if (DatabaseManager.getInstance(getActivity()).hasLogInfo()) {
            if (DatabaseManager.getInstance(getActivity()).getLogInfo() == null)
                return;
            if (userContent == null)
                return;
            if (userContent.agentProfile == null) {
                Log.i(TAG, "(UserProfileFragment.newInstance()");
                Uri resultUri=null;
                if (data!=null) {
                    resultUri = UCrop.getOutput(data);
                }
                userProfileFragment = UserProfileFragment.newInstance(resultUri);
                replaceFragment(userProfileFragment, false, "UserProfileFragment");
                if (changeTitle) {
                    mTitleBar.setupTitleBar(TitleBar.TAB_TYPE.ME_HOME_USER);
                }
            } else if (userContent.agentProfile != null &&
                    userContent.agentProfile.agentListing == null) {
                Log.i(TAG, "(NotCompleteAgentProfileFragment.newInstance()");
                notCompleteAgentProfileFragment = NotCompleteAgentProfileFragment.newInstance(pageNo, requestCode, resultCode);
                replaceFragment(notCompleteAgentProfileFragment, false, "NotCompleteAgentProfileFragment");
                if (changeTitle) {
                    mTitleBar.setupTitleBar(TitleBar.TAB_TYPE.ME_HOME_AGENT);
                }
            } else {
                // is an agent
                Log.i(TAG, "(AgentProfileFragment.newInstance()");
                replaceFragment(AgentProfileFragment.newInstance(pageNo, requestCode, resultCode), false, "AgentProfileFragment");
                if (changeTitle) {
                    mTitleBar.setupTitleBar(TitleBar.TAB_TYPE.ME_HOME_AGENT);
                }
            }
        }

    }


}