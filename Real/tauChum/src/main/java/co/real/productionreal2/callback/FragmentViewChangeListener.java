package co.real.productionreal2.callback;

import android.graphics.Bitmap;
import android.support.v4.app.Fragment;

public interface FragmentViewChangeListener {
	public void viewMoving(Bitmap captureBitmap);
	public void viewMovingEnd();
	public void hideTabBar();
	public void openTabBar();
	public void changeTabTitle(Fragment fragment);
}
