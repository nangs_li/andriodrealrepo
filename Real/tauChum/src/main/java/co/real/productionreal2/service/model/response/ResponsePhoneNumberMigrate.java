package co.real.productionreal2.service.model.response;

import com.google.gson.annotations.SerializedName;

public class ResponsePhoneNumberMigrate extends ResponseBase{

	
	@SerializedName("Content")
    public Content content; 

    public class Content {
    	@SerializedName("PhoneRegID")
        public int phoneRegId;
    	
    }
	
    
    
	
}
