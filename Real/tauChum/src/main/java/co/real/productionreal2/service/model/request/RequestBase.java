package co.real.productionreal2.service.model.request;

import com.google.gson.annotations.SerializedName;

public class RequestBase {

    @SerializedName("UniqueKey")
    public String uniqueKey = "0";

    @SerializedName("MemberID")
    public int memId;

    @SerializedName("AccessToken")
    public String session;

    public RequestBase(int memId, String session) {
        super();
        this.memId = memId;
        this.session = session;
    }

    public RequestBase(String uniqueKey, int memId, String session) {
        this.uniqueKey = uniqueKey;
        this.memId = memId;
        this.session = session;
    }
}
