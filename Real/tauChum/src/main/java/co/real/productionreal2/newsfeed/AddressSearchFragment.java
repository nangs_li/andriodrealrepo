package co.real.productionreal2.newsfeed;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListPopupWindow;
import android.widget.SimpleAdapter;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import co.real.productionreal2.R;
import co.real.productionreal2.activity.MainActivityTabBase;
import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.common.LocalStorageHelper;
import co.real.productionreal2.daomanager.DatabaseManager;
import co.real.productionreal2.service.model.AgentProfiles;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.request.RequestAddViewedListing;
import co.real.productionreal2.service.model.request.RequestAddressSearch;
import co.real.productionreal2.service.model.request.RequestReportProblem;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;
import co.real.productionreal2.service.model.response.ResponseNewsFeed;
import co.real.productionreal2.util.AppUtil;
import co.real.productionreal2.util.ImageUtil;
import co.real.productionreal2.view.TabMenu;
import co.real.productionreal2.view.TitleBar;
import widget.EnbleableSwipeViewPager;
import widget.InterceptableRelativeLayout;
import widget.viewpagerindicator.CirclePageIndicator;
import widget.viewpagerindicator.PageIndicator;

/**
 * Created by edwinchan on 8/12/15.
 */
public class AddressSearchFragment extends BaseNewsFeedFragment {
    MyPageAdapter pageAdapter;

    public static final String BUNDLE_NEWSFEED_TYPE = "BUNDLE_NEWSFEED_TYPE";

    private TitleBar mTitlebar;
    private TabMenu menuTabHost;

    private List<Fragment> fragments;

    private static final String TITLE = "title";
    private List<HashMap<String, Object>> data = new ArrayList<HashMap<String, Object>>();

    private ResponseLoginSocial.Content userProfile;

    public AgentProfiles agentProfile;

    private EnbleableSwipeViewPager pager;
    private PageIndicator mIndicator;
    public LinearLayout pagerIndicator;
    private InterceptableRelativeLayout mInterceptableRelativeLayout;

    public static RequestAddressSearch.GoogleAddressPack googleAddressPack;


    NewsFeedDetailFragment newsFeedDetailFragment;
    NewsFeedProfileFragment newsFeedProfileFragment;
    AddressSearchMidFragment addressSearchMidFragment;

    public AddressSearchFragment() {
    }

    public static final AddressSearchFragment newInstance(int newsFeedType) {
        AddressSearchFragment f = new AddressSearchFragment();
        Bundle bdl = new Bundle();
        //1 for search result
        bdl.putInt(BUNDLE_NEWSFEED_TYPE, newsFeedType);
        f.setArguments(bdl);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.e("", "edwin NewsFeedFragment onCreateView");
        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.activity_newsfeed, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        pager = (EnbleableSwipeViewPager) view.findViewById(R.id.viewpager);
        mIndicator = (CirclePageIndicator) view.findViewById(R.id.indicator);
        pagerIndicator = (LinearLayout) view.findViewById(R.id.llViewPagerIndicator);
        mInterceptableRelativeLayout = (InterceptableRelativeLayout) view.findViewById(R.id.mInterceptableRelativeLayout);

        userProfile = new Gson().fromJson(DatabaseManager.getInstance(getActivity()).getLogInfo().getLogInfo(),
                ResponseLoginSocial.Content.class);

        //init title bar
        mTitlebar = ((MainActivityTabBase) getActivity()).getTitleBar();
        menuTabHost = ((MainActivityTabBase) getActivity()).getTabHost();
        addItem(getString(R.string.reportpage__report));

//        APIUtil.callGetNewsFeedApi(context, this);


        Log.d("updateNewsfeed", "updateNewsfeed: pageAdapter: " + pageAdapter);
        if (pageAdapter == null)
            setViewPager(null);
        else
            ((AddressSearchMidFragment) fragments.get(1)).notifyDataSetChanged();
    }


    public void setViewPager(ResponseNewsFeed responseGson) {
//        if(responseGson.content.agentProfiles.size()==0){
//            return;
//        }
        fragments = getFragments(responseGson);

        pageAdapter = new MyPageAdapter(getChildFragmentManager(), fragments);
        pager.setAdapter(pageAdapter);
        pager.setOffscreenPageLimit(2);
        mIndicator.setViewPager(pager);
//        pager.setPageTransformer(true, new DepthPageTransformer());

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            int oldPosition = 1;
            int currentPosition = 1;

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                currentPosition = position;
                Log.e("", "edwin onPageScrolled " + position + " positionOffset " + positionOffset + " positionOffsetPixels " + positionOffsetPixels);
                if (position == 0) {
                    addressSearchMidFragment.setCoverViewAlpha((1f - positionOffset) * 5);
                    newsFeedDetailFragment.updateUI();
                } else if (position == 1) {
                    addressSearchMidFragment.setCoverViewAlpha((positionOffset) * 5);
                    newsFeedProfileFragment.updateUI(false);
                }
            }

            @Override
            public void onPageSelected(int position) {

                Log.d("onPageSelected","onPageSelected: "+position );
                addressSearchMidFragment.isShowing = false;
                agentProfile = LocalStorageHelper.getLocalAgentProfiles(getActivity(), getCurrentSection());
                if (position == 0) {
                    //Details
                    mTitlebar.setTitle(newsFeedDetailFragment.getTitileName());
                    mTitlebar.setupTitleBar(TitleBar.TAB_TYPE.NEWSFEED_DETAILS);
                    mTitlebar.getEditBtn().setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            showListMenu(v, 4);
                        }
                    });

                    callAddViewedListingApi();
                    pager.setSwipeable(true);
                    newsFeedDetailFragment.setFollowing();

                } else if (position == 2) {
                    //Agent Profile
                    mTitlebar.setupTitleBar(TitleBar.TAB_TYPE.NEWSFEED_PROFILE);
                    mTitlebar.getEditBtn().setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            showListMenu(v, 2);
                        }
                    });

                    callAddViewedListingApi();
                    pager.setSwipeable(true);
                    newsFeedProfileFragment.setFollowing();
                    Log.d("onPageSelected","onPageSelected: "+position );
                    newsFeedProfileFragment.updateUI(false);

                } else if (position == 1) {
                    //List view
                    ((MainActivityTabBase) getActivity()).setTabClickEnable(true);
                    if ( getCurrentSection()== MainActivityTabBase.CURRENT_SECTION.NEWSFEED)
                        ((MainActivityTabBase) getActivity()).getTitleBar().setupTitleBar(TitleBar.TAB_TYPE.NEWSFEED);
                    else
                        ((MainActivityTabBase) getActivity()).getTitleBar().setupTitleBar(TitleBar.TAB_TYPE.SEARCH);

                    pager.setSwipeable(false);
                    addressSearchMidFragment.isShowing = true;
                    addressSearchMidFragment.notifyDataSetChanged();

                }

                oldPosition = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                addressSearchMidFragment.viewPagerScrollState = state;

                if (state == ViewPager.SCROLL_STATE_IDLE) {
                    if (currentPosition == 2 || currentPosition == 0) {
                        ImageUtil.collapse(menuTabHost);
                    } else if (currentPosition == 1) {
                        pager.setSwipeable(false);
                        ((MainActivityTabBase) getActivity()).setTabClickEnable(true);
                    }
                } else if (state == ViewPager.SCROLL_STATE_DRAGGING) {
                    ((MainActivityTabBase) getActivity()).setTabClickEnable(false);
                    if (oldPosition == 0 || oldPosition == 2) {
                        ImageUtil.expand(menuTabHost);
                    }

                }

//                Log.v("","edwin onPageScrollStateChanged state "+state);

            }
        });
        pager.setCurrentItem(1);

    }


    // Use this to add items to the list that the ListPopupWindow will use
    private void addItem(String title) {
        data = new ArrayList<HashMap<String, Object>>();
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put(TITLE, title);
//        map.put(ICON, 0);
        data.add(map);
    }

    // Call this when you want to show the ListPopupWindow
    private void showListMenu(View anchor, final int problemType) {
        final ListPopupWindow popupWindow = new ListPopupWindow(context);

        agentProfile = LocalStorageHelper.getLocalAgentProfiles(getActivity(),  getCurrentSection());

        ListAdapter adapter = new SimpleAdapter(
                context,
                data,
                R.layout.menu_list_item,
                new String[]{TITLE}, // These are just the keys that the data uses
                new int[]{android.R.id.text1}); // The view ids to map the data to

        popupWindow.setAnchorView(anchor);
        popupWindow.setAdapter(adapter);
        popupWindow.setContentWidth(AppUtil.measureContentWidth(context, adapter));
        popupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int memId = agentProfile.memberID;
                int listingId = 0;
                if (agentProfile != null) {
                    listingId = problemType == 4 ? agentProfile.agentListing.listingID : 0;
                }
                callReportApi(problemType, memId, listingId);
                popupWindow.dismiss();
            }
        }); // the callback for when a list item is selected
        popupWindow.show();
    }

    private void callReportApi(int problemType, int memId, int listingId) {
        RequestReportProblem reportProblem = new RequestReportProblem(userProfile.memId, LocalStorageHelper.getAccessToken(getContext()));
        reportProblem.problemType = problemType;
        reportProblem.toMemId = memId;
        if (problemType == 4)
            reportProblem.toListingId = listingId;
        reportProblem.desc = "";
        Log.d("callReportApi", "callReportApi: " + reportProblem.toString());
        reportProblem.callReportProblemApi(context, new ApiCallback() {
            @Override
            public void success(ApiResponse apiResponse) {
                Log.d("callReportApi", "callReportApi: " + apiResponse.jsonContent);
                AppUtil.showDialog(context, getString(R.string.reportpage__thanks_for), "", false);
            }

            @Override
            public void failure(String errorMsg) {
                AppUtil.showToast(context, getString(R.string.error_message__please_try_again_later));
            }
        });
    }


    private List<Fragment> getFragments(ResponseNewsFeed responseGson) {
        List<Fragment> fList = new ArrayList<Fragment>();
        newsFeedDetailFragment = NewsFeedDetailFragment.newInstance("Fragment 1", pager, pagerIndicator, false);
        newsFeedProfileFragment = NewsFeedProfileFragment.newInstance("Fragment 3", pager);
        addressSearchMidFragment = AddressSearchMidFragment.newInstance("Fragment 2", mInterceptableRelativeLayout, pager, newsFeedDetailFragment, newsFeedProfileFragment, responseGson);

        fList.add(newsFeedDetailFragment);
        fList.add(addressSearchMidFragment);
        fList.add(newsFeedProfileFragment);
        return fList;
    }


    class MyPageAdapter extends FragmentPagerAdapter {
        private List<Fragment> fragments;

        public MyPageAdapter(FragmentManager fm, List<Fragment> fragments) {
            super(fm);
            this.fragments = fragments;
        }

        @Override
        public Fragment getItem(int position) {
            return this.fragments.get(position);
        }

        @Override
        public int getCount() {
            return this.fragments.size();
        }
    }

    public class DepthPageTransformer implements ViewPager.PageTransformer {
        private static final float MIN_SCALE = 0.75f;

        public void transformPage(View view, float position) {
//            Log.e("", "edwin view " + view.getLeft() + " " + view.getId()+" "+view.getRight()+" ");
//            Log.e("", "edwin view getText" +((TextView)view.findViewById(R.id.title)).getText().toString());

            int pageWidth = view.getWidth();
//            Log.i("","edwin transformPage position "+position+" pageWidth "+pageWidth);

            boolean isFirstPage = false;
            boolean isMainPage = false;
            if (view.getLeft() == 0) {
                isFirstPage = true;
            } else if (view.getLeft() == pageWidth) {
                isMainPage = true;
            }

            if (position < -1) { // [-Infinity,-1)
                // This page is way off-screen to the left.
//                view.setAlpha(0);

            } else if (position <= 0) { // [-1,0]
                // Use the default slide transition when moving to the left page
//                view.setAlpha(1);
//                view.setTranslationX(0);
//                if(isMainPage) {
//                    view.setTranslationX(pageWidth / 2 * -position);
//                }
//                view.setScaleX(1);
//                view.setScaleY(1);

//                if(isFirstPage){
//                    view.setTranslationX(pageWidth / 2 * -position);
////                    view.setAlpha(1+position);
//                }

            } else if (position <= 1) { // (0,1]
                // Fade the page out.
//                view.setAlpha(1 - position);

                // Counteract the default slide transition
                if (isFirstPage || isMainPage) {

                } else {
                    view.setTranslationX(pageWidth / 2 * -position);
//                    view.setAlpha(position);
                }

//                // Scale the page down (between MIN_SCALE and 1)
//                float scaleFactor = MIN_SCALE
//                        + (1 - MIN_SCALE) * (1 - Math.abs(position));
//                view.setScaleX(scaleFactor);
//                view.setScaleY(scaleFactor);

            } else { // (1,+Infinity]
                // This page is way off-screen to the right.
//                view.setAlpha(0);
            }
        }
    }

    private void callAddViewedListingApi() {
        try {
            agentProfile = LocalStorageHelper.getLocalAgentProfiles(getActivity(), getCurrentSection());
            final RequestAddViewedListing requestAddViewedListing = new RequestAddViewedListing(userProfile.memId, LocalStorageHelper.getAccessToken(getContext()), "0", agentProfile.memberID, googleAddressPack);
            requestAddViewedListing.callViewedListingApi(this.getContext(), new ApiCallback() {
                @Override
                public void success(ApiResponse apiResponse) {
                }

                @Override
                public void failure(String errorMsg) {
                }
            });


        } catch (Exception e) {

        }

    }

    public List<Fragment> getFragments() {
        return fragments;
    }

    private MainActivityTabBase.CURRENT_SECTION getCurrentSection(){
        return ((MainActivityTabBase)getActivity()).currentSection;
    }


}
