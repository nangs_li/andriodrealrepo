package co.real.productionreal2.service.model.request;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.model.InputRequest;
import co.real.productionreal2.service.OperationService;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.response.ResponseBase;
import co.real.productionreal2.util.NetworkUtil;
import co.real.productionreal2.view.Dialog;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class RequestFollowAgent extends RequestBase {

    public RequestFollowAgent(int memId, String session) {
        super(memId, session);
    }

    @SerializedName("ToRealMemberID")
    public int followingMemId;

    @SerializedName("ToAgentListingID")
    public int followingListingId;

    @SerializedName("ToBatchRealMemberID")
    public List<Integer> toBatchRealMemberID;

    public void callFollowApi(final Context context, final ApiCallback callback) {
        String json = new Gson().toJson(this);
        Log.d("tofollow", "callFollowApi: " + json);
        final OperationService weatherService = new OperationService(context);
        weatherService.getCoffeeService().agentFollow(new InputRequest(json), new Callback<ApiResponse>() {
            @Override
            public void success(ApiResponse apiResponse, Response response) {
                ResponseBase responseGson = new Gson().fromJson(apiResponse.jsonContent, ResponseBase.class);
                if (responseGson.errorCode == 4) {
                    Dialog.accessErrorForceLogoutDialog(context).show();
                } else if (responseGson.errorCode == 26) {
                    Dialog.suspendedErrorForceLogoutDialog(context).show();
                } else if (responseGson.errorCode != 0 && responseGson.errorCode != 20 && responseGson.errorCode != 21 &&
                        responseGson.errorCode != 22 && responseGson.errorCode != 23) {
                    callback.failure(responseGson.errorMsg);
                } else {
                    callback.success(apiResponse);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                callback.failure("");
                NetworkUtil.showNoNetworkMsg(context);
            }
        });
    }

    public void callUnfollowApi(final Context context, final ApiCallback callback) {
        String json = new Gson().toJson(this);
        Log.d("callUnfollowApi", "callUnfollowApi: " + json);
        final OperationService weatherService = new OperationService(context);
        weatherService.getCoffeeService().agentUnfollow(new InputRequest(json), new Callback<ApiResponse>() {
            @Override
            public void success(ApiResponse apiResponse, Response response) {
                ResponseBase responseGson = new Gson().fromJson(apiResponse.jsonContent, ResponseBase.class);
                if (responseGson.errorCode == 4) {
                    Dialog.accessErrorForceLogoutDialog(context).show();
                } else if (responseGson.errorCode == 26) {
                    Dialog.suspendedErrorForceLogoutDialog(context).show();
                } else if (responseGson.errorCode != 0 && responseGson.errorCode != 20 && responseGson.errorCode != 21 &&
                        responseGson.errorCode != 22 && responseGson.errorCode != 23) {
                    callback.failure(responseGson.errorMsg);
                } else {
                    callback.success(apiResponse);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                callback.failure("");
                NetworkUtil.showNoNetworkMsg(context);
            }
        });
    }

}
