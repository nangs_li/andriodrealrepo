package co.real.productionreal2.activity.createpost;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import co.real.productionreal2.Constants;
import co.real.productionreal2.CoreData;
import co.real.productionreal2.Navigation;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.BaseActivity;
import co.real.productionreal2.callback.DialogDualCallback;
import co.real.productionreal2.model.SystemSetting;
import co.real.productionreal2.service.model.request.RequestListing;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;
import co.real.productionreal2.view.Dialog;


public class CreatePostAddLangActivity extends BaseActivity {
    private static final String TAG = "CreatePostAddLangActivity";
    @InjectView(R.id.lvAddLang)
    ListView lvAddLang;
    @InjectView(R.id.rootImageView)
    ImageView rootImageView;

    private ArrayList<Integer> selectedLangIndexList;
    private ArrayList<SystemSetting.SystemLanguage> systemLanguageList;
    private AddLangAdapter addLangAdapter;
    ResponseLoginSocial.Content userContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_post_add_lang);
        userContent = CoreData.getUserContent(this);
        ButterKnife.inject(this);
        initData();
        initView();
    }

    private void initData() {
        BaseCreatePost baseCreatePost = BaseCreatePost.getInstance(this);
        selectedLangIndexList = getLangIndexList(baseCreatePost);

        //sort by position
        systemLanguageList=new ArrayList<>();
        systemLanguageList=(ArrayList)userContent.systemSetting.sysLangList;
        Collections.sort(systemLanguageList, new SystemSetting.SystemLanguage.CompPosition());

        for (SystemSetting.SystemLanguage language : systemLanguageList) {
            language.isEnable=true;
            for (int mIndex : selectedLangIndexList) {
                Log.d("langIndexList", "langIndexList: compare " + language.index + " vs " +language.isEnable);
                Log.d("langIndexList", "langIndexList: compare " + mIndex + " vs " +language.index);
                if (mIndex == language.index) {
                    language.isEnable = false;
                }
            }
        }
    }


    private ArrayList<Integer> getLangIndexList(BaseCreatePost baseCreatePost) {
        ArrayList<Integer> langIndexList = new ArrayList<>();
        for (RequestListing.Reason reason : baseCreatePost.getCreatePostRequest().reasons) {
            langIndexList.add(reason.languageIndex);
        }
        return langIndexList;
    }

    private void initView() {
        addLangAdapter = new AddLangAdapter(this,systemLanguageList);
        lvAddLang.setAdapter(addLangAdapter);
        lvAddLang.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                boolean isViladClick=true;
                for (int mIndex: selectedLangIndexList){
                    if (mIndex==systemLanguageList.get(position).index)
                        isViladClick=false;
                }
                if (isViladClick)
                addLangAdapter.setCurrentPos(position);
            }
        });
    }


    private class AddLangAdapter extends BaseAdapter {
        private ViewHolder holder;
        private List<SystemSetting.SystemLanguage> sysLangList;
        private Context context;
        private int currentPos;

        public AddLangAdapter(Context context, List<SystemSetting.SystemLanguage> sysLangList) {
            this.context = context;
            this.sysLangList = sysLangList;
        }

        public int getCurrentPos() {
            return currentPos;
        }

        public void setCurrentPos(int currentPos) {
            this.currentPos = currentPos;
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return sysLangList.size();
        }

        @Override
        public Object getItem(int position) {
            return sysLangList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            if (convertView == null) {
                convertView = inflater.inflate(R.layout.list_item_single_choice, null);
                holder = createViewHolder(convertView);
                convertView.setTag(holder);
//            } else {
//                holder = (ViewHolder) convertView.getTag();
//            }

            SystemSetting.SystemLanguage language=null;
            language=sysLangList.get(position);
//            for (SystemSetting.SystemLanguage mLang:systemLanguageList){
//                Log.d("langIndexList", "langIndexList: " + mLang.toString());
//                if (mLang.position==position)
//                    language=mLang;
//            }


            if (language!=null) {
                holder.checkedTextView.setText(language.nativeName);
                Log.d("langIndexList", "langIndexList: " + language.toString());
                if (getCurrentPos() == position)
                    holder.checkedTextView.setChecked(true);
                else
                    holder.checkedTextView.setChecked(false);

                //disable selected language option
                Log.d("langIndexList", "langIndexList: test: " + sysLangList.get(position).nativeName + " " + sysLangList.get(position).isEnable);
                if (!language.isEnable) {
                    holder.checkedTextView.setChecked(true);
                    holder.checkedTextView.setAlpha(.5f);
                    holder.checkedTextView.setClickable(false);
                }
            }
            return convertView;
        }

        private ViewHolder createViewHolder(View v) {
            ViewHolder holder = new ViewHolder();
            holder.checkedTextView = (CheckedTextView) v.findViewById(R.id.checkedTextView);
            return holder;
        }


    }

    private static class ViewHolder {
        public CheckedTextView checkedTextView;
    }

    @OnClick(R.id.nextBtn)
    public void nextBtnClick(View view) {
        BaseCreatePost.getInstance(CreatePostAddLangActivity.this).resetCurrentResonsDespImage();
        Intent intent = new Intent(CreatePostAddLangActivity.this, CreatePostWhyActivity.class);
        intent.putExtra(Constants.EXTRA_LANG_INDEX, systemLanguageList.get(addLangAdapter.getCurrentPos()).index);
        intent.putExtra(Constants.EXTRA_IS_ADD_LANG, true);

        Navigation.pushIntentForResult(this,intent,Constants.CREATE_POST_ADD_LANG);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == Constants.CREATE_POST_ADD_LANG) {
            setResult(RESULT_OK, data);
            finish();
        }
    }

    @OnClick(R.id.backBtn)
    public void backBtnClick(View view) {
        finish();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @OnClick(R.id.closeBtn)
    public void closeBtnClick(View view) {
        Dialog.exitSaveChangeDialog(this, new DialogDualCallback() {
            @Override
            public void positive() {
                //reset value
                BaseCreatePost.getInstance(CreatePostAddLangActivity.this).resetBaseCreatePost();
                exitActivity();
            }

            @Override
            public void negative() {
                //save input
                exitActivity();
            }
        }).show();
    }

    private void exitActivity() {
        setResult(RESULT_OK);
        finish();
    }
}
