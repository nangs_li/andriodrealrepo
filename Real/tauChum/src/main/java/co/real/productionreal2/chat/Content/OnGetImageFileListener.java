package co.real.productionreal2.chat.Content;

import java.io.File;

public interface OnGetImageFileListener {

    public void onGotImageFile(File imageFile);
}