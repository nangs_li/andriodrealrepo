package co.real.productionreal2.activity.chat;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import co.real.productionreal2.activity.BaseActivity;
import co.real.productionreal2.chat.core.ApplicationSessionStateCallback;
import co.real.productionreal2.chat.core.ChatService;
import co.real.productionreal2.model.UserQBUser;
import com.quickblox.chat.QBChatService;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.users.model.QBUser;

import java.util.List;

/**
 * Created by hohojo on 16/11/2015.
 */
public class BaseChatActivity extends BaseActivity implements ApplicationSessionStateCallback {
    private static final String TAG = BaseChatActivity.class.getSimpleName();

    private static final String USER_LOGIN_KEY = "USER_LOGIN_KEY";
    private static final String USER_PASSWORD_KEY = "USER_PASSWORD_KEY";

    private boolean sessionActive = false;
    private boolean needToRecreateSession = false;

    private ProgressDialog progressDialog;
    private final Handler handler = new Handler();

    public boolean isSessionActive() {
        return sessionActive;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // 'initialised' will be true if it's the 1st start of the app or if the app's process was killed by OS(or user)
        //
        boolean initialised = ChatService.initIfNeed(this);
        if(initialised && savedInstanceState != null){
            needToRecreateSession = true;
        }else{
            sessionActive = true;
        }
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        if(needToRecreateSession){
            needToRecreateSession = false;

            Log.d(TAG, "Need to restore chat connection");

//            QBUser user = new QBUser();
//            user.setLogin(savedInstanceState.getString(USER_LOGIN_KEY));
//            user.setPassword(savedInstanceState.getString(USER_PASSWORD_KEY));
//
//            savedInstanceState.remove(USER_LOGIN_KEY);
//            savedInstanceState.remove(USER_PASSWORD_KEY);

            recreateSession(UserQBUser.getNewQBUser(this));
        }
    }

    private void recreateSession(QBUser user){
        sessionActive = false;
        this.onStartSessionRecreation();

        showProgressDialog();

        // Restoring Chat session
        //
        if (user == null)
            user = UserQBUser.getNewQBUser(this);
        final QBUser finalUser = user;
        ChatService.initIfNeed(this);
        ChatService.getInstance().login(user, new QBEntityCallbackImpl() {
            @Override
            public void onSuccess() {
                Log.d(TAG, "Chat login onSuccess");

                progressDialog.dismiss();
                progressDialog = null;

                sessionActive = true;
                BaseChatActivity.this.onFinishSessionRecreation(true);
                if (!QBChatService.getInstance().isLoggedIn()) {
                    QBChatService.getInstance().login(UserQBUser.getNewQBUser(BaseChatActivity.this), new QBEntityCallbackImpl() {

                        @Override
                        public void onSuccess() {
                            // success
                            Log.d(TAG, "Chat login onSuccess");
                        }

                        @Override
                        public void onError(List errors) {
                            // errror
                            Log.d(TAG, "Chat login errors = "+errors);
                        }
                    });
                }
            }

            @Override
            public void onError(List errors) {

                Log.d(TAG, "Chat login onError: " + errors);

                Toast toast = Toast.makeText(getApplicationContext(),
                        "Error in the recreate session request, trying again in 3 seconds.. Check you internet connection.", Toast.LENGTH_SHORT);
                toast.show();

                // try again
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        recreateSession(finalUser);
                    }
                }, 3000);

                BaseChatActivity.this.onFinishSessionRecreation(false);
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        ChatService.initIfNeed(this);
//        QBUser currentUser = ChatService.getInstance().getCurrentUser();
//        if(currentUser != null) {
////            outState.putString(USER_LOGIN_KEY, currentUser.getLogin());
////            outState.putString(USER_PASSWORD_KEY, currentUser.getPassword());
//            currentUser = UserQBUser.getNewQBUser(this);
//        }

        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(outState);
    }

    private void showProgressDialog(){
        if(progressDialog == null) {
            progressDialog = new ProgressDialog(BaseChatActivity.this);
            progressDialog.setTitle("Loading");
            progressDialog.setMessage("Restoring chat session...");
            progressDialog.setProgressStyle(progressDialog.STYLE_SPINNER);
        }
        progressDialog.show();
    }


    //
    // ApplicationSessionStateCallback
    //

    @Override
    public void onStartSessionRecreation() {
    }

    @Override
    public void onFinishSessionRecreation(boolean success) {
    }
}
