package co.real.productionreal2.activity.login;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.BaseActivity;
import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.request.RequestLegalStatement;
import co.real.productionreal2.service.model.response.ResponseLegalStatement;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class SignUpPrivacyActivity extends BaseActivity {
    @InjectView(R.id.privacyTextView)
    TextView privacyTextView;
    @InjectView(R.id.progressBar)
    ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_privacy);
        ButterKnife.inject(this);

        progressBar.setVisibility(View.VISIBLE);

        RequestLegalStatement legalStatement = new RequestLegalStatement(0, "");
        legalStatement.langCode = "en";
        legalStatement.msgType = 1;
        legalStatement.callLegalStatementGetApi(this, new ApiCallback() {
            @Override
            public void success(ApiResponse apiResponse) {
                progressBar.setVisibility(View.GONE);
                ResponseLegalStatement responseGson = new Gson().fromJson(apiResponse.jsonContent, ResponseLegalStatement.class);
                privacyTextView.setText(responseGson.content.msg);
            }

            @Override
            public void failure(String errorMsg) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }
}
