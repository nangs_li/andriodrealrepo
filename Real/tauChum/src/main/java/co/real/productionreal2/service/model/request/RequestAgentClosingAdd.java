package co.real.productionreal2.service.model.request;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.model.InputRequest;
import co.real.productionreal2.service.AdminService;
import co.real.productionreal2.service.model.AgentPastClosing;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.response.ResponseBase;
import co.real.productionreal2.util.NetworkUtil;
import co.real.productionreal2.view.Dialog;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class RequestAgentClosingAdd extends RequestBase {

    public RequestAgentClosingAdd(int memId, String session) {
        super(memId, session);
        agentPastCloseList = new ArrayList<AgentPastClosing>();
    }

    @SerializedName("AgentPastClosing")
    public List<AgentPastClosing> agentPastCloseList;

    public void callAddPastClosingApi(final Context context, final ApiCallback callback) {
        String json = new Gson().toJson(this);
        final AdminService weatherService = new AdminService(context);
        weatherService.getCoffeeService().agentProfileClosingAdd(new InputRequest(json), new Callback<ApiResponse>() {
            @Override
            public void success(ApiResponse apiResponse, Response response) {
                ResponseBase responseGson = new Gson().fromJson(apiResponse.jsonContent, ResponseBase.class);
                if (responseGson.errorCode == 4) {
                    Dialog.accessErrorForceLogoutDialog(context).show();
                } else if (responseGson.errorCode == 26) {
                    Dialog.suspendedErrorForceLogoutDialog(context).show();
                } else if (responseGson.errorCode != 0 && responseGson.errorCode != 20 && responseGson.errorCode != 21 &&
                        responseGson.errorCode != 22 && responseGson.errorCode != 23) {
                    callback.failure(responseGson.errorMsg);
                } else {
                    callback.success(apiResponse);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                callback.failure("");
                NetworkUtil.showNoNetworkMsg(context);
            }
        });
    }

    public void callDelPastClosingApi(final Context context, final ApiCallback callback) {
        String json = new Gson().toJson(this);
        final AdminService weatherService = new AdminService(context);
        weatherService.getCoffeeService().agentProfileClosingDel(new InputRequest(json), new Callback<ApiResponse>() {
            @Override
            public void success(ApiResponse apiResponse, Response response) {
                ResponseBase responseGson = new Gson().fromJson(apiResponse.jsonContent, ResponseBase.class);
                if (responseGson.errorCode == 4) {
                    Dialog.accessErrorForceLogoutDialog(context).show();
                } else if (responseGson.errorCode == 26) {
                    Dialog.suspendedErrorForceLogoutDialog(context).show();
                } else if (responseGson.errorCode != 0 && responseGson.errorCode != 20 && responseGson.errorCode != 21 &&
                        responseGson.errorCode != 22 && responseGson.errorCode != 23) {
                    callback.failure(responseGson.errorMsg);
                } else {
                    callback.success(apiResponse);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                callback.failure("");
                NetworkUtil.showNoNetworkMsg(context);
            }
        });
    }
}
