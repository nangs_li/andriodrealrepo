package co.real.productionreal2.newsfeed;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import co.real.productionreal2.R;
import co.real.productionreal2.model.AgentListing;
import com.squareup.picasso.Picasso;

import java.util.List;

public class NewsFeed_AgentProfilesPhotoListAdapter extends RecyclerView.Adapter<NewsFeed_AgentProfilesPhotoListAdapter.ViewHolder>{

    Context context;
    List<AgentListing.Photo> photos;

    public NewsFeed_AgentProfilesPhotoListAdapter(Context context, List<AgentListing.Photo> photos) {
        super();
        this.context = context;
        this.photos = photos;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_agentprofiles_imagelist_item, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        AgentListing.Photo photo = photos.get(i);
        Picasso.with(context).load(photo.url).into(viewHolder.iv_thumbnail);
    }

    @Override
    public int getItemCount() {
        return photos.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        public ImageView iv_thumbnail;

        public ViewHolder(View itemView) {
            super(itemView);
            iv_thumbnail = (ImageView)itemView.findViewById(R.id.iv_thumbnail);
        }
    }
}
