package co.real.productionreal2.common;

import android.content.Context;
import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.real.productionreal2.activity.MainActivityTabBase;
import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.chat.pushnotifications.Consts;
import co.real.productionreal2.daomanager.DatabaseManager;
import co.real.productionreal2.service.model.AgentProfiles;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.request.RequestNewsFeed;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;
import co.real.productionreal2.service.model.response.ResponseNewsFeed;
import co.real.productionreal2.util.NetworkUtil;
import co.real.productionreal2.view.TabMenu;

/**
 * Created by kelvinsun on 29/1/16.
 */
public class IntervalTimerHelper {

    private String TAG = "IntervalTimerHelper";


    private ResponseLoginSocial.Content userProfile;

    boolean isRunning = false;
    private static int renewTime = 60000 * 3; // TODO: 60000*5 , 30s for testing
    private int newCount;
    Context context;

    private static CountDownTimer countDownTimer;

    public static CountDownTimer getTimerInstance() {
        return countDownTimer;
    }

    private static IntervalTimerHelper helper = new IntervalTimerHelper();

    public static IntervalTimerHelper getInstance() {
        return helper;
    }

    public IntervalTimerHelper() {
    }

    public IntervalTimerHelper(Context ctx) {
        this.isRunning = true;
        this.context = ctx;
        try {
            userProfile = new Gson().fromJson(DatabaseManager.getInstance(ctx).getLogInfo().getLogInfo(),
                    ResponseLoginSocial.Content.class);
        } catch (Exception e) {
            e.printStackTrace();
            this.isRunning = false;
        }

    }


    public void Count() {

        if (countDownTimer != null && userProfile == null) {
            countDownTimer.cancel();
            return;
        }

        if (countDownTimer != null) {
            countDownTimer.cancel();
        }

        countDownTimer = new CountDownTimer(renewTime, 1000) {

            public void onTick(long millisUntilFinished) {

                String min = String.valueOf(millisUntilFinished / 60000);

                long re = millisUntilFinished % 60000;
                String secs = String.valueOf(re / 1000);

//                Log.d(TAG, "timer: " + min + " ' " + secs + " ''");
                isRunning = true;

            }

            public void onFinish() {
                // finish action
                long myCurrentTimeMillis = System.currentTimeMillis();
                Log.d(TAG, "onFinish " + myCurrentTimeMillis);

                if (NetworkUtil.isConnected(context)) {
                    callGetNewsFeedApi();
                }

                isRunning = false;

            }

        };

        countDownTimer.start();


    }

    /**
     * Refresh newsfeed list in background
     */
    public void callGetNewsFeedApi() {

        try {
            userProfile = new Gson().fromJson(DatabaseManager.getInstance(context).getLogInfo().getLogInfo(),
                    ResponseLoginSocial.Content.class);
        } catch (Exception e) {
            e.printStackTrace();
            this.isRunning = false;
        }

        if (userProfile == null)
            return;

        final RequestNewsFeed requestNewsFeed = new RequestNewsFeed(userProfile.memId, LocalStorageHelper.getAccessToken(context), "test_uniqueKey", 1, 1);

        requestNewsFeed.callNewsFeedApi(context, new

                        ApiCallback() {
                            @Override
                            public void success(ApiResponse apiResponse) {
                                ResponseNewsFeed responseGson = new Gson().fromJson(apiResponse.jsonContent, ResponseNewsFeed.class);
                                Log.d("NewsfeedSearchFragment", "IntervalTimerHelper: fetchAPI: " + responseGson.content.toString());


                                int newCount = LocalStorageHelper.defaultHelper(context).getNewCount(TabMenu.TAB_SECTION.NEWSFEED);
                                List<AgentProfiles> tempList = LocalStorageHelper.getLocalAgentProfileList(context, true);
                                Date latestAgentListingDate = new Date();
                                if (tempList.size() > 0) {
                                    latestAgentListingDate = tempList.get(0).agentListing.getCreationDate();
                                }
                                for (AgentProfiles agentProfiles : responseGson.content.agentProfiles) {
                                    if (latestAgentListingDate.before(agentProfiles.agentListing.getCreationDate())) {
                                        newCount++;
                                    }
                                }

                                List<AgentProfiles> agentProfileList = new ArrayList<AgentProfiles>();
                                agentProfileList.addAll(responseGson.content.agentProfiles);
                                LocalStorageHelper.saveLocalAgentProfileListToLocalStorage(context, agentProfileList, MainActivityTabBase.CURRENT_SECTION.NEWSFEED);

                                //update alert on tab
                                LocalStorageHelper.defaultHelper(context).saveNewCountToLocalStorage(newCount, TabMenu.TAB_SECTION.NEWSFEED);
                                // notify about new count
                                Intent intentNewCountPush = new Intent(Consts.NEWSFEED_NEW_COUNT_EVENT);
                                LocalBroadcastManager.getInstance(context).sendBroadcast(intentNewCountPush);

                                //update renew
                                countDownTimer.start();

                            }

                            @Override
                            public void failure(String errorMsg) {
//                                Toast.makeText(context, "callGetNewsFeedApi fail: " + errorMsg, Toast.LENGTH_LONG).show();
                            }
                        }

        );
    }
}
