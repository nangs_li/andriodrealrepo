package co.real.productionreal2.service.model.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by hohojo on 18/11/2015.
 */
public class ResponseChatRoomCreate extends ResponseBase{
    @SerializedName("Content")
    public Content content;

    public class Content {
        @SerializedName("ChatRoomID")
        public int chatRoomId;
    }
}
