package co.real.productionreal2.util;

import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Data;
import android.util.Log;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import java.util.ArrayList;

import co.real.productionreal2.dao.account.DBPhoneBook;
import co.real.productionreal2.service.model.RePhonebookItem;
import co.real.productionreal2.service.model.ReUploadContact;

/**
 * Created by kelvinsun on 1/6/16.
 */
public class PhoneBookUtil {

    public static ArrayList<ReUploadContact> loadUploadContactFromLocal(Context context) {
        ArrayList<ReUploadContact> agentList = new ArrayList<>();
        Cursor data = context.getContentResolver().query(
                ContactsContract.Contacts.CONTENT_URI,
                null,
                null,
                null,
                null);

        while (data.moveToNext()) {
            String id = data.getString(data.getColumnIndex(ContactsContract.Contacts._ID));
            String name = data.getString(data.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
            String email = "", countryCode = "", phoneNo = "";
            if (Integer.parseInt(data.getString(data.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                // get the phone number
                Cursor pCur = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                        new String[]{id}, null);
                while (pCur.moveToNext()) {
                    String phone = pCur.getString(
                            pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
                    try {
                        Phonenumber.PhoneNumber phoneNumberProto = phoneUtil.parse(phone, LocationUtil.getCountryBasedOnSimCardOrNetwork(context).toUpperCase());
                        //add + sign as prefix
                        countryCode = "+" + String.valueOf(phoneNumberProto.getCountryCode());
                        phoneNo = String.valueOf(phoneNumberProto.getNationalNumber());
                    } catch (NumberParseException e) {
                        System.err.println("NumberParseException was thrown: " + e.toString());
                    }
                }
                pCur.close();

                // get email and type
                Cursor emailCur = context.getContentResolver().query(
                        ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                        null,
                        ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                        new String[]{id}, null);
                while (emailCur.moveToNext()) {
                    // This would allow you get several email addresses
                    // if the email addresses were stored in an array
                    email = emailCur.getString(
                            emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                }
                emailCur.close();
            }
            //only insert valid data either with phone number or email
            if (isDataValid(countryCode, phoneNo)||ValidUtil.isValidEmail(email)) {
                ReUploadContact reUploadContact = new ReUploadContact(name, countryCode, phoneNo, email);
                agentList.add(reUploadContact);
            }
        }
        data.close();

        return agentList;
    }

    //retrieve phonebook list
    public static ArrayList<DBPhoneBook> loadPhoneBookFromLocal(Context context) {
        ArrayList<DBPhoneBook> phoneBookList = new ArrayList<>();
        Cursor phones = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
        while (phones.moveToNext()) {
            String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            boolean hasPhoneNumber = phones.getInt(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.HAS_PHONE_NUMBER)) == 1 ? true : false;
            //show contact with number ONLY
            if (hasPhoneNumber) {
                PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
                try {
                    Phonenumber.PhoneNumber phoneNumberProto = phoneUtil.parse(phoneNumber, LocationUtil.getCountryBasedOnSimCardOrNetwork(context).toUpperCase());
                    //add + sign as prefix
                    String countryCode = "+" + String.valueOf(phoneNumberProto.getCountryCode());
                    String phoneNo = String.valueOf(phoneNumberProto.getNationalNumber());
                    if (isDataValid(countryCode, phoneNo)) {
                        DBPhoneBook reUploadContact = new DBPhoneBook();
                        reUploadContact.setCountryCode(countryCode);
                        reUploadContact.setPhoneNumber(Long.valueOf(phoneNo));
                        reUploadContact.setName(name);
                        phoneBookList.add(reUploadContact);
                    }
                } catch (NumberParseException e) {
                    System.err.println("NumberParseException was thrown: " + e.toString());
                }
            }
        }
        phones.close();

        return phoneBookList;
    }

    private static boolean isDataValid(String countryCode, String phoneNo) {
        if (countryCode == "" || phoneNo == "") {
            return false;
        }
        Phonenumber.PhoneNumber numberProto =
                new Phonenumber.PhoneNumber().setCountryCode(Integer.valueOf(countryCode)).setNationalNumber(Long.valueOf(phoneNo));
        return PhoneNumberUtil.getInstance().isValidNumber(numberProto);
    }

}
