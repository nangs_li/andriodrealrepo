package co.real.productionreal2.model;

import android.content.Context;

import co.real.productionreal2.common.LocalStorageHelper;

import java.util.ArrayList;

/**
 * Created by hohojo on 4/3/2016.
 */
public class CurrentFollower {
    public CurrentFollower(int memId, int followingMemId, String qbId, String followingQbId, boolean isfollowing) {
        this.memId = memId;
        this.followingMemId = followingMemId;
        this.qbId = qbId;
        this.followingQbId = followingQbId;
        this.isfollowing = isfollowing;
    }

    private  int isInFollowingList(ArrayList<CurrentFollower> currentFollowerList, int followMemId) {
        if (currentFollowerList != null) {
            for (int i = 0; i < currentFollowerList.size(); i++) {
                if (currentFollowerList.get(i).memId == this.memId &&
                        currentFollowerList.get(i).followingMemId == followMemId) {
                    return i;
                }
            }
        }
        return -1;
    }


    public void updateCurrentFollowingList(Context context) {
        ArrayList<CurrentFollower> currentFollowerList = LocalStorageHelper.getCurrentFollowerList(context);
        int currentFollowingListItem = isInFollowingList(currentFollowerList, this.followingMemId);
        if (currentFollowingListItem != -1) {
            currentFollowerList.get(currentFollowingListItem).isfollowing = this.isfollowing;
        } else
            currentFollowerList.add(this);

        LocalStorageHelper.defaultHelper(context).saveCurrentFollowingListToLocalStorage(currentFollowerList);
    }


//    public void updateCurrentFollowingList(Context context,  int followMemId) {
//        ArrayList<CurrentFollower> currentFollowerList = LocalStorageHelper.getCurrentFollowerList(context);
//        if (currentFollowerList == null || currentFollowerList.size() == 0)
//            currentFollowerList = new ArrayList<>();
//
//        int currentFollowingListItem = isInFollowingList(currentFollowerList, followMemId);
//        if (currentFollowingListItem != -1)
//            currentFollowerList.get(currentFollowingListItem).isfollowing = true;
//        else
//            currentFollowerList.add(this);
//
//        LocalStorageHelper.defaultHelper(context).saveCurrentFollowingListToLocalStorage(currentFollowerList);
//    }
//    public static void followCurrentFollowingList(Context context) {
//        ArrayList<CurrentFollower> currentFollowerList = LocalStorageHelper.getCurrentFollowerList(context);
//        if (currentFollowerList == null || currentFollowerList.size() == 0)
//            currentFollowerList = new ArrayList<>();
//
//        int currentFollowingListItem = isInFollowingList(currentFollowerList, followMemId);
//    }
//    public static void unfollowCurrentFollowingList(Context context, int unfollowMemId) {
//        ArrayList<CurrentFollower> currentFollowerList = LocalStorageHelper.getCurrentFollowerList(context);
//        if (currentFollowerList != null &&
//                currentFollowerList.size() > 0) {
//            for (int i = 0; i < currentFollowerList.size(); i++) {
////                if (mAgentProfile.memberID == currentFollowerList.get(i).memId)
//                if (unfollowMemId == currentFollowerList.get(i).memId)
//                    currentFollowerList.remove(i);
//            }
//            LocalStorageHelper.defaultHelper(context).saveCurrentFollowingListToLocalStorage(currentFollowerList);
//        }
//    }


    public int memId;
    public int followingMemId;
    public String qbId;
    public String followingQbId;
    public boolean isfollowing;
}
