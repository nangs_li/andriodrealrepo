package co.real.productionreal2.chat;

import android.content.Context;

import com.google.gson.Gson;
import com.quickblox.chat.model.QBDialog;

import java.util.ArrayList;
import java.util.List;

import co.real.productionreal2.ApplicationSingleton;
import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.chat.core.ChatService;
import co.real.productionreal2.common.LocalStorageHelper;
import co.real.productionreal2.daomanager.DatabaseManager;
import co.real.productionreal2.service.BaseService;
import co.real.productionreal2.service.model.AgentProfiles;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.request.RequestAgentProfileGetListEx;
import co.real.productionreal2.service.model.request.RequestChatRoomGet;
import co.real.productionreal2.service.model.response.ResponseAgentProfileListEx;
import co.real.productionreal2.service.model.response.ResponseChatRoomGet;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;
import co.real.productionreal2.util.QBUtil;
import co.real.productionreal2.view.Dialog;

/**
 * Created by hohojo on 1/3/2016.
 */
public class ChatLobbyDbApi {
    private Context context;
    private ApiCallback mainCallback;

    public ChatLobbyDbApi(Context context) {
        this.context = context;
    }

    public void getDialogsFromQBAndGetAgentProfileListAndChatSetting(ApiCallback apiCallback) {
        this.mainCallback = apiCallback;
        ApplicationSingleton.getInstance().currentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ChatService.initIfNeed(context);
                ChatService.getInstance().getAllDialogs(new BaseService.BaseServiceCallBack() {
                    @Override
                    public void onSuccess(Object var1) {
                        if (mainCallback != null) {
                            mainCallback.success(null);
                        }
                    }

                    @Override
                    public void onError(BaseService.RealServiceError serviceError) {
                        if (mainCallback != null) {
                            mainCallback.failure(null);
                        }
                    }
                });
            }
        });
    }

    private void getUserProfileListAndChatSetting(final ArrayList<QBDialog> dialogs) {
        List<String> qbIds = new ArrayList<>();

        ResponseLoginSocial.Content userContent = new Gson().fromJson(DatabaseManager.getInstance(context).getLogInfo().getLogInfo(),
                ResponseLoginSocial.Content.class);
        final String currentQbId = userContent.qbid;
        for (int i = 0; i < dialogs.size(); i++) {
            qbIds.add(String.valueOf(QBUtil.getOpponentIDForPrivateDialog(dialogs.get(i), currentQbId)));
        }


        RequestAgentProfileGetListEx request = new RequestAgentProfileGetListEx(userContent.memId, LocalStorageHelper.getAccessToken(context), qbIds);
        request.callAgentProfileGetListExApi(context, new ApiCallback() {
            @Override
            public void success(ApiResponse apiResponse) {
                ResponseAgentProfileListEx agentProfileListResponseEx = new Gson()
                        .fromJson(apiResponse.jsonContent,
                                ResponseAgentProfileListEx.class);

                if (agentProfileListResponseEx.errorCode == 4) {
                    Dialog.accessErrorForceLogoutDialog(context).show();
                } else if (agentProfileListResponseEx.errorCode == 26) {
                    Dialog.suspendedErrorForceLogoutDialog(context).show();
                } else if (agentProfileListResponseEx.errorCode != 0 && agentProfileListResponseEx.errorCode != 20 && agentProfileListResponseEx.errorCode != 21 &&
                        agentProfileListResponseEx.errorCode != 22 && agentProfileListResponseEx.errorCode != 23) {
                    mainCallback.failure(agentProfileListResponseEx.errorMsg);
                } else {
                    List<AgentProfiles> agentProfileList = agentProfileListResponseEx.content.profiles;
                    //update profiles
                    getChatSettingAndDbAction(dialogs, currentQbId, agentProfileList);
                }

            }

            @Override
            public void failure(String errorMsg) {
                mainCallback.failure(errorMsg);
            }
        });

    }

    private void getChatSettingAndDbAction(final ArrayList<QBDialog> dialogs, final String currentQbId, final List<AgentProfiles> userProfileList) {
        ResponseLoginSocial.Content userContent = new Gson().fromJson(DatabaseManager.getInstance(context).getLogInfo().getLogInfo(),
                ResponseLoginSocial.Content.class);
        RequestChatRoomGet chatRoomGet = new RequestChatRoomGet(userContent.memId, LocalStorageHelper.getAccessToken(context));
        chatRoomGet.callChatRoomGetApi(context, new ApiCallback() {
            @Override
            public void success(ApiResponse apiResponse) {
                ResponseChatRoomGet responseChatRoomGet = new Gson().fromJson(apiResponse.jsonContent, ResponseChatRoomGet.class);
                if (responseChatRoomGet.errorCode == 4) {
                    Dialog.accessErrorForceLogoutDialog(context).show();
                } else if (responseChatRoomGet.errorCode == 26) {
                    Dialog.suspendedErrorForceLogoutDialog(context).show();
                } else if (responseChatRoomGet.errorCode != 0 && responseChatRoomGet.errorCode != 20 && responseChatRoomGet.errorCode != 21 &&
                        responseChatRoomGet.errorCode != 22 && responseChatRoomGet.errorCode != 23) {
                    mainCallback.failure(responseChatRoomGet.errorMsg);
                } else {
                    ChatLobbyDbViewController.getInstance(context).updateDialogDb(dialogs, userProfileList, currentQbId, responseChatRoomGet);
                    if (mainCallback != null) {
                        mainCallback.success(null);
                    }
                }


            }

            @Override
            public void failure(String errorMsg) {
                mainCallback.failure(errorMsg);
//                Toast.makeText(context, "getChatSettingAndDbAction error: " + errorMsg, Toast.LENGTH_LONG).show();
            }
        });
    }
}
