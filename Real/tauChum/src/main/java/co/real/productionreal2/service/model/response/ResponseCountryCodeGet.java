package co.real.productionreal2.service.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseCountryCodeGet extends ResponseBase {

    @SerializedName("Content")
    public Content content;

    public class Content {
        @SerializedName("SMSCountryCodeList")
        public List<SMSCountryCode> sMSCountryCodeList;
    }

    public class SMSCountryCode {
        @SerializedName("k")
        public String k;

        @SerializedName("v")
        public String v;
    }

}
