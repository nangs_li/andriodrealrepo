package co.real.productionreal2.Event;

public class UpdateInputPhoneNumberEvent {
    public final String countryCode;
    public final String phoneNumber;
    public final int phoneRegID;
    public final int regType;

    public UpdateInputPhoneNumberEvent(String countryCode, String phoneNumber, int phoneRegID, int regType) {
        this.countryCode = countryCode;
        this.phoneNumber = phoneNumber;
        this.phoneRegID = phoneRegID;
        this.regType = regType;
    }
}
