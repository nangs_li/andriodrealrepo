package co.real.productionreal2.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import co.real.productionreal2.R;
import co.real.productionreal2.activity.MainActivityTabBase;
import co.real.productionreal2.view.TitleBar;

/**
 * Created by hohojo on 13/10/2015.
 */
public class ChatUnloginFragment extends Fragment {

    private TitleBar mTitlebar;

    public static ChatUnloginFragment newInstance() {

        Bundle args = new Bundle();

        ChatUnloginFragment fragment = new ChatUnloginFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTitlebar = ((MainActivityTabBase) getActivity()).getTitleBar();
        mTitlebar.setQBunlogin();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_chan_unlogin, container, false);
        return view;
    }


}
