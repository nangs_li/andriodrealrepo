package co.real.productionreal2.activity.login;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.mixpanel.android.mpmetrics.MixpanelAPI;

import java.util.Locale;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import co.real.productionreal2.Constants;
import co.real.productionreal2.Navigation;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.BaseActivity;
import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.callback.LoginDbViewListener;
import co.real.productionreal2.config.AppConfig;;
import co.real.productionreal2.common.LocalStorageHelper;
import co.real.productionreal2.daomanager.ConfigDataBaseManager;
import co.real.productionreal2.daomanager.DatabaseManager;
import co.real.productionreal2.login.FacebookLoginController;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.request.RequestRealNetworkCheckEmailAvailability;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;
import co.real.productionreal2.util.AppUtil;
import co.real.productionreal2.util.NetworkUtil;
import co.real.productionreal2.util.PermissionUtil;
import co.real.productionreal2.util.ValidUtil;


public class SignUpActivity extends BaseActivity implements LoginDbViewListener {
    @InjectView(R.id.emailEditText)
    EditText emailEditText;
    @InjectView(R.id.errorTextView)
    TextView errorTextView;
    @InjectView(R.id.rlNext)
    Button btnNext;
    @InjectView(R.id.tv_signup_title)
    TextView tvSignupTitle;

    private static final String TAG = "SignUpActivity";
    private ProgressDialog ringProgressDialog;
    private FacebookLoginController facebookLoginController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        //set current locale
        Locale locale = AppUtil.getCurrentLangLocale(this);
        Locale.setDefault(locale);

        ButterKnife.inject(this);

        tvSignupTitle.setText(Html.fromHtml(getString(R.string.sign_up__createanaccount)));
        errorTextView.setVisibility(View.INVISIBLE);

        emailEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    pressDone();
                }
                return false;
            }
        });


        emailEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                btnNext.setEnabled(true);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    @OnClick(R.id.loginFbRelativeLayout)
    public void loginFbRelativeLayoutClick(View view) {
        if (NetworkUtil.isConnected(this)) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.READ_PHONE_STATE)
                    != PackageManager.PERMISSION_GRANTED) {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_PHONE_STATE},
                        PermissionUtil.MY_PERMISSIONS_REQUEST_READ_PHONE_STATUS_FB);

                // MY_PERMISSIONS_REQUEST_READ_PHONE_STATUS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
                return;
            }
           facebookLogin();
        } else {
            NetworkUtil.showNoNetworkMsg(this);
        }
    }

    @OnClick(R.id.rlNext)
    public void rlNextClick(View view) {
        pressDone();
    }

    private void facebookLogin(){
        // Mixpanel
        MixpanelAPI mixpanel = MixpanelAPI.getInstance(this, AppConfig.getMixpanelToken());
        mixpanel.track("Chose Facebook Signup");
        facebookLoginController = new FacebookLoginController(this);
    }


    private void hideLoginRingProgressDialog() {
        if (ringProgressDialog != null) {
            ringProgressDialog.dismiss();
            ringProgressDialog.cancel();
            ringProgressDialog = null;
        }
    }

    private void showLoginRingProgressDialog(String title, String message) {
        ringProgressDialog = ProgressDialog.show(this, title, message);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i(TAG, "onActivityResult " + requestCode + " " + resultCode);
        if (resultCode == RESULT_CANCELED) {
//            hideLoginRingProgressDialog();
        }
        switch (requestCode) {
            case FacebookLoginController.REQUEST_CODE:
                showLoginRingProgressDialog(getString(R.string.alert__Initializing), getString(R.string.common__login_facebook));
                facebookLoginController.onActivityResult(requestCode, resultCode, data);
                break;
//            default:
//                googleplusLoginController.connectGoogleApiClient();
//                break;

        }

    }

    void pressDone() {

        // Mixpanel
        MixpanelAPI mixpanel = MixpanelAPI.getInstance(this, AppConfig.getMixpanelToken());
        mixpanel.track("Chose Email Signup");

        if (emailEditText.getText().toString().isEmpty()) {
            errorTextView.setText(R.string.error_message__email_cannot_be);
            errorTextView.setVisibility(View.VISIBLE);
//            Dialog.noEmailDialog(this).show();
        } else {
            if (ValidUtil.isValidEmail(emailEditText.getText().toString())) {

                errorTextView.setVisibility(View.INVISIBLE);
//                if (ringProgressDialog != null && ringProgressDialog.isShowing())
//                    return;
//                showLoginRingProgressDialog(getResources().getString(R.string.please_wait),
//                        getResources().getString(R.string.reset_email_sent_dialog_title));

                btnNext.setEnabled(false);

                // Mixpanel
                mixpanel = MixpanelAPI.getInstance(this, AppConfig.getMixpanelToken());
                mixpanel.getPeople().set("$email", emailEditText.getText().toString());

                RequestRealNetworkCheckEmailAvailability checkEmailAvailability = new RequestRealNetworkCheckEmailAvailability(emailEditText.getText().toString());
                checkEmailAvailability.callRealNetworkCheckEmailAvailabilityApi(this, new ApiCallback() {
                    @Override
                    public void success(ApiResponse apiResponse) {
                        Intent intent = new Intent(SignUpActivity.this, SignUpInfoActivity.class);
                        intent.putExtra(Constants.EXTRA_SIGN_UP_EMAIL, emailEditText.getText().toString());
                        Navigation.pushIntent(SignUpActivity.this, intent);
//                        if (ringProgressDialog != null) {
//                            ringProgressDialog.dismiss();
//                            ringProgressDialog.cancel();
//                            ringProgressDialog = null;
//                        }
                    }

                    @Override
                    public void failure(String errorMsg) {
                        btnNext.setEnabled(true);
                        if (errorMsg.trim() != "")
                            AppUtil.showDialog(SignUpActivity.this, errorMsg, "", false);
//                        if (ringProgressDialog != null) {
//                            ringProgressDialog.dismiss();
//                            ringProgressDialog.cancel();
//                            ringProgressDialog = null;
//                        }
                    }
                });
            } else {
                errorTextView.setText(R.string.error_message__please_enter_valid);
                errorTextView.setVisibility(View.VISIBLE);
//                Dialog.emailNotValidDialog(this).show();
            }

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PermissionUtil.MY_PERMISSIONS_REQUEST_READ_PHONE_STATUS_FB: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    facebookLogin();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;

                // other 'case' lines to check for other
                // permissions this app might request
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        btnNext.setEnabled(true);
    }


    @Override
    public void updateLoginDbAndToMainTabActivity(ResponseLoginSocial.Content content) {
        LocalStorageHelper.setUpByLogin(this, content);

        ConfigDataBaseManager configDataBaseManager = ConfigDataBaseManager.getInstance(this);
        DatabaseManager.getInstance(this).dbActionForLogin(content);
        configDataBaseManager.dbActionMain(content.memId);

        hideLoginRingProgressDialog();
        AppUtil.goToMainTabActivity(this, content);
    }

    @Override
    public void loginFail(String failMsg) {
        hideLoginRingProgressDialog();
    }

    @Override
    public void loginCancel() {
        hideLoginRingProgressDialog();
    }
}
