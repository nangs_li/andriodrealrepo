package co.real.productionreal2.activity.login;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.VideoView;

import com.mixpanel.android.mpmetrics.MixpanelAPI;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.ButterKnife;
import butterknife.InjectView;
import co.real.productionreal2.Constants;
import co.real.productionreal2.CoreData;
import co.real.productionreal2.Navigation;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.BaseActivity;
import co.real.productionreal2.common.LocalStorageHelper;
import co.real.productionreal2.config.AppConfig;
import co.real.productionreal2.fragment.InputPhoneNoFragment;
import co.real.productionreal2.fragment.VerifyPhoneNoFragment;
import co.real.productionreal2.util.AppUtil;
import co.real.productionreal2.util.DBUtil;

/**
 * Created by hohojo on 24/8/2015.
 */
public class IntroActivity extends BaseActivity  {
    private static final String TAG = "IntroActivity";

    @InjectView(R.id.vv_bg)
    VideoView vvBg;
    @InjectView(R.id.vp_intro)
    ViewPager vp_intro;

    InputPhoneNoFragment inputPhoneNoFragment;
    VerifyPhoneNoFragment verifyPhoneNoFragment;

    public void goToInputPage(){
        vp_intro.setCurrentItem(0,true);
    }

    public void goToVerifyPage(){
        vp_intro.setCurrentItem(1,true);
//        verifyPhoneNumberFragment.initView();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DBUtil.copyDB(this);
        CoreData.isOldUser = getIntent().getBooleanExtra(Constants.EXTRA_IS_OLD_USER,false);

        setContentView(R.layout.activity_intro);
        ButterKnife.inject(this);

        //if no previous setting, load default language
        if (LocalStorageHelper.getCurrentLocale(this)==null)
        AppUtil.setLocale(this, Locale.getDefault());

        initView();


    }

    @Override
    protected void onResume() {
        super.onResume();
        initVideoView();
    }

    private void initVideoView() {

        Uri uri= Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.real_for_android_1080p_lower);
        vvBg.setMediaController(null);
        vvBg.setVideoURI(uri);
        vvBg.start();

        //Video Loop
        vvBg.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                vvBg.start(); //need to make transition seamless.
            }
        });


//        Animation a = AnimationUtils.loadAnimation(this, R.anim.fade_in);
//        a.reset();
//        a.setDuration(2500);
//        flTopLayer.clearAnimation();
//        flTopLayer.startAnimation(a);
    }

    private void initView() {

        MyPageAdapter pageAdapter = new MyPageAdapter(getSupportFragmentManager(), getFragments());
        vp_intro.setAdapter(pageAdapter);
        vp_intro.setOffscreenPageLimit(2);

    }
    private List<Fragment> getFragments() {
        List<Fragment> fList = new ArrayList<Fragment>();
        inputPhoneNoFragment = InputPhoneNoFragment.newInstance(this);
        verifyPhoneNoFragment = VerifyPhoneNoFragment.newInstance("1","2",3);

        fList.add(inputPhoneNoFragment);
        fList.add(verifyPhoneNoFragment);
        return fList;
    }


    class MyPageAdapter extends FragmentPagerAdapter {
        private List<Fragment> fragments;

        public MyPageAdapter(FragmentManager fm, List<Fragment> fragments) {
            super(fm);
            this.fragments = fragments;
        }

        @Override
        public Fragment getItem(int position) {
            return this.fragments.get(position);
        }

        @Override
        public int getCount() {
            return this.fragments.size();
        }
    }



    public void signinClick(View view) {

        // Mixpanel
        MixpanelAPI mixpanel = MixpanelAPI.getInstance(this, AppConfig.getMixpanelToken());
        mixpanel.track("Attempted to Log In");

        goToSignIn();
    }

    public void registerClick(View view) {
        // Mixpanel
        MixpanelAPI mixpanel = MixpanelAPI.getInstance(this, AppConfig.getMixpanelToken());
        mixpanel.track("Attempted to Sign Up");

        goToRegister();
    }

    private void goToSignIn() {
        Intent intent = new Intent(this, RealLoginActivity.class);
        Navigation.pushIntent(this,intent);
    }

    private void goToRegister() {
        Intent intent = new Intent(this, SignUpActivity.class);
        Navigation.pushIntent(this,intent);
    }

    public void realClick(View view) {

    }


    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    public void onBackPressed() {
        if(vp_intro.getCurrentItem()!=0){
            vp_intro.setCurrentItem(vp_intro.getCurrentItem()-1);
            return;
        }
        super.onBackPressed();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


}
