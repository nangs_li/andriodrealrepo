package co.real.productionreal2.login;

import android.content.Context;
//import com.sina.weibo.sdk.auth.AuthInfo;
//import com.sina.weibo.sdk.auth.Oauth2AccessToken;
//import com.sina.weibo.sdk.auth.WeiboAuthListener;
//import com.sina.weibo.sdk.exception.WeiboException;
//import com.sina.weibo.sdk.net.RequestListener;
//import com.sina.weibo.sdk.openapi.UsersAPI;
//import com.sina.weibo.sdk.openapi.models.User;
//import com.sina.weibo.sdk.widget.LoginButton;

//import weibo4j.Weibo;

/**
 * Created by hohojo on 26/8/2015.
 */
public class WeiboLoginController extends BaseLoginController {
//    private static final String TAG = "WeiboLoginController";
//    private static WeiboLoginController instance;
//    private AuthInfo authInfo;
//    public static final int REQUEST_CODE = 32973;
//    private static final String APP_KEY = "1927216851";
//    private static final String REDIRECT_URL = "http://api.weibo.com/oauth2/default.html";
//    public static final String SCOPE =
//            "email,direct_messages_read,direct_messages_write,"
//                    + "friendships_groups_read,friendships_groups_write,statuses_to_me_read,"
//                    + "follow_app_official_microblog," + "invitation_write";
//    private AuthListener loginListener = new AuthListener();
//    private LoginButton weiboLoginBtn;
//    private static Oauth2AccessToken oauth2AccessToken;
//
//
//    public WeiboLoginController(Context context, LoginButton weiboLoginBtn) {
//        super(context);
//        this.weiboLoginBtn = weiboLoginBtn;
//        authInfo = new AuthInfo(context, APP_KEY, REDIRECT_URL, SCOPE);
//        this.weiboLoginBtn.setWeiboAuthInfo(authInfo, loginListener);
//    }
//
//    public static WeiboLoginController getInstance(Context context, LoginButton weiboLoginBtn) {
//        if (instance == null) {
//            instance = new WeiboLoginController(context, weiboLoginBtn);
//        }
//        return instance;
//    }
//
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        weiboLoginBtn.onActivityResult(requestCode, resultCode, data);
//    }
//
//    private class AuthListener implements WeiboAuthListener {
//
//        @Override
//        public void onComplete(Bundle values) {
//            Oauth2AccessToken accessToken = Oauth2AccessToken.parseAccessToken(values);
//            if (accessToken != null && accessToken.isSessionValid()) {
//                //TODO this is the accessToken
//                WeiboLoginController.oauth2AccessToken = accessToken;
//                UsersAPI usersAPI = new UsersAPI(context, APP_KEY, accessToken);
//                long uid = Long.parseLong(accessToken.getUid());
//                usersAPI.show(uid, weiboRequestListener);
////                accessToken.getUid();
////                accessToken.getToken();
//            }
//        }
//
//        @Override
//        public void onWeiboException(WeiboException e) {
//            loginFail("Weibo fail: " + e);
//            Log.e(TAG, "Weibo fail: " + e);
//        }
//
//        @Override
//        public void onCancel() {
//            loginFail("Weibo onCancel ");
//        }
//    }
//
//    private RequestListener weiboRequestListener = new RequestListener() {
//        @Override
//        public void onComplete(String response) {
//            if (!TextUtils.isEmpty(response)) {
//                User user = User.parse(response);
//
//                if (user != null) {
//                    setRequestSocialLoginRegisterWithAccessToken(WeiboLoginController.oauth2AccessToken, user);
//                } else {
//                    loginFail(response);
//                }
//            }
//        }
//
//        @Override
//        public void onWeiboException(WeiboException e) {
//
//        }
//    };
//
//    private void setRequestSocialLoginRegisterWithAccessToken(Oauth2AccessToken accessToken, User user) {
//
//        RequestSocialLogin socialRequest = new RequestSocialLogin(
//                AdminService.DEVICE_TYPE_ANDROID,
//                FileUtil.getDeviceId(context),
//                AdminService.SOCIAL_FACEBOOK,
//                user.name,
//                accessToken.getUid(),
//                accessToken.getUid() + "@weibo.com",
//                user.profile_image_url,
//                accessToken.getToken());
//        Log.w(TAG, "getName: " + user.name);
//        Log.w(TAG, "getId: " + accessToken.getUid());
//        Log.w(TAG, "getEmail: " + accessToken.getUid() + "@weibo.com");
//        Log.w(TAG, "profile_image_url: " + user.profile_image_url);
//        Log.w(TAG, "getToken: " + accessToken.getToken());
//        socialRequest.callLoginSocialApi(context);
//
//
//    }

    public WeiboLoginController(Context context){
        super(context);
    }
}



