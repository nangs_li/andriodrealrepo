package co.real.productionreal2.service.model.response;

import android.content.Context;

import com.google.gson.annotations.SerializedName;
import co.real.productionreal2.model.SystemSetting;
import co.real.productionreal2.service.model.AgentProfiles;

public class ResponseLoginSocial extends ResponseBase {


    @SerializedName("Content")
    public Content content;

    @Override
    public String toString() {
        return "ResponseLoginSocial{" +
                "content=" + content.toString() +
                '}';
    }

    public class Content {
        @SerializedName("MemberID")
        public int memId;

        @SerializedName("MemberName")
        public String memName;

        @SerializedName("AccessToken")
        public String session;

        @SerializedName("QuickBloxLogin")
        public String qbLogin;

        @SerializedName("QuickBloxPassword")
        public String qbPassword;

        @SerializedName("QBID")
        public String qbid;

        @SerializedName("QBPwd")
        public String qbPwd;

        @SerializedName("PhotoURL")
        public String photoUrl;

        @SerializedName("MemberType")
        public int memeberType;

        @SerializedName("followerCount")
        public int followerCount;

        @SerializedName("FollowingCount")
        public int followingCount;

        @SerializedName("CountryCode")
        public String countryCode;

        @SerializedName("PhoneNumber")
        public String phoneNumber;

        // null == 17, yes -- 33
        @SerializedName("AgentProfile")
        public AgentProfiles agentProfile;


        @SerializedName("SystemSettings")
        public SystemSetting systemSetting;

        @Override
        public String toString() {
            return "Content{" +
                    "memId=" + memId +
                    ", memName='" + memName + '\'' +
                    ", session='" + session + '\'' +
                    ", qbLogin='" + qbLogin + '\'' +
                    ", qbPassword='" + qbPassword + '\'' +
                    ", qbid='" + qbid + '\'' +
                    ", countryCode='" + countryCode + '\'' +
                    ", phoneNumber='" + phoneNumber + '\'' +
                    ", photoUrl='" + photoUrl + '\'' +
                    ", memeberType=" + memeberType +
                    ", followerCount=" + followerCount +
                    ", followingCount=" + followingCount +
                    ", agentProfile=" + agentProfile +
                    ", systemSetting=" + systemSetting +
                    '}';
        }
    }

    public static void updateQBIdToServer(final Context context, ResponseLoginSocial.Content content) {
//        RequestQBIDUpdate qbidUpdate = new RequestQBIDUpdate(content.memId, content.session);
//
//        qbidUpdate.QBID = DatabaseManager.getInstance(context).getLogInfo().getQbUserId() + "";
//        Gson gson = new Gson();
//        String json = gson.toJson(qbidUpdate);
//        final AdminService weatherService = new AdminService(context);
//
//        weatherService.getCoffeeService().updateQBID(
//                new InputRequest(json),
//                new retrofit.Callback<ApiResponse>() {
//                    @Override
//                    public void failure(RetrofitError error) {
//                        Log.e("retrofit error", error.getMessage());
//                    }
//
//                    @Override
//                    public void success(ApiResponse arg0, Response arg1) {
//                        Log.e("retrofit success", " success");
//                    }
//                });
    }


}
