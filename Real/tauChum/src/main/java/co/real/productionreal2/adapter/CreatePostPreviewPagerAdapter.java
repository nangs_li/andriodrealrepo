package co.real.productionreal2.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import co.real.productionreal2.fragment.PreviewPagerItemFragment;

import java.util.ArrayList;

/**
 * Created by hohojo on 23/10/2015.
 */
public class CreatePostPreviewPagerAdapter extends FragmentStatePagerAdapter {
    private ArrayList<String> langNameList;
    private ArrayList<Integer> langIndexList;
    private Context context;
//    private int numOfPage = 1;

    public CreatePostPreviewPagerAdapter(Context context,FragmentManager fm) {
        super(fm);
        this.context=context;
    }

    @Override
    public Fragment getItem(int position) {
        return PreviewPagerItemFragment.newInstance(langIndexList.get(position));
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        return langNameList.size();
    }

//    public void setNumOfPage(int numOfPage) {
//        this.numOfPage = numOfPage;
//    }
//
//    public int getNumOfPage() {
//        return numOfPage;
//    }

    public void setLangNameList(ArrayList<String> langNameList) {
        this.langNameList = langNameList;
    }

    public ArrayList<Integer> getLangIndexList() {
        return langIndexList;
    }

    public void setLangIndexList(ArrayList<Integer> langIndexList) {
        this.langIndexList = langIndexList;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return langNameList.get(position);
    }

    public void removeView(int index) {
        langNameList.remove(index);
        langIndexList.remove(index);
        notifyDataSetChanged();
    }

}
