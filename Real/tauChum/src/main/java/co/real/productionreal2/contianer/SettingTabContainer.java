package co.real.productionreal2.contianer;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import co.real.productionreal2.R;
import co.real.productionreal2.BaseContainerFragment;
import co.real.productionreal2.activity.MainActivityTabBase;
import co.real.productionreal2.fragment.SettingFragment;

import java.util.List;

public class SettingTabContainer extends BaseContainerFragment {

    boolean needInit = true;

    public void fragmentBecameVisible() {
        if(needInit){
            initView();

            needInit = false;
        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.container_fragment, null);
        rootView.setPadding(0, MainActivityTabBase.tabHeight, 0, 0);

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void initView() {
        replaceFragment(new SettingFragment(), false);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        List<Fragment> fragments = getChildFragmentManager().getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                fragment.onActivityResult(requestCode, resultCode, data);
            }
        }
    }


}