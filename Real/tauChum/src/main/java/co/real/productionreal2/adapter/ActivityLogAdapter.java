package co.real.productionreal2.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;

import co.real.productionreal2.CoreData;
import co.real.productionreal2.R;
import co.real.productionreal2.service.model.ActivityLog;
import co.real.productionreal2.service.model.ActivityPlace;
import co.real.productionreal2.util.AppUtil;
import co.real.productionreal2.util.TimeUtils;
import co.real.productionreal2.view.NonScrollListView;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by kelvinsun on 15/1/16.
 */
public class ActivityLogAdapter extends BaseExpandableListAdapter {

    private Context context;
    private LayoutInflater parent_Inflater = null;
    private LayoutInflater child_Inflater = null;

    private List<ActivityLog> logArrayList = new ArrayList<ActivityLog>();//child

    protected HeaderListExpandableAdapterListener mListener;

    public static interface HeaderListExpandableAdapterListener {
        public void selectionItem(int position);
    }

    public ActivityLogAdapter(Context context, List<ActivityLog> logArrayList, HeaderListExpandableAdapterListener listener) {
        this.context = context;
        parent_Inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        child_Inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertDateFormat(logArrayList);
        this.mListener = listener;
    }

    private void convertDateFormat(List<ActivityLog> logArrayList) {
        int i = 0;
        for (ActivityLog activityLog : logArrayList) {
//            activityLog.createDate=TimeUtils.toPrettyTime(activityLog.createDate);
            activityLog.dateOfJoinString = TimeUtils.toPrettyTime(context, activityLog.dateOfJoin);
            activityLog.dateOfFollowString = TimeUtils.toPrettyTime(context, activityLog.dateOfFollow);
            i++;
        }
        this.logArrayList = logArrayList;
    }


    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        childViewHolder child_ViewHolder = null;

        if (convertView == null) {

            convertView = child_Inflater.inflate(R.layout.list_item_activty_log_child, null);
            child_ViewHolder = new childViewHolder();
            child_ViewHolder.tvJoinRealSince = (TextView) convertView.findViewById(R.id.tv_log_join_real);
            child_ViewHolder.tvBasedIn = (TextView) convertView.findViewById(R.id.tv_log_base_in);
            child_ViewHolder.tvFollowing = (TextView) convertView.findViewById(R.id.tv_log_following);
            child_ViewHolder.tvActiveChat = (TextView) convertView.findViewById(R.id.tv_log_active_chat);
            child_ViewHolder.tvFollowedYou = (TextView) convertView.findViewById(R.id.tv_log_followed_you);
            child_ViewHolder.pieChart = (PieChart) convertView.findViewById(R.id.pie_chart);
            child_ViewHolder.chartTitle = (TextView) convertView.findViewById(R.id.tv_chart_title);
            child_ViewHolder.lvChartInfo = (NonScrollListView) convertView.findViewById(R.id.lv_chart_info);

            convertView.setTag(child_ViewHolder);
        } else {
            child_ViewHolder = (childViewHolder) convertView.getTag();
//            return convertView;
        }

        final ActivityLog activityLog = (ActivityLog) getGroup(groupPosition);

        child_ViewHolder.tvJoinRealSince.setText(context.getString(R.string.me__join_real) + " ");
        Spannable joinRealSinceStr = new SpannableString(activityLog.dateOfJoinString);
        joinRealSinceStr.setSpan(new RelativeSizeSpan(0.8f), 0, joinRealSinceStr.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE); // set size
        joinRealSinceStr.setSpan(new ForegroundColorSpan(Color.DKGRAY), 0, joinRealSinceStr.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE); //set color
        child_ViewHolder.tvJoinRealSince.append(joinRealSinceStr);

        child_ViewHolder.tvBasedIn.setText(context.getString(R.string.me__based_in) + " ");
        Spannable basedInStr = new SpannableString(activityLog.baseIn);
        basedInStr.setSpan(new RelativeSizeSpan(0.8f), 0, basedInStr.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE); // set size
        basedInStr.setSpan(new ForegroundColorSpan(Color.DKGRAY), 0, basedInStr.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        child_ViewHolder.tvBasedIn.append(basedInStr);

        child_ViewHolder.tvFollowing.setText(context.getString(R.string.me__following) + " ");
//        Spannable followingStr = new SpannableString(context.getResources().getQuantityString(R.plurals.numberOfAgents, activityLog.followingCount, activityLog.followingCount));
       //plural and singular distinguish
        int followingCount = activityLog.followingCount;
        int strId = (followingCount > 1 ? R.string.common__agents : R.string.common__agents);
        Spannable followingStr=  new SpannableString(followingCount+" "+context.getResources().getString(strId));
        followingStr.setSpan(new RelativeSizeSpan(0.8f), 0, followingStr.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE); // set size
        followingStr.setSpan(new ForegroundColorSpan(Color.DKGRAY), 0, followingStr.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        child_ViewHolder.tvFollowing.append(followingStr);

        child_ViewHolder.tvActiveChat.setText(context.getString(R.string.me__active_chat) + " ");
        Spannable activeChatStr = new SpannableString(String.valueOf(activityLog.activeChatCount));
        activeChatStr.setSpan(new RelativeSizeSpan(0.8f), 0, activeChatStr.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE); // set size
        activeChatStr.setSpan(new ForegroundColorSpan(Color.DKGRAY), 0, activeChatStr.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        child_ViewHolder.tvActiveChat.append(activeChatStr);

        child_ViewHolder.tvFollowedYou.setText(context.getString(R.string.me__followed_you) + " ");
        Spannable followedYouStr = new SpannableString(activityLog.dateOfFollowString);
        followedYouStr.setSpan(new RelativeSizeSpan(0.8f), 0, followedYouStr.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE); // set size
        followedYouStr.setSpan(new ForegroundColorSpan(Color.DKGRAY), 0, followedYouStr.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        child_ViewHolder.tvFollowedYou.append(followedYouStr);

        Log.d("ActivityLogAdapter", "getChildView: " + activityLog.logType + " " + activityLog.dateOfFollowString);
        Log.d("ActivityLogAdapter", "getChildView: " + groupPosition + " " + activityLog.activityPlaceList.size());
        String location = "null";
        Float percentage = (float) 0;
        //set up pie chart
        if (activityLog.activityPlaceList.size() > 0) {
            location = activityLog.activityPlaceList.get(0).location;
            percentage = activityLog.activityPlaceList.get(0).percentage;
        }

        child_ViewHolder.pieChart.setDescription("");

        child_ViewHolder.pieChart.setCenterTextColor(getColor(R.color.holo_blue));
        child_ViewHolder.pieChart.setCenterText(String.valueOf(percentage) + "%");
        child_ViewHolder.pieChart.setCenterTextTypeface(Typeface.DEFAULT_BOLD);

        child_ViewHolder.pieChart.setDrawSliceText(false);
        child_ViewHolder.pieChart.setUsePercentValues(false);
        child_ViewHolder.pieChart.invalidate();

        PieData data = new PieData();
        PieDataSet set;
        set = createSet(percentage);
        data.addDataSet(set);
        child_ViewHolder.pieChart.setData(data);

        //set up chart info list
        ChartInfoAdapter chartInfoAdapter = new ChartInfoAdapter(context, activityLog.activityPlaceList);
        child_ViewHolder.lvChartInfo.setAdapter(chartInfoAdapter);

        child_ViewHolder.pieChart.animateY(800, Easing.EasingOption.EaseOutCirc);

        child_ViewHolder.chartTitle.setText(location);

        return convertView;
    }

    //get parent
    @Override
    public Object getGroup(int groupPosition) {
        // TODO Auto-generated method stub
        return logArrayList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return logArrayList.get(groupPosition);
    }

    //get parent count
    @Override
    public int getGroupCount() {
        // TODO Auto-generated method stub
        return logArrayList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    //get parent position
    @Override
    public long getGroupId(int groupPosition) {
        // TODO Auto-generated method stub
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    //get parent view
    @Override
    public View getGroupView(final int groupPosition, final boolean isExpanded,
                             View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        parentViewHolder parent_ViewHolder = null;
        if (convertView == null) {
            convertView = parent_Inflater.inflate(R.layout.list_item_activty_log_parent, null);
            parent_ViewHolder = new parentViewHolder();

            parent_ViewHolder.ivLogIcon = (ImageView) convertView.findViewById(R.id.iv_activity_log_img);
            parent_ViewHolder.tvLogDesc = (TextView) convertView.findViewById(R.id.tv_activity_log_desc);
            parent_ViewHolder.ivLogArrow = (ImageView) convertView.findViewById(R.id.btn_log_expand);

            convertView.setTag(parent_ViewHolder);
        } else {
//            return convertView;
            parent_ViewHolder = (parentViewHolder) convertView.getTag();
        }

        final ActivityLog activityLog = (ActivityLog) getGroup(groupPosition);

        //fill text
        CharSequence descStr;
        if (activityLog.logType == 0 || activityLog.logType == 1) {
            String location = "null";
            if (activityLog.activityPlaceList.size() > 0) {
                location = activityLog.activityPlaceList.get(0).location;
            }
            descStr = Html.fromHtml(getFormattedDesc(location, activityLog.logType, TimeUtils.toPrettyTime(context, activityLog.createDate)));
        } else {
            descStr = Html.fromHtml(getFormattedDesc("", activityLog.logType, TimeUtils.toPrettyTime(context, activityLog.createDate)));
        }

        parent_ViewHolder.tvLogDesc.setText(descStr);

        int iconType;
        if (activityLog.logType == 0) {
            iconType = R.drawable.activity_log_icon_follow;
            parent_ViewHolder.ivLogArrow.setVisibility(View.VISIBLE);
        } else if (activityLog.logType == 1) {
            iconType = R.drawable.activity_log_icon_unfollow;
            parent_ViewHolder.ivLogArrow.setVisibility(View.VISIBLE);
        } else if (activityLog.logType == 2) {
            iconType = R.drawable.activity_log_icon_invitefollow;
            //disable expand icon, remarks to set invisible instead of gone to fulfill the UI
            parent_ViewHolder.ivLogArrow.setVisibility(View.INVISIBLE);
        } else {
            //default
            iconType = R.drawable.activity_log_icon_follow;
            parent_ViewHolder.ivLogArrow.setVisibility(View.VISIBLE);
        }

        Log.d("ActivityLogAdapter", "getGroupView: " + activityLog.logType + " " + activityLog.dateOfFollowString);
        //set icon image
        parent_ViewHolder.ivLogIcon.setImageDrawable(context.getResources().getDrawable(iconType));

        if (isExpanded) {
            parent_ViewHolder.ivLogArrow.setImageDrawable(context.getResources().getDrawable(R.drawable.ico_activity_listing_min));
        } else {
            parent_ViewHolder.ivLogArrow.setImageDrawable(context.getResources().getDrawable(R.drawable.ico_activity_listing_pull));
        }

        return convertView;
    }


    private String getFormattedDesc(String place, int logType, String createDate) {
        StringBuilder sb = new StringBuilder();
        if (logType == 0) {
            sb.append(String.format(context.getString(R.string.me__someone_following_you), AppUtil.getColoredString(place, getColor(R.color.holo_blue))));
        } else if (logType == 1) {
            sb.append(String.format(context.getString(R.string.me__someone_unfollowing_you), AppUtil.getColoredString(place, getColor(R.color.holo_blue))));
        } else if (logType == 2) {
            CoreData.inviteToFollowShownCount=0;
            sb.append(AppUtil.getColoredString(context.getString(R.string.me__someone_invited_following_you), getColor(R.color.black)));
        } else {
            //default
            sb.append(String.format(context.getString(R.string.me__someone_following_you), AppUtil.getColoredString(place, getColor(R.color.holo_blue))));
        }
        sb.append(" " + AppUtil.getColoredString("<small>" + createDate + "</small>", getColor(R.color.dark_grey)));
        Log.d("textHTML", "testHtml: " + sb.toString());
        return sb.toString();
    }

    private int getColor(int color) {
        return context.getResources().getColor(color);
    }

    //....
    @Override
    public boolean hasStableIds() {
        // TODO Auto-generated method stub
        return true;
    }

    //....
    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        // TODO Auto-generated method stub
        return true;
    }

    public final class parentViewHolder {
        private ImageView ivLogIcon;
        private TextView tvLogDesc;
        private ImageView ivLogArrow;
    }

    public final class childViewHolder {
        private TextView tvJoinRealSince;
        private TextView tvBasedIn;
        private TextView tvFollowing;
        private TextView tvActiveChat;
        private TextView tvFollowedYou;
        private PieChart pieChart;
        private TextView chartTitle;
        private NonScrollListView lvChartInfo;


    }

    private PieDataSet createSet(float quarterOne) {

        ArrayList<Entry> valsComp1 = new ArrayList<Entry>();
        Entry c1e1 = new Entry(quarterOne, 0); // 0 == quarter 1
        valsComp1.add(c1e1);
        Entry c1e2 = new Entry(100 - quarterOne, 1); // 1 == quarter 2 ...
        valsComp1.add(c1e2);

        PieDataSet set = new PieDataSet(valsComp1, "DataSet 1");
        set.setColor(R.color.red);
        set.setLabel("");
        set.setSliceSpace(5);
        set.setValueTextSize(0);

        //set color
        // sets colors for the dataset, resolution of the resource name to a "real" color is done internally
        set.setColors(new int[]{R.color.holo_blue, R.color.dark_grey}, context);

        return set;
    }

    private class ChartInfoAdapter extends BaseAdapter {

        private Context context;
        private List<ActivityPlace> activityPlaceList = new ArrayList<ActivityPlace>();//child

        public ChartInfoAdapter(Context context, List<ActivityPlace> activityPlaceList) {
            this.context = context;
            this.activityPlaceList = activityPlaceList;
        }

        @Override
        public int getCount() {
            return activityPlaceList.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            // Get the data item for this position
            final Object place = getItem(position);
            // Check if an existing view is being reused, otherwise inflate the view
            if (convertView == null) {
                convertView = LayoutInflater.from(context).inflate(R.layout.view_chart_info, parent, false);
            }
            // Lookup view for data population
            TextView tvPercentage = (TextView) convertView.findViewById(R.id.tv_percentage);
            TextView tvPlace = (TextView) convertView.findViewById(R.id.tv_place_name);
            // Populate the data into the template view using the data object
            tvPercentage.setTextColor(position == 0 ? getColor(R.color.holo_blue) : getColor(R.color.dark_grey));
            tvPlace.setTextColor(position == 0 ? getColor(R.color.holo_blue) : getColor(R.color.dark_grey));

            tvPercentage.setText(String.valueOf(activityPlaceList.get(position).percentage) + "%");
            tvPlace.setText(activityPlaceList.get(position).location);


            return convertView;
        }
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        convertDateFormat(logArrayList);
    }
}