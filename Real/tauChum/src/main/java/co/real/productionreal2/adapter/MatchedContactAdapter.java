package co.real.productionreal2.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import co.real.productionreal2.Constants;
import co.real.productionreal2.R;
import co.real.productionreal2.service.model.Follower;
import co.real.productionreal2.service.model.ReMatchContact;
import co.real.productionreal2.util.RoundedTransformation;
import co.real.productionreal2.view.FollowButton;
import widget.RoundedImageView;

/**
 * Created by kelvinsun on 19/2/16.
 */
public class MatchedContactAdapter extends BaseAdapter {

    public final static class FollowingView {
        public TextView tvName;
        public TextView tvRegion;
        public RoundedImageView ivImage;
        public FollowButton btnFollower;
        public View divider;

    }

    protected LayoutInflater mInflater;
    protected Context mContext;
    protected ArrayList<ReMatchContact> mMatchedContactList;
    protected MatchedContactAdapterListener mListener;

    private THEME theme= THEME.LIGHT;
    public enum THEME {DRAK,LIGHT};

    public static interface MatchedContactAdapterListener {
        public ArrayList<ReMatchContact> getFollowingList();

        public void onFollowItem(ReMatchContact matchContact);
    }

    public MatchedContactAdapter(Context context, ArrayList<ReMatchContact> matchedContactList, MatchedContactAdapterListener listener) {
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mMatchedContactList = matchedContactList;
        mListener = listener;

    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return mListener.getFollowingList().size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
//        return mProductList.get(position);
        return mListener.getFollowingList().get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view;
        view = newView(parent, position);
        bindView(view, position);
        return view;
    }

    protected View newView(final ViewGroup parent, final int position) {
        if (mInflater == null) {
            mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        View view;
        view = mInflater.inflate(R.layout.list_item_following, parent, false);
        cacheView(view, position);

        return view;
    }

    protected void cacheView(View view, int position) {
        final FollowingView views = new FollowingView();
        // find view to cache
        views.tvName = (TextView) view.findViewById(R.id.tv_following_name);
        views.tvRegion = (TextView) view.findViewById(R.id.tv_following_region);
        views.ivImage = (RoundedImageView) view.findViewById(R.id.iv_following_image);
        views.btnFollower = (FollowButton) view.findViewById(R.id.btn_to_follow);
        views.divider =(View) view.findViewById(R.id.line_horizontal);

        //THEME control
        if (theme== THEME.DRAK){
            views.tvName.setTextColor(mContext.getResources().getColor(R.color.black));
            views.tvRegion.setTextColor(mContext.getResources().getColor(R.color.black_translucent));
            views.divider.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.dash_line_horizontal));
        }else {
            views.tvName.setTextColor(mContext.getResources().getColor(R.color.white));
            views.tvRegion.setTextColor(mContext.getResources().getColor(R.color.white));
            views.divider.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.dash_line_horizontal_light));
        }

        views.tvRegion.setAlpha((float)0.8);


        final ReMatchContact mMatchContact = mListener.getFollowingList().get(position);

        views.tvName.setText(mMatchContact.name);
        views.tvRegion.setText(mMatchContact.location);
        Picasso.with(mContext).load(mMatchContact.photoURL)
                .placeholder(R.drawable.com_facebook_profile_picture_blank_square).transform(new RoundedTransformation()).into(views.ivImage);

        view.setTag(views);


        //update border color
        if (mMatchContact.isFollowing == 1) {
            views.ivImage.setImageBorder(Constants.STYLE_TYPE.AGENT_FOLLOWED);
        } else {
            views.ivImage.setImageBorder(Constants.STYLE_TYPE.AGENT_UNFOLLOWED);
        }


        views.btnFollower.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onFollowItem(mMatchContact);
                views.btnFollower.playAnimation();
                if (mMatchContact.isFollowing == 1) {
                    views.btnFollower.setStyle(Constants.STYLE_TYPE.AGENT_FOLLOWED);
                } else {
                    views.btnFollower.setStyle(Constants.STYLE_TYPE.AGENT_UNFOLLOWED);
                }
            }
        });
    }

    protected void bindView(View view, final int position) {
        final ReMatchContact matchContact = (ReMatchContact) getItem(position);

        final FollowingView cachedView = (FollowingView) view.getTag();
        if (matchContact.isFollowing == 1) {
            cachedView.btnFollower.setStyle(Constants.STYLE_TYPE.AGENT_FOLLOWED);
        } else {
            cachedView.btnFollower.setStyle(Constants.STYLE_TYPE.AGENT_UNFOLLOWED);
        }
//        cachedView.tvName.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mListener.onFollowItem(follower);
//            }
//        });
    }

}

