package co.real.productionreal2.activity.viral;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.mixpanel.android.mpmetrics.MixpanelAPI;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import co.real.productionreal2.Constants;
import co.real.productionreal2.CoreData;
import co.real.productionreal2.Navigation;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.BaseActivity;
import co.real.productionreal2.config.AppConfig;
import co.real.productionreal2.fragment.ViralSuccessFragment;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;
import co.real.productionreal2.util.AppUtil;
import co.real.productionreal2.util.ImageUtil;
import co.real.productionreal2.view.Dialog;
import io.branch.indexing.BranchUniversalObject;
import io.branch.referral.Branch;
import io.branch.referral.BranchError;
import io.branch.referral.util.LinkProperties;
import lib.blurbehind.OnBlurCompleteListener;

;

public class InviteToDownloadActivity extends BaseActivity {

    private static final int REQUEST_CODE_VIRAL=100;
    private static final String TAG = "InviteToFollowActivity";
    private ResponseLoginSocial.Content userContent;

    @InjectView(R.id.TextView_tnc)
    TextView TextView_tnc;
    @InjectView(R.id.tv_inviteto_download)
    TextView tvInviteToDownload;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_to_download);
        AppUtil.setBlurBehind(this);

        userContent = CoreData.getUserContent(this);

        ButterKnife.inject(this);
        initView();

    }

    public static void start(final Context context) {

                ImageUtil.getInstance().prepareBlurImageFromActivity((Activity) context, InviteToDownloadActivity.class, new OnBlurCompleteListener() {
                    @Override
                    public void onBlurComplete() {
                        Intent intent = new Intent(context, InviteToDownloadActivity.class);
                        Navigation.presentIntent((Activity) context,intent);
                    }
                });


    }

    private void initView() {

        tvInviteToDownload.setText(Html.fromHtml(getString(R.string.setting__connect_agent)));

        String htmlTNC = "<subtitle><yellow>"+getString(R.string.invite_to_download__desc_1)+"\n</yellow></subtitle>"+getString(R.string.invite_to_download__desc_2)+"\n\n<subtitle><yellow>"+getString(R.string.invite_to_download__desc_3)+
                "\n</yellow></subtitle>"+getString(R.string.invite_to_download__desc_4);
        TextView_tnc.setText(Html.fromHtml(htmlTNC.replaceAll("\n", "<br>")
                        .replaceAll("<subtitle>", "<font size=\"12\"><b>")
                        .replaceAll("</subtitle>", "</b></font>")
                        .replaceAll("<yellow>", "<font color=#" + Integer.toHexString(ContextCompat.getColor(this, R.color.invite_yellow) & 0x00ffffff) + ">")
                        .replaceAll("</yellow>", "</font>")
        ));

    }

    private void genShortUrl() {
        BranchUniversalObject branchUniversalObject = genBranchUniObj();
        LinkProperties linkProps = genLinkProps();
        branchUniversalObject.generateShortUrl(this, linkProps, new Branch.BranchLinkCreateListener() {
            @Override
            public void onLinkCreate(String url, BranchError error) {
                if (error != null) {
                    Dialog.normalDialog(InviteToDownloadActivity.this, "Branch.io gen shortUrl error: " + error);
                } else {
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_SEND);
                    intent.setType("text/plain");
                    intent.putExtra(Intent.EXTRA_SUBJECT,getString(R.string.invite_to_download__subject));
                    intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.invite_to_download__content)+" "+ url);
                    startActivityForResult(Intent.createChooser(intent, getString(R.string.invite_to_download__title)),REQUEST_CODE_VIRAL);
                }
            }
        });
    }

    private BranchUniversalObject genBranchUniObj() {
        BranchUniversalObject branchUniObj = new BranchUniversalObject()
                .setTitle("Real")
                .setContentDescription("Someone invite you to Invite in Real")
                .setContentIndexingMode(BranchUniversalObject.CONTENT_INDEX_MODE.PUBLIC)
                .addContentMetadata(Constants.BRANCH_MEM_ID, "" + userContent.memId)
                .addContentMetadata(Constants.BRANCH_MEM_NAME, userContent.memName)
                .addContentMetadata(Constants.BRANCH_ACTION, Constants.BRANCH_ACTION_NONE);

        return branchUniObj;
    }

    private LinkProperties genLinkProps() {
        LinkProperties linkProperties = new LinkProperties()
                .setChannel(Constants.BRANCH_CHANNEL_DOWNLOAD)
                .setFeature("invite");
        return linkProperties;
    }

    @Override
    public void onBackPressed() {
        Log.d(TAG,"onActivityResult: "+"onBackPressed");
        finish();
    }

    @OnClick(R.id.closeBtn)
    public void closeBtnClick(View view) {
        onBackPressed();
    }

    @OnClick(R.id.inviteToDownloadBtn)
    public void inviteToDownloadBtnClick(View view) {

        // Mixpanel
        MixpanelAPI mixpanel = MixpanelAPI.getInstance(this, AppConfig.getMixpanelToken());
        mixpanel.track("Invite to Download");

        genShortUrl();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String packageName="";
        if (data!=null) {
            packageName = data.getComponent().getPackageName();
        }
        Log.d(TAG,"onActivityResult: "+resultCode+ " "+requestCode+ " "+packageName);
        if (resultCode==RESULT_OK && requestCode==REQUEST_CODE_VIRAL){
            showSuccessDialog();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG,"onActivityResult: "+"onResume");
    }

    void showSuccessDialog() {
        int mStackLevel=0;
        mStackLevel++;

        // DialogFragment.show() will take care of adding the fragment
        // in a transaction.  We also want to remove any currently showing
        // dialog, so make our own transaction and take care of that here.
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        DialogFragment newFragment = ViralSuccessFragment.newInstance(mStackLevel);
        newFragment.show(ft, "dialog");
    }
}
