package co.real.productionreal2.service.model.request;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.model.InputRequest;
import co.real.productionreal2.service.OperationService;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.response.ResponseNewsFeed;
import co.real.productionreal2.view.Dialog;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by kelvinsun on 23/2/16.
 */
public class RequestWriteViralLog extends RequestBase{
    public RequestWriteViralLog(int memId,
                              String session,
                              int vType,
                              int inviteBy,
                                int inviteTo) {
        super(memId, session);
        this.vType = vType;
        this.inviteBy = inviteBy;
        this.inviteTo = inviteTo;
    }

    @SerializedName("vType")
    public int vType;

    @SerializedName("InviteBy")
    public int inviteBy;

    @SerializedName("InviteTo")
    public int inviteTo;

    public void callWriteViralLogApi(final Context context,  final ApiCallback callback) {
        String json = new Gson().toJson(this);
        final OperationService weatherService = new OperationService(context);
        weatherService.getCoffeeService().viralLogWrite(new InputRequest(json), new Callback<ApiResponse>() {
            @Override
            public void success(ApiResponse apiResponse, Response response) {
                ResponseNewsFeed responseGson = new Gson().fromJson(apiResponse.jsonContent, ResponseNewsFeed.class);
                if (responseGson.errorCode == 4) {
                    Dialog.accessErrorForceLogoutDialog(context).show();
                } else if (responseGson.errorCode == 26) {
                    Dialog.suspendedErrorForceLogoutDialog(context).show();
                } else if (responseGson.errorCode != 0 && responseGson.errorCode != 20 && responseGson.errorCode != 21 &&
                        responseGson.errorCode != 22 && responseGson.errorCode != 23) {
                    callback.failure(responseGson.errorMsg);
                } else {
                    callback.success(apiResponse);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                callback.failure(error.getMessage());
            }
        });
    }
}
