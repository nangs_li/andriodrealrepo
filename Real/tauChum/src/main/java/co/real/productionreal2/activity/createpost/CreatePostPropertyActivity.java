package co.real.productionreal2.activity.createpost;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mixpanel.android.mpmetrics.MixpanelAPI;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import co.real.productionreal2.Constants;
import co.real.productionreal2.CoreData;
import co.real.productionreal2.Navigation;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.BaseActivity;
import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.callback.DialogDualCallback;
import co.real.productionreal2.common.LocalStorageHelper;
import co.real.productionreal2.config.AppConfig;
import co.real.productionreal2.model.GoogleAddress;
import co.real.productionreal2.model.SystemSetting;
import co.real.productionreal2.service.GoogleService;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.ApiResponseGeocode;
import co.real.productionreal2.service.model.ApiResponseLangDetect;
import co.real.productionreal2.service.model.request.RequestGoogleMapGeocodeByAddress;
import co.real.productionreal2.service.model.request.RequestGoogleMapPlaceGeocodeByPlaceId;
import co.real.productionreal2.service.model.request.RequestListing;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;
import co.real.productionreal2.util.AppUtil;
import co.real.productionreal2.util.FileUtil;
import co.real.productionreal2.util.GoogleGeoUtil;
import co.real.productionreal2.util.LocationUtil;
import co.real.productionreal2.util.NetworkUtil;
import co.real.productionreal2.view.Dialog;
import co.real.productionreal2.view.SpaceTypeItem;
import retrofit.RetrofitError;
import retrofit.client.Response;

;

/**
 * Created by hohojo on 13/10/2015.
 */
public class CreatePostPropertyActivity extends BaseActivity {
    private static final String TAG = "CreatePostProperty";
    @InjectView(R.id.rootImageView)
    ImageView rootImageView;
    @InjectView(R.id.propertyLinearLayout)
    LinearLayout propertyLinearLayout;
    @InjectView(R.id.backBtn)
    ImageButton backBtn;
    @InjectView(R.id.addressEditText)
    EditText addressEditText;
    @InjectView(R.id.apartmentEditText)
    EditText apartmentEditText;
    @InjectView(R.id.googleReturnLinearLayout)
    LinearLayout googleReturnLinearLayout;
    @InjectView(R.id.googleAddressTextView)
    TextView googleAddressTextView;
    @InjectView(R.id.horizontalScrollView)
    protected HorizontalScrollView horizontalScrollView;
    @InjectView(R.id.nextBtn)
    Button btnNext;


    private ResponseLoginSocial.Content userContent;
    private boolean IS_SUPPORT_GOOGLE_SERVICE;

    private int langIndex;

    private String spaceTypeStr;
    private int spaceTypeIndex, propertyTypeIndex;
    private List<GoogleAddress> googleAddressList;

    RequestListing createPostRequest;
    String detectedLang;

    public static final int FINISH_POST = 1001;

    //supporting language list for translation
    String[] SUPPORT_LANG = {"ar", "bg", "bn", "ca", "cs", "da", "de", "el", "en", "en-AU", "en-GB", "es", "eu", "fa", "fi", "fil", "fr", "gl", "gu", "hi", "hr", "hu", "id", "it", "iw",
            "ja", "kn", "ko", "lt", "lv", "ml", "mr", "nl", "no", "pl", "pt", "pt-BR", "pt-PT", "ro", "ru", "sk", "sl", "sr", "sv", "ta", "te", "th", "tl", "tr", "uk", "vi", "xh-CN", "zh-TW"};

    public List<GoogleAddress> getGoogleAddressList() {
        return googleAddressList;
    }

    public void setGoogleAddressList(List<GoogleAddress> googleAddressList) {
        this.googleAddressList = googleAddressList;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_post_property);
        langIndex = getIntent().getIntExtra(Constants.EXTRA_LANG_INDEX, 0);
        ButterKnife.inject(this);
        initData();
        initView();
        new Handler().postDelayed(
                new Runnable() {
                    @Override
                    public void run() {
                        // Code here will run in UI thread
                        horizontalScrollView.scrollTo(horizontalScrollView.getRight(), 0);

                        new Handler().postDelayed(
                                new Runnable() {
                                    @Override
                                    public void run() {
                                        horizontalScrollView.smoothScrollTo(0, 0);
                                    }
                                }, 500);
                    }
                }, 100);

    }

    private void initData() {
        userContent = CoreData.getUserContent(this);
        IS_SUPPORT_GOOGLE_SERVICE = LocationUtil.isSupportedGoogleMap(this);

        //to check if it is checked in local storage
        spaceTypeIndex = BaseCreatePost.getInstance(CreatePostPropertyActivity.this).getCreatePostRequest().spaceType;
        propertyTypeIndex = BaseCreatePost.getInstance(CreatePostPropertyActivity.this).getCreatePostRequest().propertyType;
    }

    private void setPropertyTypeAndSpaceType(int selectItem) {
        if (selectItem < 10) {
            BaseCreatePost.getInstance(this).getCreatePostRequest().spaceType = 0;
            BaseCreatePost.getInstance(this).getCreatePostRequest().propertyType = selectItem;
        } else {
            BaseCreatePost.getInstance(this).getCreatePostRequest().spaceType = 1;
            BaseCreatePost.getInstance(this).getCreatePostRequest().propertyType = selectItem - 10;
        }
    }

    private void initView() {
        String addressStr = BaseCreatePost.getInstance(CreatePostPropertyActivity.this).getAddressInputStr();
        if (addressStr != null) {
            //do search if there is local storage
            addressEditText.setText(addressStr);
            callGeoCodeApi(addressEditText.getText().toString(), true);
        } else {
            btnNext.setEnabled(false);
        }
        addressEditText.setOnFocusChangeListener(addressEditTextFocusChangeListener);

        addressEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    btnNext.setEnabled(false);
                    googleReturnLinearLayout.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        apartmentEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    AppUtil.hideKeyboard(CreatePostPropertyActivity.this, apartmentEditText);
                }

            }
        });

        ///add space type in horizontal scroll layout
        final List<SystemSetting.SpaceType> spaceTypeList = FileUtil.getAllSpaceType(this, AppUtil.getCurrentLangIndex(this));

        propertyLinearLayout.removeAllViews();

        int i = 0;
        for (SystemSetting.SpaceType spaceType : spaceTypeList) {
            final SpaceTypeItem spaceTypeItem = new SpaceTypeItem(this, spaceType);
            spaceTypeItem.setTag(spaceType.propertyTypeIndex + "," + spaceType.index);
            spaceTypeItem.updateUI(spaceType);

            boolean isSelected = false;

            //default select the first item if no previous saving
            if (spaceTypeIndex == -1 && propertyTypeIndex == -1) {
                if (i == 0) {
                    isSelected = true;
                    spaceTypeStr = (String) spaceTypeItem.getTag();
                    String[] split = spaceTypeStr.split(",");
                    BaseCreatePost.getInstance(CreatePostPropertyActivity.this).getCreatePostRequest().spaceType = Integer.parseInt(split[1]);
                    BaseCreatePost.getInstance(CreatePostPropertyActivity.this).getCreatePostRequest().propertyType = Integer.parseInt(split[0]);
                }
                //to read the index format
            } else if (spaceType.propertyTypeIndex == propertyTypeIndex && spaceType.index == spaceTypeIndex) {
                isSelected = true;
            }
            spaceTypeItem.setSelected(isSelected);
            spaceTypeItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //single selection
                    resetSpaceTypeSelection();
                    v.setSelected(!v.isSelected());
                    if (v.isSelected()) {
                        spaceTypeStr = (String) v.getTag();
                        String[] split = spaceTypeStr.split(",");
                        BaseCreatePost.getInstance(CreatePostPropertyActivity.this).getCreatePostRequest().spaceType = Integer.parseInt(split[1]);
                        BaseCreatePost.getInstance(CreatePostPropertyActivity.this).getCreatePostRequest().propertyType = Integer.parseInt(split[0]);
                    } else {
                        spaceTypeStr = (String) v.getTag();
                    }
                }
            });
            propertyLinearLayout.addView(spaceTypeItem);
            i++;
        }


    }

    private void resetSpaceTypeSelection() {
        for (int i = 0; i < propertyLinearLayout.getChildCount(); i++) {
            propertyLinearLayout.getChildAt(i).setSelected(false);
        }
    }

    View.OnFocusChangeListener addressEditTextFocusChangeListener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (!hasFocus) {
                AppUtil.hideKeyboard(CreatePostPropertyActivity.this, addressEditText);
                if (!addressEditText.getText().toString().isEmpty()) {
                    //detect input language
                    String longestStr = AppUtil.longestWord(addressEditText.getText().toString());
                    Log.d(TAG, "LongestStr: " + longestStr);
                    detectLanguageInput(longestStr);
                }
            }

        }
    };

    private void callGeoCodeApi(String search, final boolean isAutoSearch) {
        if (IS_SUPPORT_GOOGLE_SERVICE) {
            final GoogleService weatherService =
                    new GoogleService(this);
            weatherService.getGeocodeService().geocodeRequest(
                    search, true, detectedLang,
                    new retrofit.Callback<ApiResponseGeocode>() {
                        @Override
                        public void failure(RetrofitError error) {
                            Log.e("result", error.toString());
                        }

                        @Override
                        public void success(final ApiResponseGeocode apiResponse, Response response) {
                            processGeocodeByAddressResult(apiResponse, isAutoSearch);
                        }
                    }
            );
        } else {
            RequestGoogleMapGeocodeByAddress requestGoogleMapGeocodeByAddress = new RequestGoogleMapGeocodeByAddress(userContent.memId, LocalStorageHelper.getAccessToken(getBaseContext()), detectedLang, search);
            requestGoogleMapGeocodeByAddress.callGoogleMapPlaceAPIGeocodeBySearchAddress(this, new ApiCallback() {
                @Override
                public void success(ApiResponse apiResponse) {
                    try {
                        JSONObject jsonObject = new JSONObject(apiResponse.jsonContent.toString());
                        ApiResponseGeocode responseGson = new Gson().fromJson(jsonObject.optString("GoogleResult"), ApiResponseGeocode.class);
                        processGeocodeByAddressResult(responseGson, isAutoSearch);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void failure(String errorMsg) {
                    Log.e("result", errorMsg.toString());
                }
            });
        }
    }

    private void processGeocodeByAddressResult(final ApiResponseGeocode apiResponse, boolean isAutoSearch) {
        if (apiResponse.status.equals("OK") && apiResponse.googleAddress.size() > 0) {
            if (isAutoSearch) {
                if (BaseCreatePost.getInstance(CreatePostPropertyActivity.this).getCreatePostRequest().googleAddressList.size() > 0) {
                    GoogleAddress address = BaseCreatePost.getInstance(CreatePostPropertyActivity.this).getCreatePostRequest().googleAddressList.get(0).address;
                    //call place api
                    //prepare the well-format google address
                    callPlaceApi(address);

                    googleReturnLinearLayout.setVisibility(View.VISIBLE);
                    googleAddressTextView.setText(address.formattedAddress);

                    btnNext.setEnabled(true);
                } else {
                    showNoResultDialog();
                }
            } else {
                launchSelectCountryActivity(apiResponse.googleAddress);
            }
            //save address input
            RequestListing.AddressUserInput addressUserInput = new RequestListing.AddressUserInput();
            BaseCreatePost.getInstance(CreatePostPropertyActivity.this).getCreatePostRequest().addressInputs.add(addressUserInput);
            BaseCreatePost.getInstance(CreatePostPropertyActivity.this).setAddressInputStr(addressEditText.getText().toString());
        } else {
            showNoResultDialog();
        }
    }

    private void showNoResultDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!isFinishing()) {
                    Dialog.noReturnAddressDialog(CreatePostPropertyActivity.this).show();
                }
            }
        });
    }



    private void callPlaceApi(GoogleAddress googleAddress) {
        String countryShortName = GoogleGeoUtil.getTypeFromGoogleAddress(googleAddress);
        ArrayList<String> countryLangList = GoogleGeoUtil.getCountryLangList(CreatePostPropertyActivity.this, countryShortName);

        Log.d(TAG, "countryLangList: " + AppUtil.removeDuplicate(countryLangList).toString());
        if (detectedLang != null) {
            Log.d(TAG, "getLanguageDetection: " + detectedLang);
            //combine TC and SC to one language
            if (detectedLang.equals("zh-CN"))
                detectedLang = "zh-TW";
            countryLangList.add(0, detectedLang);
        }

        ArrayList<String> finalCountryLangList = AppUtil.removeDuplicate(countryLangList);
        Log.d(TAG, "countryLangList: " + AppUtil.removeDuplicate(countryLangList).toString());

        BaseCreatePost.getInstance(CreatePostPropertyActivity.this).getCreatePostRequest().googleAddressList.clear();
        if (finalCountryLangList != null)
            for (final String countryLang : finalCountryLangList) {
                //check if support google play services
                if (IS_SUPPORT_GOOGLE_SERVICE) {
                    final GoogleService weatherService =
                            new GoogleService(this);
                    weatherService.getGeocodeService().geocodeWithLangRequest(
                            googleAddress.placeId, GoogleService.API_KEY, countryLang,
                            new retrofit.Callback<ApiResponseGeocode>() {
                                @Override
                                public void failure(RetrofitError error) {
                                    Log.e("result", error.toString());
                                }

                                @Override
                                public void success(final ApiResponseGeocode apiResponse, Response response) {
                                    processGoogleMapPlacecodeByPlaceIdResult(apiResponse, countryLang);
                                }
                            }
                    );
                } else {
                    //TODO: handle china request
                    RequestGoogleMapPlaceGeocodeByPlaceId requestGoogleMapPlaceGeocodeByPlaceId = new RequestGoogleMapPlaceGeocodeByPlaceId(userContent.memId,
                            LocalStorageHelper.getAccessToken(getBaseContext()), AppUtil.getCurrentLangCode(this), googleAddress.placeId);
                    requestGoogleMapPlaceGeocodeByPlaceId.callGoogleMapPlaceAPIGeocodeByPlaceId(this, new ApiCallback() {
                        @Override
                        public void success(ApiResponse apiResponse) {
                            try {
                                JSONObject jsonObject = new JSONObject(apiResponse.jsonContent.toString());
                                ApiResponseGeocode responseGson = new Gson().fromJson(jsonObject.optString("GoogleResult"), ApiResponseGeocode.class);
                                processGoogleMapPlacecodeByPlaceIdResult(responseGson, countryLang);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void failure(String errorMsg) {
                            Log.e(TAG, errorMsg.toString());
                        }
                    });
                }
            }
        else if (Constants.isDevelopment)
            Toast.makeText(this, "This is not valid place", Toast.LENGTH_LONG).show();
    }

    private void processGoogleMapPlacecodeByPlaceIdResult(ApiResponseGeocode apiResponse, String countryLang) {
        Log.w(TAG, "apiResponse = " + apiResponse);
        createPostRequest = BaseCreatePost.getInstance(CreatePostPropertyActivity.this).getCreatePostRequest();
        RequestListing.GoogleAddressPack googleAddressPack =
                createPostRequest.new GoogleAddressPack();
        if (apiResponse.status.equals("OK") && apiResponse.googleAddress.size() > 0) {
            googleAddressPack.lang = countryLang;
            googleAddressPack.address = apiResponse.googleAddress.get(0);
            createPostRequest.googleAddressList.add(googleAddressPack);
//                                    BaseCreatePost.getInstance(CreatePostPropertyActivity.this).getCreatePostRequest().googleAddressList.add(googleAddressPack);
            List<RequestListing.GoogleAddressPack> googleAddressList = createPostRequest.googleAddressList;
            Log.w(TAG, "googleAddressList = " + googleAddressList);
            Log.d(TAG, "callPlaceApi-response: " + BaseCreatePost.getInstance(CreatePostPropertyActivity.this).getCreatePostRequest().toString());

        } else {
            if (Constants.isDevelopment)
                Toast.makeText(CreatePostPropertyActivity.this, "Please enter another place", Toast.LENGTH_LONG).show();
        }
    }

    private void launchSelectCountryActivity(final List<GoogleAddress> googleAddressList) {
        setGoogleAddressList(googleAddressList);
        ArrayList<String> formattedAddressList = new ArrayList<>();
        for (GoogleAddress googleAddress : googleAddressList) {
            formattedAddressList.add(googleAddress.formattedAddress);
        }
        Intent intent = new Intent(this, CreatePostSelectCountryActivity.class);
        intent.putExtra(Constants.EXTRA_CREATE_POST_GOOGLE_ADDRESS_LIST, formattedAddressList);
        Navigation.presentIntentForResult(this, intent, Constants.CREATE_POST_SELECT_COUNTRY);

    }


    @OnClick(R.id.closeBtn)
    public void closeBtnClick(View view) {
        onBackPressed();
    }

    private void showExitDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!isFinishing()) {
                    Dialog.exitSaveChangeDialog(CreatePostPropertyActivity.this, new DialogDualCallback() {
                        @Override
                        public void positive() {
                            //reset value
                            BaseCreatePost.getInstance(CreatePostPropertyActivity.this).resetBaseCreatePost();
                            exitActivity(false);
                        }

                        @Override
                        public void negative() {
                            exitActivity(true);
                        }
                    }).show();
                }
            }
        });
    }

    private void exitActivity(boolean resultOk) {
        setResult(resultOk ? RESULT_OK : RESULT_CANCELED);
        finish();
    }

    @OnClick(R.id.backBtn)
    public void backBtnClick(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        if (BaseCreatePost.getInstance(this).getAddressInputStr() != null)
            showExitDialog();
        else
            finish();
    }

    @OnClick(R.id.nextBtn)
    public void nextBtnClick(View view) {
        RequestListing.AddressUserInput addressUserInput = new RequestListing.AddressUserInput();

        // Mixpanel
        MixpanelAPI mixpanel = MixpanelAPI.getInstance(this, AppConfig.getMixpanelToken());
        mixpanel.track("Saved Posting Details_Address");

        if (apartmentEditText.getText() != null && !apartmentEditText.getText().toString().isEmpty()) {
            addressUserInput.aptSuite = apartmentEditText.getText().toString();
        }
        addressUserInput.street = addressEditText.getText().toString();
        //reset value
        BaseCreatePost.getInstance(this).getCreatePostRequest().addressInputs.clear();
        BaseCreatePost.getInstance(this).getCreatePostRequest().addressInputs.add(addressUserInput);
        Intent intent = new Intent(this, CreatePostUnitDetailActivity.class);
        intent.putExtra(Constants.EXTRA_LANG_INDEX, langIndex);
        Navigation.pushIntentForResult(this, intent, FINISH_POST);
    }

    private void detectLanguageInput(final String inputStr) {

        //TODO: do nothing if google play service not available
        if (IS_SUPPORT_GOOGLE_SERVICE) {
            final GoogleService languageDetectionService =
                    new GoogleService(this);
            languageDetectionService.getTranslationService().getLanguageDetection(
                    inputStr, GoogleService.API_KEY,
                    new retrofit.Callback<ApiResponseLangDetect>() {
                        @Override
                        public void success(ApiResponseLangDetect apiResponse, Response response) {
                            Log.w(TAG, "apiResponse = " + apiResponse);
                            detectedLang = null;

//                        String countryShortName = GoogleGeoUtil.getTypeFromGoogleAddress(googleAddress);
                            ArrayList<String> countryLangList = GoogleGeoUtil.getCountryLangList(CreatePostPropertyActivity.this, detectedLang);

                            if (apiResponse.data.languageTypes != null)
                                if (apiResponse.data.languageTypes.size() > 0)
                                    if (apiResponse.data.languageTypes.get(0).size() > 0) {
                                        if (apiResponse.data.languageTypes.get(0).get(0).confidence < 0.5 || !Arrays.asList(SUPPORT_LANG).contains(apiResponse.data.languageTypes.get(0).get(0).language))
                                            detectedLang = "en";
                                        else
                                            detectedLang = apiResponse.data.languageTypes.get(0).get(0).language.toString();
                                    }


                            if (detectedLang != null) {
                                Log.d(TAG, "getLanguageDetection: " + detectedLang + " -> " + apiResponse.data.languageTypes.get(0).get(0).language + " " + apiResponse.data.languageTypes.get(0).get(0).confidence);
                                //combine TC and SC to one language
                                if (detectedLang.equals("zh-CN"))
                                    detectedLang = "zh-TW";
                                countryLangList.add(0, detectedLang);
                            }

                            Log.d(TAG, "countryLangList: " + AppUtil.removeDuplicate(countryLangList).toString());
                            //prepare the well-format google address
//                        callPlaceApi(googleAddress,AppUtil.removeDuplicate(countryLangList));
                            callGeoCodeApi(addressEditText.getText().toString(), false);
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            Log.e("result", error.toString());
                            //regardless the language detection to continue
                            callGeoCodeApi(addressEditText.getText().toString(), false);
                            NetworkUtil.showNoNetworkMsg(CreatePostPropertyActivity.this);
                        }

                    }
            );
        } else {
            //regardless the language detection to continue
            callGeoCodeApi(addressEditText.getText().toString(), false);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult: " + requestCode + " , " + resultCode);
        if (resultCode == RESULT_OK) {
            if (requestCode == FINISH_POST) {
                if (BaseCreatePost.getInstance(this).getAddressInputStr() != null)
                    setResult(RESULT_OK);
                else
                    setResult(RESULT_CANCELED);
                finish();
            } else if (requestCode == Constants.CREATE_POST_SELECT_COUNTRY) {
                int which = data.getIntExtra(Constants.EXTRA_CREATE_POST_GOOGLE_ADDRESS_SELECT_INDEX, 0);
                //prepare the well-format google address
                callPlaceApi(getGoogleAddressList().get(which));
                googleReturnLinearLayout.setVisibility(View.VISIBLE);
                googleAddressTextView.setText(getGoogleAddressList().get(which).formattedAddress);
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(CreatePostPropertyActivity.this.getCurrentFocus().getWindowToken(), 0);

                btnNext.setEnabled(true);
            }

        }
    }
}
