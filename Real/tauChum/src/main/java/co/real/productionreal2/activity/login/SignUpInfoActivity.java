package co.real.productionreal2.activity.login;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.Layout;
import android.text.Selection;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.squareup.picasso.Picasso;
import com.yalantis.ucrop.UCrop;
import com.yalantis.ucrop.UCropActivity;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import co.real.productionreal2.Constants;
import co.real.productionreal2.Navigation;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.BaseActivity;
import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.callback.DialogCallback;
import co.real.productionreal2.common.LocalStorageHelper;
import co.real.productionreal2.config.AppConfig;
import co.real.productionreal2.daomanager.ConfigDataBaseManager;
import co.real.productionreal2.daomanager.DatabaseManager;
import co.real.productionreal2.service.AdminService;
import co.real.productionreal2.service.RealNetworkService;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.request.RequestRealNetworkMemberPhoneRegistration;
import co.real.productionreal2.service.model.request.RequestSocialLogin;
import co.real.productionreal2.service.model.response.ResponseFileUpload;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;
import co.real.productionreal2.service.model.response.ResponseRealNetworkMemberPhoneRegistration;
import co.real.productionreal2.util.AppUtil;
import co.real.productionreal2.util.CryptLib;
import co.real.productionreal2.util.FileUtil;
import co.real.productionreal2.util.ImageUtil;
import co.real.productionreal2.util.PermissionUtil;
import co.real.productionreal2.util.RoundedTransformation;
import co.real.productionreal2.view.Dialog;

;


public class SignUpInfoActivity extends BaseActivity {
    private static final String TAG = "SignUpInfoActivity";
    @InjectView(R.id.firstNameEditText)
    EditText firstNameEditText;
    @InjectView(R.id.lastNameEditText)
    EditText lastNameEditText;
    @InjectView(R.id.pwEditText)
    EditText pwEditText;
    @InjectView(R.id.confirmPwEditText)
    EditText confirmPwEditText;
    @InjectView(R.id.photoImageView)
    ImageView photoImageView;
    @InjectView(R.id.tvImage)
    TextView tvImage;
    @InjectView(R.id.tvTnc)
    TextView tvTnc;
    @InjectView(R.id.rlCreate)
    Button btnCreate;
    @InjectView(R.id.tvInputHint)
    TextView tvInputHint;

    private static final int REQUEST_SELECT_PICTURE = 10011;
    private static final String SAMPLE_CROPPED_IMAGE_NAME = "cropped_pic";
    private Uri mDestinationUri;

    private String email;
    private String imageFilePath;
    private ProgressDialog ringProgressDialog;
    TouchableSpan privacyClickSpan;
    TouchableSpan tncClickSpan;

    int phoneRegID;
    String pINInput;

    private static final int SELECT_PHOTO = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_info);

        ButterKnife.inject(this);

        tvInputHint.setVisibility(View.INVISIBLE);

        //set background image
        Resources r = getResources();
        Drawable[] layers = new Drawable[2];
        layers[0] = r.getDrawable(R.drawable.bg_login);
        layers[1] = r.getDrawable(R.color.dim_light_overlay);
        layers[1].setAlpha(128);
        LayerDrawable layerDrawable = new LayerDrawable(layers);
        getWindow().setBackgroundDrawable(layerDrawable);

        pwEditText.setTypeface(Typeface.DEFAULT);
        confirmPwEditText.setTypeface(Typeface.DEFAULT);
        Intent intent = getIntent();
        email = intent.getStringExtra(Constants.EXTRA_SIGN_UP_EMAIL);
        phoneRegID = intent.getIntExtra(Constants.EXTRA_PHONE_REG_ID,-1);
        pINInput = intent.getStringExtra(Constants.EXTRA_PIN_INPUT);
        Log.v("","edwin pINInput "+pINInput);

        privacyClickSpan = new TouchableSpan(ContextCompat.getColor(this, R.color.holo_blue2_less),
                ContextCompat.getColor(this, R.color.holo_blue), Color.TRANSPARENT) {
            @Override
            public void onClick(View widget) {
                Intent intent = new Intent(SignUpInfoActivity.this, SignUpPrivacyActivity.class);
                Navigation.presentIntent(SignUpInfoActivity.this, intent);
            }
        };

        tncClickSpan = new TouchableSpan(ContextCompat.getColor(this, R.color.holo_blue2_less),
                ContextCompat.getColor(this, R.color.holo_blue), Color.TRANSPARENT) {
            @Override
            public void onClick(View widget) {
                Intent intent = new Intent(SignUpInfoActivity.this, SignUpTNCActivity.class);
                Navigation.presentIntent(SignUpInfoActivity.this, intent);
            }
        };

        initView();

    }


    private SpannableString getTncString() {
        String totalString = getString(R.string.sign_up__tnc_1) +" "+ getString(R.string.common__privacy_policy) +" "+
                getString(R.string.sign_up__tnc_2) +" "+ getString(R.string.common__terms_of_service);
        SpannableString styledString = new SpannableString(totalString);

        styledString.setSpan(privacyClickSpan, findPrivacyStartIndex(getString(R.string.common__privacy_policy), totalString),
                findPrivacyEndIndex(getString(R.string.common__privacy_policy), totalString), 0);

        styledString.setSpan(tncClickSpan, findPrivacyStartIndex(getString(R.string.common__terms_of_service), totalString),
                findPrivacyEndIndex(getString(R.string.common__terms_of_service), totalString), 0);


        return styledString;
    }

    private int findPrivacyStartIndex(String selectString, String totalString) {
        Pattern word = Pattern.compile(selectString);
        Matcher match = word.matcher(totalString);
        while (match.find()) {
            return match.start();
        }
        return 0;
    }

    private int findPrivacyEndIndex(String selectString, String totalString) {
        Pattern word = Pattern.compile(selectString);
        Matcher match = word.matcher(totalString);
        while (match.find()) {
            return match.end();
        }
        return 0;
    }

    private void initView() {
        tvTnc.setText(getTncString());
        tvTnc.setMovementMethod(LinkTouchMovementMethod.getInstance());

        confirmPwEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    //request permission for OS > 6.0
                    if (ContextCompat.checkSelfPermission(SignUpInfoActivity.this,
                            Manifest.permission.READ_PHONE_STATE)
                            != PackageManager.PERMISSION_GRANTED) {
                        // No explanation needed, we can request the permission.
                        ActivityCompat.requestPermissions(SignUpInfoActivity.this,
                                new String[]{Manifest.permission.READ_PHONE_STATE},
                                PermissionUtil.MY_PERMISSIONS_REQUEST_READ_PHONE_STATUS);

                        // MY_PERMISSIONS_REQUEST_READ_PHONE_STATUS is an
                        // app-defined int constant. The callback method gets the
                        // result of the request.
                        return false;
                    }
                    pressDone();
                }
                return false;
            }
        });

        lastNameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tvInputHint.setVisibility(View.INVISIBLE);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        pwEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tvInputHint.setVisibility(View.INVISIBLE);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        confirmPwEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tvInputHint.setVisibility(View.INVISIBLE);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        firstNameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tvInputHint.setVisibility(View.INVISIBLE);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @OnClick(R.id.addPhotoRelativeLayout)
    public void addPhotoRelativeLayoutClick(View view) {
//        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
//        photoPickerIntent.setType("image/*");
//        startActivityForResult(photoPickerIntent, SELECT_PHOTO);
        pickFromGallery();

    }

    private boolean checkImagePathValid(String imagePath) {
        if (imagePath == null || imagePath.isEmpty()) {
            return false;
        }
        return true;
    }

    @OnClick(R.id.rlCreate)
    public void rlCreateClick(View view) {

        pressDone();
    }

//    @OnClick(R.id.serviceTextView)
//    public void serviceTextViewClick(View view) {
//        Intent intent = new Intent(this, SignUpTNCActivity.class);
//        startActivity(intent);
//    }
//
//    @OnClick(R.id.privacyTextView)
//    public void privacyTextViewClick(View view) {
//        Intent intent = new Intent(this, SignUpPrivacyActivity.class);
//        startActivity(intent);
//    }
//
//    @OnClick(R.id.tncTextView)
//    public void tncTextViewClick(View view) {
//        Intent intent = new Intent(this, SignUpTNCActivity.class);
//        startActivity(intent);
//    }

    private void pickFromGallery() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN // Permission was added in API Level 16
                && ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    REQUEST_STORAGE_READ_ACCESS_PERMISSION);
        } else {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            startActivityForResult(Intent.createChooser(intent, getResources().getString(R.string.common__select)), REQUEST_SELECT_PICTURE);
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_STORAGE_READ_ACCESS_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    pickFromGallery();
                }
                break;
            case PermissionUtil.MY_PERMISSIONS_REQUEST_READ_PHONE_STATUS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    pressDone();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    /**
     * In most cases you need only to set crop aspect ration and max size for resulting image.
     *
     * @param uCrop - ucrop builder instance
     * @return - ucrop builder instance
     */
    private UCrop basisConfig(@NonNull UCrop uCrop) {

        uCrop = uCrop.useSourceImageAspectRatio();
        uCrop = uCrop.withAspectRatio(1, 1);
        return uCrop;
    }


    /**
     * Sometimes you want to adjust more options, it's done via {@link com.yalantis.ucrop.UCrop.Options} class.
     *
     * @param uCrop - ucrop builder instance
     * @return - ucrop builder instance
     */
    private UCrop advancedConfig(@NonNull UCrop uCrop) {
        UCrop.Options options = new UCrop.Options();
        options.setCompressionFormat(Bitmap.CompressFormat.JPEG);
//        options.setCompressionQuality(100);


        // If you want to configure how gestures work for all UCropActivity tabs
        options.setAllowedGestures(UCropActivity.SCALE, UCropActivity.ROTATE, UCropActivity.ALL);

        options.setOvalDimmedLayer(true);
        options.setShowCropFrame(false);
        options.setShowCropGrid(false);
        // Color palette
        options.setToolbarColor(ContextCompat.getColor(this, R.color.tab_bg));
        options.setStatusBarColor(ContextCompat.getColor(this, R.color.tab_bg));
        options.setActiveWidgetColor(ContextCompat.getColor(this, R.color.holo_blue2_less));
        options.setToolbarWidgetColor(ContextCompat.getColor(this, R.color.white));

        return uCrop.withOptions(options);
    }

    private void startCropActivity(@NonNull Uri uri) {
        mDestinationUri = Uri.fromFile(new File(getCacheDir(), SAMPLE_CROPPED_IMAGE_NAME + System.currentTimeMillis() + ".jpeg"));
        UCrop uCrop = UCrop.of(uri, mDestinationUri);

        uCrop = basisConfig(uCrop);
        uCrop = advancedConfig(uCrop);

        uCrop.start(this);
    }


    private class UploadFileTask extends AsyncTask<String, String, String> {
        private ResponseRealNetworkMemberPhoneRegistration responseGson;

        @Override
        protected String doInBackground(String... jsonContent) {
            String decryptJson;
            if (Constants.enableEncryption) {
                decryptJson = CryptLib.decrypt(jsonContent[0]);
                Log.w("requestsocialLogin", "jsonContent = " + decryptJson);
            } else {
                decryptJson = jsonContent[0];
            }
            responseGson = new Gson().fromJson(decryptJson, ResponseRealNetworkMemberPhoneRegistration.class);

            HttpParams params = new BasicHttpParams();
            params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
            DefaultHttpClient httpClient = new DefaultHttpClient(params);
            return resizePhotoAndPublish(httpClient, responseGson, imageFilePath);
        }

        @Override
        protected void onProgressUpdate(String... values) {
            Log.w(TAG, "onProgressUpdate　photoUrl = " + values[0]);
        }

        @Override
        protected void onPostExecute(String photoURL) {
            if (responseGson == null) {
                return;
            }

            // Mixpanel
            MixpanelAPI mixpanel = MixpanelAPI.getInstance(SignUpInfoActivity.this, AppConfig.getMixpanelToken());
            mixpanel.track("Became User");
            mixpanel.getPeople().set("$first_name", responseGson.content.firstName);
            mixpanel.getPeople().set("$last_name", responseGson.content.lastName);
            String name = responseGson.content.firstName + " " + responseGson.content.lastName;
            mixpanel.getPeople().set("$name", name);
            mixpanel.getPeople().set("Agent", "NO");
            mixpanel.getPeople().set("# of listings", 0);
            mixpanel.getPeople().set("User since", AppUtil.dateStringForMixpanel());
            Locale locale = LocalStorageHelper.getCurrentLocale(SignUpInfoActivity.this);
            mixpanel.getPeople().set("Language", locale.getLanguage());

            RequestSocialLogin socialLogin = new RequestSocialLogin(
                    AdminService.DEVICE_TYPE_ANDROID,
                    FileUtil.getDeviceId(SignUpInfoActivity.this),
                    AdminService.SOCIAL_PHONE,
                    responseGson.content.firstName + " " + responseGson.content.lastName,
                    responseGson.content.userId,
                    responseGson.content.email,
                    photoURL,
                    responseGson.content.accessToken
            );
            socialLogin.callLoginSocialApi(SignUpInfoActivity.this, new ApiCallback() {
                @Override
                public void success(ApiResponse apiResponse) {
                    String decryptJson;
                    if (Constants.enableEncryption) {
                        decryptJson = CryptLib.decrypt(apiResponse.jsonContent);
                        Log.w("requestsocialLogin", "jsonContent = " + decryptJson);
                    } else {
                        decryptJson = apiResponse.jsonContent;
                    }
                    ResponseLoginSocial responseGson = new Gson().fromJson(decryptJson, ResponseLoginSocial.class);

                    // Mixpanel
                    MixpanelAPI mixpanel = MixpanelAPI.getInstance(SignUpInfoActivity.this, AppConfig.getMixpanelToken());
                    mixpanel.alias("" + responseGson.content.memId, mixpanel.getDistinctId());
                    mixpanel.identify(mixpanel.getDistinctId());
                    mixpanel.getPeople().identify(mixpanel.getDistinctId());
                    mixpanel.getPeople().set("Member ID", "" + responseGson.content.memId);

                    LocalStorageHelper.setUpByLogin(SignUpInfoActivity.this, responseGson.content);

                    ConfigDataBaseManager configDataBaseManager = ConfigDataBaseManager.getInstance(SignUpInfoActivity.this);
                    DatabaseManager.getInstance(SignUpInfoActivity.this).dbActionForLogin(responseGson.content);
                    configDataBaseManager.dbActionMain(responseGson.content.memId);


                    //have permission to read local contact list
//                    boolean canReadContact=true;
//                    if (canReadContact) {
//                        Intent intent = new Intent(SignUpInfoActivity.this, ConnectAgentActivity.class);
//                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                        Navigation.pushIntent(SignUpInfoActivity.this, intent);
//                    }else {
                        AppUtil.goToMainTabActivity(SignUpInfoActivity.this, responseGson.content);
//                    }


                    if (ringProgressDialog != null) {
                        ringProgressDialog.dismiss();
                        ringProgressDialog.cancel();
                        ringProgressDialog = null;
                    }
                }

                @Override
                public void failure(final String errorMsg) {
                    if (ringProgressDialog != null) {
                        ringProgressDialog.dismiss();
                        ringProgressDialog.cancel();
                        ringProgressDialog = null;
                    }
                    if (errorMsg.trim() != "")
                        Dialog.uploadUserPhotoFileFailDialog(SignUpInfoActivity.this, new DialogCallback() {
                            @Override
                            public void yes() {
                                Intent intent = new Intent(SignUpInfoActivity.this, RealNetworkService.RealNetwork.class);
                                startActivity(intent);
                                finish();
                            }
                        }).show();

                }
            });
        }

        private String resizePhotoAndPublish(DefaultHttpClient httpClient, ResponseRealNetworkMemberPhoneRegistration responseGson, String imagePath) {
            File imageInDevice = new File(FileUtil.getAppSubFolder(Constants.UPLOAD_FILE_FOLDER_NAME),
                    "UserImage.jpg");
            try {
//                FileUtil.convertBitmapToFile(imageInDevice, ImageUtil.decodeSampledBitmapFromResource(imagePath));
                ImageUtil.getCompressImageFilePath(imagePath, imageInDevice.getAbsolutePath(), ImageUtil.iconImageType);
            } catch (Exception e) {
                e.printStackTrace();
            }
            String photoUrl = callUploadFileApi(httpClient, responseGson, imageInDevice);
            publishProgress(photoUrl);
            return photoUrl;
        }
    }

    private String callUploadFileApi(DefaultHttpClient httpClient, ResponseRealNetworkMemberPhoneRegistration responseGson, File photoFile) {

        Log.d("callUploadFileApi", "callUploadFileApi: " + responseGson.toString());
        HttpPost httppost = new HttpPost(AdminService.WEB_SERVICE_BASE_URL + "/Admin.asmx/FileUpload");
        MultipartEntity multipartEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);

        try {
            multipartEntity.addPart("MemberID", new StringBody("" + responseGson.content.userId));
            multipartEntity.addPart("AccessToken", new StringBody(responseGson.content.accessToken));
            multipartEntity.addPart("ListingID", new StringBody("0"));
            multipartEntity.addPart("Position", new StringBody("0"));
            multipartEntity.addPart("FileExt", new StringBody(FileUtil.getFileExt(photoFile.getAbsolutePath())));
            multipartEntity.addPart("FileType", new StringBody("4"));
            multipartEntity.addPart("attachment", new FileBody(photoFile));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        httppost.setEntity(multipartEntity);
        try {
            HttpResponse httpResponse = httpClient.execute(httppost);
            InputStream is = httpResponse.getEntity().getContent();
            String responseString = FileUtil.convertStreamToString(is);
            Document doc = FileUtil.getDomElement(responseString);
            doc.getDocumentElement().normalize();
            NodeList nodeList = doc.getElementsByTagName("string");
            Node node = nodeList.item(0);
            String json = FileUtil.nodeToString(node.getFirstChild());
            if (json != null) {
                ResponseFileUpload responseFileUpload = new Gson().fromJson(json, ResponseFileUpload.class);
                if (responseFileUpload.content == null)
                    return null;
                if (responseFileUpload.content.photoUrl == null)
                    return null;
                return responseFileUpload.content.photoUrl;
            }

            return null;

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    void pressDone() {

        // Mixpanel
        MixpanelAPI mixpanel = MixpanelAPI.getInstance(this, AppConfig.getMixpanelToken());
        mixpanel.track("Saved User Profile Details");

        if (!checkImagePathValid(imageFilePath)) {
            tvInputHint.setText(R.string.error_message__please_upload);
            tvInputHint.setVisibility(View.VISIBLE);
        } else if (firstNameEditText.getText().toString().isEmpty()) {
            tvInputHint.setText(R.string.error_message__first_name);
            tvInputHint.setVisibility(View.VISIBLE);
        } else if (lastNameEditText.getText().toString().isEmpty()) {
            tvInputHint.setText(R.string.error_message__last_name);
            tvInputHint.setVisibility(View.VISIBLE);
//        } else if (pwEditText.getText().toString().isEmpty()) {
//            tvInputHint.setText(R.string.error_message__passowrd_cannot);
//            tvInputHint.setVisibility(View.VISIBLE);
//        } else if (!ValidUtil.isValidPassword(pwEditText.getText().toString())) {
//            tvInputHint.setText(R.string.error_message__password_must_contain);
//            tvInputHint.setVisibility(View.VISIBLE);
//            return;
//        }else if (!confirmPwEditText.getText().toString().equals(pwEditText.getText().toString())) {
//            tvInputHint.setText(R.string.error_message__password_does_not_match);
//            tvInputHint.setVisibility(View.VISIBLE);
        } else {

            if (ringProgressDialog != null && ringProgressDialog.isShowing())
                return;


            //permission grant for OS>6.0
            //request permission for OS > 6.0
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.READ_PHONE_STATE)
                    != PackageManager.PERMISSION_GRANTED) {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_PHONE_STATE},
                        PermissionUtil.MY_PERMISSIONS_REQUEST_READ_PHONE_STATUS);

                // MY_PERMISSIONS_REQUEST_READ_PHONE_STATUS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
                return;
            }

            ringProgressDialog = ProgressDialog.show(this, getResources().getString(R.string.alert__Initializing),
                    getResources().getString(R.string.common__login_in));

            //disable multi click
            btnCreate.setEnabled(false);

//            RequestRealNetworkMemReg realNetworkMemReg = new RequestRealNetworkMemReg(email,
//                    CryptLib.md5(pwEditText.getText().toString()),
//                    firstNameEditText.getText().toString(),
//                    lastNameEditText.getText().toString());
//            realNetworkMemReg.callRealNetworkMemRegApi(this, new ApiCallback() {
//                @Override
//                public void success(final ApiResponse apiResponse) {
//                    String decryptJson;
//                    if (Constants.enableEncryption) {
//                        decryptJson = CryptLib.decrypt(apiResponse.jsonContent);
//                        Log.w("realNetworkMemReg", "jsonContent = " + decryptJson);
//                    } else {
//                        decryptJson = apiResponse.jsonContent;
//                    }
//                    LocalStorageHelper.setRealNetworkJson(SignUpInfoActivity.this, decryptJson);
//
//                    Dialog.feedbackDialog(SignUpInfoActivity.this, false,
//                            getString(R.string.error_message__reset_email_sent),
//                            getString(R.string.error_message__verift_your_email),new DialogCallback() {
//                                @Override
//                                public void yes() {
//                                    new UploadFileTask().execute(apiResponse.jsonContent);
//                                }
//                            }).show();
//
//                }
//
//                @Override
//                public void failure(String errorMsg) {
//                    if (ringProgressDialog != null) {
//                        ringProgressDialog.dismiss();
//                        ringProgressDialog.cancel();
//                        ringProgressDialog = null;
//                    }
//                    if (errorMsg.trim() != "")
//                        Dialog.realNetworkMemRegFailDialog(SignUpInfoActivity.this).show();
//                }
//            });

            RequestRealNetworkMemberPhoneRegistration requestRealNetworkMemberPhoneRegistration = new RequestRealNetworkMemberPhoneRegistration(
                    phoneRegID,
                    pINInput,
                    firstNameEditText.getText().toString(),
                    lastNameEditText.getText().toString());
            requestRealNetworkMemberPhoneRegistration.callRealNetworkMemberPhoneRegistrationApi(this, new ApiCallback() {
                @Override
                public void success(final ApiResponse apiResponse) {
                    String decryptJson;
                    if (Constants.enableEncryption) {
                        decryptJson = CryptLib.decrypt(apiResponse.jsonContent);
                        Log.w("realNetworkMemReg", "jsonContent = " + decryptJson);
                    } else {
                        decryptJson = apiResponse.jsonContent;
                    }
                    LocalStorageHelper.setRealNetworkJson(SignUpInfoActivity.this, decryptJson);

                    Dialog.feedbackDialog(SignUpInfoActivity.this, false,
                            getString(R.string.error_message__reset_email_sent),
                            getString(R.string.error_message__verift_your_email),new DialogCallback() {
                                @Override
                                public void yes() {
                                    new UploadFileTask().execute(apiResponse.jsonContent);
                                }
                            }).show();

                }

                @Override
                public void failure(String errorMsg) {
                    if (ringProgressDialog != null) {
                        ringProgressDialog.dismiss();
                        ringProgressDialog.cancel();
                        ringProgressDialog = null;
                    }
                    if (errorMsg.trim() != "")
                        Dialog.realNetworkMemRegFailDialog(SignUpInfoActivity.this).show();
                }
            });

        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
//        if (requestCode == SELECT_PHOTO && resultCode == RESULT_OK) {
//            Uri selectedImage = intent.getData();
//            imageFilePath = FileUtil.getRealPathFromURI(this, selectedImage);
//            Log.w(TAG, "imageFilePath = " + imageFilePath);
//            tvImage.setVisibility(View.GONE);
////            Picasso.with(this)
////                    .load(imageFilePath)
////                    .placeholder(R.drawable.com_facebook_profile_picture_blank_square)
////                    .error(R.drawable.com_facebook_profile_picture_blank_square)
////                    .transform(new RoundedTransformation(false)).into(photoImageView);
//            Picasso.with(this)
//                    .load(new File(imageFilePath))
//                    .placeholder(R.drawable.progress_animation)
//                    .error(R.drawable.com_facebook_profile_picture_blank_square)
//                    .transform(new RoundedTransformation()).into(photoImageView);
        if (resultCode == RESULT_OK && requestCode == REQUEST_SELECT_PICTURE) {
            try {
                final Uri selectedUri = intent.getData();
                startCropActivity(selectedUri);
                Log.w("imageFilePath", "UCrop = ok request crop:  " + selectedUri);
            } catch (Exception e) {
                AppUtil.showToast(this, getString(R.string.chatroom_error_message__incompatible_error)); //TODO: general error for some not supporting image
            }
        } else if (resultCode == RESULT_OK && requestCode == UCrop.REQUEST_CROP) {
            final Uri resultUri = UCrop.getOutput(intent);
            imageFilePath = FileUtil.getRealPathFromURI(this, resultUri);
            Log.w("imageFilePath", "imageFilePath = " + imageFilePath);
            Picasso.with(this)
                    .load(new File(imageFilePath))
                    .error(R.drawable.com_facebook_profile_picture_blank_square)
                    .transform(new RoundedTransformation(false)).into(photoImageView);
            tvImage.setVisibility(View.GONE);
            Log.w("imageFilePath", "UCrop = ok " + resultUri);
        } else if (resultCode == UCrop.RESULT_ERROR) {
            Log.w("imageFilePath", "UCrop = err");
            final Throwable cropError = UCrop.getError(intent);
        }

    }

    private class LinkTouchMovementMethod extends LinkMovementMethod {
        private TouchableSpan mPressedSpan;

        @Override
        public boolean onTouchEvent(TextView textView, Spannable spannable, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                mPressedSpan = getPressedSpan(textView, spannable, event);
                if (mPressedSpan != null) {
                    mPressedSpan.setPressed(true);
                    Selection.setSelection(spannable, spannable.getSpanStart(mPressedSpan),
                            spannable.getSpanEnd(mPressedSpan));
                }
            } else if (event.getAction() == MotionEvent.ACTION_MOVE) {
                TouchableSpan touchedSpan = getPressedSpan(textView, spannable, event);
                if (mPressedSpan != null && touchedSpan != mPressedSpan) {
                    mPressedSpan.setPressed(false);
                    mPressedSpan = null;
                    Selection.removeSelection(spannable);
                }
            } else {
                if (mPressedSpan != null) {
                    mPressedSpan.setPressed(false);
                    super.onTouchEvent(textView, spannable, event);
                }
                mPressedSpan = null;
                Selection.removeSelection(spannable);
            }
            return true;
        }

        private TouchableSpan getPressedSpan(TextView textView, Spannable spannable, MotionEvent event) {

            int x = (int) event.getX();
            int y = (int) event.getY();

            x -= textView.getTotalPaddingLeft();
            y -= textView.getTotalPaddingTop();

            x += textView.getScrollX();
            y += textView.getScrollY();

            Layout layout = textView.getLayout();
            int line = layout.getLineForVertical(y);
            int off = layout.getOffsetForHorizontal(line, x);

            TouchableSpan[] link = spannable.getSpans(off, off, TouchableSpan.class);
            TouchableSpan touchedSpan = null;
            if (link.length > 0) {
                touchedSpan = link[0];
            }
            return touchedSpan;
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        btnCreate.setEnabled(true);
    }

    public abstract class TouchableSpan extends ClickableSpan {
        private boolean mIsPressed;
        private int mPressedBackgroundColor;
        private int mNormalTextColor;
        private int mPressedTextColor;

        public TouchableSpan(int normalTextColor, int pressedTextColor, int pressedBackgroundColor) {
            mNormalTextColor = normalTextColor;
            mPressedTextColor = pressedTextColor;
            mPressedBackgroundColor = pressedBackgroundColor;
        }

        public void setPressed(boolean isSelected) {
            mIsPressed = isSelected;
        }

        @Override
        public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setColor(mIsPressed ? mPressedTextColor : mNormalTextColor);
            ds.bgColor = mIsPressed ? mPressedBackgroundColor : mPressedBackgroundColor;
            ds.setUnderlineText(false);
        }
    }

}
