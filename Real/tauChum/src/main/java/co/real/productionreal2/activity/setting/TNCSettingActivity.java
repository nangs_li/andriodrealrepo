package co.real.productionreal2.activity.setting;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;

import butterknife.ButterKnife;
import butterknife.InjectView;
import co.real.productionreal2.CoreData;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.BaseActivity;
import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.common.LocalStorageHelper;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.request.RequestLegalStatement;
import co.real.productionreal2.service.model.response.ResponseLegalStatement;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;
import co.real.productionreal2.util.AppUtil;


public class TNCSettingActivity extends BaseActivity {
    @InjectView(R.id.headerLayout)
    View headerLayout;
    @InjectView(R.id.tncTextView)
    TextView tncTextView;
    @InjectView(R.id.progressBar)
    ProgressBar progressBar;

    private ResponseLoginSocial.Content userContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tnc);
        ButterKnife.inject(this);
        userContent = CoreData.getUserContent(this);

        initView();
        progressBar.setVisibility(View.VISIBLE);
        RequestLegalStatement legalStatement = new RequestLegalStatement(userContent.memId, LocalStorageHelper.getAccessToken(getBaseContext()));
        legalStatement.langCode = "en";
        legalStatement.msgType = 2;
        legalStatement.callLegalStatementGetApi(this, new ApiCallback() {
            @Override
            public void success(ApiResponse apiResponse) {
                progressBar.setVisibility(View.GONE);
                ResponseLegalStatement responseGson = new Gson().fromJson(apiResponse.jsonContent, ResponseLegalStatement.class);
                tncTextView.setText(responseGson.content.msg);
            }

            @Override
            public void failure(String errorMsg) {
                progressBar.setVisibility(View.GONE);
               AppUtil.showToast(TNCSettingActivity.this, errorMsg);
            }
        });
    }

    private void initView() {
        TextView titleTextView = (TextView) headerLayout.findViewById(R.id.titleTextView);
        titleTextView.setText(R.string.common__terms_of_service);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
