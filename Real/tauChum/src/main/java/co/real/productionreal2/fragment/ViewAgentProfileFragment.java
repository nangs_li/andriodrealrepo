package co.real.productionreal2.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.MainActivityTabBase;
import co.real.productionreal2.adapter.PastClosingAdapter;
import co.real.productionreal2.callback.FragmentViewChangeListener;
import co.real.productionreal2.daomanager.DatabaseManager;
import co.real.productionreal2.service.model.AgentExp;
import co.real.productionreal2.service.model.AgentPastClosing;
import co.real.productionreal2.service.model.AgentProfiles;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;
import co.real.productionreal2.view.ExpandableHeightDynamicListView;
import com.nhaarman.listviewanimations.appearance.simple.AlphaInAnimationAdapter;
import com.nhaarman.listviewanimations.itemmanipulation.swipedismiss.undo.SimpleSwipeUndoAdapter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import widget.RoundedImageView;

/**
 * Created by hohojo on 7/10/2015.
 */
public class ViewAgentProfileFragment extends Fragment {
    private static final String TAG = "ViewAgentProfileFragment";
    private FragmentViewChangeListener fragmentViewChangeListener;
    @InjectView(R.id.expTextView)
    TextView expTextView;
    @InjectView(R.id.langTextView)
    TextView langTextView;
    @InjectView(R.id.specTextView)
    TextView specTextView;
    @InjectView(R.id.top5CloseTextView)
    TextView top5CloseTextView;
    @InjectView(R.id.exp_layout)
    LinearLayout expLayout;
    @InjectView(R.id.langResultLinearLayout)
    LinearLayout langResultLinearLayout;
    @InjectView(R.id.specResultLinearLayout)
    LinearLayout specResultLinearLayout;
    @InjectView(R.id.past_exp_dynamiclistview)
    ExpandableHeightDynamicListView pastClosingListView;
    @InjectView(R.id.agentNameTextView)
    TextView agentNameTextView;
    @InjectView(R.id.licenseNoTextView)
    TextView licenseNoTextView;
    @InjectView(R.id.agentImageView)
    RoundedImageView agentImageView;
    @InjectView(R.id.maskView)
    View maskView;
    @InjectView(R.id.rootView)
    RelativeLayout rootView;

    @InjectView(R.id.tvLangResult)
    TextView tvLangResult;
    @InjectView(R.id.tvSpecResult)
    TextView tvSpecResult;


    private AgentProfiles agentProfile;
    private ResponseLoginSocial.Content agentContent;
    private ViewPager pager;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            fragmentViewChangeListener = (FragmentViewChangeListener) activity;
        } catch (ClassCastException e) {
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        fragmentViewChangeListener = null;
    }

    public static ViewAgentProfileFragment newInstance(ViewPager pager) {
        Bundle args = new Bundle();
        ViewAgentProfileFragment fragment = new ViewAgentProfileFragment();
        fragment.setArguments(args);
        fragment.setPager(pager);
        return fragment;
    }

    private void setPager(ViewPager pager) {

        this.pager = pager;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_view_agent_profile, container, false);
        ButterKnife.inject(this, view);

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        agentContent = new Gson().fromJson(DatabaseManager.getInstance(getActivity()).getLogInfo().getLogInfo(),
                ResponseLoginSocial.Content.class);
        agentProfile = agentContent.agentProfile;
        initView();

        if (pager != null) {

            getView().setFocusableInTouchMode(true);
            getView().requestFocus();
            getView().setOnKeyListener(new View.OnKeyListener() {

                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {


                    Log.d(this.getClass().getName(), "onKey: ");
                    if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {

                        // handle back button
                        Log.d(this.getClass().getName(), "onBackPressed: ");
                        pager.setCurrentItem(pager.getCurrentItem()-1, true);
                        ((MainActivityTabBase)getActivity()).openTabBar();

                        return true;
                    }

                    return false;
                }
            });
        }
    }

    private void initView() {
        langTextView.setText(R.string.add_language_skills_title);
        expTextView.setTextColor(getResources().getColor(R.color.content_title));
        langTextView.setTextColor(getResources().getColor(R.color.content_title));
        specTextView.setTextColor(getResources().getColor(R.color.content_title));
        top5CloseTextView.setTextColor(getResources().getColor(R.color.content_title));
        agentNameTextView.setText(agentProfile.memberName);
        licenseNoTextView.setText(agentProfile.agentLicenseNumber);


        Picasso.with(getActivity())
                .load(agentContent.photoUrl)
                .error(R.drawable.default_profile)
                .placeholder(R.drawable.default_profile)
                .into(agentImageView);


        createExpLayout();
        createLangLayout();
        createSpecLayout();
        createTop5PastLayout();


    }

    private void createExpLayout() {
        expLayout.removeAllViews();
        for (int i = 0; i < agentProfile.agentExpList.size(); i++) {
            appendAgentExpView(agentProfile.agentExpList.get(i), i);
        }
    }

    private void createLangLayout() {
        ArrayList<String> nativeNameList = new ArrayList<>();
        for (int i = 0; i < agentProfile.agentLanguageList.size(); i++) {
            for (int j = 0; j < agentContent.systemSetting.spokenLangList.size(); j++) {
                if (agentContent.systemSetting.spokenLangList.get(j).index ==
                        agentProfile.agentLanguageList.get(i).langIndex) {
                    nativeNameList.add(agentContent.systemSetting.spokenLangList.get(j).nativeName);
                }
            }

        }
        String nativeNamesString = "";
        for (int i = 0; i < nativeNameList.size(); i++) {
            if (i == 0)
                nativeNamesString = nativeNameList.get(i);
            else
                nativeNamesString = nativeNamesString + "・" + nativeNameList.get(i);
        }
        tvLangResult.setVisibility(View.VISIBLE);
        tvLangResult.setText(nativeNamesString);
//        new VertHoriLinearLayout(getActivity(), langResultLinearLayout,
//                nativeNameList,
//                ImageUtil.getScreenWidth(getActivity()), VertHoriLinearLayout.VIEW_PROFILE_LANG,
//                new LangSpecDelCallback() {
//                    @Override
//                    public void onLangSpecDel(int tagNo, int type) {
//                    }
//                });
    }

    private void createSpecLayout() {
        String agentSpecString = "";
        for (int i = 0; i < agentProfile.agentSpecialtyList.size(); i++) {
            if (i == 0)
                agentSpecString = agentProfile.agentSpecialtyList.get(i).specialty;
            else
                agentSpecString = agentSpecString + "・" + agentProfile.agentSpecialtyList.get(i).specialty;
        }
        tvSpecResult.setVisibility(View.VISIBLE);
        tvSpecResult.setText(agentSpecString);
//        new VertHoriLinearLayout(getActivity(), specResultLinearLayout,
//                VertHoriLinearLayout.agentSpecListToStringList((ArrayList<AgentSpecialty>) agentProfile.agentSpecialtyList),
//                ImageUtil.getScreenWidth(getActivity()), VertHoriLinearLayout.VIEW_PROFILE_LANG,
//                new LangSpecDelCallback() {
//                    @Override
//                    public void onLangSpecDel(int tagNo, int type) {
//                    }
//                });
    }

    private void createTop5PastLayout() {
        if (agentContent == null)
            return;
        if (agentContent.systemSetting == null)
            return;
        if (agentContent.systemSetting.propertyTypeList == null)
            return;
        PastClosingAdapter pastClosingAdapter = new PastClosingAdapter(getActivity(), null, agentContent.systemSetting.propertyTypeList, true);
        if (agentProfile.agentPastClosingList != null && agentProfile.agentPastClosingList.size() > 0) {
            for (AgentPastClosing agentPastClosing : agentProfile.agentPastClosingList) {
                pastClosingAdapter.add(agentPastClosing);
            }
        }
        SimpleSwipeUndoAdapter simpleSwipeUndoAdapter = new SimpleSwipeUndoAdapter(pastClosingAdapter, getActivity(),
                null);
        AlphaInAnimationAdapter animAdapter = new AlphaInAnimationAdapter(simpleSwipeUndoAdapter);
        animAdapter.setAbsListView(pastClosingListView);
        assert animAdapter.getViewAnimator() != null;
        animAdapter.getViewAnimator().setInitialDelayMillis(ExpandableHeightDynamicListView.ANIM_INITIAL_DELAY_MILLIS);
        pastClosingListView.setAdapter(animAdapter);

        pastClosingListView.setAdapter(pastClosingAdapter);
        pastClosingListView.setExpanded(true);
    }

    private void appendAgentExpView(final AgentExp agentExp, int tagNo) {
        LayoutInflater layoutInflater = getActivity().getLayoutInflater();
        View view = layoutInflater.inflate(R.layout.agent_exp_item, null);

        TextView companyTextView = ButterKnife.findById(view, R.id.companyTextView);
        TextView titleTextView = ButterKnife.findById(view, R.id.titleTextView);
        TextView periodTextView = ButterKnife.findById(view, R.id.periodTextView);
        ImageButton removeBtn = ButterKnife.findById(view, R.id.removeExpBtn);
        ImageButton expEditBtn = ButterKnife.findById(view, R.id.expEditBtn);

        companyTextView.setText(agentExp.company);
        titleTextView.setText(agentExp.title);
        periodTextView.setText(agentExp.getExpPeriodText(getActivity()));

        removeBtn.setVisibility(View.GONE);
        expEditBtn.setVisibility(View.GONE);

        expLayout.addView(view);
    }

//    @OnClick(R.id.editBtn)
//    public void reportBtnClick(View view) {
//        Intent intent = new Intent(getActivity(), EditAgentProfileActivity.class);
//        maskView.setVisibility(View.VISIBLE);
//        intent.putExtra(Constants.EXTRA_SCREEN_SHOT_BYTES, ImageUtil.getScreenShotBlurAndReduceBrightnessBytes(getActivity(), -120));
//        maskView.setVisibility(View.GONE);
//        getActivity().startActivityForResult(intent, Constants.ME_VIEW_PROFILE);
//    }

}
