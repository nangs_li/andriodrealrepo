package co.real.productionreal2.callback;

/**
 * Created by hohojo on 28/8/2015.
 */
public interface UpdateSettingViewListener {
    public void settingViewIsReady();
}
