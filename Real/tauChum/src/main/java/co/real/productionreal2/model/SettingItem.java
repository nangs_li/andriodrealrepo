package co.real.productionreal2.model;

import co.real.productionreal2.R;

/**
 * Created by alexhung on 15/4/16.
 */
public class SettingItem {
    public enum SettingItemType {SETTING_TYPE_ROW,SETTING_TYPE_SECTION_SPACING,SETTING_TYPE_LOGOUT}
    public String title,value;
    public SettingItemType itemType;
    public Class<?> targetClass;
    public int backgroundColorID;
    public boolean showDivider;
    public SettingItem (){
        this.backgroundColorID = R.color.transparent;
        this.showDivider = true;
    }

    public SettingItem(String title,String value,Class<?>targetClass){
        this();
        this.title = title;
        this.value = value;
        this.itemType = SettingItemType.SETTING_TYPE_ROW;
        this.targetClass = targetClass;
    }

    public SettingItem(String title,String value,Class<?>targetClass,int backgroundcolorID){
        this();
        this.title = title;
        this.value = value;
        this.itemType = SettingItemType.SETTING_TYPE_ROW;
        this.targetClass = targetClass;
        this.backgroundColorID = backgroundcolorID;
    }
    public SettingItem(String title,String value,Class<?>targetClass,int backgroundcolorID,boolean showDivider){
        this();
        this.title = title;
        this.value = value;
        this.itemType = SettingItemType.SETTING_TYPE_ROW;
        this.targetClass = targetClass;
        this.backgroundColorID = backgroundcolorID;
        this.showDivider = showDivider;
    }
    public SettingItem(String title,String value,Class<?>targetClass,SettingItemType type){
        this();
        this.title = title;
        this.value = value;
        this.itemType = type;
        this.targetClass = targetClass;
    }

    public SettingItem(SettingItemType type,int backgroundcolorID){
        super();
        this.itemType = type;
        this.backgroundColorID = backgroundcolorID;
    }

}
