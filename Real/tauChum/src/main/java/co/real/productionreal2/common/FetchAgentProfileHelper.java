package co.real.productionreal2.common;

import android.content.Intent;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import co.real.productionreal2.ApplicationSingleton;
import co.real.productionreal2.Constants;
import co.real.productionreal2.chat.core.ChatService;
import co.real.productionreal2.daomanager.DatabaseManager;
import co.real.productionreal2.service.BaseService;

/**
 * Created by DerekCheung on 27/4/2016.
 */
public class FetchAgentProfileHelper {
    
    private static String TAG = "FetchAgentProfileHelper";

    private Handler mHandler;
    private boolean isFetching;

    private static final FetchAgentProfileHelper instance = new FetchAgentProfileHelper();

    public static FetchAgentProfileHelper getInstance() {
        return instance;
    }

    private FetchAgentProfileHelper() {

        mHandler = new Handler();
        isFetching = false;
    }

    Runnable mStatusChecker = new Runnable() {

        @Override
        public void run() {
            try {
                // pragma mark - DC
                // find out expired agent profiles
                Log.d(TAG, "finding out expired agent profiles");
                String[] QBIds = DatabaseManager.getInstance(ApplicationSingleton.getInstance().getApplicationContext()).getExpireAgentProfileQBIDList();

                if (QBIds != null && QBIds.length > 0) {

                    // get updated agent profiles
                    Log.d(TAG, "getting updated agent profiles");
                    final ArrayList<String> QBIdsCollection = new ArrayList<String>();
                    for (String QBId: QBIds) {

                        QBIdsCollection.add(QBId);
                    }

                    ChatService.getInstance().getAgentProfileByQBIDs(QBIdsCollection, new BaseService.BaseServiceCallBack() {
                        @Override
                        public void onSuccess(Object var1) {

                            // If succeed, update chat lobby UI
                            Log.d(TAG, QBIdsCollection.size() + " agent profile is updated");

                            Log.d(TAG, "updating chat lobby UI");
                            broadcastForAgentProfilesDidUpdate();
                        }

                        @Override
                        public void onError(BaseService.RealServiceError serviceError) {

                            // If failed, do nothing
                        }
                    });

                } else {

                    Log.d(TAG, "No need to fetch");
                }


            } finally {
                
                mHandler.postDelayed(mStatusChecker, Constants.AGENT_PROFILE_REFRESH_INTERVAL);
            }
        }
    };

    public void startFetching() {

        if (isFetching) {

            Log.d(TAG, "startedFetchingAlready");

        } else {

            Log.d(TAG, "startFetching");
            mHandler.post(mStatusChecker);
        }

        isFetching = true;
    }

    public void stopFetching() {

        if (isFetching) {

            Log.d(TAG, "stopFetching");
            mHandler.removeCallbacks(mStatusChecker);

        } else {

            Log.d(TAG, "stoppedFetchingAlready");
        }

        isFetching = false;
    }

    private void broadcastForAgentProfilesDidUpdate() {
        Log.d(TAG, Constants.AGENT_PROFILE_DID_UPDATE);
        Intent intent = new Intent(Constants.AGENT_PROFILE_DID_UPDATE);
        LocalBroadcastManager.getInstance(ApplicationSingleton.getInstance().getApplicationContext()).sendBroadcast(intent);
    }
}
