package co.real.productionreal2.util;

import android.content.Context;
import android.text.format.DateFormat;
import android.util.Log;

import org.ocpsoft.prettytime.PrettyTime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import co.real.productionreal2.R;
import co.real.productionreal2.common.LocalStorageHelper;


/**
 * Created by igorkhomenko on 1/13/15.
 */
public class TimeUtils {
    public final static long ONE_SECOND = 1000;
    public final static long SECONDS = 60;

    public final static long ONE_MINUTE = ONE_SECOND * 60;
    public final static long MINUTES = 60;

    public final static long ONE_HOUR = ONE_MINUTE * 60;
    public final static long HOURS = 24;

    public final static long ONE_DAY = ONE_HOUR * 24;
    public final static long ONE_WEEK = ONE_DAY * 7;
    private TimeUtils() {
    }

    public static Calendar getDatePart(Date date){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }

    public static long nowInUTC(){
        long now = System.currentTimeMillis();
        Calendar mCalendar = new GregorianCalendar();
        TimeZone mTimeZone = mCalendar.getTimeZone();
        int mGMTOffset = mTimeZone.getRawOffset();
        return now - mGMTOffset;
    }

    public static String timeDifferenceFromNow(long toMilliSecond,Context context) {
        long now = nowInUTC();
        long timeDifference = now - toMilliSecond;
        if (timeDifference > 0){
                if (timeDifference/ONE_WEEK >=1 ){
                    int weekBetween = (int) Math.ceil(timeDifference/ONE_WEEK);
                    return StringUtils.getByIdWithVars(context, R.string.me__time_diff_w,"%dw ago",weekBetween);
                }else if (timeDifference/ONE_DAY >= 1){
                    int dayBetween = (int) Math.ceil(timeDifference/ONE_DAY);
                    return StringUtils.getByIdWithVars(context, R.string.me__time_diff_d,"%dd ago",dayBetween);
                }else if (timeDifference/ONE_HOUR >= 1){
                    int hourBetween = (int) Math.ceil(timeDifference/ONE_HOUR);
                    return StringUtils.getByIdWithVars(context, R.string.me__time_diff_h,"%dh ago",hourBetween);
                }else if (timeDifference/ONE_MINUTE >= 1){
                    int minuteBetween = (int) Math.ceil(timeDifference/ONE_MINUTE);
                    return StringUtils.getByIdWithVars(context, R.string.me__time_diff_m,"%dm ago",minuteBetween);
                }
        }
            return StringUtils.getById(context,R.string.me__now_time,"now");
    }


    public static String toPrettyTime(Context context,String timeStr) {

        long timeMillis = Long.valueOf(timeStr.replaceAll("[^0-9]", ""));
        return timeDifferenceFromNow(timeMillis,context);
//        PrettyTime prettyTime = new PrettyTime();
//        prettyTime.setLocale(AppUtil.getCurrentLangLocale(context));
//        Pattern pattern = Pattern.compile("\\d+");
//        Matcher matcher = pattern.matcher(timeStr);
//        if (matcher.find())
//            return prettyTime.format(new Date(Long.valueOf(timeStr.replaceAll("[^0-9]", ""))));
//        else
//            return toPrettyTime(timeStr);
    }

    public static String toPrettyTime(String timeStr) {

        PrettyTime prettyTime = new PrettyTime();
        prettyTime.setLocale(Locale.ENGLISH);
        Pattern pattern = Pattern.compile("\\d+");
        Matcher matcher = pattern.matcher(timeStr);
        if (matcher.find())
            return prettyTime.format(new Date(Long.parseLong(matcher.group())));
        else
            return "";
    }

    /**
     * converts time (in milliseconds) to human-readable format
     * "<w> days, <x> hours, <y> minutes and (z) seconds"
     */
    public static String millisToLongDHMS(long duration) {
        if (duration > 0) {
            duration = new Date().getTime() - duration;
        }
        if (duration < 0) {
            duration = 0;
        }

        StringBuffer res = new StringBuffer();
        long temp = 0;
        if (duration >= ONE_SECOND) {
            temp = duration / ONE_DAY;
            if (temp > 0) {
                duration -= temp * ONE_DAY;
                res.append(temp).append(" day").append(temp > 1 ? "s" : "")
                        .append(duration >= ONE_MINUTE ? ", " : "");
            }

            temp = duration / ONE_HOUR;
            if (temp > 0) {
                duration -= temp * ONE_HOUR;
                res.append(temp).append(" hour").append(temp > 1 ? "s" : "")
                        .append(duration >= ONE_MINUTE ? ", " : "");
            }

            temp = duration / ONE_MINUTE;
            if (temp > 0) {
                duration -= temp * ONE_MINUTE;
                res.append(temp).append(" minute").append(temp > 1 ? "s" : "");
            }

            if (!res.toString().equals("") && duration >= ONE_SECOND) {
                res.append(" and ");
            }

            temp = duration / ONE_SECOND;
            if (temp > 0) {
                res.append(temp).append(" second").append(temp > 1 ? "s" : "");
            }
            res.append(" ago");
            return res.toString();
        } else {
            return "0 second ago";
        }
    }


    public static Calendar getCalendarFromString(String date1) {
        SimpleDateFormat form = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
//		    java.util.Date d1 = null;
        Calendar tdy1 = Calendar.getInstance();

        try {
//		        d1 = form.parse(date1);
            tdy1.setTime(form.parse(date1));
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }


        return tdy1;
    }

    public static Date getDateFromString(String date1) throws ParseException {
        SimpleDateFormat form = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//		    java.util.Date d1 = null;
        return form.parse(date1);
    }

    public static String getDateFromMilliSeconds(long milliSeconds) {
        SimpleDateFormat form = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return form.format(calendar.getTime());
    }

    public static String getRelativeTimeSpan(long fromMilliSeconds, long toMillisSeconds, Context context, boolean isLastSeen) {

        String timeSpan = "";
        long differenceInMillis = toMillisSeconds - fromMilliSeconds;
        long days = TimeUnit.MILLISECONDS.toDays(differenceInMillis);
        Date fromDate = new Date(fromMilliSeconds);
        String timezoneID = TimeZone.getDefault().getID();
        SimpleDateFormat dateFormat = new SimpleDateFormat(context.getString(R.string.general_time_format_with_a),LocalStorageHelper.getCurrentLocale(context));
        dateFormat.setTimeZone(TimeZone.getTimeZone(timezoneID));
        String defaultLastSeenFormat = "last seen %s";
        String defaultLastSeenTodayFormat = "last seen today at %s";
        if (days >= 7) {
            dateFormat.applyPattern("d/M/y");
            timeSpan = dateFormat.format(fromDate);
            if (isLastSeen){
                timeSpan = StringUtils.getByIdWithVars(context, R.string.chatroom__last_seen,defaultLastSeenFormat,timeSpan);
            }

            return timeSpan;
        } else if (days > 1) {
            dateFormat.applyPattern("EEEE");
            timeSpan = dateFormat.format(fromDate);
            if (isLastSeen){
                timeSpan = StringUtils.getByIdWithVars(context, R.string.chatroom__last_seen,defaultLastSeenFormat,timeSpan);
            }
            return timeSpan;
        } else if(days == 1) {
            timeSpan = StringUtils.getById(context,R.string.common__yesterday,"yesterday");
            if (isLastSeen){
                timeSpan = StringUtils.getByIdWithVars(context, R.string.chatroom__last_seen,defaultLastSeenFormat,timeSpan);
            }
            return timeSpan;
        }else{
            timeSpan = dateFormat.format(fromDate);
            if (isLastSeen){
                timeSpan = StringUtils.getByIdWithVars(context, R.string.chatroom__last_seen_today,defaultLastSeenTodayFormat,timeSpan);
            }
            return timeSpan;
        }
    }


    public static String getDateTimeFromMilliSeconds(long milliSeconds) {
        SimpleDateFormat form = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return form.format(calendar.getTime());
    }


    public static String getHourMinString(Context context, String dateTime) {
        Date date = new Date(Long.parseLong(dateTime) * 1000L);
//        date.setTime(Long.parseLong(dateTime));
        java.text.DateFormat outputFormat = new SimpleDateFormat(context.getString(R.string.general_time_format_with_a), context.getResources().getConfiguration().locale);

        return outputFormat.format(date);
    }


//    public static String stringToDate(String time) {
//        if (time.indexOf("(") != -1 && time.indexOf(")") != -1) {
//            return longToDate(Long.parseLong(time.substring(time.indexOf("(") + 1, time.indexOf(")"))));
//        } else {
//            SimpleDateFormat dateFormat =
//                    new SimpleDateFormat("yyyy-MM-dd");
//            Date myDate = null;
//            try {
//                myDate = dateFormat.parse(time);
//
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//            return TimeUtils.longToDate(myDate.getTime());
//        }
//    }

    public static String longToDate(long time) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time);
        String date = DateFormat.format("yyyy-MM-dd", cal).toString();
        return date;
    }

    public static String calendarToString(Calendar calendar, String format) {
        String dateString = DateFormat.format(format, calendar).toString();
        return dateString;
    }


    public static String getCurrentDateString() {
        String dateString = DateFormat.format("yyyy-MM-dd", new Date()).toString();
        return dateString;
    }

    public static String getCurrenDateTimeString() {
        return "" + new Date().getTime();
    }

    public static Long getCurrentDateTime() {
        return new Date().getTime();
    }

    public static Date stringToDate(String dateString) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = format.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }


    public static int diffMonths(String startDate, String endDate) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Calendar startCalendar = new GregorianCalendar();
        Calendar endCalendar = new GregorianCalendar();
        try {
            Date start = format.parse(startDate);
            Date end = format.parse(endDate);
            Log.i("diffMonths", "start: " + start);
            Log.i("diffMonths", "end: " + end);
            startCalendar.setTime(start);
            endCalendar.setTime(end);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        int diffYear = endCalendar.get(Calendar.YEAR) - startCalendar.get(Calendar.YEAR);
        int diffMonth = diffYear * 12 + endCalendar.get(Calendar.MONTH) - startCalendar.get(Calendar.MONTH);
        Log.i("diffMonths", "diffMonth: " + diffMonth);
        return diffMonth;
    }

    public static String diffDays(String startDate, String endDate) {
        Calendar startCalendar = getCalendarFromString(startDate);
        Calendar endCalendar = getCalendarFromString(endDate);

        long startTime = startCalendar.getTimeInMillis();
        long endTime = endCalendar.getTimeInMillis();
        long duration = endTime - startTime;

        int days = (int) TimeUnit.MILLISECONDS.toDays(duration);
        String durationText = " ( " + days / 365 + " years " + (days - days / 365 * 365) / 30 + " months " + " ) ";
        if (days / 365 == 0) {
            durationText = " ( " + (days - days / 365 * 365) / 30 + " months " + " ) ";
        }

        Log.w("diffDays", "duration = " + duration);
        Log.w("diffDays", "days = " + days);

        return durationText;
    }

    public static String getMonthFromDateString(String dateString) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(Long.parseLong(dateString));
        SimpleDateFormat month_date = new SimpleDateFormat("MMM");
        String monthName = month_date.format(calendar.getTime());
        return monthName;
    }

    public static ArrayList<Integer> calDateDiff(Date startDate, Date endDate) {

        long different = endDate.getTime() - startDate.getTime();
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;
        long weeksInMilli = daysInMilli * 7;

        long elapsedWeeks = different / weeksInMilli;
        different = different % weeksInMilli;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;
        ArrayList<Integer> calDiffList = new ArrayList<>();
        calDiffList.add((int) elapsedWeeks);
        calDiffList.add((int) elapsedDays);
        calDiffList.add((int) elapsedHours);
        return calDiffList;
    }

    public static String getYearFromDateString(String dateString) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(Long.parseLong(dateString));
        int yearName = calendar.get(Calendar.YEAR);
        return "" + yearName;
    }

    public static String currentDateString() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(new Date());
    }

    /**
     * Chatroom time stmap
     * @param timeinMilliSeccond
     * @return
     */
    public static String getDisplayDateFromLong(Context context,Long timeinMilliSeccond) {
        Calendar smsTime = Calendar.getInstance();
        smsTime.setTimeInMillis(timeinMilliSeccond);

        Calendar now = Calendar.getInstance();
        if (now.get(Calendar.DATE) == smsTime.get(Calendar.DATE)) {
            return new SimpleDateFormat("hh:mm", Locale.ENGLISH).format(new Date(timeinMilliSeccond));
        } else if (now.get(Calendar.DATE) - smsTime.get(Calendar.DATE) == 1) {
            return context.getString(R.string.common__yesterday);
        }else {
            return new SimpleDateFormat("MMM dd", Locale.ENGLISH).format(new Date(timeinMilliSeccond));
        }
    }

    public static boolean isSameDate(String dateStr1,String dateStr2) {
        Date date1 = new Date(Long.parseLong(dateStr1) * 1000L);
        Date date2 = new Date(Long.parseLong(dateStr2) * 1000L);
        return new SimpleDateFormat("MM dd, yyyy", Locale.ENGLISH).format(date1).equals(new SimpleDateFormat("MM dd, yyyy", Locale.ENGLISH).format(date2));
    }


    public static String getFullDateFromLong(Context context,String dateTime) {

        Date date = new Date(Long.parseLong(dateTime) * 1000L);
        Calendar smsTime = Calendar.getInstance();
        smsTime.setTime(date);

        Calendar now = Calendar.getInstance();
        if (now.get(Calendar.DATE) == smsTime.get(Calendar.DATE)) {
            return context.getString(R.string.common__today);
        } else if (now.get(Calendar.DATE) - smsTime.get(Calendar.DATE) == 1) {
            return context.getString(R.string.common__yesterday);
        } else {
            return new SimpleDateFormat(context.getString(R.string.general_date_format_month_year_with_week), AppUtil.getCurrentLangLocale(context)).format(date);
        }
    }
}
