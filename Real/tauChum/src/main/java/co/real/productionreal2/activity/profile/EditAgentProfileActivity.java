package co.real.productionreal2.activity.profile;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.nhaarman.listviewanimations.ArrayAdapter;
import com.nhaarman.listviewanimations.appearance.simple.AlphaInAnimationAdapter;
import com.nhaarman.listviewanimations.itemmanipulation.dragdrop.OnItemMovedListener;
import com.nhaarman.listviewanimations.itemmanipulation.swipedismiss.OnDismissCallback;
import com.nhaarman.listviewanimations.itemmanipulation.swipedismiss.undo.SimpleSwipeUndoAdapter;
import com.squareup.picasso.Picasso;
import com.yalantis.ucrop.UCrop;
import com.yalantis.ucrop.UCropActivity;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import co.real.productionreal2.Constants;
import co.real.productionreal2.CoreData;
import co.real.productionreal2.Navigation;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.AgentExpActivity;
import co.real.productionreal2.activity.AgentLangActivity;
import co.real.productionreal2.activity.AgentSpecActivity;
import co.real.productionreal2.activity.AgentTop5PastClosingActivity;
import co.real.productionreal2.activity.BaseActivity;
import co.real.productionreal2.adapter.PastClosingAdapter;
import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.callback.DialogCallback;
import co.real.productionreal2.callback.LangSpecDelCallback;
import co.real.productionreal2.common.LocalStorageHelper;
import co.real.productionreal2.dao.account.DBLogInfo;
import co.real.productionreal2.daomanager.DatabaseManager;
import co.real.productionreal2.service.AdminService;
import co.real.productionreal2.service.model.AgentExp;
import co.real.productionreal2.service.model.AgentLanguage;
import co.real.productionreal2.service.model.AgentPastClosing;
import co.real.productionreal2.service.model.AgentPastClosingPosition;
import co.real.productionreal2.service.model.AgentProfiles;
import co.real.productionreal2.service.model.AgentSpecialty;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.request.RequestAgentClosingAdd;
import co.real.productionreal2.service.model.request.RequestAgentExp;
import co.real.productionreal2.service.model.request.RequestAgentLanguage;
import co.real.productionreal2.service.model.request.RequestAgentPastClosing;
import co.real.productionreal2.service.model.request.RequestAgentProfile;
import co.real.productionreal2.service.model.request.RequestAgentSpecialty;
import co.real.productionreal2.service.model.request.RequestLicenseUpdate;
import co.real.productionreal2.service.model.response.ResponseAgentProfile;
import co.real.productionreal2.service.model.response.ResponseFileUpload;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;
import co.real.productionreal2.util.AppUtil;
import co.real.productionreal2.util.FileUtil;
import co.real.productionreal2.util.ImageUtil;
import co.real.productionreal2.view.Dialog;
import co.real.productionreal2.view.ExpandableHeightDynamicListView;
import lib.blurbehind.OnBlurCompleteListener;
import widget.RoundedImageView;

/**
 * Created by hohojo on 8/10/2015.
 */
public class EditAgentProfileActivity extends BaseActivity {
    private static final String TAG = "EditAgentProfileActivity";
    @InjectView(R.id.rootView)
    RelativeLayout rootView;
    @InjectView(R.id.licenseNoEditText)
    EditText licenseNoEditText;
    @InjectView(R.id.agentNameTextView)
    TextView agentNameTextView;
    @InjectView(R.id.agentImageView)
    RoundedImageView agentImageView;
    @InjectView(R.id.closeBtn)
    ImageButton closeBtn;
    @InjectView(R.id.exp_layout)
    LinearLayout expLayout;
    @InjectView(R.id.langResultLinearLayout)
    LinearLayout langResultLinearLayout;
    @InjectView(R.id.specResultLinearLayout)
    LinearLayout specResultLinearLayout;
    @InjectView(R.id.past_exp_dynamiclistview)
    ExpandableHeightDynamicListView pastClosingListView;

    private static final int REQUEST_SELECT_PICTURE = 10011;
    private static final String SAMPLE_CROPPED_IMAGE_NAME = "cropped_pic";
    private Uri mDestinationUri;

    private AgentProfiles currentAgentProfile;
    private VertHoriLinearLayout vertHoriLinearLayoutForLang;
    private VertHoriLinearLayout vertHoriLinearLayoutForSpec;
    private static final String REQUEST_AGENT_EXP_TAG = "RequestAgentExperience";

    private ResponseLoginSocial.Content agentContent;
    public static final int EXP = 0;
    public static final int LANG = 1;
    public static final int TOP_5_PAST_CLOSING = 2;
    public static final int SPECICALTY = 3;
    public static final int SELECT_PHOTO = 4;
    private ProgressDialog ringProgressDialog;
    private String imageFilePath;

    private ArrayAdapter<AgentPastClosing> pastClosingAdapter;

    private void getIntentInfo() {
        Intent intent = getIntent();
    }

    public static void startWithUri(@NonNull final Context context, @NonNull final Uri uri) {

                ImageUtil.getInstance().prepareBlurImageFromActivity((Activity) context, EditAgentProfileActivity.class, new OnBlurCompleteListener() {
                    @Override
                    public void onBlurComplete() {
                        Intent intent = new Intent(context, EditAgentProfileActivity.class);
                        intent.setData(uri);
                        Navigation.presentIntent((Activity) context,intent);
                    }
                });

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_agent_profile);
        getIntentInfo();
        currentAgentProfile = getCurrentAgentProfile();

        ButterKnife.inject(this);
        initView();
        editAgentInitData();

        Uri uri = getIntent().getData();
        if (uri != null)
            updateProfileImg(uri);
    }

    private void updateProfileImg(final Uri resultUri) {
        imageFilePath = FileUtil.getRealPathFromURI(this, resultUri);
        Picasso.with(this)
                .load(new File(imageFilePath))
                .error(R.drawable.com_facebook_profile_picture_blank_square)
                .into(agentImageView);
        if (ringProgressDialog != null && ringProgressDialog.isShowing())
            return;
        //show progress dialog
        ringProgressDialog.setTitle(getResources().getString(R.string.alert__Initializing));
        ringProgressDialog.setMessage(getResources().getString(R.string.common__uploading));
        ringProgressDialog.show();
        new UploadFileTask().execute("");
        Log.w("imageFilePath", "UCrop = ok " + resultUri);
    }

    private void editAgentInitData() {
        recreateLicenseNoLayout();
        recreateExpLayout();
        recreateLangLayout();
        recreateSpeclayout();
        //Past closing layout created in initView

    }

    private void initPastClosingView() {
        pastClosingAdapter = new PastClosingAdapter(this, closingRemoveBtnClickListener, agentContent.systemSetting.propertyTypeList);
        if (currentAgentProfile.agentPastClosingList != null && currentAgentProfile.agentPastClosingList.size() > 0) {
            for (AgentPastClosing agentPastClosing : currentAgentProfile.agentPastClosingList) {
                pastClosingAdapter.add(agentPastClosing);
            }
        }
        SimpleSwipeUndoAdapter simpleSwipeUndoAdapter = new SimpleSwipeUndoAdapter(pastClosingAdapter, this,
                new PastClosingOnDismissCallback(pastClosingAdapter));
        AlphaInAnimationAdapter animAdapter = new AlphaInAnimationAdapter(simpleSwipeUndoAdapter);
        animAdapter.setAbsListView(pastClosingListView);
        assert animAdapter.getViewAnimator() != null;
        animAdapter.getViewAnimator().setInitialDelayMillis(ExpandableHeightDynamicListView.ANIM_INITIAL_DELAY_MILLIS);
        pastClosingListView.setAdapter(animAdapter);

        /* Enable drag and drop functionality */
        pastClosingListView.enableDragAndDrop();
//        pastClosingListView.setDraggableManager(new TouchViewDraggableManager(R.id.list_row_draganddrop_touchview2));
        pastClosingListView.setOnItemMovedListener(new PastClosingOnItemMovedListener(pastClosingAdapter));
        pastClosingListView.setOnItemLongClickListener(new PastClosingOnItemLongClickListener(pastClosingListView));

        pastClosingListView.setAdapter(pastClosingAdapter);
        pastClosingListView.setExpanded(true);

    }

    private void initView() {

        agentContent = CoreData.getUserContent(this);
        agentNameTextView.setText(agentContent.memName);

        initPastClosingView();

        Picasso.with(this)
                .load(agentContent.photoUrl)
                .error(R.drawable.default_profile)
                .placeholder(R.drawable.default_profile)
                .into(agentImageView);
        licenseNoEditText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    Dialog.updateProfileDialog(EditAgentProfileActivity.this, true,
                            getString(R.string.common__my_profile),
                            getString(R.string.create_profile__update_license_number), new DialogCallback() {
                                @Override
                                public void yes() {
                                    RequestLicenseUpdate requestUpdate = new RequestLicenseUpdate(
                                            agentContent.memId,
                                            LocalStorageHelper.getAccessToken(getBaseContext()));

                                    requestUpdate.licenseNumber = licenseNoEditText.getText().toString();
                                    requestUpdate.callUpdateLicNoApi(EditAgentProfileActivity.this, new ApiCallback() {
                                        @Override
                                        public void success(ApiResponse apiResponse) {
                                            callAgentProfileApiAndUpdateDbLogInfoAndUpdateUI();
                                        }

                                        @Override
                                        public void failure(String errorMsg) {
                                            Dialog.normalDialog(EditAgentProfileActivity.this, errorMsg).show();
                                        }
                                    });
                                }
                            }).show();
                    return true;
                }
                return false;
            }
        });
//        licenseNoEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                Log.w(TAG, "onFocusChange: " + hasFocus);
//                if (!hasFocus) {
//                    Dialog.feedbackDialog(EditAgentProfileActivity.this, true,
//                            getString(R.string.common__my_profile),
//                            getString(R.string.alert_confirm_update_license_number), new DialogCallback() {
//                                @Override
//                                public void yes() {
//                                    RequestLicenseUpdate requestUpdate = new RequestLicenseUpdate(
//                                            agentContent.memId,
//                                            LocalStorageHelper.getAccessToken(getBaseContext()));
//
//                                    requestUpdate.licenseNumber = licenseNoEditText.getText().toString();
//                                    requestUpdate.callUpdateLicNoApi(EditAgentProfileActivity.this, new ApiCallback() {
//                                        @Override
//                                        public void success(ApiResponse apiResponse) {
//                                            callAgentProfileApiAndUpdateDbLogInfoAndUpdateUI();
//                                        }
//
//                                        @Override
//                                        public void failure(String errorMsg) {
//                                            Dialog.normalDialog(EditAgentProfileActivity.this, errorMsg).show();
//                                        }
//                                    });
//                                }
//                            }).show();
//                }
//            }
//        });

    }


    private void appendAgentExpView(final AgentExp agentExp, int tagNo) {
        final int tagNum = tagNo;
        LayoutInflater layoutInflater = getLayoutInflater();
        View view = layoutInflater.inflate(R.layout.agent_exp_item, null);
        view.setTag(REQUEST_AGENT_EXP_TAG + tagNum);

        TextView companyTextView = ButterKnife.findById(view, R.id.companyTextView);
        TextView titleTextView = ButterKnife.findById(view, R.id.titleTextView);
        TextView periodTextView = ButterKnife.findById(view, R.id.periodTextView);
        ImageButton removeBtn = ButterKnife.findById(view, R.id.removeExpBtn);
        ImageButton expEditBtn = ButterKnife.findById(view, R.id.expEditBtn);
        companyTextView.setTextColor(Color.WHITE);
        titleTextView.setTextColor(Color.WHITE);
        periodTextView.setTextColor(Color.WHITE);

        companyTextView.setText(agentExp.company);
        titleTextView.setText(agentExp.title);
        periodTextView.setText(agentExp.getExpPeriodText(this));

        removeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dialog.confirmDelDialog(EditAgentProfileActivity.this, currentAgentProfile.agentExpList.get(tagNum).company,
                        EXP, new DialogCallback() {
                            @Override
                            public void yes() {
                                RequestAgentExp delExp = new RequestAgentExp(
                                        agentContent.memId,
                                        LocalStorageHelper.getAccessToken(getBaseContext()));
                                delExp.agentExpList.add(currentAgentProfile.agentExpList.get(tagNum));
                                delExp.callDelExpApi(EditAgentProfileActivity.this,
                                        new ApiCallback() {
                                            @Override
                                            public void success(ApiResponse apiResponse) {
                                                callAgentProfileApiAndUpdateDbLogInfoAndUpdateUI();
                                            }

                                            @Override
                                            public void failure(String errorMsg) {
                                                AppUtil.showToast(EditAgentProfileActivity.this, getString(R.string.error_message__please_try_again_later) + " " + errorMsg);
                                            }
                                        });
                            }
                        }).show();

            }
        });

        expEditBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(EditAgentProfileActivity.this, AgentExpActivity.class);
                intent.putExtra(Constants.EXTRA_AGENT_EXP_IS_EDIT, true);
                intent.putExtra(Constants.EXTRA_AGENT_EXP_EDIT_TAG_NO, tagNum);
                intent.putExtra(Constants.EXTRA_AGENT_EXP_EDIT_CONTENT, agentExp);
                Navigation.pushIntentForResult(EditAgentProfileActivity.this,intent,EXP);
            }
        });

        expLayout.addView(view);
    }

    private void recreateLicenseNoLayout() {
        licenseNoEditText.setText(currentAgentProfile.agentLicenseNumber);
    }

    private void recreateExpLayout() {
        expLayout.removeAllViews();
        for (int i = 0; i < currentAgentProfile.agentExpList.size(); i++) {
            appendAgentExpView(currentAgentProfile.agentExpList.get(i), i);
        }
    }

    private void recreateLangLayout() {

        langResultLinearLayout.post(new Runnable() {
            @Override
            public void run() {
                final ArrayList<String> nativeNameList = new ArrayList<>();
                for (int i = 0; i < currentAgentProfile.agentLanguageList.size(); i++) {
                    for (int j = 0; j < agentContent.systemSetting.spokenLangList.size(); j++) {
                        if (agentContent.systemSetting.spokenLangList.get(j).index ==
                                currentAgentProfile.agentLanguageList.get(i).langIndex) {
                            nativeNameList.add(agentContent.systemSetting.spokenLangList.get(j).nativeName);
                        }
                    }

                }
                vertHoriLinearLayoutForLang = new VertHoriLinearLayout(EditAgentProfileActivity.this, langResultLinearLayout,
                        nativeNameList,
                        langResultLinearLayout.getMeasuredWidth(), VertHoriLinearLayout.PROFILE_LANG,
                        new LangSpecDelCallback() {
                            @Override
                            public void onLangSpecDel(final int tagNo, int type) {
                                Dialog.confirmDelDialog(EditAgentProfileActivity.this, nativeNameList.get(tagNo), LANG, new DialogCallback() {
                                    @Override
                                    public void yes() {
                                        RequestAgentLanguage delLang = new RequestAgentLanguage(
                                                agentContent.memId,
                                                LocalStorageHelper.getAccessToken(getBaseContext()));
                                        delLang.agentLangList.add(currentAgentProfile.agentLanguageList.get(tagNo));
                                        delLang.callDelLangApi(EditAgentProfileActivity.this, new ApiCallback() {
                                            @Override
                                            public void success(ApiResponse apiResponse) {
                                                callAgentProfileApiAndUpdateDbLogInfoAndUpdateUI();
                                            }

                                            @Override
                                            public void failure(String errorMsg) {
                                                AppUtil.showToast(EditAgentProfileActivity.this, getString(R.string.error_message__please_try_again_later));
                                            }
                                        });
                                    }
                                }).show();
                            }
                        });
            }
        });

    }

    private void recreateSpeclayout() {
        specResultLinearLayout.post(new Runnable() {
            @Override
            public void run() {
                vertHoriLinearLayoutForSpec = new VertHoriLinearLayout(EditAgentProfileActivity.this, specResultLinearLayout,
                        VertHoriLinearLayout.agentSpecListToStringList((ArrayList<AgentSpecialty>) currentAgentProfile.agentSpecialtyList),
                        specResultLinearLayout.getMeasuredWidth(), VertHoriLinearLayout.PROFILE_SPEC,
                        new LangSpecDelCallback() {
                            @Override
                            public void onLangSpecDel(final int tagNo, int type) {
                                Dialog.confirmDelDialog(EditAgentProfileActivity.this, currentAgentProfile.agentSpecialtyList.get(tagNo).specialty, SPECICALTY, new DialogCallback() {
                                    @Override
                                    public void yes() {
                                        RequestAgentSpecialty delSpec = new RequestAgentSpecialty(
                                                agentContent.memId,
                                                LocalStorageHelper.getAccessToken(getBaseContext()));
                                        delSpec.agentSpecialtyList.add(currentAgentProfile.agentSpecialtyList.get(tagNo));
                                        delSpec.callDelSpecApi(EditAgentProfileActivity.this, new ApiCallback() {
                                            @Override
                                            public void success(ApiResponse apiResponse) {
                                                callAgentProfileApiAndUpdateDbLogInfoAndUpdateUI();
                                            }

                                            @Override
                                            public void failure(String errorMsg) {
                                                AppUtil.showToast(EditAgentProfileActivity.this, getString(R.string.error_message__please_try_again_later));
                                            }
                                        });
                                    }
                                }).show();
                            }
                        });
            }
        });

    }

    private final void intentToPickImage() {
//        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
//        photoPickerIntent.setType("image/*");
//        startActivityForResult(photoPickerIntent, SELECT_PHOTO);
        pickFromGallery();
    }

    @OnClick(R.id.agentImageView)
    public void agentImageViewClick(View view) {
        //todo select image and call api
        intentToPickImage();

    }

    @OnClick(R.id.addNewExp)
    public void addNewExp(View view) {
        Intent intent = new Intent(this, AgentExpActivity.class);
        Navigation.pushIntentForResult(this,intent,EXP);
    }

    @OnClick(R.id.addNewLang)
    public void addNewLang(View view) {
        Intent intent = new Intent(this, AgentLangActivity.class);
        int[] selLangIndex = new int[currentAgentProfile.agentLanguageList.size()];
        for (int i = 0; i < currentAgentProfile.agentLanguageList.size(); i++) {
            selLangIndex[i] = currentAgentProfile.agentLanguageList.get(i).langIndex;
        }
        intent.putParcelableArrayListExtra(Constants.EXTRA_SPOKEN_LANG, (ArrayList<? extends Parcelable>) agentContent.systemSetting.spokenLangList);
        intent.putParcelableArrayListExtra(Constants.EXTRA_AGENT_LANG, (ArrayList<? extends Parcelable>) currentAgentProfile.agentLanguageList);
        Navigation.pushIntentForResult(this,intent,LANG);
    }

    @OnClick(R.id.addNewSpec)
    public void addNewSpec(View view) {
        Intent intent = new Intent(this, AgentSpecActivity.class);
        intent.putExtra(Constants.EXTRA_AGENT_SPEC_LIST, (ArrayList<? extends Parcelable>) currentAgentProfile.agentSpecialtyList);
        Navigation.pushIntentForResult(this,intent,SPECICALTY);
    }

    @OnClick(R.id.addNewTop5Closing)
    public void addNewTop5Closing(View view) {
        if (currentAgentProfile.agentPastClosingList.size() >= 5) {
            Dialog.normalDialog(this, getResources().getString(R.string.edit_profile__max_top_5)).show();
        } else {
            Intent intent = new Intent(this, AgentTop5PastClosingActivity.class);
            intent.putExtra(Constants.EXTRA_AGENT_PROPERTY_TYPE_LIST, (ArrayList<? extends Parcelable>) agentContent.systemSetting.propertyTypeList);
            Navigation.pushIntentForResult(this,intent,TOP_5_PAST_CLOSING);
        }


    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////for moveable listview //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private PastClosingAdapter.ClosingRemoveBtnClickListener closingRemoveBtnClickListener = new PastClosingAdapter.ClosingRemoveBtnClickListener() {
        @Override
        public void onClosingRemove(final int position) {
            Dialog.confirmDelDialog(EditAgentProfileActivity.this, currentAgentProfile.agentPastClosingList.get(position).address, TOP_5_PAST_CLOSING, new DialogCallback() {
                @Override
                public void yes() {
                    RequestAgentClosingAdd addPastClose = new RequestAgentClosingAdd(
                            agentContent.memId,
                            LocalStorageHelper.getAccessToken(getBaseContext()));

                    addPastClose.agentPastCloseList.add(currentAgentProfile.agentPastClosingList.get(position));
                    addPastClose.callDelPastClosingApi(EditAgentProfileActivity.this, new ApiCallback() {
                        @Override
                        public void success(ApiResponse apiResponse) {
                            callAgentProfileApiAndUpdateDbLogInfoAndUpdateUI();
//                            pastClosingAdapter.remove(position);
                        }

                        @Override
                        public void failure(String errorMsg) {
                            AppUtil.showToast(EditAgentProfileActivity.this, getString(R.string.error_message__please_try_again_later));
                        }
                    });
                }
            }).show();

        }
    };

    ///////////////////////////////////////////////// PastClosingOnDismissCallback /////////////////////////////////////////////////
    private class PastClosingOnDismissCallback implements OnDismissCallback {
        private final ArrayAdapter<AgentPastClosing> agentPastClosingAdapter;

        PastClosingOnDismissCallback(final ArrayAdapter<AgentPastClosing> adapter) {
            agentPastClosingAdapter = adapter;
        }

        @Override
        public void onDismiss(ViewGroup viewGroup, int[] reverseSortedPositions) {
            for (int position : reverseSortedPositions) {
                agentPastClosingAdapter.remove(position);
            }
        }
    }
    ///////////////////////////////////////////////// PastClosingOnDismissCallback /////////////////////////////////////////////////

    ///////////////////////////////////////////////// PastClosingOnItemMovedListener /////////////////////////////////////////////////
    private class PastClosingOnItemMovedListener implements OnItemMovedListener {
        private final ArrayAdapter<AgentPastClosing> agentPastClosingAdapter;

        private Toast mToast;

        PastClosingOnItemMovedListener(final ArrayAdapter<AgentPastClosing> adapter) {
            agentPastClosingAdapter = adapter;
        }

        @Override
        public void onItemMoved(final int originalPosition, final int newPosition) {

            Log.d("test dryanic", "onItemMoved " + originalPosition + " , " + newPosition);


            currentAgentProfile.agentPastClosingList = agentPastClosingAdapter.getItems();

            List<AgentPastClosingPosition> apiPastClosingList = new ArrayList<AgentPastClosingPosition>();

            int i = 0;
            for (AgentPastClosing adsfasd : currentAgentProfile.agentPastClosingList) {

                AgentPastClosingPosition tee = new AgentPastClosingPosition();
                tee.id = adsfasd.id;
                tee.position = i;
                apiPastClosingList.add(tee);
                i++;
            }


            RequestAgentPastClosing updateClosing = new RequestAgentPastClosing(
                    agentContent.memId,
                    LocalStorageHelper.getAccessToken(getBaseContext()));
            updateClosing.agentPastCloseList.addAll(apiPastClosingList);

            updateClosing.callUpdatePosPastClosingApi(EditAgentProfileActivity.this, updateClosing, new ApiCallback() {
                @Override
                public void success(ApiResponse apiResponse) {
//                    Toast.makeText(EditAgentProfileActivity.this, "callUpdatePosPastClosingApi success", Toast.LENGTH_LONG).show();
                }

                @Override
                public void failure(String errorMsg) {
                    AppUtil.showToast(EditAgentProfileActivity.this, getString(R.string.error_message__please_try_again_later));
                }
            });
        }
    }
    ///////////////////////////////////////////////// PastClosingOnItemMovedListener /////////////////////////////////////////////////

    ///////////////////////////////////////////////// PastClosingOnItemLongClickListener /////////////////////////////////////////////////
    private class PastClosingOnItemLongClickListener implements AdapterView.OnItemLongClickListener {
        private final ExpandableHeightDynamicListView listView;

        PastClosingOnItemLongClickListener(final ExpandableHeightDynamicListView listView) {
            this.listView = listView;
        }

        @Override
        public boolean onItemLongClick(final AdapterView<?> parent, final View view, final int position, final long id) {
            if (listView != null) {

                Log.d("test dryanic", "start drag");
                listView.markDragging();
                listView.startDragging(position - listView.getHeaderViewsCount());
            }
            return true;
        }
    }
    ///////////////////////////////////////////////// PastClosingOnItemLongClickListener /////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////for moveable listview //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @OnClick(R.id.closeBtn)
    public void closeBtnClick(View view) {
        finish();
    }


    private void callAgentProfileApiAndUpdateDbLogInfoAndUpdateUI() {
        RequestAgentProfile request = new RequestAgentProfile(
                agentContent.memId,
                LocalStorageHelper.getAccessToken(getBaseContext()),10);
        request.getAgentProfile(EditAgentProfileActivity.this, request, new ApiCallback() {
            @Override
            public void success(ApiResponse apiResponse) {
                Gson gson = new Gson();
                ResponseAgentProfile agentProfileResponse = gson.fromJson(apiResponse.jsonContent, ResponseAgentProfile.class);
                AgentProfiles agentProfile = agentProfileResponse.content.profile;

                DatabaseManager databaseManager = DatabaseManager.getInstance(EditAgentProfileActivity.this);
                DBLogInfo dbLogInfo = databaseManager.getLogInfo();

                ResponseLoginSocial.Content userContent = new Gson().fromJson(dbLogInfo.getLogInfo(),
                        ResponseLoginSocial.Content.class);
                userContent.agentProfile = agentProfile;
                CoreData.setUserContent(userContent);
                dbLogInfo.setLogInfo(gson.toJson(userContent));

                databaseManager.updateLogInfo(dbLogInfo);
                currentAgentProfile = getCurrentAgentProfile();
                recreateLicenseNoLayout();
                recreateExpLayout();
                recreateLangLayout();
                recreateSpeclayout();
                resetPastClosingAdapter();
//                initPastClosingView();
            }

            @Override
            public void failure(String errorMsg) {
                AppUtil.showToast(EditAgentProfileActivity.this, getString(R.string.error_message__please_try_again_later));
            }
        });
    }

    private AgentProfiles getCurrentAgentProfile() {
        ResponseLoginSocial.Content content = CoreData.getUserContent(this);
        return content.agentProfile;
    }

    // add experience api
    private void callAddExpApi(AgentExp agentExp) {
        RequestAgentExp requestAgentExp = new RequestAgentExp(
                agentContent.memId,
                LocalStorageHelper.getAccessToken(getBaseContext()));
        requestAgentExp.agentExpList.add(agentExp);
        requestAgentExp.callAddExpApi(this, new ApiCallback() {
            @Override
            public void success(ApiResponse apiResponse) {
                callAgentProfileApiAndUpdateDbLogInfoAndUpdateUI();
            }

            @Override
            public void failure(String errorMsg) {
                AppUtil.showToast(EditAgentProfileActivity.this, getString(R.string.error_message__please_try_again_later));
            }
        });
    }

    private void resetPastClosingAdapter() {
        pastClosingAdapter.clear();
        for (AgentPastClosing agentPastClosing : currentAgentProfile.agentPastClosingList) {
            pastClosingAdapter.add(agentPastClosing);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == EXP) {
                final AgentExp agentExp = data.getParcelableExtra(Constants.EXTRA_REQUEST_AGENT_EXP);
                // delete and add data
                if (data.getBooleanExtra(Constants.EXTRA_AGENT_EXP_IS_EDIT, false)) {
                    int tagNo = data.getIntExtra(Constants.EXTRA_AGENT_EXP_EDIT_TAG_NO, 0);
                    RequestAgentExp delExp = new RequestAgentExp(
                            agentContent.memId,
                            LocalStorageHelper.getAccessToken(getBaseContext()));
                    delExp.agentExpList.add(currentAgentProfile.agentExpList.get(tagNo));
                    delExp.callDelExpApi(this, new ApiCallback() {
                        @Override
                        public void success(ApiResponse apiResponse) {
                            callAddExpApi(agentExp);
                        }

                        @Override
                        public void failure(String errorMsg) {
                            AppUtil.showToast(EditAgentProfileActivity.this, getString(R.string.error_message__please_try_again_later));
                        }

                    });
                } else {
                    callAddExpApi(agentExp);
                }
            } else if (requestCode == LANG) {
                // add data
//                List<AgentLanguage> agentLangList = data.getParcelableArrayListExtra(Constants.EXTRA_UPDATED_AGENT_LANG);
                ArrayList<AgentLanguage> agentLangDelList = data.getParcelableArrayListExtra(Constants.EXTRA_UPDATED_AGENT_LANG_DEL_LIST);
                ArrayList<AgentLanguage> agentLangAddList = data.getParcelableArrayListExtra(Constants.EXTRA_UPDATED_AGENT_LANG_ADD_LIST);
//                ArrayList<String> nativeNameList = data.getStringArrayListExtra(Constants.EXTRA_UPDATED_AGENT_LANG_NATIVE_NAME);

                if (agentLangDelList != null) {
                    RequestAgentLanguage delLang = new RequestAgentLanguage(
                            agentContent.memId,
                            LocalStorageHelper.getAccessToken(getBaseContext()));
                    delLang.agentLangList.addAll(agentLangDelList);
                    delLang.callDelLangApi(this, new ApiCallback() {
                        @Override
                        public void success(ApiResponse apiResponse) {
                            callAgentProfileApiAndUpdateDbLogInfoAndUpdateUI();
                        }

                        @Override
                        public void failure(String errorMsg) {
                            AppUtil.showToast(EditAgentProfileActivity.this, getString(R.string.error_message__please_try_again_later));
                        }
                    });
                }
                if (agentLangAddList != null) {
                    RequestAgentLanguage addLang = new RequestAgentLanguage(
                            agentContent.memId,
                            LocalStorageHelper.getAccessToken(getBaseContext()));
                    addLang.agentLangList.addAll(agentLangAddList);
                    addLang.callAddLangApi(this, new ApiCallback() {
                        @Override
                        public void success(ApiResponse apiResponse) {
                            callAgentProfileApiAndUpdateDbLogInfoAndUpdateUI();
                        }

                        @Override
                        public void failure(String errorMsg) {
                            AppUtil.showToast(EditAgentProfileActivity.this, getString(R.string.error_message__please_try_again_later));
                        }
                    });
                }
            } else if (requestCode == SPECICALTY) {
//                List<AgentSpecialty> updateAgentSpecList = data.getParcelableArrayListExtra(Constants.EXTRA_AGENT_SPEC_LIST);
                final List<AgentSpecialty> agentSpecAddList = data.getParcelableArrayListExtra(Constants.EXTRA_AGENT_SPEC_ADD_LIST);
                List<AgentSpecialty> agentSpecDelList = data.getParcelableArrayListExtra(Constants.EXTRA_AGENT_SPEC_DEL_LIST);
                if (agentSpecDelList != null) {
                    RequestAgentSpecialty delSpec = new RequestAgentSpecialty(
                            agentContent.memId,
                            LocalStorageHelper.getAccessToken(getBaseContext()));
                    delSpec.agentSpecialtyList.addAll(agentSpecDelList);
                    delSpec.callDelSpecApi(this, new ApiCallback() {
                        @Override
                        public void success(ApiResponse apiResponse) {
//                            callAgentProfileApiAndUpdateDbLogInfoAndUpdateUI();
                                RequestAgentSpecialty addSpec = new RequestAgentSpecialty(
                                        agentContent.memId,
                                        LocalStorageHelper.getAccessToken(getBaseContext()));
                                addSpec.agentSpecialtyList.addAll(agentSpecAddList);
                                addSpec.callAddSpecApi(EditAgentProfileActivity.this, new ApiCallback() {
                                    @Override
                                    public void success(ApiResponse apiResponse) {
                                        callAgentProfileApiAndUpdateDbLogInfoAndUpdateUI();
                                    }

                                    @Override
                                    public void failure(String errorMsg) {
                                        AppUtil.showToast(EditAgentProfileActivity.this, getString(R.string.error_message__please_try_again_later));
                                    }
                                });
                        }
                        @Override
                        public void failure(String errorMsg) {
                            AppUtil.showToast(EditAgentProfileActivity.this, getString(R.string.error_message__please_try_again_later));
                        }
                    });
                }

            } else if (requestCode == TOP_5_PAST_CLOSING) {
                final AgentPastClosing agentPastClosing = data.getParcelableExtra(Constants.EXTRA_AGENT_PAST_CLOSING);

                RequestAgentClosingAdd addPastClose = new RequestAgentClosingAdd(
                        agentContent.memId,
                        LocalStorageHelper.getAccessToken(getBaseContext()));
                addPastClose.agentPastCloseList.add(agentPastClosing);
                addPastClose.callAddPastClosingApi(this, new ApiCallback() {
                    @Override
                    public void success(ApiResponse apiResponse) {
                        callAgentProfileApiAndUpdateDbLogInfoAndUpdateUI();
                    }

                    @Override
                    public void failure(String errorMsg) {
                        AppUtil.showToast(EditAgentProfileActivity.this, getString(R.string.error_message__please_try_again_later));
                    }
                });
//            } else if (resultCode == RESULT_OK && requestCode == CropActivity.REQUEST_CROP) {
//                final Uri resultUri = UCrop.getOutput(data);
//                imageFilePath = FileUtil.getRealPathFromURI(this, resultUri);
//                Log.w("imageFilePath", "imageFilePath = " + imageFilePath);
//                Picasso.with(this)
//                        .load(new File(imageFilePath))
//                        .error(R.drawable.com_facebook_profile_picture_blank_square)
//                        .into(agentImageView);
//                if (ringProgressDialog != null && ringProgressDialog.isShowing())
//                    return;
//                //show progress dialog
//                ringProgressDialog = ProgressDialog.show(this, getResources().getString(R.string.please_wait),
//                        getResources().getString(R.string.uploading_data));
//                new UploadFileTask().execute("");
//                Log.w("imageFilePath", "UCrop = ok " + resultUri);
            } else if (resultCode == RESULT_OK && requestCode == REQUEST_SELECT_PICTURE) {
                try {
                    final Uri selectedUri = data.getData();
                    startCropActivity(selectedUri);
                    Log.w("imageFilePath", "UCrop = ok request crop:  " + selectedUri);
                } catch (Exception e) {
                    Toast.makeText(this, R.string.chatroom_error_message__incompatible_error, Toast.LENGTH_SHORT).show(); //TODO: general error for some not supporting image
                }
            } else if (resultCode == RESULT_OK && requestCode == UCrop.REQUEST_CROP) {
                final Uri resultUri = UCrop.getOutput(data);
                imageFilePath = FileUtil.getRealPathFromURI(this, resultUri);
                Log.w("imageFilePath", "imageFilePath = " + imageFilePath);
                Picasso.with(this)
                        .load(new File(imageFilePath))
                        .error(R.drawable.com_facebook_profile_picture_blank_square)
                        .into(agentImageView);
                if (ringProgressDialog != null && ringProgressDialog.isShowing())
                    return;
                //show progress dialog
                ringProgressDialog = ProgressDialog.show(this, "",
                        getResources().getString(R.string.alert__Initializing));
                new UploadFileTask().execute("");
                Log.w("imageFilePath", "UCrop = ok " + resultUri);
            } else if (resultCode == UCrop.RESULT_ERROR) {
                Log.w("imageFilePath", "UCrop = err");
                final Throwable cropError = UCrop.getError(data);
            }

        }
    }


    private void pickFromGallery() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN // Permission was added in API Level 16
                && ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    REQUEST_STORAGE_READ_ACCESS_PERMISSION);
        } else {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            startActivityForResult(Intent.createChooser(intent, getResources().getString(R.string.common__select)), REQUEST_SELECT_PICTURE);
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_STORAGE_READ_ACCESS_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    pickFromGallery();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    /**
     * In most cases you need only to set crop aspect ration and max size for resulting image.
     *
     * @param uCrop - ucrop builder instance
     * @return - ucrop builder instance
     */
    private UCrop basisConfig(@NonNull UCrop uCrop) {

        uCrop = uCrop.useSourceImageAspectRatio();
        uCrop = uCrop.withAspectRatio(1, 1);
//        try {
//            float ratioX = Constants.resizeImageWidth;
//            float ratioY = Constants.resizeImageWidth;
//            if (ratioX > 0 && ratioY > 0) {
//                uCrop = uCrop.withAspectRatio(ratioX, ratioY);
//            }
//        } catch (NumberFormatException e) {
//        }

        return uCrop;
    }


    /**
     * Sometimes you want to adjust more options, it's done via {@link com.yalantis.ucrop.UCrop.Options} class.
     *
     * @param uCrop - ucrop builder instance
     * @return - ucrop builder instance
     */
    private UCrop advancedConfig(@NonNull UCrop uCrop) {
        UCrop.Options options = new UCrop.Options();
        options.setCompressionFormat(Bitmap.CompressFormat.JPEG);
//        options.setCompressionQuality(100);


        // If you want to configure how gestures work for all UCropActivity tabs
        options.setAllowedGestures(UCropActivity.SCALE, UCropActivity.ROTATE, UCropActivity.ALL);


        /*
        This sets max size for bitmap that will be decoded from source Uri.
        More size - more memory allocation, default implementation uses screen diagonal.
        options.setMaxBitmapSize(640);
        * */

        //Tune everything (ﾉ◕ヮ◕)ﾉ*:･ﾟ✧
//        options.setMaxScaleMultiplier(5);
//        options.setImageToCropBoundsAnimDuration(666);
//        options.setDimmedLayerColor(Color.GRAY);
        options.setOvalDimmedLayer(true);
        options.setShowCropFrame(false);
        options.setShowCropGrid(false);
//        options.setCropGridStrokeWidth(20);
//        options.setCropGridColor(Color.WHITE);
//        options.setCropGridColumnCount(2);
//        options.setCropGridRowCount(1);
        // Color palette
        options.setToolbarColor(ContextCompat.getColor(this, R.color.tab_bg));
        options.setStatusBarColor(ContextCompat.getColor(this, R.color.tab_bg));
        options.setActiveWidgetColor(ContextCompat.getColor(this, R.color.holo_blue));
        options.setToolbarWidgetColor(ContextCompat.getColor(this, R.color.white));

        return uCrop.withOptions(options);
    }

    private void startCropActivity(@NonNull Uri uri) {
        mDestinationUri = Uri.fromFile(new File(getCacheDir(), SAMPLE_CROPPED_IMAGE_NAME + System.currentTimeMillis() + ".jpeg"));
        UCrop uCrop = UCrop.of(uri, mDestinationUri);

        uCrop = basisConfig(uCrop);
        uCrop = advancedConfig(uCrop);

        uCrop.start(this);
    }


    private String callUploadFileApi(DefaultHttpClient httpClient, File photoFile) {

        HttpPost httppost = new HttpPost(AdminService.WEB_SERVICE_BASE_URL + "/Admin.asmx/FileUpload");
        MultipartEntity multipartEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);

        try {
            multipartEntity.addPart("MemberID", new StringBody("" + agentContent.agentProfile.memberID));
            multipartEntity.addPart("AccessToken", new StringBody(LocalStorageHelper.getAccessToken(getBaseContext())));
            multipartEntity.addPart("ListingID", new StringBody("0"));
            multipartEntity.addPart("Position", new StringBody("0"));
            multipartEntity.addPart("FileExt", new StringBody(FileUtil.getFileExt(photoFile.getAbsolutePath())));
            multipartEntity.addPart("FileType", new StringBody("2"));
            multipartEntity.addPart("attachment", new FileBody(photoFile));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        httppost.setEntity(multipartEntity);
        try {
            HttpResponse httpResponse = httpClient.execute(httppost);
            InputStream is = httpResponse.getEntity().getContent();
            String responseString = FileUtil.convertStreamToString(is);
            Document doc = FileUtil.getDomElement(responseString);
            doc.getDocumentElement().normalize();
            NodeList nodeList = doc.getElementsByTagName("string");
            Node node = nodeList.item(0);
            String json = FileUtil.nodeToString(node.getFirstChild());
            if (json != null) {
                ResponseFileUpload responseFileUpload = new Gson().fromJson(json, ResponseFileUpload.class);
                if (responseFileUpload.content == null)
                    return null;
                if (responseFileUpload.content.photoUrl == null)
                    return null;
                return responseFileUpload.content.photoUrl;
            }

            return null;

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * upload task
     */
    private class UploadFileTask extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... imageUri) {

            HttpParams params = new BasicHttpParams();
            params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
            DefaultHttpClient httpClient = new DefaultHttpClient(params);
            return resizePhotoAndPublish(httpClient);
        }

        @Override
        protected void onProgressUpdate(String... values) {
            Log.w("EditAgentProfile", "onProgressUpdate　photoUrl = " + values[0]);
        }

        @Override
        protected void onPostExecute(String photoURL) {
            CoreData.isAgentProfileUpdate = true;
        }

        private String resizePhotoAndPublish(DefaultHttpClient httpClient) {
            File imageInDevice = new File(FileUtil.getAppSubFolder(Constants.UPLOAD_FILE_FOLDER_NAME),
                    "UserImage.jpg");
//            Bitmap finalizedBm=ImageUtil.decodeSampledBitmapFromResource(imageFilePath);
            try {
//                FileUtil.convertBitmapToFile(imageInDevice.toString(),finalizedBm);
                ImageUtil.getCompressImageFilePath(imageFilePath, imageInDevice.getAbsolutePath(), ImageUtil.iconImageType);
            } catch (Exception e) {
                e.printStackTrace();
            }
            String photoUrl = callUploadFileApi(httpClient, imageInDevice);
            publishProgress(photoUrl);

            //update profile img
//            DatabaseManager.getInstance(EditAgentProfileActivity.this).getLogInfo().set
            agentContent.photoUrl = photoUrl;
            CoreData.setUserContent(agentContent);
            DBLogInfo dbLogInfo = DatabaseManager.getInstance(EditAgentProfileActivity.this).getLogInfo();
            dbLogInfo.setLogInfo(new Gson().toJson(agentContent));
            DatabaseManager.getInstance(EditAgentProfileActivity.this).updateLogInfo(dbLogInfo);
            if (ringProgressDialog != null) {
                ringProgressDialog.dismiss();
                ringProgressDialog.cancel();
                ringProgressDialog = null;
            }
            return photoUrl;
        }
    }

}
