package co.real.productionreal2.activity.setting;

import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.BaseActivity;
import co.real.productionreal2.dao.account.DBLogInfo;
import co.real.productionreal2.daomanager.DatabaseManager;


/**
 * Created by hohojo on 11/11/2015.
 */
public class ChatSettingActivity extends BaseActivity {
    @InjectView(R.id.photoAutoDownloadSwitch)
    Switch photoAutoDownloadSwitch;
    @InjectView(R.id.headerLayout)
    View headerLayout;
    private DBLogInfo dbLogInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_setting);
        ButterKnife.inject(this);

        dbLogInfo = DatabaseManager.getInstance(this).getLogInfo();

        initView();
    }

    private void initView() {
        if (dbLogInfo.getPhotoAutoDownload()==null)
        dbLogInfo.setPhotoAutoDownload(false);

        photoAutoDownloadSwitch.setChecked(dbLogInfo.getPhotoAutoDownload());
        TextView titleTextView = (TextView) headerLayout.findViewById(R.id.titleTextView);
        titleTextView.setText(R.string.common__chat_setting);
        photoAutoDownloadSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    dbLogInfo.setPhotoAutoDownload(true);
                } else {
                    dbLogInfo.setPhotoAutoDownload(false);
                }
                DatabaseManager.getInstance(ChatSettingActivity.this).updateLogInfo(dbLogInfo);
            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


}
