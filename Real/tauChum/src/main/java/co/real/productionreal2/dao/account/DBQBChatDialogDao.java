package co.real.productionreal2.dao.account;

import java.util.List;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.Property;
import de.greenrobot.dao.internal.DaoConfig;
import de.greenrobot.dao.query.Query;
import de.greenrobot.dao.query.QueryBuilder;

import co.real.productionreal2.dao.account.DBQBChatDialog;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table DBQBCHAT_DIALOG.
*/
public class DBQBChatDialogDao extends AbstractDao<DBQBChatDialog, Long> {

    public static final String TABLENAME = "DBQBCHAT_DIALOG";

    /**
     * Properties of entity DBQBChatDialog.<br/>
     * Can be used for QueryBuilder and for referencing column names.
    */
    public static class Properties {
        public final static Property Id = new Property(0, Long.class, "id", true, "_id");
        public final static Property DialogID = new Property(1, String.class, "DialogID", false, "DIALOG_ID");
        public final static Property LastMessageText = new Property(2, String.class, "LastMessageText", false, "LAST_MESSAGE_TEXT");
        public final static Property LastMessageDate = new Property(3, String.class, "LastMessageDate", false, "LAST_MESSAGE_DATE");
        public final static Property QBChatDialog = new Property(4, String.class, "QBChatDialog", false, "QBCHAT_DIALOG");
        public final static Property LastMessageUserID = new Property(5, Integer.class, "LastMessageUserID", false, "LAST_MESSAGE_USER_ID");
        public final static Property Blocked = new Property(6, Integer.class, "Blocked", false, "BLOCKED");
        public final static Property Muted = new Property(7, Integer.class, "Muted", false, "MUTED");
        public final static Property Deleted = new Property(8, Integer.class, "Deleted", false, "DELETED");
        public final static Property Exited = new Property(9, Integer.class, "Exited", false, "EXITED");
        public final static Property BlockedOpponent = new Property(10, Integer.class, "BlockedOpponent", false, "BLOCKED_OPPONENT");
        public final static Property MutedOpponent = new Property(11, Integer.class, "MutedOpponent", false, "MUTED_OPPONENT");
        public final static Property DeletedOpponent = new Property(12, Integer.class, "DeletedOpponent", false, "DELETED_OPPONENT");
        public final static Property ExitedOpponent = new Property(13, Integer.class, "ExitedOpponent", false, "EXITED_OPPONENT");
        public final static Property UnReadMessageCount = new Property(14, Integer.class, "UnReadMessageCount", false, "UN_READ_MESSAGE_COUNT");
        public final static Property NeedInit = new Property(15, Boolean.class, "NeedInit", false, "NEED_INIT");
        public final static Property IsEncrypted = new Property(16, Boolean.class, "IsEncrypted", false, "IS_ENCRYPTED");
        public final static Property QbUserID = new Property(17, long.class, "qbUserID", false, "QB_USER_ID");
    };

    private Query<DBQBChatDialog> dBQBUser_ChatDialogsQuery;

    public DBQBChatDialogDao(DaoConfig config) {
        super(config);
    }
    
    public DBQBChatDialogDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    /** Creates the underlying database table. */
    public static void createTable(SQLiteDatabase db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "'DBQBCHAT_DIALOG' (" + //
                "'_id' INTEGER PRIMARY KEY AUTOINCREMENT ," + // 0: id
                "'DIALOG_ID' TEXT," + // 1: DialogID
                "'LAST_MESSAGE_TEXT' TEXT," + // 2: LastMessageText
                "'LAST_MESSAGE_DATE' TEXT," + // 3: LastMessageDate
                "'QBCHAT_DIALOG' TEXT," + // 4: QBChatDialog
                "'LAST_MESSAGE_USER_ID' INTEGER," + // 5: LastMessageUserID
                "'BLOCKED' INTEGER," + // 6: Blocked
                "'MUTED' INTEGER," + // 7: Muted
                "'DELETED' INTEGER," + // 8: Deleted
                "'EXITED' INTEGER," + // 9: Exited
                "'BLOCKED_OPPONENT' INTEGER," + // 10: BlockedOpponent
                "'MUTED_OPPONENT' INTEGER," + // 11: MutedOpponent
                "'DELETED_OPPONENT' INTEGER," + // 12: DeletedOpponent
                "'EXITED_OPPONENT' INTEGER," + // 13: ExitedOpponent
                "'UN_READ_MESSAGE_COUNT' INTEGER," + // 14: UnReadMessageCount
                "'NEED_INIT' INTEGER," + // 15: NeedInit
                "'IS_ENCRYPTED' INTEGER," + // 16: IsEncrypted
                "'QB_USER_ID' INTEGER NOT NULL );"); // 17: qbUserID
    }

    /** Drops the underlying database table. */
    public static void dropTable(SQLiteDatabase db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "'DBQBCHAT_DIALOG'";
        db.execSQL(sql);
    }

    /** @inheritdoc */
    @Override
    protected void bindValues(SQLiteStatement stmt, DBQBChatDialog entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
 
        String DialogID = entity.getDialogID();
        if (DialogID != null) {
            stmt.bindString(2, DialogID);
        }
 
        String LastMessageText = entity.getLastMessageText();
        if (LastMessageText != null) {
            stmt.bindString(3, LastMessageText);
        }
 
        String LastMessageDate = entity.getLastMessageDate();
        if (LastMessageDate != null) {
            stmt.bindString(4, LastMessageDate);
        }
 
        String QBChatDialog = entity.getQBChatDialog();
        if (QBChatDialog != null) {
            stmt.bindString(5, QBChatDialog);
        }
 
        Integer LastMessageUserID = entity.getLastMessageUserID();
        if (LastMessageUserID != null) {
            stmt.bindLong(6, LastMessageUserID);
        }
 
        Integer Blocked = entity.getBlocked();
        if (Blocked != null) {
            stmt.bindLong(7, Blocked);
        }
 
        Integer Muted = entity.getMuted();
        if (Muted != null) {
            stmt.bindLong(8, Muted);
        }
 
        Integer Deleted = entity.getDeleted();
        if (Deleted != null) {
            stmt.bindLong(9, Deleted);
        }
 
        Integer Exited = entity.getExited();
        if (Exited != null) {
            stmt.bindLong(10, Exited);
        }
 
        Integer BlockedOpponent = entity.getBlockedOpponent();
        if (BlockedOpponent != null) {
            stmt.bindLong(11, BlockedOpponent);
        }
 
        Integer MutedOpponent = entity.getMutedOpponent();
        if (MutedOpponent != null) {
            stmt.bindLong(12, MutedOpponent);
        }
 
        Integer DeletedOpponent = entity.getDeletedOpponent();
        if (DeletedOpponent != null) {
            stmt.bindLong(13, DeletedOpponent);
        }
 
        Integer ExitedOpponent = entity.getExitedOpponent();
        if (ExitedOpponent != null) {
            stmt.bindLong(14, ExitedOpponent);
        }
 
        Integer UnReadMessageCount = entity.getUnReadMessageCount();
        if (UnReadMessageCount != null) {
            stmt.bindLong(15, UnReadMessageCount);
        }
 
        Boolean NeedInit = entity.getNeedInit();
        if (NeedInit != null) {
            stmt.bindLong(16, NeedInit ? 1l: 0l);
        }
 
        Boolean IsEncrypted = entity.getIsEncrypted();
        if (IsEncrypted != null) {
            stmt.bindLong(17, IsEncrypted ? 1l: 0l);
        }
        stmt.bindLong(18, entity.getQbUserID());
    }

    /** @inheritdoc */
    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }    

    /** @inheritdoc */
    @Override
    public DBQBChatDialog readEntity(Cursor cursor, int offset) {
        DBQBChatDialog entity = new DBQBChatDialog( //
            cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // id
            cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1), // DialogID
            cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2), // LastMessageText
            cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3), // LastMessageDate
            cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4), // QBChatDialog
            cursor.isNull(offset + 5) ? null : cursor.getInt(offset + 5), // LastMessageUserID
            cursor.isNull(offset + 6) ? null : cursor.getInt(offset + 6), // Blocked
            cursor.isNull(offset + 7) ? null : cursor.getInt(offset + 7), // Muted
            cursor.isNull(offset + 8) ? null : cursor.getInt(offset + 8), // Deleted
            cursor.isNull(offset + 9) ? null : cursor.getInt(offset + 9), // Exited
            cursor.isNull(offset + 10) ? null : cursor.getInt(offset + 10), // BlockedOpponent
            cursor.isNull(offset + 11) ? null : cursor.getInt(offset + 11), // MutedOpponent
            cursor.isNull(offset + 12) ? null : cursor.getInt(offset + 12), // DeletedOpponent
            cursor.isNull(offset + 13) ? null : cursor.getInt(offset + 13), // ExitedOpponent
            cursor.isNull(offset + 14) ? null : cursor.getInt(offset + 14), // UnReadMessageCount
            cursor.isNull(offset + 15) ? null : cursor.getShort(offset + 15) != 0, // NeedInit
            cursor.isNull(offset + 16) ? null : cursor.getShort(offset + 16) != 0, // IsEncrypted
            cursor.getLong(offset + 17) // qbUserID
        );
        return entity;
    }
     
    /** @inheritdoc */
    @Override
    public void readEntity(Cursor cursor, DBQBChatDialog entity, int offset) {
        entity.setId(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setDialogID(cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1));
        entity.setLastMessageText(cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2));
        entity.setLastMessageDate(cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3));
        entity.setQBChatDialog(cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4));
        entity.setLastMessageUserID(cursor.isNull(offset + 5) ? null : cursor.getInt(offset + 5));
        entity.setBlocked(cursor.isNull(offset + 6) ? null : cursor.getInt(offset + 6));
        entity.setMuted(cursor.isNull(offset + 7) ? null : cursor.getInt(offset + 7));
        entity.setDeleted(cursor.isNull(offset + 8) ? null : cursor.getInt(offset + 8));
        entity.setExited(cursor.isNull(offset + 9) ? null : cursor.getInt(offset + 9));
        entity.setBlockedOpponent(cursor.isNull(offset + 10) ? null : cursor.getInt(offset + 10));
        entity.setMutedOpponent(cursor.isNull(offset + 11) ? null : cursor.getInt(offset + 11));
        entity.setDeletedOpponent(cursor.isNull(offset + 12) ? null : cursor.getInt(offset + 12));
        entity.setExitedOpponent(cursor.isNull(offset + 13) ? null : cursor.getInt(offset + 13));
        entity.setUnReadMessageCount(cursor.isNull(offset + 14) ? null : cursor.getInt(offset + 14));
        entity.setNeedInit(cursor.isNull(offset + 15) ? null : cursor.getShort(offset + 15) != 0);
        entity.setIsEncrypted(cursor.isNull(offset + 16) ? null : cursor.getShort(offset + 16) != 0);
        entity.setQbUserID(cursor.getLong(offset + 17));
     }
    
    /** @inheritdoc */
    @Override
    protected Long updateKeyAfterInsert(DBQBChatDialog entity, long rowId) {
        entity.setId(rowId);
        return rowId;
    }
    
    /** @inheritdoc */
    @Override
    public Long getKey(DBQBChatDialog entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    /** @inheritdoc */
    @Override    
    protected boolean isEntityUpdateable() {
        return true;
    }
    
    /** Internal query to resolve the "chatDialogs" to-many relationship of DBQBUser. */
    public List<DBQBChatDialog> _queryDBQBUser_ChatDialogs(long qbUserID) {
        synchronized (this) {
            if (dBQBUser_ChatDialogsQuery == null) {
                QueryBuilder<DBQBChatDialog> queryBuilder = queryBuilder();
                queryBuilder.where(Properties.QbUserID.eq(null));
                dBQBUser_ChatDialogsQuery = queryBuilder.build();
            }
        }
        Query<DBQBChatDialog> query = dBQBUser_ChatDialogsQuery.forCurrentThread();
        query.setParameter(0, qbUserID);
        return query.list();
    }

}
