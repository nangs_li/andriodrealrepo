package co.real.productionreal2.chat.core;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Vibrator;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.quickblox.chat.QBChat;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBMessageStatusesManager;
import com.quickblox.chat.QBPrivateChat;
import com.quickblox.chat.QBPrivateChatManager;
import com.quickblox.chat.QBSignaling;
import com.quickblox.chat.QBSystemMessagesManager;
import com.quickblox.chat.QBWebRTCSignaling;
import com.quickblox.chat.exception.QBChatException;
import com.quickblox.chat.listeners.QBIsTypingListener;
import com.quickblox.chat.listeners.QBMessageListenerImpl;
import com.quickblox.chat.listeners.QBMessageStatusListener;
import com.quickblox.chat.listeners.QBPrivateChatManagerListener;
import com.quickblox.chat.listeners.QBSystemMessageListener;
import com.quickblox.chat.listeners.QBVideoChatSignalingManagerListener;
import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.chat.model.QBDialog;
import com.quickblox.chat.query.QueryUpdateDialog;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.core.QBRequestCanceler;
import com.quickblox.core.request.QBRequestUpdateBuilder;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;
import com.quickblox.videochat.webrtc.QBRTCClient;
import com.quickblox.videochat.webrtc.QBRTCException;
import com.quickblox.videochat.webrtc.QBRTCSession;
import com.quickblox.videochat.webrtc.callbacks.QBRTCClientConnectionCallbacks;
import com.quickblox.videochat.webrtc.callbacks.QBRTCClientSessionCallbacks;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.webrtc.VideoCapturerAndroid;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.real.productionreal2.ApplicationSingleton;
import co.real.productionreal2.Constants;
import co.real.productionreal2.CoreData;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.MainActivityTabBase;
import co.real.productionreal2.activity.chat.ChatActivity;
import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.chat.ChatDbViewController;
import co.real.productionreal2.chat.ChatLobbyDbApi;
import co.real.productionreal2.chat.ChatLobbyDbViewController;
import co.real.productionreal2.chat.ChatMainDbViewController;
import co.real.productionreal2.common.LocalStorageHelper;
import co.real.productionreal2.dao.account.DBQBChatDialog;
import co.real.productionreal2.dao.account.DBQBChatMessage;
import co.real.productionreal2.dao.account.DBQBUser;
import co.real.productionreal2.daomanager.DatabaseManager;
import co.real.productionreal2.fragment.ChatLobbyFragment;
import co.real.productionreal2.model.SystemMsgError;
import co.real.productionreal2.service.BaseService;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.ChatRoomMember;
import co.real.productionreal2.service.model.request.RequestChatRoomGet;
import co.real.productionreal2.service.model.response.ResponseChatRoomGet;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;
import co.real.productionreal2.view.Dialog;

public class PrivateChatImpl extends QBMessageListenerImpl<QBPrivateChat> implements QBPrivateChatManagerListener,
        QBRTCClientSessionCallbacks, QBRTCClientConnectionCallbacks {

    private static final String TAG = "PrivateChatManagerImpl";

    private QBPrivateChatManager privateChatManager;
    private QBPrivateChat privateChat;
    private Context context;

    //    private QBMessageListener<QBPrivateChat> privateChatMessageListener;
    // Message status manager & listener
    private QBMessageStatusesManager messageStatusesManager;
    private QBMessageStatusListener messageStatusListener;
    // System messages manager
    private QBSystemMessagesManager systemMessagesManager;
    private QBSystemMessageListener systemMessageListener;

    private QBIsTypingListener<QBChat> isTypingListener;
    private ResponseLoginSocial.Content userContent;

    private final Handler initMangerHandler = new Handler();
    /**
     * @param context The Android {@link Context}.
     * @return this.instance
     */
    private static PrivateChatImpl instance;

    public static PrivateChatImpl getInstance(Context context) {
        Log.w(TAG, "PrivateChatImpl instance: " + instance);
        if (instance == null) {
            instance = new PrivateChatImpl(context);
        }

        return instance;
    }

    public void clearInstance() {
        instance = null;
        initMangerHandler.removeCallbacksAndMessages(null);
    }


    public void initPrivateChatImpl() {
//        initPrivateChatMessageListener();
        initManagerIfNeed();
        initIsTypingListener();
        initMessageStatusManagerAndListener();
        initSystemMessagesManagerAndListener();
        initQBRTCClient();
    }

    public PrivateChatImpl(Context context) {
        this.context = context;
        initPrivateChatImpl();

        if (DatabaseManager.getInstance(context).getLogInfo() != null) {
            userContent = CoreData.getUserContent(context);
        }
    }

    private void initQBRTCClient() {
        Log.d(TAG, "initQBRTCClient()");

        if (QBChatService.getInstance() == null)
            return;
        if (QBChatService.getInstance().getVideoChatWebRTCSignalingManager() == null)
            return;
        // Add signalling manager
        QBChatService.getInstance().getVideoChatWebRTCSignalingManager().addSignalingManagerListener(new QBVideoChatSignalingManagerListener() {
            @Override
            public void signalingCreated(QBSignaling qbSignaling, boolean createdLocally) {
                if (!createdLocally) {
                    QBRTCClient.getInstance().addSignaling((QBWebRTCSignaling) qbSignaling);
                }
            }
        });

        QBRTCClient.getInstance().setCameraErrorHendler(new VideoCapturerAndroid.CameraErrorHandler() {
            @Override
            public void onCameraError(final String s) {
                Toast.makeText(context, s, Toast.LENGTH_SHORT).show();
            }
        });

//        QBRTCConfig.setAnswerTimeInterval(45l);

        // Add activity as callback to RTCClient
        QBRTCClient.getInstance().addSessionCallbacksListener(this);
        QBRTCClient.getInstance().addConnectionCallbacksListener(this);

        // Start mange QBRTCSessions according to VideoCall parser's callbacks
        QBRTCClient.getInstance().prepareToProcessCalls(context);
    }

    public void initManagerIfNeed() {
        if (privateChatManager == null) {
            ChatService.initIfNeed(context);
            QBChatService qbChatService = QBChatService.getInstance();

            Log.w(TAG, "qbChatService = " + qbChatService);
            privateChatManager = qbChatService.getPrivateChatManager();
            Log.w(TAG, "initManagerIfNeed privateChatManager = " + privateChatManager);
            if (privateChatManager == null) {
                if (Constants.isDevelopment)
                    Toast.makeText(context, "privateChatManager is null", Toast.LENGTH_LONG).show();
//                return;
                //todo wait 0.5 s
                initMangerHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
//                        showReconnectProgressDialog();
                        initManagerIfNeed();
                    }
                }, 500);

            } else {
                privateChatManager.addPrivateChatManagerListener(this);
//                hideReconnectProgressDialog();
            }

        }
    }


    public void initPrivateChatIfnNeed(Integer opponentID) {
        if (privateChatManager == null) {
            return;
        }
        privateChat = privateChatManager.getChat(opponentID);
        if (privateChat == null) {
            privateChat = privateChatManager.createChat(opponentID, this);
        }
        privateChat.addIsTypingListener(isTypingListener);
        privateChat.addMessageListener(this);

        Log.w(TAG, "privateChat = " + privateChat);
    }

    public QBPrivateChatManager getPrivateChatManager(){
        return privateChatManager;
    }

    private void initIsTypingListener() {
        isTypingListener = new QBIsTypingListener<QBChat>() {
            @Override
            public void processUserIsTyping(final QBChat chat, final Integer userId) {
                Log.w(TAG, "privateChat = " + privateChat);
                Log.w(TAG, "privateChat = " + chat);
                Log.w(TAG, "userId = " + userId);
                if (ApplicationSingleton.getInstance().currentActivity.getClass() == ChatActivity.class && ((ChatActivity)ApplicationSingleton.getInstance().currentActivity).getOpponentQBID() == userId) {
                    ChatDbViewController chatDbViewController = ChatDbViewController.getInstance(context);
                    chatDbViewController.statusAndTypingTextViewOnChange(Color.WHITE, ChatActivity.context.getString(R.string.chatroom__typing));
                    if (typingTimer != null)
                        typingTimer.start();
//                } else if (ApplicationSingleton.getInstance().currentActivity.getClass() == MainActivityTabBase.class) {

                }
            }

            @Override
            public void processUserStopTyping(QBChat chat, Integer userId) {
                Log.w(TAG, "privateChat = " + privateChat);
                Log.w(TAG, "privateChat = " + chat);
                Log.w(TAG, "userId = " + userId);
                if (ApplicationSingleton.getInstance().currentActivity.getClass() == ChatActivity.class && ((ChatActivity) ApplicationSingleton.getInstance().currentActivity).getOpponentQBID() == userId) {
                    ChatDbViewController chatDbViewController = ChatDbViewController.getInstance(context);
                    chatDbViewController.statusAndTypingTextViewOnChange(Color.WHITE, ChatActivity.context.getString(R.string.chatroom__online));
                if (typingTimer != null)
                    typingTimer.cancel();
                }
            }
        };
    }

    CountDownTimer typingTimer = new CountDownTimer(Constants.CHAT_ONLINE_STATUS_TIMER,
            Constants.CHAT_ONLINE_STATUS_TIMER) {

        @Override
        public void onTick(long millisUntilFinished) {
        }

        @Override
        public void onFinish() {
            if (ApplicationSingleton.getInstance().currentActivity.getClass() == ChatActivity.class) {
                ChatDbViewController chatDbViewController = ChatDbViewController.getInstance(context);
                chatDbViewController.statusAndTypingTextViewOnChange(Color.WHITE, ChatActivity.context.getString(R.string.chatroom__online));
            }
        }
    };


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////send read msg listener ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public void sendMessage(QBChatMessage message) throws XMPPException, SmackException.NotConnectedException {
        if (privateChat == null)
            return;
        Log.w(TAG, "message = " + new Gson().toJson(message));
        privateChat.sendMessage(message);
    }

    public void readMessage(QBChatMessage chatMessage) throws XMPPException, SmackException.NotConnectedException {
        if (privateChat == null)
            return;
        privateChat.readMessage(chatMessage);
    }

    //receive Delivered and receive read
    private void initMessageStatusManagerAndListener() {
        messageStatusesManager = QBChatService.getInstance().getMessageStatusesManager();

        messageStatusListener = new QBMessageStatusListener() {
            @Override
            public void processMessageDelivered(String messageId, String dialogId, Integer userId) {
                Activity activity = ApplicationSingleton.getInstance().currentActivity;
                DatabaseManager.getInstance(activity).updateMsgStatusDbAction(messageId, ChatDbViewController.SENT_USER);

                if (activity.getClass() == ChatActivity.class)
                    ChatDbViewController.getInstance(activity).processMessageDelivered(messageId, dialogId, userId);
                else if (activity.getClass() == MainActivityTabBase.class)
                    ChatMainDbViewController.getInstance(activity).processMessageDelivered(messageId, dialogId, userId);
            }

            @Override
            public void processMessageRead(String messageId, String dialogId, Integer userId) {
                Activity activity = ApplicationSingleton.getInstance().currentActivity;
                DatabaseManager.getInstance(activity).updateMsgStatusDbAction(messageId, ChatDbViewController.READ);

                if (activity.getClass() == ChatActivity.class)
                    ChatDbViewController.getInstance(activity).processMessageRead(messageId, dialogId, userId);
                else if (activity.getClass() == MainActivityTabBase.class)
                    ChatMainDbViewController.getInstance(activity).processMessageRead(messageId, dialogId, userId);
            }
        };
        if (messageStatusesManager == null)
            return;
        messageStatusesManager.addMessageStatusListener(messageStatusListener);
    }

    private boolean isBlockedOpponent(DBQBChatDialog dbqbChatDialog) {
        if (dbqbChatDialog != null) {
            if (dbqbChatDialog.getBlockedOpponent() != null &&
                    dbqbChatDialog.getBlockedOpponent() == 1)
                return true;
            if (dbqbChatDialog.getExited() != null &&
                    dbqbChatDialog.getExited() == 1)
                return true;
            if (dbqbChatDialog.getExitedOpponent() != null &&
                    dbqbChatDialog.getExitedOpponent() == 1)
                return true;
            return false;
        }
        return false;
    }

    private void checkisMutedOpponentAndVibrate(DBQBChatDialog dbqbChatDialog) {
        if (dbqbChatDialog == null || dbqbChatDialog.getMutedOpponent() == null || dbqbChatDialog.getMutedOpponent() == 0) {
            Vibrator vibrator = (Vibrator) this.context.getSystemService(Context.VIBRATOR_SERVICE);
            vibrator.vibrate(500);
        }
    }

    private boolean isNeedInit(QBChatMessage chatMessage) {
        List<DBQBChatMessage> dbqbChatMessageList = DatabaseManager.getInstance(context).listQbMessageByDialogId(chatMessage.getDialogId());
        Log.w(TAG, "isNeedInit dbqbChatMessageList: " + dbqbChatMessageList.size());
        if (dbqbChatMessageList == null || dbqbChatMessageList.size() == 0)
            return true;
        return false;
    }

    @Override
    public void processMessage(QBPrivateChat chat, final QBChatMessage chatMessage) {
        Boolean muted = false;

        Log.w(TAG, "processMessage chatMessage: " + new Gson().toJson(chatMessage));
        Log.d("","edwin ChatService processMessage chatMessage  " + new Gson().toJson(chatMessage));
        final Activity activity = ApplicationSingleton.getInstance().currentActivity;
        final boolean isNeedInit = isNeedInit(chatMessage);
        final String dialogId = chatMessage.getDialogId();
        Log.v("","edwin ChatService processMessage chatMessage getQBChatDialogByDialogId");
        DBQBChatDialog dbqbChatDialog = DatabaseManager.getInstance(context).getQBChatDialogByDialogId(dialogId);


        //insert / update dialog in DB
        if (dbqbChatDialog == null) {
            Log.v("","edwin ChatService processMessage chatMessage dbqbChatDialog == null");
            // new chat in chat lobby
            ChatService.getInstance().getDialogById(dialogId, new BaseService.BaseServiceCallBack() {
                @Override
                public void onSuccess(Object var1) {
                    Log.v("","edwin ChatService processMessage getDialogById ChatService");
                    ChatLobbyDbViewController.getInstance(context).reloadChatLobby(dialogId);
                }

                @Override
                public void onError(BaseService.RealServiceError serviceError) {

                }
            });

        } else {
            //old chat in chat lobby
            Log.v("","edwin ChatService processMessage chatMessage dbqbChatDialog != null");
                DBQBUser dbqbUser = DatabaseManager.getInstance(context).getQBUserByUserId(dbqbChatDialog.getQbUserID());

                Log.w(TAG, "dbqbChatDialog.getMutedOpponent() = " + dbqbChatDialog.getMutedOpponent());
                if (dbqbChatDialog.getMutedOpponent() != null && dbqbChatDialog.getMutedOpponent() == 1) {
                    muted = true;
                }


                if (isBlockedOpponent(dbqbChatDialog))
                    return;

                if (dbqbUser != null) {
                    //reset Clear user
                    boolean needUpdate = false;
                    if (dbqbUser.getIsClear() != null && dbqbUser.getIsClear()) {
                        dbqbUser.setIsClear(false);
                        needUpdate = true;
                    }
                    //reset Hidden user
                    if (dbqbUser.getIsHidden() != null && dbqbUser.getIsHidden()) {
                        dbqbUser.setIsHidden(false);
                        needUpdate = true;
                    }

                    if (needUpdate) {
                        DatabaseManager.getInstance(context).insertOrUpdateQBUser(dbqbUser);
                    }
                }

                checkisMutedOpponentAndVibrate(dbqbChatDialog);

            Long qbDateSent = chatMessage.getDateSent();
            Log.w(TAG, "date_sent = " + chatMessage.getProperty(ChatDbViewController.PROPERTY_DATE_SENT));
            if (qbDateSent == 0)
                qbDateSent = Double.valueOf(chatMessage.getProperty(ChatDbViewController.PROPERTY_DATE_SENT).toString()).longValue();

            dbqbChatDialog.setLastMessageDate("" + qbDateSent);
            dbqbChatDialog.setLastMessageText(chatMessage.getBody());
            dbqbChatDialog.setLastMessageUserID(chatMessage.getSenderId());
            Log.w(TAG, "isNeedInit: " + isNeedInit(chatMessage));
            dbqbChatDialog.setNeedInit(isNeedInit);
            DatabaseManager.getInstance(context).updateQBChatDialog(dbqbChatDialog);
        }
        Gson gson = new Gson();
        DBQBChatMessage dbqbChatMessage = DatabaseManager.getInstance(context).chatMessageToDbQbChatMessage(chatMessage, gson);
        //insert msg in DB
        if (activity.getClass() == ChatActivity.class) {
            DatabaseManager.getInstance(context).addUpdateMsgDbAction(chatMessage, ChatDbViewController.RECIEVED, false);
            ChatDbViewController.getInstance(context).receivedMsg(chatMessage);
            if (!muted && !((ChatActivity)activity).getDialogId().equalsIgnoreCase(dialogId)) {
                ApplicationSingleton.getInstance().showChatNotification(dbqbChatMessage);
            }
        } else {
            if (DatabaseManager.getInstance(context).listUnreadFirstMsg(chatMessage.getDialogId()) != null &&
                    DatabaseManager.getInstance(context).listUnreadFirstMsg(chatMessage.getDialogId()).size() > 0) {
                DatabaseManager.getInstance(context).addUpdateMsgDbAction(chatMessage, ChatDbViewController.RECIEVED, false);
            } else {
                DatabaseManager.getInstance(context).addUpdateMsgDbAction(chatMessage, ChatDbViewController.RECIEVED, true);
            }

            if (activity.getClass() == MainActivityTabBase.class) {
                if (ChatLobbyFragment.fragmentIsRunning) {
                    ChatLobbyDbViewController.getInstance(context).reloadChatLobby(chatMessage.getDialogId());
                } else {
                    ChatMainDbViewController.getInstance(context).updateViewByReceivedMsg(chat, chatMessage);
                }
            }
            ChatMainDbViewController.getInstance(context).updateTabView();
            if (!muted) {
                ApplicationSingleton.getInstance().showChatNotification(dbqbChatMessage);
            }
        }

    }

    @Override
    public void processError(QBPrivateChat chat, QBChatException error, QBChatMessage originChatMessage) {
        Log.w(TAG, "new message processError chat: " + chat);
        Log.w(TAG, "new message processError error: " + error);
        Log.w(TAG, "new message processError originChatMessage: " + originChatMessage);
        ChatDbViewController.getInstance(context).processError(originChatMessage, error);
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////send read msg listener ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////system msg listener ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private void initSystemMessagesManagerAndListener() {
        systemMessagesManager = QBChatService.getInstance().getSystemMessagesManager();
        Log.w(TAG, "systemMessageListener systemMessagesManager: " + systemMessagesManager);
        Log.w("","edwin PCI initSystemMessagesManagerAndListener ");
        systemMessageListener = new QBSystemMessageListener() {
            @Override
            public void processMessage(QBChatMessage qbChatMessage) {
                Log.w("","edwin PCI initSystemMessagesManagerAndListener processMessage ");
                Log.w(TAG, "edwin PCI systemMessageListener qbChatMessage: " + new Gson().toJson(qbChatMessage));
                if (qbChatMessage.getProperty(ChatDbViewController.PROPERTY_SYSTEM_MSG_TYPE).toString()
                        .equalsIgnoreCase(ChatDbViewController.SYSTEM_MSG_CHAT_SETTING)) {
                    Log.w("","edwin PCI initSystemMessagesManagerAndListener processMessage SYSTEM_MSG_CHAT_SETTING");
                    systemMsgChatSettingAction(qbChatMessage);
                } else if (qbChatMessage.getProperty(ChatDbViewController.PROPERTY_SYSTEM_MSG_TYPE).toString()
                        .equalsIgnoreCase(ChatDbViewController.SYSTEM_MSG_ALERT)) {
                    Log.w("","edwin PCI initSystemMessagesManagerAndListener processMessage SYSTEM_MSG_ALERT");
                    systemMsgAlertAction(qbChatMessage);
                } else if (qbChatMessage.getProperty(ChatDbViewController.PROPERTY_SYSTEM_MSG_TYPE).toString()
                        .equalsIgnoreCase(ChatDbViewController.SYSTEM_MSG_CHAT_PRIVACY_SETTING)) {
                    Log.w("","edwin PCI initSystemMessagesManagerAndListener processMessage SYSTEM_MSG_CHAT_PRIVACY_SETTING");
                    systemMsgChatPrivacyAction(qbChatMessage);
                }

            }

            @Override
            public void processError(QBChatException e, QBChatMessage qbChatMessage) {
                Log.w("","edwin PCI initSystemMessagesManagerAndListener processError ");
                ChatDbViewController.getInstance(ChatActivity.context).processSystemMessageError(e, qbChatMessage);
            }
        };
        if (systemMessagesManager == null)
            return;
        Log.w("","edwin PCI addSystemMessageListener  ");
        systemMessagesManager.addSystemMessageListener(systemMessageListener);
    }

    private void systemMsgChatPrivacyAction(final QBChatMessage qbChatMessage) {
        ApplicationSingleton.getInstance().currentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                QBUsers.getUser(qbChatMessage.getSenderId(), new QBEntityCallback<QBUser>() {
                    @Override
                    public void onSuccess(final QBUser qbUser, Bundle bundle) {
                        ApplicationSingleton.getInstance().currentActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                DBQBUser dbqbUser = DatabaseManager.getInstance(context).getQBUserByUserId((long) qbChatMessage.getSenderId());
                                if (dbqbUser != null) {
                                    Log.w(TAG, "systemMsgChatPrivacyAction qbUser: " + new Gson().toJson(qbUser));
                                    dbqbUser.setQBUser(new Gson().toJson(qbUser));
                                    DatabaseManager.getInstance(context).insertOrUpdateQBUser(dbqbUser);
                                }

                                if (ApplicationSingleton.getInstance().currentActivity.getClass() == ChatActivity.class)
                                    ChatDbViewController.getInstance(context).processSystemMessageChatPrivacy(qbChatMessage, qbUser);
                            }
                        });
                    }

                    @Override
                    public void onSuccess() {


                    }

                    @Override
                    public void onError(List<String> list) {
//                Toast.makeText(ChatActivity.this, "getUser error: " + list, Toast.LENGTH_LONG).show();
                        Log.w(TAG, "getUser fail: " + list);
                    }
                });
            }
        });

    }


    private void systemMsgAlertAction(QBChatMessage qbChatMessage) {

    }

    private void systemMsgChatSettingAction(final QBChatMessage qbChatMessage) {
        Log.i("","edwin PCI systemMsgChatSettingAction  ");
        final RequestChatRoomGet chatRoomGet = new RequestChatRoomGet(userContent.memId, LocalStorageHelper.getAccessToken(context));
        DBQBChatDialog dbqbChatDialog = DatabaseManager.getInstance(context).getQBChatDialogByQbId(qbChatMessage.getSenderId());
        Log.i("","edwin PCI systemMsgChatSettingAction 1 ");
        if (dbqbChatDialog == null)
            return;
        chatRoomGet.dialogId = dbqbChatDialog.getDialogID();
        Log.i("","edwin PCI systemMsgChatSettingAction  2");
        chatRoomGet.callChatRoomGetApi(context, new ApiCallback() {
            @Override
            public void success(ApiResponse apiResponse) {
                Log.i("","edwin PCI systemMsgChatSettingAction  callChatRoomGetApi success");
                ResponseChatRoomGet responseGson = new Gson().fromJson(apiResponse.jsonContent, ResponseChatRoomGet.class);
                DBQBChatDialog dbqbChatDialog = DatabaseManager.getInstance(context).getQBChatDialogByDialogId(chatRoomGet.dialogId);
                if (responseGson.content.chatRoom == null) {
                    if (Constants.isDevelopment)
                        Toast.makeText(context, "response chatroom = null", Toast.LENGTH_LONG).show();
                    return;
                }
                if (responseGson.content.chatRoom.size() < 1) {
                    if (Constants.isDevelopment)
                        Toast.makeText(context, "response chatroom size < 1", Toast.LENGTH_LONG).show();
                    return;
                }
                if (dbqbChatDialog == null) {
                    if (Constants.isDevelopment)
                        Toast.makeText(context, "dbqbChatDialog = null", Toast.LENGTH_LONG).show();
                    return;
                }

                for (ChatRoomMember chatRoomMem : responseGson.content.chatRoom.get(0).chatRoomMemList) {
                    if (chatRoomMem.qbId.equalsIgnoreCase(userContent.qbid)) {
                        dbqbChatDialog.setDeleted(chatRoomMem.deleted);
                        dbqbChatDialog.setExited(chatRoomMem.exited);
                        dbqbChatDialog.setBlocked(chatRoomMem.blocked);
                        dbqbChatDialog.setMuted(chatRoomMem.muted);
                    } else {
                        dbqbChatDialog.setDeletedOpponent(chatRoomMem.deleted);
                        dbqbChatDialog.setExitedOpponent(chatRoomMem.exited);
                        dbqbChatDialog.setBlockedOpponent(chatRoomMem.blocked);
                        dbqbChatDialog.setMutedOpponent(chatRoomMem.muted);
                    }
                }
                Log.i("","edwin PCI systemMsgChatSettingAction  callChatRoomGetApi success check");
                if (ApplicationSingleton.getInstance().currentActivity.getClass() == ChatActivity.class) {
                    Log.i("","edwin PCI systemMsgChatSettingAction  callChatRoomGetApi success processSystemMessage(qbChatMessage, dbqbChatDialog) ");
                    ChatDbViewController.getInstance(ChatActivity.context).processSystemMessage(qbChatMessage, dbqbChatDialog);
                }
            }

            @Override
            public void failure(String errorMsg) {
                if (Constants.isDevelopment) {
                    Toast.makeText(context, "callChatRoomGetApi fail: " + errorMsg, Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void sendSystemMsgAlert(int opponentId, SystemMsgError systemMsgError) {
        try {
            QBChatMessage chatMessage = new QBChatMessage();
            chatMessage.setProperty(ChatDbViewController.PROPERTY_SYSTEM_MSG_TYPE, ChatDbViewController.SYSTEM_MSG_CHAT_SETTING);
            chatMessage.setProperty(ChatDbViewController.PROPERTY_SYSTEM_MSG_CONTENT, new Gson().toJson(systemMsgError));
            chatMessage.setRecipientId(opponentId);

            systemMessagesManager.sendSystemMessage(chatMessage);

        } catch (SmackException.NotConnectedException e) {

        } catch (IllegalStateException ee) {

        }
    }

    public void sendSystemMsgSetting(int opponentId, String systemMsgType) {
        try {
            QBChatMessage chatMessage = new QBChatMessage();
            chatMessage.setProperty(ChatDbViewController.PROPERTY_SYSTEM_MSG_TYPE, systemMsgType);
//            chatMessage.setProperty(ChatDbViewController.PROPERTY_SYSTEM_MSG_CONTENT, "");
            chatMessage.setProperty("isChatSetting", "1");
            chatMessage.setRecipientId(opponentId);

            systemMessagesManager.sendSystemMessage(chatMessage);

            Log.w(TAG, "sendSystemMsgSetting chatMessage: " + new Gson().toJson(chatMessage));
        } catch (SmackException.NotConnectedException e) {
            Log.e(TAG, "NotConnectedException: " + e);
        } catch (IllegalStateException ee) {
            Log.e(TAG, "IllegalStateException: " + ee);

        }
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////system msg listener ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public void sendIsTypingNotification() {
        try {
            if (privateChat == null) {
                if (Constants.isDevelopment)
                    Toast.makeText(ChatActivity.context, "sendIstyping not send, privateChat = null", Toast.LENGTH_SHORT).show();
                return;
            }
            if (QBChatService.getInstance().isLoggedIn()) {
                privateChat.sendIsTypingNotification();
                Log.w(TAG, "sendIsTypingNotification");
            }
        } catch (XMPPException e) {
            e.printStackTrace();
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
        }
    }

    public void sendStopTypingNotification() {
        try {
            Log.w(TAG, "sendStopTypingNotification");
            if (privateChat != null && QBChatService.getInstance().isLoggedIn())
                privateChat.sendStopTypingNotification();
        } catch (SmackException.NotConnectedException | XMPPException e) {
            e.printStackTrace();
        }
    }

    public void release() {
        Log.w(TAG, "release private chat");
//        if (privateChat != null)
//            privateChat.removeMessageListener(this);
//        if (privateChatManager != null)
//            privateChatManager.removePrivateChatManagerListener(this);
        privateChat = null;
    }


    @Override
    public void chatCreated(QBPrivateChat incomingPrivateChat, boolean createdLocally) {
        if (!createdLocally) {
            privateChat = incomingPrivateChat;
            privateChat.addMessageListener(PrivateChatImpl.this);
        }

        Log.w(TAG, "private chat created: " + incomingPrivateChat.getParticipant() + ", createdLocally:" + createdLocally);
    }

    public void firstTimeCreateDialog(int opponentID, final String msgTextOrPhotoPath, final int type) {
        if (privateChatManager == null) {
            if (Constants.isDevelopment)
                Toast.makeText(context, "privateChatManager == null", Toast.LENGTH_LONG).show();
        }

        privateChatManager.createDialog(opponentID, new QBEntityCallback<QBDialog>() {
            @Override
            public void onSuccess(QBDialog qbDialog, Bundle bundle) {
                if (ApplicationSingleton.getInstance().currentActivity.getClass() == ChatActivity.class) {
                    ChatDbViewController.getInstance(ChatActivity.context).firstCreateDialogSuccess(qbDialog, msgTextOrPhotoPath, type);
                    new ChatLobbyDbApi(context).getDialogsFromQBAndGetAgentProfileListAndChatSetting(new ApiCallback() {
                        @Override
                        public void success(ApiResponse apiResponse) {

                        }

                        @Override
                        public void failure(String errorMsg) {
                            Dialog.normalDialog(ApplicationSingleton.getInstance().currentActivity, errorMsg).show();
                        }
                    });
                }
            }

            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(List<String> list) {
                Log.e(TAG, "firstTimeCreateDialog on error: " + list);
            }
        });
    }

    public static void delDialogToQB(QBDialog dialog, final QBEntityCallbackImpl callback) {
        QBPrivateChatManager privateChatManager = QBChatService.getInstance().getPrivateChatManager();
        privateChatManager.deleteDialog(dialog.getDialogId(), new QBEntityCallbackImpl<Void>() {
            @Override
            public void onSuccess() {
                callback.onSuccess();

            }

            @Override
            public void onError(List<String> errors) {
                callback.onError(errors);

            }
        });
    }

    public static QBRequestCanceler updateDialog(QBDialog dialog, QBRequestUpdateBuilder requestBuilder, QBEntityCallback<QBDialog> callback) {
        QueryUpdateDialog queryUpdateDialog = new QueryUpdateDialog(dialog, requestBuilder);
        return new QBRequestCanceler(queryUpdateDialog.performAsyncWithCallback(callback));
    }

    @Override
    public void onStartConnectToUser(QBRTCSession qbrtcSession, Integer integer) {
        Log.w(TAG, "onConnectedToUser");
    }

    @Override
    public void onConnectedToUser(QBRTCSession qbrtcSession, Integer integer) {
        Log.w(TAG, "onConnectedToUser");
    }

    @Override
    public void onConnectionClosedForUser(QBRTCSession qbrtcSession, Integer integer) {
        Log.w(TAG, "onConnectionClosedForUser");
    }

    @Override
    public void onDisconnectedFromUser(QBRTCSession qbrtcSession, Integer integer) {
        Log.w(TAG, "onDisconnectedFromUser");
    }

    @Override
    public void onDisconnectedTimeoutFromUser(QBRTCSession qbrtcSession, Integer integer) {
        Log.w(TAG, "onDisconnectedTimeoutFromUser");
    }

    @Override
    public void onConnectionFailedWithUser(QBRTCSession qbrtcSession, Integer integer) {
        Log.w(TAG, "onConnectionFailedWithUser");
    }

    @Override
    public void onError(QBRTCSession qbrtcSession, QBRTCException e) {
        Log.w(TAG, "recevie video onError");
    }

    @Override
    public void onReceiveNewSession(QBRTCSession qbrtcSession) {
        Log.w(TAG, "recevie video new session");
        Map<String, String> rejectInfo = new HashMap<>();
        rejectInfo.put("errorMessage", context.getString(R.string.chatroom_error_message__your_opponents));
        if (qbrtcSession.getConferenceType().getValue() == 1) {
//            video call
            Dialog.normalDialog(ApplicationSingleton.getInstance().currentActivity, qbrtcSession.getUserInfo().get("senderName") + " " + context.getString(R.string.chatroom_error_message__want_to_have_a_video)).show();
        } else if (qbrtcSession.getConferenceType().getValue() == 2) {
//            audio call
            Dialog.normalDialog(ApplicationSingleton.getInstance().currentActivity, qbrtcSession.getUserInfo().get("senderName") + " " + context.getString(R.string.chatroom_error_message__want_to_have_a_audio)).show();
        }
        qbrtcSession.rejectCall(rejectInfo);
    }

    @Override
    public void onUserNotAnswer(QBRTCSession qbrtcSession, Integer integer) {
        Log.w(TAG, "recevie video onUserNotAnswer");
    }

    @Override
    public void onCallRejectByUser(QBRTCSession qbrtcSession, Integer integer, Map<String, String> map) {
        Log.w(TAG, "recevie video onCallRejectByUser");
    }

    @Override
    public void onReceiveHangUpFromUser(QBRTCSession qbrtcSession, Integer integer) {
        Log.w(TAG, "recevie video onReceiveHangUpFromUser");
    }

    @Override
    public void onSessionClosed(QBRTCSession qbrtcSession) {
        Log.w(TAG, "recevie video onSessionClosed");
    }

    @Override
    public void onSessionStartClose(QBRTCSession qbrtcSession) {
        Log.w(TAG, "recevie video onSessionStartClose");
    }


}
