package co.real.productionreal2.newsfeed;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import co.real.productionreal2.Constants;
import co.real.productionreal2.CoreData;
import co.real.productionreal2.Navigation;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.MainActivityTabBase;
import co.real.productionreal2.activity.createpost.BaseCreatePost;
import co.real.productionreal2.adapter.CreatePostPreviewPagerAdapter;
import co.real.productionreal2.config.AppConfig;
import co.real.productionreal2.model.AgentListing;
import co.real.productionreal2.model.SystemSetting;
import co.real.productionreal2.service.model.AgentProfiles;
import co.real.productionreal2.service.model.request.RequestListing;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;
import co.real.productionreal2.util.AppUtil;
import co.real.productionreal2.util.FileUtil;
import co.real.productionreal2.util.ImageUtil;
import co.real.productionreal2.util.MathUtil;
import co.real.productionreal2.util.RecyclerItemClickListener;
import me.iwf.photopicker.PhotoPagerActivity;
import widget.ComputeVerticalScrollOffAbleRecyclerView;
import widget.EnbleableSwipeViewPager;
import widget.RoundedImageView;
import widget.observablescrollview.ObservableScrollView;
import widget.observablescrollview.ObservableScrollViewCallbacks;
import widget.observablescrollview.ScrollState;
import widget.observablescrollview.ScrollUtils;
import widget.overscroll.VerticalOverScrollBounceEffectDecorator;
import widget.overscroll.adapters.RecyclerViewOverScrollDecorAdapter;
import widget.overscroll.adapters.ScrollViewOverScrollDecorAdapter;

;


public class NewsFeedDetailFragment extends BaseNewsFeedFragment implements ObservableScrollViewCallbacks {

    private final String TAG = "NewsFeedDetailFragment";
    private AgentProfiles agentProfilesItem;
    private String propertyTypeImageName;
    private String propertyTypeName;
    public List<AgentListing.Photo> subPhotosList;
    private ArrayList<String> photoPaths = new ArrayList<String>();

    private final float MAX_TEXT_SCALE_DELTA = 0.3f;
    private final int MAX_DRAG_OFFSET = 150;
    //    private int itemPosition = -1;
    private int MemberID = -1;
    private boolean isMeDetail = false;
    private LayoutInflater inflater;

    ResponseLoginSocial.Content userContent;

    @InjectView(R.id.fl_profileDetail)
    FrameLayout fl_profileDetail;
    @InjectView(R.id.iv_house_background)
    ImageView iv_house_background;
    @InjectView(R.id.scroll)
    ObservableScrollView mScrollView;
    @InjectView(R.id.iv_icon)
    RoundedImageView iv_icon;
    @InjectView(R.id.tv_peopleName)
    TextView tv_peopleName;
    @InjectView(R.id.tv_numFollowers)
    TextView tv_numFollowers;
    @InjectView(R.id.iv_propertyType)
    ImageView iv_propertyType;
    @InjectView(R.id.tv_propertyType)
    TextView tv_propertyType;
    //    private TextView tv_houseName;
    @InjectView(R.id.tv_houseAddress)
    TextView tv_houseAddress;

    @InjectView(R.id.tv_currencyUnit)
    TextView tv_currencyUnit;
    @InjectView(R.id.tv_propertyPriceFormattedForRoman)
    TextView tv_propertyPriceFormattedForRoman;
    @InjectView(R.id.tv_propertySize)
    TextView tv_propertySize;
    @InjectView(R.id.tv_sizeUnit)
    TextView tv_sizeUnit;
    @InjectView(R.id.tv_bedroomCount)
    TextView tv_bedroomCount;
    @InjectView(R.id.tv_bathroomCount)
    TextView tv_bathroomCount;

    @InjectView(R.id.reason2LinearLayout)
    LinearLayout reason2LinearLayout;
    @InjectView(R.id.reason3LinearLayout)
    LinearLayout reason3LinearLayout;
    @InjectView(R.id.reason1TextView)
    TextView reason1TextView;
    @InjectView(R.id.reason2TextView)
    TextView reason2TextView;
    @InjectView(R.id.reason3TextView)
    TextView reason3TextView;
    @InjectView(R.id.reason1ImageView)
    ImageView reason1ImageView;
    @InjectView(R.id.reason2ImageView)
    ImageView reason2ImageView;
    @InjectView(R.id.reason3ImageView)
    ImageView reason3ImageView;

    @InjectView(R.id.flexible_space)
    View mFlexibleSpaceView;
    @InjectView(R.id.view_cover)
    View view_cover;
    @InjectView(R.id.recycler_view)
    ComputeVerticalScrollOffAbleRecyclerView mRecyclerView;

    @InjectView(R.id.previewDetailViewPager)
    ViewPager previewDetailViewPager;

    private int mOpenGapSize;
    private int mFlexibleSpaceShowFabOffset;
    private int mFlexibleSpaceImageHeight;
    private int newsfeed_item_header_height;

    private float flexibleRange;
    private int resizeRange;
    private int circle_image_height;
    private int mFlexibleSpaceHeight;
    private int flexibleSpaceAndToolbarHeight;
    private int newsfeed_detail_background_height;

    LinearLayoutManager mLayoutManager;
    RecyclerView.Adapter mAdapter;
    ViewPager pager;
    LinearLayout pagerIndicator;

    AlphaAnimation animationBeHide_fl_profileDetail;
    AlphaAnimation animationBeVisiable_fl_profileDetail;

    public static final String EXTRA_MESSAGE = "EXTRA_MESSAGE";
    public static final String BUNDLE_NEWSFEED_TYPE = "BUNDLE_NEWSFEED_TYPE";
    public static final String BUNDLE_IS_PREVIEW = "BUNDLE_IS_PREVIEW";
    public static final String BUNDLE_AGENT_PROFILE = "BUNDLE_AGENT_PROFILE";
    public static final String BUNDLE_IMAGE_NAME = "BUNDLE_IMAGE_NAME";
    public static final String BUNDLE_PROPERTY_NAME = "BUNDLE_PROPERTY_NAME";
    public static final String EXTRA_LANG_INDEX = "EXTRA_LANG_INDEX";

    private int langIndex = 0;
    List<String> metricNameList;
    List<Integer> metricIndexList;
    boolean isPreview = false;

    private CreatePostPreviewPagerAdapter createPostPreviewPagerAdapter;

    public interface ViewInterface {
        public void onViewUpdate(String buttonTxt, String txtTxt);
    }

    public static final NewsFeedDetailFragment newInstance(String message, ViewPager pager, LinearLayout pagerIndicator, boolean isMeDetail) {
        NewsFeedDetailFragment f = new NewsFeedDetailFragment();
        Bundle bdl = new Bundle();
        bdl.putString(EXTRA_MESSAGE, message);
        f.setArguments(bdl);
        f.setViewPager(pager);
        f.setPagerIndicator(pagerIndicator);
        f.setIsMeDetail(isMeDetail);
        return f;
    }

    public NewsFeedDetailFragment newInstance(String message, boolean isMeDetail, AgentProfiles agentProfiles, String imageName, String propertyTypeName, boolean isPreview, int langIndex) {
        NewsFeedDetailFragment f = new NewsFeedDetailFragment();
        Bundle bdl = new Bundle();
        bdl.putString(EXTRA_MESSAGE, message);
        bdl.putBoolean(BUNDLE_IS_PREVIEW, isPreview);
//        bdl.putParcelable(BUNDLE_AGENT_PROFILE, agentProfiles);
        bdl.putString(BUNDLE_IMAGE_NAME, imageName);
        bdl.putString(BUNDLE_PROPERTY_NAME, propertyTypeName);
//        bdl.putInt(EXTRA_LANG_INDEX, langIndex);
        f.setArguments(bdl);
        f.setIsMeDetail(isMeDetail);
        f.setAgentProfiles(agentProfiles);
        return f;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        langIndex = AppUtil.getCurrentLangIndex(getContext());
        metricNameList = FileUtil.getSystemSetting(getContext()).obtainMetricNameList(langIndex);
        metricIndexList = FileUtil.getSystemSetting(getContext()).obtainMetricIndexList(langIndex);
        userContent = CoreData.getUserContent(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.inflater = inflater;
        View view = inflater.inflate(R.layout.fragment_newsfeed_detail, container, false);

        ButterKnife.inject(this, view);

        animationBeHide_fl_profileDetail = new AlphaAnimation(1.0f, 0.0f);
        animationBeHide_fl_profileDetail.setDuration(500);
        animationBeHide_fl_profileDetail.setStartOffset(0);
        animationBeHide_fl_profileDetail.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {

                // Mixpanel
                MixpanelAPI mixpanel = MixpanelAPI.getInstance(getActivity(), AppConfig.getMixpanelToken());
                mixpanel.track("View Posting Photos");

                view_cover.setVisibility(View.VISIBLE);
                if (pager != null) {
                    ((EnbleableSwipeViewPager) pager).setSwipeable(false);
                }
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                fl_profileDetail.setVisibility(View.GONE);
                if (pagerIndicator != null)
                    pagerIndicator.setVisibility(View.GONE);
                previewDetailViewPager.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        animationBeVisiable_fl_profileDetail = new AlphaAnimation(0.0f, 1.0f);
        animationBeVisiable_fl_profileDetail.setDuration(500);
        animationBeVisiable_fl_profileDetail.setStartOffset(0);
        animationBeVisiable_fl_profileDetail.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
//                fl_profileDetail.setVisibility(View.VISIBLE);
                view_cover.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
//                mRecyclerView.setVisibility(View.INVISIBLE);
                fl_profileDetail.setVisibility(View.VISIBLE);
                if (pagerIndicator != null)
                    pagerIndicator.setVisibility(View.VISIBLE);
                //show language switcher only for Me details and multi languages
                if (isMeDetail)
                    triggerLanguageSwitch();
                if (pager != null) {
                    ((EnbleableSwipeViewPager) pager).setSwipeable(true);
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (isMeDetail && CoreData.isAgentProfileUpdate) {
            userContent = CoreData.getUserContent(context);
            //use my profile

            agentProfilesItem = userContent.agentProfile;
            if (agentProfilesItem == null)
                return;
            propertyTypeImageName = AppUtil.getPropertyTypeImageName(userContent.agentProfile.agentListing.propertyType, userContent.agentProfile.agentListing.spaceType);
            propertyTypeName = AppUtil.getPropertyTypeHashMap(getActivity(), userContent.systemSetting.propertyTypeList).get(agentProfilesItem.agentListing.propertyType + "," + agentProfilesItem.agentListing.spaceType);

            updateDetail();

        }

        //set up language panel on bottom
        if (isMeDetail) {
            setupLanguageViewpager();
        }

        if (pager != null) {

            getView().setFocusableInTouchMode(true);
            getView().requestFocus();
            getView().setOnKeyListener(new View.OnKeyListener() {

                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {

                    if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {

                        // handle back button
                        Log.d(this.getClass().getName(), "onKey: ");
                        if (((EnbleableSwipeViewPager)pager).isSwipeable()) {
                            pager.setCurrentItem(1, true);
                            ((MainActivityTabBase) getActivity()).openTabBar();
                        }else{
                            fl_profileDetail.startAnimation(animationBeVisiable_fl_profileDetail);
//                            ((EnbleableSwipeViewPager)pager).setSwipeable(true);
                            return true;
                        }

                        return true;
                    }

                    return false;
                }
            });
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (getArguments() != null) {
            isPreview = getArguments().getBoolean(BUNDLE_IS_PREVIEW);
//            agentProfilesItem = getArguments().getParcelable(BUNDLE_AGENT_PROFILE);
            propertyTypeImageName = getArguments().getString(BUNDLE_IMAGE_NAME);
            propertyTypeName = getArguments().getString(BUNDLE_PROPERTY_NAME);
            langIndex = getArguments().getInt(EXTRA_LANG_INDEX);
        }

        mFlexibleSpaceImageHeight = getResources().getDimensionPixelSize(R.dimen.newsfeed_flexible_space_image_height);
        mFlexibleSpaceShowFabOffset = getResources().getDimensionPixelSize(R.dimen.newsfeed_flexible_space_show_fab_offset);
        mOpenGapSize = getResources().getDimensionPixelSize(R.dimen.newsfeed_detail_open_gap_size);
        newsfeed_item_header_height = getResources().getDimensionPixelSize(R.dimen.newsfeed_item_header_height);
        newsfeed_detail_background_height = getResources().getDimensionPixelSize(R.dimen.newsfeed_detail_background_height);

        circle_image_height = getResources().getDimensionPixelSize(R.dimen.newsfeed_icon_height);
        flexibleRange = mFlexibleSpaceImageHeight - mOpenGapSize;
        mFlexibleSpaceHeight = getResources().getDimensionPixelSize(R.dimen.newsfeed_detail_show_fab_offset);
        flexibleSpaceAndToolbarHeight = mFlexibleSpaceHeight + newsfeed_item_header_height;
        resizeRange = mFlexibleSpaceShowFabOffset - mOpenGapSize;

//        Log.i("", "edwin onCreateView "+circle_image_height+" "+flexibleRange+" "+mFlexibleSpaceHeight+" "+flexibleSpaceAndToolbarHeight+" "+resizeRange+" "+mOpenGapSize);

//        mScrollView = (ObservableScrollView) view.findViewById(R.id.scroll);
//        mFlexibleSpaceView = view.findViewById(R.id.flexible_space);
//        view_cover = view.findViewById(R.id.view_cover);
//        mRecyclerView = (ComputeVerticalScrollOffAbleRecyclerView) view.findViewById(R.id.recycler_view);

        mScrollView.setScrollViewCallbacks(this);
        mFlexibleSpaceView.getLayoutParams().height = newsfeed_item_header_height;

        Log.d("NewsFeedDetails", "NewsFeedDetails onViewCreated: " + mScrollView);

        if (!isPreview) {
            //setUpOverScroll ScrollView Vertical
            VerticalOverScrollBounceEffectDecorator mVerticalOverScrollBounceEffectDecorator = new VerticalOverScrollBounceEffectDecorator(new ScrollViewOverScrollDecorAdapter(mScrollView)) {

                @Override
                public void overDrag() {
                    fl_profileDetail.startAnimation(animationBeHide_fl_profileDetail);
                }

                public void onBounceBackStateCreateAnimator() {
                    FrameLayout.LayoutParams mparams = (FrameLayout.LayoutParams) iv_house_background.getLayoutParams();
                    mparams.width = FrameLayout.LayoutParams.MATCH_PARENT;
                    mparams.height = (int) (newsfeed_detail_background_height);
                    iv_house_background.setLayoutParams(mparams);

//                AnimatorSet set = new AnimatorSet();
//                set
//                        .play(ObjectAnimator.ofFloat(iv_house_background, View.Y,
//                                newsfeed_detail_background_height+200f, newsfeed_detail_background_height));
//                set.setDuration(1000);
//                set.setInterpolator(new DecelerateInterpolator());
//                set.addListener(new AnimatorListenerAdapter() {
//                    @Override
//                    public void onAnimationEnd(Animator animation) {
//                        FrameLayout.LayoutParams mparams = (FrameLayout.LayoutParams) iv_house_background.getLayoutParams();
//                        mparams.width = FrameLayout.LayoutParams.MATCH_PARENT;
//                        mparams.height = (int) (newsfeed_detail_background_height);
//                        iv_house_background.setLayoutParams(mparams);
//                    }
//
//                    @Override
//                    public void onAnimationCancel(Animator animation) {
//                    }
//                });
//                set.start();

                }

                @Override
                protected void translateView(View view, float offset) {
                    super.translateView(view, offset);

                    if (offset >= 0) {
                        FrameLayout.LayoutParams mparams = (FrameLayout.LayoutParams) iv_house_background.getLayoutParams();
                        mparams.width = FrameLayout.LayoutParams.MATCH_PARENT;
                        mparams.height = (int) (newsfeed_detail_background_height - offset);
                        iv_house_background.setLayoutParams(mparams);
                    }

                }

                @Override
                protected void translateViewAndEvent(View view, float offset, MotionEvent event) {
                    super.translateViewAndEvent(view, offset, event);
                    FrameLayout.LayoutParams mparams = (FrameLayout.LayoutParams) iv_house_background.getLayoutParams();
                    mparams.width = FrameLayout.LayoutParams.MATCH_PARENT;
                    mparams.height = (int) (newsfeed_detail_background_height - offset);
                    iv_house_background.setLayoutParams(mparams);
                }
            };
            mVerticalOverScrollBounceEffectDecorator.setMAX_DRAG_OFFSET(MAX_DRAG_OFFSET);
//        mVerticalOverScrollBounceEffectDecorator.setBounceBackView(iv_house_background);
        }

        view_cover.setVisibility(View.GONE);

        mLayoutManager = new LinearLayoutManager(this.context);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(mLayoutManager);
//        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
//                super.onScrollStateChanged(recyclerView, newState);
//
//            }
//
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                super.onScrolled(recyclerView, dx, dy);
//                Log.e("", "edwin onScrolled dy" + dy + " y " + mRecyclerView.computeVerticalScrollOffset());
//                if (dx == 0 && dy == 0) {
//                    return;
//                }
//
//                if (mRecyclerView.computeVerticalScrollOffset() == 0 && dy < 0) {
//                    fl_profileDetail.startAnimation(animationBeVisiable_fl_profileDetail);
////                    mRecyclerView.startAnimation(animationBeHide_mRecyclerView);
//                }
//
//            }
//        });

        if (!isPreview) {
            mRecyclerView.addOnItemTouchListener(
                    new RecyclerItemClickListener(context, new RecyclerItemClickListener.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {

                            // Mixpanel
                            MixpanelAPI mixpanel = MixpanelAPI.getInstance(getActivity(), AppConfig.getMixpanelToken());
                            mixpanel.track("Enlarge Posting Photo");

                            Intent intent = new Intent(context, PhotoPagerActivity.class);
                            intent.putExtra(PhotoPagerActivity.EXTRA_CURRENT_ITEM, position);
                            intent.putExtra(PhotoPagerActivity.EXTRA_PHOTOS, photoPaths);
                            intent.putExtra(PhotoPagerActivity.EXTRA_SHOW_DELETE, false);
                            Navigation.fadeIntent(activity, intent);
                            ;


                        }
                    })
            );
        }

        //setUpOverScroll RecyclerView Vertical
        new VerticalOverScrollBounceEffectDecorator(new RecyclerViewOverScrollDecorAdapter(mRecyclerView)) {

            @Override
            public void overDrag() {
                if (pagerIndicator != null)
                    pagerIndicator.setVisibility(View.VISIBLE);

                fl_profileDetail.startAnimation(animationBeVisiable_fl_profileDetail);
            }
        };

        //update Detail info
        if (isPreview && agentProfilesItem!=null) {
            BaseCreatePost baseCreatePost = BaseCreatePost.getInstance(getActivity());
            agentProfilesItem.agentListing = baseCreatePost.getCreatePostRequest();
            agentProfilesItem.agentListing.photos = baseCreatePost.getPhotoPathList();
            updateDetail();
        }

    }

    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {


        float scale = 0.7f + ScrollUtils.getFloat(((float) (resizeRange - (scrollY)) / resizeRange), 0, 1f) * MAX_TEXT_SCALE_DELTA;
        Log.i("", "edwin scale " + scale + " resizeRange " + resizeRange + " scrollY " + scrollY);

        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) iv_icon.getLayoutParams();
        params.width = (int) (circle_image_height * scale);
        params.height = (int) (circle_image_height * scale);

        iv_icon.setLayoutParams(params);

        int belowDragHight = scrollY;
        if (belowDragHight < 0) {
            FrameLayout.LayoutParams mparams = (FrameLayout.LayoutParams) iv_house_background.getLayoutParams();
            mparams.width = FrameLayout.LayoutParams.MATCH_PARENT;
            mparams.height = (int) (newsfeed_detail_background_height + belowDragHight);
            iv_house_background.setLayoutParams(mparams);
        }

//        if (isNormal && scrollY == 0) {
//            isNormal = false;
//
//            fl_profileDetail.startAnimation(animationBeHide_fl_profileDetail);
////                mRecyclerView.startAnimation(animationBeVisiable_mRecyclerView);
//
//            mScrollView.postDelayed(new Runnable() {
//                @Override
//                public void run() {
////                        mScrollView.smoothScrollTo(0, mOpenGapSize);
//                    isNormal = true;
//                }
//            }, 500);
//
//        }

    }

    @Override
    public void onDownMotionEvent() {

    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {
    }

    public void update(AgentProfiles item, int position, String imageName, String propertyTypeName) {
        this.agentProfilesItem = item;
        this.propertyTypeImageName = imageName;
        this.propertyTypeName = propertyTypeName;

    }

    public void updateUI() {
        Log.d("NewsFeedDetails", "updateUI: " + MemberID);

        if (agentProfilesItem == null)
            return;
        updateDetail();
    }

    public void updateDetail() {

        Log.d("NewsFeedDetails", "updateDetail: " + agentProfilesItem.memberID + " " + mScrollView);
        if (mScrollView == null || (agentProfilesItem.memberID==MemberID &&  !CoreData.isAgentProfileUpdate))
            return;

        CoreData.isAgentProfileUpdate = false;

        MemberID = agentProfilesItem.memberID;

        //reset agent info UI posotion
        mScrollView.setScrollY(0);
        // Mixpanel
        MixpanelAPI mixpanel = MixpanelAPI.getInstance(getActivity(), AppConfig.getMixpanelToken());
        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("Agent member ID", agentProfilesItem.memberID);
            jsonObject.put("Agent name", agentProfilesItem.memberName);

        } catch (JSONException e) {

        }
        mixpanel.track("Viewed Posting", jsonObject);

        Log.d("NewsFeedDetails", "updateDetail: " + agentProfilesItem.agentListing);
        Log.d("NewsFeedDetails", "updateDetail: " + agentProfilesItem.agentListing.photos);

//        subPhotosList = agentProfile.agentListing.photos;
        if (agentProfilesItem.agentListing == null)
            return;
        if (agentProfilesItem.agentListing.photos == null)
            return;
        if (agentProfilesItem.agentListing.photos.size() > 1) {
            subPhotosList = agentProfilesItem.agentListing.photos.subList(1, agentProfilesItem.agentListing.photos.size());

            photoPaths.clear();
            for (AgentListing.Photo subPohoto : subPhotosList) {
                photoPaths.add(subPohoto.url);
            }

            mAdapter = new NewsFeed_AgentProfilesPhotoListAdapter(context, subPhotosList);
            mRecyclerView.setAdapter(mAdapter);
        }


        if (agentProfilesItem.agentListing.photos.size() > 1) {
            String coverImgPath = agentProfilesItem.agentListing.photos.get(1).url;
            if (isPreview)
                Picasso.with(getActivity()).load(new File(coverImgPath)).placeholder(R.drawable.progress_animation).into(iv_house_background);
            else
                Picasso.with(getActivity()).load(coverImgPath).placeholder(R.drawable.progress_animation).into(iv_house_background);
        }

        //set the profile pic for agent or myself
        String profileUrl = agentProfilesItem.memberID == userContent.memId ? userContent.photoUrl : agentProfilesItem.agentPhotoURL;
        Picasso.with(context).load(profileUrl).placeholder(R.drawable.default_profile).error(R.drawable.default_profile)
                .into(iv_icon);
        tv_peopleName.setText(agentProfilesItem.memberName + "");
        tv_numFollowers.setText(agentProfilesItem.followerCount + "");

        iv_propertyType.setImageResource(context.getResources().getIdentifier(propertyTypeImageName, "drawable", context.getPackageName()));
        tv_propertyType.setText(propertyTypeName);
        if (agentProfilesItem.agentListing.googleAddresses.size() > 0)
            tv_houseAddress.setText(agentProfilesItem.agentListing.googleAddresses.get(0).address);

        tv_currencyUnit.setText(agentProfilesItem.agentListing.currency + "");
        tv_propertyPriceFormattedForRoman.setText(agentProfilesItem.agentListing.propertyPriceFormattedForRoman + "");
        tv_propertySize.setText(agentProfilesItem.agentListing.propertySize + "");
        tv_sizeUnit.setText(metricNameList.get(AppUtil.getMeticPositionFromIndex(metricIndexList, agentProfilesItem.agentListing.sizeUnitType)) + "");

        tv_bedroomCount.setText(agentProfilesItem.agentListing.bedroomCount + "");
        tv_bathroomCount.setText(agentProfilesItem.agentListing.bathroomCount + "");

        //set blue border on profile pic
        setFollowing();

        for (RequestListing.Reason reason : agentProfilesItem.agentListing.reasons) {
            if (reason.reason2 != null && !reason.reason2.isEmpty() ) {
                reason2LinearLayout.setVisibility(View.VISIBLE);
            } else {
                reason2LinearLayout.setVisibility(View.GONE);
            }
            if (reason.reason3 != null && !reason.reason3.isEmpty()) {
                reason3LinearLayout.setVisibility(View.VISIBLE);
            } else {
                reason3LinearLayout.setVisibility(View.GONE);
            }
            reason1TextView.setText(reason.reason1);
            reason2TextView.setText(reason.reason2);
            reason3TextView.setText(reason.reason3);
        }

        if (isPreview) {
            //may show multi language
            initPreview(langIndex);
        } else {
            for (RequestListing.Reason reason : agentProfilesItem.agentListing.reasons) {
                if (reason.mediaURL1 != null &&
                        !reason.mediaURL1.isEmpty()) {
                    Picasso.with(getActivity())
                            .load(reason.mediaURL1)
                            .placeholder(R.drawable.progress_animation)
                            .into(reason1ImageView);
                    reason1ImageView.setVisibility(View.VISIBLE);
                } else {
                    reason1ImageView.setVisibility(View.GONE);
                }

                if (reason.mediaURL2 != null &&
                        !reason.mediaURL2.isEmpty()) {
                    Picasso.with(getActivity())
                            .load(reason.mediaURL2)
                            .placeholder(R.drawable.progress_animation)
                            .into(reason2ImageView);
                    reason2ImageView.setVisibility(View.VISIBLE);
                    reason2LinearLayout.setVisibility(View.VISIBLE);
                } else {
                    reason2ImageView.setVisibility(View.GONE);
                }

                if (reason.mediaURL3 != null &&
                        !reason.mediaURL3.isEmpty()) {
                    Picasso.with(getActivity())
                            .load(reason.mediaURL3)
                            .placeholder(R.drawable.progress_animation)
                            .into(reason3ImageView);
                    reason3ImageView.setVisibility(View.VISIBLE);
                    reason3LinearLayout.setVisibility(View.VISIBLE);
                } else {
                    reason3ImageView.setVisibility(View.GONE);
                }
            }
        }


    }

    public void updateDetail(int langIndex) {
        initPreview(langIndex);
    }


    private void initPreview(int langIndex) {
        previewDetailViewPager.setVisibility(View.GONE);
        BaseCreatePost baseCreatePost = BaseCreatePost.getInstance(getActivity());
        Log.d("initPreview", "initPreview: " + baseCreatePost.getCreatePostRequest());
        tv_currencyUnit.setText(baseCreatePost.getCreatePostRequest().currency + "");
        tv_propertyPriceFormattedForRoman.setText(MathUtil.getRomanFormat(baseCreatePost.getCreatePostRequest().propertyPrice));
        tv_propertySize.setText(String.valueOf(baseCreatePost.getCreatePostRequest().propertySize) + " " + baseCreatePost.getCreatePostRequest().sizeUnit);
        tv_sizeUnit.setText("");
        tv_bedroomCount.setText(String.valueOf(String.valueOf(baseCreatePost.getCreatePostRequest().bedroomCount)));
        tv_bathroomCount.setText(String.valueOf(String.valueOf(baseCreatePost.getCreatePostRequest().bathroomCount)));
        String propertyTypeImageName = AppUtil.getPropertyTypeImageName(baseCreatePost.getCreatePostRequest().propertyType,
                baseCreatePost.getCreatePostRequest().spaceType);
        String propertyTypeName = AppUtil.getPropertyTypeHashMap(getActivity(), userContent.systemSetting.propertyTypeList).get(baseCreatePost.getCreatePostRequest().propertyType + "," + baseCreatePost.getCreatePostRequest().spaceType);
        iv_propertyType.setImageResource(getResources().getIdentifier(propertyTypeImageName, "drawable", getActivity().getPackageName()));
        tv_propertyType.setText(propertyTypeName);
        for (RequestListing.Reason reason : baseCreatePost.getCreatePostRequest().reasons) {
            if (reason.languageIndex == langIndex) {
                if (reason.reason2 != null && !reason.reason2.isEmpty() ) {
                    reason2LinearLayout.setVisibility(View.VISIBLE);
                } else {
                    reason2LinearLayout.setVisibility(View.GONE);
                }
                if (reason.reason3 != null && !reason.reason3.isEmpty()) {
                    reason3LinearLayout.setVisibility(View.VISIBLE);
                } else {
                    reason3LinearLayout.setVisibility(View.GONE);
                }
                reason1TextView.setText(reason.reason1);
                reason2TextView.setText(reason.reason2);
                reason3TextView.setText(reason.reason3);
            }
        }

        for (BaseCreatePost.ReasonsDespImage reasonsDespImage : baseCreatePost.getReasonsDespImageList()) {
            if (reasonsDespImage.langIndex == langIndex) {
                if (imagePathIsValid(reasonsDespImage.reason1ImagePath)) {
                    reason1ImageView.setVisibility(View.VISIBLE);
                    reason1ImageView.setImageBitmap(ImageUtil.decodeSampledBitmapFromResource(reasonsDespImage.reason1ImagePath));
                } else {
                    reason1ImageView.setVisibility(View.GONE);
                }
                if (imagePathIsValid(reasonsDespImage.reason2ImagePath)) {
                    reason2LinearLayout.setVisibility(View.VISIBLE);
                    reason2ImageView.setVisibility(View.VISIBLE);
                    reason2ImageView.setImageBitmap(ImageUtil.decodeSampledBitmapFromResource(reasonsDespImage.reason2ImagePath));
                } else {
                    reason2ImageView.setVisibility(View.GONE);
                }
                if (imagePathIsValid(reasonsDespImage.reason3ImagePath)) {
                    reason3LinearLayout.setVisibility(View.VISIBLE);
                    reason3ImageView.setVisibility(View.VISIBLE);
                    reason3ImageView.setImageBitmap(ImageUtil.decodeSampledBitmapFromResource(reasonsDespImage.reason3ImagePath));
                } else {
                    reason3ImageView.setVisibility(View.GONE);
                }
            }

        }

        if (BaseCreatePost.getInstance(getActivity()) == null)
            return;
        if (BaseCreatePost.getInstance(getActivity()).getCreatePostRequest() == null)
            return;
        if (BaseCreatePost.getInstance(getActivity()).getCreatePostRequest().googleAddressList == null)
            return;
        if (BaseCreatePost.getInstance(getActivity()).getCreatePostRequest().googleAddressList.get(0) == null)
            return;
        if (BaseCreatePost.getInstance(getActivity()).getCreatePostRequest().googleAddressList.get(0).address == null)
            return;

        String street = "";
        street = BaseCreatePost.getInstance(getActivity()).getCreatePostRequest().googleAddressList.get(0).address.formattedAddress;
        tv_houseAddress.setText(street);


    }


    private void initDetailWithLanguage(int langIndex) {
        AgentListing mAgentListing = userContent.agentProfile.agentListing;
        tv_currencyUnit.setText(mAgentListing.currency + "");
        tv_propertyPriceFormattedForRoman.setText(MathUtil.getRomanFormat(mAgentListing.propertyPrice));
        tv_propertySize.setText(String.valueOf(mAgentListing.propertySize));
        tv_sizeUnit.setText(metricNameList.get(AppUtil.getMeticPositionFromIndex(metricIndexList, agentProfilesItem.agentListing.sizeUnitType)) + "");
        tv_bedroomCount.setText(String.valueOf(String.valueOf(mAgentListing.bedroomCount)));
        tv_bathroomCount.setText(String.valueOf(String.valueOf(mAgentListing.bathroomCount)));
        String propertyTypeImageName = AppUtil.getPropertyTypeImageName(mAgentListing.propertyType,
                mAgentListing.spaceType);
        String propertyTypeName = AppUtil.getPropertyTypeHashMap(getActivity(), userContent.systemSetting.propertyTypeList).get(mAgentListing.propertyType + "," + mAgentListing.spaceType);
        iv_propertyType.setImageResource(getResources().getIdentifier(propertyTypeImageName, "drawable", getActivity().getPackageName()));
        tv_propertyType.setText(propertyTypeName);
        for (RequestListing.Reason reason : mAgentListing.reasons) {
            if (reason.languageIndex == langIndex) {
                if (reason.reason2 != null && !reason.reason2.isEmpty()) {
                    reason2LinearLayout.setVisibility(View.VISIBLE);
                } else {
                    reason2LinearLayout.setVisibility(View.GONE);
                }
                if (reason.reason3 != null && !reason.reason3.isEmpty()) {
                    reason3LinearLayout.setVisibility(View.VISIBLE);
                } else {
                    reason3LinearLayout.setVisibility(View.GONE);
                }
                reason1TextView.setText(reason.reason1);
                reason2TextView.setText(reason.reason2);
                reason3TextView.setText(reason.reason3);
            }
        }

        for (RequestListing.Reason reasonsDespImage : mAgentListing.reasons) {
            if (reasonsDespImage.languageIndex == langIndex) {
                if (imagePathIsValid(reasonsDespImage.mediaURL1)) {
                    reason1ImageView.setVisibility(View.VISIBLE);
                    Picasso.with(getActivity()).load(reasonsDespImage.mediaURL1).into(reason1ImageView);
                } else {
                    reason1ImageView.setVisibility(View.GONE);
                }
                if (imagePathIsValid(reasonsDespImage.mediaURL2)) {
                    reason2ImageView.setVisibility(View.VISIBLE);
                    reason2LinearLayout.setVisibility(View.VISIBLE);
                    Picasso.with(getActivity()).load(reasonsDespImage.mediaURL2).into(reason2ImageView);
                } else {
                    reason2ImageView.setVisibility(View.GONE);
                }
                if (imagePathIsValid(reasonsDespImage.mediaURL3)) {
                    reason3ImageView.setVisibility(View.VISIBLE);
                    reason3LinearLayout.setVisibility(View.VISIBLE);
                    Picasso.with(getActivity()).load(reasonsDespImage.mediaURL3).into(reason3ImageView);
                } else {
                    reason3ImageView.setVisibility(View.GONE);
                }
            }

        }

        if (BaseCreatePost.getInstance(getActivity()) == null)
            return;
        if (BaseCreatePost.getInstance(getActivity()).getCreatePostRequest() == null)
            return;
        if (BaseCreatePost.getInstance(getActivity()).getCreatePostRequest().googleAddressList == null)
            return;
        if (BaseCreatePost.getInstance(getActivity()).getCreatePostRequest().googleAddressList.size() == 0)
            return;
        if (BaseCreatePost.getInstance(getActivity()).getCreatePostRequest().googleAddressList.get(0) == null)
            return;
        if (BaseCreatePost.getInstance(getActivity()).getCreatePostRequest().googleAddressList.get(0).address == null)
            return;

        String street = "";
//        street = BaseCreatePost.getInstance(getActivity()).getCreatePostRequest().googleAddressList.get(0).address.formattedAddress;
//        tv_houseAddress.setText(street);

        if (userContent.agentProfile.agentListing.googleAddresses.size() > 0)
            tv_houseAddress.setText(userContent.agentProfile.agentListing.googleAddresses.get(0).address);


    }


    private boolean imagePathIsValid(String imagePath) {
        Log.d(TAG, "imagePathIsValid: " + imagePath);
        if (imagePath != null && !imagePath.isEmpty()) {
            return true;
        }
        return false;
    }

    void setViewPager(ViewPager pager) {
        this.pager = pager;
    }

    void setPagerIndicator(LinearLayout pagerIndicator) {
        this.pagerIndicator = pagerIndicator;
    }

    void setIsMeDetail(boolean isMeDetail) {
        this.isMeDetail = isMeDetail;
    }

    void setAgentProfiles(AgentProfiles agentProfiles) {
        this.agentProfilesItem = agentProfiles;
    }

    public void setFollowing() {
        //set blue border on profile pic
            if (iv_icon != null && agentProfilesItem != null) {
                if (agentProfilesItem.isFollowing==1) {
                    iv_icon.setImageBorder(Constants.STYLE_TYPE.AGENT_FOLLOWED);
                } else {
                    iv_icon.setImageBorder(Constants.STYLE_TYPE.AGENT_UNFOLLOWED);

                }
            }

        tv_numFollowers.setText(String.valueOf(agentProfilesItem.followerCount));
    }

    public String getTitileName() {
        if (agentProfilesItem != null && agentProfilesItem.agentListing.googleAddresses != null && agentProfilesItem.agentListing.googleAddresses.size() > 0) {
            return agentProfilesItem.agentListing.googleAddresses.get(0).name;
        }
        return "";
    }

    private void setupLanguageViewpager() {

        if (userContent.agentProfile.agentListing == null) {
            previewDetailViewPager.setVisibility(View.GONE);
            return;
        }

        //hide the language switcher when there is one language ONLY
        if (userContent.agentProfile.agentListing.reasons.size() <= 1) {
            previewDetailViewPager.setVisibility(View.GONE);
            return;
        } else {
            previewDetailViewPager.setVisibility(View.VISIBLE);
        }


        createPostPreviewPagerAdapter = new CreatePostPreviewPagerAdapter(getActivity(), getActivity().getSupportFragmentManager());
        final ArrayList<Integer> langIndexList = getLangIndexList();
        Log.d(TAG, "langIndexList: " + langIndexList.toString());
        createPostPreviewPagerAdapter.setLangIndexList(langIndexList);
        createPostPreviewPagerAdapter.setLangNameList(getLangStringList(langIndexList, userContent));
        previewDetailViewPager.setAdapter(createPostPreviewPagerAdapter);
        ViewGroup.LayoutParams viewpagerParams = previewDetailViewPager
                .getLayoutParams();
//        viewpagerParams.height = ImageUtil.getScreenHeight(this) * 3 / 4;
        //define the height of the language switch bar
        viewpagerParams.height = 150;
        previewDetailViewPager.setLayoutParams(viewpagerParams);

        //enhance performance
        previewDetailViewPager.setOffscreenPageLimit(3);

//        previewDetailViewPager.addOnPageChangeListener(onPageChangeListener);
        previewDetailViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                Log.d(TAG, "onPageSelected: selected: " + position);
                initDetailWithLanguage(langIndexList.get(position));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        //init position as first item
        initDetailWithLanguage(langIndexList.get(0));
    }

    private void triggerLanguageSwitch() {
        //hide the language switcher when there is one language ONLY
        if (userContent.agentProfile.agentListing != null) {
            if (userContent.agentProfile.agentListing.reasons.size() <= 1) {
                previewDetailViewPager.setVisibility(View.GONE);
            } else {
                previewDetailViewPager.setVisibility(View.VISIBLE);
            }
        }
    }

    private ArrayList<Integer> getLangIndexList() {
        ArrayList<Integer> langIndexList = new ArrayList<>();
        for (RequestListing.Reason reason : userContent.agentProfile.agentListing.reasons) {
            langIndexList.add(reason.languageIndex);
        }
        return langIndexList;
    }

    private ArrayList<String> getLangStringList(ArrayList<Integer> langIndexList, ResponseLoginSocial.Content userContent) {
        ArrayList<String> langStringList = new ArrayList<>();

        for (Integer langIndex : langIndexList) {
            for (SystemSetting.SystemLanguage systemLanguage : userContent.systemSetting.sysLangList) {
                if (systemLanguage.index == langIndex) {
                    langStringList.add(systemLanguage.lang);
                }
            }
        }
        return langStringList;
    }

    private MainActivityTabBase.CURRENT_SECTION getCurrentSection(){
        return ((MainActivityTabBase)getActivity()).currentSection;
    }
}
