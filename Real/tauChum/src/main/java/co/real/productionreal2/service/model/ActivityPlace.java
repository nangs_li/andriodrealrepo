package co.real.productionreal2.service.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by kelvinsun on 15/1/16.
 */
public class ActivityPlace implements Parcelable {
    @SerializedName("Location")
    public String location;
    @SerializedName("Percentage")
    public Float percentage;

    public ActivityPlace(Parcel source) {
        location = source.readString();
        percentage = source.readFloat();
    }


    public static final Parcelable.Creator<ActivityPlace> CREATOR = new Parcelable.Creator<ActivityPlace>() {
        @Override
        public ActivityPlace createFromParcel(Parcel in) {
            return new ActivityPlace(in);
        }

        @Override
        public ActivityPlace[] newArray(int size) {
            return new ActivityPlace[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(location);
        dest.writeFloat(percentage);
    }
}