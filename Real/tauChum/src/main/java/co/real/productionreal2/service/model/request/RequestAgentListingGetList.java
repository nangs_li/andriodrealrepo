package co.real.productionreal2.service.model.request;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.model.InputRequest;
import co.real.productionreal2.service.OperationService;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.response.ResponseAgentListingGetList;
import co.real.productionreal2.util.NetworkUtil;
import co.real.productionreal2.view.Dialog;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by hohojo on 25/1/2016.
 */
public class RequestAgentListingGetList extends RequestBase {
    public RequestAgentListingGetList(int memId, String session, ArrayList<Integer> listingIdlist) {
        super(memId, session);
        this.listingIdlist = listingIdlist;
    }

    @SerializedName("ListingIDlist")
    public List<Integer> listingIdlist;

    public void callAgentListingGetListApi(final Context context, final ApiCallback callback) {
        String json = new Gson().toJson(this);
        final OperationService weatherService = new OperationService(context);
        weatherService.getCoffeeService().agentListingGetList(new InputRequest(json), new Callback<ApiResponse>() {
            @Override
            public void success(ApiResponse apiResponse, Response response) {
                ResponseAgentListingGetList responseGson = new Gson().fromJson(apiResponse.jsonContent, ResponseAgentListingGetList.class);
                if (responseGson.errorCode == 4) {
                    Dialog.accessErrorForceLogoutDialog(context).show();
                } else if (responseGson.errorCode == 26) {
                    Dialog.suspendedErrorForceLogoutDialog(context).show();
                } else if (responseGson.errorCode != 0 && responseGson.errorCode != 20 && responseGson.errorCode != 21 &&
                        responseGson.errorCode != 22 && responseGson.errorCode != 23) {
                    callback.failure(responseGson.errorMsg);
                } else {
                    callback.success(apiResponse);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                callback.failure("");
                NetworkUtil.showNoNetworkMsg(context);
            }
        });
    }
}
