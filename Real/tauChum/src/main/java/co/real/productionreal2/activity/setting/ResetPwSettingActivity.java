package co.real.productionreal2.activity.setting;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.BaseActivity;
import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.callback.DialogCallback;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.request.RequestRealNetworkSendResetPwEmail;
import co.real.productionreal2.util.ValidUtil;
import co.real.productionreal2.view.Dialog;

public class ResetPwSettingActivity extends BaseActivity {
    @InjectView(R.id.headerLayout)
    View headerLayout;
    @InjectView(R.id.emailEditText)
    EditText emailEditText;
    @InjectView(R.id.errorTextView)
    TextView errorTextView;

    private ProgressDialog ringProgressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_pw_setting);
        ButterKnife.inject(this);

        emailEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    pressDone();
                }
                return false;
            }
        });

        initView();

    }

    private void initView() {
        TextView titleTextView = (TextView)headerLayout.findViewById(R.id.titleTextView);
        titleTextView.setText(R.string.reset_password__title);
        errorTextView.setVisibility(View.INVISIBLE);
    }

    @OnClick(R.id.changePwBtn)
    public void changePwBtnClick(View view) {
        if (ringProgressDialog != null && ringProgressDialog.isShowing())
            return;
        pressDone();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    void pressDone(){

        if (emailEditText.getText().toString().isEmpty()) {
            errorTextView.setText(R.string.error_message__email_cannot_be);
            errorTextView.setVisibility(View.VISIBLE);
            return;
        } else {
            if (ValidUtil.isValidEmail(emailEditText.getText().toString())) {

                ringProgressDialog = ProgressDialog.show(this, getResources().getString(R.string.alert__Initializing),
                        getResources().getString(R.string.error_message__change_password_success));

                errorTextView.setVisibility(View.INVISIBLE);

                RequestRealNetworkSendResetPwEmail requestRealNetworkResetPw = new RequestRealNetworkSendResetPwEmail(emailEditText.getText().toString());
                requestRealNetworkResetPw.callRealNetworkSendResetPasswordEmail(this, new ApiCallback() {
                    @Override
                    public void success(ApiResponse apiResponse) {

                        if (ringProgressDialog != null) {
                            ringProgressDialog.dismiss();
                            ringProgressDialog.cancel();
                            ringProgressDialog = null;
                        }
                        Dialog.feedbackDialog(ResetPwSettingActivity.this, true,
                                getString(R.string.error_message__reset_email_sent),
                                getString(R.string.error_message__check_email_follow_instructions),
                                new DialogCallback() {
                                    @Override
                                    public void yes() {
                                        onBackPressed();
                                    }
                                }).show();
                    }

                    @Override
                    public void failure(String errorMsg) {
                        if (ringProgressDialog != null) {
                            ringProgressDialog.dismiss();
                            ringProgressDialog.cancel();
                            ringProgressDialog = null;
                        }
                        Dialog.feedbackDialog(ResetPwSettingActivity.this, true,
                                "",
                                errorMsg,
                                new DialogCallback() {
                                    @Override
                                    public void yes() {

                                    }
                                }).show();
                    }
                });
            } else {
                errorTextView.setText(R.string.error_message__please_enter_valid);
                errorTextView.setVisibility(View.VISIBLE);
                return;
            }
        }
    }
}
