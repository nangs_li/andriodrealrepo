package co.real.productionreal2.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

public class RemoveAgentProfileItemDialog extends DialogFragment{

	private YesNoDialogListener dialogListener;
    Context mContext;
    public RemoveAgentProfileItemDialog() {
        // Empty constructor required for DialogFragment
    	mContext = getActivity();
    }
    
    
    
    
    public YesNoDialogListener getDialogListener() {
		return dialogListener;
	}

	public void setDialogListener(YesNoDialogListener dialogListener) {
		this.dialogListener = dialogListener;
	}




	public interface YesNoDialogListener {
        void onYesPressesd();
        void onNoPressesd();
    }

    public static RemoveAgentProfileItemDialog newInstance(String title, int index) {
    	RemoveAgentProfileItemDialog frag = new RemoveAgentProfileItemDialog();
        Bundle args = new Bundle();
        args.putString("title", title);
        args.putInt("index", index);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
    	
    	
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle("Warming");
        alertDialogBuilder.setMessage("Are you sure to remove this item?");
        //null should be your on click listener
        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
            	dialogListener.onYesPressesd();
            	  dialog.dismiss();
            }
        });
        alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
            	dialogListener.onNoPressesd();
                dialog.dismiss();
            }
        });


        return alertDialogBuilder.create();
    }
    
    
}

