package co.real.productionreal2.fragment;

import android.app.DialogFragment;
import android.content.Context;
import android.graphics.drawable.Animatable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import co.real.productionreal2.R;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by kelvinsun on 8/1/16.
 */
public class TutorialDialogFragment extends DialogFragment implements View.OnClickListener{

    public static Context context;

    @InjectView(R.id.iv_swipe_action)
    protected ImageView ivSwipeAction;
    @InjectView(R.id.btn_start)
    protected Button btnStart;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        context = getActivity();

        View view = inflater.inflate(R.layout.fragment_tutorial, container, false);
        ButterKnife.inject(this, view);
        addListener();
        return view;
    }


    @Override
    public void onStart() {
        super.onStart();
        Window window = getDialog().getWindow();
        WindowManager.LayoutParams windowParams = window.getAttributes();
        windowParams.dimAmount = 0.0f;

        window.setAttributes(windowParams);
        window.setWindowAnimations(R.style.dialog_fade_animation);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    }

    @Override
    public void onResume() {
        super.onResume();
        initView();
    }

    private void initView() {
        ((Animatable) ivSwipeAction.getDrawable()).stop();
        ((Animatable) ivSwipeAction.getDrawable()).start();
    }

    private void addListener() {
        btnStart.setOnClickListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        ((Animatable) ivSwipeAction.getDrawable()).stop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ((Animatable) ivSwipeAction.getDrawable()).stop();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        this.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        this.setStyle(DialogFragment.STYLE_NO_FRAME, 0);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onClick(View v) {
        if (v == btnStart) {
            this.dismiss();
        }
    }
}
