package co.real.productionreal2.setting;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import co.real.productionreal2.util.FileUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kelvinsun on 30/12/15.
 */
public class SearchFilterSetting {

    private static SearchFilterSetting searchFilterSetting;
    private List<String> spaceTypeIndexList = new ArrayList<>();
    private BigDecimal price = BigDecimal.ZERO;
    private BigDecimal priceMax = BigDecimal.ZERO;
    private int priceMin = 0;
    private int size = 0;
    private int sizeMax;
    private int sizeMin;
    private int bedroomCount = 0;
    private int bathroomCount = 0;
    private int spokenLangIndex = -1;
    private int currencyUnit = 0;
    private int sizeUnitType = -1;
    private int priceApproximation = 0;   //0=above, 1=around, 2=below
    private int sizeApproximation = 0;
    private int count = 0;

    private SearchFilterSetting() {
        //ToDo here
    }

    public static SearchFilterSetting getInstance() {
        if (searchFilterSetting == null) {
            searchFilterSetting = new SearchFilterSetting();
        }
        return searchFilterSetting;
    }

    public int getSizeMax() {
        return sizeMax;
    }

    public void setSizeMax(int sizeMax) {
        this.sizeMax = sizeMax;
    }

    public int getSizeMin() {
        return sizeMin;
    }

    public void setSizeMin(int sizeMin) {
        this.sizeMin = sizeMin;
    }

    public int getBedroomCount() {
        return bedroomCount;
    }

    public void setBedroomCount(int bedroomCount) {
        this.bedroomCount = bedroomCount;
    }

    public int getBathroomCount() {
        return bathroomCount;
    }

    public void setBathroomCount(int bathroomCount) {
        this.bathroomCount = bathroomCount;
    }

    public int getSpokenLangIndex() {
        return spokenLangIndex;
    }

    public void setSpokenLangIndex(int spokenLangIndex) {
        this.spokenLangIndex = spokenLangIndex;
    }

    public int getCurrencyUnit() {
        return currencyUnit;
    }

    public void setCurrencyUnit(int currencyUnit) {
        this.currencyUnit = currencyUnit;
    }

    public int getSizeUnitType() {
        return sizeUnitType;
    }

    public void setSizeUnitType(int sizeUnitType) {
        this.sizeUnitType = sizeUnitType;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getPriceMax() {
        return priceMax;
    }

    public void setPriceMax(BigDecimal priceMax) {
        this.priceMax = priceMax;
    }

    public int getPriceMin() {
        return priceMin;
    }

    public void setPriceMin(int priceMin) {
        this.priceMin = priceMin;
    }

    public int getPriceApproximation() {
        return priceApproximation;
    }

    public void setPriceApproximation(int priceApproximation) {
        this.priceApproximation = priceApproximation;
    }

    public int getSizeApproximation() {
        return sizeApproximation;
    }

    public void setSizeApproximation(int sizeApproximation) {
        this.sizeApproximation = sizeApproximation;
    }

    public List<String> getSpaceTypeIndexList() {
        return spaceTypeIndexList;
    }

    public void setSpaceTypeIndexList(List<String> spaceTypeIndexList) {
        this.spaceTypeIndexList = spaceTypeIndexList;
    }

    public void clear() {
        searchFilterSetting = null;
    }

    public static class PriceSet implements Serializable, Parcelable {

        @SerializedName("PriceMax")
        public String priceMax;
        @SerializedName("PriceMin")
        public String priceMin;

        public PriceSet(Parcel source) {
            priceMax = source.readString();
            priceMin = source.readString();
        }


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(priceMax);
            dest.writeString(priceMin);
        }

        public static final Parcelable.Creator<PriceSet> CREATOR = new Creator<PriceSet>() {
            public PriceSet createFromParcel(Parcel source) {
                return new PriceSet(source);
            }

            @Override
            public PriceSet[] newArray(int size) {
                return new PriceSet[size];
            }
        };

    }


    public int getAlertCount() {
        count = 0;
        if (spaceTypeIndexList.size()>0) {
            count++;
        }
        if (price.compareTo(BigDecimal.ZERO)!=0) {
            count++;
        }
        if (size != 0) {
            count++;
        }
        if (bedroomCount != 0) {
            count++;
        }
        if (bathroomCount != 0) {
            count++;
        }
        if (spokenLangIndex > 0) {
            count++;
        }

        return count;
    }

    public JSONObject getFilterJson(Context context) {

        final List<String> currencyArrayList = FileUtil.getSystemSetting(context).obtainCurrencyNameList();
        JSONArray propertyTypeSetArr = new JSONArray();
        JSONArray priceTypeSetArr = new JSONArray();
        JSONObject object = new JSONObject();

        try {

            for (String index : spaceTypeIndexList) {
                String[] split=index.split(",");
                JSONObject propertyTypeObj = new JSONObject();
                propertyTypeObj.put("PropertyType", Integer.parseInt(split[0]));
                propertyTypeObj.put("SpaceType", Integer.parseInt(split[1]));
                propertyTypeSetArr.put(propertyTypeObj);
            }
            if (propertyTypeSetArr.length()>0) {
                object.put("PropertyTypeSet", propertyTypeSetArr);
            }

            if (getPrice().compareTo(BigDecimal.ZERO)!=0) {
                JSONObject priceTypeObj = new JSONObject();
                priceTypeObj.put("PriceMax", priceMax);
                priceTypeObj.put("PriceMin", priceMin);
                priceTypeSetArr.put(priceTypeObj);
                object.put("PriceSet", priceTypeSetArr);
                object.put("CurrencyUnit", currencyArrayList.get(getCurrencyUnit()));
            }

            if (getSize()!=0) {
                object.put("SizeMax", sizeMax);
                object.put("SizeMin", sizeMin);
                object.put("SizeUnitType", getSizeUnitType());
            }

            if (getBedroomCount() != 0)
                object.put("BedroomCount", getBedroomCount());

            if (getBathroomCount() != 0)
                object.put("BathroomCount", getBathroomCount());

            if (getSpokenLangIndex() >0)
                object.put("SpokenLanguageIndex", getSpokenLangIndex());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return object;
    }

    public void resetValues() {
        spaceTypeIndexList = new ArrayList<>();
        price = BigDecimal.ZERO;
        size = 0;
        sizeMax = 0;
        sizeMin = 0;
        bedroomCount = 0;
        bathroomCount = 0;
        spokenLangIndex = 0;
        currencyUnit = 0;
        sizeUnitType = -1;
        count = 0;
        priceApproximation = 0;
        sizeApproximation = 0;
    }
}

