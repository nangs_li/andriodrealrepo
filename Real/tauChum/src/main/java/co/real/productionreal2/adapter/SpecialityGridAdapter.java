package co.real.productionreal2.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;

import co.real.productionreal2.R;
import co.real.productionreal2.service.model.AgentSpecialty;

public class SpecialityGridAdapter extends BaseAdapter {
	private  List<AgentSpecialty> allElementDetails;
	SpecAdapterInterface buttonListener;
	private LayoutInflater mInflater;

	public SpecialityGridAdapter(Context context, SpecAdapterInterface buttonListener) {
	    mInflater = LayoutInflater.from(context);
	    this.buttonListener = buttonListener;
	}

	
	
	




	public void setAllElementDetails(
			List<AgentSpecialty> allElementDetails) {
		this.allElementDetails = allElementDetails;
		this.notifyDataSetChanged();
	}




	public int getCount() {
	    return allElementDetails.size();        
	}

	public Object getItem(int position) {
	    return allElementDetails.get(position);
	}

	public long getItemId(int position) {
	    return position;
	}

	public View getView(int position, View view, ViewGroup parent) 
	{
		ViewHolder holder;
	    if (view != null) {
	      holder = (ViewHolder) view.getTag();
	    } else {
	      view = mInflater.inflate(R.layout.specialty_token, parent, false);
	      holder = new ViewHolder(view);
	      view.setTag(holder);
	    }

	    AgentSpecialty specialty  = allElementDetails.get(position);
	    holder.delete.setTag(specialty.specialty);
	    holder.name.setText(specialty.specialty);
	    holder.delete.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				buttonListener.specOnClick(v);
				
			}
	    	
	    });
	    // etc...

	    return view;
	}    

	 static class ViewHolder {
		    @InjectView(R.id.name) TextView name;
		    @InjectView(R.id.delete) ImageView delete ;

		    public ViewHolder(View view) {
		      ButterKnife.inject(this, view);
		    }
		  }
	 public interface SpecAdapterInterface {
		 
		 public void specOnClick(View v);
		 
	 }
	
}