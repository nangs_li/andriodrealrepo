/*
 * Copyright 2014 Soichiro Kashima
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package co.real.productionreal2.newsfeed;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.nhaarman.listviewanimations.appearance.simple.AlphaInAnimationAdapter;
import com.nhaarman.listviewanimations.itemmanipulation.swipedismiss.undo.SimpleSwipeUndoAdapter;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import co.real.productionreal2.Constants;
import co.real.productionreal2.CoreData;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.MainActivityTabBase;
import co.real.productionreal2.activity.chat.ChatActivity;
import co.real.productionreal2.adapter.PastClosingAdapter;
import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.common.LocalStorageHelper;
import co.real.productionreal2.config.AppConfig;
import co.real.productionreal2.dao.account.DBQBChatDialog;
import co.real.productionreal2.daomanager.DatabaseManager;
import co.real.productionreal2.model.CurrentFollower;
import co.real.productionreal2.service.model.AgentExp;
import co.real.productionreal2.service.model.AgentProfiles;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.request.RequestFollowAgent;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;
import co.real.productionreal2.view.BlurBuilder;
import co.real.productionreal2.view.ExpandableHeightDynamicListView;
import co.real.productionreal2.view.FollowButton;
import widget.RoundedImageView;
import widget.observablescrollview.ObservableScrollView;
import widget.observablescrollview.ObservableScrollViewCallbacks;
import widget.observablescrollview.ScrollState;
import widget.observablescrollview.ScrollUtils;

;


public class NewsFeedProfileFragment extends BaseNewsFeedFragment implements ObservableScrollViewCallbacks {

    @InjectView(R.id.expTextView)
    TextView expTextView;
    @InjectView(R.id.langTextView)
    TextView langTextView;
    @InjectView(R.id.specTextView)
    TextView specTextView;
    @InjectView(R.id.top5CloseTextView)
    TextView top5CloseTextView;
    @InjectView(R.id.exp_layout)
    LinearLayout expLayout;

    @InjectView(R.id.expLinearLayout)
    LinearLayout expLinearLayout;
    @InjectView(R.id.langLinearLayout)
    LinearLayout langLinearLayout;
    @InjectView(R.id.specLinearLayout)
    LinearLayout specLinearLayout;
    @InjectView(R.id.top5CloseLinearLayout)
    LinearLayout top5CloseLinearLayout;

    @InjectView(R.id.langResultLinearLayout)
    LinearLayout langResultLinearLayout;
    @InjectView(R.id.specResultLinearLayout)
    LinearLayout specResultLinearLayout;
    @InjectView(R.id.past_exp_dynamiclistview)
    ExpandableHeightDynamicListView pastClosingListView;
    @InjectView(R.id.btn_to_follow)
    FollowButton btnToFollow;
    @InjectView(R.id.btn_to_chat)
    ImageView btnToChat;
    @InjectView(R.id.tvLangResult)
    TextView tvLangResult;
    @InjectView(R.id.tvSpecResult)
    TextView tvSpecResult;
    @InjectView(R.id.LinearLayout_LicenseNo)
    LinearLayout LinearLayout_LicenseNo;
    @InjectView(R.id.TextView_LicenseNo)
    TextView TextView_LicenseNo;

    @InjectView(R.id.merge_profile)
    LinearLayout merge_profile;
    @InjectView(R.id.LinearLayout_NoProfile)
    LinearLayout LinearLayout_NoProfile;
    @InjectView(R.id.TextView_no_profile_with_name)
    TextView TextView_no_profile_with_name;
    @InjectView(R.id.btnToAsk)
    TextView btnToAsk;


    int btnToFollow_Left = 0;
    int btnToFollow_Top = 0;
    int btnToFollow_Right = 0;
    int btnToFollow_Bottom = 0;
    int btnToChat_Left = 0;
    int btnToChat_Top = 0;
    int btnToChat_Right = 0;
    int btnToChat_Bottom = 0;

    private int newsFeedType = 0; // 0 = newsFeed, 1 = search

    Handler handler = new Handler();

    private AgentProfiles agentProfilesItem;
    private ResponseLoginSocial.Content agentContent;

    private static final float MAX_TEXT_SCALE_DELTA = 0.3f;
    private int itemPosition = -1;
    private int MemberID = -1;

    private ObservableScrollView mScrollView;
    private ImageView rootImageView;
    private RoundedImageView iv_icon;
    private TextView tv_peopleName;

    private View mFlexibleSpaceView;
    private View ll_profileDetail;
    private TextView tv_numFollowers;
    private TextView tv_Followers;
    private RelativeLayout panel_layout;

    private int mFollowerCount;
    private int mOpenGapSize;
    private int mFlexibleSpaceShowFabOffset;
    private int mFlexibleSpaceImageHeight;

    float flexibleRange;
    int resizeRange;
    private int circle_image_height;
    private int mFlexibleSpaceHeight;
    private int flexibleSpaceAndToolbarHeight;
    private ViewPager pager;

    public static final String EXTRA_MESSAGE = "EXTRA_MESSAGE";
    public static final String BUNDLE_NEWSFEED_TYPE = "BUNDLE_NEWSFEED_TYPE";

    public static final NewsFeedProfileFragment newInstance(String message, ViewPager pager) {
        NewsFeedProfileFragment f = new NewsFeedProfileFragment();
        Bundle bdl = new Bundle(1);
        bdl.putString(EXTRA_MESSAGE, message);
        f.setArguments(bdl);
        f.setPager(pager);

        return f;
    }

    void setPager(ViewPager pager){
        this.pager = pager;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
//        String message = getArguments().getString(EXTRA_MESSAGE);
        View view = inflater.inflate(R.layout.fragment_newsfeed_profile, container, false);
        ButterKnife.inject(this, view);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getArguments() != null) {
            newsFeedType = getArguments().getInt(BUNDLE_NEWSFEED_TYPE);
        }

        langTextView.setText(R.string.common__agentprofile_title_langauge_skills);

        agentContent = new Gson().fromJson(DatabaseManager.getInstance(getActivity()).getLogInfo().getLogInfo(),
                ResponseLoginSocial.Content.class);

//        agentProfilesItem=NewsFeedFragment.agentProfile;
//        agentProfilesItem=LocalStorageHelper.getLocalAgentProfiles(context,newsFeedType);

        mFlexibleSpaceImageHeight = getResources().getDimensionPixelSize(R.dimen.newsfeed_flexible_space_image_height);
        mFlexibleSpaceShowFabOffset = getResources().getDimensionPixelSize(R.dimen.newsfeed_flexible_space_show_fab_offset);
        mOpenGapSize = getResources().getDimensionPixelSize(R.dimen.newsfeed_open_gap_size) + 20;

        circle_image_height = getResources().getDimensionPixelSize(R.dimen.newsfeed_circle_image_height);
        flexibleRange = mFlexibleSpaceImageHeight - mOpenGapSize;
        mFlexibleSpaceHeight = getResources().getDimensionPixelSize(R.dimen.newsfeed_flexible_space_height);
        flexibleSpaceAndToolbarHeight = mFlexibleSpaceHeight + mFlexibleSpaceHeight;
        resizeRange = mFlexibleSpaceShowFabOffset - mOpenGapSize;

//        Log.i("", "edwin onCreateView "+circle_image_height+" "+flexibleRange+" "+mFlexibleSpaceHeight+" "+flexibleSpaceAndToolbarHeight+" "+resizeRange+" "+mOpenGapSize);

        rootImageView = (ImageView) view.findViewById(R.id.rootImageView);
        mFlexibleSpaceView = view.findViewById(R.id.flexible_space);
        mFlexibleSpaceView.getLayoutParams().height = flexibleSpaceAndToolbarHeight;
        mScrollView = (ObservableScrollView) view.findViewById(R.id.scroll);
        iv_icon = (RoundedImageView) view.findViewById(R.id.iv_icon);
        tv_peopleName = (TextView) view.findViewById(R.id.tv_peopleName);
        ll_profileDetail = view.findViewById(R.id.ll_profileDetail);
        tv_numFollowers = (TextView) view.findViewById(R.id.tv_numFollowers);
        tv_Followers = (TextView) view.findViewById(R.id.tv_Followers);
        panel_layout = (RelativeLayout) view.findViewById(R.id.panel_layout);

        panel_layout.setOnClickListener(null);
        mFlexibleSpaceView.getLayoutParams().height = flexibleSpaceAndToolbarHeight;
        mScrollView.setScrollViewCallbacks(this);

        btnToFollow.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View v) {
                Log.d("toFollow", "toFollow agentContent: " + agentContent.toString());
                if (agentProfilesItem != null) {

                    Log.d("toFollow", "toFollow agentProfilesItem: " + agentProfilesItem.toString());

                    DatabaseManager databaseManager = DatabaseManager.getInstance(getActivity());
                    ResponseLoginSocial.Content content = CoreData.getUserContent(context);
                    final RequestFollowAgent requestFollowAgent = new RequestFollowAgent(content.memId,  LocalStorageHelper.getAccessToken(getContext()));

                    requestFollowAgent.followingMemId = agentProfilesItem.memberID;

                    if (agentProfilesItem.agentListing != null)
                        requestFollowAgent.followingListingId = agentProfilesItem.agentListing.listingID;

                    if (agentProfilesItem.isFollowing==1) {

                        FragmentManager fm = getChildFragmentManager();
                        MyAlertDialogFragment editNameDialog = MyAlertDialogFragment
                                .newInstance(agentProfilesItem.memberName,

                                        new DialogInterface.OnClickListener() {

                                            @Override
                                            public void onClick(DialogInterface dialog,
                                                                int which) {
                                                // follow count -1
                                                unfollowAgent(requestFollowAgent);

                                                dialog.dismiss();
                                            }
                                        });
                        editNameDialog.show(fm, "fragment_edit_name");

                    } else {

                        // follow count +1
                        followAgent(requestFollowAgent);

                    }
                }
            }
        });

        btnToChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToChat();
            }
        });
        btnToAsk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToChat();
            }
        });


//        ScrollUtils.addOnGlobalLayoutListener(mScrollView, new Runnable() {
//            @Override
//            public void run() {
//                mScrollView.scrollTo(0, mOpenGapSize);
//                // If you'd like to start from scrollY == 0, don't write like this:
//                //mScrollView.scrollTo(0, 0);
//                // The initial scrollY is 0, so it won't invoke onScrollChanged().
//                // To do this, use the following:
//                //onScrollChanged(0, false, false);®
//
//                // You can also achieve it with the following codes.
//                // This causes scroll change from 1 to 0.
//                //mScrollView.scrollTo(0, 1);
//                //mScrollView.scrollTo(0, 0);
//            }
//        });

        if (itemPosition != -1) {
            updateUI(false);
        }

    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {

        float scale = 0.5f + ScrollUtils.getFloat(((float) (resizeRange - (scrollY - mOpenGapSize)) / resizeRange), 0, 1f) * MAX_TEXT_SCALE_DELTA;

        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) iv_icon.getLayoutParams();
        params.width = (int) (circle_image_height * scale);
        params.height = (int) (circle_image_height * scale);

        iv_icon.setLayoutParams(params);

        ll_profileDetail.setAlpha(ScrollUtils.getFloat(((float) (resizeRange - (scrollY - mOpenGapSize) * 1f) / resizeRange), 0, 1f));
        ll_profileDetail.setTranslationY(scrollY);

    }

    @Override
    public void onDownMotionEvent() {
    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {
    }


    public void update(AgentProfiles item, int position) {
        if (item!=null && agentProfilesItem!=null) {
            Log.d("update", "update: compare: " + item.followerCount + " <> " + agentProfilesItem.followerCount);
            Log.d("update", "update: compare: " + item.isFollowing + " <> " + agentProfilesItem.isFollowing);
        }
        if (itemPosition == position && agentProfilesItem == item) {
            return;
        }
        agentProfilesItem = item;
        itemPosition = position;
        mFollowerCount=agentProfilesItem.followerCount;
//        updateUI();
    }

    public void updateUI(boolean isForceUpdate) {


        if (!isForceUpdate) {
            if (agentProfilesItem == null || agentProfilesItem.memberID == MemberID)
                return;
        }

        MemberID = agentProfilesItem.memberID;
        //reset agent info UI posotion
        mScrollView.setScrollY(0);
        if (agentProfilesItem.agentListing.photos.size() > 0) {

            // Mixpanel
            MixpanelAPI mixpanel = MixpanelAPI.getInstance(getActivity(), AppConfig.getMixpanelToken());
            JSONObject jsonObject = new JSONObject();
            try {

                jsonObject.put("Agent member ID", agentProfilesItem.memberID);
                jsonObject.put("Agent name", agentProfilesItem.memberName);

            } catch (JSONException e) {

            }
            mixpanel.track("Viewed Agent Profile", jsonObject);

            Picasso.with(getActivity()).load(agentProfilesItem.agentListing.photos.get(0).url).into(target);
            Picasso.with(context).load(agentProfilesItem.agentPhotoURL)
                    .error(R.drawable.default_profile)
                    .placeholder(R.drawable.default_profile)
                    .into(iv_icon);
            tv_peopleName.setText(agentProfilesItem.memberName + "");

            //set blue border on profile pic
            setFollowing();


            if (agentProfilesItem.agentExpList.size() > 0 || agentProfilesItem.agentLanguageList.size() > 0 || agentProfilesItem.agentSpecialtyList.size() > 0 || agentProfilesItem.agentPastClosingList.size() > 0) {

                createExpLayout();
                createLangLayout();
                createSpecLayout();
                createTop5PastLayout();

                if (agentProfilesItem.agentLicenseNumber != null && !agentProfilesItem.agentLicenseNumber.trim().equals("")) {
                    TextView_LicenseNo.setText(this.getContext().getResources().getString(R.string.common__licese_no) + " " + agentProfilesItem.agentLicenseNumber);
                    LinearLayout_LicenseNo.setVisibility(View.VISIBLE);
                } else {
                    LinearLayout_LicenseNo.setVisibility(View.GONE);
                }

                merge_profile.setVisibility(View.VISIBLE);
                LinearLayout_NoProfile.setVisibility(View.GONE);

            } else {
                merge_profile.setVisibility(View.GONE);
                LinearLayout_NoProfile.setVisibility(View.VISIBLE);
                if (agentContent.memId == agentProfilesItem.memberID) {
                    TextView_no_profile_with_name.setText(this.getContext().getResources().getString(R.string.newsfeed__please_update_your_profile));
                } else {
                    TextView_no_profile_with_name.setText(String.format(this.getContext().getResources().getString(R.string.profile__didnt_enter),agentProfilesItem.memberName));
                }
            }
        }

        if (agentContent.memId == agentProfilesItem.memberID) {
            btnToFollow.setVisibility(View.GONE);
            btnToChat.setVisibility(View.GONE);
            btnToAsk.setVisibility(View.GONE);
        } else {
            btnToFollow.setVisibility(View.VISIBLE);
            btnToChat.setVisibility(View.VISIBLE);
            btnToAsk.setVisibility(View.VISIBLE);
        }

    }


    // ///////////////////////////// web service
    // ////////////////////////////////

    private void followAgent(final RequestFollowAgent followAgent) {

        btnToFollow.setClickable(false);
        followAgent.callFollowApi(getActivity(), new ApiCallback() {
            @Override
            public void success(ApiResponse apiResponse) {
                // Mixpanel
                MixpanelAPI mixpanel = MixpanelAPI.getInstance(getActivity(), AppConfig.getMixpanelToken());

                JSONObject properties = new JSONObject();
                try {
                    properties.put("Agent member ID", agentProfilesItem.memberID);
                    properties.put("Agent name", agentProfilesItem.memberName);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mixpanel.track("Followed Agent", properties);

                properties = new JSONObject();
                try {
                    properties.put("Follower member ID", CoreData.getUserContent(getActivity()).memId);
                    properties.put("distinct_id", agentProfilesItem.memberID);
                    properties.put("Follower name", CoreData.getUserContent(getActivity()).memName);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mixpanel.track("Gained Follower", properties);

//                CurrentFollower currentFollower = new CurrentFollower(userContent.memId, agentProfilesItem.memberID,
//                        userContent.qbid, agentProfilesItem.qBID, true);
//                currentFollower.updateCurrentFollowingList(getActivity());

                agentProfilesItem.isFollowing = 1;

                //add local following people list
                LocalStorageHelper.addAgentProfileListToLocalStorage(context,agentProfilesItem);
                mFollowerCount+=1;
                updateFollowCount();
            }

            @Override
            public void failure(String errorMsg) {
                btnToFollow.setClickable(true);
            }
        });

    }


    private void unfollowAgent(final RequestFollowAgent unfollowAgent) {

        btnToFollow.setClickable(false);
        unfollowAgent.callUnfollowApi(getActivity(), new ApiCallback() {
            @Override
            public void success(ApiResponse apiResponse) {
                // Mixpanel
                MixpanelAPI mixpanel = MixpanelAPI.getInstance(getActivity(), AppConfig.getMixpanelToken());

                JSONObject properties = new JSONObject();
                try {
                    properties.put("Agent member ID", agentProfilesItem.memberID);
                    properties.put("Agent name", agentProfilesItem.memberName);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mixpanel.track("Unfollowed Agent", properties);

                properties = new JSONObject();
                try {
                    properties.put("Unfollower member ID", CoreData.getUserContent(getActivity()).memId);
                    properties.put("distinct_id", agentProfilesItem.memberID);
                    properties.put("Unfollower name", CoreData.getUserContent(getActivity()).memName);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mixpanel.track("Lost Follower", properties);

                CurrentFollower currentFollower = new CurrentFollower(userContent.memId, agentProfilesItem.memberID,
                        userContent.qbid, agentProfilesItem.qBID, false);
                currentFollower.updateCurrentFollowingList(getActivity());

                agentProfilesItem.isFollowing = 0;

                //remove agentProfile from local following people list
                LocalStorageHelper.removeAgentProfileListToLocalStorage(context,agentProfilesItem.memberID);
                mFollowerCount-=1;

                updateFollowCount();
            }

            @Override
            public void failure(String errorMsg) {
                btnToFollow.setClickable(true);
            }
        });
//        final OperationService weatherService = new OperationService(
//                this.getActivity());
//
//        weatherService.getCoffeeService().agentUnfollow(
//
//                new InputRequest(new Gson().toJson(unfollowAgent)),
//                new retrofit.Callback<ApiResponse>() {
//
//                    @Override
//                    public void failure(RetrofitError error) {
//                        btnToFollow.setClickable(true);
//                    }
//
//                    @Override
//                    public void success(ApiResponse apiResponse,
//                                        Response response) {
//
//                        // Mixpanel
//                        MixpanelAPI mixpanel = MixpanelAPI.getInstance(getActivity(), AppConfig.getMixpanelToken());
//
//                        JSONObject properties = new JSONObject();
//                        try {
//                            properties.put("Agent member ID", agentProfilesItem.memberID);
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                        mixpanel.track("Unfollowed", properties);
//
//                        properties = new JSONObject();
//                        try {
//                            properties.put("Unfollower member ID", unfollowAgent.followingMemId);
//                            properties.put("distinct_id", agentProfilesItem.memberID);
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                        mixpanel.track("Being unfollowed", properties);
//
//                        CurrentFollower currentFollower = new CurrentFollower(userContent.memId, agentProfilesItem.memberID,
//                                userContent.qbid, agentProfilesItem.qBID, false);
//                        currentFollower.updateCurrentFollowingList(getActivity());
//
//                        agentProfilesItem.isFollowing = 0;
//
//                        updateFollowCount();
//                    }
//                });

    }



    private void updateFollowCount() {

        Log.d("following", "agentReg.isFollowing  " + agentProfilesItem.isFollowing + " followerCount: " + agentProfilesItem.followerCount);
        btnToFollow.setIsFollowing(LocalStorageHelper.checkIfAgentIsFollowing(context, agentProfilesItem.memberID) ? 1 : 0);
        btnToFollow.playAnimation();

        if (btnToFollow.isFollowing()==1){
            btnToFollow.setSelected(true);
            btnToFollow.setText(R.string.common__following);
            btnToFollow.setCompoundDrawablesWithIntrinsicBounds(R.drawable.btn_ico_follow_unfollow, 0, 0, 0);
//            mCount= agentProfilesItem.followerCount+1;
        } else {
            btnToFollow.setSelected(false);
            String followString = getString(R.string.common__follow);
            btnToFollow.setText(followString);
//            mCount= agentProfilesItem.followerCount-1;
            btnToFollow.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ico_profile_follow, 0, 0, 0);
        }

        btnToFollow.setClickable(true);
        setFollowing();
    }

    Bitmap rootBitmap;
    private Target target = new Target() {
        @Override
        public void onBitmapLoaded(final  Bitmap bitmap, Picasso.LoadedFrom from) {

            new Thread() {
                @Override
                public void run() {
                    try {
                        rootBitmap = BlurBuilder.blurBitmap(getActivity(), bitmap);
//                        rootBitmap = BlurBuilder.reduceBrightness(rootBitmap);
                        handler.post(new Runnable() {
                            public void run() {
                                rootImageView.setImageBitmap(rootBitmap);
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }.start();
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {

        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {

        }

    };

    private void createExpLayout() {
        expLayout.removeAllViews();
        if(agentProfilesItem.agentExpList.size()>0){
            expLinearLayout.setVisibility(View.VISIBLE);
            for (int i = 0; i < agentProfilesItem.agentExpList.size(); i++) {
                appendAgentExpView(agentProfilesItem.agentExpList.get(i), i);
            }
        }else{
            expLinearLayout.setVisibility(View.GONE);
        }
    }

    private void createLangLayout() {
        String nativeNamesString = "";
        if(agentProfilesItem.agentLanguageList.size()>0){
            langLinearLayout.setVisibility(View.VISIBLE);
        }else{
            langLinearLayout.setVisibility(View.GONE);
        }
        ArrayList<String> nativeNameList = new ArrayList<>();
        for (int i = 0; i < agentProfilesItem.agentLanguageList.size(); i++) {
            for (int j = 0; j < agentContent.systemSetting.spokenLangList.size(); j++) {
                if (agentContent.systemSetting.spokenLangList.get(j).index ==
                        agentProfilesItem.agentLanguageList.get(i).langIndex) {
                    nativeNameList.add(agentContent.systemSetting.spokenLangList.get(j).nativeName);
                }
            }

        }

        for (int i = 0; i < nativeNameList.size(); i++) {
            if (i == 0)
                nativeNamesString = nativeNameList.get(i);
            else
                nativeNamesString = nativeNamesString + "・" + nativeNameList.get(i);
        }
        Log.w("newsfeedprofile", "nativeNamesString = " + nativeNamesString);
        if (!nativeNamesString.isEmpty()) {
            tvLangResult.setVisibility(View.VISIBLE);
            tvLangResult.setText(nativeNamesString);
        }else{
            tvLangResult.setVisibility(View.GONE);
        }
//        new VertHoriLinearLayout(getActivity(), langResultLinearLayout,
//                nativeNameList,
//                ImageUtil.getScreenWidth(getActivity()), VertHoriLinearLayout.VIEW_PROFILE_LANG,
//                new LangSpecDelCallback() {
//                    @Override
//                    public void onLangSpecDel(int tagNo, int type) {
//                    }
//                });
    }

    private void createSpecLayout() {
        if(agentProfilesItem.agentSpecialtyList.size()>0){
            specLinearLayout.setVisibility(View.VISIBLE);
        }else{
            specLinearLayout.setVisibility(View.GONE);
        }

        String agentSpecString = "";
        for (int i = 0; i < agentProfilesItem.agentSpecialtyList.size(); i++) {
            if (i == 0)
                agentSpecString = agentProfilesItem.agentSpecialtyList.get(i).specialty;
            else
                agentSpecString = agentSpecString + "・" + agentProfilesItem.agentSpecialtyList.get(i).specialty;
        }
        if (!agentSpecString.isEmpty()) {
            tvSpecResult.setVisibility(View.VISIBLE);
            tvSpecResult.setText(agentSpecString);
        }else{
            tvSpecResult.setVisibility(View.GONE);
        }
//        new VertHoriLinearLayout(getActivity(), specResultLinearLayout,
//                VertHoriLinearLayout.agentSpecListToStringList((ArrayList<AgentSpecialty>) agentProfilesItem.agentSpecialtyList),
//                ImageUtil.getScreenWidth(getActivity()), VertHoriLinearLayout.VIEW_PROFILE_LANG,
//                new LangSpecDelCallback() {
//                    @Override
//                    public void onLangSpecDel(int tagNo, int type) {
//                    }
//                });
    }

    private void createTop5PastLayout() {
        if (agentContent == null)
            return;
        if (agentContent.systemSetting == null)
            return;
        if (agentContent.systemSetting.propertyTypeList == null)
            return;
        if(agentProfilesItem.agentPastClosingList != null && agentProfilesItem.agentPastClosingList.size()>0){
            top5CloseLinearLayout.setVisibility(View.VISIBLE);
        }else{
            top5CloseLinearLayout.setVisibility(View.GONE);
        }

        PastClosingAdapter pastClosingAdapter = new PastClosingAdapter(getActivity(), null, agentContent.systemSetting.propertyTypeList, true);
        if (agentProfilesItem.agentPastClosingList != null && agentProfilesItem.agentPastClosingList.size() > 0) {
//            for (AgentPastClosing agentPastClosing : agentProfilesItem.agentPastClosingList) {
//                pastClosingAdapter.add(agentPastClosing);
//            }
            pastClosingAdapter.addAll(agentProfilesItem.agentPastClosingList);
        }
        SimpleSwipeUndoAdapter simpleSwipeUndoAdapter = new SimpleSwipeUndoAdapter(pastClosingAdapter, getActivity(),
                null);
        AlphaInAnimationAdapter animAdapter = new AlphaInAnimationAdapter(simpleSwipeUndoAdapter);
        animAdapter.setAbsListView(pastClosingListView);
        assert animAdapter.getViewAnimator() != null;
        animAdapter.getViewAnimator().setInitialDelayMillis(ExpandableHeightDynamicListView.ANIM_INITIAL_DELAY_MILLIS);
        pastClosingListView.setAdapter(animAdapter);

        pastClosingListView.setAdapter(pastClosingAdapter);
        pastClosingListView.setExpanded(true);
    }

    private void appendAgentExpView(final AgentExp agentExp, int tagNo) {
        LayoutInflater layoutInflater = getActivity().getLayoutInflater();
        View view = layoutInflater.inflate(R.layout.agent_exp_item, null);

        TextView companyTextView = ButterKnife.findById(view, R.id.companyTextView);
        TextView titleTextView = ButterKnife.findById(view, R.id.titleTextView);
        TextView periodTextView = ButterKnife.findById(view, R.id.periodTextView);
        ImageButton removeBtn = ButterKnife.findById(view, R.id.removeExpBtn);
        ImageButton expEditBtn = ButterKnife.findById(view, R.id.expEditBtn);


        companyTextView.setText(agentExp.company);
        titleTextView.setText(agentExp.title);
        periodTextView.setText(agentExp.getExpPeriodText(getActivity()));

        removeBtn.setVisibility(View.GONE);
        expEditBtn.setVisibility(View.GONE);

        expLayout.addView(view);
    }
    
    void goToChat() {
        // Mixpanel
        MixpanelAPI mixpanel = MixpanelAPI.getInstance(getActivity(), AppConfig.getMixpanelToken());

        if (LocalStorageHelper.getQBpw(getContext()).equals("")){
            new AlertDialog.Builder(getContext())
                    .setMessage(R.string.chatlobby__no_service)
                    .setCancelable(false)
                    .setPositiveButton(R.string.alert_ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    }).create().show();
            return;
        }

        JSONObject properties = new JSONObject();
        try {
            properties.put("Agent member ID", agentProfilesItem.memberID);
            properties.put("Agent name", agentProfilesItem.memberName);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mixpanel.track("Started Chat with an Agent", properties);
        Log.d("toChat", "toChat: " + agentProfilesItem);

        String QBUserId = agentProfilesItem.qBID;
        int memId = agentProfilesItem.memberID;
        int bindingAgentListingId = agentProfilesItem.agentListing.listingID;
        DBQBChatDialog dbqbChatDialog = DatabaseManager.getInstance(context).getQBChatDialogByQbId(Integer.parseInt(QBUserId));
        String dialogId = null;
        if (dbqbChatDialog != null)
            dialogId = dbqbChatDialog.getDialogID();


        Intent i = new Intent();
        i.setClass(getActivity(), ChatActivity.class);
        Bundle bundle = new Bundle();
        if (dialogId != null)
            bundle.putString(ChatActivity.EXTRA_DIALOG_ID, dialogId);
        bundle.putString(ChatActivity.EXTRA_USER_ID, "" + QBUserId);
        bundle.putInt(ChatActivity.EXTRA_MEM_ID, memId);
        bundle.putBoolean(ChatActivity.EXTRA_HAS_HISTORY, true);
        bundle.putInt(ChatActivity.EXTRA_BINDING_AGENT_LISTING_ID, bindingAgentListingId);

        CoreData.opponentAgentProfile = agentProfilesItem;
        ChatActivity.start(getActivity(), bundle);

//                Log.d("toChat", "toChat: " + dialogId + " " + " " + QBUserId + " " + memId);
    }

    public void setFollowing() {
        //set blue border on profile pic
        if (iv_icon != null && agentProfilesItem != null) {
            if (agentProfilesItem.isFollowing==1) {
                iv_icon.setImageBorder(Constants.STYLE_TYPE.AGENT_FOLLOWED);
                btnToFollow.setSelected(true);
                btnToFollow.setText(R.string.common__following);
                btnToFollow.setCompoundDrawablesWithIntrinsicBounds(R.drawable.btn_ico_follow_unfollow, 0, 0, 0);
            } else {
                iv_icon.setImageBorder(Constants.STYLE_TYPE.AGENT_UNFOLLOWED);
                btnToFollow.setSelected(false);
                btnToFollow.setText(getResources().getString(R.string.common__follow));
                btnToFollow.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ico_profile_follow, 0, 0, 0);
            }
        }

        tv_numFollowers.setText(String.valueOf(mFollowerCount));

    }

    public static class MyAlertDialogFragment extends DialogFragment {

        DialogInterface.OnClickListener okListener;
        static String agentName;

        public MyAlertDialogFragment() {
        }

        public static MyAlertDialogFragment newInstance(String title,
                                                        DialogInterface.OnClickListener okListener) {
            MyAlertDialogFragment frag = new MyAlertDialogFragment();
            Bundle args = new Bundle();
            args.putString("title", title);
            agentName = title;
            frag.setArguments(args);
            frag.okListener = okListener;
            return frag;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    getActivity());
            alertDialogBuilder.setTitle("");
            alertDialogBuilder.setMessage(getString(R.string.agent_profile__unfollow_button) + " " + agentName + "?");
            // null should be your on click listener
            alertDialogBuilder.setPositiveButton(android.R.string.ok, okListener);
            alertDialogBuilder.setNegativeButton(android.R.string.cancel,
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

            return alertDialogBuilder.create();
        }
    }

    @Override
    public void onResume() {

        super.onResume();

        if (pager != null) {

            getView().setFocusableInTouchMode(true);
            getView().requestFocus();
            getView().setOnKeyListener(new View.OnKeyListener() {

                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {

                    if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {

                        // handle back button
                        Log.d(this.getClass().getName(), "onKey: ");
                        pager.setCurrentItem(1, true);
                        ((MainActivityTabBase)getActivity()).openTabBar();

                        return true;
                    }

                    return false;
                }
            });
        }
    }
}
