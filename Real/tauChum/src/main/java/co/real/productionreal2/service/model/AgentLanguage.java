package co.real.productionreal2.service.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class AgentLanguage implements Parcelable{

//	public static final String LANG_ENGLISH = "ENGLISH";//1;
//	public static final String LANG_CHINESE = "CHINESE";//2;
	
	@SerializedName("Language")
	public String lang = "dummy";

	@SerializedName("LangIndex")
	public int langIndex;


	public long id;
//	public int lang;

//	public AgentLanguage(String lang) {
//		super();
//		this.lang = lang;
//	}



	public AgentLanguage(int langIndex) {
		super();
		this.langIndex = langIndex;
	}



	public AgentLanguage() {
		// TODO Auto-generated constructor stub
	}



	public static final Parcelable.Creator<AgentLanguage> CREATOR = new Creator<AgentLanguage>() {

		 public AgentLanguage createFromParcel(Parcel source) {
			 AgentLanguage agentReg = new AgentLanguage();
			 agentReg.lang = source.readString();
			 agentReg.langIndex = source.readInt();
		     return agentReg;
		 }

		@Override
		public AgentLanguage[] newArray(int size) {
			return new AgentLanguage[size];
		}
	 };


	
	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(lang);
		dest.writeInt(langIndex);
		
	}
		
}
