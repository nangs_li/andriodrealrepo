package co.real.productionreal2.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.quickblox.users.model.QBUser;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Locale;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import co.real.productionreal2.Constants;
import co.real.productionreal2.CoreData;
import co.real.productionreal2.Event.UpdateInputPhoneNumberEvent;
import co.real.productionreal2.Navigation;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.login.SignUpInfoActivity;
import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.chat.ChatSessionUtil;
import co.real.productionreal2.chat.core.ChatService;
import co.real.productionreal2.chat.core.PrivateChatImpl;
import co.real.productionreal2.common.FetchAgentProfileHelper;
import co.real.productionreal2.common.LocalStorageHelper;
import co.real.productionreal2.config.AppConfig;
import co.real.productionreal2.daomanager.ConfigDataBaseManager;
import co.real.productionreal2.daomanager.DatabaseManager;
import co.real.productionreal2.model.UserQBUser;
import co.real.productionreal2.service.AdminService;
import co.real.productionreal2.service.BaseService;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.request.RequestPhoneNumberMigrate;
import co.real.productionreal2.service.model.request.RequestPhoneNumberRegEX;
import co.real.productionreal2.service.model.request.RequestPhoneNumberSMSVerifyEX;
import co.real.productionreal2.service.model.request.RequestPhoneNumberSMSVerifyForMigratePhone;
import co.real.productionreal2.service.model.request.RequestRealNetworkLogin;
import co.real.productionreal2.service.model.request.RequestSocialLogin;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;
import co.real.productionreal2.service.model.response.ResponsePhoneNumberMigrate;
import co.real.productionreal2.service.model.response.ResponsePhoneNumberRegEX;
import co.real.productionreal2.service.model.response.ResponseRealNetworkMemLogin;
import co.real.productionreal2.util.AppUtil;
import co.real.productionreal2.util.CryptLib;
import co.real.productionreal2.util.FileUtil;
import co.real.productionreal2.view.Dialog;


public class VerifyPhoneNoFragment extends BaseFragment  {
    private static final String TAG = "VerifyPhoneNoFragment";


    @InjectView(R.id.tv_phoneNumber)
    TextView tv_phoneNumber;
    @InjectView(R.id.resendBtn)
    Button resendBtn;
    @InjectView(R.id.activecodeEditText1)
    EditText activecodeEditText1;
    @InjectView(R.id.activecodeEditText2)
    EditText activecodeEditText2;
    @InjectView(R.id.activecodeEditText3)
    EditText activecodeEditText3;
    @InjectView(R.id.activecodeEditText4)
    EditText activecodeEditText4;

    String resenduttonText;
    private ResponseLoginSocial.Content userContent;

    String countryCode;
    String phoneNumber;
    boolean isOldUser;
    int regType;
    int phoneRegID;
    String pINInput;
    RequestPhoneNumberRegEX requestPhoneNumberRegEX;
    RequestPhoneNumberMigrate requestPhoneNumberMigrate;

    public static final VerifyPhoneNoFragment newInstance(String countryCode, String phoneNumber, int phoneRegID) {
        VerifyPhoneNoFragment f = new VerifyPhoneNoFragment();
        Bundle bdl = new Bundle(1);
        f.setArguments(bdl);
//        f.setCountryCode(countryCode);
//        f.setPhoneNumber(phoneNumber);
//        f.setPhoneRegID(phoneRegID);

        return f;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    public void setPhoneRegID(int phoneRegID) {
        this.phoneRegID = phoneRegID;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_verify_phone_number, container, false);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        resenduttonText = getString(R.string.phone2__button);
        userContent = CoreData.getUserContent(this.getContext());
        isOldUser = CoreData.isOldUser;

        initView();
        addListener();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }


    public void initView() {
        tv_phoneNumber.setText(countryCode+" "+phoneNumber);

        resendCountDown();
    }

    void addListener(){
        activecodeEditText1.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start,int before, int count)
            {
                // TODO Auto-generated method stub
            }
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                if(s.length()>0)
                {
                    if(s.length()>1){
                        s.delete(0,1);
                    }
                    activecodeEditText2.requestFocus();
                    checkCode(activecodeEditText1);
                }
            }

        });
        activecodeEditText2.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start,int before, int count)
            {
                // TODO Auto-generated method stub
            }
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                if(s.length()>0)
                {
                    if(s.length()>1){
                        s.delete(0,1);
                    }
                    activecodeEditText3.requestFocus();
                    checkCode(activecodeEditText2);
                }
            }

        });
        activecodeEditText2.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == KeyEvent.KEYCODE_DEL){
                    if(activecodeEditText2.length()==0)
                    {
                        activecodeEditText1.requestFocus();
                    }
                }
                return false;
            }
        });
        activecodeEditText3.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start,int before, int count)
            {
                // TODO Auto-generated method stub
            }
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                if(s.length()>0)
                {
                    if(s.length()>1){
                        s.delete(0,1);
                    }
                    activecodeEditText4.requestFocus();
                    checkCode(activecodeEditText3);
                }
            }

        });
        activecodeEditText3.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == KeyEvent.KEYCODE_DEL){
                    if(activecodeEditText3.length()==0)
                    {
                        activecodeEditText2.requestFocus();
                    }
                }
                return false;
            }
        });
        activecodeEditText4.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start,int before, int count)
            {
                // TODO Auto-generated method stub
            }
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                if(s.length()>0)
                {
                    if(s.length()>1){
                        s.delete(0,1);
                    }
                    activecodeEditText1.requestFocus();
                    checkCode(activecodeEditText4);
                }
            }

        });
        activecodeEditText4.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == KeyEvent.KEYCODE_DEL){
                    if(activecodeEditText4.length()==0)
                    {
                        activecodeEditText3.requestFocus();
                    }
                }
                return false;
            }
        });
    }

    ResendCountDownTimer countDownTimer;
    public void resendCountDown() {
        resendBtn.setEnabled(false);
        if(countDownTimer!=null){
            countDownTimer.cancel();
        }
        countDownTimer = new ResendCountDownTimer(60000, 100);
        countDownTimer.start();
    }

    void checkCode(EditText editText){
        String code1 = activecodeEditText1.getText().toString();
        String code2 = activecodeEditText2.getText().toString();
        String code3 = activecodeEditText3.getText().toString();
        String code4 = activecodeEditText4.getText().toString();

        if(!code1.equals("")&&!code2.equals("")&&!code3.equals("")&&!code4.equals("")){
            AppUtil.hideKeyboard(context, editText);
            pINInput = code1+code2+code3+code4;
            if(isOldUser){
                callPhoneNumberSMSVerifyForMigratingPhoneApi();
            }else{
                callVerifyPhoneNumberAPI();
            }
        }
    }

    void callVerifyPhoneNumberAPI(){
        launchLoadingDialog();
        RequestPhoneNumberSMSVerifyEX requestPhoneNumberSMSVerifyEX = new RequestPhoneNumberSMSVerifyEX();
        requestPhoneNumberSMSVerifyEX.phoneRegID = phoneRegID;
        requestPhoneNumberSMSVerifyEX.pINInput = pINInput;
        requestPhoneNumberSMSVerifyEX.lang = AppUtil.getCurrentLangISO2Code(context);
        requestPhoneNumberSMSVerifyEX.callPhoneNumberSMSVerifyEXApi(context, new ApiCallback() {
            @Override
            public void success(ApiResponse apiResponse) {
                cancelLoadingDialog();

//                if(isOldUser){
//                    LocalStorageHelper.setRegedPhoneAeraCode(context,countryCode);
//                    LocalStorageHelper.setRegedPhoneNumber(context,phoneNumber);
//                    AppUtil.goToMainTabActivity(getActivity(), userContent);
//                }else{
                    if(regType==113) { //Number is attached to an active account
                        login();
                    }else if(regType==114 || regType==115){ //New number
                        goToSignInfo();
                    }
//                }

            }
            @Override
            public void failure(String errorMsg) {
                AppUtil.showAlertDialog(context, errorMsg);
                cancelLoadingDialog();
                activecodeEditText1.setText("");
                activecodeEditText2.setText("");
                activecodeEditText3.setText("");
                activecodeEditText4.setText("");

                activecodeEditText1.requestFocus();
            }
        });
    }

    void callPhoneNumberSMSVerifyForMigratingPhoneApi(){
        launchLoadingDialog();
        RequestPhoneNumberSMSVerifyForMigratePhone requestPhoneNumberSMSVerifyEX = new RequestPhoneNumberSMSVerifyForMigratePhone(userContent.memId, LocalStorageHelper.getAccessToken(getActivity()),phoneRegID,pINInput);
        requestPhoneNumberSMSVerifyEX.callRequestPhoneNumberSMSVerifyForMigratingPhoneApi(context, new ApiCallback() {
            @Override
            public void success(ApiResponse apiResponse) {
                cancelLoadingDialog();

                LocalStorageHelper.setRegedPhoneAeraCode(context,countryCode);
                LocalStorageHelper.setRegedPhoneNumber(context,phoneNumber);
                AppUtil.goToMainTabActivity(getActivity(), userContent);

            }
            @Override
            public void failure(String errorMsg) {
                AppUtil.showAlertDialog(context, errorMsg);
                cancelLoadingDialog();
                activecodeEditText1.setText("");
                activecodeEditText2.setText("");
                activecodeEditText3.setText("");
                activecodeEditText4.setText("");

                activecodeEditText1.requestFocus();
            }
        });
    }

    void login(){
        launchLoadingDialog();
        RequestRealNetworkLogin realNetworkLogin = new RequestRealNetworkLogin(phoneRegID, pINInput);
        realNetworkLogin.callRealNetworkLoginApi(context, new ApiCallback() {
            @Override
            public void success(ApiResponse apiResponse) {
                String decryptJson;
                if (Constants.enableEncryption) {
                    decryptJson = CryptLib.decrypt(apiResponse.jsonContent);
                    Log.w("requestsocialLogin", "jsonContent = " + decryptJson);
                } else {
                    decryptJson = apiResponse.jsonContent;
                }
                ResponseRealNetworkMemLogin responseGson = new Gson().fromJson(decryptJson, ResponseRealNetworkMemLogin.class);

                RequestSocialLogin socialLogin = new RequestSocialLogin(
                        AdminService.DEVICE_TYPE_ANDROID,
                        FileUtil.getDeviceId(context),
                        AdminService.SOCIAL_PHONE,
                        responseGson.content.firstName + " " + responseGson.content.lastName,
                        responseGson.content.userId,
                        responseGson.content.email,
                        responseGson.content.photoURL,
                        responseGson.content.accessToken
                );

                LocalStorageHelper.setRealNetworkJson(context, decryptJson);

                socialLogin.callLoginSocialApi(context, new ApiCallback() {
                    @Override
                    public void success(ApiResponse apiResponse) {
                        String decryptJson;
                        if (Constants.enableEncryption) {
                            decryptJson = CryptLib.decrypt(apiResponse.jsonContent);
                            Log.w("requestsocialLogin", "jsonContent = " + decryptJson);
                        } else {
                            decryptJson = apiResponse.jsonContent;
                        }
                        final ResponseLoginSocial responseGson = new Gson().fromJson(decryptJson, ResponseLoginSocial.class);

                        // Mixpanel
                        MixpanelAPI mixpanel = MixpanelAPI.getInstance(context, AppConfig.getMixpanelToken());
                        mixpanel.identify("" + responseGson.content.memId);
                        mixpanel.getPeople().identify(mixpanel.getDistinctId());
                        Locale locale = AppUtil.getCurrentLangLocale(context);
                        mixpanel.getPeople().set("Language", locale.getLanguage());
                        mixpanel.track("Verified Email Login");

                        LocalStorageHelper.setUpByLogin(context, responseGson.content);

                        ConfigDataBaseManager configDataBaseManager = ConfigDataBaseManager.getInstance(context);
                        DatabaseManager.getInstance(context).dbActionForLogin(responseGson.content);
                        configDataBaseManager.dbActionMain(responseGson.content.memId);

                        QBUser user = UserQBUser.getNewQBUser(context);

                        ChatSessionUtil.callQBLogin(context, user, new ApiCallback() {
                            @Override
                            public void success(ApiResponse apiResponse) {
                                Log.d(TAG, "callQBLogin success ");
                                PrivateChatImpl.getInstance(context);

                                ChatService.getInstance().getAllDialogs(new BaseService.BaseServiceCallBack() {
                                    @Override
                                    public void onSuccess(Object var1) {
                                        Log.d(TAG, "getAllDialogs onSuccess: ");
                                        cancelLoadingDialog();
                                        FetchAgentProfileHelper.getInstance().startFetching();
                                        AppUtil.goToMainTabActivity(activity, responseGson.content);
                                    }

                                    @Override
                                    public void onError(BaseService.RealServiceError serviceError) {
                                        Log.d(TAG, "onError: "+serviceError);
                                        cancelLoadingDialog();
                                    }
                                });
                            }

                            @Override
                            public void failure(String errorMsg) {
                                Log.d(TAG, "failure: "+errorMsg);
                                cancelLoadingDialog();
                            }
                        });
                    }

                    @Override
                    public void failure(String errorMsg) {
                        cancelLoadingDialog();
                        if (errorMsg.trim() != "")
                            Dialog.normalDialog(activity, getString(R.string.login__wrong_password)).show();
//                        AppUtil.showDialog(RealLoginActivity.this, "", errorMsg, false);
                    }
                });
            }

            @Override
            public void failure(String errorMsg) {
                cancelLoadingDialog();
                if (errorMsg.trim() != "")
                    Dialog.normalDialog(activity, errorMsg).show();
//                AppUtil.showDialog(RealLoginActivity.this, errorMsg, "", false);
            }
        });
    }

    void goToSignInfo(){
        Intent intent = new Intent(activity, SignUpInfoActivity.class);
        intent.putExtra(Constants.EXTRA_PHONE_REG_ID,phoneRegID);
        intent.putExtra(Constants.EXTRA_PIN_INPUT,pINInput);
        Navigation.pushIntent(activity, intent);
        getActivity().finish();
    }

    @Subscribe
    public void onMessageEvent(UpdateInputPhoneNumberEvent event){
        countryCode = event.countryCode;
        phoneNumber = event.phoneNumber;
        phoneRegID = event.phoneRegID;
        regType = event.regType;
        initView();
    }

    @OnClick(R.id.backBtn)
    public void backBtnClick(View view) {
        getActivity().onBackPressed();
    }

    @OnClick(R.id.resendBtn)
    public void resetBtnClick(View view) {
        resendCountDown();

        launchLoadingDialog();

        if(isOldUser) {
            callPhoneNumberMigrateApi();
        }else{
            callPhoneNumberRegEXApi();
        }
        requestPhoneNumberRegEX = new RequestPhoneNumberRegEX();

        requestPhoneNumberRegEX.countryCode = countryCode;
        requestPhoneNumberRegEX.phoneNumber = phoneNumber;

        requestPhoneNumberRegEX.callPhoneNumberRegEXApi(activity, new ApiCallback() {
            @Override
            public void success(ApiResponse apiResponse) {
                ResponsePhoneNumberRegEX responseGson = new Gson().fromJson(apiResponse.jsonContent, ResponsePhoneNumberRegEX.class);
                phoneRegID = responseGson.content.phoneRegID;
                regType = responseGson.content.regType;

                cancelLoadingDialog();
            }

            @Override
            public void failure(String errorMsg) {
                AppUtil.showAlertDialog(activity, errorMsg);
                cancelLoadingDialog();
            }
        });
    }

    void callPhoneNumberRegEXApi(){
        launchLoadingDialog();
        requestPhoneNumberRegEX = new RequestPhoneNumberRegEX();

        requestPhoneNumberRegEX.countryCode = countryCode;
        requestPhoneNumberRegEX.phoneNumber = phoneNumber;

        requestPhoneNumberRegEX.callPhoneNumberRegEXApi(activity, new ApiCallback() {
            @Override
            public void success(ApiResponse apiResponse) {
                ResponsePhoneNumberRegEX responseGson = new Gson().fromJson(apiResponse.jsonContent, ResponsePhoneNumberRegEX.class);
                phoneRegID = responseGson.content.phoneRegID;
                regType = responseGson.content.regType;

                cancelLoadingDialog();
            }

            @Override
            public void failure(String errorMsg) {
                AppUtil.showAlertDialog(activity, errorMsg);
                cancelLoadingDialog();
            }
        });
    }

    void callPhoneNumberMigrateApi(){
        launchLoadingDialog();
                requestPhoneNumberMigrate = new RequestPhoneNumberMigrate(userContent.memId, LocalStorageHelper.getAccessToken(getActivity()),
                        countryCode, phoneNumber, AppUtil.getCurrentLangCode(getActivity()));
//        requestPhoneNumberMigrate = new RequestPhoneNumberMigrate(24, "5f747107-348a-46c4-a7ee-5c7cd26def42",
//                countryCode, phoneNumber, AppUtil.getCurrentLangCode(getActivity()));
        requestPhoneNumberMigrate.callPhoneNumberMigrateApi(getActivity(), new ApiCallback() {
            @Override
            public void success(ApiResponse apiResponse) {
                ResponsePhoneNumberMigrate responseGson = new Gson().fromJson(apiResponse.jsonContent, ResponsePhoneNumberMigrate.class);
                phoneRegID = responseGson.content.phoneRegId;

                cancelLoadingDialog();
            }

            @Override
            public void failure(String errorMsg) {
                AppUtil.showAlertDialog(getActivity(),errorMsg);
                cancelLoadingDialog();
            }
        });
    }

    public class ResendCountDownTimer extends CountDownTimer
    {


        public ResendCountDownTimer(long startTime, long interval)
        {
            super(startTime, interval);
        }

        @Override
        public void onFinish()
        {
            resendBtn.setText(resenduttonText);
            resendBtn.setEnabled(true);
        }

        @Override
        public void onTick(long millisUntilFinished)
        {
            long secondsRemain = millisUntilFinished/1000;
            String strRemainTime;
            if(secondsRemain>59){
                strRemainTime = secondsRemain/60+":"+secondsRemain%60;
            }else{
                strRemainTime = secondsRemain+"";
            }
            resendBtn.setText(resenduttonText + " " + String.valueOf(strRemainTime));
        }

    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroy();
        if(countDownTimer!=null){
            countDownTimer.cancel();
        }
    }
}

