package co.real.productionreal2.service.model.request;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.common.LocalStorageHelper;
import co.real.productionreal2.model.GoogleAddress;
import co.real.productionreal2.model.InputRequest;
import co.real.productionreal2.service.OperationService;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.response.ResponseBase;
import co.real.productionreal2.setting.SearchFilterSetting;
import co.real.productionreal2.util.NetworkUtil;
import co.real.productionreal2.view.Dialog;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit.RetrofitError;
import retrofit.client.Response;

public class RequestAddressSearch extends RequestBase {

    public RequestAddressSearch(int memId, String session, int languageIndex) {
        super(memId, session);
        this.languageIndex = languageIndex;
    }

    @SerializedName("LanguageIndex")
    public int languageIndex;

    @SerializedName("GoogleAddressPack")
    public GoogleAddressPack googleAddress;

    @SerializedName("UserAddress")
    public String inputAddress;

    public class GoogleAddressPack {

        @SerializedName("GoogleAddressLang")
        public String lang; //100000000

        //        @SerializedName("GoogleAddress")
//        public GooglePlace address;
        @SerializedName("GoogleAddress")
        public GoogleAddress address;

        @Override
        public String toString() {
            return "GoogleAddressPack{" +
                    "lang='" + lang + '\'' +
                    ", address=" + address +
                    '}';
        }
    }

    public void callSearchAddressApi(final Context context, boolean isApplyFiltered, final ApiCallback callback) {
        String json = new Gson().toJson(this);
        JSONObject addressJsonObj = null;
        try {
            addressJsonObj = new JSONObject(json);
            if (isApplyFiltered) {
                addressJsonObj.put("Filter", SearchFilterSetting.getInstance().getFilterJson(context));
                //apply filter
                json = addressJsonObj.toString();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final OperationService weatherService = new OperationService(
                context);

        weatherService.getCoffeeService().addressSearch(new InputRequest(json),
                new retrofit.Callback<ApiResponse>() {

                    @Override
                    public void failure(RetrofitError error) {
                        callback.failure("");
                        NetworkUtil.showNoNetworkMsg(context);
                    }

                    @Override
                    public void success(ApiResponse apiResponse,
                                        Response response) {
                        ResponseBase responseGson = new Gson().fromJson(apiResponse.jsonContent, ResponseBase.class);
                        if (responseGson.errorCode == 4) {
                            Dialog.accessErrorForceLogoutDialog(context).show();
                        } else if (responseGson.errorCode == 26) {
                            Dialog.suspendedErrorForceLogoutDialog(context).show();
                        } else if (responseGson.errorCode == 21) {
                            context.getSharedPreferences(LocalStorageHelper.SHAREPREFERENCE_CURRENT_FOLLOWING_LIST_KEY, Context.MODE_PRIVATE).edit().clear().commit();
                        } else if (responseGson.errorCode != 0 && responseGson.errorCode != 20 && responseGson.errorCode != 21 &&
                                responseGson.errorCode != 22 && responseGson.errorCode != 23) {
                            callback.failure(responseGson.errorMsg);
                        } else {
                            callback.success(apiResponse);
                        }
                    }
                });

    }


}
