package co.real.productionreal2.callback;

import com.quickblox.chat.QBPrivateChat;
import com.quickblox.chat.model.QBChatMessage;

/**
 * Created by hohojo on 18/8/2015.
 */
public interface MainChatViewListener {
    public void updateViewByReceivedMsg(QBPrivateChat chat, QBChatMessage chatMessage);

    //    public void qbUserCallback(String callbackMsg);
//    public void qbSignUpOrLoginCallback(String callbackMsg);
//    public void qbCreateSessionCallback(String callbackMsg);
    public void processMessageDelivered(String messageId, String dialogId, Integer userId);
    public void processMessageRead(String messageId, String dialogId, Integer userId);
    public void updateTabView();


}
