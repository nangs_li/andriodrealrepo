package co.real.productionreal2.activity.createpost;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;

import com.mixpanel.android.mpmetrics.MixpanelAPI;

import java.math.BigDecimal;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import co.real.productionreal2.Constants;
import co.real.productionreal2.Navigation;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.BaseActivity;
import co.real.productionreal2.callback.DialogDualCallback;
import co.real.productionreal2.config.AppConfig;
import co.real.productionreal2.service.model.request.RequestListing;
import co.real.productionreal2.util.AppUtil;
import co.real.productionreal2.view.Dialog;
import co.real.productionreal2.view.MultiPartInputField;
import co.real.productionreal2.view.RealTwoSidesButtonView;


/**
 * Created by hohojo on 16/10/2015.
 */
public class CreatePostUnitDetailActivity extends BaseActivity {
    private static final String TAG = "CreatePostUnitDetailActivity";
    @InjectView(R.id.rootImageView)
    ImageView rootImageView;
    @InjectView(R.id.btn_bedroom)
    protected RealTwoSidesButtonView btnBedroom;
    @InjectView(R.id.btn_bathroom)
    protected RealTwoSidesButtonView btnBathroom;
    @InjectView(R.id.input_price)
    protected MultiPartInputField inputPrice;
    @InjectView(R.id.input_size)
    protected MultiPartInputField inputSize;
    @InjectView(R.id.nextBtn)
    Button btnNext;


    private int langIndex;
    private RequestListing currentRequestListing;
    private int bedroomCount = 0, bathroomCount = 0;

    private BaseCreatePost baseCreatePost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_post_unit_detail);

//        langIndex = getIntent().getIntExtra(Constants.EXTRA_LANG_INDEX, 2);
        langIndex = AppUtil.getCurrentLangIndex(this);
        currentRequestListing = BaseCreatePost.getInstance(CreatePostUnitDetailActivity.this).getCreatePostRequest();
        ButterKnife.inject(this);

        initData();
        initView();
        addListener();
    }

    private void initData() {
        baseCreatePost = BaseCreatePost.getInstance(this);
    }

    private void addListener() {
        inputPrice.getEtDigit().setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    AppUtil.hideKeyboard(CreatePostUnitDetailActivity.this, inputPrice.getEtDigit());
                } else {
                    AppUtil.showKeyboard(CreatePostUnitDetailActivity.this, inputPrice.getEtDigit());
                }
                inputPrice.setSelected(hasFocus);
                inputPrice.getTvApproximation().setSelected(hasFocus);
            }
        });

        inputSize.getEtDigit().setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    AppUtil.hideKeyboard(CreatePostUnitDetailActivity.this, inputSize.getEtDigit());
                } else {
                    AppUtil.showKeyboard(CreatePostUnitDetailActivity.this, inputSize.getEtDigit());
                }
                inputSize.setSelected(hasFocus);
                inputSize.getTvApproximation().setSelected(hasFocus);
            }
        });

        inputPrice.getEtDigit().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                validInput();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        inputSize.getEtDigit().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                validInput();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private void clearEtFocus() {
        inputSize.getEtDigit().clearFocus();
        inputPrice.getEtDigit().clearFocus();
        // Check if no view has focus:
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void initView() {

        inputSize.initType(MultiPartInputField.FIELD_TYPE.SIZE, 0, -1, false);
        inputPrice.initType(MultiPartInputField.FIELD_TYPE.PRICE, 0, -1, false);
        btnBedroom.setMinValueZero(true);
        btnBathroom.setMinValueZero(true);


        RequestListing requestListing = baseCreatePost.getCreatePostRequest();
        if (requestListing != null) {
            inputPrice.setValue(requestListing.propertyPrice);
            inputPrice.setSelectedCurrencyValue(requestListing.currency);
            inputSize.setValue(requestListing.propertySize);
            inputSize.setSizeUnitStr(requestListing.sizeUnit);
            btnBedroom.setValue(requestListing.bedroomCount);
            btnBathroom.setValue(requestListing.bathroomCount);
        }

        clearEtFocus();

        validInput();

    }

    @OnClick(R.id.closeBtn)
    public void closeBtnClick(View view) {
        Dialog.exitSaveChangeDialog(this, new DialogDualCallback() {
            @Override
            public void positive() {
                //reset value
                BaseCreatePost.getInstance(CreatePostUnitDetailActivity.this).resetBaseCreatePost();
                exitActivity();
            }

            @Override
            public void negative() {
                //exit
                saveInputData();
                exitActivity();
            }
        }).show();
    }

    private void exitActivity() {
        setResult(RESULT_OK);
        finish();
    }

    @OnClick(R.id.backBtn)
    public void backBtnClick(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        saveInputData();
        setResult(RESULT_CANCELED);
        finish();
    }

    @OnClick(R.id.nextBtn)
    public void nextBtnClick(View view) {

        // Mixpanel
        MixpanelAPI mixpanel = MixpanelAPI.getInstance(this, AppConfig.getMixpanelToken());
        mixpanel.track("Saved Posting Details_Price");

        saveInputData();
//            Intent intent = new Intent(this, CreatePostPhotosActivity.class);
        Intent intent = new Intent(this, CreatePostWhyActivity.class);
        intent.putExtra(Constants.EXTRA_LANG_INDEX, langIndex);
        Navigation.pushIntentForResult(this, intent, CreatePostPropertyActivity.FINISH_POST);


    }

    private void saveInputData() {
        currentRequestListing.bathroomCount = btnBathroom.getValue();
        currentRequestListing.bedroomCount = btnBedroom.getValue();
        currentRequestListing.propertyPrice = inputPrice.getValueDecimal();
        currentRequestListing.currency = inputPrice.getSelectedCurrencyStr();
        currentRequestListing.propertySize = inputSize.getValue();
        currentRequestListing.sizeUnit = inputSize.getSizeUnitStr();
        //to match with the server metric index
        currentRequestListing.sizeUnitType = inputSize.getSelectedMetric();

    }

    private void validInput() {
        btnNext.setEnabled((!(inputPrice.getValueDecimal() == BigDecimal.valueOf(0)) &&
                !(inputSize.getValue() == 0)));

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == CreatePostPropertyActivity.FINISH_POST) {
            setResult(RESULT_OK, data);
            finish();
        }
    }


}
