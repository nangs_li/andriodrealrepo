package co.real.productionreal2.model;

public class Uploadfile {

	
	private String imageFilePath;
	
	private String imageName;

	public Uploadfile(String imageFilePath, String imageName) {
		super();
		this.imageFilePath = imageFilePath;
		this.imageName = imageName;
	}

	public String getImageFilePath() {
		return imageFilePath;
	}

	public void setImageFilePath(String imageFilePath) {
		this.imageFilePath = imageFilePath;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
	
	
	
}
