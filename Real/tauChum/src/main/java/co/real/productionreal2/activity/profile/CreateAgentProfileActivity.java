package co.real.productionreal2.activity.profile;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.nhaarman.listviewanimations.ArrayAdapter;
import com.nhaarman.listviewanimations.appearance.simple.AlphaInAnimationAdapter;
import com.nhaarman.listviewanimations.itemmanipulation.dragdrop.OnItemMovedListener;
import com.nhaarman.listviewanimations.itemmanipulation.swipedismiss.OnDismissCallback;
import com.nhaarman.listviewanimations.itemmanipulation.swipedismiss.undo.SimpleSwipeUndoAdapter;
import com.squareup.picasso.Picasso;
import com.yalantis.ucrop.UCrop;
import com.yalantis.ucrop.UCropActivity;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import co.real.productionreal2.Constants;
import co.real.productionreal2.CoreData;
import co.real.productionreal2.Navigation;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.AgentExpActivity;
import co.real.productionreal2.activity.AgentLangActivity;
import co.real.productionreal2.activity.AgentSpecActivity;
import co.real.productionreal2.activity.AgentTop5PastClosingActivity;
import co.real.productionreal2.activity.BaseActivity;
import co.real.productionreal2.adapter.PastClosingAdapter;
import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.callback.LangSpecDelCallback;
import co.real.productionreal2.common.LocalStorageHelper;
import co.real.productionreal2.config.AppConfig;
import co.real.productionreal2.dao.account.DBLogInfo;
import co.real.productionreal2.daomanager.DatabaseManager;
import co.real.productionreal2.service.AdminService;
import co.real.productionreal2.service.model.AgentExp;
import co.real.productionreal2.service.model.AgentPastClosing;
import co.real.productionreal2.service.model.AgentPastClosingPosition;
import co.real.productionreal2.service.model.AgentProfiles;
import co.real.productionreal2.service.model.AgentSpecialty;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.request.RequestAgentPastClosing;
import co.real.productionreal2.service.model.request.RequestAgentProfile;
import co.real.productionreal2.service.model.request.RequestAgentReg;
import co.real.productionreal2.service.model.response.ResponseAgentProfile;
import co.real.productionreal2.service.model.response.ResponseFileUpload;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;
import co.real.productionreal2.util.AppUtil;
import co.real.productionreal2.util.FileUtil;
import co.real.productionreal2.util.ImageUtil;
import co.real.productionreal2.view.ExpandableHeightDynamicListView;
import widget.RoundedImageView;

;

/**
 * Created by hohojo on 11/9/2015.
 */
public class CreateAgentProfileActivity extends BaseActivity {
    @InjectView(R.id.rootView)
    RelativeLayout rootView;
    @InjectView(R.id.licenseNoEditText)
    EditText licenseNoEditText;
    @InjectView(R.id.agentNameTextView)
    TextView agentNameTextView;
    @InjectView(R.id.agentImageView)
    RoundedImageView agentImageView;
    @InjectView(R.id.exp_layout)
    LinearLayout expLayout;
    @InjectView(R.id.langResultLinearLayout)
    LinearLayout langResultLinearLayout;
    @InjectView(R.id.specResultLinearLayout)
    LinearLayout specResultLinearLayout;
    @InjectView(R.id.past_exp_dynamiclistview)
    ExpandableHeightDynamicListView pastClosingListView;
    @InjectView(R.id.doneBtn)
    Button btnDone;

    private static final int REQUEST_SELECT_PICTURE = 10011;
    private static final String SAMPLE_CROPPED_IMAGE_NAME = "cropped_pic";
    private Uri mDestinationUri;
    private String imageFilePath;
    private ProgressDialog ringProgressDialog;

    private RequestAgentReg agentProfiles;
    private VertHoriLinearLayout vertHoriLinearLayoutForLang;
    private VertHoriLinearLayout vertHoriLinearLayoutForSpec;

    private static final String REQUEST_AGENT_EXP_TAG = "RequestAgentExperience";

    private ResponseLoginSocial.Content agentContent;
    public static final int EXP = 0;
    public static final int LANG = 1;
    public static final int TOP_5_PAST_CLOSING = 2;
    public static final int SPECICALTY = 3;

    private ArrayAdapter<AgentPastClosing> pastClosingAdapter;

    private void getIntentInfo() {
        Intent intent = getIntent();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_agent_profile);
        getIntentInfo();

        ButterKnife.inject(this);
        initView();

        addListener();

        agentProfiles = new RequestAgentReg(
                agentContent.memId,
                LocalStorageHelper.getAccessToken(getBaseContext()));

    }

    private void addListener() {
        licenseNoEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                updateUI();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }


    private void initPastClosingView() {
        //todo init past closing listview
        pastClosingAdapter = new PastClosingAdapter(this, closingRemoveBtnClickListener, agentContent.systemSetting.propertyTypeList);
        SimpleSwipeUndoAdapter simpleSwipeUndoAdapter = new SimpleSwipeUndoAdapter(pastClosingAdapter, this,
                new PastClosingOnDismissCallback(pastClosingAdapter));
        AlphaInAnimationAdapter animAdapter = new AlphaInAnimationAdapter(simpleSwipeUndoAdapter);
        animAdapter.setAbsListView(pastClosingListView);
        assert animAdapter.getViewAnimator() != null;
        animAdapter.getViewAnimator().setInitialDelayMillis(ExpandableHeightDynamicListView.ANIM_INITIAL_DELAY_MILLIS);
        pastClosingListView.setAdapter(animAdapter);

        /* Enable drag and drop functionality */
        pastClosingListView.enableDragAndDrop();
//        pastClosingListView.setDraggableManager(new TouchViewDraggableManager(R.id.list_row_draganddrop_touchview2));
        pastClosingListView.setOnItemMovedListener(new PastClosingOnItemMovedListener(pastClosingAdapter));
        pastClosingListView.setOnItemLongClickListener(new PastClosingOnItemLongClickListener(pastClosingListView));

        pastClosingListView.setAdapter(pastClosingAdapter);
        pastClosingListView.setExpanded(true);
    }

    private void initView() {

        btnDone.setEnabled(getCompleteRate() > 0);

        //add fade in effect
        Animation a = AnimationUtils.loadAnimation(this, R.anim.fade_in_half);
        a.reset();
        a.setDuration(500);
        rootView.clearAnimation();
        rootView.startAnimation(a);

        agentContent = CoreData.getUserContent(this);
        agentNameTextView.setText(agentContent.memName);

        initPastClosingView();

        Picasso.with(this)
                .load(agentContent.photoUrl)
                .error(R.drawable.default_profile)
                .placeholder(R.drawable.default_profile)
                .into(agentImageView);
    }

    private void appendAgentExpView(final AgentExp agentExp, int tagNo) {
        final int tagNum = tagNo;
        LayoutInflater layoutInflater = getLayoutInflater();
        View view = layoutInflater.inflate(R.layout.agent_exp_item, null);
        view.setTag(REQUEST_AGENT_EXP_TAG + tagNum);

        TextView companyTextView = ButterKnife.findById(view, R.id.companyTextView);
        TextView titleTextView = ButterKnife.findById(view, R.id.titleTextView);
        TextView periodTextView = ButterKnife.findById(view, R.id.periodTextView);
        ImageButton removeBtn = ButterKnife.findById(view, R.id.removeExpBtn);
        ImageButton expEditBtn = ButterKnife.findById(view, R.id.expEditBtn);
        companyTextView.setTextColor(Color.WHITE);
        titleTextView.setTextColor(Color.WHITE);
        periodTextView.setTextColor(Color.WHITE);
        companyTextView.setText(agentExp.company);
        titleTextView.setText(agentExp.title);
        periodTextView.setText(agentExp.getExpPeriodText(this));

        removeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                agentProfiles.agentExpList.remove(tagNum);
                recreateExpLayout();
            }
        });

        expEditBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CreateAgentProfileActivity.this, AgentExpActivity.class);
                intent.putExtra(Constants.EXTRA_AGENT_EXP_IS_EDIT, true);
                intent.putExtra(Constants.EXTRA_AGENT_EXP_EDIT_TAG_NO, tagNum);
                intent.putExtra(Constants.EXTRA_AGENT_EXP_EDIT_CONTENT, agentExp);
                Navigation.pushIntentForResult(CreateAgentProfileActivity.this, intent, EXP);
            }
        });

        expLayout.addView(view);
    }

    private void recreateExpLayout() {
        expLayout.removeAllViews();
        agentProfiles.sortAgentExps();
        for (int i = 0; i < agentProfiles.agentExpList.size(); i++) {
            appendAgentExpView(agentProfiles.agentExpList.get(i), i);
        }

        updateUI();
    }

    private void recreateLangLayout() {
        langResultLinearLayout.post(new Runnable() {
            @Override
            public void run() {
                ArrayList<String> nativeNameList = new ArrayList<>();
                for (int i = 0; i < agentProfiles.agentLangList.size(); i++) {
                    for (int j = 0; j < agentContent.systemSetting.spokenLangList.size(); j++) {
                        if (agentContent.systemSetting.spokenLangList.get(j).index ==
                                agentProfiles.agentLangList.get(i).langIndex) {
                            nativeNameList.add(agentContent.systemSetting.spokenLangList.get(j).nativeName);
                        }
                    }
                }

                vertHoriLinearLayoutForLang = new VertHoriLinearLayout(CreateAgentProfileActivity.this, langResultLinearLayout,
                        nativeNameList,
                        langResultLinearLayout.getMeasuredWidth(), VertHoriLinearLayout.PROFILE_LANG,
                        new LangSpecDelCallback() {
                            @Override
                            public void onLangSpecDel(int tagNo, int type) {
                                agentProfiles.agentLangList.remove(tagNo);
                                vertHoriLinearLayoutForLang.removeViewAndGetContentList(tagNo);

                                updateUI();
                            }
                        });
            }
        });
    }

    private void recreateSpeclayout() {
        specResultLinearLayout.post(new Runnable() {
            @Override
            public void run() {
                vertHoriLinearLayoutForSpec = new VertHoriLinearLayout(CreateAgentProfileActivity.this, specResultLinearLayout,
                        VertHoriLinearLayout.agentSpecListToStringList((ArrayList<AgentSpecialty>) agentProfiles.agentSpecialityList),
                        specResultLinearLayout.getMeasuredWidth(), VertHoriLinearLayout.PROFILE_SPEC,
                        new LangSpecDelCallback() {
                            @Override
                            public void onLangSpecDel(int tagNo, int type) {
                                agentProfiles.agentSpecialityList.remove(tagNo);
                                vertHoriLinearLayoutForSpec.removeViewAndGetContentList(tagNo);
                                updateUI();
                            }
                        });
            }
        });
    }


    private void recreatePastClosingLayout(AgentPastClosing agentPastClosing) {
        pastClosingAdapter.add(agentPastClosing);
        updateUI();
    }

    private final void intentToPickImage() {
//        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
//        photoPickerIntent.setType("image/*");
//        startActivityForResult(photoPickerIntent, SELECT_PHOTO);
        pickFromGallery();
    }

    @OnClick(R.id.agentImageView)
    public void agentImageViewClick(View view) {
        //todo select image and call api
        intentToPickImage();

    }

    @OnClick(R.id.addNewExp)
    public void addNewExp(View view) {
        Intent intent = new Intent(this, AgentExpActivity.class);
        Navigation.pushIntentForResult(this, intent, EXP);
    }

    @OnClick(R.id.addNewLang)
    public void addNewLang(View view) {
        Intent intent = new Intent(this, AgentLangActivity.class);
        int[] selLangIndex = new int[agentProfiles.agentLangList.size()];
        for (int i = 0; i < agentProfiles.agentLangList.size(); i++) {
            selLangIndex[i] = agentProfiles.agentLangList.get(i).langIndex;
        }
        intent.putParcelableArrayListExtra(Constants.EXTRA_SPOKEN_LANG, (ArrayList<? extends Parcelable>) agentContent.systemSetting.spokenLangList);
        intent.putParcelableArrayListExtra(Constants.EXTRA_AGENT_LANG, (ArrayList<? extends Parcelable>) agentProfiles.agentLangList);
        Navigation.pushIntentForResult(this, intent, LANG);
    }

    @OnClick(R.id.addNewSpec)
    public void addNewSpec(View view) {
        Intent intent = new Intent(this, AgentSpecActivity.class);
        intent.putExtra(Constants.EXTRA_AGENT_SPEC_LIST, (ArrayList<? extends Parcelable>) agentProfiles.agentSpecialityList);
        Navigation.pushIntentForResult(this, intent, SPECICALTY);
    }

    @OnClick(R.id.addNewTop5Closing)
    public void addNewTop5Closing(View view) {
        Intent intent = new Intent(this, AgentTop5PastClosingActivity.class);

        intent.putExtra(Constants.EXTRA_AGENT_PROPERTY_TYPE_LIST, (ArrayList<? extends Parcelable>) agentContent.systemSetting.propertyTypeList);
        Navigation.pushIntentForResult(this, intent, TOP_5_PAST_CLOSING);
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////for moveable listview //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private PastClosingAdapter.ClosingRemoveBtnClickListener closingRemoveBtnClickListener = new PastClosingAdapter.ClosingRemoveBtnClickListener() {
        @Override
        public void onClosingRemove(int position) {
            pastClosingAdapter.remove(position);
        }
    };

    ///////////////////////////////////////////////// PastClosingOnDismissCallback /////////////////////////////////////////////////
    private class PastClosingOnDismissCallback implements OnDismissCallback {
        private final ArrayAdapter<AgentPastClosing> agentPastClosingAdapter;

        PastClosingOnDismissCallback(final ArrayAdapter<AgentPastClosing> adapter) {
            agentPastClosingAdapter = adapter;
        }

        @Override
        public void onDismiss(ViewGroup viewGroup, int[] reverseSortedPositions) {
            for (int position : reverseSortedPositions) {
                agentPastClosingAdapter.remove(position);
            }
        }
    }
    ///////////////////////////////////////////////// PastClosingOnDismissCallback /////////////////////////////////////////////////

    ///////////////////////////////////////////////// PastClosingOnItemMovedListener /////////////////////////////////////////////////
    private class PastClosingOnItemMovedListener implements OnItemMovedListener {
        private final ArrayAdapter<AgentPastClosing> agentPastClosingAdapter;

        private Toast mToast;

        PastClosingOnItemMovedListener(final ArrayAdapter<AgentPastClosing> adapter) {
            agentPastClosingAdapter = adapter;
        }

        @Override
        public void onItemMoved(final int originalPosition, final int newPosition) {

            Log.d("test dryanic", "onItemMoved " + originalPosition + " , " + newPosition);


            agentProfiles.agentPastCloseList = agentPastClosingAdapter.getItems();

            List<AgentPastClosingPosition> apiPastClosingList = new ArrayList<AgentPastClosingPosition>();

            int i = 0;
            for (AgentPastClosing adsfasd : agentProfiles.agentPastCloseList) {

                AgentPastClosingPosition tee = new AgentPastClosingPosition();
                tee.id = adsfasd.id;
                tee.position = i;
                apiPastClosingList.add(tee);
                i++;
            }


            RequestAgentPastClosing updateClosing = new RequestAgentPastClosing(
                    agentContent.memId,
                    LocalStorageHelper.getAccessToken(getBaseContext()));
            updateClosing.agentPastCloseList.addAll(apiPastClosingList);

        }
    }
    ///////////////////////////////////////////////// PastClosingOnItemMovedListener /////////////////////////////////////////////////

    ///////////////////////////////////////////////// PastClosingOnItemLongClickListener /////////////////////////////////////////////////
    private class PastClosingOnItemLongClickListener implements AdapterView.OnItemLongClickListener {
        private final ExpandableHeightDynamicListView listView;

        PastClosingOnItemLongClickListener(final ExpandableHeightDynamicListView listView) {
            this.listView = listView;
        }

        @Override
        public boolean onItemLongClick(final AdapterView<?> parent, final View view, final int position, final long id) {
            if (listView != null) {

                Log.d("test dryanic", "start drag");
                listView.markDragging();
                listView.startDragging(position - listView.getHeaderViewsCount());
            }
            return true;
        }
    }
    ///////////////////////////////////////////////// PastClosingOnItemLongClickListener /////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////for moveable listview //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @OnClick(R.id.closeBtn)
    public void closeBtnClick(View view) {
        finish();
        setResult(RESULT_CANCELED);
    }

    @OnClick(R.id.doneBtn)
    public void doneBtnClick(View view) {

        // Mixpanel
        MixpanelAPI mixpanel = MixpanelAPI.getInstance(this, AppConfig.getMixpanelToken());
        mixpanel.track("Saved Agent Profile Details");

        String licenseNo = licenseNoEditText.getText().toString();
        agentProfiles.agentLicNum = licenseNo.length() > 0 ? licenseNo : " ";
//        agentProfiles.agentName = agentContent.memName;
//        agentProfiles.qbId = agentContent.qbid;
//        agentProfiles.uniqueKey = UUID.randomUUID().toString();

        final ProgressDialog ringProgressDialog = ProgressDialog.show(this, "", getResources().getString(R.string.alert__Initializing));
        ringProgressDialog.setCancelable(false);
        agentProfiles.registerAgentService(this, agentProfiles, new ApiCallback() {
            @Override
            public void success(ApiResponse apiResponse) {

                // Mixpanel
                MixpanelAPI mixpanel = MixpanelAPI.getInstance(CreateAgentProfileActivity.this, AppConfig.getMixpanelToken());
                mixpanel.track("Became Agent");
                mixpanel.getPeople().set("Agent", "YES");
                mixpanel.getPeople().set("Agent since", AppUtil.dateStringForMixpanel());

//                FileUtil.writeJsonToFile("agentProfile.json", apiResponse.jsonContent);
                //todo call agentprofileget and update to db
                RequestAgentProfile request = new RequestAgentProfile(
                        agentContent.memId,
                        LocalStorageHelper.getAccessToken(getBaseContext()), 5);
                request.getAgentProfile(CreateAgentProfileActivity.this, request, new ApiCallback() {
                    @Override
                    public void success(ApiResponse apiResponse) {
                        ringProgressDialog.dismiss();
                        Gson gson = new Gson();
                        ResponseAgentProfile agentProfileResponse = gson.fromJson(apiResponse.jsonContent, ResponseAgentProfile.class);
                        AgentProfiles agentProfile = agentProfileResponse.content.profile;

                        DatabaseManager databaseManager = DatabaseManager.getInstance(CreateAgentProfileActivity.this);
                        DBLogInfo dbLogInfo = databaseManager.getLogInfo();

                        ResponseLoginSocial.Content userContent = new Gson().fromJson(dbLogInfo.getLogInfo(),
                                ResponseLoginSocial.Content.class);
                        userContent.agentProfile = agentProfile;
                        CoreData.setUserContent(userContent);
                        dbLogInfo.setLogInfo(gson.toJson(userContent));

                        databaseManager.updateLogInfo(dbLogInfo);

                        setResult(RESULT_OK);
                        finish();

                    }

                    @Override
                    public void failure(String errorMsg) {
                        AppUtil.showToast(CreateAgentProfileActivity.this, getString(R.string.error_message__please_try_again_later));
                        ringProgressDialog.dismiss();
                    }
                });

            }

            @Override
            public void failure(String errorMsg) {
                AppUtil.showToast(CreateAgentProfileActivity.this, getString(R.string.error_message__please_try_again_later));
                ringProgressDialog.dismiss();
            }

        });
    }

    private void updateUI() {
        btnDone.setEnabled(getCompleteRate() > 0);
    }


    private int getCompleteRate() {
        int completeRate = 0;
        if (agentContent == null)
            return 0;
        if (agentProfiles == null)
            return 0;

        if (agentProfiles.agentExpList != null && agentProfiles.agentExpList.size() > 0) {
            completeRate = completeRate + 20;
        }
        if (licenseNoEditText.getText().toString().trim().length() > 0) {
            completeRate = completeRate + 20;
        }
        if (agentProfiles.agentLangList != null && agentProfiles.agentLangList.size() > 0) {
            completeRate = completeRate + 20;
        }
        if (agentProfiles.agentSpecialityList != null && agentProfiles.agentSpecialityList.size() > 0) {
            completeRate = completeRate + 20;
        }
        if (agentProfiles.agentPastCloseList != null && agentProfiles.agentPastCloseList.size() > 0) {
            completeRate = completeRate + 20;
        }
        Log.d("getCompleteRate", "getCompleteRate: " + completeRate);
        return completeRate;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == EXP) {
                AgentExp agentExp = data.getParcelableExtra(Constants.EXTRA_REQUEST_AGENT_EXP);
                // delete and add data
                if (data.getBooleanExtra(Constants.EXTRA_AGENT_EXP_IS_EDIT, false)) {
                    int tagNo = data.getIntExtra(Constants.EXTRA_AGENT_EXP_EDIT_TAG_NO, 0);
                    agentProfiles.agentExpList.remove(tagNo);
                }
                agentProfiles.agentExpList.add(agentExp);
                //change view
                recreateExpLayout();
            } else if (requestCode == LANG) {
                // add data
                agentProfiles.agentLangList = data.getParcelableArrayListExtra(Constants.EXTRA_UPDATED_AGENT_LANG);
                ArrayList<String> nativeNameList = data.getStringArrayListExtra(Constants.EXTRA_UPDATED_AGENT_LANG_NATIVE_NAME);
                //change view
                recreateLangLayout();

            } else if (requestCode == SPECICALTY) {
                // add data
                agentProfiles.agentSpecialityList = data.getParcelableArrayListExtra(Constants.EXTRA_AGENT_SPEC_LIST);
                //change view
                recreateSpeclayout();

            } else if (requestCode == TOP_5_PAST_CLOSING) {
                AgentPastClosing agentPastClosing = data.getParcelableExtra(Constants.EXTRA_AGENT_PAST_CLOSING);
                agentProfiles.agentPastCloseList.add(agentPastClosing);

                recreatePastClosingLayout(agentPastClosing);

            } else if (resultCode == RESULT_OK && requestCode == REQUEST_SELECT_PICTURE) {
                try {
                    final Uri selectedUri = data.getData();
                    startCropActivity(selectedUri);
                    Log.w("imageFilePath", "UCrop = ok request crop:  " + selectedUri);
                } catch (Exception e) {
                    Toast.makeText(this, R.string.chatroom_error_message__incompatible_error, Toast.LENGTH_SHORT).show(); //TODO: general error for some not supporting image
                }
            } else if (resultCode == RESULT_OK && requestCode == UCrop.REQUEST_CROP) {
                final Uri resultUri = UCrop.getOutput(data);
                imageFilePath = FileUtil.getRealPathFromURI(this, resultUri);
                Log.w("imageFilePath", "imageFilePath = " + imageFilePath);
                Picasso.with(this)
                        .load(new File(imageFilePath))
                        .error(R.drawable.com_facebook_profile_picture_blank_square)
                        .into(agentImageView);
                //show progress dialog
                ringProgressDialog = ProgressDialog.show(this, "",
                        getResources().getString(R.string.alert__Initializing));

                new UploadFileTask().execute("");
                Log.w("imageFilePath", "UCrop = ok " + resultUri);
            } else if (resultCode == UCrop.RESULT_ERROR) {
                Log.w("imageFilePath", "UCrop = err");
                final Throwable cropError = UCrop.getError(data);
            }
            btnDone.setEnabled(getCompleteRate() > 0);
        }
    }


    private void pickFromGallery() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN // Permission was added in API Level 16
                && ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    REQUEST_STORAGE_READ_ACCESS_PERMISSION);
        } else {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            startActivityForResult(Intent.createChooser(intent, getResources().getString(R.string.common__select)), REQUEST_SELECT_PICTURE);
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_STORAGE_READ_ACCESS_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    pickFromGallery();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    /**
     * In most cases you need only to set crop aspect ration and max size for resulting image.
     *
     * @param uCrop - ucrop builder instance
     * @return - ucrop builder instance
     */
    private UCrop basisConfig(@NonNull UCrop uCrop) {

        uCrop = uCrop.useSourceImageAspectRatio();
        uCrop = uCrop.withAspectRatio(1, 1);
//        try {
//            float ratioX = Constants.resizeImageWidth;
//            float ratioY = Constants.resizeImageWidth;
//            if (ratioX > 0 && ratioY > 0) {
//                uCrop = uCrop.withAspectRatio(ratioX, ratioY);
//            }
//        } catch (NumberFormatException e) {
//        }

        return uCrop;
    }


    /**
     * Sometimes you want to adjust more options, it's done via {@link com.yalantis.ucrop.UCrop.Options} class.
     *
     * @param uCrop - ucrop builder instance
     * @return - ucrop builder instance
     */
    private UCrop advancedConfig(@NonNull UCrop uCrop) {
        UCrop.Options options = new UCrop.Options();
        options.setCompressionFormat(Bitmap.CompressFormat.JPEG);
//        options.setCompressionQuality(100);


        // If you want to configure how gestures work for all UCropActivity tabs
        options.setAllowedGestures(UCropActivity.SCALE, UCropActivity.ROTATE, UCropActivity.ALL);


        /*
        This sets max size for bitmap that will be decoded from source Uri.
        More size - more memory allocation, default implementation uses screen diagonal.
        options.setMaxBitmapSize(640);
        * */

        //Tune everything (ﾉ◕ヮ◕)ﾉ*:･ﾟ✧
//        options.setMaxScaleMultiplier(5);
//        options.setImageToCropBoundsAnimDuration(666);
//        options.setDimmedLayerColor(Color.GRAY);
        options.setOvalDimmedLayer(true);
        options.setShowCropFrame(false);
        options.setShowCropGrid(false);
//        options.setCropGridStrokeWidth(20);
//        options.setCropGridColor(Color.WHITE);
//        options.setCropGridColumnCount(2);
//        options.setCropGridRowCount(1);
        // Color palette
        options.setToolbarColor(ContextCompat.getColor(this, R.color.tab_bg));
        options.setStatusBarColor(ContextCompat.getColor(this, R.color.tab_bg));
        options.setActiveWidgetColor(ContextCompat.getColor(this, R.color.holo_blue));
        options.setToolbarWidgetColor(ContextCompat.getColor(this, R.color.white));

        return uCrop.withOptions(options);
    }

    private void startCropActivity(@NonNull Uri uri) {
        mDestinationUri = Uri.fromFile(new File(getCacheDir(), SAMPLE_CROPPED_IMAGE_NAME + System.currentTimeMillis() + ".jpeg"));
        UCrop uCrop = UCrop.of(uri, mDestinationUri);

        uCrop = basisConfig(uCrop);
        uCrop = advancedConfig(uCrop);

        uCrop.start(this);
    }


    private String callUploadFileApi(DefaultHttpClient httpClient, File photoFile) {

        HttpPost httppost = new HttpPost(AdminService.WEB_SERVICE_BASE_URL + "/Admin.asmx/FileUpload");
        MultipartEntity multipartEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);

        try {
            multipartEntity.addPart("MemberID", new StringBody("" + agentContent.memId));
            multipartEntity.addPart("AccessToken", new StringBody(LocalStorageHelper.getAccessToken(getBaseContext())));
            multipartEntity.addPart("ListingID", new StringBody("0"));
            multipartEntity.addPart("Position", new StringBody("0"));
            multipartEntity.addPart("FileExt", new StringBody(FileUtil.getFileExt(photoFile.getAbsolutePath())));
            multipartEntity.addPart("FileType", new StringBody("2"));
            multipartEntity.addPart("attachment", new FileBody(photoFile));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        httppost.setEntity(multipartEntity);
        try {
            HttpResponse httpResponse = httpClient.execute(httppost);
            InputStream is = httpResponse.getEntity().getContent();
            String responseString = FileUtil.convertStreamToString(is);
            Document doc = FileUtil.getDomElement(responseString);
            doc.getDocumentElement().normalize();
            NodeList nodeList = doc.getElementsByTagName("string");
            Node node = nodeList.item(0);
            String json = FileUtil.nodeToString(node.getFirstChild());
            if (json != null) {
                ResponseFileUpload responseFileUpload = new Gson().fromJson(json, ResponseFileUpload.class);
                if (responseFileUpload.content == null)
                    return null;
                if (responseFileUpload.content.photoUrl == null)
                    return null;
                return responseFileUpload.content.photoUrl;
            }

            return null;

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * upload task
     */
    private class UploadFileTask extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... imageUri) {

            HttpParams params = new BasicHttpParams();
            params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
            DefaultHttpClient httpClient = new DefaultHttpClient(params);
            return resizePhotoAndPublish(httpClient);
        }

        @Override
        protected void onProgressUpdate(String... values) {
            Log.w("EditAgentProfile", "onProgressUpdate　photoUrl = " + values[0]);
        }

        @Override
        protected void onPostExecute(String photoURL) {
            CoreData.isAgentProfileUpdate = true;
        }

        private String resizePhotoAndPublish(DefaultHttpClient httpClient) {
            File imageInDevice = new File(FileUtil.getAppSubFolder(Constants.UPLOAD_FILE_FOLDER_NAME),
                    "UserImage.jpg");
//            Bitmap finalizedBm=ImageUtil.decodeSampledBitmapFromResource(imageFilePath);
            try {
//                FileUtil.convertBitmapToFile(imageInDevice.toString(),finalizedBm);
                ImageUtil.getCompressImageFilePath(imageFilePath, imageInDevice.getAbsolutePath(), ImageUtil.iconImageType);
            } catch (Exception e) {
                e.printStackTrace();
            }
            String photoUrl = callUploadFileApi(httpClient, imageInDevice);
            publishProgress(photoUrl);

            //update profile img
//            DatabaseManager.getInstance(EditAgentProfileActivity.this).getLogInfo().set
            agentContent.photoUrl = photoUrl;
            CoreData.setUserContent(agentContent);
            DBLogInfo dbLogInfo = DatabaseManager.getInstance(CreateAgentProfileActivity.this).getLogInfo();
            dbLogInfo.setLogInfo(new Gson().toJson(agentContent));
            DatabaseManager.getInstance(CreateAgentProfileActivity.this).updateLogInfo(dbLogInfo);
            if (ringProgressDialog != null) {
                ringProgressDialog.dismiss();
                ringProgressDialog.cancel();
                ringProgressDialog = null;
            }
            return photoUrl;
        }
    }

}
