package co.real.productionreal2.service.model.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by hohojo on 19/10/2015.
 */
public class ResponseFileUpload extends ResponseBase {

    @SerializedName("Content")
    public Content content;

    public class Content {

        @SerializedName("PhotoURL")
        public String photoUrl;

        @SerializedName("PhotoID")
        public String photoId;

    }


}

