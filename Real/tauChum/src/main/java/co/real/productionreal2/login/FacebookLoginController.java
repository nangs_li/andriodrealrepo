package co.real.productionreal2.login;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginBehavior;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONObject;

import java.util.Arrays;

import co.real.productionreal2.service.AdminService;
import co.real.productionreal2.service.model.request.RequestSocialLogin;
import co.real.productionreal2.util.FileUtil;

/**
 * Created by hohojo on 25/8/2015.
 */
public class FacebookLoginController extends BaseLoginController {
    private static final String TAG = "FacebookLoginController";
    private CallbackManager callbackManager;
    private static FacebookLoginController instance;
    public static final int REQUEST_CODE = 64206;

    public FacebookLoginController(Context context) {
        super(context);
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().logOut();
        LoginManager.getInstance().setLoginBehavior(LoginBehavior.SUPPRESS_SSO); //Web ONLY
        LoginManager.getInstance().logInWithReadPermissions((Activity) context, Arrays.asList("public_profile", "email"));
        LoginManager.getInstance().registerCallback(callbackManager, fbCallback);
    }


    public static FacebookLoginController getInstance(Context context) {
        if (instance == null) {
            instance = new FacebookLoginController(context);
        }
        return instance;
    }

    private FacebookCallback<LoginResult> fbCallback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(final LoginResult loginResult) {
            Log.w(TAG, "getAccessToken: " + loginResult.getAccessToken().getToken());
            Log.w(TAG, "getRecentlyDeniedPermissions: " + loginResult.getRecentlyDeniedPermissions());
            Log.w(TAG, "getRecentlyGrantedPermissions: " + loginResult.getRecentlyGrantedPermissions());
            GraphRequest request = GraphRequest.newMeRequest(
                    loginResult.getAccessToken(),
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(
                                JSONObject object,
                                GraphResponse response) {

                            // Application code
                            Log.v(TAG,"edwinfacebook FacebookCallback onSuccess newMeRequest getAccessToken onCompleted");
                            if(object != null) {
                                Log.d(TAG, "object = " + object);
                                Log.d(TAG, "response = " + response);
                                Log.d(TAG, "rawResponse = " + response.getRawResponse());
//                            FacebookLoginResponse facebookLoginResponse = new Gson().fromJson(response.getRawResponse(), FacebookLoginResponse.class);
                                setRequestSocialLoginRegisterWithAccessToken(object, loginResult.getAccessToken().getToken());
                            }else{
                                loginFail("fbCallback fail: " + response.getError());
                            }
                        }
                    });
            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,name,email,picture");
            request.setParameters(parameters);
            request.executeAsync();
        }

        @Override
        public void onCancel() {
            loginCancel();
        }

        @Override
        public void onError(FacebookException e) {
            Log.w(TAG, "onError = " + e);
            loginFail("fbCallback fail: " + e.getMessage());
        }
    };

    private void setRequestSocialLoginRegisterWithAccessToken(JSONObject jsonObject, String accessToken) {

        Log.w(TAG, "getName: " + jsonObject.optString("name"));
        Log.w(TAG, "getId: " + jsonObject.optString("id"));
        Log.w(TAG, "getEmail: " + jsonObject.optString("email"));
        if (Profile.getCurrentProfile() != null)
            Log.w(TAG, "getProfilePictureUri: " + Profile.getCurrentProfile().getProfilePictureUri(80, 80).toString());
        //handle no profile pic case
        String profileUrl = null;
        try {
            if (jsonObject != null && jsonObject.optJSONObject("picture") != null) {
                profileUrl = jsonObject.optJSONObject("picture").optJSONObject("data").optString("url");
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        if(profileUrl == null || profileUrl.isEmpty()) {
            profileUrl = "https://s3.amazonaws.com/real-files-bucket/default_profile.png";
        }

        RequestSocialLogin socialRequest = new RequestSocialLogin(
                AdminService.DEVICE_TYPE_ANDROID,
                FileUtil.getDeviceId(context),
                AdminService.SOCIAL_FACEBOOK,
                jsonObject.optString("name"),
                jsonObject.optString("id"),
                jsonObject.optString("email"),
                profileUrl,
                accessToken);
        socialRequest.callLoginSocialApi(context);
    }

    private AccessTokenTracker accessTokenTracker = new AccessTokenTracker() {
        @Override
        protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
            // Set the access token using
            // currentAccessToken when it's loaded or set.
        }
    };

    private ProfileTracker profileTracker = new ProfileTracker() {
        @Override
        protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {
        }
    };

    private class FacebookLoginResponse {
        private String id;
        private String name;
        private String email;
        private String gender;


        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }


    }


    //LifeCycle from IntroActivity
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public void onDestroy() {
        accessTokenTracker.stopTracking();
        profileTracker.stopTracking();
    }


}
