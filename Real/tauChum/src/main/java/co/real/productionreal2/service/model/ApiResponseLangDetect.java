package co.real.productionreal2.service.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by kelvinsun on 1/3/16.
 */
public class ApiResponseLangDetect {

        @SerializedName("data")
        public LangDetection data;

    public class LangDetection {

        @SerializedName("detections")
        public List<List<LanguageType>> languageTypes;

        public class LanguageType {

            @SerializedName("language")
            public String language;

            @SerializedName("confidence")
            public float confidence;
        }
    }

}
