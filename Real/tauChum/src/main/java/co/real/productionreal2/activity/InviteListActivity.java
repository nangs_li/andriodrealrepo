package co.real.productionreal2.activity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.ArrayList;

import co.real.productionreal2.R;
import co.real.productionreal2.dao.account.DBPhoneBook;
import co.real.productionreal2.service.model.RePhonebookItem;
import co.real.productionreal2.util.PhoneBookUtil;

/**
 * Created by edwinchan on 1/6/16.
 */
public class InviteListActivity  extends BaseActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LinearLayout ll = new LinearLayout(this);
        ListView lv = new ListView(this);
        ll.addView(lv, LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);
        setContentView(ll);

        ArrayList<DBPhoneBook> pohneList = PhoneBookUtil.loadPhoneBookFromLocal(this);
        String[] values = new String[pohneList.size()];
        for(int i=0;i<values.length;i++){
            values[i] = pohneList.get(i).getName()+" "+pohneList.get(i).getCountryCode()+" "+pohneList.get(i).getPhoneNumber();
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.list_item, values);
        lv.setAdapter(adapter);

    }

}
