package co.real.productionreal2.chat;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import co.real.productionreal2.ApplicationSingleton;
import co.real.productionreal2.BuildConfig;
import co.real.productionreal2.Constants;
import co.real.productionreal2.CoreData;
import co.real.productionreal2.activity.MainActivityTabBase;
import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.chat.core.ChatService;
import co.real.productionreal2.chat.core.PrivateChatImpl;
import co.real.productionreal2.common.FetchAgentProfileHelper;
import co.real.productionreal2.util.FileUtil;
import co.real.productionreal2.util.PermissionUtil;
import co.real.productionreal2.view.TabMenu;

import com.quickblox.chat.QBChatService;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.messages.QBMessages;
import com.quickblox.messages.model.QBSubscription;
import com.quickblox.users.model.QBUser;

import java.util.ArrayList;
import java.util.List;

public class ChatSessionUtil {
    private static final Handler signupOrLoginHandler = new Handler();

    public static void logoutQBuser(final Context context) {
        logoutQBuser(context, null);
    }

    public static void logoutQBuser(final Context context, final ApiCallback callback) {
        if (ChatService.getInstance() == null)
            return;
        PrivateChatImpl.getInstance(context).clearInstance();
        Log.w("ChatSessionUtil", "clearInstance");
        QBChatService.getInstance().logout(new QBEntityCallbackImpl() {

            @Override
            public void onSuccess() {
//                    chatService.destroy();
                if (Constants.isDevelopment)
                    Toast.makeText(context, "chat logout", Toast.LENGTH_LONG).show();
                Log.w("ChatSessionUtil", "Logout success");

                if (callback != null) {
                    callback.success(null);
                }
            }

            @Override
            public void onError(final List list) {
                Log.w("ChatSessionUtil", "Logout fail: " + list);

                if (callback != null) {

                    callback.failure("");
                }
            }
        });

        FetchAgentProfileHelper.getInstance().stopFetching();

    }

    // unregister from QuickBlox
    public static void unsubscriptQBPush(final Context context) {
        Log.w("ChatSessionUtil", "unsubscriptQBPush process");
        QBMessages.getSubscriptions(new QBEntityCallbackImpl<ArrayList<QBSubscription>>() {
            @Override
            public void onSuccess(ArrayList<QBSubscription> subscriptions, Bundle args) {

                //check if permission granted
                if (ContextCompat.checkSelfPermission(context,
                        Manifest.permission.READ_PHONE_STATE)
                        != PackageManager.PERMISSION_GRANTED) {
                    // MY_PERMISSIONS_REQUEST_READ_PHONE_STATUS is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                    return;
                }else {

                    String deviceId = FileUtil.getDeviceId(context);

                    Log.w("ChatSessionUtil", "unsubscriptQBPush process" + deviceId);

                    for (QBSubscription subscription : subscriptions) {
                        if (subscription.getDevice().getId().equals(deviceId)) {
                            QBMessages.deleteSubscription(subscription.getId(), new QBEntityCallbackImpl<Void>() {
                                @Override
                                public void onSuccess() {
                                    Log.w("ChatSessionUtil", "unsubscriptQBPush onSuccess");
                                }

                                @Override
                                public void onError(List<String> errors) {
                                    Log.w("ChatSessionUtil", "unregister QB Push errors : " + errors);
                                }
                            });
                            break;
                        }
                    }
            }

        }

        @Override
        public void onError (List < String > errors) {

        }
    }

    );
}

    public static void callQBLogin(final Context context, final QBUser qbUser) {

        callQBLogin(context, qbUser, null);
    }

    public static void callQBLogin(final Context context, final QBUser qbUser, final ApiCallback callback) {
        ChatService.initIfNeed(context);
        ChatService.getInstance().login(qbUser, new QBEntityCallbackImpl() {

            @Override
            public void onSuccess() {
                if (ApplicationSingleton.getInstance().currentActivity.getClass() == MainActivityTabBase.class) {
                    ChatMainDbViewController.getInstance(MainActivityTabBase.context).qbSignUpOrLoginCallback(ChatMainDbViewController.CALLBACK_SUCCESS);

                    MainActivityTabBase mainActivityTabBase = (MainActivityTabBase) ApplicationSingleton.getInstance().currentActivity;
                    TabMenu menuTabHost = mainActivityTabBase.getTabHost();
                    if (menuTabHost.getChatTabIcon().isSelected()) {
                        mainActivityTabBase.onTabClicked(menuTabHost.getChatTabIcon(), false);
                    }
                }
                Log.w("ChatSessionUtil", "callQBLogin success");
                Handler h = new Handler(context.getMainLooper());
                h.post(new Runnable() {
                    @Override
                    public void run() {
                        ChatSessionUtil.subscribeToPushNotifications(context, CoreData.regId);

                        if (callback != null) {
                            callback.success(null);
                        }
                    }
                });
            }

            @Override
            public void onError(List errors) {
                if (ApplicationSingleton.getInstance().currentActivity.getClass() == MainActivityTabBase.class)
                    ChatMainDbViewController.getInstance(MainActivityTabBase.context).qbSignUpOrLoginCallback("callQBLogin errors:  " + errors);
                Log.e("ChatSessionUtil", "callQBLogin errors:  " + errors);
                if (callback != null) {
                    callback.failure("");
                }
            }
        });

        FetchAgentProfileHelper.getInstance().startFetching();
    }


    /**
     * Subscribe to Push Notifications
     *
     * @param regId registration ID
     */
    public static void subscribeToPushNotifications(Context context, String regId) {
        // Create push token with Registration Id for Android
        //
        Log.d("ChatSessionUtil", "subscribing...");
        Log.w("", "GCMTesting subscribeToPushNotifications regId " + regId + " FileUtil.getDeviceId(context) " + FileUtil.getDeviceId(context));


        QBMessages.subscribeToPushNotificationsTask(regId, FileUtil.getDeviceId(context),
                Constants.QB_ENVIROMENT,
                new QBEntityCallbackImpl<ArrayList<QBSubscription>>() {
                    @Override
                    public void onSuccess(
                            ArrayList<QBSubscription> qbSubscriptions,
                            Bundle bundle) {
                        Log.d("ChatSessionUtil", "subscribed");
                        Log.w("", "GCMTesting subscribeToPushNotifications onSuccess ");
                    }

                    @Override
                    public void onError(List<String> strings) {
                        Log.w("ChatSessionUtil", "subscribeToPushNotifications onError: " + strings);
                        Log.w("", "GCMTesting subscribeToPushNotifications onError " + strings);
                    }
                });
    }


}
