package co.real.productionreal2.service.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by kelvinsun on 1/6/16.
 */
public class RePhonebookItem {
    @SerializedName("name")
    public String name;
    @SerializedName("countryCode")
    public String countryCode;
    @SerializedName("phoneNo")
    public String phoneNo;

    public enum STATUS{CHECKED,FLAGGED,NONE};
    public STATUS status=RePhonebookItem.STATUS.NONE;

    public boolean hasSent;

    public RePhonebookItem(String name, String countryCode, String phoneNo) {
        this.name = name;
        this.countryCode = countryCode;
        this.phoneNo = phoneNo;
    }

    public STATUS getStatus() {
        return status;
    }

    public void setStatus(STATUS status) {
        this.status = status;
    }

    public boolean isHasSent() {
        return hasSent;
    }

    public void setHasSent(boolean hasSent) {
        this.hasSent = hasSent;
    }

}

