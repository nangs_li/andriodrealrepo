package co.real.productionreal2.util;

import android.content.Context;
import android.os.Environment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;

import co.real.productionreal2.daomanager.ConfigDataBaseManager;
import co.real.productionreal2.daomanager.DatabaseManager;

/**
 * Created by hohojo on 30/7/2015.
 */
public class DBUtil {
    public static void copyConFigDB(Context context){
        try {
            File sd = Environment.getExternalStorageDirectory();
//            File data = Environment.getDataDirectory();

            if (sd.canWrite()) {
                String currentDBPath = ConfigDataBaseManager.getInstance(context).getDbPath();
                String backupDBPath = "configData.db";
                File currentDB = new File(currentDBPath);
                File backupDB = new File(sd, backupDBPath);

                if (currentDB.exists()) {
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                }
            }
        } catch (Exception e) {
e.printStackTrace();
        }
    }


    public static void copyDB(Context context) {
        try {
            File sd = Environment.getExternalStorageDirectory();

            if (sd.canWrite()) {
                String currentDBPath = DatabaseManager.getInstance(context).getDbPath();
                String backupDBPath = "backupReal.db";
                File currentDB = new File(currentDBPath);
                File backupDB = new File(sd, backupDBPath);

                if (currentDB.exists()) {
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
