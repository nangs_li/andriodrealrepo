package co.real.productionreal2.service.model.response;

import java.util.List;

import com.google.gson.annotations.SerializedName;
import co.real.productionreal2.service.model.AgentProfiles;

public class ResponseAgentProfiles extends ResponseBase{

	
	@SerializedName("Content")
    public Content content; 

    public class Content {
    	
    	@SerializedName("AgentProfiles")
        public List<AgentProfiles> profiles;
    	
    	
    }
	
    
    
	
}
