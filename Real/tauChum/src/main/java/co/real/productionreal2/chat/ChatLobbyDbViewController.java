package co.real.productionreal2.chat;

import android.content.Context;
import android.util.Log;

import com.quickblox.chat.model.QBDialog;

import java.util.ArrayList;
import java.util.List;

import co.real.productionreal2.callback.ChatLobbyDbViewUpdateListener;
import co.real.productionreal2.daomanager.DatabaseManager;
import co.real.productionreal2.fragment.ChatLobbyFragment;
import co.real.productionreal2.service.model.AgentProfiles;
import co.real.productionreal2.service.model.ChatRoom;
import co.real.productionreal2.service.model.ChatRoomMember;
import co.real.productionreal2.service.model.response.ResponseChatRoomGet;

/**
 * Created by hohojo on 12/8/2015.
 */
public class ChatLobbyDbViewController {
    private static final String TAG = "ChatLobbyDbViewController";
    private ChatLobbyDbViewUpdateListener chatLobbyDbViewUpdateListener;
    private static ChatLobbyDbViewController instance;
    private DatabaseManager databaseManager;
    private Context context;

    public ChatLobbyDbViewController(Context context) {
        this.context = context;
        databaseManager = DatabaseManager.getInstance(context);
        chatLobbyDbViewUpdateListener = ChatLobbyFragment.chatLobbyFragment;

    }

    public static ChatLobbyDbViewController getInstance(Context context) {
        if (instance == null) {
            initChatLobbyDbViewController(context);
        }
        return instance;
    }

    public static void initChatLobbyDbViewController(Context context) {
            instance = new ChatLobbyDbViewController(context);
    }

    public static void clear(){
        instance = null;
    }

    public void reloadChatLobby(String dialogId) {
        Log.v("","edwin ChatLobbyDbViewController reloadChatLobby dialogId: "+dialogId);
        if(chatLobbyDbViewUpdateListener!=null) {
            chatLobbyDbViewUpdateListener.reloadDialogDbAndView(dialogId);
        }
    }

    public void updateDialogDb(ArrayList<QBDialog> dialogs, List<AgentProfiles> userProfileList, String currentQbId, ResponseChatRoomGet responseChatRoomGet) {
//        databaseManager.deleteAllChatroomAgentProfiles();
//        databaseManager.deleteAllQBUser();
//        databaseManager.deleteAllQBChatDialog();

        for (int i = 0; i < dialogs.size(); i++) {
            boolean isAgent = false;
            if (userProfileList.get(i) != null &&
                    userProfileList.get(i).agentListing != null) {
                isAgent = true;
            }


            ChatRoomMember userChatRoomMem = null;
            ChatRoomMember opponentChatRoomMem = null;
            for (ChatRoom chatRoom: responseChatRoomGet.content.chatRoom) {
                if (chatRoom.dialogId.equalsIgnoreCase(dialogs.get(i).getDialogId())) {
                    for (ChatRoomMember chatRoomMem: chatRoom.chatRoomMemList) {
                        if (chatRoomMem.qbId.equalsIgnoreCase(currentQbId) ) {
                            userChatRoomMem = chatRoomMem;
                        } else {
                            opponentChatRoomMem = chatRoomMem;
                        }
                    }

                    break;
                }
            }
            databaseManager.dbActionForChatLobby(dialogs.get(i), isAgent, userProfileList.get(i), currentQbId, userChatRoomMem, opponentChatRoomMem);
        }

    }


}
