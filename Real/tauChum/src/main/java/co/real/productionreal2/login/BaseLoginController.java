package co.real.productionreal2.login;

import android.content.Context;

import co.real.productionreal2.callback.LoginDbViewListener;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;

/**
 * Created by hohojo on 25/8/2015.
 */
public class BaseLoginController {
    private LoginDbViewListener loginDbViewListener;
    public Context context;
    private static BaseLoginController instance;

    public BaseLoginController(Context context) {
        this.context = context;
        loginDbViewListener = (LoginDbViewListener) context;

    }

    public static BaseLoginController getInstance(Context context) {
        if (instance == null) {
            instance = new BaseLoginController(context);
        }
        return instance;
    }


    public void updateLoginDbAndToMainTabActivity(ResponseLoginSocial.Content content) {
        loginDbViewListener.updateLoginDbAndToMainTabActivity(content);
    }

    public void loginFail(String failMsg) {
        loginDbViewListener.loginFail(failMsg);
    }

    public void loginCancel() {
        loginDbViewListener.loginCancel();
    }
}

