package co.real.productionreal2.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import co.real.productionreal2.R;

/**
 * Created by kelvinsun on 22/12/15.
 */
public class RealTwoSidesButtonView extends LinearLayout {


    private TextView tvValue;
    private ImageView btnMinus;
    private ImageView btnAdd;
    private int value=0;
    private boolean isMinValueZero=false;

    public RealTwoSidesButtonView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RealTwoSidesButtonView(Context context) {
        super(context);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        // find views
        tvValue=(TextView)findViewById(R.id.tv_value);
        btnMinus=(ImageView) findViewById(R.id.btn_minus);
        btnAdd =(ImageView) findViewById(R.id.btn_add);

        addListener();
        updateView();
    }

    public void setMinValueZero(boolean isMinValueZero) {
        this.isMinValueZero=isMinValueZero;
        tvValue.setText("0");
    }


    private void addListener() {
        btnMinus.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //no negative number
                if (value<1)
                    return;

                value--;
                updateView();
            }
        });

        btnAdd.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                value++;
                updateView();
            }
        });
    }

    private void updateView() {
        //to disable the minus for ZERO case
//        btnMinus.setEnabled(value != 0);
        if (value==0 && !isMinValueZero) {
            tvValue.setText(getContext().getString(R.string.common__any));
        }else {
            tvValue.setText(String.valueOf(value));
        }

    }

    public ImageView getBtnMinus() {
        return btnMinus;
    }

    public int getCount() {
        return value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int count){
        tvValue.setText(String.valueOf(count));
        value=count;
        updateView();
    }

    public void reset(){
        value=0;
        updateView();
    }

}
