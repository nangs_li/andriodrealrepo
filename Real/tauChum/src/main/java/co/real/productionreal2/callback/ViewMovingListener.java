package co.real.productionreal2.callback;

import android.graphics.Bitmap;

public interface ViewMovingListener {
	public void viewMoving(Bitmap captureBitmap);
	public void viewMovingEnd();
}
