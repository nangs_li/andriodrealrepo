package co.real.productionreal2.service.model.request;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.model.InputRequest;
import co.real.productionreal2.service.OperationService;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.response.ResponseChatRoomGet;
import co.real.productionreal2.util.NetworkUtil;
import co.real.productionreal2.view.Dialog;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by hohojo on 18/11/2015.
 */
public class RequestNewsFeed extends RequestBase {
    public RequestNewsFeed(int memberID, String accessToken, String uniqueKey, int languageIndex, int page) {
        super(memberID, accessToken);
        this.languageIndex = languageIndex;
        this.page = page;

    }

    @SerializedName("LanguageIndex")
    public int languageIndex;
    @SerializedName("Page")
    public int page;

    public void callNewsFeedApi(final Context context, final ApiCallback callback) {
        String json = new Gson().toJson(this);
        Log.d("callNewsFeedApi", "callNewsFeedApi = " + json);
        final OperationService weatherService = new OperationService(context);
        weatherService.getCoffeeService().newsFeed(new InputRequest(json), new Callback<ApiResponse>() {
            @Override
            public void success(ApiResponse apiResponse, Response response) {
                ResponseChatRoomGet responseGson = new Gson().fromJson(apiResponse.jsonContent, ResponseChatRoomGet.class);
                if (responseGson.errorCode == 4) {
                    if (!((Activity) context).isFinishing()) {
                        Dialog.accessErrorForceLogoutDialog(context).show();
                    }
                } else if (responseGson.errorCode == 26) {
                    if (!((Activity) context).isFinishing()) {
                        Dialog.suspendedErrorForceLogoutDialog(context).show();
                    }
                } else if (responseGson.errorCode != 0 && responseGson.errorCode != 20 && responseGson.errorCode != 21 &&
                        responseGson.errorCode != 22 && responseGson.errorCode != 23) {
                    callback.failure(responseGson.errorMsg);
                } else {
                    callback.success(apiResponse);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d("callNewsFeedApi", "callNewsFeedApi error " + error.toString());
                callback.failure("");
                NetworkUtil.showNoNetworkMsg(context);
            }
        });
    }

}
