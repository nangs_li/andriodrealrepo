package co.real.productionreal2.service.model.request;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import co.real.productionreal2.R;
import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.model.InputRequest;
import co.real.productionreal2.service.RealNetworkService;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.response.ResponseBase;
import co.real.productionreal2.util.AppUtil;
import co.real.productionreal2.util.NetworkUtil;
import co.real.productionreal2.view.Dialog;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by hohojo on 15/12/2015.
 */
public class RequestRealNetworkSendResetPwEmail {
    public RequestRealNetworkSendResetPwEmail(String email) {
        this.email = email;
    }

    @SerializedName("Email")
    public String email;
    @SerializedName("Lang")
    public String lang;

    public void callRealNetworkSendResetPasswordEmail(final Context context, final ApiCallback callback) {
        this.lang = AppUtil.getCurrentLangISO2Code(context);
        String json = new Gson().toJson(this);
        final RealNetworkService apiService = new RealNetworkService(context);
        apiService.getCoffeeService().resetPwEmail(new InputRequest(json), new Callback<ApiResponse>() {
            @Override
            public void success(ApiResponse apiResponse, Response response) {
                ResponseBase responseGson = new Gson().fromJson(apiResponse.jsonContent, ResponseBase.class);
                if (responseGson.errorCode == 0) {
                    callback.success(apiResponse);
                } else if (responseGson.errorCode == 4) {
                    callback.failure(responseGson.errorMsg);
                } else if (responseGson.errorCode == 26) {
                    Dialog.suspendedErrorForceLogoutDialog(context).show();
                } else if (responseGson.errorCode == 102) {
                    callback.failure(context.getString(R.string.reset_password__email_not_found));
                } else if (responseGson.errorCode != 0 && responseGson.errorCode != 20 && responseGson.errorCode != 21 &&
                        responseGson.errorCode != 22 && responseGson.errorCode != 23) {
                    callback.failure(responseGson.errorMsg);
                } else {
                    callback.failure(context.getString(R.string.reset_password__email_not_found));
                }
            }

            @Override
            public void failure(RetrofitError error) {
                callback.failure("");
                NetworkUtil.showNoNetworkMsg(context);
            }
        });
    }
}
