package co.real.productionreal2.service.model.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by hohojo on 18/11/2015.
 */
public class RequestIsChatRoomMuted extends RequestBase{

    public RequestIsChatRoomMuted(int memId, String session) {
        super(memId, session);
    }
    @SerializedName("DialogID")
    public String dialogId;

}
