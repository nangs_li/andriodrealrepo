package co.real.productionreal2.activity.setting;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import co.real.productionreal2.Navigation;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.BaseActivity;
import co.real.productionreal2.util.AppUtil;
import co.real.productionreal2.util.StringUtils;

/**
 * Created by alexhung on 15/4/16.
 */
public class SettingAboutActivity extends BaseActivity {
    @InjectView(R.id.headerLayout)
    View headerLayout;
    @InjectView(R.id.versionTextView)
    TextView versionTextView;
    @InjectView(R.id.termLayout)
    View termLayout;
    @InjectView(R.id.privacyLayout)
    View privacyLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_about);
        ButterKnife.inject(this);
        initView();
    }

    private void initView() {
        TextView titleTextView = (TextView) headerLayout.findViewById(R.id.titleTextView);
        titleTextView.setText(StringUtils.getById(this,R.string.setting__about,"About"));
        String version = AppUtil.getAppVersion(this) +"\n"+AppUtil.getBuildVersion(this);
        versionTextView.setText(version);

        TextView termTitleTextView = (TextView)termLayout.findViewById(R.id.titleTextView);
        TextView privacyTitleTextView = (TextView) privacyLayout.findViewById(R.id.titleTextView);

        termTitleTextView.setText(StringUtils.getById(this,R.string.setting__terms_of_service,"Terms of service"));
        privacyTitleTextView.setText(StringUtils.getById(this,R.string.setting__privacy_policy,"Privacy policy"));
        privacyTitleTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingAboutActivity.this, PrivacySettingActivity.class);
                Navigation.pushIntent(SettingAboutActivity.this, intent);
            }
        });
        termTitleTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingAboutActivity.this, TNCSettingActivity.class);
                Navigation.pushIntent(SettingAboutActivity.this, intent);
            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
