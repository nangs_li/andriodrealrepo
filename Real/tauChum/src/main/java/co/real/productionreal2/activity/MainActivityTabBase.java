package co.real.productionreal2.activity;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.Gson;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBPrivateChat;
import com.quickblox.chat.model.QBChatMessage;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import co.real.productionreal2.BuildConfig;
import co.real.productionreal2.Constants;
import co.real.productionreal2.CoreData;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.chat.ChatActivity;
import co.real.productionreal2.activity.viral.ConnectAgentActivity;
import co.real.productionreal2.adapter.SearchResultAdapter;
import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.callback.ChatMenuListener;
import co.real.productionreal2.callback.FragmentViewChangeListener;
import co.real.productionreal2.callback.MainChatViewListener;
import co.real.productionreal2.chat.BaseChatFragmentActivity;
import co.real.productionreal2.chat.ChatMainDbViewController;
import co.real.productionreal2.chat.core.PrivateChatImpl;
import co.real.productionreal2.chat.pushnotifications.Consts;
import co.real.productionreal2.chat.pushnotifications.PlayServicesHelper;
import co.real.productionreal2.common.IntervalTimerHelper;
import co.real.productionreal2.common.LocalStorageHelper;
import co.real.productionreal2.contianer.ChatTabContainer;
import co.real.productionreal2.contianer.MeTabContainer;
import co.real.productionreal2.contianer.NewsFeedContainer;
import co.real.productionreal2.contianer.SearchTabContainer;
import co.real.productionreal2.contianer.SettingTabContainer;
import co.real.productionreal2.dao.account.DBChatroomAgentProfiles;
import co.real.productionreal2.dao.account.DBLogInfo;
import co.real.productionreal2.dao.account.DBQBChatDialog;
import co.real.productionreal2.daomanager.DatabaseManager;
import co.real.productionreal2.fragment.TutorialDialogFragment;
import co.real.productionreal2.service.model.AgentProfiles;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.request.RequestAddressSearch;
import co.real.productionreal2.service.model.request.RequestAgentProfile;
import co.real.productionreal2.service.model.response.ResponseAgentProfile;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;
import co.real.productionreal2.setting.SearchFilterSetting;
import co.real.productionreal2.util.APIUtil;
import co.real.productionreal2.util.AppUtil;
import co.real.productionreal2.util.DBUtil;
import co.real.productionreal2.util.DisplayUtil;
import co.real.productionreal2.util.FileUtil;
import co.real.productionreal2.util.ImageUtil;
import co.real.productionreal2.util.PermissionUtil;
import co.real.productionreal2.view.Dialog;
import co.real.productionreal2.view.TabMenu;
import co.real.productionreal2.view.TabMenu.TabMenuListener;
import co.real.productionreal2.view.TitleBar;
import widget.EnbleableSwipeViewPager;

public class MainActivityTabBase extends BaseChatFragmentActivity implements
        FragmentViewChangeListener, MainChatViewListener, TabMenuListener {

    @InjectView(R.id.tabhost)
    public TabMenu tabMenu;

    @InjectView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    //    @InjectView(R.id.swipeForChatLobbyImageView)
//    ImageView swipeForChatLobbyImageView;
//    @InjectView(R.id.contentFrameLayout)
//    FrameLayout contentFrameLayout;
//    @InjectView(R.id.titleRelativeLayout)
//    RelativeLayout titleRelativeLayout;
//    @InjectView(R.id.titleBarLinearLayout)
//    LinearLayout titleBarLinearLayout;
    @InjectView(R.id.title_bar)
    TitleBar mTitleBar;
    @InjectView(R.id.fl_search_result)
    LinearLayout flSearchResult;
    @InjectView(R.id.lv_search_result)
    ListView lvSearchResult;
    @InjectView(R.id.search_overlay)
    ImageView searchOverlay;


    @InjectView(R.id.viewpager)
    EnbleableSwipeViewPager mainViewPager;
    private PlayServicesHelper playServicesHelper;

    // private TomNavigationDrawerFragmentStart mStartNavigationDrawerFragment;
    // private TomNavigationDrawerFragmentEnd mEndDrawerFragment;
//    private String photoUrl, qbid;

    private static final String TAG = "MainActivityTabBase";
    private static final String SEARCH_TAB_TAG = "search_tab";
    private static final String NEWS_TAB_TAG = "news_tab";
    private static final String ME_TAB_TAG = "me_tab";
    private static final String SETTING_TAB_TAG = "setting_tab";
    private static final String CHAT_TAB_TAG = "chat_tab";

    private float lastTranslate = 0.0f;

    public static Context context;
    public static boolean activityIsRunning = false;
    public static int tabHeight;
    public static int titleHeight;

    private ResponseLoginSocial.Content userContent;

    private TutorialDialogFragment tutorialDialogFragment = null;

    public ArrayList<Object> placesList = new ArrayList<>();
    public SearchResultAdapter searchResultAdapter;

    public enum CURRENT_SECTION {NEWSFEED, ADDRESS_SEARCH, CHAT, ME, SETTING}

    ;
    public CURRENT_SECTION currentSection = CURRENT_SECTION.NEWSFEED;

    public TabOnClickListener tabOnClickListener;

    public ChatlobbyUpdateListener chatlobbyUpdateListener;

    public SearchResultUpdateListener searchResultUpdateListener;
    public boolean isDefaultSearch = false;

    public MiniViewUpdateListener miniViewUpdateListener;
    boolean isMiniview = false;
    boolean isInit = true;

    private IntervalTimerHelper intervalTimerHelper;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    Handler handler = new Handler();

    public interface ChatlobbyUpdateListener {
        public void refreshLobby();
    }

    public interface SearchResultUpdateListener {
        public void fetchResult(String currentPlaceName);

        public void updateNewsfeed();

    }

    public interface MiniViewUpdateListener {
        public void updateMiniView(boolean isMiniview);

        public void addNewStoryTag();

        public void updateNewsfeed(boolean isCallApi);
    }

    public static interface TabOnClickListener {
        public void singleTap();

        public void doubleTap();
    }

    @Override
    protected void onStart() {
        super.onStart();
        activityIsRunning = true;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_tab);
        playServicesHelper = PlayServicesHelper.getInstance(this);
        APIUtil.clear();
        DatabaseManager.initDatabaseManager(this);
        context = this;
        FileUtil.initSystemSetting(this);
        ChatMainDbViewController.getInstance(this).updateMainChatViewListener(this);

        ButterKnife.inject(this);
        initView();
        addListener();
        if (DatabaseManager.getInstance(this).getLogInfo() == null)
            return;
        userContent = CoreData.getUserContent(context);
        if (userContent == null)
            return;
        AppUtil.setupPropertyTypeHashMap(this, userContent.systemSetting.propertyTypeList);
        if (userContent.memeberType == Constants.MEM_TYPE_CREATED_POST_AGENT) {
            callAgentProfileGetApi();
        }
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
        final ViewTreeObserver vto = mainViewPager.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                if (isInit) {

                    Log.d("", "edwin SELECT_CHAT xx2");
                    onTabClicked(tabMenu.getNewsTabIcon(), false);
                    Log.d("", "edwin SELECT_CHAT xx3");
                    tabMenu.getNewsTabIcon().setSelected(true);
//                    openTab(Constants.SELECT_AGENT_SEARCH);
                    isInit = false;
                    checkIntentOpenTab(getIntent());
                }
            }
        });

        if (BuildConfig.FLAVOR.equals("prod")) {
            logUser();
        }


        if (CoreData.isNewUser ) {
            Handler handler=new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    showFindYourAgentDialog();
                }
            },2000);

        }


    }

    private void logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        Crashlytics.setUserIdentifier(String.valueOf(userContent.memId));
        Crashlytics.setUserName(userContent.memName);
    }


    private void callAgentProfileGetApi() {

        RequestAgentProfile request = new RequestAgentProfile(
                userContent.memId,
                LocalStorageHelper.getAccessToken(getBaseContext()), 10);
        request.getAgentProfile(this, request, new ApiCallback() {
            @Override
            public void success(ApiResponse apiResponse) {
                Gson gson = new Gson();
                ResponseAgentProfile agentProfileResponse = gson.fromJson(apiResponse.jsonContent, ResponseAgentProfile.class);
                AgentProfiles agentProfile = agentProfileResponse.content.profile;

                DatabaseManager databaseManager = DatabaseManager.getInstance(MainActivityTabBase.this);
                DBLogInfo dbLogInfo = databaseManager.getLogInfo();

                ResponseLoginSocial.Content userContent = new Gson().fromJson(dbLogInfo.getLogInfo(),
                        ResponseLoginSocial.Content.class);
                userContent.agentProfile = agentProfile;
                CoreData.setUserContent(userContent);
                dbLogInfo.setLogInfo(gson.toJson(userContent));

                databaseManager.updateLogInfo(dbLogInfo);

            }

            @Override
            public void failure(String errorMsg) {
            }
        });
    }

    public void clearTabSelected() {
        tabMenu.getSearchTabIcon().setSelected(false);
        tabMenu.getNewsTabIcon().setSelected(false);
        tabMenu.getChatTabIcon().setSelected(false);
        tabMenu.getMeTabIcon().setSelected(false);
        tabMenu.getSettingTabIcon().setSelected(false);
    }

    public void openTab(final int index) {
        if (index != -1) {
            handler.post(new Runnable() {
                public void run() {
                    clearTabSelected();

                    if (index == Constants.SELECT_AGENT_SEARCH) {
                        onTabClicked(tabMenu.getSearchTabIcon(), false);
                        tabMenu.getSearchTabIcon().setSelected(true);

                    } else if (index == Constants.SELECT_NEWS) {
                        onTabClicked(tabMenu.getNewsTabIcon(), false);
                        tabMenu.getNewsTabIcon().setSelected(true);

                    } else if (index == Constants.SELECT_CHAT) {
                        Log.i("", "edwin SELECT_CHAT 1");
                        onTabClicked(tabMenu.getChatTabIcon(), false);
                        Log.i("", "edwin SELECT_CHAT 2");
                        tabMenu.getChatTabIcon().setSelected(true);
                        Log.i("", "edwin SELECT_CHAT 3");

                    } else if (index == Constants.SELECT_PROFILE) {
                        onTabClicked(tabMenu.getMeTabIcon(), false);
                        tabMenu.getMeTabIcon().setSelected(true);

                    } else if (index == Constants.SELECT_SETTING) {
                        onTabClicked(tabMenu.getSettingTabIcon(), false);
                        tabMenu.getSettingTabIcon().setSelected(true);

                    }
                }
            });

        }
    }

    public void checkIntentOpenTab(Intent intent) {
        int selectTab = intent.getIntExtra(Constants.EXTRA_SELECT_TAB, -1);
        if (selectTab != -1) {
            openTab(selectTab);
            intent.removeExtra(Constants.EXTRA_SELECT_TAB);
            String str_EXTRA_USER_ID = intent.getStringExtra(ChatActivity.EXTRA_USER_ID);
            intent.removeExtra(ChatActivity.EXTRA_USER_ID);
            if (str_EXTRA_USER_ID != null && !str_EXTRA_USER_ID.isEmpty()) {
                DBQBChatDialog existDbQbChatDialog = DatabaseManager.getInstance(this).getQBChatDialogByQbId(Integer.parseInt(str_EXTRA_USER_ID));
                Bundle bundle = new Bundle();
                if (existDbQbChatDialog != null) {
                    bundle.putString(ChatActivity.EXTRA_DIALOG_ID, existDbQbChatDialog.getDialogID());
                }
                DBChatroomAgentProfiles mDBChatroomAgentProfiles = DatabaseManager.getInstance(this).getChatroomAgentProfilesByUserId(Long.parseLong(str_EXTRA_USER_ID));
                bundle.putString(ChatActivity.EXTRA_USER_ID, str_EXTRA_USER_ID);
                if (mDBChatroomAgentProfiles != null) {
                    int memberId = mDBChatroomAgentProfiles.getMemberID();
                    bundle.putInt(ChatActivity.EXTRA_MEM_ID, memberId);
                }

                ChatActivity.start(this, bundle);
            }

        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent.getBooleanExtra("isNeedRestart", false)) {
            Intent newIntent = new Intent(this, MainActivityTabBase.class);
            newIntent.putExtra(Constants.EXTRA_SELECT_TAB, Constants.SELECT_SETTING);
            newIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
            return;
        }
        checkIntentOpenTab(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        playServicesHelper.checkPlayServices();
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter(Consts.ME_NEW_COUNT_EVENT));
        LocalBroadcastManager.getInstance(this).registerReceiver(mNewsfeedAlertReceiver, new IntentFilter(Consts.NEWSFEED_NEW_COUNT_EVENT));

        updateTabView();


        if (!QBChatService.getInstance().isLoggedIn()) {
            //check if permission granted
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.READ_PHONE_STATE)
                    != PackageManager.PERMISSION_GRANTED) {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_PHONE_STATE},
                        PermissionUtil.MY_PERMISSIONS_REQUEST_READ_PHONE_STATUS);

                // MY_PERMISSIONS_REQUEST_READ_PHONE_STATUS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
                return;
            } else {
                ChatMainDbViewController.getInstance(this).callQBUserSignUpOrLogin();
            }
        } else {
            PrivateChatImpl.getInstance(this);
        }


//        if (!AppUtil.isAppIsInBackground(MainActivityTabBase.this) && userContent!= null) {
        if (AppUtil.isAppOnForeground(this) && userContent != null) {
            Log.d(TAG, "userContent: session: " + LocalStorageHelper.getAccessToken(getBaseContext()));

            intervalTimerHelper = new IntervalTimerHelper(this);//2000s
            intervalTimerHelper.Count();
        }

    }

    private void addListener() {

        //init tabMenu listener
        tabMenu.setListener(this);


    }


    @Override
    protected void onStop() {
        super.onStop();
        if (client != null) {
            client.disconnect();
        }
    }

    //Must unregister onPause()
    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNewsfeedAlertReceiver);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
//        ChatSessionUtil.setPrivateChatManager(null);
    }


    NewsFeedContainer mNewsFeedContainer;
    SearchTabContainer mSearchTabContainer;
    ChatTabContainer mChatTabContainer;
    public MeTabContainer mMeTabContainer;
    SettingTabContainer mSettingTabContainer;

    MyPageAdapter pageAdapter;

    private List<Fragment> getFragments() {
        List<Fragment> fList = new ArrayList<Fragment>();
        mSearchTabContainer = new SearchTabContainer();
        mNewsFeedContainer = new NewsFeedContainer();
        mChatTabContainer = new ChatTabContainer();
        mMeTabContainer = new MeTabContainer();
        mSettingTabContainer = new SettingTabContainer();

        fList.add(mSearchTabContainer);
        fList.add(mNewsFeedContainer);
        fList.add(mChatTabContainer);
        fList.add(mMeTabContainer);
        fList.add(mSettingTabContainer);
        return fList;
    }

    class MyPageAdapter extends FragmentPagerAdapter {
        private List<Fragment> fragments;

        public MyPageAdapter(FragmentManager fm, List<Fragment> fragments) {
            super(fm);
            this.fragments = fragments;
        }

        @Override
        public Fragment getItem(int position) {
            return this.fragments.get(position);
        }

        @Override
        public int getCount() {
            return this.fragments.size();
        }
    }

    private void initMenuTabHostView() {

        List<Fragment> fragments = getFragments();

        pageAdapter = new MyPageAdapter(this.getSupportFragmentManager(), fragments);
        mainViewPager.setAdapter(pageAdapter);
        mainViewPager.setOffscreenPageLimit(4);

    }

    boolean tabClickEnable = true;

    public void setTabClickEnable(boolean tabClickEnable) {
        this.tabClickEnable = tabClickEnable;
        Log.w(TAG, "tabClickEnable = " + tabClickEnable);

        tabMenu.getSearchTabIcon().setEnabled(tabClickEnable);
        tabMenu.getNewsTabIcon().setEnabled(tabClickEnable);
        tabMenu.getChatTabIcon().setEnabled(tabClickEnable);
        tabMenu.getMeTabIcon().setEnabled(tabClickEnable);
        tabMenu.getSettingTabIcon().setEnabled(tabClickEnable);
    }

    @Override
    public void onTabClicked(final View v, final boolean isDoubleClick) {
        if (!tabClickEnable) {
            return;
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!isDoubleClick) {
                    //normal change tab action
                    if (v == tabMenu.getSearchTabIcon()) {
                        currentSection = CURRENT_SECTION.ADDRESS_SEARCH;
                        changeTabSection(mSearchTabContainer, 0);
                        mSearchTabContainer.fragmentBecameVisible();
                        searchResultUpdateListener.updateNewsfeed();
                    } else if (v == tabMenu.getNewsTabIcon()) {
                        currentSection = CURRENT_SECTION.NEWSFEED;
                        changeTabSection(mNewsFeedContainer, 1);
                        mNewsFeedContainer.fragmentBecameVisible();
                        refreshNewsfeed(false);
                    } else if (v == tabMenu.getChatTabIcon()) {
                        changeTabSection(mChatTabContainer, 2);
                        mChatTabContainer.fragmentBecameVisible();
                        if (chatlobbyUpdateListener != null) {
                            chatlobbyUpdateListener.refreshLobby();
                        }
                    } else if (v == tabMenu.getMeTabIcon()) {
                        changeTabSection(mMeTabContainer, 3);
                        mMeTabContainer.fragmentBecameVisible();
                        if (tabOnClickListener != null)
                            tabOnClickListener.singleTap();
                    } else if (v == tabMenu.getSettingTabIcon()) {
                        changeTabSection(mSettingTabContainer, 4);
                        mSettingTabContainer.fragmentBecameVisible();
                    }
                } else {
                    //trigger double tap action
                    if (v == tabMenu.getSearchTabIcon()) {
                        //nothing
                        currentSection = CURRENT_SECTION.ADDRESS_SEARCH;
                        changeTabSection(mSearchTabContainer, 0);
                        mSearchTabContainer.fragmentBecameVisible();
                    } else if (v == tabMenu.getNewsTabIcon()) {
                        //refresh list
                        currentSection = CURRENT_SECTION.NEWSFEED;
                        changeTabSection(mNewsFeedContainer, 1);
                        mNewsFeedContainer.fragmentBecameVisible();
                        refreshNewsfeed(true);
                    } else if (v == tabMenu.getChatTabIcon()) {
                        //nothing
                        changeTabSection(mChatTabContainer, 2);
                        mChatTabContainer.fragmentBecameVisible();
                    } else if (v == tabMenu.getMeTabIcon()) {
                        //refresh list
                        changeTabSection(mMeTabContainer, 3);
                        mMeTabContainer.fragmentBecameVisible();
                        if (tabOnClickListener != null)
                            tabOnClickListener.doubleTap();
                    } else if (v == tabMenu.getSettingTabIcon()) {
                        //nothing
                        changeTabSection(mSettingTabContainer, 4);
                        mSettingTabContainer.fragmentBecameVisible();
                    }
                }

            }
        });
    }

    private void changeTabSection(Fragment fragment, int positionIndex) {
        changeTabTitle(fragment);
        mainViewPager.setCurrentItem(positionIndex, false);
    }

    private void initView() {

        initMenuTabHostView();
        tabHeight = ImageUtil.getViewHeight(tabMenu);

        titleHeight = ImageUtil.dpToPx(this, 60);
        // http://stackoverflow.com/questions/18210486/how-can-i-open-drawer-layout-moving-all-the-view-and-not-just-slide-over
        drawerLayout
                .setDrawerListener(new DrawerLayout.DrawerListener() {

                    @Override
                    public void onDrawerClosed(View drawerView) {
                    }

                    @Override
                    public void onDrawerOpened(View drawerView) {

                    }

                    @Override
                    public void onDrawerSlide(View drawerView, float slideOffset) {
                    }

                    @Override
                    public void onDrawerStateChanged(int newState) {

                    }

                });
    }

    @Override
    public void onBackPressed() {
        if (flSearchResult.isShown()) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(MainActivityTabBase.this.getCurrentFocus().getWindowToken(), 0);
            ImageUtil.collapse(flSearchResult);
            mTitleBar.getEtSearchField().clearFocus();
            //set current address search name
            int resultCount = LocalStorageHelper.defaultHelper(MainActivityTabBase.this).getCurrentSearchCount();
            mTitleBar.setResultCount(resultCount);
            RequestAddressSearch requestAddressSearch = LocalStorageHelper.defaultHelper(MainActivityTabBase.this).getCurrentAddressSearch(MainActivityTabBase.this);
            if (requestAddressSearch != null) {
                String searchText = requestAddressSearch.inputAddress;
                mTitleBar.setSearchFieldText(searchText);
            }

        } else {
            Dialog.quitAppDialog(this).show();
        }
    }

//    public String getPhotoUrl() {
//        return photoUrl;
//    }
//
//    public void setPhotoUrl(String photoUrl) {
//        this.photoUrl = photoUrl;
//    }

    public DrawerLayout getDrawerLayout() {
        return drawerLayout;
    }

    @Override
    public void viewMoving(Bitmap captureBitmap) {
        Log.w(TAG, "viewMoving captureBitmap = " + captureBitmap);
//        swipeForChatLobbyImageView.setImageBitmap(captureBitmap);
        // realtabcontent.setBackground(new BitmapDrawable(getResources(),
        // captureBitmap));
    }

    @Override
    public void viewMovingEnd() {
//        swipeForChatLobbyImageView.setImageBitmap(null);
    }

    @Override
    public void hideTabBar() {
        ImageUtil.collapse(tabMenu);
    }

    @Override
    public void openTabBar() {
        ImageUtil.expand(tabMenu);
    }

    @Override
    public void changeTabTitle(final Fragment fragment) {
        Log.i("", "edwin changeTabTitle fragment " + fragment);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                titleRelativeLayout.removeAllViews();
//                flSearchResult.setVisibility(fragment instanceof SearchTabContainer?View.VISIBLE:View.GONE);
                flSearchResult.setVisibility(View.GONE);

                if (fragment instanceof NewsFeedContainer) {
                    mTitleBar.setupTitleBar(TitleBar.TAB_TYPE.NEWSFEED);

                } else if (fragment instanceof SearchTabContainer) {
                    mTitleBar.setupTitleBar(TitleBar.TAB_TYPE.SEARCH);

                    searchOverlay.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(MainActivityTabBase.this.getCurrentFocus().getWindowToken(), 0);
                            ImageUtil.collapse(flSearchResult);
                            //set total count
                            int resultCount = LocalStorageHelper.defaultHelper(MainActivityTabBase.this).getCurrentSearchCount();
                            mTitleBar.setResultCount(resultCount);

                            RequestAddressSearch requestAddressSearch = LocalStorageHelper.defaultHelper(MainActivityTabBase.this).getCurrentAddressSearch(MainActivityTabBase.this);
                            if (requestAddressSearch != null) {
                                String searchText = requestAddressSearch.inputAddress;
                                mTitleBar.setSearchFieldText(searchText);
                            }

                            View current = getCurrentFocus();
                            if (current != null) current.clearFocus();
                            return false;
                        }
                    });

                    //show soft keyboard
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(mTitleBar.getEtSearchField(), 0);

                } else if (fragment instanceof ChatTabContainer) {
                    mTitleBar.setupTitleBar(TitleBar.TAB_TYPE.CHAT);
                } else if (fragment instanceof MeTabContainer) {
                    if (userContent.agentProfile != null) {
                        mTitleBar.setupTitleBar(TitleBar.TAB_TYPE.ME_HOME_AGENT);
                    } else {
                        mTitleBar.setupTitleBar(TitleBar.TAB_TYPE.ME_HOME_USER);
                    }
                } else if (fragment instanceof SettingTabContainer) {
                    mTitleBar.setupTitleBar(TitleBar.TAB_TYPE.SETTING);
                }
            }

        });
    }

    private void createSearchingModeLayout(final Object listener, String searchingText) {
        mTitleBar.getTvTitle().setVisibility(View.GONE);
        mTitleBar.getChatSearchLinearLayout().setVisibility(View.VISIBLE);
        mTitleBar.getAgentSearchBtn().setVisibility(View.GONE);
        mTitleBar.getSearchAgentEditText().setText(searchingText);
        mTitleBar.getSearchAgentEditText().requestFocus();
        mTitleBar.getAgentSearchCancelBtn().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtil.hideKeyboard(MainActivityTabBase.this, mTitleBar.getSearchAgentEditText());
                ((ChatMenuListener) listener).filterLocalAgentSearch("");
                mTitleBar.getSearchAgentEditText().setText("");
                mTitleBar.getChatSearchLinearLayout().setVisibility(View.GONE);
                mTitleBar.getAgentSearchBtn().setVisibility(View.VISIBLE);
                mTitleBar.getTvTitle().setVisibility(View.VISIBLE);
            }
        });
        AppUtil.showKeyboard(MainActivityTabBase.this, mTitleBar.getSearchAgentEditText());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mTitleBar.getSearchAgentEditText().setShowSoftInputOnFocus(true);
        }
//        mTitleBar.getSearchAgentEditText().addTextChangedListener(new SearchWatcher("chat", listener));
    }

    public void showResultSuggestion(boolean hasFocus) {
        Log.d("showResultSuggestion", "showResultSuggestion: " + hasFocus);
        if (hasFocus) {
            //manual search
            isDefaultSearch = false;

            flSearchResult.removeAllViews();
            // Attach the adapter to a ListView
            mTitleBar.getEtSearchField().setSelectAllOnFocus(true);
            mTitleBar.getEtSearchField().selectAll();

            lvSearchResult.setAdapter(searchResultAdapter);
            flSearchResult.addView(lvSearchResult);

            ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, DisplayUtil.getDisplayHeight(context));
            searchOverlay.setImageResource(R.color.black_overlay);
            searchOverlay.setLayoutParams(lp);
            flSearchResult.addView(searchOverlay);
            searchOverlay.setOnClickListener(null);
            ImageUtil.expand(flSearchResult);
            AppUtil.showKeyboard(context, mTitleBar.getEtSearchField());
            mTitleBar.setResultCount(0);
        } else {
            AppUtil.hideKeyboard(context, mTitleBar.getEtSearchField());
            ImageUtil.collapse(flSearchResult);
        }
    }

    public void updateFilterAlert() {
        int alertCount = SearchFilterSetting.getInstance().getAlertCount();
        mTitleBar.getTvFilterAlert().setVisibility(alertCount != 0 ? View.VISIBLE : View.GONE);
        mTitleBar.getTvFilterAlert().setText(String.valueOf(alertCount));
    }

    @Override
    public void updateViewByReceivedMsg(QBPrivateChat chat, QBChatMessage chatMessage) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

            }
        });

    }

    @Override
    public void processMessageDelivered(String messageId, String dialogId, Integer userId) {
//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
////                Toast.makeText(getApplicationContext(), "You receive Delivered", Toast.LENGTH_LONG).show();
//            }
//        });
    }

    @Override
    public void processMessageRead(String messageId, String dialogId, Integer userId) {
//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
////                Toast.makeText(getApplicationContext(), "You receive Read", Toast.LENGTH_LONG).show();
//            }
//        });
    }

    @Override
    public void updateTabView() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // Chat tab bubble update
                int unreadDialogCount = DatabaseManager.getInstance(MainActivityTabBase.this).getUnreadDialogCount();
                ImageView img = (ImageView) findViewById(R.id.img_tabtxt_tab_chat);
                Log.w(TAG, "updateTabView unreadDialogCount: " + unreadDialogCount);
                Log.w(TAG, "updateTabView img: " + img);
                img.setImageResource(R.drawable.tab_indicator_chat);
                TextView noTextView = (TextView) findViewById(R.id.noTextView_tab_chat);
                if (unreadDialogCount > 0) {
                    noTextView.setVisibility(View.VISIBLE);
                    noTextView.setText("" + unreadDialogCount);
                } else {
                    noTextView.setVisibility(View.GONE);
                }

            }
        });
    }


    public void updateMeTabView(final TabMenu.TAB_SECTION section) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                // tab bubble update
                int unreadDialogCount = LocalStorageHelper.defaultHelper(MainActivityTabBase.this).getNewCount(section);
//                tabMenu.setCount(unreadDialogCount, section);


                //show new story if there is any news
                if (section == TabMenu.TAB_SECTION.NEWSFEED)
                    if (unreadDialogCount > 0) {
                        tabMenu.setCount(unreadDialogCount, section);
                        if (miniViewUpdateListener != null)
                            miniViewUpdateListener.addNewStoryTag();
                    }


            }
        });

    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.w(TAG, "resultCode = " + resultCode);
        Log.w(TAG, "requestCode = " + requestCode);

        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.SEARCH_FILTER) {
                Log.w(TAG, "requestCode = count : " + SearchFilterSetting.getInstance().getAlertCount());
                performSearch();
                updateFilterAlert();
            }else if(requestCode == Constants.INPUT_PHONE_NUMBER_REQUSTCODE){
//                mTitleBar.updateInputPhoneNumberBtn();
            }
            else {
                List<Fragment> fragments = getSupportFragmentManager().getFragments();
                if (fragments != null) {
                    for (int idx = 0; idx < fragments.size(); idx++) {
                        if (fragments.get(idx) instanceof MeTabContainer || fragments.get(idx) instanceof SettingTabContainer)
                            fragments.get(idx).onActivityResult(requestCode, resultCode, data);
                    }
                }
            }
        } else if (resultCode == RESULT_CANCELED) {
            if (requestCode == Constants.SEARCH_FILTER) {
                //do nothing
            } else {
                List<Fragment> fragments = getSupportFragmentManager().getFragments();
                if (fragments != null) {
                    for (int idx = 0; idx < fragments.size(); idx++) {
                        if (fragments.get(idx) instanceof MeTabContainer)
                            fragments.get(idx).onActivityResult(requestCode, resultCode, data);
                    }
                }
            }
        }


    }


    public void copyDbClick(View view) {
        DBUtil.copyDB(this);
    }

    public void deleteDbClick(View view) {
        DatabaseManager.getInstance(this).dropDatabase();
    }


    //for filter search click action
    public void performSearch() {
        isDefaultSearch = false;
        String currentPlaceName = "";

        updateSearchView(currentPlaceName);

        searchResultUpdateListener.fetchResult(currentPlaceName);
    }

    //for refresh newsfeed
    public void refreshNewsfeed(boolean isCallApi) {
        Log.i(TAG, "refreshNewsfeed: ");
        if (miniViewUpdateListener != null)
            miniViewUpdateListener.updateNewsfeed(isCallApi);
    }

    public void updateSearchView(String currentPlaceName) {

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        ImageUtil.collapse(flSearchResult);

        View current = getCurrentFocus();
        if (current != null) {
            imm.hideSoftInputFromWindow(current.getWindowToken(), 0);
            current.clearFocus();
        }

    }


    private void showTutorialDialog() {
        tutorialDialogFragment = new TutorialDialogFragment();
        tutorialDialogFragment.show(getFragmentManager(), "TutorialDialogFragment");
    }


    private void showFindYourAgentDialog() {
        ConnectAgentActivity.start(this, ConnectAgentActivity.ConnectAgentActivityType.NORMAL);
    }


    public boolean isTutorialDialogFragmentVisible() {
        if (tutorialDialogFragment == null)
            return false;
        return tutorialDialogFragment.isVisible();
    }

    public TitleBar getTitleBar() {
        return mTitleBar;
    }

    public TabMenu getTabHost() {
        return tabMenu;
    }


    //This is the handler that will manager to process the broadcast intent
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i(TAG, "mMessageReceiver: " + mMessageReceiver);
            //do other stuff here
            updateMeTabView(TabMenu.TAB_SECTION.ME);

        }
    };

    //This is the handler that will manager to process the broadcast intent
    private BroadcastReceiver mNewsfeedAlertReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i(TAG, "mNewsfeedAlertReceiver: " + mNewsfeedAlertReceiver);
            //do other stuff here
            updateMeTabView(TabMenu.TAB_SECTION.NEWSFEED);
        }
    };


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PermissionUtil.MY_PERMISSIONS_REQUEST_READ_PHONE_STATUS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    ChatMainDbViewController.getInstance(this).callQBUserSignUpOrLogin();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
        }
    }

}
