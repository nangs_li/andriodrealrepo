package co.real.productionreal2.activity.createpost;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.quickblox.chat.model.QBDialog;
import com.quickblox.chat.query.QueryUpdateDialog;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.request.QBRequestUpdateBuilder;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import co.real.productionreal2.Constants;
import co.real.productionreal2.CoreData;
import co.real.productionreal2.Navigation;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.BaseFragmentActivity;
import co.real.productionreal2.activity.viral.InviteToFollowActivity;
import co.real.productionreal2.adapter.CreatePostPreviewPagerAdapter;
import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.callback.DialogDualCallback;
import co.real.productionreal2.common.LocalStorageHelper;
import co.real.productionreal2.config.AppConfig;
import co.real.productionreal2.dao.account.DBLogInfo;
import co.real.productionreal2.dao.account.DBQBChatDialog;
import co.real.productionreal2.daomanager.DatabaseManager;
import co.real.productionreal2.model.AgentListing;
import co.real.productionreal2.model.SystemSetting;
import co.real.productionreal2.newsfeed.NewsFeedDetailFragment;
import co.real.productionreal2.service.AdminService;
import co.real.productionreal2.service.model.AgentProfiles;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.request.RequestAgentProfile;
import co.real.productionreal2.service.model.request.RequestListing;
import co.real.productionreal2.service.model.response.ResponseAgentProfile;
import co.real.productionreal2.service.model.response.ResponseCreateListing;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;
import co.real.productionreal2.util.AppUtil;
import co.real.productionreal2.util.FileUtil;
import co.real.productionreal2.util.ImageUtil;
import co.real.productionreal2.view.Dialog;
import widget.RoundedImageView;
import widget.slidinguppanel.SlidingUpPanelLayout;

;

/**
 * Created by hohojo on 16/10/2015.
 */
public class CreatePostPreviewActivity extends BaseFragmentActivity {
    private static final String TAG = "CreatePostPreviewAct";
    @InjectView(R.id.rootImageView)
    ImageView rootImageView;
    @InjectView(R.id.coverImageView)
    ImageView coverImageView;
    @InjectView(R.id.agentImageView)
    RoundedImageView agentImageView;
    //    @InjectView(R.id.langLinearLayout)
//    LinearLayout langLinearLayout;
    @InjectView(R.id.nameTextView)
    TextView nameTextView;
    @InjectView(R.id.followingCountTextView)
    TextView followingCountTextView;
    @InjectView(R.id.previewDetailViewPager)
    ViewPager previewDetailViewPager;
    @InjectView(R.id.slidingUpLayout)
    SlidingUpPanelLayout slidingUpLayout;
    @InjectView(R.id.deleteBtn)
    ImageButton deleteBtn;
    @InjectView(R.id.fl_preview_container)
    FrameLayout flPreviewContainer;

    private final static String TAG_LISTING_DETAILS_FRAGMENT = "TAG_LISTING_DETAILS_FRAGMENT";

    private CreatePostPreviewPagerAdapter createPostPreviewPagerAdapter;
    private NewsFeedDetailFragment newsFeedDetailFragment;
    private ProgressDialog barProgressDialog;
    private int successCount = 0;
    private ResponseLoginSocial.Content agentContent;
    private ResponseLoginSocial.Content userContent;

    private ArrayList<Integer> langIndexList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_post_preview);
        ButterKnife.inject(this);
        initView();
        initSlidingUpLayout();

        userContent = CoreData.getUserContent(this);
        //set up preview
        setupPreviewContainer();
        Log.d(TAG, "onPageSelected: init: " + newsFeedDetailFragment);


        //init pager default position as 0
        final int currentPage=previewDetailViewPager.getCurrentItem();

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                onPageChangeListener.onPageSelected(0);
                Log.d(TAG, "onActivityResult delay"+ currentPage);
            }
        }, 300);

    }


    private void setupPreviewContainer() {

        // set the preview container
        newsFeedDetailFragment = new NewsFeedDetailFragment();
        AgentProfiles agentProfiles = agentContent.agentProfile;
        //get current agent listing for preview
        AgentListing agentListing = getCurrentCreatePost().getCreatePostRequest();

        String imageName = AppUtil.getPropertyTypeImageName(agentListing.propertyType, agentListing.spaceType);
        String propertyTypeName = AppUtil.getPropertyTypeHashMap(this, userContent.systemSetting.propertyTypeList).get(agentListing.propertyType + "," + agentListing.spaceType);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fl_preview_container, newsFeedDetailFragment.newInstance("Preview", false, agentProfiles, imageName, propertyTypeName, true, 0), TAG_LISTING_DETAILS_FRAGMENT)
                .commit();
    }

    private void initSlidingUpLayout() {

        slidingUpLayout.setPanelHeight(ImageUtil.getScreenHeight(this) / 3);
        slidingUpLayout.setPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
            }

            @Override
            public void onPanelExpanded(View panel) {
                BaseCreatePost baseCreatePost = BaseCreatePost.getInstance(CreatePostPreviewActivity.this);
                if (baseCreatePost.getCreatePostRequest().reasons.size() > 1) {
                    deleteBtn.setVisibility(View.VISIBLE);
                } else {
                    deleteBtn.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPanelCollapsed(View panel) {
//                deleteBtn.setVisibility(View.GONE);
            }

            @Override
            public void onPanelAnchored(View panel) {
            }

            @Override
            public void onPanelHidden(View panel) {
            }
        });
    }

    private void initView() {
        agentContent = CoreData.getUserContent(this);
        Picasso.with(this)
                .load(agentContent.photoUrl)
                .error(R.drawable.default_profile)
                .placeholder(R.drawable.default_profile).into(agentImageView);
        coverImageView.setImageBitmap(BitmapFactory.decodeFile(getCurrentCreatePost().getPhotoPathList().get(0).url));

        nameTextView.setText(agentContent.memName);
        followingCountTextView.setText(String.valueOf(agentContent.followingCount));

        createPostPreviewPagerAdapter = new CreatePostPreviewPagerAdapter(this, getSupportFragmentManager());
        //get current reason language list
        langIndexList = getLangIndexList(getCurrentCreatePost());
        Log.d(TAG, "langIndexList: " + langIndexList.toString());
        createPostPreviewPagerAdapter.setLangIndexList(langIndexList);
        createPostPreviewPagerAdapter.setLangNameList(getLangStringList(langIndexList, agentContent));
        previewDetailViewPager.setAdapter(createPostPreviewPagerAdapter);
        ViewGroup.LayoutParams viewpagerParams = previewDetailViewPager
                .getLayoutParams();
//        viewpagerParams.height = ImageUtil.getScreenHeight(this) * 3 / 4;
        viewpagerParams.height = 150;
        previewDetailViewPager.setLayoutParams(viewpagerParams);
//        previewDetailViewPager.post(new Runnable() {
//            @Override
//            public void run() {
//                previewDetailViewPager.setCurrentItem(previewDetailViewPager.getCurrentItem());
//            }
//        });

        //enhance performance
        previewDetailViewPager.setOffscreenPageLimit(3);
        previewDetailViewPager.setOnPageChangeListener(onPageChangeListener);

        if (getCurrentCreatePost().getCreatePostRequest().reasons.size() > 1) {
            deleteBtn.setVisibility(View.VISIBLE);
        } else {
            deleteBtn.setVisibility(View.GONE);
        }

    }

    private ViewPager.OnPageChangeListener onPageChangeListener=new ViewPager.OnPageChangeListener() {
//        final ArrayList<Integer> langIndexList = getLangIndexList(baseCreatePost);
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            Log.d(TAG, "onPageSelected: selected: " + newsFeedDetailFragment);
            Log.d(TAG, "onPageSelected: " + position);
            Log.d(TAG, "onPageSelected: " + langIndexList.toString());
//                newsFeedDetailFragment.updateContent(langIndexList.get(position));
            ((NewsFeedDetailFragment) getSupportFragmentManager().findFragmentByTag(TAG_LISTING_DETAILS_FRAGMENT)).updateDetail(langIndexList.get(position));

        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    private ArrayList<Integer> getLangIndexList(BaseCreatePost baseCreatePost) {
        ArrayList<Integer> langIndexList = new ArrayList<>();
        for (RequestListing.Reason reason : baseCreatePost.getCreatePostRequest().reasons) {
            langIndexList.add(reason.languageIndex);
        }
        return langIndexList;
    }

    private ArrayList<String> getLangStringList(ArrayList<Integer> langIndexList, ResponseLoginSocial.Content userContent) {
        ArrayList<String> langStringList = new ArrayList<>();

        for (Integer langIndex : langIndexList) {
            for (SystemSetting.SystemLanguage systemLanguage : userContent.systemSetting.sysLangList) {
                if (systemLanguage.index == langIndex) {
                    langStringList.add(systemLanguage.nativeName);
                }
            }
        }
        return langStringList;
    }

    @Override
    public void onBackPressed() {
        if (slidingUpLayout != null &&
                (slidingUpLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED ||
                        slidingUpLayout.getPanelState() == SlidingUpPanelLayout.PanelState.ANCHORED)) {
            slidingUpLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        } else {
            super.onBackPressed();
            finish();
        }
    }

    @OnClick(R.id.closeBtn)
    public void closeBtnClick(View view) {
        Dialog.exitSaveChangeDialog(this, new DialogDualCallback() {
            @Override
            public void positive() {
                //reset value
                BaseCreatePost.getInstance(CreatePostPreviewActivity.this).resetBaseCreatePost();
                exitActivity();
            }

            @Override
            public void negative() {
                //save input
                exitActivity();
            }
        }).show();
    }

    private void exitActivity() {
        setResult(RESULT_OK);
        finish();
    }

    @OnClick(R.id.backBtn)
    public void backBtnClick(View view) {
        finish();
    }


    private BaseCreatePost getCurrentCreatePost(){
        return BaseCreatePost.getInstance(this);
    }

    @OnClick(R.id.deleteBtn)
    public void deleteBtnClick(View view) {
        Log.d(TAG, "onActivityResult delay" + previewDetailViewPager.getChildCount());
        Log.d(TAG, "onActivityResult delay before: " + previewDetailViewPager.getCurrentItem());
        int currentPage = previewDetailViewPager.getCurrentItem();
        if (currentPage == 0) {
//            previewDetailViewPager.setCurrentItem(1);
            previewDetailViewPager.setCurrentItem(1);
            Log.d(TAG, "onActivityResult delay after: " + previewDetailViewPager.getCurrentItem());
        } else if (currentPage == getCurrentCreatePost().getCreatePostRequest().reasons.size() - 1) {
            previewDetailViewPager.setCurrentItem(0);
        }


        //remove selected langauge to delete
        BaseCreatePost.getInstance(this).getReasonsDespImageList().remove(currentPage);
        getCurrentCreatePost().getCreatePostRequest().reasons.remove(currentPage);
        createPostPreviewPagerAdapter.removeView(currentPage);

        previewDetailViewPager.invalidate();
        createPostPreviewPagerAdapter.notifyDataSetChanged();

        //hide the delete button if only one existing language
        if (getCurrentCreatePost().getCreatePostRequest().reasons.size() > 1) {
            deleteBtn.setVisibility(View.VISIBLE);
        } else {
            deleteBtn.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.addLangBtn)
    public void addLangBtnClick(View view) {
        launchAddLangActivity();
    }

    @OnClick(R.id.publishBtn)
    public void publishBtnClick(View view) {
        // Mixpanel
        MixpanelAPI mixpanel = MixpanelAPI.getInstance(this, AppConfig.getMixpanelToken());
        mixpanel.track("Publishing Posting");

        CoreData.isAgentProfileUpdate = true;
        launchBarDialog();
    }

    private void launchAddLangActivity() {
        Intent intent = new Intent(this, CreatePostAddLangActivity.class);
        Navigation.pushIntentForResult(this,intent,Constants.CREATE_POST_ADD_LANG);
    }

    private void launchBarDialog() {
        barProgressDialog = new ProgressDialog(this);
        barProgressDialog.setTitle(getString(R.string.alert__Initializing));
        barProgressDialog.setMessage(getString(R.string.alert__uploading));
        barProgressDialog.setCancelable(false);
        barProgressDialog.setCanceledOnTouchOutside(false);
        if (!barProgressDialog.isShowing())
            barProgressDialog.show();


        BaseCreatePost baseCreatePost = BaseCreatePost.getInstance(this);
        baseCreatePost.getCreatePostRequest().callAgentListingAddAndUploadFileApi(this, new ApiCallback() {
            @Override
            public void success(ApiResponse apiResponse) {
                new UploadFileTask().execute(apiResponse.jsonContent);
            }

            @Override
            public void failure(String errorMsg) {
                AppUtil.showToast(CreatePostPreviewActivity.this, getString(R.string.error_message__please_try_again_later));
            }
        });


    }

    private class UploadFileTask extends AsyncTask<String, Boolean, Boolean> {

        @Override
        protected Boolean doInBackground(String... jsonContent) {
            ResponseCreateListing responseGson = new Gson().fromJson(jsonContent[0], ResponseCreateListing.class);
            HttpParams params = new BasicHttpParams();
            params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
            DefaultHttpClient httpClient = new DefaultHttpClient(params);

            BaseCreatePost baseCreatePost = BaseCreatePost.getInstance(CreatePostPreviewActivity.this);
            //upload post photos
            for (int i = 0; i < baseCreatePost.getPhotoPathList().size(); i++) {
                if (baseCreatePost.getPhotoPathList().get(i) != null &&
                        !baseCreatePost.getPhotoPathList().get(i).url.isEmpty()) {
                    File fileInDevice = new File(FileUtil.getAppSubFolder(Constants.UPLOAD_FILE_FOLDER_NAME),
                            "file_" + i + ".jpg");
                    try {
//                        FileUtil.convertBitmapToFile(fileInDevice, ImageUtil.decodeSampledBitmapFromResource(baseCreatePost.getPhotoPathList().get(i)));
                        ImageUtil.getCompressImageFilePath(baseCreatePost.getPhotoPathList().get(i).url, fileInDevice.getAbsolutePath(), ImageUtil.normalImageType);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    // save to DB
                    Log.w(TAG, "fileInDevice: " + fileInDevice.length() / 1000);
                    DatabaseManager databaseManager = DatabaseManager.getInstance(CreatePostPreviewActivity.this);
                    DBLogInfo dbLogInfo = databaseManager.getLogInfo();
                    publishProgress(callUploadFileApi(httpClient, Constants.UPLOAD_LISTING_PHOTO, responseGson.content.agentListingId, i, 0,
                            fileInDevice));

                }
            }
            //upload reason photos

            for (int i = 0; i < baseCreatePost.getReasonsDespImageList().size(); i++) {
                resizePhotoAndPublish(httpClient, baseCreatePost.getReasonsDespImageList().get(i).reason1ImagePath, "reason1file_" + i + ".png", 1,
                        baseCreatePost.getReasonsDespImageList().get(i).langIndex, responseGson.content.agentListingId);
                resizePhotoAndPublish(httpClient, baseCreatePost.getReasonsDespImageList().get(i).reason2ImagePath, "reason2file_" + i + ".png", 2,
                        baseCreatePost.getReasonsDespImageList().get(i).langIndex, responseGson.content.agentListingId);
                resizePhotoAndPublish(httpClient, baseCreatePost.getReasonsDespImageList().get(i).reason3ImagePath, "reason3file_" + i + ".png", 3,
                        baseCreatePost.getReasonsDespImageList().get(i).langIndex, responseGson.content.agentListingId);
            }
            return true;
        }

        private boolean checkImagePathValid(String imagePath) {
            if (imagePath == null || imagePath.isEmpty()) {
                return false;
            }
            return true;
        }

        private void resizePhotoAndPublish(DefaultHttpClient httpClient, String imagePath, String fileName, int position, int langIndex, int listingId) {
            File reason1InDevice = new File(FileUtil.getAppSubFolder(Constants.UPLOAD_FILE_FOLDER_NAME),
                    fileName);
            if (checkImagePathValid(imagePath)) {
                try {
//                    FileUtil.convertBitmapToFile(reason1InDevice, ImageUtil.decodeSampledBitmapFromResource(imagePath));
                    ImageUtil.getCompressImageFilePath(imagePath, reason1InDevice.getAbsolutePath(), ImageUtil.normalImageType);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                publishProgress(callUploadFileApi(httpClient, Constants.UPLOAD_REASON_ID_PHOTO, listingId, position, langIndex,
                        reason1InDevice));
            }

        }


        @Override
        protected void onProgressUpdate(Boolean... values) {
            if (values[0]) {
                successCount++;
                barProgressDialog.setMessage(getString(R.string.alert__uploading) + successCount + " / " + getCurrentCreatePost().getUploadImageCount());
            }
        }

        @Override
        protected void onPostExecute(Boolean bool) {
            successCount = 0;
            callAgentProfileGetApi();
        }
    }

    private void updateQBDialog(int listingId) {
        Gson gson = new Gson();
        List<DBQBChatDialog> dbqbChatDialogList = DatabaseManager.getInstance(this).listQBChatDialog();
        if (dbqbChatDialogList == null)
            return;
        for (DBQBChatDialog dbqbChatDialog : dbqbChatDialogList) {
            QBDialog dialog = gson.fromJson(dbqbChatDialog.getQBChatDialog(), QBDialog.class);
            Map<String, String> data = new HashMap<>();
            data.put("data[currentAgentListingID]", "" + listingId);
            dialog.setData(data);
            QBRequestUpdateBuilder requestBuilder = new QBRequestUpdateBuilder();
            QueryUpdateDialog queryUpdateDialog = new QueryUpdateDialog(dialog, requestBuilder);
            queryUpdateDialog.performAsyncWithCallback(new QBEntityCallback<QBDialog>() {
                @Override
                public void onSuccess(QBDialog qbDialog, Bundle bundle) {
                    Log.w("queryUpdateDialog", "onSuccess" + qbDialog);
                }

                @Override
                public void onSuccess() {

                }

                @Override
                public void onError(List<String> list) {
                    Log.e("queryUpdateDialog", "onError" + list);
                }
            });
        }

    }

    private void updateDbByApiResponse(ResponseAgentProfile agentProfileResponse) {
        AgentProfiles agentProfile = agentProfileResponse.content.profile;

        DatabaseManager databaseManager = DatabaseManager.getInstance(CreatePostPreviewActivity.this);
        DBLogInfo dbLogInfo = databaseManager.getLogInfo();

        ResponseLoginSocial.Content userContent = new Gson().fromJson(dbLogInfo.getLogInfo(),
                ResponseLoginSocial.Content.class);
        userContent.agentProfile = agentProfile;
        CoreData.setUserContent(userContent);
        dbLogInfo.setLogInfo(new Gson().toJson(userContent));
        databaseManager.updateLogInfo(dbLogInfo);
    }

    private void callAgentProfileGetApi() {
        RequestAgentProfile request = new RequestAgentProfile(
                userContent.memId,
                LocalStorageHelper.getAccessToken(getBaseContext()),10);
        request.getAgentProfile(this, request, new ApiCallback() {
            @Override
            public void success(ApiResponse apiResponse) {
                barProgressDialog.dismiss();
                Gson gson = new Gson();
                ResponseAgentProfile agentProfileResponse = gson.fromJson(apiResponse.jsonContent, ResponseAgentProfile.class);
                updateDbByApiResponse(agentProfileResponse);

                BaseCreatePost.getInstance(CreatePostPreviewActivity.this).resetBaseCreatePost();
                //todo update dialog
                if (agentProfileResponse != null && agentProfileResponse.content != null &&
                        agentProfileResponse.content.profile != null &&
                        agentProfileResponse.content.profile.agentListing != null)
                    updateQBDialog(agentProfileResponse.content.profile.agentListing.listingID);

                // Mixpanel
                MixpanelAPI mixpanel = MixpanelAPI.getInstance(CreatePostPreviewActivity.this, AppConfig.getMixpanelToken());
                mixpanel.track("Successful Publish Posting");
                mixpanel.getPeople().increment("# of listings", 1);
                mixpanel.getPeople().set("Listing last updated", AppUtil.dateStringForMixpanel());
                mixpanel.getPeople().setOnce("First listing created", AppUtil.dateStringForMixpanel());


                //go to invite to follow list or exit
                if (needShowInviteToFollow())
                    InviteToFollowActivity.start(CreatePostPreviewActivity.this, InviteToFollowActivity.InviteToFollowActivityType.SHOW_SKIP);
                else
                    exitActivity();
            }

            @Override
            public void failure(String errorMsg) {
                AppUtil.showToast(CreatePostPreviewActivity.this, getString(R.string.error_message__please_try_again_later));
            }
        });
    }

    private boolean needShowInviteToFollow() {
        return CoreData.inviteToFollowShownCount>0;
    }


    private boolean callUploadFileApi(DefaultHttpClient httpClient, int fileType, int agentListingId, int position, int langIndex, File photoFile) {
        agentContent = new Gson().fromJson(DatabaseManager.getInstance(CreatePostPreviewActivity.this).getLogInfo().getLogInfo(),
                ResponseLoginSocial.Content.class);

        HttpPost httppost = new HttpPost(AdminService.WEB_SERVICE_BASE_URL + "/Admin.asmx/FileUpload");
        MultipartEntity multipartEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);

        try {
            multipartEntity.addPart("MemberID", new StringBody("" + agentContent.memId));
            multipartEntity.addPart("AccessToken", new StringBody(LocalStorageHelper.getAccessToken(getBaseContext())));
            multipartEntity.addPart("ListingID", new StringBody("" + agentListingId));
            multipartEntity.addPart("Position", new StringBody("" + position));
            multipartEntity.addPart("FileExt", new StringBody(FileUtil.getFileExt(photoFile.getAbsolutePath())));
            multipartEntity.addPart("FileType", new StringBody("" + fileType));
            multipartEntity.addPart("LanguageIndex", new StringBody("" + langIndex));
            multipartEntity.addPart("attachment", new FileBody(photoFile));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        httppost.setEntity(multipartEntity);
        try {
            HttpResponse httpResponse = httpClient.execute(httppost);
            BufferedReader reader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent(), "UTF-8"));
            StringBuilder builder = new StringBuilder();
            for (String line = null; (line = reader.readLine()) != null; ) {
                builder.append(line).append("\n");
            }
            String json = builder.toString();
            return httpResponse.getStatusLine().getStatusCode() == 200;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_CANCELED && requestCode == InviteToFollowActivity.InviteToFollowActivityType.SHOW_SKIP.getValue()) {
            exitActivity();
        } else if (resultCode == RESULT_OK && requestCode == Constants.CREATE_POST_ADD_LANG) {
//            exitActivity();
            initView();
//            final int selectedPage=langIndexList.size()-1;
            final int selectedPage= createPostPreviewPagerAdapter.getCount()-1;
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    previewDetailViewPager.setCurrentItem(selectedPage,false);
                    onPageChangeListener.onPageSelected(selectedPage);
                    previewDetailViewPager.invalidate();
                    createPostPreviewPagerAdapter.notifyDataSetChanged();
                    Log.d(TAG, "onActivityResult delay" + selectedPage);
                }
            }, 300);
        }
    }
}
