package co.real.productionreal2.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import co.real.productionreal2.R;
import co.real.productionreal2.common.LocalStorageHelper;
import co.real.productionreal2.setting.SearchFilterSetting;

/**
 * Created by kelvinsun on 4/1/16.
 */
public class TitleBar extends RelativeLayout {

    public static interface TitleBarListener {
        public void OnTitleButtonClick();

        public void OnEditeButtonClick();

    }

    private LinearLayout llSearchSection;
    private TextView tvTitle;
    private EditText etSearchField;
    private TextView tvSearchResultCount;
    private ImageButton miniviewBtn;
    private FrameLayout flFilterBtn;
    private ImageButton filterBtn;
    private TextView tvFilterAlert;
    private ImageButton inviteFollowBtn;
    private ImageButton editBtn;
    private ImageButton inviteToChatBtn;
    private LinearLayout chatSearchLinearLayout;
    private EditText searchAgentEditText;
    private ImageButton agentSearchCancelBtn;
    private ImageButton agentSearchBtn;
    private ImageButton editProfileBtn;
    private ImageButton editChatLobbyBtn;
    private ImageButton findAgentBtn;
//    private ImageButton inputPhoneNumberBtn;


    public TAB_TYPE tabType;

    public enum TAB_TYPE {SEARCH, NEWSFEED, CHAT, SETTING, NEWSFEED_DETAILS, NEWSFEED_PROFILE, ME_HOME_AGENT, ME_HOME_USER, ME_PROFILE, ME_PREVIEW}

    private TitleBarListener mListener;

    public void setListener(TitleBarListener listener) {
        mListener = listener;
    }

    public TitleBar(Context context) {
        super(context);
    }

    public TitleBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TitleBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        // find views
        llSearchSection = (LinearLayout) findViewById(R.id.ll_search_section);
        tvTitle = (TextView) findViewById(R.id.tv_tab_container_title);
        etSearchField = (EditText) findViewById(R.id.et_search_field);
        tvSearchResultCount = (TextView) findViewById(R.id.tv_search_result_count);
        findAgentBtn= (ImageButton) findViewById(R.id.findAgentBtn);
        miniviewBtn = (ImageButton) findViewById(R.id.miniviewBtn);
        flFilterBtn = (FrameLayout) findViewById(R.id.fl_filter_btn);
        filterBtn = (ImageButton) findViewById(R.id.filterBtn);
        tvFilterAlert = (TextView) findViewById(R.id.tv_filter_alert);
        inviteFollowBtn = (ImageButton) findViewById(R.id.broadcastBtn);
        editBtn = (ImageButton) findViewById(R.id.editBtn);
        inviteToChatBtn = (ImageButton) findViewById(R.id.inviteToChatBtn);
        agentSearchBtn = (ImageButton) findViewById(R.id.agentSearchBtn);
        chatSearchLinearLayout = (LinearLayout) findViewById(R.id.chatSearchLinearLayout);
        searchAgentEditText = (EditText) findViewById(R.id.searchAgentEditText);
        agentSearchCancelBtn = (ImageButton) findViewById(R.id.agentSearchCancelBtn);
        editProfileBtn = (ImageButton) findViewById(R.id.editProfileBtn);
        editChatLobbyBtn = (ImageButton) findViewById(R.id.editChatLobbyBtn);
//        inputPhoneNumberBtn = (ImageButton) findViewById(R.id.verifyPhoneNumberBtn);


        filterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null) {
                    mListener.OnTitleButtonClick();
                }
            }
        });

        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null) {
                    mListener.OnEditeButtonClick();
                }
            }
        });

//        inputPhoneNumberBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                ImageUtil.getInstance().prepareBlurImageFromActivity((Activity)TitleBar.this.getContext(), InputPhoneNumberActivity.class, new OnBlurCompleteListener() {
//                    @Override
//                    public void onBlurComplete() {
//                        Intent intent = new Intent(TitleBar.this.getContext(), InputPhoneNumberActivity.class);
//                        Navigation.presentIntentForResult((Activity) TitleBar.this.getContext(), intent, Constants.INPUT_PHONE_NUMBER_REQUSTCODE);
//
//                    }
//                });
//            }
//        });
//        updateInputPhoneNumberBtn();

    }

//    public void updateInputPhoneNumberBtn(){
//        if(LocalStorageHelper.getIsPhoneReged(TitleBar.this.getContext())){
//            inputPhoneNumberBtn.setImageResource(R.drawable.btn_nav_bar_verify_on);
//            inputPhoneNumberBtn.setEnabled(false);
//        }else{
//            inputPhoneNumberBtn.setImageResource(R.drawable.btn_nav_bar_verify_off);
//            inputPhoneNumberBtn.setEnabled(true);
//        }
//    }

    public void setupTitleBar(TAB_TYPE mTabType) {
//        inputPhoneNumberBtn.setVisibility(GONE);
        switch (mTabType) {
            case SEARCH:
                //search

                tvTitle.setVisibility(View.GONE);
                llSearchSection.setVisibility(View.VISIBLE);
                findAgentBtn.setVisibility(GONE);
                miniviewBtn.setVisibility(View.GONE);
                flFilterBtn.setVisibility(View.VISIBLE);
                filterBtn.setEnabled(LocalStorageHelper.getLatestSearchResultList(getContext()).size() > 0);
                int alertCount = SearchFilterSetting.getInstance().getAlertCount();
                tvFilterAlert.setVisibility(alertCount != 0 ? View.VISIBLE : View.GONE);
                tvFilterAlert.setText(String.valueOf(alertCount));
                editBtn.setVisibility(View.GONE);
                inviteToChatBtn.setVisibility(View.GONE);
                inviteFollowBtn.setVisibility(View.GONE);
                agentSearchBtn.setVisibility(View.GONE);
                chatSearchLinearLayout.setVisibility(View.GONE);
                editProfileBtn.setVisibility(View.GONE);
                chatSearchLinearLayout.setVisibility(View.GONE);
                editChatLobbyBtn.setVisibility(GONE);

                break;

            case NEWSFEED:
                //newsfeed

                tvTitle.setVisibility(View.VISIBLE);
                tvTitle.setText(getContext().getString(R.string.newsfeed__title));
                llSearchSection.setVisibility(View.GONE);
                findAgentBtn.setVisibility(VISIBLE);
                miniviewBtn.setVisibility(View.VISIBLE);
                flFilterBtn.setVisibility(View.GONE);
                editBtn.setVisibility(View.GONE);
                inviteToChatBtn.setVisibility(View.GONE);
                inviteFollowBtn.setVisibility(View.GONE);
                agentSearchBtn.setVisibility(View.GONE);
                chatSearchLinearLayout.setVisibility(View.GONE);
                editProfileBtn.setVisibility(View.GONE);
                editChatLobbyBtn.setVisibility(GONE);
                break;
            case CHAT:
                //chat
                tvTitle.setText(getContext().getString(R.string.chatlobby__title));
                llSearchSection.setVisibility(View.GONE);
                findAgentBtn.setVisibility(GONE);
                miniviewBtn.setVisibility(View.GONE);
                flFilterBtn.setVisibility(View.GONE);
                editBtn.setVisibility(View.GONE);
                inviteFollowBtn.setVisibility(View.GONE);
                if(getSearchAgentEditText().getText().toString().isEmpty()){
                    tvTitle.setVisibility(View.VISIBLE);
                    chatSearchLinearLayout.setVisibility(View.GONE);
                    agentSearchBtn.setVisibility(View.VISIBLE);
                    inviteToChatBtn.setVisibility(View.VISIBLE);
                }else{
                    tvTitle.setVisibility(View.GONE);
                    chatSearchLinearLayout.setVisibility(View.VISIBLE);
                    agentSearchBtn.setVisibility(View.GONE);
                    inviteToChatBtn.setVisibility(View.GONE);
                }
                editProfileBtn.setVisibility(View.GONE);
                editChatLobbyBtn.setVisibility(View.VISIBLE);

                break;
            case SETTING:
                //setting

                tvTitle.setVisibility(View.VISIBLE);
                tvTitle.setText(getContext().getString(R.string.setting__settings));
                llSearchSection.setVisibility(View.GONE);
                flFilterBtn.setVisibility(View.INVISIBLE);
                findAgentBtn.setVisibility(GONE);
                miniviewBtn.setVisibility(View.GONE);
                editBtn.setVisibility(View.GONE);
                inviteToChatBtn.setVisibility(View.GONE);
                inviteFollowBtn.setVisibility(View.GONE);
                agentSearchBtn.setVisibility(View.GONE);
                chatSearchLinearLayout.setVisibility(View.GONE);
                editProfileBtn.setVisibility(View.GONE);
                editChatLobbyBtn.setVisibility(GONE);
                break;
            case NEWSFEED_DETAILS:
                //newsfeed detail
                tvTitle.setVisibility(View.VISIBLE);
                llSearchSection.setVisibility(View.GONE);
                flFilterBtn.setVisibility(View.GONE);
                findAgentBtn.setVisibility(GONE);
                miniviewBtn.setVisibility(View.GONE);
                editBtn.setVisibility(View.VISIBLE);
                inviteToChatBtn.setVisibility(View.GONE);
                inviteFollowBtn.setVisibility(View.GONE);
                agentSearchBtn.setVisibility(View.GONE);
                chatSearchLinearLayout.setVisibility(View.GONE);
                editProfileBtn.setVisibility(View.GONE);
                break;
            case NEWSFEED_PROFILE:
                //newsfeed agent profile
                tvTitle.setVisibility(View.VISIBLE);
                tvTitle.setText(getContext().getString(R.string.profile__title));
                llSearchSection.setVisibility(View.GONE);
                flFilterBtn.setVisibility(View.GONE);
                findAgentBtn.setVisibility(GONE);
                miniviewBtn.setVisibility(View.GONE);
                editBtn.setVisibility(View.VISIBLE);
                inviteToChatBtn.setVisibility(View.GONE);
                inviteFollowBtn.setVisibility(View.GONE);
                agentSearchBtn.setVisibility(View.GONE);
                chatSearchLinearLayout.setVisibility(View.GONE);
                editProfileBtn.setVisibility(View.GONE);
                editChatLobbyBtn.setVisibility(GONE);
                break;
            case ME_HOME_AGENT:
                //me home agent

                tvTitle.setVisibility(View.VISIBLE);
                tvTitle.setText(getContext().getString(R.string.me__title));
                llSearchSection.setVisibility(View.GONE);
                flFilterBtn.setVisibility(View.GONE);
                findAgentBtn.setVisibility(GONE);
                miniviewBtn.setVisibility(View.GONE);
                editBtn.setVisibility(View.GONE);
                inviteToChatBtn.setVisibility(View.GONE);
                inviteFollowBtn.setVisibility(View.VISIBLE);
                agentSearchBtn.setVisibility(View.GONE);
                editProfileBtn.setVisibility(View.GONE);
                chatSearchLinearLayout.setVisibility(View.GONE);
                editChatLobbyBtn.setVisibility(GONE);
                break;
            case ME_HOME_USER:
                //me home user

                tvTitle.setVisibility(View.VISIBLE);
                tvTitle.setText(getContext().getString(R.string.me__title));
                llSearchSection.setVisibility(View.GONE);
                flFilterBtn.setVisibility(View.GONE);
                findAgentBtn.setVisibility(GONE);
                miniviewBtn.setVisibility(View.GONE);
                editBtn.setVisibility(View.GONE);
                inviteToChatBtn.setVisibility(View.GONE);
                inviteFollowBtn.setVisibility(View.GONE);
                agentSearchBtn.setVisibility(View.GONE);
                editProfileBtn.setVisibility(View.VISIBLE);
                chatSearchLinearLayout.setVisibility(View.GONE);
                editChatLobbyBtn.setVisibility(GONE);
                break;
            case ME_PROFILE:
                //me profile
                tvTitle.setVisibility(View.VISIBLE);
                tvTitle.setText(getContext().getString(R.string.common__my_profile));
                llSearchSection.setVisibility(View.GONE);
                flFilterBtn.setVisibility(View.GONE);
                findAgentBtn.setVisibility(GONE);
                miniviewBtn.setVisibility(View.GONE);
                editBtn.setVisibility(View.GONE);
                inviteToChatBtn.setVisibility(View.GONE);
                inviteFollowBtn.setVisibility(View.GONE);
                agentSearchBtn.setVisibility(View.GONE);
                editProfileBtn.setVisibility(View.VISIBLE);
                chatSearchLinearLayout.setVisibility(View.GONE);
                editChatLobbyBtn.setVisibility(GONE);

                break;
            case ME_PREVIEW:
                //me listing preview
                tvTitle.setVisibility(View.VISIBLE);
                editProfileBtn.setVisibility(View.GONE);
                llSearchSection.setVisibility(View.GONE);
                flFilterBtn.setVisibility(View.GONE);
                findAgentBtn.setVisibility(GONE);
                miniviewBtn.setVisibility(View.GONE);
                editBtn.setVisibility(View.GONE);
                inviteToChatBtn.setVisibility(View.GONE);
                inviteFollowBtn.setVisibility(View.GONE);
                agentSearchBtn.setVisibility(View.GONE);
                chatSearchLinearLayout.setVisibility(View.GONE);
                editChatLobbyBtn.setVisibility(GONE);
            default:
                break;
        }
    }

    public void setMePreviewTitle(String location) {
        tvTitle.setText(location);
    }

    public void setResultCount(int count) {
        String resultCountStr;
        if (count > 0) {
            if (count > 1000)
                resultCountStr = "1000+ " + getResources().getString(R.string.common__agents);
            else
                resultCountStr = count + " " + getResources().getString(R.string.common__agents);

            tvSearchResultCount.setText(resultCountStr);

            tvSearchResultCount.setVisibility(View.VISIBLE);
            Animation a = AnimationUtils.loadAnimation(getContext(), R.anim.slide_in_right);
            a.reset();
            tvSearchResultCount.clearAnimation();
            tvSearchResultCount.startAnimation(a);
        } else {
            tvSearchResultCount.setVisibility(View.GONE);
        }
    }

    public LinearLayout getLlSearchSection() {
        return llSearchSection;
    }

    public void setLlSearchSection(LinearLayout llSearchSection) {
        this.llSearchSection = llSearchSection;
    }

    public TextView getTvTitle() {
        return tvTitle;
    }

    public void setTitle(String title) {
        tvTitle.setText(title);
    }

    public EditText getEtSearchField() {
        return etSearchField;
    }

    public void setSearchFieldText(String text) {
        etSearchField.setText(text);
    }


    public ImageButton getFindAgentBtn() {
        return findAgentBtn;
    }

    public ImageButton getMiniviewBtn() {
        return miniviewBtn;
    }

    public void setMiniviewBtn(ImageButton miniviewBtn) {
        this.miniviewBtn = miniviewBtn;
    }

    public FrameLayout getFlFilterBtn() {
        return flFilterBtn;
    }

    public void setFlFilterBtn(FrameLayout flFilterBtn) {
        this.flFilterBtn = flFilterBtn;
    }

    public ImageButton getFilterBtn() {
        return filterBtn;
    }

    public void setFilterBtn(ImageButton filterBtn) {
        this.filterBtn = filterBtn;
    }

    public TextView getTvFilterAlert() {
        return tvFilterAlert;
    }

    public void setTvFilterAlert(TextView tvFilterAlert) {
        this.tvFilterAlert = tvFilterAlert;
    }

    public ImageButton getInviteFollowBtn() {
        return inviteFollowBtn;
    }

    public void setInviteFollowBtn(ImageButton inviteFollowBtn) {
        this.inviteFollowBtn = inviteFollowBtn;
    }

    public ImageButton getEditBtn() {
        return editBtn;
    }

    public ImageButton getAgentSearchBtn() {
        return agentSearchBtn;
    }

    public void setAgentSearchBtn(ImageButton agentSearchBtn) {
        this.agentSearchBtn = agentSearchBtn;
    }

    public ImageButton getInviteToChatBtn() {
        return inviteToChatBtn;
    }

    public void setInviteToChatBtn(ImageButton inviteToChatBtn) {
        this.inviteToChatBtn = inviteToChatBtn;
    }

    public void setEditBtn(ImageButton editBtn) {
        this.editBtn = editBtn;
    }

    public LinearLayout getChatSearchLinearLayout() {
        return chatSearchLinearLayout;
    }

    public void setChatSearchLinearLayout(LinearLayout chatSearchLinearLayout) {
        this.chatSearchLinearLayout = chatSearchLinearLayout;
    }

    public EditText getSearchAgentEditText() {
        return searchAgentEditText;
    }

    public void setSearchAgentEditText(EditText searchAgentEditText) {
        this.searchAgentEditText = searchAgentEditText;
    }

    public ImageButton getAgentSearchCancelBtn() {
        return agentSearchCancelBtn;
    }

    public void setAgentSearchCancelBtn(ImageButton agentSearchCancelBtn) {
        this.agentSearchCancelBtn = agentSearchCancelBtn;
    }

    public ImageButton getEditProfileBtn() {
        return editProfileBtn;
    }

    public void setEditProfileBtn(ImageButton editProfileBtn) {
        this.editProfileBtn = editProfileBtn;
    }

    public ImageButton getEditChatLobbyBtn() {
        return editChatLobbyBtn;
    }

    public void setEditChatLobbyBtn(ImageButton editChatLobbyBtn) {
        this.editChatLobbyBtn = editChatLobbyBtn;
    }


    public void setQBunlogin() {
        inviteToChatBtn.setVisibility(View.GONE);
        agentSearchBtn.setVisibility(View.GONE);
        editChatLobbyBtn.setVisibility(View.GONE);
    }
}
