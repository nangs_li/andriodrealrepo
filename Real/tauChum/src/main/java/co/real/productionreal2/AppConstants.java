package co.real.productionreal2;

/**
 * Created by kelvinsun on 28/12/15.
 */
public class AppConstants {

    public enum CommercialSpaceType {
        CommericalOffice,
        CommericalIndustrial,
        CommericalWarehouse,
        CommericalFlexSpace,
        CommericalRetail,
        CommericalLand,
        CommericalAgricultural,
        CommericalHotelAndMotel,
        CommericalHealthCare,
        CommericalSpecialPurpose,
        CommericalOther;
    }

    public enum ResidentialSpaceType {
        ResidentialApartment,
        ResidentialUnit,
        ResidentialTownhouse,
        ResidentialLoft,
        ResidentialHouseboat,
        ResidentialVilla,
        ResidentialLand,
        ResidentialCondos,
        ResidentialSuburbanHome,
        ResidentialOther
    }


}
