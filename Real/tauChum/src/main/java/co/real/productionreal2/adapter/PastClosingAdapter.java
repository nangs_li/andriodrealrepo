package co.real.productionreal2.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nhaarman.listviewanimations.ArrayAdapter;
import com.nhaarman.listviewanimations.itemmanipulation.swipedismiss.undo.UndoAdapter;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.ButterKnife;
import butterknife.InjectView;
import co.real.productionreal2.R;
import co.real.productionreal2.common.LocalStorageHelper;
import co.real.productionreal2.model.SystemSetting;
import co.real.productionreal2.service.model.AgentPastClosing;
import co.real.productionreal2.util.AppUtil;
import co.real.productionreal2.util.StringUtils;
import co.real.productionreal2.util.TimeUtils;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

public class PastClosingAdapter extends ArrayAdapter<AgentPastClosing> implements UndoAdapter, StickyListHeadersAdapter {

    private final Context mContext;

    private ClosingRemoveBtnClickListener mClickListener = null;

    private List<SystemSetting.PropertyType> propertyTypes;
    private boolean isViewOnly = false;
    private int currentLangIndex;

    public PastClosingAdapter(Context context,
                              ClosingRemoveBtnClickListener listener,
                              List<SystemSetting.PropertyType> propertyTypes) {
        mContext = context;
        this.propertyTypes = propertyTypes;
        mClickListener = listener;
        currentLangIndex = AppUtil.getCurrentLangIndex(mContext);
//        for (int i = 0; i < 1000; i++) {
//            add(mContext.getString(R.string.row_number, i));
//        }
    }

    public PastClosingAdapter(Context context,
                              ClosingRemoveBtnClickListener listener,
                              List<SystemSetting.PropertyType> propertyTypes, boolean isViewOnly) {
        mContext = context;
        this.propertyTypes = propertyTypes;
        mClickListener = listener;
        currentLangIndex = AppUtil.getCurrentLangIndex(mContext);
        this.isViewOnly = isViewOnly;
    }

    public void append(List<AgentPastClosing> pastClosingList) {
        this.append(pastClosingList);
    }


    @Override
    public long getItemId(final int position) {
        return getItem(position).hashCode();
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getView(final int position, View view, final ViewGroup parent) {

        ViewHolder holder;
        if (view != null) {
            holder = (ViewHolder) view.getTag();
        } else {
            if (isViewOnly) {
                view = LayoutInflater.from(mContext).inflate(R.layout.view_list_row_past_closing, parent, false);
            } else {
                view = LayoutInflater.from(mContext).inflate(R.layout.list_row_past_closings, parent, false);
            }

            holder = new ViewHolder(view);
            view.setTag(holder);
        }

        final AgentPastClosing pastclosing = getItem(position);

        String propertyTypeStr = pastclosing.propertyType + "," + pastclosing.spaceType;
        holder.tvApartment.setText(LocalStorageHelper.loadPropertyTypeHashMap(mContext).get(propertyTypeStr));


//    	    if ( view.getContext().getResources().getStringArray(R.array.space_type_array).length > pastclosing.spaceType  ){
//    	    	holder.tvApartment.setText( view.getContext().getResources().getStringArray(R.array.space_type_array)[pastclosing.spaceType ] );
//    	    }
        holder.tvPrice.setText(pastclosing.soldPrice);

//        if (pastclosing.soldDate.indexOf("(") != -1 && pastclosing.soldDate.indexOf(")") != -1) {
//            holder.tvDate.setText(getDate(Long.parseLong(pastclosing.soldDate.substring(pastclosing.soldDate.indexOf("(") + 1, pastclosing.soldDate.indexOf(")")))));
//        } else {
//            holder.tvDate.setText(pastclosing.soldDate + "");
//        }
        Date soldDate = TimeUtils.stringToDate(pastclosing.soldDateString);
        if (soldDate != null) {
            SimpleDateFormat outputDateFormat = new SimpleDateFormat(mContext.getString(R.string.general_date_format_month_year), mContext.getResources().getConfiguration().locale);
            holder.tvDate.setText(StringUtils.getById(mContext, R.string.agent_profile__closing_date, "closing") + ": " + outputDateFormat.format(soldDate));
        }

        holder.tvAddress.setText(pastclosing.address + "");

        holder.removeBtn.setTag(position);
        holder.removeBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (mClickListener != null)
                    mClickListener.onClosingRemove((Integer) v.getTag());
            }
        });

        return view;
    }

    static class ViewHolder {

        @InjectView(R.id.tv_apartment)
        TextView tvApartment;
        @InjectView(R.id.tv_price)
        TextView tvPrice;
        @InjectView(R.id.tv_date)
        TextView tvDate;
        @InjectView(R.id.tv_address)
        TextView tvAddress;
        @InjectView(R.id.removeExpBtn)
        ImageView removeBtn;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }

    @NonNull
    @Override
    public View getUndoView(final int position, final View convertView, @NonNull final ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.undo_row, parent, false);
        }
        return view;
    }

    @NonNull
    @Override
    public View getUndoClickView(@NonNull final View view) {
        return view.findViewById(R.id.undo_row_undobutton);
    }

    @Override
    public View getHeaderView(final int position, final View convertView, final ViewGroup parent) {
        TextView view = (TextView) convertView;
        if (view == null) {
            view = (TextView) LayoutInflater.from(mContext).inflate(R.layout.list_header, parent, false);
        }

        view.setText(mContext.getString(R.string.header, getHeaderId(position)));

        return view;
    }

    @Override
    public long getHeaderId(final int position) {
        return position / 10;
    }


//	private String stringToDate(String time){
//		if (time.indexOf("(") != -1 && time.indexOf(")") != -1){
//			return  stringToDate( Long.parseLong( time.substring( time.indexOf("(") + 1,  time.indexOf(")" ) ) ) )  ;
//		} else {
//			return time;
//		}
//	}

    private String getDate(long time) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time);
        String date = DateFormat.format("dd-MM-yyyy", cal).toString();
        return date;
    }

    //http://stackoverflow.com/questions/20132359/how-to-add-onclicklistener-to-a-button-inside-a-listview-adapter
    public interface ClosingRemoveBtnClickListener {
        public abstract void onClosingRemove(int position);
    }


}