package co.real.productionreal2.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import co.real.productionreal2.Constants;
import co.real.productionreal2.CoreData;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.AlbumActivity;
import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.chat.ChatDbViewController;
import co.real.productionreal2.common.LocalStorageHelper;
import co.real.productionreal2.dao.account.DBQBChatMessage;
import co.real.productionreal2.daomanager.DatabaseManager;
import co.real.productionreal2.model.AgentListing;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.request.RequestAgentListingGetList;
import co.real.productionreal2.service.model.response.ResponseAgentListingGetList;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;
import co.real.productionreal2.util.AppUtil;
import co.real.productionreal2.util.CryptLib;
import co.real.productionreal2.util.FileUtil;
import co.real.productionreal2.util.ImageUtil;
import co.real.productionreal2.util.TimeUtils;
import co.real.productionreal2.view.ListSelectorDialog;
import co.real.productionreal2.view.RoundedCornersTransformation;

/**
 * Created by hohojo on 23/7/2015.
 */
public class ChatAdapter extends BaseAdapter {

    private List<DBQBChatMessage> chatMessages = new ArrayList<>();
    private Activity context;
    private static ChatAdapter instance;
    private Gson gson;
    private int imageDisplaySize;
    private String dialogId;
    private String opponentName;
    private ResponseLoginSocial.Content userContent;
    private HashMap<Integer, Integer> listingMessageLineNoMap;
    private ArrayList<DBQBChatMessage> listingMessageList;
    private ArrayList<Integer> listingMessageLineNoList;
//    public static ChatAdapter getInstance(Activity contexte) {
//
//        if (instance == null) {
//            instance = new ChatAdapter(context);
//        }
//
//        return instance;
//    }

    public String getOpponentName() {
        return opponentName;
    }

    public void setOpponentName(String opponentName) {
        this.opponentName = opponentName;
    }

    public void setDialogId(String dialogId) {
        this.dialogId = dialogId;
    }

    public String getDialogId() {
        return dialogId;
    }

    View.OnClickListener messageImageOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            String photoName = (String) view.getTag();
            Intent intent = new Intent(context, AlbumActivity.class);
            intent.putExtra(Constants.EXTRA_DIALOG_ID, getDialogId());
            intent.putExtra(Constants.EXTRA_PHOTO_NAME, photoName);
            context.startActivity(intent);
        }
    };


    public ChatAdapter(Activity context) {
        this.context = context;
        gson = new Gson();
        imageDisplaySize = ImageUtil.getScreenWidth(context) * 2 / 3;
        userContent = CoreData.getUserContent(context);
        listingMessageList = new ArrayList<>();
        listingMessageLineNoList = new ArrayList<>();
        listingMessageLineNoMap = new HashMap<>();

    }


    @Override
    public int getCount() {
        if (chatMessages != null) {
            return chatMessages.size();
        } else {
            return 0;
        }
    }

    @Override
    public DBQBChatMessage getItem(int position) {
        if (chatMessages != null) {
            return chatMessages.get(position);
        } else {
            return null;
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        DBQBChatMessage dbqbChatMessage = getItem(position);

        DBQBChatMessage previousDbqbChatMessage=null;
        if (position>0)
            previousDbqbChatMessage = getItem(position - 1);

        LayoutInflater vi = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            convertView = vi.inflate(R.layout.message_list_item, null);
            holder = createViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.outErrorImageView.setVisibility(View.GONE);
        holder.inErrorImageView.setVisibility(View.GONE);
        holder.unReadMsgRelativeLayout.setVisibility(View.GONE);

        boolean isOutGoing;
        if (dbqbChatMessage.getMessageType().equalsIgnoreCase(ChatDbViewController.SEND))
            isOutGoing = true;
        else
            isOutGoing = false;

        Log.d("getFullDateFromLong","getFullDateFromLong: "+dbqbChatMessage.getDateSent());
        //date divider
        if (previousDbqbChatMessage!=null &&  dbqbChatMessage!=null) {
            if (!TimeUtils.isSameDate(previousDbqbChatMessage.getDateSent(), dbqbChatMessage.getDateSent())) {
                holder.tvDateDivider.setVisibility(View.VISIBLE);
                holder.tvDateDivider.setText(TimeUtils.getFullDateFromLong(context, dbqbChatMessage.getDateSent()));
            } else {
                holder.tvDateDivider.setVisibility(View.GONE);
            }
        }else if (previousDbqbChatMessage==null){
            holder.tvDateDivider.setVisibility(View.VISIBLE);
            holder.tvDateDivider.setText(TimeUtils.getFullDateFromLong(context, dbqbChatMessage.getDateSent()));
        }

//////////////////////// unread msg bar ////////////////////////
        if (dbqbChatMessage.getIsUnreadFirstMessage() != null &&
                dbqbChatMessage.getIsUnreadFirstMessage())
            holder.unReadMsgRelativeLayout.setVisibility(View.VISIBLE);

        if (dbqbChatMessage.getMessageCategory() == null) {
            holder.inMsgRelativeLayout.setVisibility(View.GONE);
            holder.outMsgRelativeLayout.setVisibility(View.GONE);
            holder.rlAgentListing.setVisibility(View.GONE);
            return convertView;
        }
        if (dbqbChatMessage.getMessageCategory() != null &&
                dbqbChatMessage.getMessageCategory().equalsIgnoreCase(ChatDbViewController.MSG_TYPE_AGENTLISTING)) {
            //////////////////////////////////////////////// agent listing ////////////////////////////////////////////////
            holder.inMsgRelativeLayout.setVisibility(View.GONE);
            holder.outMsgRelativeLayout.setVisibility(View.GONE);
            holder.rlAgentListing.setVisibility(View.GONE);
            if (dbqbChatMessage.getAgentListing() == null)
                callAgentListingGetListAndSetView(dbqbChatMessage, holder);
            else {
                setAgentListingView(gson.fromJson(dbqbChatMessage.getAgentListing(), AgentListing.class), holder);
            }
        } else {
            holder.rlAgentListing.setVisibility(View.GONE);
            if (isOutGoing) {
////////////////////////////////////////////////////////////////////////////////////////////////sending msg////////////////////////////////////////////////////////////////////////////////////////////////
                holder.inMsgRelativeLayout.setVisibility(View.GONE);
                holder.outMsgRelativeLayout.setVisibility(View.VISIBLE);
                holder.outTimeTextView.setText(TimeUtils.getHourMinString(context, dbqbChatMessage.getDateSent()));

                if (dbqbChatMessage.getMessageCategory().equalsIgnoreCase(ChatDbViewController.MSG_TYPE_TEXT)) {
                    //////////////////////////////////////////////// text ////////////////////////////////////////////////
                    holder.outMsgTextView.setVisibility(View.VISIBLE);
                    holder.outMsgImageBtn.setVisibility(View.GONE);

                    holder.outMsgTextView.setBackgroundResource(R.drawable.chat_bubble_blue);
                    holder.outMsgTextView.setText(CryptLib.decryptedQBText(dbqbChatMessage));
                    holder.outMsgTextView.measure(0, 0);
                    int newWidth = holder.outMsgTextView.getMeasuredWidth();
                    if (newWidth > 158)
                        holder.outMsgTextView.setGravity(Gravity.LEFT);
                    else
                        holder.outMsgTextView.setGravity(Gravity.CENTER);
                } else if (dbqbChatMessage.getMessageCategory().equalsIgnoreCase(ChatDbViewController.MSG_TYPE_IMAGE)) {
                    //////////////////////////////////////////////// photos ////////////////////////////////////////////////
                    if (dbqbChatMessage.getPhotoLocalPath() != null || dbqbChatMessage.getPhotoURL() != null) {
                        //check is image or text
                        holder.outMsgTextView.setVisibility(View.GONE);
                        holder.outMsgImageBtn.setVisibility(View.VISIBLE);
                        if (dbqbChatMessage.getPhotoLocalPath() != null) {
                            File photoFile = new File(dbqbChatMessage.getPhotoLocalPath());
                            Picasso.with(context).load(photoFile)
                                    .placeholder(R.drawable.progress_animation)
                                    .resize(imageDisplaySize, imageDisplaySize)
                                    .centerInside()
                                    .transform(new RoundedCornersTransformation(20, 20))
                                    .into(holder.outMsgImageBtn);
                            holder.outMsgImageBtn.setTag(dbqbChatMessage.getPhotoLocalPath());
                        } else {
                            String photoUrl = dbqbChatMessage.getPhotoURL();
                            Picasso.with(context).load(photoUrl)
                                    .placeholder(R.drawable.progress_animation)
                                    .resize(imageDisplaySize, imageDisplaySize)
                                    .centerInside()
                                    .transform(new RoundedCornersTransformation(20, 20))
                                    .into(holder.outMsgImageBtn);
                            holder.outMsgImageBtn.setTag(dbqbChatMessage.getPhotoURL());
                        }

                        holder.outMsgImageBtn.setOnClickListener(messageImageOnClickListener);

                    }
                }

                setTextViewCopyable(holder.outMsgTextView);

            } else {
                ////////////////////////////////////////////////////////////////////////////////////////////////receiving msg////////////////////////////////////////////////////////////////////////////////////////////////
                holder.inMsgRelativeLayout.setVisibility(View.VISIBLE);
                holder.outMsgRelativeLayout.setVisibility(View.GONE);

                holder.inTimeTextView.setText(TimeUtils.getHourMinString(context, dbqbChatMessage.getDateSent()));

                if (dbqbChatMessage.getMessageCategory().equalsIgnoreCase(ChatDbViewController.MSG_TYPE_RATE)) {
                    //////////////////////////////////////////////// rate ////////////////////////////////////////////////
                    Log.w("ChatAdapter", "rate dbqbChatMessage.getMessageCategory() = " + dbqbChatMessage.getMessageCategory());
                    holder.inMsgTextView.setVisibility(View.GONE);
                    holder.inMsgImageBtn.setVisibility(View.GONE);
                    holder.llRateMsg.setVisibility(View.VISIBLE);
                    holder.tvRateMsg.setText(context.getString(R.string.chatroom__rate_message));
                    holder.rateMsgBtn.setVisibility(View.VISIBLE);
                    holder.rateMsgBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //todo to play store link
                            String url = Constants.RATE_URL;
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(url));
                            context.startActivity(i);
                        }
                    });
                } else if (dbqbChatMessage.getMessageCategory().equalsIgnoreCase(ChatDbViewController.MSG_TYPE_IMAGE)) {
                    //////////////////////////////////////////////// photos ////////////////////////////////////////////////
                    if (dbqbChatMessage.getPhotoLocalPath() != null || dbqbChatMessage.getPhotoURL() != null) {
                        //check is image or text
                        holder.inMsgTextView.setVisibility(View.GONE);
                        holder.inMsgImageBtn.setVisibility(View.VISIBLE);
                        holder.llRateMsg.setVisibility(View.GONE);
                        if (dbqbChatMessage.getPhotoLocalPath() != null) {
                            File photoFile = new File(dbqbChatMessage.getPhotoLocalPath());
                            Picasso.with(context).load(photoFile)
                                    .placeholder(R.drawable.progress_animation)
                                    .resize(imageDisplaySize, imageDisplaySize)
                                    .centerInside()
                                    .transform(new RoundedCornersTransformation(20, 20))
                                    .into(holder.inMsgImageBtn);
                            holder.inMsgImageBtn.setTag(dbqbChatMessage.getPhotoLocalPath());
                        } else {
                            String photoUrl = dbqbChatMessage.getPhotoURL();
                            Picasso.with(context).load(photoUrl)
                                    .placeholder(R.drawable.progress_animation)
                                    .resize(imageDisplaySize, imageDisplaySize)
                                    .centerInside()
                                    .transform(new RoundedCornersTransformation(20, 20))
                                    .into(holder.inMsgImageBtn);
                            holder.inMsgImageBtn.setTag(dbqbChatMessage.getPhotoURL());
                        }
                        holder.inMsgImageBtn.setOnClickListener(messageImageOnClickListener);
                    }
                } else {
                    Log.w("ChatAdapter", "text dbqbChatMessage.getMessageCategory() = " + dbqbChatMessage.getMessageCategory());
                    holder.inMsgTextView.setVisibility(View.VISIBLE);
                    holder.inMsgImageBtn.setVisibility(View.GONE);
                    holder.llRateMsg.setVisibility(View.GONE);
                    holder.inMsgTextView.setBackgroundResource(R.drawable.chat_bubble_grey);

                    if (dbqbChatMessage.getMessageCategory() != null &&
                            !dbqbChatMessage.getMessageCategory().isEmpty()) {
                        if (dbqbChatMessage.getMessageCategory().equalsIgnoreCase(ChatDbViewController.MSG_TYPE_AUDIO)) {
                            holder.inMsgTextView.setText(String.format(context.getString(R.string.chatroom_error_message___want_to_have_a_audio), getOpponentName()));
                        } else if (dbqbChatMessage.getMessageCategory().equalsIgnoreCase(ChatDbViewController.MSG_TYPE_VIDEO)) {
                            holder.inMsgTextView.setText(String.format(context.getString(R.string.chatroom_error_message___want_to_have_a_video), getOpponentName()));
                        } else if (dbqbChatMessage.getMessageCategory().equalsIgnoreCase(ChatDbViewController.MSG_TYPE_UNKNOWN)) {
                            holder.inMsgTextView.setText(String.format(context.getString(R.string.chatroom_error_message___want_to_have_a_something), getOpponentName()));
                        } else if (dbqbChatMessage.getMessageCategory().equalsIgnoreCase(ChatDbViewController.MSG_TYPE_TEXT)) {
                            holder.inMsgTextView.setText(CryptLib.decryptedQBText(dbqbChatMessage));
                        }
                    } else {
                        holder.inMsgTextView.setText(dbqbChatMessage.getBody());
                    }
                    holder.inMsgTextView.measure(0, 0);
                    int newWidth = holder.inMsgTextView.getMeasuredWidth();
                    if (newWidth > 158)
                        holder.inMsgTextView.setGravity(Gravity.LEFT);
                    else
                        holder.inMsgTextView.setGravity(Gravity.CENTER);
                }

                setTextViewCopyable(holder.inMsgTextView);

            }


            // new, sent, sent_user, read, received,  sent_read, error status
            if (dbqbChatMessage.getSendStatus() != null) {
                if (dbqbChatMessage.getMessageType() != null && dbqbChatMessage.getMessageType().equalsIgnoreCase(ChatDbViewController.SEND)) {
                    //send msg
                    holder.outReadImageView.setVisibility(View.VISIBLE);
                    holder.outReadImageView.setImageResource(0);
                    if (dbqbChatMessage.getSendStatus().equalsIgnoreCase(ChatDbViewController.NEW)) {
                        holder.outReadImageView.setVisibility(View.INVISIBLE);
                    } else if (dbqbChatMessage.getSendStatus().equalsIgnoreCase(ChatDbViewController.SENT)) {
                        holder.outReadImageView.setImageResource(R.drawable.chat_sent);
                    } else if (dbqbChatMessage.getSendStatus().equalsIgnoreCase(ChatDbViewController.SENT_USER)) {
                        holder.outReadImageView.setImageResource(R.drawable.chat_sent_user);
                    } else if (dbqbChatMessage.getSendStatus().equalsIgnoreCase(ChatDbViewController.READ)) {
                        if (isDisplayEnableReadMsgTick(dbqbChatMessage)) {
                            holder.outReadImageView.setImageResource(R.drawable.chat_read);
                        } else {
                            holder.outReadImageView.setImageResource(R.drawable.chat_sent_user);
                        }
                    } else if (dbqbChatMessage.getSendStatus().equalsIgnoreCase(ChatDbViewController.ERROR)) {
                        holder.outErrorImageView.setVisibility(View.VISIBLE);
                        holder.outReadImageView.setVisibility(View.INVISIBLE);
                    } else if (dbqbChatMessage.getSendStatus().equalsIgnoreCase(ChatDbViewController.BLOCKED)) {
                        holder.outReadImageView.setImageResource(R.drawable.chat_sent);
                    }
                } else if (dbqbChatMessage.getMessageType() != null &&
                        dbqbChatMessage.getMessageType().equalsIgnoreCase(ChatDbViewController.RECEIVE)) {
                    holder.outReadImageView.setVisibility(View.INVISIBLE);
                }
            }else{
                holder.outReadImageView.setVisibility(View.INVISIBLE);
            }
        }


        return convertView;
    }

    void setTextViewCopyable(TextView mTextView){
        final String selectedText = mTextView.getText().toString();
        mTextView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                ListSelectorDialog dlg = new ListSelectorDialog(context);
                String[] listv = new String[]{context.getString(android.R.string.copy)};
                dlg.show(listv, new ListSelectorDialog.listSelectorInterface() {
                    // procedure for when a user selects an item in the dialog.
                    public void selectedItem(int index, String item) {
                        android.content.ClipboardManager clipboard = (android.content.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                        android.content.ClipData clip = android.content.ClipData.newPlainText("text label", selectedText);
                        clipboard.setPrimaryClip(clip);
                    }
                });

                return false;
            }
        });
    }

    private void setAgentListingView(AgentListing agentListing, ViewHolder holder) {
        holder.rlAgentListing.setVisibility(View.VISIBLE);
        Picasso.with(context).load(agentListing.photos.get(0).url).placeholder(R.drawable.progress_animation)
                .into(holder.agentListingImageView);
        holder.titleTextView.setText(agentListing.googleAddresses.get(0).name);
        holder.subtitleTextView.setText(agentListing.googleAddresses.get(0).location);
        holder.priceTextView.setText(agentListing.currency + " " + agentListing.propertyPriceFormattedForRoman);
        int langIndex=AppUtil.getCurrentLangIndex(context);
        List<Integer> metricIndexList= FileUtil.getSystemSetting(context).obtainMetricIndexList(langIndex);
        List<String> metricNameList =FileUtil.getSystemSetting(context).obtainMetricNameList(langIndex);
        holder.sizeTextView.setText(agentListing.propertySize + " " + metricNameList.get(AppUtil.getMeticPositionFromIndex(metricIndexList, agentListing.sizeUnitType)) + "");
        holder.bedroomTextView.setText("" + agentListing.bedroomCount);
        holder.bathroomTextView.setText("" + agentListing.bathroomCount);
    }

    private void callAgentListingGetListAndSetView(final DBQBChatMessage dbqbChatMessage, final ViewHolder holder) {
        ArrayList<Integer> listingIdList = new ArrayList<>();
        listingIdList.add(Integer.parseInt(dbqbChatMessage.getBindingAgentListingId()));
        RequestAgentListingGetList request = new RequestAgentListingGetList(userContent.memId, LocalStorageHelper.getAccessToken(context), listingIdList);
        request.callAgentListingGetListApi(context, new ApiCallback() {
            @Override
            public void success(ApiResponse apiResponse) {
                ResponseAgentListingGetList responseGson = new Gson().fromJson(apiResponse.jsonContent, ResponseAgentListingGetList.class);
                if (responseGson.content.agentListingsList != null && responseGson.content.agentListingsList.size() > 0) {
                    AgentListing agentListing = responseGson.content.agentListingsList.get(0);
                    dbqbChatMessage.setAgentListing(gson.toJson(agentListing));
                    DatabaseManager.getInstance(context).updateQBChatMessage(dbqbChatMessage);
                    setAgentListingView(agentListing, holder);
                }
            }

            @Override
            public void failure(String errorMsg) {
                holder.rlAgentListing.setVisibility(View.GONE);
            }
        });
    }


    public void add(DBQBChatMessage message) {
        chatMessages.add(message);
    }

    public void setChatMessages(List<DBQBChatMessage> chatMessages) {
        this.chatMessages = chatMessages;
        listingMessageList.clear();
        listingMessageLineNoList.clear();
        listingMessageLineNoMap.clear();
        for (int i = 0; i < chatMessages.size(); i++) {
            if (chatMessages.get(i).getMessageCategory() != null &&
                    chatMessages.get(i).getMessageCategory().equalsIgnoreCase(ChatDbViewController.MSG_TYPE_AGENTLISTING)) {
                if (i > 0) {
                    listingMessageLineNoMap.put(i - 1, listingMessageLineNoMap.size());
                    listingMessageLineNoList.add(i - 1);
                } else {
                    listingMessageLineNoMap.put(0, listingMessageLineNoMap.size());
                    listingMessageLineNoList.add(0);
                }
                listingMessageList.add(chatMessages.get(i));

            }
        }
        notifyDataSetChanged();
    }

    public ArrayList<Integer> getListingMessageLineNoList() {
        return listingMessageLineNoList;
    }

    public ArrayList<DBQBChatMessage> getListingMessageList() {
        return listingMessageList;
    }

    public HashMap<Integer, Integer> getListingMessageLineNoMap() {
        return listingMessageLineNoMap;
    }

    public void clear() {
        chatMessages = null;
    }


    // enable read msg tick in the view
    private boolean isDisplayEnableReadMsgTick(DBQBChatMessage dbqbChatMessage) {
        if (dbqbChatMessage.getEnableReadMsg() != null && dbqbChatMessage.getEnableReadMsg() == false)
            return false;

        return true;
    }

    private ViewHolder createViewHolder(View v) {
        ViewHolder holder = new ViewHolder();
        holder.unReadMsgRelativeLayout = (RelativeLayout) v.findViewById(R.id.unReadMsgRelativeLayout);
        holder.outMsgRelativeLayout = (RelativeLayout) v.findViewById(R.id.outMsgRelativeLayout);
        holder.outMsgTextView = (TextView) v.findViewById(R.id.outMsgTextView);
        holder.outTimeTextView = (TextView) v.findViewById(R.id.outTimeTextView);
        holder.outReadImageView = (ImageView) v.findViewById(R.id.readImageView);
        holder.outErrorImageView = (ImageView) v.findViewById(R.id.outErrorImageView);
        holder.outMsgImageBtn = (ImageButton) v.findViewById(R.id.outMsgImageBtn);

        holder.inMsgRelativeLayout = (RelativeLayout) v.findViewById(R.id.inMsgRelativeLayout);
        holder.inMsgTextView = (TextView) v.findViewById(R.id.inMsgTextView);
        holder.inTimeTextView = (TextView) v.findViewById(R.id.inTimeTextView);
        holder.inErrorImageView = (ImageView) v.findViewById(R.id.inErrorImageView);
        holder.inMsgImageBtn = (ImageButton) v.findViewById(R.id.inMsgImageBtn);

        holder.rlAgentListing = (RelativeLayout) v.findViewById(R.id.rlAgentListing);
        holder.agentListingImageView = (ImageView) v.findViewById(R.id.agentListingImageView);
        holder.titleTextView = (TextView) v.findViewById(R.id.titleTextView);
        holder.subtitleTextView = (TextView) v.findViewById(R.id.subtitleTextView);
//        holder.propertyTypeTextView = (TextView) v.findViewById(R.id.propertyTypeTextView);
        holder.priceTextView = (TextView) v.findViewById(R.id.priceTextView);
        holder.sizeTextView = (TextView) v.findViewById(R.id.sizeTextView);
        holder.bathroomTextView = (TextView) v.findViewById(R.id.bathroomTextView);
        holder.bedroomTextView = (TextView) v.findViewById(R.id.bedroomTextView);

        holder.llRateMsg = (LinearLayout) v.findViewById(R.id.llRateMsg);
        holder.tvRateMsg = (TextView) v.findViewById(R.id.tvRateMsg);
        holder.rateMsgBtn = (Button) v.findViewById(R.id.rateMsgBtn);

        holder.tvDateDivider= (TextView) v.findViewById(R.id.tv_date_divider);

        return holder;
    }


    private static class ViewHolder {
        public RelativeLayout outMsgRelativeLayout;
        public TextView outMsgTextView;
        public TextView outTimeTextView;
        public ImageView outReadImageView;
        public ImageView outErrorImageView;
        public ImageButton outMsgImageBtn;
        public RelativeLayout unReadMsgRelativeLayout;

        public RelativeLayout inMsgRelativeLayout;
        public TextView inMsgTextView;
        public TextView inTimeTextView;
        public ImageView inErrorImageView;
        public ImageButton inMsgImageBtn;

        public RelativeLayout rlAgentListing;
        public ImageView agentListingImageView;
        public TextView titleTextView;
        public TextView subtitleTextView;
        //        public TextView propertyTypeTextView;
        public TextView priceTextView;
        public TextView sizeTextView;
        public TextView bedroomTextView;
        public TextView bathroomTextView;

        public LinearLayout llRateMsg;
        public TextView tvRateMsg;
        public Button rateMsgBtn;

        public TextView tvDateDivider;


    }
}

