package co.real.productionreal2.newsfeed;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import co.real.productionreal2.Constants;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.MainActivityTabBase;
import co.real.productionreal2.model.SystemSetting;
import co.real.productionreal2.service.model.AgentProfiles;
import co.real.productionreal2.util.AppUtil;
import widget.RoundedImageView;

public class NewsFeed_AgentProfilesAdapter extends HeaderRecyclerViewAdapterV2 {

    List<AgentProfiles> agentProfilesList;
    Context context;
    List<SystemSetting.Slogan> sloganList;
    List<SystemSetting.PropertyType> propertyTypeList;
    HashMap<String, String> propertyTypeHashMap = new HashMap<>();
    protected NewsFeed_AgentProfilesAdapterListener mListener;

    boolean isMiniview = false;
    int minivewHeight = -1;

    protected boolean showLoader=false;
    private static final int VIEWTYPE_ITEM = 1;
    private static final int VIEWTYPE_LOADER = 2;

    public interface NewsFeed_AgentProfilesAdapterListener {
        ArrayList<AgentProfiles> getAgentProfileList();
    }

    public NewsFeed_AgentProfilesAdapter(Context context, List<AgentProfiles> agentProfilesList, List<SystemSetting.Slogan> sloganList, List<SystemSetting.PropertyType> propertyTypeList, HashMap<String, String> propertyTypeHashMap, NewsFeed_AgentProfilesAdapterListener mListener) {
        super();
        this.agentProfilesList = agentProfilesList;
        this.context = context;
        this.sloganList = sloganList;
        this.propertyTypeList = propertyTypeList;
        this.propertyTypeHashMap = AppUtil.getPropertyTypeHashMap(context);
        this.mListener=mListener;
    }


    @Override
    public boolean useHeader() {
        return false;
    }

    @Override
    public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindHeaderView(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public boolean useFooter() {
        return true;
    }


    public void setLoaded(){

    }

    @Override
    public RecyclerView.ViewHolder onCreateFooterViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_footer_item, parent, false);
        Footer_ViewHolder viewHolder = new Footer_ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindFooterView(RecyclerView.ViewHolder holder, int position) {
        // Loader ViewHolder
        Log.d("onBindFooterView","onBindFooterView "+holder.toString()+ " "+position + showLoader);
        if (holder instanceof Footer_ViewHolder) {
            Footer_ViewHolder loaderViewHolder = (Footer_ViewHolder)holder;
            if (showLoader) {
                loaderViewHolder.progressSpinner.setVisibility(View.VISIBLE);
            } else {
                loaderViewHolder.progressSpinner.setVisibility(View.GONE);
            }
            return;
        }
    }


    public void showLoader(boolean status) {
        showLoader = status;
    }



    @Override
    public RecyclerView.ViewHolder onCreateBasicItemViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_agentprofiles_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindBasicItemView(RecyclerView.ViewHolder holder, final int position) {

        final AgentProfiles agentProfilesItem = agentProfilesList.get(position);

        if (agentProfilesItem==null)
            return;

        ViewHolder viewHolder = (ViewHolder) holder;

        if (isMiniview) {
            viewHolder.ll_miniview.setVisibility(View.VISIBLE);
            viewHolder.ll_normalview.setVisibility(View.GONE);
            if (minivewHeight == -1) {
//                minivewHeight = viewHolder.ll_miniview.getHeight();
                final View miniview = viewHolder.ll_miniview;
                ViewTreeObserver vto = miniview.getViewTreeObserver();
                vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    public void onGlobalLayout() {
                        minivewHeight = miniview.getHeight();
                    }
                });
            }
        } else {
            viewHolder.ll_miniview.setVisibility(View.GONE);
            viewHolder.ll_normalview.setVisibility(View.VISIBLE);
        }

        String mImageName = AppUtil.getPropertyTypeImageName(agentProfilesItem.agentListing.propertyType, agentProfilesItem.agentListing.spaceType);
        String mPropertyTypeName =propertyTypeHashMap.get(agentProfilesItem.agentListing.propertyType+","+agentProfilesItem.agentListing.spaceType);

        if (mImageName==null)
            mImageName = AppUtil.getPropertyTypeImageName(1, 10);

        if (mPropertyTypeName==null)
            mPropertyTypeName = propertyTypeHashMap.get(1 + "," + 10);

        Log.d("propertyTypeName", "propertyTypeName: spaceIndex: " + mPropertyTypeName + " from " + agentProfilesItem.agentListing.propertyType + "," + agentProfilesItem.agentListing.spaceType);

        final String propertyTypeName=mPropertyTypeName;
        final String imageName=mImageName;

        Picasso.with(context).load(agentProfilesItem.agentPhotoURL)
                .error(R.drawable.default_profile)
                .placeholder(R.drawable.default_profile)
                .into(viewHolder.iv_miniview_icon);
        viewHolder.iv_miniview_name.setText(agentProfilesItem.memberName);
        viewHolder.iv_miniview_propertyType.setImageResource(context.getResources().getIdentifier(imageName+"_mini", "drawable", context.getPackageName()));
        viewHolder.tv_miniview_location.setText(agentProfilesItem.agentListing.googleAddresses.get(0).address);
        viewHolder.tv_miniview_numFollowers.setText(agentProfilesItem.followerCount + "");


        viewHolder.tv_name.setText(agentProfilesItem.memberName);
        viewHolder.tv_numFollowers.setText(agentProfilesItem.followerCount + "");
        Picasso.with(context).load(agentProfilesItem.agentPhotoURL)
                .error(R.drawable.default_profile)
                .placeholder(R.drawable.default_profile)
                .into(viewHolder.img_icon);
        if (agentProfilesItem.agentListing.photos.size() > 0) {
            Picasso.with(context).load(agentProfilesItem.agentListing.photos.get(0).url)
                    .placeholder(R.drawable.progress_animation)
                    .into(viewHolder.iv_thumbnail);
//            Animation a = AnimationUtils.loadAnimation(context, R.anim.fade_in_half);
//            a.reset();
//            a.setDuration(800);
//            viewHolder.iv_thumbnail.clearAnimation();
//            viewHolder.iv_thumbnail.startAnimation(a);
        }

        viewHolder.iv_propertyType.setImageResource(context.getResources().getIdentifier(imageName, "drawable", context.getPackageName()));
        viewHolder.tv_propertyType.setText(propertyTypeName);
//        viewHolder.tv_houseName.setText(agentProfilesItem.agentListing.googleAddresses.get(0).name);
        viewHolder.tv_houseAddress.setText(agentProfilesItem.agentListing.googleAddresses.get(0).location);
        viewHolder.tv_currencyUnit.setText(agentProfilesItem.agentListing.currency);
        viewHolder.tv_propertyPriceFormattedForRoman.setText(agentProfilesItem.agentListing.propertyPriceFormattedForRoman);
        viewHolder.tv_bubble.setText(AppUtil.getSlogan(sloganList, agentProfilesItem.agentListing.reasons.get(0).languageIndex, agentProfilesItem.agentListing.reasons.get(0).sloganIndex));


        viewHolder.iv_miniview_icon.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                doOnTouchEvent(event, agentProfilesItem, position, imageName, propertyTypeName);
                return false;
            }
        });
        viewHolder.iv_miniview_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToProfileFragment(agentProfilesItem);
            }
        });
        viewHolder.ll_miniview.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                doOnTouchEvent(event, agentProfilesItem, position, imageName, propertyTypeName);
                return false;
            }
        });
        viewHolder.ll_miniview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToDetailFragment(agentProfilesItem);
            }
        });
        viewHolder.ll_wholeItemLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                doOnTouchEvent(event, agentProfilesItem, position, imageName, propertyTypeName);
                return false;
            }
        });
        viewHolder.img_icon.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                doOnTouchEvent(event, agentProfilesItem, position, imageName, propertyTypeName);
                return false;
            }
        });
        viewHolder.img_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToProfileFragment(agentProfilesItem);
            }
        });
        viewHolder.iv_thumbnail.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                doOnTouchEvent(event, agentProfilesItem, position, imageName, propertyTypeName);
                return false;
            }
        });
        viewHolder.iv_thumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToDetailFragment(agentProfilesItem);
            }
        });

        Log.d("checkIsFollowing", "checkIsFollowing: " + agentProfilesItem.memberName + " --> " + agentProfilesItem.isFollowing);

        //set blue border on profile pic
        if (agentProfilesItem.isFollowing==1) {
            viewHolder.img_icon.setImageBorder(Constants.STYLE_TYPE.AGENT_FOLLOWED);
            viewHolder.iv_miniview_icon.setImageBorder(Constants.STYLE_TYPE.AGENT_FOLLOWED);
        } else {
            viewHolder.img_icon.setImageBorder(Constants.STYLE_TYPE.AGENT_UNFOLLOWED);
            viewHolder.iv_miniview_icon.setImageBorder(Constants.STYLE_TYPE.AGENT_UNFOLLOWED);
        }

    }

    void doOnTouchEvent(MotionEvent event, AgentProfiles agentProfilesItem, int position, String imageName, String propertyTypeName) {
//        setSwipeable(true);
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            updateItem(agentProfilesItem, position, imageName, propertyTypeName);
        }
    }

    public void updateItem(AgentProfiles agentProfilesItem, int position, String imageName, String propertyTypeName) {

    }

    public void setType(MainActivityTabBase.CURRENT_SECTION SECTION, boolean isMiniview) {
        this.isMiniview = isMiniview;
    }

    public boolean getIsMiniview() {
        return isMiniview;
    }

    public void goToProfileFragment(AgentProfiles agentProfilesItem) {
    }

    public void goToDetailFragment(AgentProfiles agentProfilesItem) {
    }

    public int getMinivewHeight() {
        return minivewHeight;
    }

    @Override
    public int getBasicItemCount() {
        return agentProfilesList.size();
    }

    @Override
    public int getBasicItemType(int position) {
        return 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout ll_wholeItemLayout;

        public LinearLayout ll_miniview;
        public RoundedImageView iv_miniview_icon;
        public TextView iv_miniview_name;
        public ImageView iv_miniview_propertyType;
        public TextView tv_miniview_location;
        public TextView tv_miniview_numFollowers;

        public LinearLayout ll_normalview;
        public RelativeLayout rl_headerBar;
        public TextView tv_name;
        public TextView tv_numFollowers;
        public TextView tv_bubble;
        public RoundedImageView img_icon;
        public ImageView iv_thumbnail;
        public ImageView iv_propertyType;
        public TextView tv_propertyType;
//        public TextView tv_houseName;
        public TextView tv_houseAddress;
        public TextView tv_currencyUnit;
        public TextView tv_propertyPriceFormattedForRoman;

        public ViewHolder(View itemView) {
            super(itemView);
            ll_wholeItemLayout = (LinearLayout) itemView.findViewById(R.id.ll_wholeItemLayout);

            ll_miniview = (LinearLayout) itemView.findViewById(R.id.ll_miniview);
            iv_miniview_icon = (RoundedImageView) itemView.findViewById(R.id.iv_miniview_icon);
            iv_miniview_name = (TextView) itemView.findViewById(R.id.iv_miniview_name);
            iv_miniview_propertyType = (ImageView) itemView.findViewById(R.id.iv_miniview_propertyType);
            tv_miniview_location = (TextView) itemView.findViewById(R.id.tv_miniview_location);
            tv_miniview_numFollowers = (TextView) itemView.findViewById(R.id.tv_miniview_numFollowers);

            ll_normalview = (LinearLayout) itemView.findViewById(R.id.ll_normalview);
            rl_headerBar = (RelativeLayout) itemView.findViewById(R.id.rl_headerBar);
            tv_name = (TextView) itemView.findViewById(R.id.tv_name);
            tv_numFollowers = (TextView) itemView.findViewById(R.id.tv_numFollowers);
            tv_bubble = (TextView) itemView.findViewById(R.id.tv_bubble);
            img_icon = (RoundedImageView) itemView.findViewById(R.id.img_icon);
            iv_thumbnail = (ImageView) itemView.findViewById(R.id.iv_thumbnail);
            iv_propertyType = (ImageView) itemView.findViewById(R.id.iv_propertyType);
            tv_propertyType = (TextView) itemView.findViewById(R.id.tv_propertyType);
//            tv_houseName = (TextView) itemView.findViewById(R.id.tv_houseName);
            tv_houseAddress = (TextView) itemView.findViewById(R.id.tv_houseAddress);
            tv_currencyUnit = (TextView) itemView.findViewById(R.id.tv_currencyUnit);
            tv_propertyPriceFormattedForRoman = (TextView) itemView.findViewById(R.id.tv_propertyPriceFormattedForRoman);
        }
    }

    class Footer_ViewHolder extends RecyclerView.ViewHolder {

        public ProgressBar progressSpinner;

        public Footer_ViewHolder(View itemView) {
            super(itemView);
            progressSpinner= (ProgressBar) itemView.findViewById(R.id.progress_spinner);
        }
    }

    public void setAgentProfilesList(List<AgentProfiles> agentProfilesList) {
        this.agentProfilesList = agentProfilesList;
        notifyDataSetChanged();
    }



}
