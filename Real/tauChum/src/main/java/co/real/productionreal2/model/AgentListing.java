package co.real.productionreal2.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import co.real.productionreal2.service.model.request.RequestListing;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AgentListing extends RequestListing {

    @SerializedName("GoogleAddress")
    public List<GoogleAddress> googleAddresses;
    @SerializedName("Photos")
    public List<Photo> photos;
    @SerializedName("AgentListingID")
    public int listingID;
    @SerializedName("CreationDateString")
    public String creationDateString;

//	@SerializedName("SloganIndex")
//	public int sloganIndex;
//	@SerializedName("PropertyType")
//	public int propertyType;
//	@SerializedName("SpaceType")
//	public int spaceType;
//	@SerializedName("PropertyPrice")
//	public int propertyPrice;
//	@SerializedName("PropertySize")
//	public int propertySize;
//	@SerializedName("CurrencyUnit")
//	public String currency;
//	@SerializedName("SizeUnit")
//	public String sizeUnit;
//	@SerializedName("SizeUnitType")
//	public int sizeUnitType;
//	@SerializedName("BedroomCount")
//	public int bedroomCount;
//	@SerializedName("BathroomCount")
//	public int bathroomCount;
//	@SerializedName("BalconyCount")
//	public int balconyCount;
//	@SerializedName("KitchenCount")
//	public int kitchenCount;
//	@SerializedName("LivingRoomCount")
//	public int livingRoomCount;
//	@SerializedName("ListingFollowerCount")
//	public int listingFollowerCount;
//	@SerializedName("Reasons")
//	public List<Reason> reasons;
//
//	@SerializedName("AddressUserInput")
//	public List<AddressUserInput> addressInputs;
//
//	@SerializedName("PropertyPriceFormattedForRoman")
//	public String propertyPriceFormattedForRoman;
//	@SerializedName("PropertyPriceFormattedForChinese")
//	public String propertyPriceFormattedForChinese;
//	@SerializedName("PropertyPriceFormattedForIndian")
//	public String propertyPriceFormattedForIndian;


    public AgentListing(int memId, String session) {
        super(memId, session);
        googleAddresses = new ArrayList<GoogleAddress>();
        photos = new ArrayList<Photo>();

        reasons = new ArrayList<Reason>();
        addressInputs = new ArrayList<AddressUserInput>();
    }

    public AgentListing(Parcel source) {
        super(0, null);
        googleAddresses = new ArrayList<GoogleAddress>();
        photos = new ArrayList<Photo>();

        reasons = new ArrayList<Reason>();
        addressInputs = new ArrayList<AddressUserInput>();

        source.readTypedList(googleAddresses, GoogleAddress.CREATOR);
        source.readTypedList(photos, Photo.CREATOR);
        source.readTypedList(reasons, Reason.CREATOR);
        source.readTypedList(addressInputs, AddressUserInput.CREATOR);

        creationDateString = source.readString();
        listingID = source.readInt();
        sloganIndex = source.readInt();
        propertyType = source.readInt();
        spaceType = source.readInt();
//		propertyPrice = source.readInt();
        currency = source.readString();
        propertySize = source.readInt();
        sizeUnit = source.readString();
        sizeUnitType = source.readInt();
        bedroomCount = source.readInt();
        bathroomCount = source.readInt();
//		balconyCount = source.readInt();
//		kitchenCount = source.readInt();
//		livingRoomCount = source.readInt();
//		listingFollowerCount = source.readInt();

        propertyPriceFormattedForRoman = source.readString();
        propertyPriceFormattedForChinese = source.readString();
//		propertyPriceFormattedForIndian = source.readString();

    }


//    @Override
//    public void writeToParcel(Parcel dest, int flags) {
//        dest.writeTypedList(googleAddresses);
//        dest.writeTypedList(photos);
//        dest.writeInt(listingID);
//        dest.writeString(creationDateString);
//
//        dest.writeInt(sloganIndex);
//        dest.writeInt(propertyType);
//        dest.writeInt(spaceType);
////		dest.writeInt(propertyPrice);
//        dest.writeString(currency);
//        dest.writeInt(propertySize);
//        dest.writeString(sizeUnit);
//        dest.writeInt(sizeUnitType);
//        dest.writeInt(bedroomCount);
//        dest.writeInt(bathroomCount);
////		dest.writeInt(balconyCount);
////		dest.writeInt(kitchenCount);
////		dest.writeInt(livingRoomCount);
////		dest.writeInt(listingFollowerCount);
//
//        dest.writeString(propertyPriceFormattedForRoman);
//        dest.writeString(propertyPriceFormattedForChinese);
////		dest.writeString(propertyPriceFormattedForIndian);
//
//        dest.writeTypedList(reasons);
//        dest.writeTypedList(addressInputs);
//    }
//
//    @Override
//    public int describeContents() {
//        return 0;
//    }
//
//
//    public static final Parcelable.Creator<AgentListing> CREATOR = new Creator<AgentListing>() {
//        public AgentListing createFromParcel(Parcel source) {
//            return new AgentListing(source);
//        }
//
//        @Override
//        public AgentListing[] newArray(int size) {
//            return new AgentListing[size];
//        }
//    };


    public static class GoogleAddress implements Parcelable {

        @SerializedName("Lang")
        public String lang; //100000000

        @SerializedName("Name")
        public String name;

        @SerializedName("FormattedAddress")
        public String address;

        @SerializedName("PlaceID")
        public String placeID;

        @SerializedName("Location")
        public String location;

        public GoogleAddress(Parcel source) {
            lang = source.readString();
            name = source.readString();
            address = source.readString();
            placeID = source.readString();
            location = source.readString();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(lang);
            dest.writeString(address);
            dest.writeString(placeID);
            dest.writeString(location);
        }

        public final static Parcelable.Creator<GoogleAddress> CREATOR = new Creator<GoogleAddress>() {
            public GoogleAddress createFromParcel(Parcel source) {
                return new GoogleAddress(source);
            }

            @Override
            public GoogleAddress[] newArray(int size) {
                return new GoogleAddress[size];
            }
        };

    }


    public static class Photo implements Parcelable {

        @SerializedName("PhotoID")
        public int photoID;

        @SerializedName("Position")
        public int position;

        @SerializedName("URL")
        public String url;

        public Photo(Parcel source) {
            photoID = source.readInt();
            position = source.readInt();
            url = source.readString();
        }

        public Photo(int position, String url) {
            this.photoID=0;
            this.position = position;
            this.url = url;
        }

        public String getUrl() {
            return url;
        }


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(photoID);
            dest.writeInt(position);
            dest.writeString(url);

        }

        public static final Parcelable.Creator<Photo> CREATOR = new Creator<Photo>() {
            public Photo createFromParcel(Parcel source) {
                return new Photo(source);
            }

            @Override
            public Photo[] newArray(int size) {
                return new Photo[size];
            }
        };

        @Override
        public String toString() {
            return "Photo{" +
                    "photoID=" + photoID +
                    ", position=" + position +
                    ", url='" + url + '\'' +
                    '}';
        }
    }


    public Date getCreationDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(creationDateString);//"2016-01-27 09:48:45";
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return convertedDate;
    }

}
