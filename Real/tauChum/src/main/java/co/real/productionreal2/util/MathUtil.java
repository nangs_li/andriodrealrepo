package co.real.productionreal2.util;

import android.content.Context;
import android.util.Log;

import co.real.productionreal2.R;

import java.math.BigDecimal;

/**
 * Created by kelvinsun on 23/3/16.
 */
public class MathUtil {

    public static String getRomanFormat(int size) {
        if (size / (1000 ^ 2) > 1) {
            return String.format("%.1f", size / (1000 ^ 2)) + "M";
        } else if (size / (1000) > 1) {
            return String.format("%.1f", size / (1000)) + "K";
        } else {
            return String.valueOf(size);
        }
    }

    public static String getRomanFormat(BigDecimal number) {
        Log.d("getRomanFormat", "getRomanFormat: " + (number.divide(new BigDecimal(1000*1000), 1, BigDecimal.ROUND_HALF_EVEN)));
        if ((number.divide(new BigDecimal(1000*1000),1,BigDecimal.ROUND_HALF_EVEN)).doubleValue()>1) {
            return String.format("%.1f",number.divide(new BigDecimal(1000*1000),1,BigDecimal.ROUND_HALF_EVEN).doubleValue()) + "M";
        } else if ((number.divide(new BigDecimal(1000),1,BigDecimal.ROUND_HALF_EVEN)).doubleValue()>1) {
            return String.format("%.1f", number.divide(new BigDecimal(1000),1,BigDecimal.ROUND_HALF_EVEN).doubleValue()) + "K";
        } else {
            return String.valueOf(number);
        }
    }


    public static String getApproximatedValue(Context context, int index) {
        String approximateVaule;
        switch (index) {
            case 0:
                approximateVaule = context.getResources().getString(R.string.search_filter__above);
                break;
            case 1:
                approximateVaule = context.getResources().getString(R.string.search_filter__around);
                break;
            case 2:
                approximateVaule = context.getResources().getString(R.string.search_filter__below);
                break;
            default:
                approximateVaule = context.getResources().getString(R.string.search_filter__above);
                break;
        }
        return approximateVaule;
    }
}
