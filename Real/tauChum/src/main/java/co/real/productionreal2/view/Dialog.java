package co.real.productionreal2.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Build;
import android.view.View;
import android.widget.DatePicker;

import co.real.productionreal2.R;
import co.real.productionreal2.activity.profile.EditAgentProfileActivity;
import co.real.productionreal2.callback.DialogCallback;
import co.real.productionreal2.callback.DialogDualCallback;
import co.real.productionreal2.chat.ChatSessionUtil;
import co.real.productionreal2.login.AccountSetting;
import co.real.productionreal2.util.AppUtil;
import co.real.productionreal2.util.DBUtil;

import java.lang.reflect.Field;
import java.util.Date;
import java.util.Locale;

/**
 * Created by hohojo on 7/8/2015.
 */
public class Dialog {
    public static AlertDialog quitAppDialog(final Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(R.string.common__confirm_to_quit);
        builder.setCancelable(true);
        builder.setPositiveButton(android.R.string.yes,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ChatSessionUtil.logoutQBuser(activity);
                        dialog.cancel();
                        activity.finish();
                    }
                });
        builder.setNegativeButton(android.R.string.no,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        return builder.create();
    }

    public static AlertDialog confirmDelDialog(final Context context, String itemName, int profileType, final DialogCallback callback) {
//        String dialogMsg = context.getString(R.string.confirm_delete) + " " + itemName + " ";
        String dialogMsg = null;
        switch (profileType) {
            case EditAgentProfileActivity.EXP:
                dialogMsg = String.format(context.getString(R.string.edit_profile__confirm_to_delete_experience_desc), itemName);
                break;
            case EditAgentProfileActivity.LANG:
                dialogMsg = String.format(context.getString(R.string.alert_confirm_delete_language), itemName);
                break;
            case EditAgentProfileActivity.SPECICALTY:
                dialogMsg = String.format(context.getString(R.string.alert_confirm_delete_specialty), itemName);
                break;
            case EditAgentProfileActivity.TOP_5_PAST_CLOSING:
                dialogMsg = String.format(context.getString(R.string.edit_profile__confirm_to_delete_personal_best_closing_desc), itemName);
                break;
        }


        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(dialogMsg);
        builder.setCancelable(true);
        builder.setPositiveButton(android.R.string.yes,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        callback.yes();
                        dialog.cancel();
                    }
                });
        builder.setNegativeButton(android.R.string.no,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        return builder.create();
    }

    /**
     * change phone number checking logic
     * @return
     */
    public static AlertDialog changePhoneNoSuccessDialog(final Context context, String phoneNumOld,String phoneNum,final DialogCallback callback) {
        String dialogTitle= context.getString(R.string.invite_to_chat__success);
        String dialogMsg = String.format(context.getString(R.string.change_phone_no__change_number_success_msg), phoneNumOld,phoneNum);

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(dialogTitle);
        builder.setMessage(dialogMsg);
        builder.setCancelable(true);
        builder.setPositiveButton(android.R.string.ok,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        callback.yes();
                        dialog.cancel();
                    }
                });
        return builder.create();
    }

    /**
     * change phone number checking logic
     * @return
     */
    public static AlertDialog confirmChangePhoneNoDialog(final Context context, String phoneNo, final DialogCallback callback) {
        String dialogTitle= context.getString(R.string.change_phone_no__confirm_number_title);
        String dialogMsg = String.format(context.getString(R.string.change_phone_no__confirm_number_msg), phoneNo);

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(dialogTitle);
        builder.setMessage(dialogMsg);
        builder.setCancelable(true);
        builder.setPositiveButton(android.R.string.ok,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        callback.yes();
                        dialog.cancel();
                    }
                });
        builder.setNegativeButton(R.string.common__edit,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        return builder.create();
    }

    public static AlertDialog noAddressDialog(final Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(R.string.more_about_your_listing__confirm_address);
        builder.setCancelable(true);
        builder.setPositiveButton(android.R.string.ok,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        return builder.create();
    }

    public static AlertDialog noReturnAddressDialog(final Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(R.string.chat_lobby_search__no_results);
        builder.setCancelable(true);
        builder.setPositiveButton(android.R.string.ok,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        return builder.create();
    }

    public static AlertDialog noPhotoDialog(final Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(R.string.create_listing__please_select_photos);
        builder.setCancelable(true);
        builder.setPositiveButton(android.R.string.ok,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        return builder.create();
    }


    public static AlertDialog accessErrorForceLogoutDialog(final Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(R.string.error_message__another_account_logged_into_app);
        builder.setCancelable(false);
        builder.setPositiveButton(android.R.string.ok,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        AccountSetting.logout(context);
                    }
                });
        AccountSetting.clearDataBeforeLogoutAction(context);

        return builder.create();
    }

    public static AlertDialog suspendedErrorForceLogoutDialog(final Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(R.string.alert_account_suspended);
        builder.setCancelable(false);
        builder.setPositiveButton(android.R.string.ok,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        AccountSetting.logout(context);
                        Intent i = new Intent(Intent.ACTION_SEND);
                        i.setType("message/rfc822");
                        i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"team@real.co"});
                        try {
                            context.startActivity(Intent.createChooser(i, "Send mail..."));
                        } catch (android.content.ActivityNotFoundException ex) {
                        }
                    }
                });

        builder.setPositiveButton(android.R.string.cancel,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        AccountSetting.logout(context);
                    }
                });
        AccountSetting.clearDataBeforeLogoutAction(context);

        return builder.create();
    }

    public static AlertDialog settingLogoutDialog(final Context context) {
        //edwin temp add to test DB
        DBUtil.copyDB(context);

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(R.string.setting__confirm_logout);
        builder.setCancelable(true);
        builder.setPositiveButton(R.string.common__log_out,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        AccountSetting.clearDataBeforeLogoutAction(context);
                        AccountSetting.logout(context);
                    }
                });
        builder.setNegativeButton(android.R.string.no,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        return builder.create();

    }

    public static AlertDialog noNetworkDialog(final Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(R.string.error_message__network_failure);
        builder.setCancelable(true);
        builder.setPositiveButton(android.R.string.ok,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        return builder.create();
    }

    public static AlertDialog unexitChatDialog(final Context context, String opponentName, final DialogCallback callback) {
        String dialogMsg = "Enter Conversation: " + opponentName;


        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(dialogMsg);
        builder.setCancelable(true);
        builder.setPositiveButton("Unexit Chat",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        callback.yes();
                        dialog.cancel();
                    }
                });
        builder.setNegativeButton(android.R.string.cancel,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        return builder.create();
    }

    public static AlertDialog unblockChatDialog(final Context context, String opponentName, final DialogCallback callback) {
//        String dialogMsg = context.getString(R.string.block__confirm_unblock_box) + ": " + opponentName;
        String dialogMsg = context.getString(R.string.alert_confirm_unblock_title);


        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(dialogMsg);
        builder.setCancelable(true);
        builder.setPositiveButton(R.string.common__unblock,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        callback.yes();
                        dialog.cancel();
                    }
                });
        builder.setNegativeButton(android.R.string.cancel,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        return builder.create();
    }

    public static AlertDialog blockChatDialog(final Context context, String opponentName, final DialogCallback callback) {
        String dialogMsg = context.getString(R.string.chatroom__block_confirmation_box);

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(dialogMsg);
        builder.setCancelable(true);
        builder.setPositiveButton(R.string.chatroom__block_this_contact,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        callback.yes();
                        dialog.cancel();
                    }
                });
        builder.setNegativeButton(android.R.string.cancel,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        return builder.create();
    }


    public static AlertDialog unfollowDialog(final Context context,String name, final DialogCallback callback) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(context.getString(R.string.agent_profile__unfollow_button) + " " + name + "?");
        builder.setCancelable(true);
        builder.setPositiveButton(R.string.agent_profile__unfollow_button,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        callback.yes();
                        dialog.cancel();
                    }
                });
        builder.setNegativeButton(android.R.string.cancel,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        return builder.create();
    }

    public static AlertDialog noEmailDialog(final Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(R.string.error_message__email_cannot_be);
        builder.setCancelable(true);
        builder.setPositiveButton(android.R.string.ok,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        return builder.create();
    }

    public static AlertDialog noFillInDialog(final Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(R.string.common__please_fill_information);
        builder.setCancelable(true);
        builder.setPositiveButton(android.R.string.ok,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        return builder.create();
    }

    public static AlertDialog pwNotValidDialog(final Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(R.string.error_message__password_must_contain);
        builder.setCancelable(true);
        builder.setPositiveButton(android.R.string.ok,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        return builder.create();
    }


    public static AlertDialog emailNotValidDialog(final Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(R.string.error_message__please_enter_valid);
        builder.setCancelable(true);
        builder.setPositiveButton(android.R.string.ok,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        return builder.create();
    }

    public static AlertDialog realNetworkMemRegFailDialog(final Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(R.string.error_message__network_failure);
        builder.setCancelable(true);
        builder.setPositiveButton(android.R.string.ok,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        return builder.create();
    }

    public static AlertDialog uploadUserPhotoFileFailDialog(final Activity activity, final DialogCallback callback) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(R.string.error_message__network_failure);
        builder.setCancelable(false);
        builder.setPositiveButton(android.R.string.ok,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        callback.yes();
                        dialog.cancel();
                    }
                });

        return builder.create();
    }

    /**
     * reminder to save before exit
     * @param activity
     * @param callback
     * @return
     */
    public static AlertDialog exitSaveChangeDialog(final Activity activity, final DialogDualCallback callback) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(R.string.common__discard_change);
        builder.setCancelable(true);
        builder.setPositiveButton(R.string.common__discard,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        callback.positive();
                        dialog.cancel();
                    }
                });
        builder.setNegativeButton(R.string.common__save, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                callback.negative();
                dialog.cancel();
            }
        });

        return builder.create();
    }


    public static AlertDialog normalDialog(final Activity activity, String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(msg);
        builder.setCancelable(true);
        builder.setPositiveButton(android.R.string.yes, null);
        return builder.create();
    }

    public static AlertDialog normalDialog(final Activity activity, String title, String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(title);
        builder.setMessage(msg);
        builder.setCancelable(true);
        builder.setNeutralButton(android.R.string.yes, null);
        return builder.create();
    }

    public static AlertDialog feedbackDialog(final Activity activity, boolean isCancelable, String title, String msg, final DialogCallback callback) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(title);
        builder.setMessage(msg);
        builder.setCancelable(isCancelable);
        builder.setPositiveButton(android.R.string.ok,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        callback.yes();
                        dialog.cancel();
                    }
                });

        return builder.create();
    }


    public static AlertDialog updateProfileDialog(final Activity activity, boolean isCancelable, String title, String msg, final DialogCallback callback) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("");
        builder.setMessage(msg);
        builder.setCancelable(isCancelable);
        builder.setPositiveButton(R.string.common__update,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        callback.yes();
                        dialog.cancel();
                    }
                });

        return builder.create();
    }

    public static AlertDialog clearChatDialog(final Activity activity, final DialogCallback callback) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(R.string.chatroom__clear_chat);
        builder.setCancelable(true);
        builder.setPositiveButton(android.R.string.ok,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        callback.yes();
                        dialog.cancel();
                    }
                });
        builder.setNegativeButton(android.R.string.no,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        return builder.create();
    }

    public static DatePickerDialog datePickerDialogWithoutDay(Context context, DatePickerDialog.OnDateSetListener listener, int year,
                                                              int monthOfYear) {
        //set current locale
        Locale locale = AppUtil.getCurrentLangLocale(context);
        Locale.setDefault(locale);

        DatePickerDialog dpd = new DatePickerDialog(context, AlertDialog.THEME_HOLO_LIGHT, listener, year, monthOfYear, 1);
        dpd.getDatePicker().setMaxDate(new Date().getTime());
        dpd.setTitle("");
        DatePicker dp_mes = dpd.getDatePicker();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int daySpinnerId = Resources.getSystem().getIdentifier("day", "id", "android");
            if (daySpinnerId != 0) {
                View daySpinner = dp_mes.findViewById(daySpinnerId);
                if (daySpinner != null) {
                    daySpinner.setVisibility(View.GONE);
                }
            }

            int monthSpinnerId = Resources.getSystem().getIdentifier("month", "id", "android");
            if (monthSpinnerId != 0) {
                View monthSpinner = dp_mes.findViewById(monthSpinnerId);
                if (monthSpinner != null) {
                    monthSpinner.setVisibility(View.VISIBLE);
                }
            }

            int yearSpinnerId = Resources.getSystem().getIdentifier("year", "id", "android");
            if (yearSpinnerId != 0) {
                View yearSpinner = dp_mes.findViewById(yearSpinnerId);
                if (yearSpinner != null) {
                    yearSpinner.setVisibility(View.VISIBLE);
                }
            }
        } else { //Older SDK versions
            Field f[] = dp_mes.getClass().getDeclaredFields();
            for (Field field : f) {
                if (field.getName().equals("mDayPicker") || field.getName().equals("mDaySpinner")) {
                    field.setAccessible(true);
                    Object dayPicker = null;
                    try {
                        dayPicker = field.get(dp_mes);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                    ((View) dayPicker).setVisibility(View.GONE);
                }

                if (field.getName().equals("mMonthPicker") || field.getName().equals("mMonthSpinner")) {
                    field.setAccessible(true);
                    Object monthPicker = null;
                    try {
                        monthPicker = field.get(dp_mes);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                    ((View) monthPicker).setVisibility(View.VISIBLE);
                }

                if (field.getName().equals("mYearPicker") || field.getName().equals("mYearSpinner")) {
                    field.setAccessible(true);
                    Object yearPicker = null;
                    try {
                        yearPicker = field.get(dp_mes);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                    ((View) yearPicker).setVisibility(View.VISIBLE);
                }
            }
        }
        return dpd;
    }
}
