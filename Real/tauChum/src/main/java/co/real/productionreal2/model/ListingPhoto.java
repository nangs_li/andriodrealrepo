package co.real.productionreal2.model;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class ListingPhoto {

	@SerializedName("PhotoID")
	public int photoID; 

	@SerializedName("Position")
	public int position; 
	
	@SerializedName("URL")
	public String url;

	public String localPath;
	
	
	public ListingPhoto(int position) {
		super();
		this.position = position;
	}




	public static List<ListingPhoto> get10Objects() {
		
		
		List<ListingPhoto> photoList = new ArrayList<ListingPhoto>(10);
		
		for ( int i = 0 ;i  < 9 ; i ++){
			photoList.add(new ListingPhoto ( i));
		}
		
		return photoList;
	}
	
	
	
	
}
