package co.real.productionreal2.activity.setting;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.BaseActivity;
import co.real.productionreal2.dao.account.DBLogInfo;
import co.real.productionreal2.daomanager.DatabaseManager;
import co.real.productionreal2.util.AppUtil;
import co.real.productionreal2.util.FileUtil;

public class MetricSettingActivity extends BaseActivity {
    @InjectView(R.id.headerLayout)
    View headerLayout;
    @InjectView(R.id.tv_metric)
    TextView tvMetric;
    int langIndex = 0;
    private DBLogInfo dbLogInfo;
    private int selectedMetric = 0;
    private AlertDialog mConfirmDialog;

    //declare language index
    List<String> metricNameList;
    List<Integer> metricIndexList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_metric_setting);
        ButterKnife.inject(this);
        dbLogInfo = DatabaseManager.getInstance(this).getLogInfo();

        initData();
        initView();
    }

    private void initData() {
        langIndex = AppUtil.getCurrentLangIndex(this);
        metricNameList = FileUtil.getSystemSetting(this).obtainMetricNameList(langIndex);
        metricIndexList = FileUtil.getSystemSetting(this).obtainMetricIndexList(langIndex);
        if (dbLogInfo!=null)
        selectedMetric= AppUtil.getMeticPositionFromIndex(metricIndexList, dbLogInfo.getMetricIndex() == null ? 0 : dbLogInfo.getMetricIndex());
        tvMetric.setText(metricNameList.get(selectedMetric));
    }

    private void initView() {
        TextView titleTextView = (TextView) headerLayout.findViewById(R.id.titleTextView);
        titleTextView.setText(R.string.common__unit);
    }

    private void openMetricUnitDialog() {
        final ArrayAdapter<String> metricList = new ArrayAdapter<>(this, android.R.layout.simple_list_item_checked, metricNameList );
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this)
                .setSingleChoiceItems(metricList, selectedMetric, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        tvMetric.setText(metricNameList.get(i));
                        dbLogInfo.setMetricIndex(AppUtil.getMetricIndexFromName(MetricSettingActivity.this,metricNameList.get(i)));
                        DatabaseManager.getInstance(MetricSettingActivity.this).updateLogInfo(dbLogInfo);
                        selectedMetric=AppUtil.getMeticPositionFromIndex(metricIndexList, dbLogInfo.getMetricIndex());
                        dialogInterface.dismiss();
                    }
                });

        mConfirmDialog = dialogBuilder.create();
        mConfirmDialog.show();
    }


    @OnClick(R.id.tv_metric)
    public void metricBtnClick(View view) {
        openMetricUnitDialog();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
