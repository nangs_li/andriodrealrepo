package co.real.productionreal2.util;

import com.quickblox.chat.model.QBDialog;

/**
 * Created by hohojo on 20/7/2015.
 */
public class QBUtil {

    public static Integer getOpponentIDForPrivateDialog(QBDialog dialog, String qbId){
        Integer opponentID = -1;
        try {
            for(Integer userID : dialog.getOccupants()){
                if(!userID.equals(Integer.parseInt(qbId))){
                    opponentID = userID;
                    break;
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return  opponentID;
    }
}
