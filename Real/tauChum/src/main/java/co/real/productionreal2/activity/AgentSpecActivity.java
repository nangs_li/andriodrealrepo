package co.real.productionreal2.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import co.real.productionreal2.Constants;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.profile.VertHoriLinearLayout;
import co.real.productionreal2.callback.LangSpecDelCallback;
import co.real.productionreal2.service.model.AgentSpecialty;
import co.real.productionreal2.util.ImageUtil;
import co.real.productionreal2.view.Dialog;

/**
 * Created by hohojo on 11/9/2015.
 */
public class AgentSpecActivity extends BaseActivity {
    @InjectView(R.id.rootView)
    View rootView;
    @InjectView(R.id.rootSpecLinearlayout)
    LinearLayout rootSpecLinearlayout;
    @InjectView(R.id.specEditText)
    EditText specEditText;
    private ArrayList<String> agentSpecStringList;
    //    private ArrayList<AgentSpecialty> agentSpecList;
    private VertHoriLinearLayout vertHoriLinearLayout;
    private ArrayList<AgentSpecialty> beforeAgentSpecList;
//    private int totalWidth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agent_specialty);

        ButterKnife.inject(this);
        initView();

        vertHoriLinearLayout = new VertHoriLinearLayout(this, rootSpecLinearlayout, agentSpecStringList,
                ImageUtil.getScreenWidth(this), VertHoriLinearLayout.SPEC, new LangSpecDelCallback() {
            @Override
            public void onLangSpecDel(int tagNo, int type) {
                agentSpecStringList = vertHoriLinearLayout.removeViewAndGetContentList(tagNo);
            }
        });
    }

    private void initView() {
        Intent intent = getIntent();
        if (intent.getParcelableArrayListExtra(Constants.EXTRA_AGENT_SPEC_LIST) != null) {
            beforeAgentSpecList = intent.getParcelableArrayListExtra(Constants.EXTRA_AGENT_SPEC_LIST);
            agentSpecStringList = VertHoriLinearLayout.agentSpecListToStringList(beforeAgentSpecList);
        } else {
            agentSpecStringList = new ArrayList<>();
        }


        specEditText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    //todo add text in
                    if (!specEditText.getText().toString().trim().isEmpty()) {
                        agentSpecStringList = vertHoriLinearLayout.addViewAndGetContentList(specEditText.getText().toString());
                    }else{
                      //do nothing for empty string
                    }
                    specEditText.setText("");
                    return true;
                }
                return false;
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @OnClick(R.id.backBtn)
    public void closeBtn(View view) {
        setResult(RESULT_CANCELED);
        finish();
    }

    @OnClick(R.id.saveBtn)
    public void saveBtn(View view) {
        if (agentSpecStringList.size() != 0) {
            Intent intent = new Intent();
            intent.putExtra(Constants.EXTRA_AGENT_SPEC_LIST, VertHoriLinearLayout.stringListToAgentSpecList(agentSpecStringList));
            intent.putExtra(Constants.EXTRA_AGENT_SPEC_DEL_LIST, getAgentSpecDelList());
            intent.putExtra(Constants.EXTRA_AGENT_SPEC_ADD_LIST, getAgentSpecAddList());

            setResult(RESULT_OK, intent);
            finish();
        } else {
            Dialog.normalDialog(this, getString(R.string.create_profile__what_specialty)).show();
        }

    }
    private ArrayList<AgentSpecialty> getAgentSpecAddList() {
        ArrayList<AgentSpecialty> agentSpecAddList = new ArrayList<>();
        ArrayList<AgentSpecialty> currentAgentSpecList = VertHoriLinearLayout.stringListToAgentSpecList(agentSpecStringList);

        for (int i = 0; i < currentAgentSpecList.size(); i++) {
            if (!beforeAgentSpecList.contains(currentAgentSpecList.get(i)))
                agentSpecAddList.add(currentAgentSpecList.get(i));
        }
        return agentSpecAddList;
    }
    private ArrayList<AgentSpecialty> getAgentSpecDelList() {
        ArrayList<AgentSpecialty> agenSpecDelList = new ArrayList<>();
        ArrayList<AgentSpecialty> currentAgentSpecList = VertHoriLinearLayout.stringListToAgentSpecList(agentSpecStringList);

        for (int i = 0; i < beforeAgentSpecList.size(); i++) {
            if (!currentAgentSpecList.contains(beforeAgentSpecList.get(i)))
                agenSpecDelList.add(beforeAgentSpecList.get(i));
        }
        return agenSpecDelList;
    }

}
