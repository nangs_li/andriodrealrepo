package co.real.productionreal2.model;

/**
 * Created by hohojo on 18/1/2016.
 */
public class SystemMsgError {
    public SystemMsgError(String title, String message, String cancelActionTitle, String cancelActionURL, String goActionTitle, String goActionURL) {
        this.title = title;
        this.message = message;
        this.cancelActionTitle = cancelActionTitle;
        this.cancelActionURL = cancelActionURL;
        this.goActionTitle = goActionTitle;
        this.goActionURL = goActionURL;
    }
    public String title;
    public String message;
    public String cancelActionTitle;
    public String cancelActionURL;
    public String goActionTitle;
    public String goActionURL;
}
