package co.real.productionreal2.service.model.request;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import co.real.productionreal2.R;
import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.model.InputRequest;
import co.real.productionreal2.service.AdminService;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.response.ResponseBase;
import co.real.productionreal2.util.AppUtil;
import co.real.productionreal2.util.NetworkUtil;
import co.real.productionreal2.view.Dialog;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class RequestPhoneNumberMigrate extends RequestBase {
    public RequestPhoneNumberMigrate(int memberID, String accessToken, String countryCode,
                                     String phoneNumber, String lang) {
        super(AppUtil.getUniqueKey(), memberID, accessToken);
        this.countryCode = countryCode;
        this.phoneNumber = phoneNumber;
        this.lang = lang;
    }

    @SerializedName("CountryCode")
    public String countryCode;
    @SerializedName("PhoneNumber")
    public String phoneNumber;
    @SerializedName("Lang")
    public String lang;

    public void callPhoneNumberMigrateApi(final Context context, final ApiCallback callback) {
        String json = new Gson().toJson(this);
        Log.d("callPhoneNumberMigrate", "callPhoneNumberMigrateApi = " + json);
        final AdminService weatherService = new AdminService(context);
        weatherService.getCoffeeService().phoneNumberMigrate(
                new InputRequest(json),
                new retrofit.Callback<ApiResponse>() {
                    @Override
                    public void failure(RetrofitError error) {
                        callback.failure(error.getMessage());
                        NetworkUtil.showNoNetworkMsg(context);
                    }

                    @Override
                    public void success(ApiResponse apiResponse, Response response) {
                        ResponseBase responseGson = new Gson().fromJson(apiResponse.jsonContent, ResponseBase.class);
                        Log.w("success", "responseGson = " + responseGson);
                        if (responseGson.errorCode == 4) {
                            Dialog.accessErrorForceLogoutDialog(context).show();
                        } else if (responseGson.errorCode == 26) {
                            Dialog.suspendedErrorForceLogoutDialog(context).show();
                        } else if (responseGson.errorCode == 105) {
                            callback.failure(context.getString(R.string.error_message__old_phone_num_not_match));
                        } else if (responseGson.errorCode == 106) {
                            callback.failure(context.getString(R.string.error_message__phone_num_duplicated));
                        } else {
                            callback.success(apiResponse);
                        }
                    }
                }
        );
    }
}
