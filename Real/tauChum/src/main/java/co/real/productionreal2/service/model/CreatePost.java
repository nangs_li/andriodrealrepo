package co.real.productionreal2.service.model;

import co.real.productionreal2.service.model.request.RequestListing;

/**
 * Created by hohojo on 22/10/2015.
 */
public class CreatePost {
    public CreatePost(int langIndex) {
        this.langIndex = langIndex;
    }

    public int langIndex;
    public RequestListing createPostRequest;
    public String reason1ImagePath;
    public String reason2ImagePath;
    public String reason3ImagePath;

}
