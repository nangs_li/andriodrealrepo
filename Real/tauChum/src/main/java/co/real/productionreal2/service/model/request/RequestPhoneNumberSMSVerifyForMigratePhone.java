package co.real.productionreal2.service.model.request;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import co.real.productionreal2.R;
import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.model.InputRequest;
import co.real.productionreal2.service.AdminService;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.response.ResponseBase;
import co.real.productionreal2.util.AppUtil;
import co.real.productionreal2.util.NetworkUtil;
import co.real.productionreal2.view.Dialog;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class RequestPhoneNumberSMSVerifyForMigratePhone extends RequestBase {
    public RequestPhoneNumberSMSVerifyForMigratePhone(int memberID, String accessToken, int phoneRegID,
                                                      String pinInput) {
        super(AppUtil.getUniqueKey(),memberID, accessToken);
        this.phoneRegID=phoneRegID;
        this.pinInput=pinInput;
    }
    @SerializedName("PhoneRegID")
    public int phoneRegID;
    @SerializedName("PINInput")
    public String pinInput;

    public void callRequestPhoneNumberSMSVerifyForMigratingPhoneApi(final Context context, final ApiCallback callback) {
        String json = new Gson().toJson(this);
        Log.d("callVerifyForMigrater", "callRequestPhoneNumberSMSVerifyForMigratingPhoneApi = " + json);
        final AdminService weatherService = new AdminService(context);
        weatherService.getCoffeeService().phoneNumberSMSVerifyForMigratingPhone(
                new InputRequest(json),
                new retrofit.Callback<ApiResponse>() {
                    @Override
                    public void failure(RetrofitError error) {
                        callback.failure("");
                        NetworkUtil.showNoNetworkMsg(context);
                    }

                    @Override
                    public void success(ApiResponse apiResponse, Response response) {
                        ResponseBase responseGson = new Gson().fromJson(apiResponse.jsonContent, ResponseBase.class);
                        Log.w("success", "responseGson = " + responseGson);
                        if (responseGson.errorCode == 4) {
                            Dialog.accessErrorForceLogoutDialog(context).show();
                        } else if (responseGson.errorCode == 26) {
                            Dialog.suspendedErrorForceLogoutDialog(context).show();
                        } else if (responseGson.errorCode == 110) {
                            callback.failure(context.getString(R.string.error_message__sms_pin_incorrect));
                        } else if (responseGson.errorCode == 111) {
                            callback.failure(context.getString(R.string.error_message__sms_pin_expired));
                        } else {
                            callback.success(apiResponse);
                        }
                    }
                }
        );
    }
}
