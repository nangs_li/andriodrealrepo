package co.real.productionreal2.service.model.response;

import com.google.gson.annotations.SerializedName;

public class ResponsePhoneNumberRegEX extends ResponseBase{
    @SerializedName("Content")
    public Content content;

    public class Content {
        @SerializedName("RegType")
        public int regType;
        @SerializedName("PhoneRegID")
        public int phoneRegID;
    }
}
