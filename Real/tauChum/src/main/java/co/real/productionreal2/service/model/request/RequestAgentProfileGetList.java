package co.real.productionreal2.service.model.request;

import com.google.gson.annotations.SerializedName;

public class RequestAgentProfileGetList extends RequestBase {

    public RequestAgentProfileGetList(int memId, String session) {
        super(memId, session);
    }


    @SerializedName("QBIDList")
    public String[] qbIds;


}