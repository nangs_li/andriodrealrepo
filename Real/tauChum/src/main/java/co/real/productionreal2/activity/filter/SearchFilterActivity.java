package co.real.productionreal2.activity.filter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import co.real.productionreal2.Constants;
import co.real.productionreal2.Navigation;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.BaseActivity;
import co.real.productionreal2.model.SystemSetting;
import co.real.productionreal2.setting.SearchFilterSetting;
import co.real.productionreal2.util.AppUtil;
import co.real.productionreal2.util.FileUtil;
import co.real.productionreal2.util.ImageUtil;
import co.real.productionreal2.util.MathUtil;
import co.real.productionreal2.view.MultiPartInputField;
import co.real.productionreal2.view.RealTwoSidesButtonView;
import co.real.productionreal2.view.SpaceTypeItem;
import lib.blurbehind.OnBlurCompleteListener;

public class SearchFilterActivity extends BaseActivity implements View.OnClickListener {
    private static final String TAG = "SearchFilterActivity";
    public static Context context;

    @InjectView(R.id.btn_close)
    protected ImageView btnClose;
    @InjectView(R.id.btn_bedroom)
    protected RealTwoSidesButtonView btnBedroom;
    @InjectView(R.id.btn_bathroom)
    protected RealTwoSidesButtonView btnBathroom;
    @InjectView(R.id.input_price)
    protected MultiPartInputField inputPrice;
    @InjectView(R.id.input_size)
    protected MultiPartInputField inputSize;
    @InjectView(R.id.tv_spoken_language)
    protected TextView tvLanguage;
    @InjectView(R.id.ll_space_type)
    protected LinearLayout llSpaceType;
    @InjectView(R.id.btn_apply_filter)
    protected TextView btnApply;
    @InjectView(R.id.btn_reset)
    protected TextView btnReset;
    @InjectView(R.id.horizontalScrollView)
    protected HorizontalScrollView horizontalScrollView;


//    private ResponseLoginSocial.Content userContent;

    private Dialog mConfirmDialog;

    private ArrayList<SystemSetting.SpokenLanguage> spokenLangList;
    private String[] languageArray;

    private SearchFilterSetting searchFilterSetting = SearchFilterSetting.getInstance();
    private int langIndex;
    private List<String> spaceTypeIndexList;


    public static void start(final Context context) {
        ImageUtil.getInstance().prepareBlurImageFromActivity((Activity) context, SearchFilterActivity.class, new OnBlurCompleteListener() {
            @Override
            public void onBlurComplete() {
                Intent intent = new Intent(context, SearchFilterActivity.class);
                Navigation.presentIntentForResult((Activity) context,intent, Constants.SEARCH_FILTER);
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_filter);
//        AppUtil.setBlurBehind(this);
        ButterKnife.inject(this);
        initView();
        addListener();
        new Handler().postDelayed(
                new Runnable() {
                    @Override
                    public void run() {
                        // Code here will run in UI thread
                        horizontalScrollView.scrollTo(horizontalScrollView.getRight(),0);

                        new Handler().postDelayed(
                                new Runnable() {
                                    @Override
                                    public void run() {
                                        if(horizontalScrollView != null) {
                                            horizontalScrollView.smoothScrollTo(0, 0);
                                        }
                                    }
                                }, 500);
                    }
                }, 100);
    }

    private void addListener() {
        btnClose.setOnClickListener(this);
        tvLanguage.setOnClickListener(this);
        btnApply.setOnClickListener(this);
        btnReset.setOnClickListener(this);

        inputPrice.getEtDigit().setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    AppUtil.hideKeyboard(SearchFilterActivity.this, inputPrice.getEtDigit());
                }else{
                    AppUtil.showKeyboard(SearchFilterActivity.this, inputPrice.getEtDigit());
                }
                inputPrice.setSelected(hasFocus);
                inputPrice.getTvApproximation().setSelected(hasFocus);
            }
        });

        inputSize.getEtDigit().setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    AppUtil.hideKeyboard(SearchFilterActivity.this, inputSize.getEtDigit());
                }else{
                    AppUtil.showKeyboard(SearchFilterActivity.this, inputSize.getEtDigit());
                }
                inputSize.setSelected(hasFocus);
                inputSize.getTvApproximation().setSelected(hasFocus);
            }
        });
    }

    private void initView() {
        spokenLangList = new ArrayList<>();
        spokenLangList.addAll(FileUtil.getSystemSetting(this).spokenLangList);
        languageArray = new String[spokenLangList.size() + 1];
        languageArray[0] = "Any";
        //sort the spoken language list by POSITION
        Collections.sort(spokenLangList, new SystemSetting.SpokenLanguage.CompPosition());
        for (int i = 1; i <= spokenLangList.size(); i++) {
            languageArray[i] = spokenLangList.get(i - 1).nativeName;
        }
        langIndex = searchFilterSetting.getSpokenLangIndex();
//        dataObjects = context.getResources().getStringArray(R.array.space_type_array);
        final List<SystemSetting.SpaceType> spaceTypeList = FileUtil.getAllSpaceType(this, AppUtil.getCurrentLangIndex(this));
        spaceTypeIndexList = new ArrayList<>();
        spaceTypeIndexList.addAll(searchFilterSetting.getSpaceTypeIndexList());
        Log.d("spaceTypeIndexList", "spaceTypeIndexList: " + spaceTypeIndexList.toString());
        llSpaceType.removeAllViews();

        for (SystemSetting.SpaceType spaceType : spaceTypeList) {
            final SpaceTypeItem spaceTypeItem = new SpaceTypeItem(this, spaceType);
            spaceTypeItem.setTag(spaceType.propertyTypeIndex + "," + spaceType.index);

            spaceTypeItem.updateUI(spaceType);

            //to check if it is checked in the previous filter
            boolean isSelected = false;
            for (String s : spaceTypeIndexList) {
                //to read the index format
                String[] split = s.split(",");
                if (spaceType.propertyTypeIndex == Integer.parseInt(split[0]) && spaceType.index == Integer.parseInt(split[1])) {
                    isSelected = true;
                }
            }
            spaceTypeItem.setSelected(isSelected);

            spaceTypeItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    v.setSelected(!v.isSelected());
                    if (v.isSelected()) {
                        spaceTypeIndexList.add((String) v.getTag());
                    } else {
                        spaceTypeIndexList.remove((String) v.getTag());
                    }
                    Log.d("spaceTypeIndexList", "spaceTypeIndexList: " + spaceTypeIndexList.toString());
                }
            });

            llSpaceType.addView(spaceTypeItem);
        }

        inputPrice.initType(MultiPartInputField.FIELD_TYPE.PRICE, searchFilterSetting.getCurrencyUnit(), -1, true);
        inputSize.initType(MultiPartInputField.FIELD_TYPE.SIZE, -1, searchFilterSetting.getSizeUnitType(), true);
        inputPrice.getEtDigit().setText(String.valueOf(searchFilterSetting.getPrice()));
        inputSize.getEtDigit().setText(String.valueOf(searchFilterSetting.getSize()));
        tvLanguage.setText(langIndex < 1 ? getString(R.string.common__any): spokenLangList.get(langIndex - 1).nativeName);

        inputPrice.getTvApproximation().setText(MathUtil.getApproximatedValue(SearchFilterActivity.this,searchFilterSetting.getPriceApproximation()));
        inputSize.getTvApproximation().setText(MathUtil.getApproximatedValue(SearchFilterActivity.this,searchFilterSetting.getSizeApproximation()));

        btnBathroom.setValue(searchFilterSetting.getBathroomCount());
        btnBedroom.setValue(searchFilterSetting.getBedroomCount());

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.reset(this);
    }

    @Override
    public void onClick(View v) {
        if (v == btnClose) {
            finish();
        } else if (v == tvLanguage) {
            openLanguageDialog();
        } else if (v == btnApply) {
            //save preferences
            savePreferences();
            setResult(RESULT_OK);
            finish();
        } else if (v == btnReset) {
            resetForm();
        }
    }

    private void savePreferences() {
        searchFilterSetting.setPrice(inputPrice.getValueDecimal());
        searchFilterSetting.setPriceMax(inputPrice.getMaxValueDecimal());
        searchFilterSetting.setPriceMin(inputPrice.getMinValue());
        searchFilterSetting.setSize(inputSize.getValue());
        searchFilterSetting.setSizeMax(inputSize.getMaxValue());
        searchFilterSetting.setSizeMin(inputSize.getMinValue());
        searchFilterSetting.setBedroomCount(btnBedroom.getCount());
        searchFilterSetting.setBathroomCount(btnBathroom.getCount());
        searchFilterSetting.setSpokenLangIndex(langIndex);
        searchFilterSetting.setSizeUnitType(inputSize.getSelectedMetric());
        searchFilterSetting.setCurrencyUnit(inputPrice.getSelectedCurrency());
        searchFilterSetting.setPriceApproximation(inputPrice.getCurrentApproximateValue());
        searchFilterSetting.setSizeApproximation(inputSize.getCurrentApproximateValue());
        searchFilterSetting.setSpaceTypeIndexList(spaceTypeIndexList);
    }


    private void openLanguageDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this)
                .setItems(languageArray, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        tvLanguage.setText(languageArray[i]);
                        langIndex = (i > 0 ? i : -1);
                    }
                });
//        mConfirmDialog.setTitle(R.string.spoken_lang);
        mConfirmDialog = dialogBuilder.create();
        mConfirmDialog.show();
    }

    //reset all values
    private void resetForm() {
        searchFilterSetting.resetValues();
        initView();
    }


    @Override
    public void onBackPressed() {
        finish();

    }

}
