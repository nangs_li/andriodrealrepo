package co.real.productionreal2.service.model;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;
import co.real.productionreal2.model.AgentListing;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Created by hohojo on 18/11/2015.
 */
public class AgentProfiles {
    @SerializedName("MemberID")
    public int memberID;
    @SerializedName("MemberName")
    public String memberName;
    @SerializedName("AgentPhotoURL")
    public String agentPhotoURL;
    @SerializedName("AgentLicenseNumber")
    public String agentLicenseNumber;
    @SerializedName("QBID")
    public String qBID;
    @SerializedName("IsFollowing")
    public int isFollowing;
    @SerializedName("FollowingCount")
    public int followingCount;
    @SerializedName("FollowerCount")
    public int followerCount;

    @SerializedName("MemberType")
    public int memberType;

    @SerializedName("AgentLanguage")
    public List<AgentLanguage> agentLanguageList;
    @SerializedName("AgentExperience")
    public List<AgentExp> agentExpList;
    @SerializedName("AgentPastClosing")
    public List<AgentPastClosing> agentPastClosingList;
    @SerializedName("AgentSpecialty")
    public List<AgentSpecialty> agentSpecialtyList;

    @SerializedName("AgentListing")
    public AgentListing agentListing;

    @SerializedName("AgentListingHistory")
    public List<AgentListing> agentListingHistoryList;


    public AgentProfiles(Parcel source) {
        memberID = source.readInt();
        memberName = source.readString();
        agentPhotoURL = source.readString();
        agentLicenseNumber = source.readString();
        qBID = source.readString();
        isFollowing = source.readInt();
        followingCount = source.readInt();
        followerCount = source.readInt();
        memberType = source.readInt();

        agentLanguageList = new ArrayList<>();
        source.readTypedList(agentLanguageList, AgentLanguage.CREATOR);
        agentExpList = new ArrayList<>();
        source.readTypedList(agentExpList, AgentExp.CREATOR);
        agentPastClosingList = new ArrayList<>();
        source.readTypedList(agentPastClosingList, AgentPastClosing.CREATOR);
        agentSpecialtyList = new ArrayList<>();
        source.readTypedList(agentSpecialtyList, AgentSpecialty.CREATOR);

        agentListing = source.readParcelable(AgentListing.class.getClassLoader());
    }

//
//    public static final Creator<AgentProfiles> CREATOR = new Creator<AgentProfiles>() {
//        @Override
//        public AgentProfiles createFromParcel(Parcel in) {
//            return new AgentProfiles(in);
//        }
//
//        @Override
//        public AgentProfiles[] newArray(int size) {
//            return new AgentProfiles[size];
//        }
//    };
//
//    @Override
//    public int describeContents() {
//        return 0;
//    }
//
//    @Override
//    public void writeToParcel(Parcel dest, int flags) {
//        dest.writeInt(memberID);
//        dest.writeString(memberName);
//        dest.writeString(agentPhotoURL);
//        dest.writeString(agentLicenseNumber);
//        dest.writeString(qBID);
//        dest.writeInt(isFollowing);
//        dest.writeInt(followingCount);
//        dest.writeInt(followerCount);
//        dest.writeInt(memberType);
//
//        dest.writeTypedList(agentLanguageList);
//        dest.writeTypedList(agentExpList);
//        dest.writeTypedList(agentPastClosingList);
//        dest.writeTypedList(agentSpecialtyList);
//
//        dest.writeParcelable(agentListing, flags);
//    }

    public int getIsFollowing() {
        return isFollowing;
    }

    public void setIsFollowing(int isFollowing) {
        this.isFollowing = isFollowing;
    }

    @Override
    public String toString() {
        return "AgentProfiles{" +
                "memberID=" + memberID +
                ", memberName='" + memberName + '\'' +
                ", agentPhotoURL='" + agentPhotoURL + '\'' +
                ", agentLicenseNumber='" + agentLicenseNumber + '\'' +
                ", qBID='" + qBID + '\'' +
                ", isFollowing=" + isFollowing +
                ", followingCount=" + followingCount +
                ", followerCount=" + followerCount +
                ",memberType=" + memberType +
                ", agentLanguageList=" + agentLanguageList +
                ", agentExpList=" + agentExpList +
                ", agentPastClosingList=" + agentPastClosingList +
                ", agentSpecialtyList=" + agentSpecialtyList +
                ", agentListing=" + agentListing +
                '}';
    }
    // Comparator
    public static class CompPosition implements Comparator<AgentProfiles> {
        @Override
        public int compare( AgentProfiles arg0, AgentProfiles arg1) {
//            return arg0.agentListing.getCreationDate().before(arg1.agentListing.getCreationDate())?1:0;
            if (arg0.agentListing.getCreationDate().after(arg1.agentListing.getCreationDate())) return -1;
            else if (arg0.agentListing.getCreationDate().before(arg1.agentListing.getCreationDate())) return 1;
            else return 0;
        }
    }
}
