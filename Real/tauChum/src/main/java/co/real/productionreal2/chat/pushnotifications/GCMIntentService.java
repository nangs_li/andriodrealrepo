package co.real.productionreal2.chat.pushnotifications;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import co.real.productionreal2.Constants;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.MainActivityTabBase;
import co.real.productionreal2.activity.chat.ChatActivity;
import co.real.productionreal2.common.LocalStorageHelper;
import co.real.productionreal2.dao.account.DBQBChatDialog;
import co.real.productionreal2.daomanager.DatabaseManager;
import co.real.productionreal2.util.AppUtil;
import co.real.productionreal2.view.TabMenu;

public class GCMIntentService extends IntentService {

    public static final int NOTIFICATION_ID = 1;

    private static final String TAG = "GCMIntentService";


    private NotificationManager notificationManager;

    public GCMIntentService() {
        super(Consts.GCM_INTENT_SERVICE);
    }

    public static String bundle2string(Bundle bundle) {
        if (bundle == null) {
            return null;
        }
        String string = "Bundle{";
        for (String key : bundle.keySet()) {
            string += " " + key + " => " + bundle.get(key) + ";";
        }
        string += " }Bundle";
        return string;
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        Log.i(TAG, "new push");

        Bundle extras = intent.getExtras();
        Log.i("", "GCMTesting onHandleIntent " + bundle2string(extras));

        GoogleCloudMessaging googleCloudMessaging = GoogleCloudMessaging.getInstance(this);
        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
        String messageType = googleCloudMessaging.getMessageType(intent);
        String dialogId = extras.getString("dialog_id");
        if (dialogId != null) {
            DBQBChatDialog dbqbChatDialog = DatabaseManager.getInstance(this).getQBChatDialogByDialogId(dialogId);
            if (dbqbChatDialog != null) {
                Log.w(TAG, "dbqbChatDialog.getMutedOpponent() = "+dbqbChatDialog.getMutedOpponent());
                if (dbqbChatDialog.getMutedOpponent() != null && dbqbChatDialog.getMutedOpponent() == 1) {
                    if (Constants.isDevelopment)
                        Log.w(TAG, "receive msg but muted");
//                        Toast.makeText(this, "receive msg but muted", Toast.LENGTH_LONG).show();
                    return;
                }
            }
        }

        Log.w(TAG, "msg not muted");

        if (!extras.isEmpty()) {  // has effect of unparcelling Bundle
            Log.i(TAG, "extras = " + extras);
            if (GoogleCloudMessaging.
                    MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
                processNotification(Consts.GCM_SEND_ERROR, extras);
            } else if (GoogleCloudMessaging.
                    MESSAGE_TYPE_DELETED.equals(messageType)) {
                processNotification(Consts.GCM_DELETED_MESSAGE, extras);
                // If it's a regular GCM message, do some work.
            } else if (GoogleCloudMessaging.
                    MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                //todo if it's not mute
                // Post notification of received message.
                processNotification(Consts.GCM_RECEIVED, extras);
                Log.i(TAG, "Received: " + extras.toString());
            }
        }
        // Release the wake lock provided by the WakefulBroadcastReceiver.
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    // Put the message into a notification and post it.
    // This is just one simple example of what you might choose to do with
    // a GCM message.
    private void processNotification(String type, Bundle extras) {
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        String messageValue = extras.getString(Consts.GCM_MESSAGE_VALUE);
        final String newCount = extras.getString(Consts.GCM_KEY_NEW_COUNT);
        final String pushType = extras.getString(Consts.GCM_KEY_PUSH_TYPE);

        // pushType=1 for Me section
        if (pushType!=null && pushType.equals("1")){
            messageValue = extras.getString(Consts.GCM_KEY_ALERT);
            LocalStorageHelper.defaultHelper(getApplicationContext()).saveNewCountToLocalStorage(Integer.parseInt(pushType), TabMenu.TAB_SECTION.ME);
            // notify about new count
            Intent intentNewCountPush = new Intent(Consts.ME_NEW_COUNT_EVENT);
            LocalBroadcastManager.getInstance(this).sendBroadcast(intentNewCountPush);
        }

        Log.v("","edwinwww extras "+extras.toString());
        Log.i(TAG, "messageValue = " + messageValue);
        Log.i(TAG, "pushType = " + pushType);
        Log.i(TAG, "newCount = " + newCount);
        Log.i(TAG, "user_id = " + extras.getString("user_id"));
//        Log.i(TAG, "dialog_id = " + extras.getString("dialog_id"));
        Intent intent = new Intent(getApplicationContext(), MainActivityTabBase.class);

        if (pushType!=null && pushType.equals("1")) {

            intent.putExtra(Constants.EXTRA_SELECT_TAB, Constants.SELECT_NEWS);

        } else {

            intent.putExtra(Constants.EXTRA_SELECT_TAB, Constants.SELECT_CHAT);


            // Check if app is active
            if (!AppUtil.isAppOnForeground(this)) {

                // false -> login QB and receive the messages
            }
        }

        intent.putExtra(ChatActivity.EXTRA_USER_ID, extras.getString("user_id"));
//        intent.putExtra(Constants.EXTRA_DIALOG_ID, extras.getString("dialog_id"));
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);


        PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(), (int) System.currentTimeMillis(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.min_app_icon)
                        .setLargeIcon(bm)
                        .setContentTitle(this.getString(R.string.app_name))
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(messageValue))
                        .setContentText(messageValue)
                        .setAutoCancel(true);

        mBuilder.setDefaults(Notification.DEFAULT_ALL);

        mBuilder.setContentIntent(contentIntent);

        int requestID = (int) System.currentTimeMillis();
        notificationManager.notify(requestID, mBuilder.build());


        // notify about new push
        //
        Intent intentNewPush = new Intent(Consts.NEW_PUSH_EVENT);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intentNewPush);

        Log.i(TAG, "Broadcasting event " + Consts.NEW_PUSH_EVENT + " with data: " + messageValue);
    }


}