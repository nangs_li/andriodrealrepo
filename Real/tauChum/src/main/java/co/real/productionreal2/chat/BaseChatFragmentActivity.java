package co.real.productionreal2.chat;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.Toast;

import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.users.model.QBUser;

import java.util.List;

import co.real.productionreal2.R;
import co.real.productionreal2.chat.core.ApplicationSessionStateCallback;
import co.real.productionreal2.chat.core.ChatService;
import co.real.productionreal2.model.UserQBUser;

/**
 * Created by hohojo on 31/7/2015.
 */
public class BaseChatFragmentActivity extends FragmentActivity implements ApplicationSessionStateCallback {
    private static final String TAG = BaseChatFragmentActivity.class.getSimpleName();

    private static final String USER_LOGIN_KEY = "USER_LOGIN_KEY";
    private static final String USER_PASSWORD_KEY = "USER_PASSWORD_KEY";

    private boolean sessionActive = false;
    private boolean needToRecreateSession = false;

    private ProgressDialog ringProgressDialog;
    private ProgressDialog progressDialog;
    private final Handler handler = new Handler();

    public boolean isSessionActive() {
        return sessionActive;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // 'initialised' will be true if it's the 1st start of the app or if the app's process was killed by OS(or user)
        //
        boolean initialised = ChatService.initIfNeed(this);
        if(initialised){
            needToRecreateSession = true;
        }else{
            sessionActive = true;
        }
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.w(TAG, "login key = "+savedInstanceState.getString(USER_LOGIN_KEY));
        Log.w(TAG, "login Password = " + savedInstanceState.getString(USER_PASSWORD_KEY));
        if(needToRecreateSession){
            needToRecreateSession = false;

            Log.d(TAG, "Need to restore chat connection");

//            QBUser user = new QBUser();
//            user.setLogin(savedInstanceState.getString(USER_LOGIN_KEY));
//            user.setPassword(savedInstanceState.getString(USER_PASSWORD_KEY));
//
//            savedInstanceState.remove(USER_LOGIN_KEY);
//            savedInstanceState.remove(USER_PASSWORD_KEY);

            QBUser qbUser = UserQBUser.getNewQBUser(this);
            recreateSession(qbUser);
        }
    }

    private void recreateSession(QBUser user){
        sessionActive = false;
        this.onStartSessionRecreation();

        showProgressDialog();

        // Restoring Chat session
        //
        if (user == null)
            user = UserQBUser.getNewQBUser(this);
        final QBUser finalUser = user;
        ChatService.initIfNeed(this);
        ChatService.getInstance().login(user, new QBEntityCallbackImpl() {
            @Override
            public void onSuccess() {
                Log.d(TAG, "Chat login onSuccess");

                progressDialog.dismiss();
                progressDialog = null;

                sessionActive = true;
                BaseChatFragmentActivity.this.onFinishSessionRecreation(true);
            }

            @Override
            public void onError(List errors) {

                Log.d(TAG, "Chat login onError: " + errors);

                Toast toast = Toast.makeText(getApplicationContext(),
                        "Error in the recreate session request, trying again in 3 seconds.. Check you internet connection.", Toast.LENGTH_SHORT);
                toast.show();

                // try again
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        recreateSession(finalUser);
                    }
                }, 500);

                BaseChatFragmentActivity.this.onFinishSessionRecreation(false);
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        ChatService.initIfNeed(this);
        QBUser currentUser = ChatService.getInstance().getCurrentUser();
        if(currentUser != null) {
            outState.putString(USER_LOGIN_KEY, currentUser.getLogin());
            outState.putString(USER_PASSWORD_KEY, currentUser.getPassword());
        }

        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(outState);
    }

    private void showProgressDialog(){
        if(progressDialog == null) {
            progressDialog = new ProgressDialog(BaseChatFragmentActivity.this);
            progressDialog.setTitle("");
            progressDialog.setMessage(getString(R.string.alert__Initializing));
//            progressDialog.setMessage("Restoring chat session...");
            progressDialog.setProgressStyle(progressDialog.STYLE_SPINNER);
        }
        progressDialog.show();
    }


    public void showLoadingDialog() {
        if (ringProgressDialog != null && ringProgressDialog.isShowing())
            return;
        ringProgressDialog = ProgressDialog.show(this,"", getResources().getString(R.string.alert__Initializing));
    }

    public void hideLoadingDialog() {
        if (ringProgressDialog != null) {
            ringProgressDialog.dismiss();
            ringProgressDialog.cancel();
            ringProgressDialog = null;
        }
    }

    //
    // ApplicationSessionStateCallback
    //

    @Override
    public void onStartSessionRecreation() {
    }

    @Override
    public void onFinishSessionRecreation(boolean success) {
    }

}