package co.real.productionreal2;

import android.content.Context;

import com.google.gson.Gson;

import co.real.productionreal2.daomanager.DatabaseManager;
import co.real.productionreal2.service.model.AgentProfiles;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;

/**
 * Created by edwinchan on 6/1/16.
 */
public class CoreData {

    static public boolean isAgentProfileUpdate = false;
    static public int langIndex = -1;
    static public String regId = "";
    static public boolean isNewUser = false;
    static public AgentProfiles opponentAgentProfile = null;
    static public int qbUserIdForPush = -1;
    static public boolean isOldUser = false;
    /**
     * Kelvin
     * used to control whether show InviteToFollow right after publish story,
     * shown count >10 or Once Successful InviteToFollow case, then not show anymore
     */
    static public int inviteToFollowShownCount = 10;

    static private ResponseLoginSocial.Content userContent = null;

    static public void clear() {
        isAgentProfileUpdate = false;
        langIndex = -1;
        regId = "";
        opponentAgentProfile = null;
        userContent = null;
        inviteToFollowShownCount = 0;
    }

    static public ResponseLoginSocial.Content getUserContent(Context context) {
        if (userContent == null) {
            intitUserContent(context);
        }
        return userContent;
    }

    static public void intitUserContent(Context context) {
        if (DatabaseManager.getInstance(context).getLogInfo() != null)
            if (DatabaseManager.getInstance(context).getLogInfo().getLogInfo() != null)
                userContent = new Gson().fromJson(DatabaseManager.getInstance(context).getLogInfo().getLogInfo(), ResponseLoginSocial.Content.class);
    }

    static public void setUserContent(ResponseLoginSocial.Content newUserContent) {
        userContent = newUserContent;
    }
}
