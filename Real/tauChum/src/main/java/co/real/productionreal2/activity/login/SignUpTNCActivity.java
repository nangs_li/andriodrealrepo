package co.real.productionreal2.activity.login;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.BaseActivity;
import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.request.RequestLegalStatement;
import co.real.productionreal2.service.model.response.ResponseLegalStatement;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class SignUpTNCActivity extends BaseActivity {
    @InjectView(R.id.tncTextView)
    TextView tncTextView;
    @InjectView(R.id.progressBar)
    ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_tnc);
        ButterKnife.inject(this);

        progressBar.setVisibility(View.VISIBLE);
        RequestLegalStatement legalStatement = new RequestLegalStatement(0, "");
        legalStatement.langCode = "en";
        legalStatement.msgType = 2;
        legalStatement.callLegalStatementGetApi(this, new ApiCallback() {
            @Override
            public void success(ApiResponse apiResponse) {
                progressBar.setVisibility(View.GONE);
                ResponseLegalStatement responseGson = new Gson().fromJson(apiResponse.jsonContent, ResponseLegalStatement.class);
                tncTextView.setText(responseGson.content.msg);
            }

            @Override
            public void failure(String errorMsg) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }
}
