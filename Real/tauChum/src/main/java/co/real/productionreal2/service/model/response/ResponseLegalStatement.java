package co.real.productionreal2.service.model.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by hohojo on 12/11/2015.
 */
public class ResponseLegalStatement extends ResponseBase{

    @SerializedName("Content")
    public Content content;

    public class Content {
        @SerializedName("Message")
        public String msg;
    }
}
