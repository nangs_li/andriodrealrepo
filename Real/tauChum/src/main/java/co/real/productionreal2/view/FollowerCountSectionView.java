package co.real.productionreal2.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import co.real.productionreal2.R;
import co.real.productionreal2.util.AppUtil;

/**
 * Created by kelvinsun on 14/1/16.
 */
public class FollowerCountSectionView extends LinearLayout {
    private TextView tvCount;
    private TextView tvDesc;
    private ImageView ivAlert;
    private Context context;

    public FollowerCountSectionView(Context context) {
        super(context);
        this.context=context;
    }

    public FollowerCountSectionView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context=context;

    }

    private void initView() {
        // find views
        tvCount = (TextView) findViewById(R.id.tv_follow_count);
        tvDesc = (TextView) findViewById(R.id.tv_follow_desc);
        ivAlert= (ImageView) findViewById(R.id.iv_follow_alert);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        initView();
    }

    public void setCount(int count){
        tvCount.setText(AppUtil.formatAmount((count)));
    }

    public void setDesc(int desc){
        tvDesc.setText(desc);
    }

    public void setViewSelected(boolean isSelected){
        setSelected(isSelected);
        tvDesc.setSelected(isSelected);
    }

    public void showAlert(boolean isShow){
        ivAlert.setVisibility(isShow?View.VISIBLE:View.GONE);
    }

}