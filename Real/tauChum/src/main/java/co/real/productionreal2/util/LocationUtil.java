package co.real.productionreal2.util;

import android.content.Context;
import android.telephony.TelephonyManager;
import android.util.Log;

import java.util.Locale;

/**
 * Created by hohojo on 30/12/2015.
 */
public class LocationUtil {
    public static final String DEFAULT_COUNTRY = "us";

    public static String getCountryIso(Context context) {
        TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String countryName = manager.getNetworkCountryIso();
        return countryName.isEmpty() ? "USA" : countryName;
    }

    public static String getCountryIsoByGPS(Context context) {

        // check if GPS enabled
        GPSUtil gpsTracker = new GPSUtil(context);

        if (gpsTracker.getIsGPSTrackingEnabled()) {
            String countryCode = gpsTracker.getCountryCode(context);
            Log.d("LocationUtil", "LocationUtil: countryCode: " + countryCode);
            if (countryCode!=null)
            return countryCode.toLowerCase();
        } else {
            // can't get location
            // GPS or Network is not enabled
            // return default country
            return DEFAULT_COUNTRY;
        }
        return DEFAULT_COUNTRY;
    }

    /**
     * Get ISO 3166-1 alpha-2 country code for this device (or null if not available)
     *
     * @param context Context reference to get the TelephonyManager instance from
     * @return country code or null
     */
    public static String getCountryIsoBySIM(Context context) {
        try {
            final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            final String simCountry = tm.getSimCountryIso();
            if (simCountry != null && simCountry.length() == 2) { // SIM country code is available
                return simCountry.toLowerCase(Locale.US);
            } else if (tm.getPhoneType() != TelephonyManager.PHONE_TYPE_CDMA) { // device is not 3G (would be unreliable)
                String networkCountry = tm.getNetworkCountryIso();
                if (networkCountry != null && networkCountry.length() == 2) { // network country code is available
                    return networkCountry.toLowerCase(Locale.US);
                }
            }
        } catch (Exception e) {
        }
        return getCountryIsoByGPS(context);
    }

    public static boolean isSupportedGoogleMap(Context context){
        return getCountryIsoBySIM(context)!="cn"|| AppUtil.checkGooglePlayServiceAvailability(context,-1);
    }


    /**
     * Get ISO 3166-1 alpha-2 country code for this device (or null if not available)
     *
     * @param context Context reference to get the TelephonyManager instance from
     * @return country code or null
     */
    public static String getCountryBasedOnSimCardOrNetwork(Context context) {
        try {
            final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            final String simCountry = tm.getSimCountryIso();
            if (simCountry != null && simCountry.length() == 2) { // SIM country code is available
                return simCountry.toLowerCase(Locale.US);
            } else if (tm.getPhoneType() != TelephonyManager.PHONE_TYPE_CDMA) { // device is not 3G (would be unreliable)
                String networkCountry = tm.getNetworkCountryIso();
                if (networkCountry != null && networkCountry.length() == 2) { // network country code is available
                    return networkCountry.toLowerCase(Locale.US);
                }
            }
        } catch (Exception e) {
        }
        return DEFAULT_COUNTRY;
    }


}
