package co.real.productionreal2;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;


public class BaseContainerFragment extends Fragment {

    public void replaceFragment(Fragment fragment, boolean addToBackStack) {
        String backStateName = fragment.getClass().getName();
//        boolean fragmentPopped = getChildFragmentManager().popBackStackImmediate(backStateName, 0);
//        if (!fragmentPopped) {

        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        if (addToBackStack) {
            transaction.addToBackStack(backStateName);
        }
        transaction.replace(R.id.container_framelayout, fragment);
        transaction.commitAllowingStateLoss();
//            transaction.commit();
        getChildFragmentManager().executePendingTransactions();
//        }
    }


    public void replaceFragment(Fragment fragment, boolean addToBackStack, String tag) {
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        if (addToBackStack) {
            transaction.addToBackStack(null);
        }
        transaction.replace(R.id.container_framelayout, fragment, tag);
        transaction.commitAllowingStateLoss();
//		transaction.commit();
        getChildFragmentManager().executePendingTransactions();
    }


    public boolean popFragment() {
        Log.e("Ritesh", "pop fragment: " + getChildFragmentManager().getBackStackEntryCount());
        boolean isPop = false;
        if (getChildFragmentManager().getBackStackEntryCount() > 0) {
            isPop = true;
            getChildFragmentManager().popBackStack();
        }
        return isPop;
    }
}