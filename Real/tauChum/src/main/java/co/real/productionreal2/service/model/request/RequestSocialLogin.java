package co.real.productionreal2.service.model.request;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.quickblox.users.model.QBUser;

import java.util.Date;
import java.util.Locale;

import co.real.productionreal2.Constants;
import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.chat.ChatSessionUtil;
import co.real.productionreal2.chat.core.ChatService;
import co.real.productionreal2.chat.core.PrivateChatImpl;
import co.real.productionreal2.common.FetchAgentProfileHelper;
import co.real.productionreal2.common.LocalStorageHelper;
import co.real.productionreal2.config.AppConfig;
import co.real.productionreal2.daomanager.DatabaseManager;
import co.real.productionreal2.login.BaseLoginController;
import co.real.productionreal2.model.InputRequest;
import co.real.productionreal2.model.UserQBUser;
import co.real.productionreal2.service.AdminService;
import co.real.productionreal2.service.BaseService;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;
import co.real.productionreal2.util.AppUtil;
import co.real.productionreal2.util.CryptLib;
import co.real.productionreal2.util.NetworkUtil;
import co.real.productionreal2.view.Dialog;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class RequestSocialLogin {

    @SerializedName("DeviceType")
    int deviceType;

    @SerializedName("DeviceToken")
    String udid;

    @SerializedName("SocialNetworkType")
    int socialType;

    @SerializedName("UserName")
    String userName;

    @SerializedName("UserID")
    String accountId;

    @SerializedName("Email")
    String email;

    @SerializedName("PhotoURL")
    String photoURL;

    @SerializedName("AccessToken")
    String accessToken;

    @SerializedName("Lang")
    String lang;

    public RequestSocialLogin(int deviceType, String udid, int socialType,
                              String userName, String accountId, String email, String photoURL,
                              String accessToken) {
        super();

        this.deviceType = deviceType;
        this.udid = udid;
        this.socialType = socialType;
        this.userName = userName;
        this.accountId = accountId;
        this.email = email;
        this.photoURL = photoURL;
        this.accessToken = accessToken;
    }

    public void callLoginSocialApi(final Context context) {
        this.lang = AppUtil.getCurrentLangISO2Code(context);
        String json = new Gson().toJson(this);
        String encryptJson;
        if (Constants.enableEncryption) {
            encryptJson = CryptLib.encrypt(json);
        } else {
            encryptJson = json;
        }

        final AdminService weatherService = new AdminService(context);

        weatherService.getCoffeeService().loginSocial(
                new InputRequest(encryptJson),
                new retrofit.Callback<ApiResponse>() {

                    @Override
                    public void failure(RetrofitError error) {
                        NetworkUtil.showNoNetworkMsg(context);
                    }

                    @Override
                    public void success(ApiResponse apiResponse, Response response) {
                        if (context != null) {
                            Gson gson = new Gson();
                            Log.w("requestsocialLogin", "apiResponse = " + apiResponse);
                            String decryptJson;
                            if (Constants.enableEncryption) {
                                decryptJson = CryptLib.decrypt(apiResponse.jsonContent);
                                Log.w("requestsocialLogin", "jsonContent = " + decryptJson);
                            } else {
                                decryptJson = apiResponse.jsonContent;
                            }

                            ResponseLoginSocial responseLoginSocial = gson.fromJson(decryptJson, ResponseLoginSocial.class);

                            LocalStorageHelper.setUpByLogin(context, responseLoginSocial.content);
                            DatabaseManager.getInstance(context).dbActionForLogin(responseLoginSocial.content);

                            QBUser user = UserQBUser.getNewQBUser(context);

                            final ResponseLoginSocial.Content content = responseLoginSocial.content;
                            if (responseLoginSocial.errorCode == 4) {
                                Dialog.accessErrorForceLogoutDialog(context).show();
                            } else if (responseLoginSocial.errorCode == 26) {
                                Dialog.suspendedErrorForceLogoutDialog(context).show();
                            } else if (responseLoginSocial.errorCode == 20 || responseLoginSocial.errorCode == 21 || responseLoginSocial.errorCode == 22) {

                                // Mixpanel
                                MixpanelAPI mixpanel = MixpanelAPI.getInstance(context, AppConfig.getMixpanelToken());
                                mixpanel.getPeople().set("$name", content.memName);

                                if (responseLoginSocial.errorCode == 22) {
                                    // New user
                                    mixpanel.alias("" + content.memId, mixpanel.getDistinctId());
                                    mixpanel.identify(mixpanel.getDistinctId());
                                    mixpanel.getPeople().identify(mixpanel.getDistinctId());
                                    mixpanel.track("Became User");
                                    mixpanel.getPeople().set("Member ID", "" + content.memId);
                                    mixpanel.getPeople().set("Agent", "NO");
                                    mixpanel.getPeople().set("# of listings", 0);
                                    mixpanel.getPeople().set("User since", new Date());
                                    Locale locale = LocalStorageHelper.getCurrentLocale(context);
                                    mixpanel.getPeople().set("Language", locale.getLanguage());

                                    AppUtil.goToMainTabActivity((Activity) context, responseLoginSocial.content);

                                } else {
                                    // Old user
                                    mixpanel.identify("" + content.memId);
                                    mixpanel.getPeople().identify(mixpanel.getDistinctId());
                                    mixpanel.getPeople().set("Member ID", "" + content.memId);
                                    Locale locale = LocalStorageHelper.getCurrentLocale(context);
                                    mixpanel.getPeople().set("Language", locale.getLanguage());
                                    mixpanel.track("Verified Facebook Login");

                                    // pragma mark - DC
                                    // get dialogs, QBUsers, chatrooms and agent profiles
                                    // If success, process to main tab activity
                                    ChatSessionUtil.callQBLogin(context, user, new ApiCallback() {
                                        @Override
                                        public void success(ApiResponse apiResponse) {
                                            PrivateChatImpl.getInstance((Activity)context);

                                            ChatService.getInstance().getAllDialogs(new BaseService.BaseServiceCallBack() {
                                                @Override
                                                public void onSuccess(Object var1) {

                                                    FetchAgentProfileHelper.getInstance().startFetching();
                                                    AppUtil.goToMainTabActivity((Activity)context, content);
                                                }

                                                @Override
                                                public void onError(BaseService.RealServiceError serviceError) {

                                                    AppUtil.goToMainTabActivity((Activity)context, content);
                                                }
                                            });

                                        }

                                        @Override
                                        public void failure(String errorMsg) {

                                            Log.d("ChatSessionUtil", "failure: ");
                                            AppUtil.goToMainTabActivity((Activity)context, content);
                                        }
                                    });

                                }


                            } else {
                                BaseLoginController.getInstance(context).loginFail(responseLoginSocial.errorMsg);
                            }
                        }
                    }
                }
        );
    }

    public void callLoginSocialApi(final Context context, final ApiCallback callback) {
        this.lang = AppUtil.getCurrentLangISO2Code(context);
        String json = new Gson().toJson(this);
        String encryptJson;
        if (Constants.enableEncryption) {
            encryptJson = CryptLib.encrypt(json);
        } else {
            encryptJson = json;
        }
        final AdminService weatherService = new AdminService(context);
        weatherService.getCoffeeService().loginSocial(
                new InputRequest(encryptJson),
                new retrofit.Callback<ApiResponse>() {
                    @Override
                    public void failure(RetrofitError error) {
                        callback.failure("");
                        NetworkUtil.showNoNetworkMsg(context);
                    }

                    @Override
                    public void success(final ApiResponse apiResponse, Response response) {
                        if (context != null) {
                            Gson gson = new Gson();
                            Log.w("requestsocialLogin", "apiResponse = " + apiResponse);
                            String decryptJson;
                            if (Constants.enableEncryption) {
                                decryptJson = CryptLib.decrypt(apiResponse.jsonContent);
                                Log.w("requestsocialLogin", "jsonContent = " + decryptJson);
                            } else {
                                decryptJson = apiResponse.jsonContent;
                            }
                            ResponseLoginSocial responseGson = gson.fromJson(decryptJson, ResponseLoginSocial.class);
                            if (responseGson.errorCode == 4) {
                                Dialog.accessErrorForceLogoutDialog(context).show();
                            } else if (responseGson.errorCode == 26) {
                                Dialog.suspendedErrorForceLogoutDialog(context).show();
                            } else if (responseGson.errorCode != 0 && responseGson.errorCode != 20 && responseGson.errorCode != 21 &&
                                    responseGson.errorCode != 22 && responseGson.errorCode != 23) {
                                callback.failure(responseGson.errorMsg);
                            } else {
                                callback.success(apiResponse);
                            }
                        }

                    }
                }
        );
    }


}
