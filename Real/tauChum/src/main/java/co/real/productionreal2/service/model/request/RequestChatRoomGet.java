package co.real.productionreal2.service.model.request;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Collection;

import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.model.InputRequest;
import co.real.productionreal2.service.OperationService;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.response.ResponseChatRoomGet;
import co.real.productionreal2.util.NetworkUtil;
import co.real.productionreal2.view.Dialog;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by hohojo on 18/11/2015.
 */
public class RequestChatRoomGet extends RequestBase{

    public RequestChatRoomGet(int memId, String session) {
        super(memId, session);
    }

    public RequestChatRoomGet(int memId, String session, Collection<String> dialogIDs){
        super(memId,session);
        if (dialogIDs != null && dialogIDs.size() > 0) {
            this.dialogIDs = new ArrayList<>(dialogIDs);
        }
    }
    @SerializedName("DialogID")
    public String dialogId;

    @SerializedName("DialogIDList")
    public Collection<String> dialogIDs;

    public void callChatRoomGetApi(final Context context,  final ApiCallback callback) {
        String json = new Gson().toJson(this);
        final OperationService weatherService = new OperationService(context);
        weatherService.getCoffeeService().chatRoomGet(new InputRequest(json), new Callback<ApiResponse>() {
            @Override
            public void success(ApiResponse apiResponse, Response response) {
                ResponseChatRoomGet responseGson = new Gson().fromJson(apiResponse.jsonContent, ResponseChatRoomGet.class);
                if (responseGson.errorCode == 4) {
                    Dialog.accessErrorForceLogoutDialog(context).show();
                } else if (responseGson.errorCode == 26) {
                    Dialog.suspendedErrorForceLogoutDialog(context).show();
                } else if (responseGson.errorCode != 0 && responseGson.errorCode != 20 && responseGson.errorCode != 21 &&
                        responseGson.errorCode != 22 && responseGson.errorCode != 23) {
                    callback.failure(responseGson.errorMsg);
                } else {
                    callback.success(apiResponse);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                callback.failure("");                 NetworkUtil.showNoNetworkMsg(context);
            }
        });
    }
}
