package co.real.productionreal2.service.model.response;

import com.google.gson.annotations.SerializedName;

import co.real.productionreal2.service.model.Follower;

import java.util.List;

/**
 * Created by kelvinsun on 19/2/16.
 */
public class ResponseFollowingList extends ResponseBase {
    @SerializedName("Content")
    public Content content;

    public class Content {

        @SerializedName("Page")
        public int page;

        @SerializedName("TotalCount")
        public int totalCount;

        @SerializedName("RecordCount")
        public int recordCount;

        @SerializedName("Followers")
        public List<Follower> followers;


    }
}
