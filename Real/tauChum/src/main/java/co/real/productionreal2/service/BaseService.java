package co.real.productionreal2.service;


import co.real.productionreal2.config.AppConfig;

/**
 * Created by kelvinsun on 15/1/16.
 */
public class BaseService {

    public static final String WEB_SERVICE_BASE_URL = AppConfig.getRealApiBaseUrl();
    public class RealServiceError{
        private int errorCode;
        private String message;

        public RealServiceError(){

        }

        public RealServiceError(int errorCode, String message){
            super();
            this.errorCode = errorCode;
            this.message = message;
        }

        public RealServiceError(int errorCode){
            super();
            this.errorCode = errorCode;
            String message = "Error";
            this.message = message;
        }

        public int getErrorCode() {
            return errorCode;
        }

        public String getMessage() {
            return message;
        }
    }
    public interface BaseServiceCallBack<T> {
            void onSuccess(T var1);

            void onError(RealServiceError serviceError);
    }

}
