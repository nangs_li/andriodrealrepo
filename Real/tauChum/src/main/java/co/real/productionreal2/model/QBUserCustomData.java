package co.real.productionreal2.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by hohojo on 27/8/2015.
 */
public class QBUserCustomData {
    @SerializedName("memberID")
    public String memberID;
    @SerializedName("agentListingID")
    public String agentListingID;
    @SerializedName("lastUpdateTimeStamp")
    public String lastUpdateTimeStamp;
    @SerializedName("chatroomshowlastseen")
    public boolean chatRoomShowLastSeen = true;
    @SerializedName("agentListing")
    public String agentListing;
    @SerializedName("mypersonalphotourl")
    public String myPersonalPhotoUrl;
    @SerializedName("chatroomshowreadreceipts")
    public boolean chatRoomShowReadReceipts = true;
    @SerializedName("agentProfileID")
    public String agentProfileId;
    @SerializedName("photoURL")
    public String photoUrl;
    @SerializedName("chatVersion")
    public String chatVersion;
    @SerializedName("block")
    public String block;
    @SerializedName("mute")
    public String mute;

//    public QBUserCustomData(Parcel source) {
//        myPersonalPhotoUrl = source.readString();
//        chatRoomShowLastSeen = source.readString();
//        chatRoomShowReadReceipts = source.readString();
//        agentProfileId = source.readString();
//        agentListingId = source.readString();
//        chatVersion = source.readString();
//        block = source.readString();
//        mute = source.readString();
//    }

    public QBUserCustomData(){}

//    public static final Creator<QBUserCustomData> CREATOR = new Creator<QBUserCustomData>() {
//        @Override
//        public QBUserCustomData createFromParcel(Parcel in) {
//            return new QBUserCustomData(in);
//        }
//
//        @Override
//        public QBUserCustomData[] newArray(int size) {
//            return new QBUserCustomData[size];
//        }
//    };
//
//    @Override
//    public int describeContents() {
//        return 0;
//    }
//
//    @Override
//    public void writeToParcel(Parcel dest, int flags) {
//        dest.writeString(myPersonalPhotoUrl);
//        dest.writeString(chatRoomShowLastSeen);
//        dest.writeString(chatRoomShowReadReceipts);
//        dest.writeString(agentProfileId);
//        dest.writeString(agentListingId);
//        dest.writeString(chatVersion);
//        dest.writeString(block);
//        dest.writeString(mute);
//    }
}
