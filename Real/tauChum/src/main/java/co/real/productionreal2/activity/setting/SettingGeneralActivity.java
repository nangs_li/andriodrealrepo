package co.real.productionreal2.activity.setting;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import co.real.productionreal2.Constants;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.BaseActivity;
import co.real.productionreal2.common.LocalStorageHelper;
import co.real.productionreal2.model.SettingItem;
import co.real.productionreal2.util.AppUtil;
import co.real.productionreal2.util.StringUtils;
import co.real.productionreal2.view.SettingSectionRecyclerView;

public class SettingGeneralActivity extends BaseActivity {

    @InjectView(R.id.headerLayout)
    View headerLayout;
    @InjectView(R.id.settingRecyclerView)
    SettingSectionRecyclerView settingRecyclerView;
    List<SettingItem> items;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_general);
        ButterKnife.inject(this);

    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("onResume","re");
        initData();
        initView();
    }

    private void initData() {
        items = new ArrayList<>();
        String languageTitle = StringUtils.getById(this, R.string.setting__language, "Language");
        String languageValue = AppUtil.getCurrentLangNativeName(this);
        String unitTitle = StringUtils.getById(this, R.string.setting__unit, "Unit");
        String unitValue = AppUtil.getSelectedMetricName(this);
        String changePasswordTitle = StringUtils.getById(this, R.string.change_password__title, "Change password");
        String changeEmailTitle = StringUtils.getById(this, R.string.change_email__title, "Change email");
        String changePhoneNoTitle = StringUtils.getById(this, R.string.change_phone_no__title, "Change phone number");
        items.add(new SettingItem(SettingItem.SettingItemType.SETTING_TYPE_SECTION_SPACING, R.color.transparent));
        items.add(new SettingItem(languageTitle, languageValue, ChangeLangSettingActivity.class, R.color.setting_spacing_blue,false));
        items.add(new SettingItem(SettingItem.SettingItemType.SETTING_TYPE_SECTION_SPACING, R.color.transparent));
        items.add(new SettingItem(unitTitle, unitValue, MetricSettingActivity.class, R.color.setting_spacing_blue,false));
        items.add(new SettingItem(SettingItem.SettingItemType.SETTING_TYPE_SECTION_SPACING, R.color.transparent));

        //add change phone number row
        items.add(new SettingItem(changePhoneNoTitle,"",ChangePhoneNumberActivity.class,R.color.setting_spacing_blue,false));
//        if (isRealNetworkLogin()) {
//            items.add(new SettingItem(changeEmailTitle, "", ChangeEmailSettingActivity.class, R.color.setting_spacing_blue));
//            items.add(new SettingItem(changePasswordTitle, "", ChangePwSettingActivity.class, R.color.setting_spacing_blue,false));
//        }
    }

    private void initView() {
        TextView titleTextView = (TextView) headerLayout.findViewById(R.id.titleTextView);
        titleTextView.setText(StringUtils.getById(this, R.string.setting__general, "General"));
        initRecyclerView(items);
    }

    private void initRecyclerView(List<SettingItem> items) {
        settingRecyclerView.initAdapterWithItems(items, this);
    }


    private boolean isRealNetworkLogin() {
        SharedPreferences prefs = this.getSharedPreferences(Constants.PREF_REAL_NETWORK, this.MODE_PRIVATE);
        String realJson = LocalStorageHelper.getRealNetworkJson(this);
        if (realJson == null || realJson.equals(""))
            return false;

        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
