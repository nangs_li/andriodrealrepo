package co.real.productionreal2.service.model.response;

import com.google.gson.annotations.SerializedName;
import co.real.productionreal2.service.model.AgentProfiles;

import java.util.List;

/**
 * Created by hohojo on 1/3/2016.
 */
public class ResponseAgentProfileGetListEx extends ResponseBase {
    @SerializedName("Content")
    public Content content;

    public class Content {
        @SerializedName("AgentProfiles")
        public List<AgentProfiles> profiles;
    }
}
