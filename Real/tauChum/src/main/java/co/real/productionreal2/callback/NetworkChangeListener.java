package co.real.productionreal2.callback;

/**
 * Created by hohojo on 23/7/2015.
 */
public interface NetworkChangeListener {

    public void networkChange(int status);
}
