package co.real.productionreal2.activity.createpost;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.askerov.dynamicgrid.DynamicGridView;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import co.real.productionreal2.Constants;
import co.real.productionreal2.Navigation;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.BaseActivity;
import co.real.productionreal2.adapter.ImageGridViewDynamicAdapter;
import co.real.productionreal2.callback.DialogDualCallback;
import co.real.productionreal2.config.AppConfig;
import co.real.productionreal2.model.AgentListing.Photo;
import co.real.productionreal2.util.AppUtil;
import co.real.productionreal2.util.FileUtil;
import co.real.productionreal2.util.ImageUtil;
import co.real.productionreal2.util.PermissionUtil;
import co.real.productionreal2.view.CustomScrollView;
import co.real.productionreal2.view.Dialog;
import co.real.productionreal2.view.TouchImageView;
import me.iwf.photopicker.PhotoPickerActivity;
import me.iwf.photopicker.utils.PhotoPickerIntent;

;

/**
 * Created by hohojo on 2/10/2015.
 */
public class CreatePostPhotosActivity extends BaseActivity {
    private static final String TAG = "CreatePostPhotos";
    @InjectView(R.id.coverImageView)
    TouchImageView coverImageView;
    @InjectView(R.id.sv_photo_section)
    CustomScrollView svImageSection;
    @InjectView(R.id.imageGridView)
    DynamicGridView imageGridView;
    @InjectView(R.id.noPhotoImageLayout)
    View noPhotoImageLayout;
    @InjectView(R.id.doneEditBtn)
    Button doneEditBtn;
    @InjectView(R.id.resetBtn)
    Button deleteAllBtn;
    @InjectView(R.id.backBtn)
    ImageButton backBtn;
    @InjectView(R.id.closeBtn)
    ImageButton closeBtn;
    @InjectView(R.id.nextBtn)
    Button nextBtn;
    @InjectView(R.id.coverImageFrameLayout)
    FrameLayout coverImageFrameLayout;
    @InjectView(R.id.changePhotoRelativeLayout)
    RelativeLayout changePhotoRelativeLayout;
    @InjectView(R.id.rootImageView)
    ImageView rootImageView;
    @InjectView(R.id.imageView2)
    ImageView noImageImageView;
    @InjectView(R.id.textView7)
    TextView noImageTextView;

    private boolean isBeforeDragInside = false;

    private List<Photo> photoFullList;
    private List<Photo> photoPathList;
    private List<String> photoPathListForGrid;
    private String coverPhotoPath;
    public static final int SELECT_PHOTO = 0;
    private ImageGridViewDynamicAdapter imageGridViewDynamicAdapter;
    private boolean isEditing = false;
    private int outsideDragOldPosition = 0;
    private boolean didOutsideDrag = false;
    private boolean hasCoverLoaded = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_post_photos);

        ButterKnife.inject(this);
        photoPathList = new ArrayList<Photo>();
        photoFullList = new ArrayList<Photo>();
        photoPathListForGrid = new ArrayList<String>();
        //load saved photo if any
        if (BaseCreatePost.getInstance(this).getPhotoPathList() != null) {
            photoPathList.addAll(BaseCreatePost.getInstance(this).getPhotoPathList());
            photoFullList.addAll(photoPathList);
        }
        Log.d(TAG, "photoPathListForGrid: photoFullList: before: " + photoFullList.size());
        imageGridViewDynamicAdapter = new ImageGridViewDynamicAdapter(this, photoPathListForGrid, imageGridView,
                3);

        imageGridView.setAdapter(imageGridViewDynamicAdapter);

        initView();

        if (photoFullList.size() > 0) {
//            Picasso.with(this)
//                    .load(new File(getCoverImagefromFile()))
//                    .placeholder(R.drawable.chat_progress_animation)
//                    .centerCrop().fit().into(coverImageView);
            hasCoverLoaded=true;
            setCoverPhotoPath(photoPathList.get(0).url);
            coverImageView.setImageBitmap(BitmapFactory.decodeFile(BaseCreatePost.getInstance(this).getPhotoPathList().get(0).url));
            coverImageView.setRotateAngle(0);
            photoPathListForGrid = new ArrayList<>();
            for (Photo photo: photoFullList)
                photoPathListForGrid.add(photo.url);
            Log.d(TAG, "photoPathListForGrid: before: " + photoPathListForGrid.size());
            photoPathListForGrid.remove(1);
            photoPathListForGrid.remove(0);
            imageGridView.setRealImageCount(photoPathListForGrid.size());
            imageGridViewDynamicAdapter.set(photoPathListForGrid);
            Log.d(TAG, "photoPathListForGrid: after: " + photoPathListForGrid.size());
        }

        //start from top
        svImageSection.fullScroll(View.FOCUS_UP);

    }

    private void initView() {

        nextBtn.setEnabled(photoFullList.size()>0);

        noPhotoImageLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                        && ContextCompat.checkSelfPermission(CreatePostPhotosActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(CreatePostPhotosActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            PermissionUtil.MY_PERMISSIONS_REQUEST_EXTERNAL_STORAGE);
                    return;
                }

                PhotoPickerIntent intent = new PhotoPickerIntent(CreatePostPhotosActivity.this);
                intent.setPhotoCount(Constants.MAX_SELECT_PHOTO);
                startActivityForResult(intent, CreatePostPhotosActivity.SELECT_PHOTO);
            }
        });

        noPhotoImageLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                if (photoFullList.size() > 0 || isEditing == false) {

                    startEditMode(-1);
                }
                return true;
            }
        });

//        imageGridView.setOnDropListener(new DynamicGridView.OnDropListener() {
//            @Override
//            public void onActionDrop() {
//                if (didOutsideDrag) {
//
//                    String photoURL = (String) imageGridViewDynamicAdapter.getItem(outsideDragOldPosition);
//                    photoPathListForGrid.remove(photoURL);
//                    //move the cover to the end of list
//                    photoPathListForGrid.add(getCoverPhotoPath());
//                    setCoverPhotoPath(photoURL);
//                    setCoverImageInImageView(getCoverPhotoPath());
//                    ArrayList<Photo> tempPhotos = new ArrayList<Photo>(photoFullList);
//
//                    for (Photo photo : photoFullList) {
//                        if (photo.getUrl() == photoURL) {
//                            tempPhotos.remove(photo);
//                        }
//                    }
//                    tempPhotos.add(new Photo(0, getCoverPhotoPath()));
//                    photoFullList = new ArrayList<Photo>(tempPhotos);
//                    Log.w(TAG, "photoList = " + photoPathListForGrid.size());
//                    imageGridViewDynamicAdapter.set(photoPathListForGrid);
//                    Log.d(TAG, "photoPathListForGrid: photoFullList: after: " + photoFullList.size());
//                    didOutsideDrag = false;
//                }
//            }
//        });

        imageGridView.setOnDragListener(new DynamicGridView.OnDragListener() {
            @Override
            public void onDragStarted(int position) {
                Log.d(TAG, "drag started at position " + position);
                Log.d(TAG, "test grid: start: " + imageGridViewDynamicAdapter.getItems().size());
                photoPathListForGrid=new ArrayList<String>();
                photoPathListForGrid.addAll(imageGridViewDynamicAdapter.getPhotosPath());
            }

            @Override
            public void onDragPositionsChanged(int oldPosition, int newPosition) {
                Log.d(TAG, String.format("drag item position changed from %d to %d", oldPosition, newPosition));
                Log.w(TAG, "onDragPositionsChanged");
            }

            @Override
            public void onDragUpperOutsideUp(final int oldPosition) {
                if (!imageGridView.isEditMode()) {
                    imageGridViewDynamicAdapter.notifyDataSetChanged();
                    return;
                }
                outsideDragOldPosition = oldPosition;
                imageGridView.resetHoverView();
                String photoURL = (String) imageGridViewDynamicAdapter.getItem(outsideDragOldPosition);
                photoPathListForGrid.remove(photoURL);
                //move the cover to the end of list
                photoPathListForGrid.add(getCoverPhotoPath());
                setCoverPhotoPath(photoURL);
                setCoverImageInImageView(getCoverPhotoPath());
                ArrayList<Photo> tempPhotos = new ArrayList<Photo>(photoFullList);

                for (Photo photo : photoFullList) {
                    if (photo.getUrl() == photoURL) {
                        tempPhotos.remove(photo);
                    }
                }
                tempPhotos.add(new Photo(0, getCoverPhotoPath()));
                photoFullList = new ArrayList<Photo>(tempPhotos);
                imageGridViewDynamicAdapter.set(photoPathListForGrid);
                didOutsideDrag = false;
                changePhotoRelativeLayout.setVisibility(View.GONE);
            }

            @Override
            public void onDragUpperOutsideMove(int Position) {
                if (isBeforeDragInside) {
                    changePhotoRelativeLayout.setVisibility(View.GONE);
//                    setCoverImageInImageView(getCoverPhotoPath());
                    isBeforeDragInside = false;
                }

            }

            @Override
            public void onDragUpperInsideMove(int Position) {
                if (!imageGridView.isEditMode()) {
                    return;
                } else {
                    isBeforeDragInside = true;
                    didOutsideDrag = true;
                    changePhotoRelativeLayout.setVisibility(View.VISIBLE);
//                    setCoverImageInImageView(imageGridViewDynamicAdapter.getPhotosPath().get(Position));
                }
            }
        });
        imageGridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                startEditMode(position);
                return true;
            }
        });

        imageGridView.setOnEditModeChangeListener(new DynamicGridView.OnEditModeChangeListener() {
            @Override
            public void onEditModeChanged(boolean inEditMode) {
                Log.d("onEditModeChanged", "onEditModeChanged: " + inEditMode);
                svImageSection.setScrollingEnabled(!inEditMode);
            }
        });

        updateNoImageElement();
    }

    private void setCoverImageInImageView(String coverPhotoPath) {
        Log.w(TAG, "coverPhotoPath = " + coverPhotoPath);
        Picasso.with(this)
                .load(new File(coverPhotoPath))
                .placeholder(R.drawable.progress_animation).into(coverImageView);
        coverImageView.setRotateAngle(0);

//        Bitmap bitmap = BitmapFactory.decodeFile(coverPhotoPath);
//        coverImageView.setImageBitmap(bitmap);
//        coverImageView.setRotateAngle(0);

    }

    @Override
    public void onBackPressed() {
        if (imageGridView.isEditMode()) {
            doneEditBtn.performClick();
        } else {
            savePhotoPath();
            finish();
            setResult(RESULT_CANCELED);
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent ev) {
        onTouchEvent(ev);
        return false;
    }
    private String saveCoverImageAsFile() {
        if (getCoverPhotoPath()!=null) {
            Bitmap coverImageBitmap = ImageUtil.takeViewScreenShot(coverImageFrameLayout, this);
            File fileInDevice = new File(FileUtil.getAppSubFolder(Constants.COVER_IMAGE_FOLDER_NAME),
                    "coverimage.jpg");
            try {
                FileUtil.convertBitmapToFile(fileInDevice, coverImageBitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return fileInDevice.getAbsolutePath();
        }else{
            return null;
        }
    }

    private String getCoverImagefromFile() {
        File fileInDevice = new File(FileUtil.getAppSubFolder(Constants.COVER_IMAGE_FOLDER_NAME),
                "coverimage.jpg");
        return fileInDevice.getAbsolutePath();
    }

    public String getCoverPhotoPath() {
        return coverPhotoPath;
    }

    public void setCoverPhotoPath(String coverPhotoPath) {
        if (coverPhotoPath!=null){
            noPhotoImageLayout.setVisibility(View.GONE);
        }
        this.coverPhotoPath = coverPhotoPath;
    }

    @OnClick(R.id.closeBtn)
    public void closeBtnClick(View view) {
        Dialog.exitSaveChangeDialog(this, new DialogDualCallback() {
            @Override
            public void positive() {
                //reset value
                BaseCreatePost.getInstance(CreatePostPhotosActivity.this).resetBaseCreatePost();
                exitActivity();
            }

            @Override
            public void negative() {
                //exit
                savePhotoPath();
                exitActivity();
            }
        }).show();
    }

    private void exitActivity() {
        setResult(RESULT_OK);
        finish();
    }

    @OnClick(R.id.backBtn)
    public void backBtnClick(View view) {
        onBackPressed();
    }

    @OnClick(R.id.nextBtn)
    public void nextBtnClick(View view) {
//        CreatePost currentPost = BaseCreatePost.getInstance(this).findCurrentCreatePost(getIntent().getIntExtra(Constants.EXTRA_LANG_INDEX, 2));
//        currentPost.photoPathList = photoPathList;
        savePhotoPath();
        Log.d(TAG, "test grid: " + photoFullList.size() + getCoverPhotoPath());
        if (photoFullList.size() > 0 && getCoverPhotoPath()!=null) {
            // wait for the cover image loaded
            if (!hasCoverLoaded){
                AppUtil.showToast(this,getString(R.string.error_message__please_try_again_later));
                return;
            }

            // Mixpanel
            MixpanelAPI mixpanel = MixpanelAPI.getInstance(this, AppConfig.getMixpanelToken());
            mixpanel.track("Saved Posting Details_Photos");

            Intent intent = new Intent(this, CreatePostPreviewActivity.class);
            Navigation.pushIntentForResult(this,intent, CreatePostPropertyActivity.FINISH_POST);
        } else {
            Dialog.noPhotoDialog(this).show();
        }

    }

    @OnClick(R.id.doneEditBtn)
    public void doneEditBtnClick(View view) {

        stopEditMode();
        Log.d(TAG, "test grid: " + imageGridViewDynamicAdapter.getItems().size());
    }

    @OnClick(R.id.resetBtn)
    public void resetBtnClick(View view) {
        photoFullList.clear();
        setCoverPhotoPath(null);
        photoPathListForGrid.clear();
        imageGridViewDynamicAdapter.clear();
        imageGridViewDynamicAdapter.notifyDataSetChanged();
        coverImageView.setImageBitmap(null);
        coverImageView.setRotateAngle(0);
        noPhotoImageLayout.setVisibility(View.VISIBLE);
        hasCoverLoaded=false;
        nextBtn.setEnabled(photoFullList.size()>0);
    }

    private void savePhotoPath() {
        photoFullList.clear();
        if (getCoverPhotoPath()!=null) {
            photoFullList.add(new Photo(0,saveCoverImageAsFile()));
            photoFullList.add(new Photo(0, getCoverPhotoPath()));
            for (String photoPath : imageGridViewDynamicAdapter.getPhotosPath()) {
                photoFullList.add(new Photo(0,photoPath));
            }
        }
        Log.d(TAG, "savePhotoPath: " + photoFullList.toString());
        BaseCreatePost.getInstance(this).setPhotoPathList(photoFullList);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if (resultCode == RESULT_OK && requestCode == SELECT_PHOTO) {
                ArrayList<String> photoURLs = null;
                if (data != null)
                    photoURLs = data.getStringArrayListExtra(PhotoPickerActivity.KEY_SELECTED_PHOTOS);
                if (photoURLs != null) {
                    photoFullList.clear();
                    for (int i = 0;i < photoURLs.size(); i++){
                        String url = photoURLs.get(i);
                        photoFullList.add(new Photo(i,url));
                    }
                    nextBtn.setEnabled(true);
                    Log.d(TAG, "photoFullList.get(0).url: " + photoFullList.toString());
                    Log.d(TAG, "photoFullList.get(0).url: " + String.valueOf(photoFullList.get(0)));
                    setCoverPhotoPath(photoURLs.get(0));

                    Picasso.with(this)
                            .load(new File(photoURLs.get(0)))
                            .placeholder(R.drawable.progress_animation).into(coverImageView, new Callback() {
                        @Override
                        public void onSuccess() {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {//You will get your bitmap here
                                    hasCoverLoaded=true;
                                }
                            },100);
                        }
                        @Override
                        public void onError() {
                        }
                    });

//                Bitmap bitmap = BitmapFactory.decodeFile(getCoverPhotoPath());
//                coverImageView.setImageBitmap(bitmap);
                    coverImageView.setRotateAngle(0);
                    photoFullList.add(1, photoFullList.get(0));
                    photoPathListForGrid = new ArrayList<>(photoURLs);
                    photoPathListForGrid.remove(0);
                    imageGridView.setRealImageCount(photoPathListForGrid.size());
                    imageGridViewDynamicAdapter.set(photoPathListForGrid);
                }
                updateNoImageElement();
            }
            if (resultCode == RESULT_OK && requestCode == CreatePostPropertyActivity.FINISH_POST) {
                setResult(RESULT_OK, data);
                finish();
            }
        } catch (Exception e) {
            AppUtil.showToast(this, getString(R.string.chatroom_error_message__incompatible_error)); //TODO: general error for some not supporting image
        }
    }

    private void startEditMode(final int position) {


        ImageUtil.collapse(nextBtn);
        imageGridViewDynamicAdapter.showEditView = true;
        imageGridViewDynamicAdapter.notifyDataSetChanged();
        imageGridView.post(new Runnable() {
            @Override
            public void run() {
                imageGridView.startEditMode(position);
            }
        });
        doneEditBtn.setVisibility(View.VISIBLE);
        deleteAllBtn.setVisibility(View.VISIBLE);
        closeBtn.setVisibility(View.GONE);
        backBtn.setVisibility(View.GONE);
        noPhotoImageLayout.setVisibility(View.GONE);
        isEditing = true;
        updateNoImageElement();
    }

    private void stopEditMode() {


        ImageUtil.expand(nextBtn);
        imageGridViewDynamicAdapter.showEditView = false;
        imageGridViewDynamicAdapter.notifyDataSetChanged();
        imageGridView.post(new Runnable() {
            @Override
            public void run() {
                imageGridView.stopEditMode();
            }
        });
        doneEditBtn.setVisibility(View.GONE);
        deleteAllBtn.setVisibility(View.GONE);
        closeBtn.setVisibility(View.VISIBLE);
        backBtn.setVisibility(View.VISIBLE);
        noPhotoImageLayout.setVisibility(View.VISIBLE);
        isEditing = false;
        updateNoImageElement();
    }

    private void updateNoImageElement() {

        nextBtn.setEnabled(photoFullList.size()>0);

        if (photoFullList.size() > 0 || isEditing == true) {

            noImageImageView.setVisibility(View.INVISIBLE);
            noImageTextView.setVisibility(View.INVISIBLE);

        } else {

            noImageImageView.setVisibility(View.VISIBLE);
            noImageTextView.setVisibility(View.VISIBLE);
        }
    }
}
