package co.real.productionreal2.service.model.request;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import co.real.productionreal2.Constants;
import co.real.productionreal2.CoreData;
import co.real.productionreal2.R;
import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.model.InputRequest;
import co.real.productionreal2.service.RealNetworkService;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.response.ResponseRealNetworkMemLogin;
import co.real.productionreal2.util.CryptLib;
import co.real.productionreal2.util.NetworkUtil;
import co.real.productionreal2.view.Dialog;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by hohojo on 15/12/2015.
 */
public class RequestRealNetworkLogin {
    public RequestRealNetworkLogin(int phoneRegID, String pINInput) {
        this.phoneRegID = phoneRegID;
        this.pINInput = pINInput;
    }

    @SerializedName("PhoneRegID")
    public int phoneRegID;
    @SerializedName("PINInput")
    public String pINInput;

    public void callRealNetworkLoginApi(final Context context, final ApiCallback callback) {
        String json = new Gson().toJson(this);
        String encryptJson;
        if (Constants.enableEncryption) {
            encryptJson = CryptLib.encrypt(json);
            Log.w("callRealNetworkLoginApi", "json = " + json);
        } else {
            encryptJson = json;
        }
        final RealNetworkService apiService = new RealNetworkService(context);
        apiService.getCoffeeService().memLogin(new InputRequest(encryptJson), new Callback<ApiResponse>() {
            @Override
            public void success(ApiResponse apiResponse, Response response) {
                String decryptJson;
                if (Constants.enableEncryption) {
                    decryptJson = CryptLib.decrypt(apiResponse.jsonContent);
                    Log.w("callRealNetworkLoginApi", "decryptJson = " + decryptJson);
                } else {
                    decryptJson = apiResponse.jsonContent;
                }
                ResponseRealNetworkMemLogin responseGson = new Gson().fromJson(decryptJson, ResponseRealNetworkMemLogin.class);
                Log.d("callRealNetworkLoginApi", "success: " + apiResponse.jsonContent);
                Log.d("callRealNetworkLoginApi", "success: " + responseGson.errorCode);
                Log.d("callRealNetworkLoginApi", "success: " + responseGson.errorMsg);
                //to distinguish if it is new user
                CoreData.isNewUser= (responseGson.errorCode ==22); //22 = Login as new member
                if (responseGson.errorCode == 4) {
                    callback.failure(responseGson.errorMsg);
                } else if (responseGson.errorCode == 101) {
                    callback.failure(context.getString(R.string.login__wrong_password));
                }else if (responseGson.errorCode == 26) {
                    Dialog.suspendedErrorForceLogoutDialog(context).show();
                } else if (responseGson.errorCode != 0 && responseGson.errorCode != 20 && responseGson.errorCode != 21 &&
                        responseGson.errorCode != 22 && responseGson.errorCode != 23) {
                    callback.failure(responseGson.errorMsg);
                } else {
                    callback.success(apiResponse);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                callback.failure("");
                NetworkUtil.showNoNetworkMsg(context);
            }
        });
    }
}
