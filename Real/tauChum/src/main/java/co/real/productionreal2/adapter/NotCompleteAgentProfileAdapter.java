package co.real.productionreal2.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import co.real.productionreal2.service.model.response.ResponseLoginSocial;

import java.util.List;

/**
 * Created by hohojo on 9/3/2016.
 */
public class NotCompleteAgentProfileAdapter extends FragmentStatePagerAdapter {
    private List<Fragment> fragments;

    public NotCompleteAgentProfileAdapter(FragmentManager fm, ResponseLoginSocial.Content userContent, List<Fragment> fragments) {
        super(fm);
        this.fragments = fragments;
    }

//    @Override
//    public int getItemPosition(Object object) {
//        return POSITION_NONE;
//    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }


    @Override
    public int getCount() {
        return fragments.size();
    }
}
