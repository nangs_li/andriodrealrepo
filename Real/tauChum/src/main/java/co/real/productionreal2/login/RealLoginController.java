package co.real.productionreal2.login;

import android.content.Context;

/**
 * Created by hohojo on 15/12/2015.
 */
public class RealLoginController extends BaseLoginController {
    private static final String TAG = "RealLoginController";
    private static RealLoginController instance;

    public RealLoginController(Context context) {
        super(context);
    }

    public static RealLoginController getInstance(Context context) {
        if (instance == null) {
            instance = new RealLoginController(context);
        }
        return instance;
    }
}
