package co.real.productionreal2.service.model;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.Date;

import co.real.productionreal2.R;
import co.real.productionreal2.util.StringUtils;
import co.real.productionreal2.util.TimeUtils;

public class AgentExp implements Parcelable{

	@SerializedName("ID")
	public int id;
	
	@SerializedName("Company")
	public String company;
			
	@SerializedName("Title")
	public String title;
	
	@SerializedName("IsCurrentJob")
	public int isCurrentJob;
	
	@SerializedName("StartDate")
	public String startDate;
	
	@SerializedName("EndDate")
	public String endDate;

	@SerializedName("StartDateString")
	public String startDateString;

	@SerializedName("EndDateString")
	public String endDateString;

	public AgentExp(){}
	
	public AgentExp(Parcel in) {
		company = in.readString();
		title = in.readString();
		isCurrentJob = in.readInt();
		startDate = in.readString();
		endDate = in.readString();
		startDateString = in.readString();
		endDateString = in.readString();
		id	= in.readInt();
//        bool1 = in.readByte() != 0x00;
    }
	
//	public AgentExp(String company, String title,
//			int isCurrentJob, String startDate, String endDate) {
//		super();
//		this.company = company;
//		this.title = title;
//		this.isCurrentJob = isCurrentJob;
//		this.startDate = startDate;
//		this.endDate = endDate;
//	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(company);
		dest.writeString(title);
        dest.writeInt(isCurrentJob);
        dest.writeString(startDate);
        dest.writeString(endDate);
		dest.writeString(startDateString);
		dest.writeString(endDateString);
        dest.writeInt(id);
	}

	
    public static final Parcelable.Creator<AgentExp> CREATOR = new Parcelable.Creator<AgentExp>() {
        @Override
        public AgentExp createFromParcel(Parcel in) {
            return new AgentExp(in);
        }

        @Override
        public AgentExp[] newArray(int size) {
            return new AgentExp[size];
        }
    };

	public String getExpPeriodText(Context context) {
		if (context != null) {
			String periodText = "";
			int diffYears = 0;
			int diffMonths = 0;
			int totalMonths = 0;
			Date startDate = TimeUtils.stringToDate(this.startDateString);
			Date endDate = TimeUtils.stringToDate(this.endDateString);


			SimpleDateFormat outputDateFormat = new SimpleDateFormat(context.getString(R.string.general_date_format_month_year), context.getResources().getConfiguration().locale);

			if (this.isCurrentJob == 1) {
				if (startDate != null) {
					periodText = outputDateFormat.format(startDate) + " - " + context.getResources().getString(R.string.agent_profile__to_present);
					totalMonths = TimeUtils.diffMonths(this.startDateString, TimeUtils.getCurrentDateString());
				}
			} else {
				if (startDate != null && endDate != null) {
					periodText = outputDateFormat.format(startDate) + " - " + outputDateFormat.format(endDate);
					totalMonths = TimeUtils.diffMonths(this.startDateString, this.endDateString);
				}
			}
			diffYears = totalMonths / 12;
			diffMonths = totalMonths % 12;
			if (diffYears > 0) {
				if (diffMonths <= 0) {
					diffMonths = 1;
				}
				String yearUnit = StringUtils.getById(context,R.string.agent_profile__yrs,"yrs");
				String monthUnit = StringUtils.getById(context,R.string.agent_profile__mos,"mos");
				if (diffYears <= 1){
					yearUnit = StringUtils.getById(context,R.string.agent_profile__yr,"yr");
				}

				if (diffMonths <= 1){
					monthUnit = StringUtils.getById(context,R.string.agent_profile__mo,"mo");
				}

				periodText = periodText + " (" +
						diffYears + " " + yearUnit + " " + diffMonths + " " + monthUnit + " )";
			}else if (diffMonths > 1) {
				periodText = periodText + " (" + diffMonths + " " +  StringUtils.getById(context,R.string.agent_profile__mos,"mos") + ")";
			}else {
				diffMonths = 1;
				periodText = periodText + " (" + diffMonths + " " + StringUtils.getById(context,R.string.agent_profile__mo,"mo") + ")";
			}
			return periodText;
		} else {
			return "Context Null";
		}
	}
}
