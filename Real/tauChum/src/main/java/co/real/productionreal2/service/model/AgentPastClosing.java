package co.real.productionreal2.service.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class AgentPastClosing  implements Parcelable {

	@SerializedName("ID")
	public int id;
	
	@SerializedName("Position")
	public int position;
		
	@SerializedName("PropertyType")
	public int propertyType = -1;
	
	@SerializedName("SpaceType")
	public int spaceType = -1;
	
	@SerializedName("Country")
	public String country ="";
	
	@SerializedName("State")
	public String state ="";
	
	@SerializedName("Address")
	public String address ="";
	
	@SerializedName("SoldPrice")
	public String soldPrice ="";
	
	@SerializedName("SoldDate")
	public String soldDate ="";

	@SerializedName("SoldDateString")
	public String soldDateString ="";

	public AgentPastClosing(){}
	
	public AgentPastClosing(Parcel in) {
		propertyType = in.readInt();
		spaceType = in.readInt();
		country = in.readString();
		state = in.readString();
		address = in.readString();
		soldPrice = in.readString();
		soldDate = in.readString();
		soldDateString = in.readString();
		id = in.readInt();
//        bool1 = in.readByte() != 0x00;
    }
	
	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(propertyType);
		dest.writeInt(spaceType);
        dest.writeString(country);
        dest.writeString(state);
        dest.writeString(address);
        dest.writeString(soldPrice);
        dest.writeString(soldDate);
		dest.writeString(soldDateString);
        dest.writeInt(id);
//        dest.writeByte((byte) (bool1 ? 0x01 : 0x00));
		
	}
	
//	public AgentPastClosing(int propertyType, int spaceType,
//			String country, String state, String address, String soldPrice,
//			String soldDate) {
//		super();
//		this.propertyType = propertyType;
//		this.spaceType = spaceType;
//		this.country = country;
//		this.state = state;
//		this.address = address;
//		this.soldPrice = soldPrice;
//		this.soldDate = soldDate;
//	}

	
	@SuppressWarnings("unused")
    public static final Parcelable.Creator<AgentPastClosing> CREATOR = new Parcelable.Creator<AgentPastClosing>() {
        @Override
        public AgentPastClosing createFromParcel(Parcel in) {
            return new AgentPastClosing(in);
        }

        @Override
        public AgentPastClosing[] newArray(int size) {
            return new AgentPastClosing[size];
        }
    };

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((country == null) ? 0 : country.hashCode());
		result = prime * result + id;
		result = prime * result + propertyType;
		result = prime * result
				+ ((soldDate == null) ? 0 : soldDate.hashCode());
		result = prime * result
				+ ((soldPrice == null) ? 0 : soldPrice.hashCode());
		result = prime * result + spaceType;
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AgentPastClosing other = (AgentPastClosing) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (country == null) {
			if (other.country != null)
				return false;
		} else if (!country.equals(other.country))
			return false;
		if (id != other.id)
			return false;
		if (propertyType != other.propertyType)
			return false;
		if (soldDate == null) {
			if (other.soldDate != null)
				return false;
		} else if (!soldDate.equals(other.soldDate))
			return false;
		if (soldPrice == null) {
			if (other.soldPrice != null)
				return false;
		} else if (!soldPrice.equals(other.soldPrice))
			return false;
		if (spaceType != other.spaceType)
			return false;
		if (state == null) {
			if (other.state != null)
				return false;
		} else if (!state.equals(other.state))
			return false;
		return true;
	}
	
		
}
