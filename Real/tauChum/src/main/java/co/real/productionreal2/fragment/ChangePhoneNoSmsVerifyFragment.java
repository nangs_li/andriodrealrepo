package co.real.productionreal2.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.Time;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import co.real.productionreal2.CoreData;
import co.real.productionreal2.Event.UpdateResendTimerEvent;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.setting.ChangePhoneNumberActivity;
import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.callback.DialogCallback;
import co.real.productionreal2.common.LocalStorageHelper;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.request.RequestPhoneNumberChange;
import co.real.productionreal2.service.model.request.RequestPhoneNumberChangeSmsVerify;
import co.real.productionreal2.service.model.response.ResponseChangePhoneNum;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;
import co.real.productionreal2.util.AppUtil;
import co.real.productionreal2.view.Dialog;

/**
 * Created by kelvinsun on 15/6/16.
 */
public class ChangePhoneNoSmsVerifyFragment extends Fragment {

    @InjectView(R.id.tv_phoneNumber)
    TextView tvPhoneNum;
    @InjectView(R.id.resendBtn)
    Button bnResend;
    @InjectView(R.id.activecodeEditText1)
    EditText activecodeEditText1;
    @InjectView(R.id.activecodeEditText2)
    EditText activecodeEditText2;
    @InjectView(R.id.activecodeEditText3)
    EditText activecodeEditText3;
    @InjectView(R.id.activecodeEditText4)
    EditText activecodeEditText4;

    private static final String TAG = "ChangePhoneNoSmsVerifyFrag";

    private ResponseLoginSocial.Content userContent;

    RequestPhoneNumberChangeSmsVerify requestPhoneNumberChangeSmsVerify;

    int color_gray;
    int color_bule;

    private String mCountryCodeOld, mPhoneNumOld;
    private String mCountryCode, mPhoneNum;
    private long mRegId = 0;

    /**
     * Create a new instance of CountingFragment, providing "num"
     * as an argument.
     */
    public static ChangePhoneNoSmsVerifyFragment newInstance(String countryCodeOld, String phoneNumold, String countryCode, String phoneNum, long regId) {
        ChangePhoneNoSmsVerifyFragment f = new ChangePhoneNoSmsVerifyFragment();
        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putString(ChangePhoneNumberActivity.COUNTRY_CODE_OLD, countryCodeOld);
        args.putString(ChangePhoneNumberActivity.PHONE_NUM_OLD, phoneNumold);
        args.putString(ChangePhoneNumberActivity.COUNTRY_CODE, countryCode);
        args.putString(ChangePhoneNumberActivity.PHONE_NUM, phoneNum);
        args.putLong(ChangePhoneNumberActivity.REG_ID, regId);
        f.setArguments(args);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_change_phone_no_verify, container, false);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        color_gray = ContextCompat.getColor(getActivity(), R.color.ios_dark_gray);
        color_bule = ContextCompat.getColor(getActivity(), R.color.holo_blue);
        userContent = CoreData.getUserContent(getActivity());

        if (getArguments() != null) {
            mCountryCodeOld = getArguments().getString(ChangePhoneNumberActivity.COUNTRY_CODE_OLD);
            mPhoneNumOld = getArguments().getString(ChangePhoneNumberActivity.PHONE_NUM_OLD);
            mCountryCode = getArguments().getString(ChangePhoneNumberActivity.COUNTRY_CODE);
            mPhoneNum = getArguments().getString(ChangePhoneNumberActivity.PHONE_NUM);
            mRegId = getArguments().getLong(ChangePhoneNumberActivity.REG_ID);
        }

        initView();
        addListener();
    }

    public void initView() {
        tvPhoneNum.setText(mCountryCode + " " + mPhoneNum);
    }

    void addListener() {
        activecodeEditText1.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                if (s.length() > 0) {
                    if(s.length()>1){
                        s.delete(0,1);
                    }
                    activecodeEditText2.requestFocus();
                    checkCode(activecodeEditText1);
                }
            }

        });
        activecodeEditText2.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                if (s.length() > 0) {
                    if(s.length()>1){
                        s.delete(0,1);
                    }
                    activecodeEditText3.requestFocus();
                    checkCode(activecodeEditText2);
                }
            }

        });
        activecodeEditText2.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_DEL) {
                    if (activecodeEditText2.length() == 0) {
                        activecodeEditText1.requestFocus();
                    }
                }
                return false;
            }
        });
        activecodeEditText3.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                if (s.length() > 0) {
                    if(s.length()>1){
                        s.delete(0,1);
                    }
                    activecodeEditText4.requestFocus();
                    checkCode(activecodeEditText3);
                }
            }

        });
        activecodeEditText3.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_DEL) {
                    if (activecodeEditText3.length() == 0) {
                        activecodeEditText2.requestFocus();
                    }
                }
                return false;
            }
        });
        activecodeEditText4.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                if (s.length() > 0) {
                    if(s.length()>1){
                        s.delete(0,1);
                    }
                    activecodeEditText1.requestFocus();
                    checkCode(activecodeEditText4);
                }
            }

        });
        activecodeEditText4.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_DEL) {
                    if (activecodeEditText4.length() == 0) {
                        activecodeEditText3.requestFocus();
                    }
                }
                return false;
            }
        });
    }


    void checkCode(EditText editText) {
        String code1 = activecodeEditText1.getText().toString();
        String code2 = activecodeEditText2.getText().toString();
        String code3 = activecodeEditText3.getText().toString();
        String code4 = activecodeEditText4.getText().toString();

        if (!code1.equals("") && !code2.equals("") && !code3.equals("") && !code4.equals("")) {
            AppUtil.hideKeyboard(getActivity(), editText);
            //call SMS verify API
            smsVerify();
        }
    }

    private void smsVerify() {
        final int memId = CoreData.getUserContent(getActivity()).memId;
        final String accessToken = LocalStorageHelper.getAccessToken(getActivity());
        final String regId = String.valueOf(mRegId);
        final String pin = activecodeEditText1.getText().toString() + activecodeEditText2.getText().toString() +
                activecodeEditText3.getText().toString() + activecodeEditText4.getText().toString();
        if (requestPhoneNumberChangeSmsVerify == null) {
            requestPhoneNumberChangeSmsVerify = new RequestPhoneNumberChangeSmsVerify(memId, accessToken, regId, pin);
            requestPhoneNumberChangeSmsVerify.callPhoneNumberChangeSmsVerifyApi(getActivity(), new ApiCallback() {
                @Override
                public void success(ApiResponse apiResponse) {
                    String phoneNumOld=mCountryCodeOld+" "+mPhoneNumOld;
                    String phoneNum=mCountryCode+" "+mPhoneNum;
                    Dialog.changePhoneNoSuccessDialog(getActivity(), phoneNumOld, phoneNum, new DialogCallback() {
                        @Override
                        public void yes() {
                            if (getActivity() instanceof ChangePhoneNumberActivity) {
                                ((ChangePhoneNumberActivity) getActivity()).closeActivity();
                            }
                        }
                    }).show();
                    requestPhoneNumberChangeSmsVerify = null;
                }

                @Override
                public void failure(String errorMsg) {
                    AppUtil.showAlertDialog(getActivity(), errorMsg);
                    resetPinInput();
                    requestPhoneNumberChangeSmsVerify = null;
                }
            });
        }
    }

    private void resetPinInput() {
        activecodeEditText1.setText("");
        activecodeEditText2.setText("");
        activecodeEditText3.setText("");
        activecodeEditText4.setText("");
        activecodeEditText1.requestFocus();
    }

    @Override
    public void onResume() {
        super.onResume();
        bnResend.setEnabled(false);
        bnResend.setText(getString(R.string.phone2__button) + " ");
        if (getActivity() instanceof ChangePhoneNumberActivity) {
            ((ChangePhoneNumberActivity) getActivity()).updateTitle(R.string.change_phone_no_verification__title);
            ((ChangePhoneNumberActivity) getActivity()).resendCountDown();
        }
    }

    @OnClick(R.id.resendBtn)
    public void verifyNewPhoneNoClick(View view) {
        resendSMS();
    }


    private void resendSMS() {

        final int memId = CoreData.getUserContent(getActivity()).memId;
        final String accessToken = LocalStorageHelper.getAccessToken(getActivity());
        final String countryCode = mCountryCode;
        final String phoneNumber = mPhoneNum;
        final String countryCodeOld = mCountryCodeOld;
        final String phoneNumberOld = mPhoneNumOld;
        final String locale = AppUtil.getCurrentLangCode(getActivity());

        RequestPhoneNumberChange requestPhoneNumberChange = new RequestPhoneNumberChange(memId, accessToken,
                countryCode, phoneNumber, countryCodeOld, phoneNumberOld, locale);
        requestPhoneNumberChange.callPhoneNumberChangeApi(getActivity(), new ApiCallback() {
            @Override
            public void success(ApiResponse apiResponse) {
                ResponseChangePhoneNum responseChangePhoneNum = new Gson().fromJson(apiResponse.jsonContent, ResponseChangePhoneNum.class);
                mRegId = responseChangePhoneNum.content.phoneRegId;
                if (getActivity() instanceof ChangePhoneNumberActivity) {
                    ((ChangePhoneNumberActivity) getActivity()).resendCountDown();
                }
            }

            @Override
            public void failure(String errorMsg) {
                AppUtil.showAlertDialog(getActivity(), errorMsg);
            }
        });

    }

    @Subscribe
    public void onResendTimerEvent(UpdateResendTimerEvent event) {
        long min, sec;
        String btnText = getString(R.string.phone2__button);
        min = event.min;
        sec = event.sec;

        Time time = new Time();
        time.set((int) sec, (int) min, 0, 0, 0, 0);

        if (min > 0 || sec > 0) {
            btnText += " " + time.format("%M:%S");
        }
        //update btn status
        bnResend.setEnabled(min == 0 && sec == 0);
        bnResend.setText(btnText);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }
}
