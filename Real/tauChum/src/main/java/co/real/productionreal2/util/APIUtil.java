package co.real.productionreal2.util;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.google.gson.Gson;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;
import org.greenrobot.eventbus.EventBus;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import co.real.productionreal2.CoreData;
import co.real.productionreal2.Event.UploadTxtFileEvent;
import co.real.productionreal2.activity.MainActivityTabBase;
import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.common.LocalStorageHelper;
import co.real.productionreal2.newsfeed.NewsFeedFragment;
import co.real.productionreal2.newsfeed.NewsFeedSearchFragment;
import co.real.productionreal2.service.AdminService;
import co.real.productionreal2.service.model.AgentProfiles;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.request.RequestNewsFeed;
import co.real.productionreal2.service.model.response.ResponseFileUpload;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;
import co.real.productionreal2.service.model.response.ResponseNewsFeed;
import co.real.productionreal2.view.TabMenu;

/**
 * Created by edwinchan on 15/1/16.
 */
public class APIUtil {

    static ResponseLoginSocial.Content userContent;

    static public void clear(){
        userContent = null;
    }

    static void checkUserContent(Context context) {
//        if (userContent == null) {
            userContent = CoreData.getUserContent(context);
//        }
    }

    static public void callGetNewsFeedApi(final Context context, final Fragment fragment, final int page) {
        checkUserContent(context);
        final RequestNewsFeed requestNewsFeed = new RequestNewsFeed(userContent.memId, LocalStorageHelper.getAccessToken(context), "test_uniqueKey", 1, page);

        requestNewsFeed.callNewsFeedApi(context, new ApiCallback() {
            @Override
            public void success(ApiResponse apiResponse) {
                ResponseNewsFeed responseGson = new Gson().fromJson(apiResponse.jsonContent, ResponseNewsFeed.class);
                Log.d("NewsfeedSearchFragment", "NewsfeedSearchFragment: fetchAPI: " + responseGson.content.toString());
                Log.d("NewsfeedSearchFragment", "NewsfeedSearchFragment: fetchAPI: " + responseGson.content.agentProfiles.size() + " / " + responseGson.content.recordCount);

                List<AgentProfiles> agentProfileList = new ArrayList<AgentProfiles>();
                if (page>1){
                    agentProfileList.addAll(LocalStorageHelper.getLocalAgentProfileList(context, MainActivityTabBase.CURRENT_SECTION.NEWSFEED));
                }

                agentProfileList.addAll(responseGson.content.agentProfiles);
                LocalStorageHelper.saveLocalAgentProfileListToLocalStorage(context, agentProfileList, MainActivityTabBase.CURRENT_SECTION.NEWSFEED);

                if (fragment instanceof NewsFeedFragment) {
                    ((NewsFeedFragment) fragment).setViewPager(responseGson);
                } else if (fragment instanceof NewsFeedSearchFragment) {
                    ((NewsFeedSearchFragment) fragment).updateView(responseGson,responseGson.content.recordCount==responseGson.content.totalCount);
                }


                //remove alert on tab
                LocalStorageHelper.defaultHelper(context).saveNewCountToLocalStorage(0, TabMenu.TAB_SECTION.NEWSFEED);
                // notify about new count
//                Intent intentNewCountPush = new Intent(Consts.NEWSFEED_NEW_COUNT_EVENT);
//                LocalBroadcastManager.getInstance(context).sendBroadcast(intentNewCountPush);


                //hide loading
                ((MainActivityTabBase)context).hideLoadingDialog();

            }

            @Override
            public void failure(String errorMsg) {
                Log.d("NewsfeedSearchFragment", "NewsfeedSearchFragment: fetchAPI: fail: " + errorMsg);
                if (fragment instanceof NewsFeedSearchFragment) {
                    ((NewsFeedSearchFragment) fragment).stopRefresh();
                }
                AppUtil.showToast(context,errorMsg);
                //hide loading
                        ((MainActivityTabBase) context).hideLoadingDialog();
            }
        });
    }


    /**
     * Upload txt file
     * FileType: 1 = Listing photo, 2 = Member profile photo, 3 = Reason ID 4 = Real Network member photo, 5 = Contact List
     */
    static public String uploadTxtFileApi(final Context context, ResponseLoginSocial.Content agentContent,File file,int fileType) {
        Log.d("uploadContactList","uploadTxtFileApi: "+agentContent);
        Log.d("uploadContactList","uploadTxtFileApi: "+file.getAbsolutePath());
        HttpPost httppost = new HttpPost(AdminService.WEB_SERVICE_BASE_URL + "/Admin.asmx/FileUpload");
        MultipartEntity multipartEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);

        try {
            multipartEntity.addPart("MemberID", new StringBody("" + agentContent.memId));
            multipartEntity.addPart("AccessToken", new StringBody(LocalStorageHelper.getAccessToken(context)));
            multipartEntity.addPart("ListingID", new StringBody("0"));
            multipartEntity.addPart("Position", new StringBody("0"));
            multipartEntity.addPart("FileExt", new StringBody(FileUtil.getFileExt(file.getAbsolutePath())));
            multipartEntity.addPart("FileType", new StringBody(String.valueOf(fileType)));
            multipartEntity.addPart("attachment", new FileBody(file));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        httppost.setEntity(multipartEntity);
        try {
            HttpParams params = new BasicHttpParams();
            params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
            DefaultHttpClient httpClient = new DefaultHttpClient(params);
            HttpResponse httpResponse = httpClient.execute(httppost);
            InputStream is = httpResponse.getEntity().getContent();
            String responseString = FileUtil.convertStreamToString(is);
            Document doc = FileUtil.getDomElement(responseString);
            doc.getDocumentElement().normalize();
            NodeList nodeList = doc.getElementsByTagName("string");
            Node node = nodeList.item(0);
            String json = FileUtil.nodeToString(node.getFirstChild());
            if (json != null) {
                ResponseFileUpload responseFileUpload = new Gson().fromJson(json, ResponseFileUpload.class);
                Log.d("uploadContactList","responseFileUpload: "+responseFileUpload);
                Log.d("uploadContactList","responseFileUpload: "+responseFileUpload.errorCode);
                if (responseFileUpload.errorCode==0){
                    //success
                    EventBus.getDefault().post(new UploadTxtFileEvent(true));
                }else{
                    EventBus.getDefault().post(new UploadTxtFileEvent(false));
                }

                if (responseFileUpload.content == null)
                    return null;
                if (responseFileUpload.content.photoUrl == null)
                    return null;
                return responseFileUpload.content.photoUrl;
            }
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Upload file
     */
    static public String uploadFileApi(final Context context, ResponseLoginSocial.Content agentContent,File file) {

        HttpPost httppost = new HttpPost(AdminService.WEB_SERVICE_BASE_URL + "/Admin.asmx/FileUpload");
        MultipartEntity multipartEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);

        try {
            multipartEntity.addPart("MemberID", new StringBody("" + agentContent.agentProfile.memberID));
            multipartEntity.addPart("AccessToken", new StringBody(LocalStorageHelper.getAccessToken(context)));
            multipartEntity.addPart("ListingID", new StringBody("0"));
            multipartEntity.addPart("Position", new StringBody("0"));
            multipartEntity.addPart("FileExt", new StringBody(FileUtil.getFileExt(file.getAbsolutePath())));
            multipartEntity.addPart("FileType", new StringBody("2"));
            multipartEntity.addPart("attachment", new FileBody(file));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        httppost.setEntity(multipartEntity);
        try {
            HttpParams params = new BasicHttpParams();
            params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
            DefaultHttpClient httpClient = new DefaultHttpClient(params);
            HttpResponse httpResponse = httpClient.execute(httppost);
            InputStream is = httpResponse.getEntity().getContent();
            String responseString = FileUtil.convertStreamToString(is);
            Document doc = FileUtil.getDomElement(responseString);
            doc.getDocumentElement().normalize();
            NodeList nodeList = doc.getElementsByTagName("string");
            Node node = nodeList.item(0);
            String json = FileUtil.nodeToString(node.getFirstChild());
            if (json != null) {
                ResponseFileUpload responseFileUpload = new Gson().fromJson(json, ResponseFileUpload.class);
                if (responseFileUpload.content == null)
                    return null;
                if (responseFileUpload.content.photoUrl == null)
                    return null;
                return responseFileUpload.content.photoUrl;
            }
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

}
