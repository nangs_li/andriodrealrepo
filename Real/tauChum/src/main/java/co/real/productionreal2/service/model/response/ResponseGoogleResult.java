package co.real.productionreal2.service.model.response;

import com.google.gson.annotations.SerializedName;

import co.real.productionreal2.service.model.ApiResponsePlace;

/**
 * Created by kelvinsun on 7/3/16.
 */
public class ResponseGoogleResult extends ResponseBase{

    @SerializedName("GoogleResult")
    public ApiResponsePlace googleResult;

//    public class Content {
//        @SerializedName("MemberID")
//        public int memId;

}
