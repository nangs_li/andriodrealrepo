package co.real.productionreal2.service;

import android.content.Context;

import co.real.productionreal2.Constants;
import co.real.productionreal2.model.InputRequest;
import co.real.productionreal2.service.model.ApiResponse;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.http.Body;
import retrofit.http.POST;

public class OperationService extends BaseService{


    public final static int DEVICE_TYPE_ANDROID = 2;

    public final static int SOCIAL_FACEBOOK = 1;
    public final static int SOCIAL_TWITTER = 2;
    public final static int SOCIAL_GOOGLE_AUTH2 = 3;


    //	public static final String WEB_SERVICE_BASE_URL = "http://52.24.198.180";//"http://192.168.1.138/";


    //	public static final String WEB_SERVICE_BASE_URL = "http://192.168.1.138/";
    //	CookieHeaderProvider cookieHeaderProvider = new CookieHeaderProvider();
    public final Operation operService;

    public OperationService(Context context) {
        RequestInterceptor requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestInterceptor.RequestFacade request) {
                request.addHeader("Accept", "application/json");
                request.addHeader("Content-type", "application/json");

            }
        };


        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(WEB_SERVICE_BASE_URL)
                .setRequestInterceptor(requestInterceptor)
                .setLogLevel(Constants.isDevelopment? RestAdapter.LogLevel.FULL : RestAdapter.LogLevel.NONE)
                .build();

        operService = restAdapter.create(Operation.class);
    }

    public OperationService(Context context, boolean dummy) {
        RequestInterceptor requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestInterceptor.RequestFacade request) {
                request.addHeader("Accept", "*/*");
                request.addHeader("Content-type", "multipart/form-data");

            }
        };

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(WEB_SERVICE_BASE_URL)
                .setRequestInterceptor(requestInterceptor)
                .setLogLevel(Constants.isDevelopment? RestAdapter.LogLevel.FULL : RestAdapter.LogLevel.NONE)
//                .setConverter(new MixedConverter(new SimpleXMLConverter(), new GsonConverter(gson)));
//                .setClient(new InterceptingOkClient())
//                .setClient(client)
                .build();

        operService = restAdapter.create(Operation.class);
    }


    //RealNetwork.java
    public interface Operation {


        @POST("/Operation.asmx/AddressSearch")
        public void addressSearch(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);


        @POST("/Operation.asmx/AgentSearch")
        public void agentSearch(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);

        @POST("/Operation.asmx/Follow")
        public void agentFollow(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);


        @POST("/Operation.asmx/Unfollow")
        public void agentUnfollow(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);
        ////////////////////////////////////////////////

        @POST("/Operation.asmx/ReportProblem")
        public void reportProblem(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);

        @POST("/Operation.asmx/LegalStatementGet")
        public void legalStatementGet(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);

        //////////////////////////////////////////////// NewsFeed ////////////////////////////////////////////////
        @POST("/Operation.asmx/NewsFeed")
        public void newsFeed(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);

        //////////////////////////////////////////////// Chat ////////////////////////////////////////////////
        @POST("/Operation.asmx/ChatRoomCreate")
        public void chatRoomCreate(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);

        @POST("/Operation.asmx/ChatRoomExit")
        public void chatRoomExit(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);

        @POST("/Operation.asmx/ChatRoomBlock")
        public void chatRoomBlock(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);

        @POST("/Operation.asmx/ChatRoomMute")
        public void chatRoomMute(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);

        @POST("/Operation.asmx/ChatRoomDelete")
        public void chatRoomDelete(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);

        @POST("/Operation.asmx/ChatRoomGet")
        public void chatRoomGet(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);

        @POST("/Operation.asmx/IsChatRoomMuted")
        public void isChatRoomMuted(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);

        @POST("/Operation.asmx/IsChatRoomExited")
        public void isChatRoomExited(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);

        @POST("/Operation.asmx/IsChatRoomDeleted")
        public void isChatRoomDeleted(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);

        @POST("/Operation.asmx/IsChatRoomBlocked")
        public void isChatRoomBlocked(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);

        @POST("/Operation.asmx/SendSMSViaTwilio")
        public void sendSMSViaTwilio(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);
        //////////////////////////////////////////////// Chat ////////////////////////////////////////////////

        //////////////////////////////////////////////// Tracking  ////////////////////////////////////////////////
        @POST("/Operation.asmx/AddViewedListing")
        public void addViewedListing(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);
        //////////////////////////////////////////////// Tracking ////////////////////////////////////////////////

        //fetch Activity Log
        @POST("/Operation.asmx/ActivityLogGet")
        public void activityLogGet(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);


        //update device token
        @POST("/Admin.asmx/MemberProfileDeviceTokenUpdate")
        public void updateDeviceToken(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);

        @POST("/Operation.asmx/AgentListingGetList")
        public void agentListingGetList(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);

        @POST("/Operation.asmx/AddressSearchRetrieveAgentProfile")
        public void addressSearchRetrieveAgentProfile(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);

        @POST("/Operation.asmx/FollowingListGet")
        public void followingListGet(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);

        @POST("/Operation.asmx/FollowInfoGet")
        public void followInfoGet(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);

        @POST("/Operation.asmx/ViralLogWrite")
        public void viralLogWrite(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);

        @POST("/Operation.asmx/Version")
        public void version(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);


        @POST("/Operation.asmx/MatchContactGet")
        public void matchContactGet(
                @Body InputRequest body, retrofit.Callback<ApiResponse> callback);

    }


    public Operation getCoffeeService() {
        return operService;
    }


}
