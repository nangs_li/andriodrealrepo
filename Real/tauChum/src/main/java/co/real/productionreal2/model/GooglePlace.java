package co.real.productionreal2.model;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class GooglePlace {

	
	@SerializedName("description")
	public String description;
	
	@SerializedName("id")
	public String id;
		
	@SerializedName("matched_substrings")
	public List<OffsetValue> matchedSubStrList;
	
	@SerializedName("place_id")
	public String placeId;
	
	@SerializedName("reference")
	public String reference;
	
	@SerializedName("terms")
	public List<Term> termsList;
	
	@SerializedName("types")
	public List<String> typesList;
		
    public class OffsetValue {
    	
    	@SerializedName("length")
    	public int length;
    	
    	@SerializedName("offset")
    	public int offset; 
    	    	
    }
	
  
    public class Term {
    	
    	@SerializedName("value")
    	public String value;
    	
    	@SerializedName("offset")
    	public int offset; 
    	    	
    }
}
