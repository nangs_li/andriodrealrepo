package co.real.productionreal2.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import co.real.productionreal2.Navigation;
import co.real.productionreal2.R;
import co.real.productionreal2.util.ImageUtil;
import lib.blurbehind.OnBlurCompleteListener;


/**
 * Created by alexhung on 21/4/16.
 */
public class BaseFragmentActivity extends FragmentActivity implements OnBlurCompleteListener {
    protected static final int REQUEST_STORAGE_READ_ACCESS_PERMISSION = 101;
    protected static final int REQUEST_STORAGE_WRITE_ACCESS_PERMISSION = 102;
    private boolean didApplyBlurredBackground = false;
    private Navigation.NavigationType navigationType;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.navigationType = Navigation.getNavigationTypeFromIntent(getIntent());
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
        ImageUtil.getInstance().blurCompleteListener = this;
        applyBlurredBackground();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onBlurComplete() {
        didApplyBlurredBackground = true;
        applyBlurredBackground();
    }

    private void applyBlurredBackground(){
        if (ImageUtil.getInstance().hasBluredBackground()){
            ImageUtil.getInstance().applyBlurBackground(this);
        }
    }

    @Override
    public void finish() {
        super.finish();
        ImageUtil.getInstance().clearBlurredBackground(this);
        if (this.navigationType == Navigation.NavigationType.Push) {
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        } else if (this.navigationType == Navigation.NavigationType.Present) {
            overridePendingTransition(R.anim.fade_in, R.anim.slide_bottom_down);
        }else if (this.navigationType == Navigation.NavigationType.Fade){
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        }
    }

}
