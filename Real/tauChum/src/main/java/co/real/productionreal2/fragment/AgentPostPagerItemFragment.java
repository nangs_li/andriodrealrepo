package co.real.productionreal2.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;
import butterknife.InjectView;
import co.real.productionreal2.Constants;
import co.real.productionreal2.CoreData;
import co.real.productionreal2.R;
import co.real.productionreal2.service.model.request.RequestListing;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;

/**
 * Created by hohojo on 4/11/2015.
 */
public class AgentPostPagerItemFragment extends Fragment {
    @InjectView(R.id.addressDetailTextView)
    TextView addressDetailTextView;
    @InjectView(R.id.propertyTypeImageView)
    ImageView propertyTypeImageView;
    @InjectView(R.id.propertyTypeTextView)
    TextView propertyTypeTextView;
    @InjectView(R.id.priceTextView)
    TextView priceTextView;
    @InjectView(R.id.sizeTextView)
    TextView sizeTextView;
    @InjectView(R.id.bedroomCountTextView)
    TextView bedroomCountTextView;
    @InjectView(R.id.bathroomCountTextView)
    TextView bathroomCountTextView;
    @InjectView(R.id.reason1ImageView)
    ImageView reason1ImageView;
    @InjectView(R.id.reason2ImageView)
    ImageView reason2ImageView;
    @InjectView(R.id.reason3ImageView)
    ImageView reason3ImageView;
    @InjectView(R.id.reason1TextView)
    TextView reason1TextView;
    @InjectView(R.id.reason2TextView)
    TextView reason2TextView;
    @InjectView(R.id.reason3TextView)
    TextView reason3TextView;
    @InjectView(R.id.reason2LinearLayout)
    LinearLayout reason2LinearLayout;
    @InjectView(R.id.reason3LinearLayout)
    LinearLayout reason3LinearLayout;
    ResponseLoginSocial.Content agentContent;

    private int langIndex;

    public static AgentPostPagerItemFragment newInstance(Integer langIndex) {

        Bundle args = new Bundle();
        args.putInt(Constants.EXTRA_LANG_INDEX, langIndex);
        AgentPostPagerItemFragment fragment = new AgentPostPagerItemFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        agentContent = CoreData.getUserContent(getActivity());
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        langIndex = getArguments().getInt(Constants.EXTRA_LANG_INDEX, 0);
        View view = inflater.inflate(R.layout.preview_detail_item, container,
                false);
        ButterKnife.inject(this, view);
        initView();
        return view;
    }

    private void initView() {
        if (agentContent == null)
            return;
        if (agentContent.agentProfile == null)
            return;
        if (agentContent.agentProfile.agentListing == null)
            return;
        priceTextView.setText("" + agentContent.agentProfile.agentListing.currency + " " + agentContent.agentProfile.agentListing.propertyPrice);
        sizeTextView.setText("" + agentContent.agentProfile.agentListing.propertySize + " " + agentContent.agentProfile.agentListing.sizeUnit);
        bedroomCountTextView.setText("" + agentContent.agentProfile.agentListing.bedroomCount);
        bathroomCountTextView.setText("" + agentContent.agentProfile.agentListing.bathroomCount);

        for (RequestListing.Reason reason : agentContent.agentProfile.agentListing.reasons) {
            if (reason.languageIndex == langIndex) {
                if (reason.reason2 != null && !reason.reason2.isEmpty()) {
                    reason2LinearLayout.setVisibility(View.VISIBLE);
                }
                if (reason.reason3 != null && !reason.reason3.isEmpty()) {
                    reason3LinearLayout.setVisibility(View.VISIBLE);
                }
                reason1TextView.setText(String.valueOf(reason.reason1));
                reason2TextView.setText(String.valueOf(reason.reason2));
                reason3TextView.setText(String.valueOf(reason.reason3));
            }
        }

        for (RequestListing.Reason reason : agentContent.agentProfile.agentListing.reasons) {
            if (reason.mediaURL1 != null &&
                    !reason.mediaURL1.isEmpty())
                Picasso.with(getActivity())
                        .load(reason.mediaURL1)
                        .placeholder(R.drawable.com_facebook_profile_picture_blank_square)
                        .error(R.drawable.com_facebook_profile_picture_blank_square)
                        .into(reason1ImageView);

            if (reason.mediaURL2 != null &&
                    !reason.mediaURL2.isEmpty())
                Picasso.with(getActivity())
                        .load(reason.mediaURL2)
                        .placeholder(R.drawable.com_facebook_profile_picture_blank_square)
                        .error(R.drawable.com_facebook_profile_picture_blank_square)
                        .into(reason2ImageView);

            if (reason.mediaURL3 != null &&
                    !reason.mediaURL3.isEmpty())
                Picasso.with(getActivity())
                        .load(reason.mediaURL3)
                        .placeholder(R.drawable.com_facebook_profile_picture_blank_square)
                        .error(R.drawable.com_facebook_profile_picture_blank_square)
                        .into(reason3ImageView);
        }

//        for (AgentListing.Photo photo : agentContent.agentProfile.agentListing.photos) {
//            if (photo.photoID == Constants.UPLOAD_REASON_ID_PHOTO) {
//                switch (photo.position) {
//                    case 1:
//                        Picasso.with(getActivity())
//                                .load(photo.url)
//                                .placeholder(R.drawable.com_facebook_profile_picture_blank_square)
//                                .error(R.drawable.com_facebook_profile_picture_blank_square)
//                                .into(reason1ImageView);
//                        break;
//                    case 2:
//                        Picasso.with(getActivity())
//                                .load(photo.url)
//                                .placeholder(R.drawable.com_facebook_profile_picture_blank_square)
//                                .error(R.drawable.com_facebook_profile_picture_blank_square)
//                                .into(reason2ImageView);
//                        break;
//                    case 3:
//                        Picasso.with(getActivity())
//                                .load(photo.url)
//                                .placeholder(R.drawable.com_facebook_profile_picture_blank_square)
//                                .error(R.drawable.com_facebook_profile_picture_blank_square)
//                                .into(reason3ImageView);
//                        break;
//                }
//
//            } else if (photo.photoID == Constants.UPLOAD_LISTING_PHOTO) {
//
//            }
//        }
        String street = "";
        if (agentContent.agentProfile.agentListing.googleAddresses.get(0).address != null)
            street = agentContent.agentProfile.agentListing.googleAddresses.get(0).address;
        addressDetailTextView.setText(street);

    }

}
