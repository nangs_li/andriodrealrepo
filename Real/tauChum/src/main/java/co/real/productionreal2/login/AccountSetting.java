package co.real.productionreal2.login;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import co.real.productionreal2.CoreData;
import co.real.productionreal2.activity.createpost.BaseCreatePost;
import co.real.productionreal2.activity.login.IntroActivity;
import co.real.productionreal2.chat.ChatSessionUtil;
import co.real.productionreal2.chat.pushnotifications.PlayServicesHelper;
import co.real.productionreal2.common.LocalStorageHelper;
import co.real.productionreal2.daomanager.DatabaseManager;
import co.real.productionreal2.model.UserQBUser;
import co.real.productionreal2.setting.SearchFilterSetting;

/**
 * Created by hohojo on 4/11/2015.
 */
public class AccountSetting {

    public static void logout(Context context) {
        // need call AccountSetting.clearDataBeforeLogoutAction(context); first
        Log.i("","edwin AccountSetting logout");
        toLoginPage(context);
    }

    public static void clearDataBeforeLogoutAction(Context context) {
        Log.i("","edwin AccountSetting clearDataBeforeLogout");
        PlayServicesHelper.clear();
        logoutQBSetting(context);
        deleteLoginInfoDB(context);
        removeLocalStorage(context);
        SearchFilterSetting.getInstance().clear();
        CoreData.isNewUser=true;
        //reset value
        BaseCreatePost.getInstance(context).resetBaseCreatePost();
        CoreData.clear();
    }

    private static void removeLocalStorage(Context context) {
        LocalStorageHelper.defaultHelper(context).removeAll();
    }

    public static void toLoginPage(Context context) {
        Intent loginIntent = new Intent(context, IntroActivity.class);
        loginIntent.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(loginIntent);
        ((Activity)context).finish();
    }

    public static void logoutQBSetting(Context context) {
        ChatSessionUtil.logoutQBuser(context);
        ChatSessionUtil.unsubscriptQBPush(context);
        DatabaseManager.getInstance(context).dropDatabase();
        UserQBUser.qbUser = null;
        UserQBUser.isLogin = false;
    }

    public static void deleteLoginInfoDB(Context context) {
        DatabaseManager.getInstance(context).deleteAllLogInfo();
    }


}
