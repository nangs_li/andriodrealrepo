package co.real.productionreal2.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import co.real.productionreal2.Constants;
import co.real.productionreal2.R;
import co.real.productionreal2.model.SystemSetting;
import co.real.productionreal2.service.model.AgentPastClosing;
import co.real.productionreal2.util.AppUtil;
import co.real.productionreal2.util.FileUtil;
import co.real.productionreal2.util.TimeUtils;
import co.real.productionreal2.view.Dialog;
import co.real.productionreal2.view.SpaceTypeItem;

/**
 * Created by hohojo on 11/9/2015.
 */
public class AgentTop5PastClosingActivity extends BaseActivity {
    @InjectView(R.id.rootView)
    View rootView;
    @InjectView(R.id.addressEditText)
    EditText addressEditText;
    @InjectView(R.id.closingDateTextView)
    TextView closingDateTextView;
    @InjectView(R.id.soldPriceEditText)
    EditText soldPriceEditText;
    @InjectView(R.id.propertyLinearLayout)
    protected LinearLayout propertyLinearLayout;
    @InjectView(R.id.horizontalScrollView)
    HorizontalScrollView propertyScollView;
    @InjectView(R.id.saveBtn)
    Button saveBtn;
    private String spaceTypeStr;
    private AgentPastClosing agentPastClosing;
    private ArrayList<SystemSetting.PropertyType> propertyTypeList;
    private Calendar soldDateCalendar = Calendar.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agent_top5_past_closing);

        ButterKnife.inject(this);
        agentPastClosing = new AgentPastClosing();
        initView();
        addListener();


    }

    private void addListener() {
        addressEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                updateUI();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        soldPriceEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                updateUI();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }


    private void initView() {
        Intent intent = getIntent();
        if (intent.getParcelableArrayListExtra(Constants.EXTRA_AGENT_PROPERTY_TYPE_LIST) != null) {
            propertyTypeList = intent.getParcelableArrayListExtra(Constants.EXTRA_AGENT_PROPERTY_TYPE_LIST);
        }

        ///add space type in horizontal scroll layout
        final List<SystemSetting.SpaceType> spaceTypeList = FileUtil.getAllSpaceType(this, AppUtil.getCurrentLangIndex(this));

        propertyLinearLayout.removeAllViews();
        SpaceTypeItem firstSpaceTypeItem = null;
        for (SystemSetting.SpaceType spaceType : spaceTypeList) {
            final SpaceTypeItem spaceTypeItem = new SpaceTypeItem(this, spaceType);
            spaceTypeItem.setTag(spaceType.propertyTypeIndex + "," + spaceType.index);
            spaceTypeItem.updateUI(spaceType);
            //to check if it is checked in local storage
            boolean isSelected = false;
            spaceTypeItem.setSelected(isSelected);
            spaceTypeItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //single selection
                    resetSpaceTypeSelection();
                    v.setSelected(!v.isSelected());
                    if (v.isSelected()) {
                        spaceTypeStr = (String) v.getTag();
                        String[] split = spaceTypeStr.split(",");
                        agentPastClosing.spaceType = Integer.parseInt(split[1]);
                        agentPastClosing.propertyType = Integer.parseInt(split[0]);
                    } else {
                        spaceTypeStr = (String) v.getTag();
                    }
                }
            });
            if (firstSpaceTypeItem == null) {
                firstSpaceTypeItem = spaceTypeItem;
            }
            propertyLinearLayout.addView(spaceTypeItem);
        }

        if (firstSpaceTypeItem != null) {
            firstSpaceTypeItem.performClick();
        }

        performPropertScrollViewAnimation();

        closingDateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Dialog().datePickerDialogWithoutDay(
                        AgentTop5PastClosingActivity.this,
                        soldDate,
                        soldDateCalendar.get(Calendar.YEAR),
                        soldDateCalendar.get(Calendar.MONTH)
                ).show();
            }
        });

        updateUI();
    }

    private void performPropertScrollViewAnimation() {
        if (propertyScollView != null) {
            propertyScollView.post(new Runnable() {
                @Override
                public void run() {
                    Rect mTempRect = new Rect();
                    int width = propertyScollView.getWidth();

                    mTempRect.left = 0;
                    mTempRect.right = width;
                    int count = propertyScollView.getChildCount();
                    if (count > 0) {
                        View view = propertyScollView.getChildAt(0);
                        mTempRect.right = view.getRight();
                        mTempRect.left = mTempRect.right - width;
                    }
                    propertyScollView.scrollTo(mTempRect.right, 0);
                    propertyScollView.fullScroll(HorizontalScrollView.FOCUS_LEFT);
                }

            });
        }
    }

    private void resetSpaceTypeSelection() {
        for (int i = 0; i < propertyLinearLayout.getChildCount(); i++) {
            propertyLinearLayout.getChildAt(i).setSelected(false);
        }
    }


    private void updateUI() {
        agentPastClosing.address = addressEditText.getText().toString();
        agentPastClosing.soldPrice = soldPriceEditText.getText().toString();
        agentPastClosing.soldDate = TimeUtils.calendarToString(soldDateCalendar, "yyyy-MM-dd");
        agentPastClosing.soldDateString = agentPastClosing.soldDate;
        if (closingDateTextView.getText().toString().equalsIgnoreCase(getString(R.string.add_experience__MM_YY)) ||
                addressEditText.getText().toString().toString().isEmpty() ||
                closingDateTextView.getText().toString().isEmpty() ||
                agentPastClosing.spaceType == -1 ||
                agentPastClosing.propertyType == -1) {
            saveBtn.setEnabled(false);
        } else {
            saveBtn.setEnabled(true);
        }
    }

    @OnClick(R.id.backBtn)
    public void closeBtn(View view) {
        setResult(RESULT_CANCELED);
        finish();
    }

    @OnClick(R.id.saveBtn)
    public void saveBtn(View view) {
        Intent intent = new Intent();
        intent.putExtra(Constants.EXTRA_AGENT_PAST_CLOSING, agentPastClosing);

        setResult(RESULT_OK, intent);
        finish();


    }

    DatePickerDialog.OnDateSetListener soldDate = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            soldDateCalendar.set(Calendar.YEAR, year);
            soldDateCalendar.set(Calendar.MONTH, monthOfYear);
            soldDateCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            closingDateTextView.setText(TimeUtils.calendarToString(soldDateCalendar, getString(R.string.general_date_format_month_year)));
            updateUI();
        }

    };
}
