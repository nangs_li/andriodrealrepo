package co.real.productionreal2.dao.account;

import java.util.List;
import java.util.ArrayList;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.Property;
import de.greenrobot.dao.internal.SqlUtils;
import de.greenrobot.dao.internal.DaoConfig;

import co.real.productionreal2.dao.account.DBQBUser;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table DBQBUSER.
*/
public class DBQBUserDao extends AbstractDao<DBQBUser, Long> {

    public static final String TABLENAME = "DBQBUSER";

    /**
     * Properties of entity DBQBUser.<br/>
     * Can be used for QueryBuilder and for referencing column names.
    */
    public static class Properties {
        public final static Property Id = new Property(0, Long.class, "id", true, "_id");
        public final static Property UserID = new Property(1, Integer.class, "UserID", false, "USER_ID");
        public final static Property MemberID = new Property(2, Integer.class, "MemberID", false, "MEMBER_ID");
        public final static Property Name = new Property(3, String.class, "Name", false, "NAME");
        public final static Property QBUser = new Property(4, String.class, "QBUser", false, "QBUSER");
        public final static Property EnableStatus = new Property(5, Integer.class, "EnableStatus", false, "ENABLE_STATUS");
        public final static Property EnableReadMsg = new Property(6, Integer.class, "EnableReadMsg", false, "ENABLE_READ_MSG");
        public final static Property IsHidden = new Property(7, Boolean.class, "isHidden", false, "IS_HIDDEN");
        public final static Property IsClear = new Property(8, Boolean.class, "isClear", false, "IS_CLEAR");
        public final static Property ChatroomAgentProfilesId = new Property(9, long.class, "chatroomAgentProfilesId", false, "CHATROOM_AGENT_PROFILES_ID");
    };

    private DaoSession daoSession;


    public DBQBUserDao(DaoConfig config) {
        super(config);
    }
    
    public DBQBUserDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
        this.daoSession = daoSession;
    }

    /** Creates the underlying database table. */
    public static void createTable(SQLiteDatabase db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "'DBQBUSER' (" + //
                "'_id' INTEGER PRIMARY KEY AUTOINCREMENT ," + // 0: id
                "'USER_ID' INTEGER," + // 1: UserID
                "'MEMBER_ID' INTEGER," + // 2: MemberID
                "'NAME' TEXT," + // 3: Name
                "'QBUSER' TEXT," + // 4: QBUser
                "'ENABLE_STATUS' INTEGER," + // 5: EnableStatus
                "'ENABLE_READ_MSG' INTEGER," + // 6: EnableReadMsg
                "'IS_HIDDEN' INTEGER," + // 7: isHidden
                "'IS_CLEAR' INTEGER," + // 8: isClear
                "'CHATROOM_AGENT_PROFILES_ID' INTEGER NOT NULL );"); // 9: chatroomAgentProfilesId
    }

    /** Drops the underlying database table. */
    public static void dropTable(SQLiteDatabase db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "'DBQBUSER'";
        db.execSQL(sql);
    }

    /** @inheritdoc */
    @Override
    protected void bindValues(SQLiteStatement stmt, DBQBUser entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
 
        Integer UserID = entity.getUserID();
        if (UserID != null) {
            stmt.bindLong(2, UserID);
        }
 
        Integer MemberID = entity.getMemberID();
        if (MemberID != null) {
            stmt.bindLong(3, MemberID);
        }
 
        String Name = entity.getName();
        if (Name != null) {
            stmt.bindString(4, Name);
        }
 
        String QBUser = entity.getQBUser();
        if (QBUser != null) {
            stmt.bindString(5, QBUser);
        }
 
        Integer EnableStatus = entity.getEnableStatus();
        if (EnableStatus != null) {
            stmt.bindLong(6, EnableStatus);
        }
 
        Integer EnableReadMsg = entity.getEnableReadMsg();
        if (EnableReadMsg != null) {
            stmt.bindLong(7, EnableReadMsg);
        }
 
        Boolean isHidden = entity.getIsHidden();
        if (isHidden != null) {
            stmt.bindLong(8, isHidden ? 1l: 0l);
        }
 
        Boolean isClear = entity.getIsClear();
        if (isClear != null) {
            stmt.bindLong(9, isClear ? 1l: 0l);
        }
        stmt.bindLong(10, entity.getChatroomAgentProfilesId());
    }

    @Override
    protected void attachEntity(DBQBUser entity) {
        super.attachEntity(entity);
        entity.__setDaoSession(daoSession);
    }

    /** @inheritdoc */
    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }    

    /** @inheritdoc */
    @Override
    public DBQBUser readEntity(Cursor cursor, int offset) {
        DBQBUser entity = new DBQBUser( //
            cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // id
            cursor.isNull(offset + 1) ? null : cursor.getInt(offset + 1), // UserID
            cursor.isNull(offset + 2) ? null : cursor.getInt(offset + 2), // MemberID
            cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3), // Name
            cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4), // QBUser
            cursor.isNull(offset + 5) ? null : cursor.getInt(offset + 5), // EnableStatus
            cursor.isNull(offset + 6) ? null : cursor.getInt(offset + 6), // EnableReadMsg
            cursor.isNull(offset + 7) ? null : cursor.getShort(offset + 7) != 0, // isHidden
            cursor.isNull(offset + 8) ? null : cursor.getShort(offset + 8) != 0, // isClear
            cursor.getLong(offset + 9) // chatroomAgentProfilesId
        );
        return entity;
    }
     
    /** @inheritdoc */
    @Override
    public void readEntity(Cursor cursor, DBQBUser entity, int offset) {
        entity.setId(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setUserID(cursor.isNull(offset + 1) ? null : cursor.getInt(offset + 1));
        entity.setMemberID(cursor.isNull(offset + 2) ? null : cursor.getInt(offset + 2));
        entity.setName(cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3));
        entity.setQBUser(cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4));
        entity.setEnableStatus(cursor.isNull(offset + 5) ? null : cursor.getInt(offset + 5));
        entity.setEnableReadMsg(cursor.isNull(offset + 6) ? null : cursor.getInt(offset + 6));
        entity.setIsHidden(cursor.isNull(offset + 7) ? null : cursor.getShort(offset + 7) != 0);
        entity.setIsClear(cursor.isNull(offset + 8) ? null : cursor.getShort(offset + 8) != 0);
        entity.setChatroomAgentProfilesId(cursor.getLong(offset + 9));
     }
    
    /** @inheritdoc */
    @Override
    protected Long updateKeyAfterInsert(DBQBUser entity, long rowId) {
        entity.setId(rowId);
        return rowId;
    }
    
    /** @inheritdoc */
    @Override
    public Long getKey(DBQBUser entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    /** @inheritdoc */
    @Override    
    protected boolean isEntityUpdateable() {
        return true;
    }
    
    private String selectDeep;

    protected String getSelectDeep() {
        if (selectDeep == null) {
            StringBuilder builder = new StringBuilder("SELECT ");
            SqlUtils.appendColumns(builder, "T", getAllColumns());
            builder.append(',');
            SqlUtils.appendColumns(builder, "T0", daoSession.getDBChatroomAgentProfilesDao().getAllColumns());
            builder.append(" FROM DBQBUSER T");
            builder.append(" LEFT JOIN DBCHATROOM_AGENT_PROFILES T0 ON T.'CHATROOM_AGENT_PROFILES_ID'=T0.'_id'");
            builder.append(' ');
            selectDeep = builder.toString();
        }
        return selectDeep;
    }
    
    protected DBQBUser loadCurrentDeep(Cursor cursor, boolean lock) {
        DBQBUser entity = loadCurrent(cursor, 0, lock);
        int offset = getAllColumns().length;

        DBChatroomAgentProfiles chatroomAgentProfiles = loadCurrentOther(daoSession.getDBChatroomAgentProfilesDao(), cursor, offset);
         if(chatroomAgentProfiles != null) {
            entity.setChatroomAgentProfiles(chatroomAgentProfiles);
        }

        return entity;    
    }

    public DBQBUser loadDeep(Long key) {
        assertSinglePk();
        if (key == null) {
            return null;
        }

        StringBuilder builder = new StringBuilder(getSelectDeep());
        builder.append("WHERE ");
        SqlUtils.appendColumnsEqValue(builder, "T", getPkColumns());
        String sql = builder.toString();
        
        String[] keyArray = new String[] { key.toString() };
        Cursor cursor = db.rawQuery(sql, keyArray);
        
        try {
            boolean available = cursor.moveToFirst();
            if (!available) {
                return null;
            } else if (!cursor.isLast()) {
                throw new IllegalStateException("Expected unique result, but count was " + cursor.getCount());
            }
            return loadCurrentDeep(cursor, true);
        } finally {
            cursor.close();
        }
    }
    
    /** Reads all available rows from the given cursor and returns a list of new ImageTO objects. */
    public List<DBQBUser> loadAllDeepFromCursor(Cursor cursor) {
        int count = cursor.getCount();
        List<DBQBUser> list = new ArrayList<DBQBUser>(count);
        
        if (cursor.moveToFirst()) {
            if (identityScope != null) {
                identityScope.lock();
                identityScope.reserveRoom(count);
            }
            try {
                do {
                    list.add(loadCurrentDeep(cursor, false));
                } while (cursor.moveToNext());
            } finally {
                if (identityScope != null) {
                    identityScope.unlock();
                }
            }
        }
        return list;
    }
    
    protected List<DBQBUser> loadDeepAllAndCloseCursor(Cursor cursor) {
        try {
            return loadAllDeepFromCursor(cursor);
        } finally {
            cursor.close();
        }
    }
    

    /** A raw-style query where you can pass any WHERE clause and arguments. */
    public List<DBQBUser> queryDeep(String where, String... selectionArg) {
        Cursor cursor = db.rawQuery(getSelectDeep() + where, selectionArg);
        return loadDeepAllAndCloseCursor(cursor);
    }
 
}
