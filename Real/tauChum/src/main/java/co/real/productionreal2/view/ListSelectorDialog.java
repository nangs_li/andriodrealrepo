package co.real.productionreal2.view;

import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by edwinchan on 17/5/16.
 */
public class ListSelectorDialog {
    Context context;
    Builder adb;
    String title;

    // our interface so we can return the selected key/item pair.
    public interface listSelectorInterface {
        void selectedItem(int index, String item);
    }

    public ListSelectorDialog(Context c) {
        this.context = c;
    }
    public ListSelectorDialog(Context c, String newTitle) {
        this.context = c;
        this.title = newTitle;
    }

    public ListSelectorDialog setTitle(String newTitle) {
        this.title = newTitle;
        return this;
    }

    public ListSelectorDialog show(ArrayList il, final listSelectorInterface di) {
        String l[] = null;
        l = (String[]) il.toArray(new String[il.size()]);

        show(l, l, di);
        return this;
    }

    public ListSelectorDialog show(ArrayList il, ArrayList ik,
                            final listSelectorInterface di) {
        // convert the ArrayList's to standard Java arrays.
        String l[] = null; String k[] = null;
        l = (String[]) il.toArray(new String[il.size()]);
        k = (String[]) ik.toArray(new String[ik.size()]);

        show(l, k, di);
        return this;
    }

    public ListSelectorDialog show(HashMap hashmap, final listSelectorInterface di) {
        // convert the hashmap to lists
        String[] il = new String[hashmap.size()];
        String[] ik = new String[hashmap.size()];
        // HashMap iteration
        int i = 0;
        for (Object key: hashmap.keySet()) {
            il[i] = key.toString();
            ik[i] = hashmap.get(key).toString();
            i++;
        }
        // now show the selection dialog
        show(il, ik, di);
        return this;
    }

    public ListSelectorDialog show(final String[] itemList, final listSelectorInterface di) {
        // if only 1 list supplied, the list serves as both keys and values.
        show(itemList, itemList, di);
        return this;
    }

    public ListSelectorDialog show(final String[] itemList, final String[] keyList,
                            final listSelectorInterface di) {
        // set up the dialog
        adb = new Builder(context);
        adb.setCancelable(true);
        adb.setItems(itemList, new DialogInterface.OnClickListener() {
            // when an item is clicked, notify our interface
            public void onClick(DialogInterface d, int which) {
                d.dismiss();
                di.selectedItem(which, itemList[which]);
            }
        });
//        adb.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//            // when user clicks cancel, notify our interface
//            public void onClick(DialogInterface d, int n) {
//                d.dismiss();
//                di.selectorCanceled();
//            }
//        });
        adb.setTitle(title);
        // show the dialog
        adb.show();
        return this;
    }
}