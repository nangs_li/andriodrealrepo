package co.real.productionreal2.callback;

import co.real.productionreal2.dao.account.DBQBChatDialog;

import java.util.List;

/**
 * Created by hohojo on 15/7/2015.
 */
public interface ChatLobbyDbViewUpdateListener {
    public void reloadDialogDbAndView(String dialogId);
    public void updateDialogView(List<DBQBChatDialog> dbqbChatDialogs);
}
