package co.real.productionreal2.util;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import co.real.productionreal2.CoreData;
import co.real.productionreal2.model.GoogleAddress;
import co.real.productionreal2.model.SystemSetting;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;

/**
 * Created by kelvinsun on 29/12/15.
 */
public class GoogleGeoUtil {

    public static String getTypeFromGoogleAddress(GoogleAddress googleAddress) {
        List<GoogleAddress.AddressComponent> addressComponents = googleAddress.googleAddressList;
        String countryShortName = "";
        for (int i = 0; i < addressComponents.size(); i++) {
            List<String> currTypes = addressComponents.get(i).types;
            for (String currType : currTypes) {
                currType.equals("country");
                countryShortName = addressComponents.get(i).shortName;
                break;
            }
        }
        return countryShortName;

    }

    public static  ArrayList<String> getCountryLangList(Context context,String countryShortName) {
        ArrayList<String> langList = new ArrayList<>();
        ResponseLoginSocial.Content agentContent = CoreData.getUserContent(context);
        List<SystemSetting.GoogleAddressLanguage> googleAddressLangList = agentContent.systemSetting.googleAddressLanguageList;
        for (int i = 0; i < googleAddressLangList.size(); i++) {
            if (googleAddressLangList.get(i).country.equalsIgnoreCase(countryShortName)) {
                return googleAddressLangList.get(i).langList;
            }
        }
        langList.add("en");
        return langList;
    }
}
