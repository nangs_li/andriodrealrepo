package co.real.productionreal2.activity.viral;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.mixpanel.android.mpmetrics.MixpanelAPI;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import co.real.productionreal2.Constants;
import co.real.productionreal2.CoreData;
import co.real.productionreal2.Navigation;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.BaseActivity;
import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.common.LocalStorageHelper;
import co.real.productionreal2.config.AppConfig;
import co.real.productionreal2.fragment.ViralSuccessFragment;
import co.real.productionreal2.model.SystemSetting;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.request.RequestSendSMSViaTwilio;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;
import co.real.productionreal2.util.AppUtil;
import co.real.productionreal2.util.ImageUtil;
import co.real.productionreal2.view.Dialog;
import io.branch.indexing.BranchUniversalObject;
import io.branch.referral.Branch;
import io.branch.referral.BranchError;
import io.branch.referral.util.LinkProperties;
import lib.blurbehind.OnBlurCompleteListener;
import widget.RoundedImageView;


public class InviteToChatActivity extends BaseActivity {
    private static final String TAG = "InviteToChatActivity";
    private static final int REQUEST_CODE_VIRAL=100;
    @InjectView(R.id.TextView_tnc)
    TextView TextView_tnc;
    @InjectView(R.id.countryCodeBtn)
    Button countryCodeBtn;
    @InjectView(R.id.phoneEditText)
    EditText phoneEditText;
    @InjectView(R.id.tv_inviteto_chat)
    TextView tvInviteToChat;
    @InjectView(R.id.iv_mask)
    ImageView ivMask;
    @InjectView(R.id.iv_success)
    ImageView ivTick;
    @InjectView(R.id.iv_success_border)
    RoundedImageView ivTickBorder;
//    @InjectView(R.id.fl_circle_loading)
//    FrameLayout flSuccess;

    private ResponseLoginSocial.Content userContent;
    private ProgressDialog ringProgressDialog;
    private AlertDialog mConfirmDialog;
    private final int DEFAULT_COUNTRY_CODE = 852;
    private int selectedPos = -1;
    private int countryCode;

    public static void start(final Context context) {

        ImageUtil.getInstance().prepareBlurImageFromActivity((Activity) context, InviteToChatActivity.class, new OnBlurCompleteListener() {
            @Override
            public void onBlurComplete() {
                Intent intent = new Intent(context, InviteToChatActivity.class);
                Navigation.presentIntent((Activity) context, intent);
            }
        });


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_to_chat);
        AppUtil.setBlurBehind(this);

        userContent = CoreData.getUserContent(this);

        ButterKnife.inject(this);
        initView();

    }

    private int getCountryCode(String countryIso) {
        int i = 0;
        for (SystemSetting.CountryCodeInfo countryCodeInfo : userContent.systemSetting.smsCountryCodeInfoList) {
            Log.w(TAG, "countryCodeInfo.countryCode = " + countryCodeInfo.countryShortName + " " + countryCodeInfo.countryCode + " " + countryIso);
            if (countryCodeInfo.countryShortName.equalsIgnoreCase(countryIso)) {
                return Integer.valueOf(countryCodeInfo.countryCode);
            }
            i++;
        }
        return DEFAULT_COUNTRY_CODE;
    }

    private ArrayList<Integer> filterDeputeCountryCodeList(ArrayList<Integer> countryCodeList) {
        Set<Integer> hs = new HashSet<>();
        hs.addAll(countryCodeList);
        countryCodeList.clear();
        countryCodeList.addAll(hs);
        Collections.sort(countryCodeList);
        return countryCodeList;
    }

    private void initView() {

        String htmlTNC = "<subtitle><yellow> " + getString(R.string.invite_to_chat__desc_1) + "\n</yellow></subtitle>" + getString(R.string.invite_to_chat__desc_2) + "\n\n<subtitle><yellow>" + getString(R.string.invite_to_chat__desc_3) +
                "\n</yellow></subtitle>" + getString(R.string.invite_to_chat__desc_4);
        tvInviteToChat.setText(Html.fromHtml(getString(R.string.setting__invite_to_chat)));
        TextView_tnc.setText(Html.fromHtml(htmlTNC.replaceAll("\n", "<br>")
                .replaceAll("<subtitle>", "<font size=\"12\"><b>")
                .replaceAll("</subtitle>", "</b></font>")
                .replaceAll("<yellow>", "<font color=#" + Integer.toHexString(ContextCompat.getColor(this, R.color.invite_yellow) & 0x00ffffff) + ">")
                .replaceAll("</yellow>", "</font>")
        ));

        String countryIso = AppUtil.getCurrentLangISO2Code(this);
        Log.w(TAG, "countryIso = " + countryIso);
        countryCode = getCountryCode(countryIso);
        countryCodeBtn.setText("+" + countryCode);

    }

    private void genShortUrlAndCallSmsViaTwilioApi(final String countryCode, final String phoneNo) {
        BranchUniversalObject branchUniversalObject = genBranchUniObj();
        LinkProperties linkProps = genLinkProps();
        branchUniversalObject.generateShortUrl(this, linkProps, new Branch.BranchLinkCreateListener() {
            @Override
            public void onLinkCreate(String url, BranchError error) {
                Log.w(TAG, "branch.io url = " + url);
                phoneEditText.setEnabled(false);
                if (error != null) {
                    hideLoadingDialog();
                    Dialog.normalDialog(InviteToChatActivity.this, "Branch.io gen shortUrl error: " + error);
                    phoneEditText.setEnabled(true);
                } else {
                    RequestSendSMSViaTwilio sendSMSViaTwilio = new RequestSendSMSViaTwilio(userContent.memId,
                            LocalStorageHelper.getAccessToken(getBaseContext()),
                            countryCode,
                            phoneNo,
                            url
                    );

                    sendSMSViaTwilio.callSendSMSViaTwilioApi(InviteToChatActivity.this, new ApiCallback() {
                        @Override
                        public void success(ApiResponse apiResponse) {
                            showSuccessDialog();
                            //clear input mobile number
                            phoneEditText.setText("");
                            phoneEditText.setEnabled(true);
                        }

                        @Override
                        public void failure(String errorMsg) {
                            hideLoadingDialog();
                            Dialog.normalDialog(InviteToChatActivity.this, getString(R.string.invite_to_chat__twillio_sms_error)).show();
                            phoneEditText.setEnabled(true);
                        }
                    });
                }
            }
        });
    }

    private BranchUniversalObject genBranchUniObj() {
        BranchUniversalObject branchUniObj = new BranchUniversalObject()
                .setTitle("Real")
                .setContentDescription("Someone invite you to Chat in Real")
                .setContentIndexingMode(BranchUniversalObject.CONTENT_INDEX_MODE.PUBLIC)
                .addContentMetadata(Constants.BRANCH_MEM_ID, "" + userContent.memId)
                .addContentMetadata(Constants.BRANCH_MEM_NAME, userContent.memName)
                .addContentMetadata(Constants.BRANCH_ACTION, Constants.BRANCH_ACTION_CHAT)
                .addContentMetadata("member_qbid", userContent.qbid);

        return branchUniObj;
    }

    private LinkProperties genLinkProps() {
        LinkProperties linkProperties = new LinkProperties()
                .setChannel(Constants.BRANCH_CHANNEL_CHAT)
                .setFeature("invite");
        return linkProperties;
    }

    private void genShortUrl() {
        BranchUniversalObject branchUniversalObject = genBranchUniObj();
        LinkProperties linkProps = genLinkProps();
        branchUniversalObject.generateShortUrl(this, linkProps, new Branch.BranchLinkCreateListener() {
            @Override
            public void onLinkCreate(String url, BranchError error) {
                if (error != null) {
                    Dialog.normalDialog(InviteToChatActivity.this, "Branch.io gen shortUrl error: " + error).show();
                } else {
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_SEND);
                    intent.setType("text/plain");
                    intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.invite_to_chat__subject));
                    intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.invite_to_chat__content) + " " + url);
                    startActivityForResult(Intent.createChooser(intent, getString(R.string.common__invite_to_chat)),REQUEST_CODE_VIRAL);
                }
            }
        });
    }

    @Override
    public void onBackPressed() {

        finish();
    }

    @OnClick(R.id.closeBtn)
    public void closeBtnClick(View view) {
        onBackPressed();
    }

    @OnClick(R.id.inviteBtn)
    public void inviteBtnClick(View view) {
        if (phoneEditText.getText().toString() == null ||
                phoneEditText.getText().toString().isEmpty())
            Dialog.normalDialog(this, getString(R.string.common__please_input_the_phone_number)).show();
        else {

            // Mixpanel
            MixpanelAPI mixpanel = MixpanelAPI.getInstance(this, AppConfig.getMixpanelToken());
            mixpanel.track("Invite to Chat_New Agent (SMS)");

            genShortUrlAndCallSmsViaTwilioApi(countryCodeBtn.getText().toString(), phoneEditText.getText().toString());

        }


    }

    @OnClick(R.id.countryCodeBtn)
    public void countryCodeBtnClick(View view) {
        ArrayList<Integer> countryCodeList = new ArrayList<>();
        for (SystemSetting.CountryCodeInfo countryCodeInfo : userContent.systemSetting.smsCountryCodeInfoList) {
            countryCodeList.add(Integer.parseInt(countryCodeInfo.countryCode));
        }

        final ArrayList<Integer> filterCountryCodeList = filterDeputeCountryCodeList(countryCodeList);

        final ArrayAdapter<Integer> arrayAdapter = new ArrayAdapter<>(
                this,
                android.R.layout.simple_list_item_checked,
                countryCodeList
        );

        //init value
        if (selectedPos == -1) {
            int i = 0;
            for (int index : filterCountryCodeList) {
                if (index == countryCode) {
                    selectedPos = i;
                }
                i++;
            }
        }

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this)
                .setSingleChoiceItems(arrayAdapter, selectedPos, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        countryCodeBtn.setText("+" + filterCountryCodeList.get(i));
                        selectedPos = i;
                        dialogInterface.dismiss();
                    }
                });

        mConfirmDialog = dialogBuilder.create();
        mConfirmDialog.show();

    }

    @OnClick(R.id.inviteAgnetToChatBtn)
    public void inviteAgnetToChatBtnClick(View view) {

        // Mixpanel
        MixpanelAPI mixpanel = MixpanelAPI.getInstance(this, AppConfig.getMixpanelToken());
        mixpanel.track("Invite to Chat_Old Agent");

        genShortUrl();
    }

    public void showLoadingDialog() {

        ivMask.setVisibility(View.VISIBLE);
        ivTick.setVisibility(View.VISIBLE);
        AlphaAnimation alphaAnimation=new AlphaAnimation((float)0.1,(float)1.0);
        alphaAnimation.setFillAfter(false);
        alphaAnimation.setDuration(1000);

        Animation a = AnimationUtils.loadAnimation(this, R.anim.zoom_in_out);
        a.reset();
        a.setDuration(1000);
        a.setFillAfter(false);
        ivTick.clearAnimation();
        ivTick.setAnimation(alphaAnimation);
        ivTick.startAnimation(a);

        //set tick color to green
        animateImageView(ivTick);
        animateImageView(ivTickBorder);

        ivTickBorder.setAnimation(alphaAnimation);
        ivTickBorder.clearAnimation();
        ivTickBorder.startAnimation(alphaAnimation);
        ivTickBorder.setImageBorder(Constants.STYLE_TYPE.SUCCESS);
        ivTickBorder.setVisibility(View.VISIBLE);


    }

    public void animateImageView(final ImageView v) {
        final int green = getResources().getColor(R.color.holo_green);

        final ValueAnimator colorAnim = ObjectAnimator.ofFloat(0f, 1f);
        colorAnim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float mul = (Float) animation.getAnimatedValue();
                int alphaGreen = adjustAlpha(green, mul);
                v.setColorFilter(alphaGreen, PorterDuff.Mode.SRC_ATOP);
                if (mul == 0.0) {
                    v.setColorFilter(null);
                }
            }
        });

        colorAnim.setDuration(1000);
        colorAnim.setRepeatMode(ValueAnimator.REVERSE);
        colorAnim.setRepeatCount(-1);
        colorAnim.start();

    }

    public int adjustAlpha(int color, float factor) {
        int alpha = Math.round(Color.alpha(color) * factor);
        int red = Color.red(color);
        int green = Color.green(color);
        int blue = Color.blue(color);
        return Color.argb(alpha, red, green, blue);
    }

    public void hideLoadingDialog() {
        if (ringProgressDialog != null) {
            ringProgressDialog.dismiss();
            ringProgressDialog.cancel();
            ringProgressDialog = null;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode==RESULT_OK && requestCode==REQUEST_CODE_VIRAL){
            showSuccessDialog();
        }
    }


    void showSuccessDialog() {

        int mStackLevel=0;
        mStackLevel++;

        // DialogFragment.show() will take care of adding the fragment
        // in a transaction.  We also want to remove any currently showing
        // dialog, so make our own transaction and take care of that here.
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        DialogFragment newFragment = ViralSuccessFragment.newInstance(mStackLevel);
        newFragment.show(ft, "dialog");
    }
}
