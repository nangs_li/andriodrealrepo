package co.real.productionreal2.fragment;


import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.yalantis.ucrop.UCrop;
import com.yalantis.ucrop.UCropActivity;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import co.real.productionreal2.ApplicationSingleton;
import co.real.productionreal2.Constants;
import co.real.productionreal2.CoreData;
import co.real.productionreal2.Navigation;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.MainActivityTabBase;
import co.real.productionreal2.activity.profile.CreateAgentProfileActivity;
import co.real.productionreal2.adapter.FollowingAdapter;
import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.callback.FragmentViewChangeListener;
import co.real.productionreal2.common.LocalStorageHelper;
import co.real.productionreal2.config.AppConfig;
import co.real.productionreal2.contianer.MeTabContainer;
import co.real.productionreal2.dao.account.DBLogInfo;
import co.real.productionreal2.daomanager.DatabaseManager;
import co.real.productionreal2.model.CurrentFollower;
import co.real.productionreal2.newsfeed.NewsFeedProfileFragment;
import co.real.productionreal2.service.AdminService;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.Follower;
import co.real.productionreal2.service.model.request.RequestFollowAgent;
import co.real.productionreal2.service.model.request.RequestFollowInfo;
import co.real.productionreal2.service.model.request.RequestFollowingList;
import co.real.productionreal2.service.model.response.ResponseFileUpload;
import co.real.productionreal2.service.model.response.ResponseFollowInfo;
import co.real.productionreal2.service.model.response.ResponseFollowingList;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;
import co.real.productionreal2.util.AppUtil;
import co.real.productionreal2.util.FileUtil;
import co.real.productionreal2.util.ImageUtil;
import co.real.productionreal2.util.NetworkUtil;
import co.real.productionreal2.view.BlurBuilder;
import co.real.productionreal2.view.TitleBar;
import lib.blurbehind.OnBlurCompleteListener;
import widget.RoundedImageView;

;

/**
 * Created by hohojo on 10/9/2015.
 */
public class UserProfileFragment extends Fragment implements FollowingAdapter.FollowingAdapterListener,   MainActivityTabBase.TabOnClickListener{
    @InjectView(R.id.rootImageView)
    ImageView rootImageView;
    @InjectView(R.id.userImageView)
    RoundedImageView userImageView;
    @InjectView(R.id.userNameTextView)
    TextView userNameTextView;
    @InjectView(R.id.followingCountTextView)
    TextView followingCountTextView;
    @InjectView(R.id.beAnAgentBtn)
    public Button beAnAgentBtn;
    @InjectView(R.id.maskView)
    View maskView;
    @InjectView(R.id.lv_following_list)
    ListView lvFollowingList;
    @InjectView(R.id.following_list_swipe_refresh_layout)
    SwipeRefreshLayout followingListSwipeLayout;
    @InjectView(R.id.tv_empty_message)
    TextView tvEmptyMessage;

    private static final String TAG = "UserProfileFragment";

    protected static final int REQUEST_STORAGE_READ_ACCESS_PERMISSION = 101;
    private static final int REQUEST_SELECT_PICTURE = 10011;
    private static final String SAMPLE_CROPPED_IMAGE_NAME = "cropped_pic";
    private Uri mDestinationUri;
    private ProgressDialog ringProgressDialog;
    private String uriStr;
    private FragmentViewChangeListener fragmentViewChangeListener;
    private ResponseLoginSocial.Content userContent;
    private String imageUrl;

    private TitleBar mTitleBar;

    Handler handler = new Handler();

    private int followingCount;
    private int PAGE_NO_FOLLOW = 1;

    private FollowingAdapter followingAdapter;
    private ArrayList<Follower> followingList;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            fragmentViewChangeListener = (FragmentViewChangeListener) activity;
        } catch (ClassCastException e) {
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        fragmentViewChangeListener = null;
    }

    public static UserProfileFragment newInstance(Uri uri) {
        Bundle args = new Bundle();
        UserProfileFragment fragment = new UserProfileFragment();
        if (uri!=null)
        args.putString(MeTabContainer.RESULT_DATA, uri.toString());
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userContent = new Gson().fromJson(DatabaseManager.getInstance(getActivity()).getLogInfo().getLogInfo(), ResponseLoginSocial.Content.class);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_profile, container, false);
        ButterKnife.inject(this, view);
        fragmentViewChangeListener.changeTabTitle(this);
        return view;
    }

    private void initView() {

        //init title bar
        mTitleBar = ((MainActivityTabBase) getActivity()).getTitleBar();
        mTitleBar.setupTitleBar(TitleBar.TAB_TYPE.ME_HOME_USER);

        Log.d("initView", "uriStr: "+uriStr);
        Log.d("initView", "userContent.photoUrl: " + userContent.photoUrl);

        if (uriStr!=null){
            imageUrl=uriStr;
            new UploadFileTask().execute("");
        }else{
            imageUrl=userContent.photoUrl;
        }

        followingAdapter = new FollowingAdapter(getActivity(), followingList, UserProfileFragment.this, FollowingAdapter.THEME.LIGHT);
        lvFollowingList.setAdapter(followingAdapter);

        //load following list
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {

            @Override
            public void run() {
                updateFollowInfo();
                fetchFollowingList(PAGE_NO_FOLLOW);
            }
        }, 300);

        Picasso.with(getActivity())
                .load(imageUrl)
                .error(R.drawable.default_profile)
                .placeholder(R.drawable.default_profile)
                .into(userImageView);

        Picasso.with(getActivity()).load(imageUrl).into(target);

        userNameTextView.setText(userContent.memName);
        followingCountTextView.setText(String.valueOf(userContent.followingCount) + "");

    }


    @Override
    public void onDestroyView() {
        Picasso.with(getActivity()).cancelRequest(target);
        super.onDestroyView();
    }

    Bitmap rootBitmap;
    private Target target = new Target() {
        @Override
        public void onBitmapLoaded(final  Bitmap bitmap, Picasso.LoadedFrom from) {

            new Thread() {
                @Override
                public void run() {
                    try {
                        rootBitmap = BlurBuilder.blurBitmap(getActivity(), bitmap);
//                        rootBitmap = BlurBuilder.reduceBrightness(rootBitmap);
                        handler.post(new Runnable() {
                            public void run() {
                                rootImageView.setImageBitmap(rootBitmap);
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }.start();
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {

        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {

        }

    };

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            uriStr = getArguments().getString(MeTabContainer.RESULT_DATA);
        }

        ((MainActivityTabBase) getActivity()).tabOnClickListener = this;
        initData();
        initView();
        addListener();
    }

    private void initData() {
        followingList = LocalStorageHelper.defaultHelper(getActivity()).getMeFollowingList(getActivity());
        followingCount = LocalStorageHelper.defaultHelper(getActivity()).getFollowingCount();
    }

    private void addListener() {
        //setup refresh list for following list
        followingListSwipeLayout.setColorSchemeResources(R.color.newsfeed_text_nuber_blue);
        followingListSwipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //online mode
                if (NetworkUtil.isConnected(getActivity())) {
                    PAGE_NO_FOLLOW = 1;
                    followingList = new ArrayList<Follower>();
                    fetchFollowingList(PAGE_NO_FOLLOW);
                } else {
                    stopRefresh();
                }
            }
        });

        beAnAgentBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Mixpanel
                MixpanelAPI mixpanel = MixpanelAPI.getInstance(getActivity(), AppConfig.getMixpanelToken());
                mixpanel.track("Attempted to Become Agent");


                ImageUtil.getInstance().prepareBlurImageFromActivity(getActivity(), CreateAgentProfileActivity.class, new OnBlurCompleteListener() {
                    @Override
                    public void onBlurComplete() {
                        Intent intent = new Intent(getActivity(), CreateAgentProfileActivity.class);
                        Navigation.presentIntentForResult(getActivity(),intent, Constants.ME_BE_AN_AGENT);
                    }
                });

            }
        });


        lvFollowingList.setOnScrollListener(new EndlessScrollListener() {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                // TODO Auto-generated method stub
                PAGE_NO_FOLLOW++;
                fetchFollowingList(PAGE_NO_FOLLOW);
            }
        });

        mTitleBar.getEditProfileBtn().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //same as the action clicking profile image
                userImageView.performClick();
            }
        });
    }

    @OnClick(R.id.userImageView)
    public void agentImageViewClick(View view) {
        //todo select image and call api
        pickFromGallery();

    }



    /**
     * get following list
     *
     * @param pageNo
     */
    private void fetchFollowingList(final int pageNo) {
        if (followingList == null)
            return;
        Log.w(TAG, "responseGson = " + "fetchFollowingList: " + pageNo);
//        if (followingList.size() < 1 || pageNo > 1) {
            RequestFollowingList requestFollowingList = new RequestFollowingList(userContent.memId, LocalStorageHelper.getAccessToken(getContext()), pageNo);
            requestFollowingList.callFollowingListApi(getActivity(), new ApiCallback() {
                @Override
                public void success(ApiResponse apiResponse) {
                    Log.w(TAG, "responseGson = " + apiResponse.jsonContent);

                    //to show empty me
                    int totalCount = 0;
                    try {
                        JSONObject jsonObject = new JSONObject(apiResponse.jsonContent);
                        totalCount = jsonObject.optJSONObject("Content").optInt("TotalCount");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    followingCountTextView.setText(String.valueOf(totalCount));
                    tvEmptyMessage.setVisibility(totalCount > 0 ? View.GONE : View.VISIBLE);
                    lvFollowingList.setVisibility(totalCount>0?View.VISIBLE:View.GONE);

                    if (pageNo == 1) {
                        followingList = new ArrayList<Follower>();
                    }
                    //add new record to existing list
                    followingList.addAll(new Gson().fromJson(
                            apiResponse.jsonContent,
                            ResponseFollowingList.class).content.followers);

                    if (pageNo == 1) {
                        if (followingList.size() < 1) {
//                            llEmptyActivityLog.setVisibility(View.VISIBLE);
//                            tvEmptyLogMsg.setVisibility(View.GONE);
                            stopRefresh();
                            return;
                        } else {
//                            llEmptyActivityLog.setVisibility(View.GONE);
                        }
                        followingAdapter = new FollowingAdapter(ApplicationSingleton.getInstance().getBaseContext(), followingList, UserProfileFragment.this, FollowingAdapter.THEME.LIGHT);
                        lvFollowingList.setAdapter(followingAdapter);

                    }

//                    updateFollowInfo();
                    LocalStorageHelper.defaultHelper(getActivity()).saveMeFollowingListToLocalStorage(followingList);
                    stopRefresh();

                }

                @Override
                public void failure(String errorMsg) {
                    Log.w(TAG, "errorMsg = " + errorMsg);
                    stopRefresh();
                }
            });
//        }
    }

    public void stopRefresh() {
        followingListSwipeLayout.setRefreshing(false);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    if (resultCode == getActivity().RESULT_OK && requestCode == REQUEST_SELECT_PICTURE) {
        try {
            final Uri selectedUri = data.getData();
            startCropActivity(selectedUri);
            Log.w("imageFilePath", "UCrop = ok request crop:  " + selectedUri);
        } catch (Exception e) {
          AppUtil.showToast(getActivity(),getString(R.string.chatroom_error_message__incompatible_error)); //TODO: general error for some not supporting image
        }
    } else if (resultCode == getActivity().RESULT_OK && requestCode == UCrop.REQUEST_CROP) {
        final Uri resultUri = UCrop.getOutput(data);
        uriStr = FileUtil.getRealPathFromURI(getActivity(), resultUri);
        Log.w("imageFilePath", "imageFilePath = " + uriStr);
        Picasso.with(getActivity())
                .load(new File(uriStr))
                .error(R.drawable.com_facebook_profile_picture_blank_square)
                .into(userImageView);
        Picasso.with(getActivity()).load(userContent.photoUrl).into(target);
        if (ringProgressDialog != null && ringProgressDialog.isShowing())
            return;
        //show progress dialog
        ringProgressDialog = ProgressDialog.show(getActivity(), getResources().getString(R.string.alert__Initializing),
                getResources().getString(R.string.common__uploading));
        new UploadFileTask().execute("");
        Log.w("imageFilePath", "UCrop = ok " + resultUri);
        Log.w("imageFilePath", "UploadFileTask " + uriStr);
    } else if (resultCode == UCrop.RESULT_ERROR) {
        Log.w("imageFilePath", "UCrop = err");
        final Throwable cropError = UCrop.getError(data);
    }
    }

    private void pickFromGallery() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M // Permission was added in API Level 16
                && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    REQUEST_STORAGE_READ_ACCESS_PERMISSION);
        } else {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            startActivityForResult(Intent.createChooser(intent, getResources().getString(R.string.common__select)), REQUEST_SELECT_PICTURE);
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_STORAGE_READ_ACCESS_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    pickFromGallery();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    /**
     * In most cases you need only to set crop aspect ration and max size for resulting image.
     *
     * @param uCrop - ucrop builder instance
     * @return - ucrop builder instance
     */
    private UCrop basisConfig(@NonNull UCrop uCrop) {
        uCrop = uCrop.useSourceImageAspectRatio();
        uCrop = uCrop.withAspectRatio(1, 1);
        return uCrop;
    }


    /**
     * Sometimes you want to adjust more options, it's done via {@link com.yalantis.ucrop.UCrop.Options} class.
     *
     * @param uCrop - ucrop builder instance
     * @return - ucrop builder instance
     */
    private UCrop advancedConfig(@NonNull UCrop uCrop) {
        UCrop.Options options = new UCrop.Options();
        options.setCompressionFormat(Bitmap.CompressFormat.JPEG);
//        options.setCompressionQuality(100);


        // If you want to configure how gestures work for all UCropActivity tabs
        options.setAllowedGestures(UCropActivity.SCALE, UCropActivity.ROTATE, UCropActivity.ALL);

        options.setOvalDimmedLayer(true);
        options.setShowCropFrame(false);
        options.setShowCropGrid(false);
        // Color palette
        options.setToolbarColor(ContextCompat.getColor(getActivity(), R.color.tab_bg));
        options.setStatusBarColor(ContextCompat.getColor(getActivity(), R.color.tab_bg));
        options.setActiveWidgetColor(ContextCompat.getColor(getActivity(), R.color.holo_blue));
        options.setToolbarWidgetColor(ContextCompat.getColor(getActivity(), R.color.white));

        return uCrop.withOptions(options);
    }

    private void startCropActivity(@NonNull Uri uri) {
        mDestinationUri = Uri.fromFile(new File(getActivity().getCacheDir(), SAMPLE_CROPPED_IMAGE_NAME + System.currentTimeMillis() + ".jpeg"));
        UCrop uCrop = UCrop.of(uri, mDestinationUri);

        uCrop = basisConfig(uCrop);
        uCrop = advancedConfig(uCrop);

        uCrop.start(getActivity());
    }


    private String callUploadFileApi(DefaultHttpClient httpClient, File photoFile) {

        HttpPost httppost = new HttpPost(AdminService.WEB_SERVICE_BASE_URL + "/Admin.asmx/FileUpload");
        MultipartEntity multipartEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
       Log.d("callUploadFileApi", "callUploadFileApi: " + userContent.toString());
        try {
            multipartEntity.addPart("MemberID", new StringBody("" + userContent.memId));
            multipartEntity.addPart("AccessToken", new StringBody(LocalStorageHelper.getAccessToken(getContext())));
            multipartEntity.addPart("ListingID", new StringBody("0"));
            multipartEntity.addPart("Position", new StringBody("0"));
            multipartEntity.addPart("FileExt", new StringBody(FileUtil.getFileExt(photoFile.getAbsolutePath())));
            multipartEntity.addPart("FileType", new StringBody("2"));
            multipartEntity.addPart("attachment", new FileBody(photoFile));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        httppost.setEntity(multipartEntity);
        try {
            HttpResponse httpResponse = httpClient.execute(httppost);
            InputStream is = httpResponse.getEntity().getContent();
            String responseString = FileUtil.convertStreamToString(is);
            Document doc = FileUtil.getDomElement(responseString);
            doc.getDocumentElement().normalize();
            NodeList nodeList = doc.getElementsByTagName("string");
            Node node = nodeList.item(0);
            String json = FileUtil.nodeToString(node.getFirstChild());
            if (json != null) {
                ResponseFileUpload responseFileUpload = new Gson().fromJson(json, ResponseFileUpload.class);
                if (responseFileUpload.content == null)
                    return null;
                if (responseFileUpload.content.photoUrl == null)
                    return null;
                return responseFileUpload.content.photoUrl;
            }

            return null;

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public ArrayList<Follower> getFollowingList() {
        return followingList;
    }

    @Override
    public void onFollowItem(final Follower mFollower) {
        Log.d("toFollow", "toFollow agentContent: " + userContent.toString());
        Log.d("toFollow", "toFollow mAgentProfile: " + mFollower.memberName + mFollower.isFollowing);

        DatabaseManager databaseManager = DatabaseManager.getInstance(getActivity());
        ResponseLoginSocial.Content content = new Gson().fromJson(databaseManager.getLogInfo().getLogInfo(), ResponseLoginSocial.Content.class);
        final RequestFollowAgent requestFollowAgent = new RequestFollowAgent(userContent.memId,  LocalStorageHelper.getAccessToken(getContext()));
        requestFollowAgent.followingListingId = mFollower.listingID;
        requestFollowAgent.followingMemId = mFollower.memberID;

        if (mFollower.isFollowing == 0) {
            // follow count +1
            followAgent(requestFollowAgent, mFollower);

        } else if (mFollower.isFollowing == 1) {

            FragmentManager fm = getChildFragmentManager();
            NewsFeedProfileFragment.MyAlertDialogFragment editNameDialog = NewsFeedProfileFragment.MyAlertDialogFragment
                    .newInstance(mFollower.memberName,

                            new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    // follow count -1
                                    unfollowAgent(requestFollowAgent, mFollower);
                                    dialog.dismiss();
                                }
                            });
            editNameDialog.show(fm, "fragment_edit_name");

        } else {
            // u are ureself
        }
    }


    private void followAgent(RequestFollowAgent followAgent, final Follower mFollower) {

        followAgent.callFollowApi(getActivity(), new ApiCallback() {
            @Override
            public void success(ApiResponse apiResponse) {
                Log.d("toFollow", "toFollow follow: " + mFollower.memberName + " " + mFollower.isFollowing);
                mFollower.isFollowing = (mFollower.isFollowing == 0 ? 1 : 0);
                followingAdapter.notifyDataSetChanged();
                updateFollowInfo();

                CurrentFollower currentFollower = new CurrentFollower(userContent.memId, mFollower.memberID,
                        userContent.qbid, null, true);
                currentFollower.updateCurrentFollowingList(getActivity());

                //add local following people list
                LocalStorageHelper.addAgentProfileListToLocalStorage(getActivity(),mFollower.memberID);

            }

            @Override
            public void failure(String errorMsg) {
                Log.d("toFollow", "toFollow follow: " + errorMsg.toString());
            }
        });
    }

    private void updateFollowInfo() {
        //offline mode
        if (!NetworkUtil.isConnected(getActivity())) {
            followingCountTextView.setText(String.valueOf(followingCount));
            return;
        }

        RequestFollowInfo requestFollowInfo = new RequestFollowInfo(userContent.memId, LocalStorageHelper.getAccessToken(getContext()));
        requestFollowInfo.callFollowInfoApi(getActivity(), new ApiCallback() {
            @Override
            public void success(ApiResponse apiResponse) {
                Log.d(TAG, "responseGson = " + apiResponse.jsonContent);
                ResponseFollowInfo.Content followInfo = new Gson().fromJson(apiResponse.jsonContent, ResponseFollowInfo.class).content;
                followingCountTextView.setText(String.valueOf(followInfo.followingCount));
                LocalStorageHelper.saveFollowerCountToLocalStorage(followInfo.followCount);
            }

            @Override
            public void failure(String errorMsg) {
                Log.w(TAG, "errorMsg = " + errorMsg);
            }
        });
    }

    private void unfollowAgent(RequestFollowAgent unfollowAgent, final Follower mFollower) {

        unfollowAgent.callUnfollowApi(getActivity(), new ApiCallback() {

            @Override
            public void success(ApiResponse apiResponse) {
                Log.d("toFollow", "toFollow unfollow: " + mFollower.memberName + " " + mFollower.isFollowing);
                mFollower.isFollowing = (mFollower.isFollowing == 0 ? 1 : 0);
                followingAdapter.notifyDataSetChanged();
                updateFollowInfo();

                CurrentFollower currentFollower = new CurrentFollower(userContent.memId, mFollower.memberID,
                        userContent.qbid, null, false);
                currentFollower.updateCurrentFollowingList(getActivity());

                //add local following people list
                LocalStorageHelper.removeAgentProfileListToLocalStorage(getActivity(),mFollower.memberID);
            }

            @Override
            public void failure(String errorMsg) {

            }
        });
    }

    @Override
    public void singleTap() {
        //refresh followInfo
        PAGE_NO_FOLLOW=1;
       fetchFollowingList(PAGE_NO_FOLLOW);
    }

    @Override
    public void doubleTap() {

    }


    /**
     * upload task
     */
    private class UploadFileTask extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... imageUri) {

            HttpParams params = new BasicHttpParams();
            params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
            DefaultHttpClient httpClient = new DefaultHttpClient(params);
            return resizePhotoAndPublish(httpClient);
        }

        @Override
        protected void onProgressUpdate(String... values) {
            Log.w("EditAgentProfile", "onProgressUpdate　photoUrl = " + values[0]);
        }

        @Override
        protected void onPostExecute(String photoURL) {
        }

        private String resizePhotoAndPublish(DefaultHttpClient httpClient) {
            File imageInDevice = new File(FileUtil.getAppSubFolder(Constants.UPLOAD_FILE_FOLDER_NAME),
                    "UserImage.jpg");
//            Bitmap finalizedBm=ImageUtil.decodeSampledBitmapFromResource(imageFilePath);
            try {
//                FileUtil.convertBitmapToFile(imageInDevice.toString(),finalizedBm);
                ImageUtil.getCompressImageFilePath(FileUtil.getRealPathFromURI(getActivity(), Uri.parse(imageUrl)), imageInDevice.getAbsolutePath(), ImageUtil.iconImageType);
            } catch (Exception e) {
                e.printStackTrace();
            }
            String photoUrl = callUploadFileApi(httpClient, imageInDevice);
            publishProgress(photoUrl);

            //update profile img
//            DatabaseManager.getInstance(EditAgentProfileActivity.this).getLogInfo().set
            userContent.photoUrl = photoUrl;
            CoreData.setUserContent(userContent);
            DBLogInfo dbLogInfo = DatabaseManager.getInstance(getActivity()).getLogInfo();
            dbLogInfo.setLogInfo(new Gson().toJson(userContent));
            DatabaseManager.getInstance(getActivity()).updateLogInfo(dbLogInfo);
            if (ringProgressDialog != null) {
                ringProgressDialog.dismiss();
                ringProgressDialog.cancel();
                ringProgressDialog = null;
            }
            return photoUrl;
        }
    }

    public abstract class EndlessScrollListener implements AbsListView.OnScrollListener {
        // The minimum amount of items to have below your current scroll position
        // before loading more.
        private int visibleThreshold = 5;
        // The current offset index of data you have loaded
        private int currentPage = 0;
        // The total number of items in the dataset after the last load
        private int previousTotalItemCount = 0;
        // True if we are still waiting for the last set of data to load.
        private boolean loading = true;
        // Sets the starting page index
        private int startingPageIndex = 0;

        public EndlessScrollListener() {
        }

        public EndlessScrollListener(int visibleThreshold) {
            this.visibleThreshold = visibleThreshold;
        }

        public EndlessScrollListener(int visibleThreshold, int startPage) {
            this.visibleThreshold = visibleThreshold;
            this.startingPageIndex = startPage;
            this.currentPage = startPage;
        }

        // This happens many times a second during a scroll, so be wary of the code you place here.
        // We are given a few useful parameters to help us work out if we need to load some more data,
        // but first we check if we are waiting for the previous load to finish.
        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            // If the total item count is zero and the previous isn't, assume the
            // list is invalidated and should be reset back to initial state
            if (totalItemCount < previousTotalItemCount) {
                this.currentPage = this.startingPageIndex;
                this.previousTotalItemCount = totalItemCount;
                if (totalItemCount == 0) {
                    this.loading = true;
                }
            }

            // If it’s still loading, we check to see if the dataset count has
            // changed, if so we conclude it has finished loading and update the current page
            // number and total item count.
            if (loading && (totalItemCount > previousTotalItemCount)) {
                loading = false;
                previousTotalItemCount = totalItemCount;
                currentPage++;
            }

            // If it isn’t currently loading, we check to see if we have breached
            // the visibleThreshold and need to reload more data.
            // If we do need to reload some more data, we execute onLoadMore to fetch the data.
            if (!loading && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {
                onLoadMore(currentPage + 1, totalItemCount);
                loading = true;
            }
        }

        // Defines the process for actually loading more data based on page
        public abstract void onLoadMore(int page, int totalItemsCount);

        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
            // Don't take any action on changed
        }

    }

}
