package co.real.productionreal2;

import android.content.Context;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.LinearLayout;

public class LL extends LinearLayout {

    public LL(Context context) {
        super(context);
    }

    public LL(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public float getXFraction() {
        final int width = getWidth();
        if (width != 0) return getX() / getWidth();
        else return getX();
    }

    public void setXFraction(float xFraction) {
        final int width = getWidth();
        float newWidth = (width > 0) ? (xFraction * width) : -9999;
        setX(newWidth);
    }

	@Override
	public boolean onTouchEvent(MotionEvent event) {
//		   final int action = MotionEventCompat.getActionMasked(e);
//
//	        if (LOCAL_LOGV) {
//	            Log.v(TAG, "onTouchEvent() action = " + action);
//	        }
//
//	        if (!isSwiping()) {
//	            // Log.w(TAG, "onTouchEvent() - unexpected state");
//	            return;
//	        }
//
//	        switch (action) {
//	            case MotionEvent.ACTION_UP:
//	            case MotionEvent.ACTION_CANCEL:
//	                handleActionUpOrCancelWhileSwiping(e);
//	                break;
//
//	            case MotionEvent.ACTION_MOVE:
//	                handleActionMoveWhileSwiping(e);
//	                break;
//	        }
//		
//		 // Interpret MotionEvent data
//        // Handle touch here

//		return true;
		
		return super.onTouchEvent(event);
	}
    

    
}