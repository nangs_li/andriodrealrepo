package co.real.productionreal2.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

import co.real.productionreal2.R;
import co.real.productionreal2.Constants;

/**
 * Created by kelvinsun on 19/2/16.
 */
public class FollowButton extends Button {

    private int isFollowing = 0;
    private Context context;

    public FollowButton(Context context) {
        super(context);
        this.context = context;
    }

    public FollowButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;

    }

    public FollowButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
    }

    public int isFollowing() {
        return isFollowing;
    }

    public void setIsFollowing(int isFollowing) {
        this.isFollowing = isFollowing;
    }

    public void setStyle(Constants.STYLE_TYPE styleType) {
        switch (styleType) {
            case USER:
                this.setVisibility(GONE);
                setEnabled(true);
                break;
            case AGENT_FOLLOWED:
                this.setVisibility(VISIBLE);
                setEnabled(true);
                setBackgroundResource(R.drawable.bg_btn_follow_lv);
                setSelected(true);
                setText(R.string.common__following);
                setTextColor(getResources().getColor(R.color.white));
                setCompoundDrawablesWithIntrinsicBounds(R.drawable.btn_ico_follow_unfollow_small, 0, 0, 0);
                break;
            case AGENT_UNFOLLOWED:
                this.setVisibility(VISIBLE);
                setEnabled(true);
                setBackgroundResource(R.drawable.bg_btn_follow_lv);
                setSelected(false);
                setText(getResources().getString(R.string.common__follow));
                setTextColor(getResources().getColor(R.color.holo_blue));
                setCompoundDrawablesWithIntrinsicBounds(R.drawable.btn_ico_follow_follow, 0, 0, 0);
                break;
            case CUSTOMER_SERVICE:
                this.setVisibility(GONE);
                setEnabled(true);
                break;
            case ROBOT:
                this.setVisibility(VISIBLE);
                setEnabled(false);
                setBackgroundColor(getResources().getColor(R.color.holo_green));
                setText(getResources().getString(R.string.chat_lobby__real_team));
                setTextColor(getResources().getColor(R.color.white));
                setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                break;
            case DEFAULT:
                this.setVisibility(GONE);
                setEnabled(true);
                break;
        }
//        if (isFollowing == 0) {
//            setSelected(false);
//            setText("+ " + getResources().getString(R.string.common__follow));
//            setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
//        } else {
//            setSelected(true);
//            setText(R.string.common__following);
//            setCompoundDrawablesWithIntrinsicBounds(R.drawable.btn_ico_follow_unfollow_small, 0, 0, 0);
//        }
    }

    public void playAnimation() {
        if (isFollowing == 0) {
            setSelected(false);
            setText(getResources().getString(R.string.common__follow));
            setCompoundDrawablesWithIntrinsicBounds(R.drawable.btn_ico_follow_follow, 0, 0, 0);
        } else {
            setSelected(true);
            setText(R.string.common__following);
            setCompoundDrawablesWithIntrinsicBounds(R.drawable.btn_ico_follow_unfollow_small, 0, 0, 0);
        }
        Animation anim = AnimationUtils.loadAnimation(context, R.anim.follow_btn_pop);
        anim.reset();
        startAnimation(anim);
    }
}
