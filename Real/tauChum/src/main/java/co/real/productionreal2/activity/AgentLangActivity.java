package co.real.productionreal2.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ListView;

import java.sql.Struct;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import co.real.productionreal2.Constants;
import co.real.productionreal2.R;
import co.real.productionreal2.adapter.AgentLangAdapter;
import co.real.productionreal2.model.SystemSetting;
import co.real.productionreal2.service.model.AgentLanguage;

/**
 * Created by hohojo on 11/9/2015.
 */
public class AgentLangActivity extends BaseActivity {
    @InjectView(R.id.rootView)
    View rootView;
    @InjectView(R.id.langListView)
    ListView langListView;

    AgentLangAdapter agentLangAdapter;
    private List<AgentLanguage> beforeAgentLangList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agent_language);

        ButterKnife.inject(this);
        initView();
    }

    private void initView() {
        Intent intent = getIntent();
        final List<SystemSetting.SpokenLanguage> spokenLangList = intent.getParcelableArrayListExtra(Constants.EXTRA_SPOKEN_LANG);
        beforeAgentLangList = intent.getParcelableArrayListExtra(Constants.EXTRA_AGENT_LANG);
        agentLangAdapter = new AgentLangAdapter(this, spokenLangList, beforeAgentLangList);


//        agentLangAdapter = new AgentLangAdapter(this, spokenLangList, beforeAgentLangList);
        langListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        langListView.setAdapter(agentLangAdapter);

        for (int j = 0; j < spokenLangList.size(); j++) {
            if (beforeAgentLangList != null) {
                for (int i = 0; i < beforeAgentLangList.size(); i++) {
                    if (spokenLangList.get(j).index == beforeAgentLangList.get(i).langIndex) {
                        langListView.setItemChecked(spokenLangList.get(j).position, true);
                    }
                }
            }
        }

        langListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                AbsListView list = (AbsListView)parent;
                Adapter adapter = list.getAdapter();
                SparseBooleanArray array = list.getCheckedItemPositions();
                List<SystemSetting.SpokenLanguage> checked = new ArrayList<>(list.getCheckedItemCount());
                for (int i = 0; i < array.size(); i++) {
                    int key = array.keyAt(i);
                    if (array.get(key)) {
                        checked.add((SystemSetting.SpokenLanguage)adapter.getItem(key));
                    }
                }

            Log.d("onitemclick","onitemclick: "+position+ " "+((AbsListView) parent).isItemChecked(position));
                if (((AbsListView) parent).isItemChecked(position)) {
                    agentLangAdapter.getSpokenLangIsCheckedList().set(position, true);
                } else {
                    agentLangAdapter.getSpokenLangIsCheckedList().set(position, false);
                }
            }
        });

//        if (beforeAgentLangList != null) {
//            for (int i = 0; i < beforeAgentLangList.size(); i++) {
//                langListView.setItemChecked(beforeAgentLangList.get(i).langIndex, true);
//            }
//        }
    }

    private ArrayList<AgentLanguage> getAgentSpecAddList() {
        ArrayList<AgentLanguage> agenLangAddList = new ArrayList<>();
        ArrayList<AgentLanguage> currentAgentSpecList = agentLangAdapter.getUpdateAgentSpokenLangList();

        for (int i = 0; i < currentAgentSpecList.size(); i++) {
            if (!beforeAgentLangList.contains(currentAgentSpecList.get(i)))
                agenLangAddList.add(currentAgentSpecList.get(i));
        }
        return agenLangAddList;
    }
    private ArrayList<AgentLanguage> getAgentSpecDelList() {
        ArrayList<AgentLanguage> agenLangDelList = new ArrayList<>();
        ArrayList<AgentLanguage> currentAgentSpecList = agentLangAdapter.getUpdateAgentSpokenLangList();

        for (int i = 0; i < beforeAgentLangList.size(); i++) {
            if (!currentAgentSpecList.contains(beforeAgentLangList.get(i)))
                agenLangDelList.add(beforeAgentLangList.get(i));
        }
        return agenLangDelList;
    }

    @OnClick(R.id.backBtn)
    public void closeBtn(View view) {
        setResult(RESULT_CANCELED);
        finish();
    }

    @OnClick(R.id.saveBtn)
    public void saveBtn(View view) {
        Intent intent = new Intent();
        intent.putExtra(Constants.EXTRA_UPDATED_AGENT_LANG, agentLangAdapter.getUpdateAgentSpokenLangList());
        intent.putExtra(Constants.EXTRA_UPDATED_AGENT_LANG_NATIVE_NAME, agentLangAdapter.getUpdateAgentSpokenLangNativeNameList());
        intent.putExtra(Constants.EXTRA_UPDATED_AGENT_LANG_DEL_LIST, getAgentSpecDelList());
        intent.putExtra(Constants.EXTRA_UPDATED_AGENT_LANG_ADD_LIST, getAgentSpecAddList());
        setResult(RESULT_OK, intent);
        finish();
    }
}
