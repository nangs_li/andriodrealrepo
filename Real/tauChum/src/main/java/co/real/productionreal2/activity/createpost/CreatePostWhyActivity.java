package co.real.productionreal2.activity.createpost;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.mixpanel.android.mpmetrics.MixpanelAPI;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import co.real.productionreal2.Constants;
import co.real.productionreal2.CoreData;
import co.real.productionreal2.Navigation;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.BaseActivity;
import co.real.productionreal2.callback.DialogDualCallback;
import co.real.productionreal2.config.AppConfig;
import co.real.productionreal2.model.SystemSetting;
import co.real.productionreal2.service.model.request.RequestListing;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;
import co.real.productionreal2.util.AppUtil;
import co.real.productionreal2.view.Dialog;

;


/**
 * Created by hohojo on 12/10/2015.
 */
public class CreatePostWhyActivity extends BaseActivity {
    @InjectView(R.id.rootImageView)
    ImageView rootImageView;
    @InjectView(R.id.whyListView)
    ListView whyListView;
    WhyAdapter whyAdapter;
    @InjectView(R.id.tv_language)
    TextView tvLanguage;
    private byte[] backgroundBitmapBytes;
    private int slogenSelectedPosition = 0;

    private BaseCreatePost baseCreatePost;
    private int langIndex = 0;
    private boolean isAddLang = false;

    private ResponseLoginSocial.Content agentContent;
    private ArrayList<SystemSetting.Slogan> sloganList = new ArrayList<>();
    private String[] sloganStrList;
    private BaseCreatePost.ReasonsDespImage currentReason;

    private List<String> langList = new ArrayList<>();
    private AlertDialog mConfirmDialog;
    private int selectedLang = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_post_why);

        langIndex = getIntent().getIntExtra(Constants.EXTRA_LANG_INDEX, 0);
        isAddLang = getIntent().getBooleanExtra(Constants.EXTRA_IS_ADD_LANG, false);

        Log.d("CreatePostWhy", "CreatePostWhy: " + langIndex);
        ButterKnife.inject(this);

//        try {
//            if (BaseCreatePost.getInstance(this).getCreatePostList().size() > 0)
//                slogenSelectedPosition = BaseCreatePost.getInstance(this).getCreatePostList().get(0).createPostRequest.sloganIndex;
//        } catch (NullPointerException e) {
//        }
    }

    private void initData() {

        baseCreatePost = BaseCreatePost.getInstance(this);

//        if (baseCreatePost.getReasonsDespImageList().size() > 0 && selectedLang < baseCreatePost.getReasonsDespImageList().size()) {
//            currentReason = baseCreatePost.getCurrentReasonsDespImage(selectedLang);
//        } else {
//            currentReason = baseCreatePost.getCurrentReasonsDespImage();
//        }

        if (baseCreatePost.getCurrentReasonsImage(langIndex) == null) {
            baseCreatePost.initCurrentReasonsImage(langIndex, slogenSelectedPosition);
        }
        currentReason = baseCreatePost.getCurrentReasonsImage(langIndex);

        Log.d("onResume", "currentReason: " + currentReason);

        agentContent = CoreData.getUserContent(this);
        slogenSelectedPosition = currentReason.sloganIndex;
        langList = getLangStringList(getLangIndexList(), agentContent);

    }


    private int getCurrentReasonPosition() {
        return (selectedLang == -1 ? 0 : selectedLang);
    }


    @Override
    protected void onResume() {
        super.onResume();
        initData();
        initView();
        Log.d("onResume", "onResume: " + slogenSelectedPosition);
    }

    private void initView() {
        //show or hide the language switch button
        if (isAddLang) {
            tvLanguage.setVisibility(View.GONE);
        } else {
            if (langList.size() > 0)
                tvLanguage.setText(langList.get(selectedLang));
            tvLanguage.setVisibility(langList.size() < 2 ? View.GONE : View.VISIBLE);
        }

        //init slogan list
        sloganList = new ArrayList<>();
        sloganList.addAll(AppUtil.getSloganListByLocale(agentContent.systemSetting.sloganList, langIndex));
        Log.d("SloganList", "SloganList: " + sloganList.toString());
        whyAdapter = new WhyAdapter(this, sloganList);
        whyListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
//        whyListView.setAdapter(new ArrayAdapter<SystemSetting.Slogan>(this, R.layout.list_item_single_choice, sloganList));
        whyListView.setAdapter(whyAdapter);

        whyListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                slogenSelectedPosition = position;
                whyAdapter.notifyDataSetChanged();

                Log.d("whyListView", "langIndex: " + langIndex);
                if (baseCreatePost.getCurrentReasonsDespImage() == null) {
                    baseCreatePost.initCurrentReasonsImage(langIndex, slogenSelectedPosition);
                } else {
                    baseCreatePost.getCurrentReasonsImage(langIndex).sloganIndex = slogenSelectedPosition;
                }

                Log.d("whyListView", "whyListView: " + selectedLang + " " + slogenSelectedPosition);
            }
        });

        whyListView.setItemChecked(slogenSelectedPosition, true);

    }

    @OnClick(R.id.tv_language)
    public void languageBtnClick(View view) {
        openLanguageSelectionDialog();
    }

    @OnClick(R.id.closeBtn)
    public void closeBtnClick(View view) {
        Dialog.exitSaveChangeDialog(this, new DialogDualCallback() {
            @Override
            public void positive() {
                //reset value
                BaseCreatePost.getInstance(CreatePostWhyActivity.this).resetBaseCreatePost();
                exitActivity();
            }

            @Override
            public void negative() {
                //exit
                exitActivity();
            }
        }).show();
    }

    private void exitActivity() {
        setResult(RESULT_OK);
        finish();
    }

    @OnClick(R.id.backBtn)
    public void backBtnClick(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        finish();
    }

    @OnClick(R.id.nextBtn)
    public void nextBtnClick(View view) {
        // Mixpanel
        MixpanelAPI mixpanel = MixpanelAPI.getInstance(this, AppConfig.getMixpanelToken());
        mixpanel.track("Saved Posting Details_Slogan");

        Intent intent = new Intent(this, CreatePost3ReasonsActivity.class);
        intent.putExtra(Constants.EXTRA_SLOGAN, AppUtil.getSlogan(sloganList, langIndex, sloganList.get(slogenSelectedPosition).index));
        intent.putExtra(Constants.EXTRA_SLOGAN_INDEX, slogenSelectedPosition);
        intent.putExtra(Constants.EXTRA_LANG_INDEX, langIndex);
        if (isAddLang)
            intent.putExtra(Constants.EXTRA_IS_ADD_LANG, true);
        Navigation.pushIntentForResult(this,intent,CreatePostPropertyActivity.FINISH_POST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == CreatePostPropertyActivity.FINISH_POST) {
            setResult(RESULT_OK, data);
            finish();
        }
    }

    private class WhyAdapter extends BaseAdapter {
        private ArrayList<SystemSetting.Slogan> sloganList;
        private ViewHolder holder;
        private Context context;

        public WhyAdapter(Context context, ArrayList<SystemSetting.Slogan> sloganList) {
            this.sloganList = sloganList;
            this.context = context;
        }


        @Override
        public int getCount() {
            return sloganList.size();
        }

        @Override
        public Object getItem(int position) {
            return sloganList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.list_item_single_choice, null);
                holder = createViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            if (position == slogenSelectedPosition)
                holder.checkedTextView.setChecked(true);
            else
                holder.checkedTextView.setChecked(false);

            holder.checkedTextView.setText(sloganList.get(position).slogan);
            return convertView;
        }

        private ViewHolder createViewHolder(View v) {
            ViewHolder holder = new ViewHolder();
            holder.checkedTextView = (CheckedTextView) v.findViewById(R.id.checkedTextView);
            return holder;
        }

    }

    private static class ViewHolder {
        public CheckedTextView checkedTextView;
    }


    private void openLanguageSelectionDialog() {

        final ArrayAdapter<String> metricList = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_checked, langList);
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this)
                .setSingleChoiceItems(metricList, selectedLang, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        tvLanguage.setText(langList.get(i));
//                        SearchFilterSetting.getInstance().setSizeUnitType(selectedMetric);
//                        selectedLang = AppUtil.getMeticPositionFromIndex(langList, AppUtil.getMetricIndexFromName(CreatePost3ReasonsActivity.this, langList.get(i)));
                        dialogInterface.dismiss();
                        selectedLang = i;
                        langIndex = getLangIndexList().get(i);
                        Log.d("openLanguageSelection", "openLanguageSelectionDialog: " + langIndex);
                        initData();
                        initView();
                    }
                });

//        mConfirmDialog.setTitle(R.string.spoken_lang);
        mConfirmDialog = dialogBuilder.create();
        mConfirmDialog.show();
    }

    private ArrayList<Integer> getLangIndexList() {
        ArrayList<Integer> langIndexList = new ArrayList<>();
        for (RequestListing.Reason reason : baseCreatePost.getCreatePostRequest().reasons) {
            langIndexList.add(reason.languageIndex);
        }
        return langIndexList;
    }

    private ArrayList<String> getLangStringList(ArrayList<Integer> langIndexList, ResponseLoginSocial.Content userContent) {
        ArrayList<String> langStringList = new ArrayList<>();

        for (Integer langIndex : langIndexList) {
            for (SystemSetting.SystemLanguage systemLanguage : userContent.systemSetting.sysLangList) {
                if (systemLanguage.index == langIndex) {
                    langStringList.add(systemLanguage.nativeName);
                }
            }
        }
        return langStringList;
    }

}


