package co.real.productionreal2.service.model.response;

import com.google.gson.annotations.SerializedName;
import co.real.productionreal2.service.model.AgentProfiles;

public class ResponseAgentProfile extends ResponseBase{

	
	@SerializedName("Content")
    public Content content; 

    public class Content {
    	
    	@SerializedName("AgentProfile")
        public AgentProfiles profile;
    	
    	
    }
	
    
    
	
}
