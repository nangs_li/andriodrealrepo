package co.real.productionreal2.service.model.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by alexhung on 12/5/16.
 */
public class ResponseVersion extends ResponseBase {
    @SerializedName("Ver")
    public Ver ver;

    @SerializedName("URL")
    public String url;

    public class Ver {
        @SerializedName("androidVer")
        public String androidVer;
        @SerializedName("ForceUpdate")
        public int forceUpdate;
    }
}
