/*
 * Copyright 2014 Soichiro Kashima
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package co.real.productionreal2.newsfeed;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import co.real.productionreal2.CoreData;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.viral.ConnectAgentActivity;
import co.real.productionreal2.activity.MainActivityTabBase;
import co.real.productionreal2.adapter.SearchResultAdapter;
import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.common.LocalStorageHelper;
import co.real.productionreal2.model.AgentListing;
import co.real.productionreal2.model.GooglePlace;
import co.real.productionreal2.model.SystemSetting;
import co.real.productionreal2.service.model.AgentProfiles;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.request.RequestAddressSearch;
import co.real.productionreal2.service.model.request.RequestListing;
import co.real.productionreal2.service.model.request.RequestNewsFeed;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;
import co.real.productionreal2.service.model.response.ResponseNewsFeed;
import co.real.productionreal2.setting.HistorySearchSetting;
import co.real.productionreal2.util.AppUtil;
import co.real.productionreal2.util.LocationUtil;
import co.real.productionreal2.view.TabMenu;
import co.real.productionreal2.view.TitleBar;
import widget.ComputeVerticalScrollOffAbleRecyclerView;
import widget.EnbleableSwipeViewPager;
import widget.InterceptableListener;
import widget.InterceptableRelativeLayout;

;


public class NewsFeedSearchFragment extends BaseNewsFeedFragment implements MainActivityTabBase.MiniViewUpdateListener, NewsFeed_AgentProfilesAdapter.NewsFeed_AgentProfilesAdapterListener {

    private static final String TAG = "NewsFeedSearchFragment";

    @InjectView(R.id.noti_new_story)
    LinearLayout notiNewStory;
    @InjectView(R.id.btn_new_story)
    TextView tvNewStory;

    NewsFeedDetailFragment newsFeedDetailFragment;
    NewsFeedProfileFragment newsFeedProfileFragment;

    SwipeRefreshLayout mSwipeRefreshLayout;
    ComputeVerticalScrollOffAbleRecyclerView mRecyclerView;
    FrameLayout flEmptyView;
    FrameLayout flEmptyNewsFeedView;
    TextView tvEmptyResultDesc;
    LinearLayoutManager mLayoutManager;
    private NewsFeed_AgentProfilesAdapter mAdapter;
    private HashMap<String, String> propertyTypeHashMap = new HashMap<>();

    int newsfeed_item_hight = 0;
    int newsfeed_item_header_height = 0;
    int newsfeed_item_thumbnail_height = 0;
    int newsfeed_item_bottom_height = 0;
    int newsfeed_item_shadow_height = 0;
    int newsfeed_item_end_height = 0;
    int newsfeed_icon_height;
    int newsfeed_icon_bottom_height = 0;

    int min_newsfeed_icon_height = 0;
    int my_newsfeed_icon_height = 0;
    int bottom_hight = 0;

    AlphaAnimation animationBeHide;
    AlphaAnimation animationBeVisiable;

    private TitleBar mTitleBar;

    private boolean isMiniview = false;
    int topItemPostion = 0;
    ResponseNewsFeed responseGson;

    private int PAGE_NO = 1;
    private boolean hasPageEnd = false;
    private List<Integer> agentResultList = new ArrayList<>();

    private static List<AgentProfiles> agentProfilesList = new ArrayList<>();
    private List<SystemSetting.Slogan> sloganList = new ArrayList<>();
    private List<SystemSetting.PropertyType> propertyTypeList = new ArrayList<>();

    private Location mLastLocation;

    // Google client to interact with Google API
    private List<GooglePlace> googeplaceList;
    protected RequestAddressSearch addressSearch;
    private boolean isDefaultSearch = false;

    private SearchResultAdapter adapter;
    public ViewPager pager;
    private InterceptableRelativeLayout mInterceptableRelativeLayout;

    private View newsfeed_moving_cover_view1;
    private View newsfeed_moving_cover_view2;
    private View newsfeed_moving_cover_view3;

    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    public boolean isShowing = false;
    public int viewPagerScrollState = ViewPager.SCROLL_STATE_IDLE;

    private boolean IS_SUPPORT_GOOGLE_SERVICE;

    private String regionName;

    private RequestNewsFeed requestNewsFeed;

    public static final String BUNDLE_NEWSFEED_TYPE = "BUNDLE_NEWSFEED_TYPE";
    public static final String EXTRA_MESSAGE = "EXTRA_MESSAGE";

    public static final NewsFeedSearchFragment newInstance(String message, InterceptableRelativeLayout mInterceptableRelativeLayout, ViewPager pager, NewsFeedDetailFragment newsFeedDetailFragment, NewsFeedProfileFragment newsFeedProfileFragment, ResponseNewsFeed responseGson) {
        NewsFeedSearchFragment f = new NewsFeedSearchFragment();
        f.setNewsFeedDetailFragment(newsFeedDetailFragment);
        f.setNewsFeedProfileFragment(newsFeedProfileFragment);
        f.setInterceptableRelativeLayout(mInterceptableRelativeLayout);
        f.setViewPager(pager);
        Bundle bdl = new Bundle(1);
        bdl.putString(EXTRA_MESSAGE, message);
        f.setArguments(bdl);
        return f;
    }

    void setNewsFeedDetailFragment(NewsFeedDetailFragment newsFeedDetailFragment) {
        this.newsFeedDetailFragment = newsFeedDetailFragment;
    }

    void setNewsFeedProfileFragment(NewsFeedProfileFragment newsFeedProfileFragment) {
        this.newsFeedProfileFragment = newsFeedProfileFragment;
    }

    void setInterceptableRelativeLayout(InterceptableRelativeLayout view) {
        mInterceptableRelativeLayout = view;
    }

    void setViewPager(ViewPager pager) {
        this.pager = pager;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
//        String message = getArguments().getString(EXTRA_MESSAGE);

        animationBeHide = new AlphaAnimation(1.0f, 0.0f);
        animationBeHide.setDuration(500);
        animationBeHide.setStartOffset(0);

        animationBeVisiable = new AlphaAnimation(0.0f, 1.0f);
        animationBeVisiable.setDuration(500);
        animationBeVisiable.setStartOffset(0);

        View view = inflater.inflate(R.layout.fragment_newsfeed_search, container, false);

        view.setPadding(0, MainActivityTabBase.tabHeight, 0, 0);

        context = this.getContext();
        userContent = CoreData.getUserContent(context);
        propertyTypeHashMap = AppUtil.getPropertyTypeHashMap(getActivity(), userContent.systemSetting.propertyTypeList);

        ButterKnife.inject(this, view);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ((MainActivityTabBase) getActivity()).miniViewUpdateListener = this;

        //init title bar
        initTitlebar();

        //initData
        initData();

        newsfeed_item_header_height = getResources().getDimensionPixelSize(R.dimen.newsfeed_item_header_height);
        newsfeed_item_thumbnail_height = getResources().getDimensionPixelSize(R.dimen.newsfeed_item_thumbnail_height);
        newsfeed_item_bottom_height = getResources().getDimensionPixelSize(R.dimen.newsfeed_item_bottom_height);
        newsfeed_item_shadow_height = getResources().getDimensionPixelSize(R.dimen.newsfeed_item_shadow_height);
        newsfeed_icon_height = getResources().getDimensionPixelSize(R.dimen.newsfeed_icon_height);
        newsfeed_icon_bottom_height = getResources().getDimensionPixelSize(R.dimen.newsfeed_icon_bottom_height);

        min_newsfeed_icon_height = (int) (newsfeed_icon_height / 2);
        bottom_hight = newsfeed_icon_height + newsfeed_item_shadow_height;

        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.newsfeed_swipe_refresh_layout);
        mRecyclerView = (ComputeVerticalScrollOffAbleRecyclerView) view.findViewById(R.id.recycler_view);
        flEmptyView = (FrameLayout) view.findViewById(R.id.fl_empty_view);
        flEmptyNewsFeedView = (FrameLayout) view.findViewById(R.id.fl_empty_newsfeed_view);
        tvEmptyResultDesc = (TextView) view.findViewById(R.id.tv_empty_result_desc);

        newsfeed_moving_cover_view1 = view.findViewById(R.id.newsfeed_moving_cover_view1);
        newsfeed_moving_cover_view2 = view.findViewById(R.id.newsfeed_moving_cover_view2);
        newsfeed_moving_cover_view3 = view.findViewById(R.id.newsfeed_moving_cover_view3);

        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this.getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                View itemView = mLayoutManager.getChildAt(0);
                myOnScrollStateChanged(itemView, newState);

                View itemView2 = mLayoutManager.getChildAt(1);
                if (itemView2 != null) {
                    myOnScrollStateChanged(itemView2, newState);
                }
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    topItemPostion = mLayoutManager.findFirstVisibleItemPosition();
                    preLoadDetailImage(topItemPostion);
                    int intNextPostion = topItemPostion + 1;
                    if (mLayoutManager.getItemCount() > 0) {
                        if (intNextPostion < mLayoutManager.getItemCount() - 1) {
                            preLoadDetailImage(intNextPostion);
                        }
                    }
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dx == 0 && dy == 0) {
                    return;
                }
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (!hasPageEnd) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
//                            loading = false;
                            Log.v("...", "Last Item Wow !" + PAGE_NO++);
                            Log.v("...", "Last Item Wow !" + getCurrentSection().name());
                            //Do pagination.. i.e. fetch new data

                            callApi(PAGE_NO);

                        }
                    }
                }
                updateNormalViewListDisplay();
            }
        });

        mSwipeRefreshLayout.setColorSchemeResources(R.color.newsfeed_text_nuber_blue);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                agentProfilesList = new ArrayList<>();
                PAGE_NO = 1;
                hasPageEnd = false;
                isDefaultSearch = true;

                //refresh
                refreshResult();
            }
        });


        loadData();


    }

    private void refreshResult() {
        callApi(PAGE_NO);

    }


    private void initData() {
        ArrayList<RequestAddressSearch> googlePlaceList = LocalStorageHelper.getLatestSearchResultList(getActivity());
        if (googlePlaceList.size() > 0)
            HistorySearchSetting.getInstance().setLatestFiveAddressSearchList(googlePlaceList);

        Log.d(TAG, "initData: " + googlePlaceList.size());
        IS_SUPPORT_GOOGLE_SERVICE = LocationUtil.isSupportedGoogleMap(context);
    }

    private void initTitlebar() {
        mTitleBar = ((MainActivityTabBase) getActivity()).getTitleBar();

        //used in newsfeed section only

        ImageButton miniviewBtn = mTitleBar.getMiniviewBtn();
        ImageButton findAgentBtn = mTitleBar.getFindAgentBtn();
        if (isMiniview) {
            miniviewBtn.setImageResource(R.drawable.btn_nav_bar_thumb);
        } else {
            miniviewBtn.setImageResource(R.drawable.btn_nav_bar_miniview);
        }
        miniviewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isMiniview = !isMiniview;
                if (isMiniview) {
                    mTitleBar.getMiniviewBtn().setImageResource(R.drawable.btn_nav_bar_thumb);
//                                listener.showMiniViewClick();
                    updateMiniView(isMiniview);
                } else {
                    mTitleBar.getMiniviewBtn().setImageResource(R.drawable.btn_nav_bar_miniview);
//                                listener.showNormalViewClick();
                    updateMiniView(isMiniview);
                }
            }
        });

        findAgentBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConnectAgentActivity.start(getActivity(), ConnectAgentActivity.ConnectAgentActivityType.NORMAL);
            }
        });

    }

    @OnClick(R.id.btn_new_story)
    public void fetchNewStory() {

        Log.d("NewsfeedSearchFragment", "NewsfeedSearchFragment: fetchNewStory: " + " " + " " + PAGE_NO + " " + loading);
        agentProfilesList = new ArrayList<>();
        PAGE_NO = 1;
        isDefaultSearch = true;
        hasPageEnd = false;
        callApi(PAGE_NO);

        //fade out button
        tvNewStory.setVisibility(View.GONE);
//        Animation a = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out);
//        a.reset();
//        a.setDuration(2000);
//        notiNewStory.clearAnimation();
//        notiNewStory.startAnimation(a);
    }

    private void callApi(int pageNo) {
        Log.d("NewsfeedSearchFragment", "callApi" + hasPageEnd + " " + " " + " " + pageNo + " " + loading);
        Log.d("NewsfeedSearchFragment", "NewsfeedSearchFragment: fetchAPI: " + hasPageEnd + " " + " " + pageNo + " " + loading);
            if (!hasPageEnd) {
               callGetNewsFeedApi(context, this, pageNo);
                if (pageNo == 1) {
                    //show loading
//                    ((MainActivityTabBase) getActivity()).showLoadingDialog();

                    //hide new stories reminder
                    tvNewStory.setVisibility(View.GONE);
                    ((MainActivityTabBase) getActivity()).tabMenu.setCount(0, TabMenu.TAB_SECTION.NEWSFEED);
                    //update alert on tab
                    LocalStorageHelper.defaultHelper(context).saveNewCountToLocalStorage(0, TabMenu.TAB_SECTION.NEWSFEED);
                }
            }


    }


  private void callGetNewsFeedApi(final Context context, final Fragment fragment, final int page) {

      if (requestNewsFeed==null) {

          ResponseLoginSocial.Content userContent = CoreData.getUserContent(context);

          requestNewsFeed = new RequestNewsFeed(userContent.memId, LocalStorageHelper.getAccessToken(getContext()), "test_uniqueKey", 1, page);

          requestNewsFeed.callNewsFeedApi(context, new ApiCallback() {
              @Override
              public void success(ApiResponse apiResponse) {
                  ResponseNewsFeed responseGson = new Gson().fromJson(apiResponse.jsonContent, ResponseNewsFeed.class);
                  Log.d("NewsfeedSearchFragment", "NewsfeedSearchFragment: fetchAPI: " + responseGson.content.toString());
                  Log.d("NewsfeedSearchFragment", "NewsfeedSearchFragment: fetchAPI: " + responseGson.content.totalCount + " / " + responseGson.content.recordCount);

                  List<AgentProfiles> agentProfileList = new ArrayList<AgentProfiles>();
                  if (page > 1) {
                      agentProfileList.addAll(LocalStorageHelper.getLocalAgentProfileList(context, MainActivityTabBase.CURRENT_SECTION.NEWSFEED));
                  }

                  agentProfileList.addAll(responseGson.content.agentProfiles);
                  LocalStorageHelper.saveLocalAgentProfileListToLocalStorage(context, agentProfileList, MainActivityTabBase.CURRENT_SECTION.NEWSFEED);

                  if (fragment instanceof NewsFeedFragment) {
                      ((NewsFeedFragment) fragment).setViewPager(responseGson);
                  } else if (fragment instanceof NewsFeedSearchFragment) {
                     updateView(responseGson, responseGson.content.recordCount == responseGson.content.totalCount);
                  }

                  //remove alert on tab
                  LocalStorageHelper.defaultHelper(context).saveNewCountToLocalStorage(0, TabMenu.TAB_SECTION.NEWSFEED);
                  // notify about new count
//                Intent intentNewCountPush = new Intent(Consts.NEWSFEED_NEW_COUNT_EVENT);
//                LocalBroadcastManager.getInstance(context).sendBroadcast(intentNewCountPush);


                  //hide loading
                  ((MainActivityTabBase) context).hideLoadingDialog();
                  requestNewsFeed = null;

              }

              @Override
              public void failure(String errorMsg) {
                  Log.d("NewsfeedSearchFragment", "NewsfeedSearchFragment: fetchAPI: fail: " + errorMsg);
                  if (fragment instanceof NewsFeedSearchFragment) {
                      ((NewsFeedSearchFragment) fragment).stopRefresh();
                  }
                  AppUtil.showToast(context, errorMsg);
                  //hide loading
                  ((MainActivityTabBase) context).hideLoadingDialog();
                  requestNewsFeed = null;
              }
          });
      }
    }


    private void myOnScrollStateChanged(View itemView, int newState) {
        // Hide the tv_bubble when is dragging
//        if (itemView != null) {
//
//            TextView tv_bubble = (TextView) itemView.findViewById(R.id.tv_bubble);
//            if (tv_bubble != null) {
//                if (newState == RecyclerView.SCROLL_STATE_DRAGGING) {
//                    tv_bubble.startAnimation(animationBeHide);
//                    tv_bubble.setVisibility(View.GONE);
//                } else if (newState == RecyclerView.SCROLL_STATE_IDLE) {
//                    tv_bubble.startAnimation(animationBeVisiable);
//                    tv_bubble.setVisibility(View.VISIBLE);
//                } else {
//                }
//            }
//        }
    }

    private void resetItem(int postion) {
        try {
            View itemView = mLayoutManager.getChildAt(postion);
            RelativeLayout rl_headerBar = (RelativeLayout) itemView.findViewById(R.id.rl_headerBar);
            ImageView img_icon = (ImageView) itemView.findViewById(R.id.img_icon);
            rl_headerBar.setTranslationY(0);

            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) img_icon.getLayoutParams();
            params.width = (int) (newsfeed_icon_height * 1f);
            params.height = (int) (newsfeed_icon_height * 1f);

            img_icon.setLayoutParams(params);
            img_icon.setTranslationY(0);

        } catch (Exception e) {
        }

    }

    private void updateNormalViewListDisplay() {
        if (!(getCurrentSection() == MainActivityTabBase.CURRENT_SECTION.NEWSFEED && isMiniview)) {
            my_newsfeed_icon_height = newsfeed_icon_height;
            topItemPostion = mLayoutManager.findFirstVisibleItemPosition();
            int moveY = mRecyclerView.computeVerticalScrollOffset() - newsfeed_item_hight * topItemPostion;

            View itemView = mLayoutManager.getChildAt(0);
            RelativeLayout rl_headerBar = (RelativeLayout) itemView.findViewById(R.id.rl_headerBar);
            ImageView img_icon = (ImageView) itemView.findViewById(R.id.img_icon);

            if (newsfeed_item_hight - moveY > newsfeed_item_end_height) {
                rl_headerBar.setTranslationY(moveY);
            } else {
                rl_headerBar.setTranslationY(newsfeed_item_hight - newsfeed_item_end_height);
                my_newsfeed_icon_height = Math.max(min_newsfeed_icon_height, Math.min(newsfeed_icon_height, (newsfeed_item_hight - newsfeed_icon_bottom_height - min_newsfeed_icon_height - moveY)));
            }

            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) img_icon.getLayoutParams();
            params.width = my_newsfeed_icon_height;
            params.height = my_newsfeed_icon_height;
            img_icon.setLayoutParams(params);

            if (newsfeed_item_hight - moveY > bottom_hight) {
                img_icon.setTranslationY(moveY);
            } else {
                img_icon.setTranslationY(newsfeed_item_hight - bottom_hight);
            }

            resetItem(1);
        }
    }

    public void updateView(ResponseNewsFeed responseGson, boolean mHasPageEnd) {
        //update pageEnd status
        hasPageEnd = mHasPageEnd;

        if (mAdapter!=null) {
            mAdapter.showLoader(!hasPageEnd);
            mAdapter.notifyDataSetChanged();
        }

        this.responseGson = responseGson;


        Log.d(TAG, "updateView: result: " + responseGson + " " + getCurrentSection() + " " + LocalStorageHelper.getLocalAgentProfileList(getActivity(), true));


        agentProfilesList = LocalStorageHelper.getLocalAgentProfileList(getActivity(), true);


        Log.d(TAG, "updateView: resultset: " + agentProfilesList.size());


        Log.d(TAG, "updateView: updateView: " + mAdapter+ " "+agentProfilesList.size());

        if (agentProfilesList == null)
            return;

        //show empty message
        flEmptyNewsFeedView.setVisibility(agentProfilesList.size() > 0 ? View.GONE : View.VISIBLE);


        //to init the Adapter in the beginning ONLY
        if (mAdapter == null || agentProfilesList.size() == 0) {
            mAdapter = new NewsFeed_AgentProfilesAdapter(context, agentProfilesList, userContent.systemSetting.sloganList, userContent.systemSetting.propertyTypeList, propertyTypeHashMap, this) {
                @Override
                public void updateItem(AgentProfiles agentProfilesItem, int position, String imageName, String propertyTypeName) {
                    newsFeedProfileFragment.update(agentProfilesItem, position);
                    newsFeedDetailFragment.update(agentProfilesItem, position, imageName, propertyTypeName);
//                NewsFeedFragment.agentProfile = agentProfilesItem;
                    LocalStorageHelper.saveAgentProfilesToLocalStorage(context, agentProfilesItem, getCurrentSection());
                    Log.d(TAG, "updateView: " + agentProfilesItem.toString());
                }

                @Override
                public void goToProfileFragment(AgentProfiles agentProfilesItem) {
                    LocalStorageHelper.saveAgentProfilesToLocalStorage(context, agentProfilesItem, getCurrentSection());
                    pager.setCurrentItem(2, true);
                }

                @Override
                public void goToDetailFragment(AgentProfiles agentProfilesItem) {
                    LocalStorageHelper.saveAgentProfilesToLocalStorage(context, agentProfilesItem, getCurrentSection());
                    pager.setCurrentItem(0, true);
                }
            };
            mAdapter.setType(getCurrentSection(), isMiniview);
            mRecyclerView.setAdapter(mAdapter);
        } else {
            //update list
            mAdapter.setAgentProfilesList(agentProfilesList);
            mAdapter.notifyDataSetChanged();
        }

        final ViewTreeObserver vto = mRecyclerView.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                if (mLayoutManager != null && mLayoutManager.getChildAt(0) != null) {
                    newsfeed_item_hight = mLayoutManager.getChildAt(0).getHeight();
                    newsfeed_item_end_height = newsfeed_item_header_height + newsfeed_item_bottom_height + newsfeed_item_shadow_height;

//                    int[] locations = new int[2];
//                    mRecyclerView.getTop().getLocationInWindow(locations);
                    if (vto.isAlive()) {
                        // Unregister the listener to only call scrollToPosition once
                        vto.removeGlobalOnLayoutListener(this);
                        // Use vto.removeOnGlobalLayoutListener(this) on API16+ devices as
                        // removeGlobalOnLayoutListener is deprecated.
                        // They do the same thing, just a rename so your choice.
                    }

                    mInterceptableRelativeLayout.setInterceptableListener(new InterceptableListener() {

                        @Override
                        public boolean onInterceptTouchEvent(MotionEvent motionEvent) {

                            if (isShowing && viewPagerScrollState == ViewPager.SCROLL_STATE_IDLE) {
//                                Log.v("", "edwin mInterceptableRelativeLayout onInterceptTouchEvent " + motionEvent.getAction() + " x " + motionEvent.getX() + " y " + (motionEvent.getY() - MainActivityTabBase.tabHeight));
                                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                                    View child = mRecyclerView.findChildViewUnder(motionEvent.getX(), (motionEvent.getY() - MainActivityTabBase.tabHeight));
                                    try {
                                        if (child != null) {
                                            ((EnbleableSwipeViewPager) pager).setSwipeable(true);
//                                            int idx = mRecyclerView.getChildPosition(child);
//                                            Log.d("", "edwin cover onInterceptTouchEvent idx " + idx + " " + (viewLocation[1]) + " " + coverLocation[1] + " " + MainActivityTabBase.tabHeight + " " + MainActivityTabBase.titleHeight + " " + mRecyclerView.getTop() + " " + newsfeed_item_hight + " " + child.getTop());
                                            int child_Top = child.getTop();

                                            newsfeed_moving_cover_view1.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (child_Top < 0 ? 0 : child_Top)));

                                            newsfeed_moving_cover_view2.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, mAdapter.isMiniview ? mAdapter.getMinivewHeight() : newsfeed_item_hight + (child_Top > 0 ? 0 : child_Top)));

                                        }
                                    } catch (Exception e) {

                                    }

                                }
                            }
                            return false;
                        }

                        @Override
                        public boolean onTouchEvent(MotionEvent motionEvent) {

//                            Log.v("","edwin mInterceptableRelativeLayout onTouchEvent "+motionEvent.getAction());
                            if (motionEvent.getAction() == MotionEvent.ACTION_UP || motionEvent.getAction() == MotionEvent.ACTION_CANCEL) {
                                View child = mRecyclerView.findChildViewUnder(motionEvent.getX(), motionEvent.getY());

                                try {
                                    child.setAlpha(1f);
                                } catch (Exception e) {

                                }
                                ((EnbleableSwipeViewPager) pager).setSwipeable(false);
                            }
                            return false;
                        }
                    });

                }
            }
        });

        stopRefresh();

        for (int i = 0; i < agentProfilesList.size(); i++) {
            if (i == 2) {
                break;
            }
            preLoadDetailImage(i);
        }
        for (int i = 1; i < agentProfilesList.size(); i++) {
            AgentProfiles agentProfilesItem = agentProfilesList.get(i);
            Picasso.with(getActivity()).load(agentProfilesItem.agentPhotoURL).fetch();
            if (agentProfilesItem.agentListing.photos.size() > 0)
                Picasso.with(getActivity()).load(agentProfilesItem.agentListing.photos.get(0).url).fetch();
        }
    }

    public void setCoverViewAlpha(float alpha) {
        if (newsfeed_moving_cover_view1 != null) {
            newsfeed_moving_cover_view1.setAlpha(alpha);
            newsfeed_moving_cover_view3.setAlpha(alpha);
        }
    }

    void preLoadDetailImage(int i) {
        if (responseGson != null && i > 0 && i < responseGson.content.agentProfiles.size()) {
            AgentProfiles agentProfilesItem = responseGson.content.agentProfiles.get(i);
            Picasso.with(getActivity()).load(agentProfilesItem.agentPhotoURL).fetch();
            if (agentProfilesItem.agentListing.photos.size() > 0)
                Picasso.with(getActivity()).load(agentProfilesItem.agentListing.photos.get(0).url).fetch();

            for (RequestListing.Reason reason : agentProfilesItem.agentListing.reasons) {

                if (reason.mediaURL1 != null &&
                        !reason.mediaURL1.isEmpty()) {
                    Picasso.with(getActivity()).load(reason.mediaURL1).fetch();
                }

                if (reason.mediaURL2 != null &&
                        !reason.mediaURL2.isEmpty()) {
                    Picasso.with(getActivity()).load(reason.mediaURL2).fetch();
                }

                if (reason.mediaURL3 != null &&
                        !reason.mediaURL3.isEmpty()) {
                    Picasso.with(getActivity()).load(reason.mediaURL3).fetch();
                }
            }

            for (int photoIndex = 1; photoIndex < agentProfilesItem.agentListing.photos.size(); photoIndex++) {
                AgentListing.Photo photo = agentProfilesItem.agentListing.photos.get(photoIndex);
                Picasso.with(getActivity()).load(photo.url).fetch();
            }
        }
    }

    public void stopRefresh() {
        mSwipeRefreshLayout.setRefreshing(false);

        //hide loading
        ((MainActivityTabBase) getActivity()).hideLoadingDialog();
    }


    public void refresh() {
        mAdapter.notifyDataSetChanged();
    }


    @Override
    public void updateNewsfeed(boolean isCallApi) {
        List<AgentProfiles> agentProfilesList = LocalStorageHelper.getLocalAgentProfileList(getActivity(), true);
        if (agentProfilesList != null && mAdapter != null) {
            mAdapter.setAgentProfilesList(agentProfilesList);
        }
        Log.d(TAG, "updateNewsfeed: child1:  " + getCurrentSection().name() + " " + mAdapter + " " + isCallApi);
        if (isCallApi) {
            hasPageEnd = false;
            PAGE_NO = 1;
            callApi(PAGE_NO);
        } else {
            notifyDataSetChanged();
            if (LocalStorageHelper.defaultHelper(getActivity()).getNewCount(TabMenu.TAB_SECTION.NEWSFEED)>0){
                if (tvNewStory.getVisibility() == View.GONE) {
                    tvNewStory.setVisibility(View.VISIBLE);
                    Animation a = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in);
                    a.reset();
                    a.setDuration(2000);
                    tvNewStory.clearAnimation();
                    tvNewStory.startAnimation(a);
                }
            }
        }

        flEmptyView.setVisibility(View.GONE);
        flEmptyNewsFeedView.setVisibility(agentProfilesList.size() > 0 ? View.GONE : View.VISIBLE);

    }

    @Override
    public void addNewStoryTag() {
        Log.d(TAG, "addNewStoryTag " + getCurrentSection());
//        if (newsFeedType == 1) {
//            notiNewStory.setVisibility(View.GONE);
//        } else if (newsFeedType == 0) {
        if (tvNewStory.getVisibility() == View.GONE) {
            tvNewStory.setVisibility(View.VISIBLE);
            Animation a = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in);
            a.reset();
            a.setDuration(2000);
            tvNewStory.clearAnimation();
            tvNewStory.startAnimation(a);
        }
//        }
    }


    @Override
    public void updateMiniView(boolean isMiniview) {
        topItemPostion = mLayoutManager.findFirstVisibleItemPosition();
        this.isMiniview = isMiniview;
        if (mAdapter != null) {
            mAdapter.setType(getCurrentSection(), isMiniview);
            mAdapter.notifyDataSetChanged();
        }
        mLayoutManager.scrollToPosition(topItemPostion);
//        updateNormalViewListDisplay();
        resetItem(0);
        resetItem(1);
    }

    /**
     * handle the agent follower count change
     */
    public void updateAgentList(int memId, int newFollwerCount) {
        Log.d(TAG, "updateAgentList " + " " + newFollwerCount);
        int i = 0;
        for (AgentProfiles agentProfiles : agentProfilesList) {
            if (agentProfiles.memberID == memId) {
                agentProfilesList.get(i).followerCount = newFollwerCount;

                mAdapter.notifyDataSetChanged();

            }
            i++;
        }
    }


    public void loadData() {
        Log.d(TAG, "onResume " + mAdapter);
        Log.d(TAG, "onResume " + agentProfilesList.size() + " " + getCurrentSection());

        agentProfilesList = LocalStorageHelper.getLocalAgentProfileList(getActivity(), true);

        if (agentProfilesList.size() > 0) {
            //TODO: temp to set 20, hide the loading for pagination
            updateView(responseGson, agentProfilesList.size()<20);
        } else {
            callApi(PAGE_NO);
        }
        flEmptyNewsFeedView.setVisibility(agentProfilesList.size() > 0 ? View.GONE : View.VISIBLE);

    }

    public void notifyDataSetChanged() {
        Log.d(TAG, "notifyDataSetChanged " + mAdapter + " ");
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private MainActivityTabBase.CURRENT_SECTION getCurrentSection() {
        return ((MainActivityTabBase) getActivity()).currentSection;
    }

    @Override
    public ArrayList<AgentProfiles> getAgentProfileList() {
        return (ArrayList)LocalStorageHelper.getLocalAgentProfileList(context,true);
    }
}
