package co.real.productionreal2.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import co.real.productionreal2.R;

/**
 * Created by hohojo on 6/10/2015.
 */
public class MeHomeFragment extends Fragment{
    public static MeHomeFragment newInstance() {

        Bundle args = new Bundle();

        MeHomeFragment fragment = new MeHomeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_agent_profile, container, false);
        return super.onCreateView(inflater, container, savedInstanceState);
    }
}
