package co.real.productionreal2.service.model.request;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Collection;

import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.login.AccountSetting;
import co.real.productionreal2.model.InputRequest;
import co.real.productionreal2.service.AdminService;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.response.ResponseAgentProfileGetListEx;
import co.real.productionreal2.util.NetworkUtil;
import co.real.productionreal2.view.Dialog;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by hohojo on 1/3/2016.
 */
public class RequestAgentProfileGetListEx extends RequestBase {

    public RequestAgentProfileGetListEx(int memId, String session, Collection<String> qbIds) {
        super(memId, session);
        if (qbIds != null && qbIds.size() > 0) {
            this.qbIds = new ArrayList<>(qbIds);
        }
    }

    @SerializedName("QBIDList")
    public Collection<String> qbIds;

    public void callAgentProfileGetListExApi(final Context context, final ApiCallback callback) {
        String json = new Gson().toJson(this);
        final AdminService weatherService = new AdminService(context);
        weatherService.getCoffeeService().agentProfileGetListEx(new InputRequest(json), new Callback<ApiResponse>() {
            @Override
            public void success(ApiResponse apiResponse, Response response) {
                ResponseAgentProfileGetListEx responseGson = new Gson().fromJson(apiResponse.jsonContent, ResponseAgentProfileGetListEx.class);
                if (responseGson.errorCode == 4) {
                    if (context instanceof Activity)
                        Dialog.accessErrorForceLogoutDialog(context).show();
                    else
                        AccountSetting.logout(context);
                } else if (responseGson.errorCode == 26) {
                    if (context instanceof Activity)
                        Dialog.suspendedErrorForceLogoutDialog(context).show();
                    else
                        AccountSetting.logout(context);
                } else if (responseGson.errorCode != 0 && responseGson.errorCode != 20 && responseGson.errorCode != 21 &&
                        responseGson.errorCode != 22 && responseGson.errorCode != 23) {
                    callback.failure(responseGson.errorMsg);
                } else {
                    callback.success(apiResponse);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                callback.failure("");
                NetworkUtil.showNoNetworkMsg(context);
            }
        });
    }
}
