package co.real.productionreal2.activity.createpost;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.mixpanel.android.mpmetrics.MixpanelAPI;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import co.real.productionreal2.Constants;
import co.real.productionreal2.CoreData;
import co.real.productionreal2.Navigation;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.BaseFragmentActivity;
import co.real.productionreal2.adapter.Top3ReasonsPagerAdapter;
import co.real.productionreal2.callback.DialogDualCallback;
import co.real.productionreal2.config.AppConfig;
import co.real.productionreal2.fragment.Reason1Fragment;
import co.real.productionreal2.fragment.Reason2Fragment;
import co.real.productionreal2.fragment.Reason3Fragment;
import co.real.productionreal2.model.SystemSetting;
import co.real.productionreal2.service.model.CreatePost;
import co.real.productionreal2.service.model.request.RequestListing;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;
import co.real.productionreal2.view.Dialog;
import co.real.productionreal2.view.NonSwipeableViewPager;

;


/**
 * Created by hohojo on 12/10/2015.
 */
public class CreatePost3ReasonsActivity extends BaseFragmentActivity implements Reason1Fragment.Reason1FragmentListener {
    private static final String TAG = "CreatePost3ReasonsAct";
    @InjectView(R.id.rootImageView)
    ImageView rootImageView;
    @InjectView(R.id.top3resonsViewPager)
    NonSwipeableViewPager top3resonsViewPager;
    //    @InjectView(R.id.langSpinner)
//    Spinner langSpinner;
    @InjectView(R.id.tv_language_reason)
    TextView tvLanguage;
    @InjectView(R.id.reason1Btn)
    ImageButton reason1Btn;
    @InjectView(R.id.reason2Btn)
    ImageButton reason2Btn;
    @InjectView(R.id.reason3Btn)
    ImageButton reason3Btn;
    @InjectView(R.id.backBtn)
    ImageButton backBtn;
    @InjectView(R.id.nextBtn)
    Button nextBtn;
    @InjectView(R.id.whyTextView)
    TextView whyTextView;


    private ArrayAdapter<String> spinnerArrayAdapter;
    private List<String> langList = new ArrayList<>();
    private Top3ReasonsPagerAdapter pagerAdapter;
    public static final int SELECT_PHOTO_1 = 0;
    public static final int SELECT_PHOTO_2 = 1;
    public static final int SELECT_PHOTO_3 = 2;
    private ResponseLoginSocial.Content agentContent;
    private CreatePost currentCreatePost;
    private BaseCreatePost baseCreatePost;
    private int sloganIndex = 0;
    private int langIndex = 0;
    private boolean isAddLang = false;

    //    private ArrayList<String> langList=new ArrayList<>();
    private AlertDialog mConfirmDialog;
    private int selectedLang = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_post_3_reasons);

        langIndex = getIntent().getIntExtra(Constants.EXTRA_LANG_INDEX, 0);
        sloganIndex = getIntent().getIntExtra(Constants.EXTRA_SLOGAN_INDEX, 0);
        isAddLang = getIntent().getBooleanExtra(Constants.EXTRA_IS_ADD_LANG, false);


        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
//        baseCreatePost.findCurrentCreatePost(langIndex).createPostRequest.sloganIndex = getIntent().getIntExtra(Constants.EXTRA_SLOGAN_INDEX, 0);
//        RequestListing createPostRequest = baseCreatePost.findCurrentCreatePost(langIndex).createPostRequest;

        ButterKnife.inject(this);
    }

    private void initData() {
        baseCreatePost = BaseCreatePost.getInstance(this);
        agentContent = CoreData.getUserContent(this);
        langList = getLangStringList(getLangIndexList(), agentContent);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private String getLangNameByIndex(int index) {
        for (SystemSetting.SystemLanguage sysLang : agentContent.systemSetting.sysLangList) {
            if (sysLang.index == index) {
                return sysLang.nativeName;
            }
        }
        return "";
    }

    private void initView() {

        if (baseCreatePost == null)
            return;

        if (isAddLang)
            selectedLang = baseCreatePost.getReasonsDespImageList().size();


//        langList = new ArrayList<>();
//        for (int i = 0; i < baseCreatePost.getCreatePostList().size(); i++) {
//            for (SystemSetting.SpokenLanguage spokenLang:agentContent.systemSetting.spokenLangList) {
//                if (spokenLang.index == langIndex)
//                    langList.add(spokenLang.nativeName);
//            }
//        }
//        int sloganIndex = baseCreatePost.findCurrentCreatePost(langIndex).createPostRequest.sloganIndex;

//        for (RequestListing.Reason reason : baseCreatePost.getCreatePostRequest().reasons) {
//            langList.add(getLangNameByIndex(reason.languageIndex));
//        }
        Log.d(TAG, "langList: before: " + langList.toString());
        if (isAddLang) {
            langList.add(getLangNameByIndex(langIndex));
            tvLanguage.setVisibility(View.GONE);
        } else {
            //show or hide the language switch button
            if (langList.size() > 0)
                if (selectedLang == -1)
                    tvLanguage.setText(langList.get(0));
                else
                    tvLanguage.setText(langList.get(selectedLang));
            tvLanguage.setVisibility(langList.size() < 2 ? View.GONE : View.VISIBLE);
            getLangNameByIndex(langIndex);
        }

        Log.d(TAG, "langList: after: " + langList.toString());

        whyTextView.setText(getIntent().getStringExtra(Constants.EXTRA_SLOGAN));


//        if (baseCreatePost.getCurrentReasonsDespImage() == null) {
//            baseCreatePost.initCurrentReasonsImage(langIndex,sloganIndex);
//        }

        baseCreatePost.setCurrentReasonsImage(baseCreatePost.getCurrentReasonsDespImage());

        validInput();
        selectPage(0);

        spinnerArrayAdapter = new ArrayAdapter<>(this,
                R.layout.spinner_item, langList);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

//        tvLanguage.setText(getLangNameByIndex(langIndex));
//        langSpinner.setAdapter(spinnerArrayAdapter);
//        langSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });
        Log.d(TAG, "langList: langIndex: " + langIndex);

        int index = isAddLang ? langIndex : selectedLang;
        pagerAdapter = new Top3ReasonsPagerAdapter(this.getSupportFragmentManager(), getFragments(index));
        top3resonsViewPager.setAdapter(pagerAdapter);
        pagerAdapter.notifyDataSetChanged();

        reason1Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectPage(0);
            }
        });

        reason2Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BaseCreatePost.ReasonsDespImage reasonsDespImage = getCurrentReason();

                if (!reasonsDespImage.reason1.equals("") || reasonsDespImage.reason1ImagePath != null) {
                    selectPage(1);
                }
            }
        });

        reason3Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BaseCreatePost.ReasonsDespImage reasonsDespImage = getCurrentReason();

                if ((!reasonsDespImage.reason1.equals("")|| reasonsDespImage.reason1ImagePath != null) &&
                        (!reasonsDespImage.reason2.equals("") || reasonsDespImage.reason2ImagePath != null)) {
                    selectPage(2);
                }
            }
        });

        tvLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openLanguageSelectionDialog();
            }
        });
    }

    private void selectPage(int index) {
        top3resonsViewPager.setCurrentItem(index);
        switch (index) {
            case 0:
                reason1Btn.setSelected(true);
                reason2Btn.setSelected(false);
                reason3Btn.setSelected(false);
                break;
            case 1:
                reason1Btn.setSelected(false);
                reason2Btn.setSelected(true);
                reason3Btn.setSelected(false);
                break;
            case 2:
                reason1Btn.setSelected(false);
                reason2Btn.setSelected(false);
                reason3Btn.setSelected(true);
                break;
        }
    }

    private boolean checkIfReasonFilled(int index) {
        BaseCreatePost.ReasonsDespImage reasonsDespImage = getCurrentReason();
        switch (index) {
            case 2:
                if (reasonsDespImage.reason1 == null && reasonsDespImage.reason1ImagePath == null)
                    return true;
            case 1:
                if (!reasonsDespImage.reason1.isEmpty() || reasonsDespImage.reason1ImagePath != null
                        || !reasonsDespImage.reason2.isEmpty() || reasonsDespImage.reason2ImagePath != null)
                    return true;
            case 0:
                return true;
            default:
                return false;
        }
    }

    private BaseCreatePost.ReasonsDespImage getCurrentReason() {
        BaseCreatePost.ReasonsDespImage reasonsDespImage;
        if (BaseCreatePost.getInstance(CreatePost3ReasonsActivity.this).getReasonsDespImageList().size() > 0 && selectedLang < baseCreatePost.getReasonsDespImageList().size())
            reasonsDespImage = BaseCreatePost.getInstance(CreatePost3ReasonsActivity.this).getCurrentReasonsDespImage(selectedLang);
        else
            reasonsDespImage = BaseCreatePost.getInstance(CreatePost3ReasonsActivity.this).getCurrentReasonsDespImage();
        return reasonsDespImage;
    }


    private List<Fragment> getFragments(int langIndex) {
        List<Fragment> fList = new ArrayList<Fragment>();
        Reason1Fragment reason1Fragment = Reason1Fragment.newInstance(selectedLang,this);
        Reason2Fragment reason2Fragment = Reason2Fragment.newInstance(selectedLang);
        Reason3Fragment reason3Fragment = Reason3Fragment.newInstance(selectedLang);

        fList.add(reason1Fragment);
        fList.add(reason2Fragment);
        fList.add(reason3Fragment);
        return fList;
    }


    @Override
    protected void onResume() {
        super.onResume();
//        spinnerArrayAdapter.clear();
//        spinnerArrayAdapter.addAll(langList);
        initData();
        initView();
        spinnerArrayAdapter.notifyDataSetChanged();
    }

    @OnClick(R.id.closeBtn)
    public void closeBtnClick(View view) {
        Dialog.exitSaveChangeDialog(this, new DialogDualCallback() {
            @Override
            public void positive() {
                //reset value
                BaseCreatePost.getInstance(CreatePost3ReasonsActivity.this).resetBaseCreatePost();
                exitActivity();
            }

            @Override
            public void negative() {
                //exit
                exitActivity();
            }
        }).show();
    }

    private void exitActivity() {
        setResult(RESULT_OK);
        finish();
    }

    @OnClick(R.id.backBtn)
    public void backBtnClick(View view) {
        switch (top3resonsViewPager.getCurrentItem()) {
            case 0:
                //todo if createPostList.size > 1, dialog create, cancel object
                onBackPressed();
                break;
            case 1:
                reason1Btn.performClick();
                break;
            case 2:
                reason2Btn.performClick();
                break;
        }

    }


    @OnClick(R.id.nextBtn)
    public void nextBtnClick(View view) {
        // Mixpanel
        MixpanelAPI mixpanel = MixpanelAPI.getInstance(this, AppConfig.getMixpanelToken());
        mixpanel.track("Saved Posting Details_Three Reasons");

        RequestListing.Reason currReason = new RequestListing.Reason();
        currReason.languageIndex = langIndex;//English

        switch (top3resonsViewPager.getCurrentItem()) {
            case 0:
                reason2Btn.performClick();
                break;
            case 1:
                BaseCreatePost.ReasonsDespImage reasonsDespImage = getCurrentReason();
                if (!reasonsDespImage.reason2.equals("")|| reasonsDespImage.reason2ImagePath != null) {
                    reason3Btn.performClick();
                } else {
                    saveReasonsAndToNextActivity();
                }
                break;
            case 2:
                saveReasonsAndToNextActivity();
                break;
        }
    }

    private void saveReasonsAndToNextActivity() {
        BaseCreatePost.ReasonsDespImage reasonsDespImage = getCurrentReason();
        Log.d(TAG, "saveReasonsAndToNextActivity: " +reasonsDespImage.reason1ImagePath+ " "+reasonsDespImage.reason1.equals(""));
        if (reasonsDespImage.reason1ImagePath != null ||
                (!reasonsDespImage.reason1.equals("") && reasonsDespImage.reason1!=null)) {
            int replaceResonsDespImageIndex = isReasonsNeedReplace(reasonsDespImage);
            if (replaceResonsDespImageIndex == -1) {
                addReasonsAndDespImage(reasonsDespImage);
            } else {
                replaceResonsAndDespImage(reasonsDespImage, replaceResonsDespImageIndex);
            }
            BaseCreatePost.getInstance(this).formatReasonInRequest();
            Intent intent;
            if (isAddLang) {
                setResult(RESULT_OK);
                //reset value
                isAddLang = false;
                finish();
            } else {
                intent = new Intent(this, CreatePostPhotosActivity.class);
                intent.putExtra(Constants.EXTRA_LANG_INDEX, langIndex);
                Navigation.pushIntentForResult(this, intent, CreatePostPropertyActivity.FINISH_POST);
            }
        }

    }

    private void validInput() {
        BaseCreatePost.ReasonsDespImage reasonsDespImage = getCurrentReason();
        if (reasonsDespImage!=null) {
            nextBtn.setEnabled(reasonsDespImage.reason1ImagePath != null ||
                    (!reasonsDespImage.reason1.equals("") && reasonsDespImage.reason1 != null));
            Log.d(TAG, "validInput: " +reasonsDespImage.reason1ImagePath+ " "+reasonsDespImage.reason1.equals(""));
        }
    }

    private int isReasonsNeedReplace(BaseCreatePost.ReasonsDespImage currentReasonsDespImage) {
        if (BaseCreatePost.getInstance(this).getReasonsDespImageList().size() > 0) {
            for (int i = 0; i < BaseCreatePost.getInstance(this).getReasonsDespImageList().size(); i++) {
                if (BaseCreatePost.getInstance(this).getReasonsDespImageList().get(i).langIndex == currentReasonsDespImage.langIndex)
                    return i;
            }
        }
        return -1;
    }


    private void addReasonsAndDespImage(BaseCreatePost.ReasonsDespImage currentReasonsDespImage) {
//        BaseCreatePost.getInstance(this).addReasonsToRequest();
        BaseCreatePost.getInstance(this).getReasonsDespImageList().add(currentReasonsDespImage);
    }

    private void replaceResonsAndDespImage(BaseCreatePost.ReasonsDespImage currentReasonsDespImage, int index) {
//        BaseCreatePost.getInstance(this).replaceReasonsInRequest(selectedLang);
        BaseCreatePost.getInstance(this).getReasonsDespImageList().set(index, currentReasonsDespImage);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        Log.d(TAG, "onActivityResult: before: " + top3resonsViewPager.getCurrentItem());
        pagerAdapter.notifyDataSetChanged();
//        int index=isAddLang?langIndex:selectedLang;
//        List<Fragment> fragments = getFragments(index);
        List<Fragment> fragments = this.getSupportFragmentManager().getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                if (fragment != null)
                    fragment.onActivityResult(requestCode, resultCode, data);
            }
        }
        if (resultCode == RESULT_OK && requestCode == CreatePostPropertyActivity.FINISH_POST) {
            setResult(RESULT_OK, data);
            finish();
            return;
        }

        final int currentPage = top3resonsViewPager.getCurrentItem();

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
//                reason2Btn.performClick();
//                pagerAdapter.notifyDataSetChanged();
                selectPage(currentPage);
                Log.d(TAG, "onActivityResult delay" + currentPage);
            }
        }, 300);

        Log.d(TAG, "onActivityResult: later " + top3resonsViewPager.getCurrentItem());

        if (currentCreatePost!=null)
        validInput();
    }

    private void openLanguageSelectionDialog() {

        final ArrayAdapter<String> mLanguageList = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_checked, langList);
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this)
                .setSingleChoiceItems(mLanguageList, selectedLang, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        tvLanguage.setText(langList.get(i));
//                        SearchFilterSetting.getInstance().setSizeUnitType(selectedMetric);
//                        selectedLang = AppUtil.getMeticPositionFromIndex(langList, AppUtil.getMetricIndexFromName(CreatePost3ReasonsActivity.this, langList.get(i)));
                        dialogInterface.dismiss();
                        selectedLang = i;
                        langIndex = getLangIndexList().get(i);
                        Log.d(TAG, "openLanguageSelectionDialog: " + langIndex);
                        initView();
                    }
                });

//        mConfirmDialog.setTitle(R.string.spoken_lang);
        mConfirmDialog = dialogBuilder.create();
        mConfirmDialog.show();
    }

    private ArrayList<Integer> getLangIndexList() {
        ArrayList<Integer> langIndexList = new ArrayList<>();
        for (RequestListing.Reason reason : baseCreatePost.getCreatePostRequest().reasons) {
            langIndexList.add(reason.languageIndex);
        }
        return langIndexList;
    }

    private ArrayList<String> getLangStringList(ArrayList<Integer> langIndexList, ResponseLoginSocial.Content userContent) {
        ArrayList<String> langStringList = new ArrayList<>();

        for (Integer langIndex : langIndexList) {
            for (SystemSetting.SystemLanguage systemLanguage : userContent.systemSetting.sysLangList) {
                if (systemLanguage.index == langIndex) {
                    langStringList.add(systemLanguage.nativeName);
                }
            }
        }
        return langStringList;
    }

    @Override
    public void reason1TextChange(String reason1) {
        validInput();
    }
}
