package co.real.productionreal2.service.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by hohojo on 18/11/2015.
 */
public class ChatRoomMember implements Parcelable {
    @SerializedName("MemberID")
    public int memId;
    @SerializedName("QBID")
    public String qbId;
    @SerializedName("Name")
    public String name;
    @SerializedName("PhotoURL")
    public String photoUrl;
    @SerializedName("Blocked")
    public int blocked;
    @SerializedName("Muted")
    public int muted;
    @SerializedName("Deleted")
    public int deleted;
    @SerializedName("Exited")
    public int exited;

    protected ChatRoomMember(Parcel in) {
        memId = in.readInt();
        qbId = in.readString();
        name = in.readString();
        photoUrl = in.readString();
        blocked = in.readInt();
        muted = in.readInt();
        deleted = in.readInt();
        exited = in.readInt();
    }

    public static final Creator<ChatRoomMember> CREATOR = new Creator<ChatRoomMember>() {
        @Override
        public ChatRoomMember createFromParcel(Parcel in) {
            return new ChatRoomMember(in);
        }

        @Override
        public ChatRoomMember[] newArray(int size) {
            return new ChatRoomMember[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeInt(memId);
        dest.writeString(qbId);
        dest.writeString(name);
        dest.writeString(photoUrl);
        dest.writeInt(blocked);
        dest.writeInt(muted);
        dest.writeInt(deleted);
        dest.writeInt(exited);
    }
}
