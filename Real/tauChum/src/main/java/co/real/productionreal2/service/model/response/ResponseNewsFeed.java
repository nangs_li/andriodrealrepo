package co.real.productionreal2.service.model.response;

import com.google.gson.annotations.SerializedName;
import co.real.productionreal2.service.model.AgentProfiles;

import java.util.List;

/**
 * Created by hohojo on 23/11/2015.
 */
public class ResponseNewsFeed extends ResponseBase{
@SerializedName("Content")
public Content content;

public class Content {
    @SerializedName("TotalCount")
    public int totalCount;
    @SerializedName("RecordCount")
    public int recordCount;
    @SerializedName("FullResultSet")
    public List<Integer> fullResultSet;
    @SerializedName("AgentProfiles")
    public List<AgentProfiles> agentProfiles;

    @Override
    public String toString() {
        return "Content{" +
                "totalCount=" + totalCount +
                ", recordCount=" + recordCount +
                ", fullResultSet=" + fullResultSet +
                ", agentProfiles=" + agentProfiles +
                '}';
    }
}
}
