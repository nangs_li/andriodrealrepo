package co.real.productionreal2.util;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Build;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListAdapter;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.gson.Gson;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.UUID;

import co.real.productionreal2.BuildConfig;
import co.real.productionreal2.Constants;
import co.real.productionreal2.CoreData;
import co.real.productionreal2.R;
import co.real.productionreal2.activity.MainActivityTabBase;
import co.real.productionreal2.common.LocalStorageHelper;
import co.real.productionreal2.dao.account.DBLogInfo;
import co.real.productionreal2.daomanager.DatabaseManager;
import co.real.productionreal2.model.SystemSetting;
import co.real.productionreal2.service.model.response.ResponseLoginSocial;
import lib.blurbehind.BlurBehind;

/**
 * Created by kelvinsun on 24/12/15.
 */
public class AppUtil {

    private static HashMap<String, String> propertyTypeHashMap = new HashMap<>();
    private static Toast mToast = null;

    /**
     * generate a unique key
     * @return
     */
    public static String getUniqueKey(){
        return UUID.randomUUID().toString();
    }

    public static String formatAmount(double num) {
        DecimalFormat decimalFormat = new DecimalFormat();
        DecimalFormatSymbols decimalFormateSymbol = new DecimalFormatSymbols();
        decimalFormateSymbol.setGroupingSeparator(',');
        decimalFormat.setDecimalFormatSymbols(decimalFormateSymbol);
        return decimalFormat.format(num);
    }

    public static String formatAmount(int num) {
        DecimalFormat decimalFormat = new DecimalFormat();
        DecimalFormatSymbols decimalFormateSymbol = new DecimalFormatSymbols();
        decimalFormateSymbol.setGroupingSeparator(',');
        decimalFormat.setDecimalFormatSymbols(decimalFormateSymbol);
        return decimalFormat.format(num);
    }

    public static String getCurrentLangCode(Context context) {
        String langCode = context.getResources().getConfiguration().locale.toString();
        Log.d("getCurrentLangCode", context.getResources().getConfiguration().locale.getDisplayLanguage());
        Log.d("getCurrentLangCode", context.getResources().getConfiguration().locale.getLanguage());
        Log.d("getCurrentLangCode", context.getResources().getConfiguration().locale.getDisplayLanguage(Locale.getDefault()));
        Log.d("getCurrentLangCode", context.getResources().getConfiguration().locale.getCountry());
        Log.d("getCurrentLangCode", context.getResources().getConfiguration().locale.getDisplayCountry());
//        if (langCode.equalsIgnoreCase("zh_TW")) {
//            return "zh-CHT";
//        } else if (langCode.equalsIgnoreCase("zh_CN")) {
//            return "zh-CHS";
//        } else {
        return langCode;
//        }
    }

    public static String getSelectedMetricName(Context context) {
        String name = "";
        DBLogInfo dbLogInfo = DatabaseManager.getInstance(context).getLogInfo();
        int langIndex = AppUtil.getCurrentLangIndex(context);
        List<String> metricNameList = FileUtil.getSystemSetting(context).obtainMetricNameList(langIndex);
        List<Integer> metricIndexList = FileUtil.getSystemSetting(context).obtainMetricIndexList(langIndex);
        if (dbLogInfo != null) {
            int selectedMetric = AppUtil.getMeticPositionFromIndex(metricIndexList, dbLogInfo.getMetricIndex() == null ? 0 : dbLogInfo.getMetricIndex());
            name = metricNameList.get(selectedMetric);
        }
        return name;
    }

    public static String getCurrentLangNativeName(Context context) {
        return AppUtil.getLangNativeNameFromLocale(context, AppUtil.getCurrentLangLocale(context));
    }

    public static String getCurrentLangISO2Code(Context context) {
        Locale locale = context.getResources().getConfiguration().locale;
        String langCode = locale.getLanguage();
        if (langCode.equalsIgnoreCase("zh")) {
            langCode = langCode + "-" + locale.getCountry();
        }
        return langCode;
    }

    public static Locale getCurrentLangLocale(Context context) {
        return context.getResources().getConfiguration().locale;
    }

    public static int getCurrentLangIndex(Context context) {
        if (CoreData.langIndex == -1) {
            CoreData.langIndex = LocalStorageHelper.getLangIndex(context);
            if (CoreData.langIndex == -1) {
                CoreData.langIndex = getLangIndexFromLocale(context, context.getResources().getConfiguration().locale);
                LocalStorageHelper.setLangIndex(context, CoreData.langIndex);
            }
        }
        return CoreData.langIndex;
    }

    public static String getLangNativeNameFromLocale(Context context, Locale locale) {
        String nativeName = "";
        SystemSetting systemSetting = FileUtil.getSystemSetting(context);
        if (locale.toString().equalsIgnoreCase("zh_TW") || locale.toString().equalsIgnoreCase("zh_HK")) {
            for (SystemSetting.SystemLanguage systemLanguage : systemSetting.sysLangList) {
                if (systemLanguage.code.equalsIgnoreCase("zh-CHT")) {
                    return systemLanguage.nativeName;
                }
            }
        } else if (locale.toString().equalsIgnoreCase("zh_CN")) {
            for (SystemSetting.SystemLanguage systemLanguage : systemSetting.sysLangList) {
                if (systemLanguage.code.equalsIgnoreCase("zh-CHS")) {
                    return systemLanguage.nativeName;
                }
            }
        } else {
            for (SystemSetting.SystemLanguage systemLanguage : systemSetting.sysLangList) {
                if (locale.getLanguage().equalsIgnoreCase(systemLanguage.code)) {
                    return systemLanguage.nativeName;
                }
            }
        }
        return nativeName;
    }

    public static int getLangIndexFromLocale(Context context, Locale locale) {
        //check if there is any system setting in local
        SystemSetting systemSetting = FileUtil.getSystemSetting(context);
        if (systemSetting == null)
            return -1;

        if (locale.toString().equalsIgnoreCase("zh_TW") || locale.toString().equalsIgnoreCase("zh_HK")) {
            for (SystemSetting.SystemLanguage systemLanguage : systemSetting.sysLangList) {
                if (systemLanguage.code.equalsIgnoreCase("zh-CHT")) {
                    return systemLanguage.index;
                }
            }
        } else if (locale.toString().equalsIgnoreCase("zh_CN")) {
            for (SystemSetting.SystemLanguage systemLanguage : systemSetting.sysLangList) {
                if (systemLanguage.code.equalsIgnoreCase("zh-CHS")) {
                    return systemLanguage.index;
                }
            }
        } else {
            for (SystemSetting.SystemLanguage systemLanguage : systemSetting.sysLangList) {
                if (locale.getLanguage().equalsIgnoreCase(systemLanguage.code)) {
                    return systemLanguage.index;
                }
            }
        }
        return -1;
    }

    public static String getPropertyTypeImageNameMini(int propertyTypeIndex, int spaceTypeIndex){
        return getPropertyTypeImageName(propertyTypeIndex,spaceTypeIndex)+"_mini";
    }


    public static String getPropertyTypeImageName(int propertyTypeIndex, int spaceTypeIndex) {
        String imageName = "ico_type_space_apartment";
        if (propertyTypeIndex == 0) {
            switch (spaceTypeIndex) {
                case 0:
                    //do something
                    imageName = "ico_type_space_apartment";
                    break;
                case 1:
                    //do something
                    imageName = "ico_type_space_unit";
                    break;
                case 2:
                    //do something
                    imageName = "ico_type_space_townhouse";
                    break;
                case 3:
                    //do something
                    imageName = "ico_type_space_loft";
                    break;
                case 4:
                    //do something
                    imageName = "ico_type_space_houseboat";
                    break;
                case 5:
                    //do something
                    imageName = "ico_type_space_villa";
                    break;
                case 6:
                    //do something
                    imageName = "ico_type_space_land";
                    break;
                case 7:
                    //do something
                    imageName = "ico_type_space_condos";
                    break;
                case 8:
                    //do something
                    imageName = "ico_type_space_suburban";
                    break;
                case 9:
                    //do something
                    imageName = "ico_type_space_others";
                    break;
                default:
                    //do something
                    imageName = "ico_type_space_others";
                    break;
            }
        } else {
            switch (spaceTypeIndex) {
                case 0:
                    //do something
                    imageName = "ico_type_space_office";
                    break;
                case 1:
                    //do something
                    imageName = "ico_type_space_industrial";
                    break;
                case 2:
                    //do something
                    imageName = "ico_type_space_warehouse";
                    break;
                case 3:
                    //do something
                    imageName = "ico_type_space_flex";
                    break;
                case 4:
                    //do something
                    imageName = "ico_type_space_retail";
                    break;
                case 5:
                    //do something
                    imageName = "ico_type_space_land";
                    break;
                case 6:
                    //do something
                    imageName = "ico_type_space_agricultural";
                    break;
                case 7:
                    //do something
                    imageName = "ico_type_space_hotel";
                    break;
                case 8:
                    //do something
                    imageName = "ico_type_space_health";
                    break;
                case 9:
                    //do something
                    imageName = "ico_type_space_special";
                    break;
                case 10:
                    //do something
                    imageName = "ico_type_space_others";
                    break;
                default:
                    //do something
                    imageName = "ico_type_space_others";
                    break;
            }
        }
        return imageName;
    }

    public static void setupPropertyTypeList(Context context, List<SystemSetting.PropertyType> propertyTypeList) {
        HashMap<String, String> hashMap = new HashMap<>();
        for (SystemSetting.PropertyType propertyTypeItem : propertyTypeList) {
            if (propertyTypeItem.langIndex == getCurrentLangIndex(context)) {
                for (SystemSetting.SpaceType spaceIndex : propertyTypeItem.spaceTypeList) {
                    hashMap.put(propertyTypeItem.index + "," + spaceIndex.index, spaceIndex.spaceTypeName);
                }
            }
        }
        LocalStorageHelper.savePropertyTypeHashMap(context, hashMap);
        Log.d("setupPropertyTypeList", "setupPropertyTypeList: hashmap: " + hashMap.toString());
    }

    public static void setupPropertyTypeHashMap(Context context, List<SystemSetting.PropertyType> propertyTypeList) {
        AppUtil.setupPropertyTypeList(context, propertyTypeList);
        propertyTypeHashMap = LocalStorageHelper.loadPropertyTypeHashMap(context);
    }

    public static HashMap<String, String> getPropertyTypeHashMap(Context context, List<SystemSetting.PropertyType> propertyTypeList) {
        if (propertyTypeHashMap == null || propertyTypeHashMap.size() == 0) {
            setupPropertyTypeHashMap(context, propertyTypeList);
        }
        return propertyTypeHashMap;
    }

    public static HashMap<String, String> getPropertyTypeHashMap(Context context) {
        if (propertyTypeHashMap == null || propertyTypeHashMap.size() == 0) {
            propertyTypeHashMap = LocalStorageHelper.loadPropertyTypeHashMap(context);
        }
        return propertyTypeHashMap;
    }


    public static String getSlogan(List<SystemSetting.Slogan> sloganList, int languageIndex, int sloganIndex) {
        for (SystemSetting.Slogan sloganItem : sloganList) {
            if (sloganItem.langIndex == languageIndex && sloganItem.index == sloganIndex) {
                return sloganItem.slogan;
            }
        }
        return "";
    }

    public static List<SystemSetting.Slogan> getSloganListByLocale(List<SystemSetting.Slogan> sloganList, int languageIndex) {
        List<SystemSetting.Slogan> currSloganList = new ArrayList<>();
        for (SystemSetting.Slogan sloganItem : sloganList) {
            if (sloganItem.langIndex == languageIndex) {
                currSloganList.add(sloganItem);
            }
        }
        return currSloganList;
    }


    public static void hideKeyboard(Context context, EditText editText) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    public static void showKeyboard(Context context, EditText editText) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editText, 0);
    }

    public static int measureContentWidth(Context context, ListAdapter listAdapter) {
        ViewGroup mMeasureParent = null;
        int maxWidth = 0;
        View itemView = null;
        int itemType = 0;

        final ListAdapter adapter = listAdapter;
        final int widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        final int heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        final int count = adapter.getCount();
        for (int i = 0; i < count; i++) {
            final int positionType = adapter.getItemViewType(i);
            if (positionType != itemType) {
                itemType = positionType;
                itemView = null;
            }

            if (mMeasureParent == null) {
                mMeasureParent = new FrameLayout(context);
            }

            itemView = adapter.getView(i, itemView, mMeasureParent);
            itemView.measure(widthMeasureSpec, heightMeasureSpec);

            final int itemWidth = itemView.getMeasuredWidth();

            if (itemWidth > maxWidth) {
                maxWidth = itemWidth;
            }
        }

        return maxWidth;
    }

    public static void showDialog(Context context, String title, String msg, boolean isOptional) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        // set title
        alertDialogBuilder.setTitle("");

        // set dialog message
        alertDialogBuilder
                .setMessage(title)
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, close
                    }
                })
                .setNegativeButton(isOptional ? context.getString(android.R.string.cancel) : "", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        dialog.cancel();
                    }
                });


        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    public static String getColoredString(String text, int color) {
        String hex_value = color < 0
                ? "-" + Integer.toHexString(-color)
                : Integer.toHexString(color);
        return "<font color=\"#" + hex_value + "\">" + text + "</font>";
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public static void forceRTLIfSupported(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            activity.getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        }
    }

    /**
     * General message show in toast
     *
     * @param context
     * @param message
     */
    public static void showToast(Context context, String message) {

        if (context == null) return;

        if (!TextUtils.isEmpty(message) && message.trim() != "") {
            if (mToast != null) {
                mToast.cancel();
            }
            mToast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
            mToast.show();
        }
    }

    /**
     * General system message show in toast
     *
     * @param context
     * @param messageId
     */
    public static void showToast(Context context, int messageId) {

        if (context == null) return;

        if (mToast != null) {
            mToast.cancel();
        }
        mToast = Toast.makeText(context, messageId, Toast.LENGTH_SHORT);
        mToast.show();
    }

    /**
     * check if the app is running on foreground
     *
     * @param context
     * @return
     */
    public static boolean isAppOnForeground(Context context) {
        // Returns a list of application processes that are running on the
        // device

        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        String packageName = context.getPackageName();

        List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager
                .getRunningAppProcesses();
        if (appProcesses == null)
            return false;

        for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
            // The name of the process that this object is associated with.
            if (appProcess.processName.equals(packageName)
                    && appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                return true;
            }
        }
        return false;

    }

    /**
     * change Locale language
     *
     * @param lang
     * @return
     */
    public static void setLocale(Context context, String lang) {
        Locale myLocale = new Locale(lang);
        setLocale(context, myLocale);
    }

    public static void setLocale(Context context, Locale myLocale) {
        Log.w("setLocale", "myLocale " + myLocale.toString());
        Resources res = context.getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
        LocalStorageHelper.saveCurrentLocaleToLocalStorage(context, myLocale);
    }

    public static void setBlurBehind(Activity mActivity) {
        BlurBehind.getInstance()
                .withAlpha(40)
                .withFilterColor(Color.parseColor("#FFFFFF"))
                .withRadius(20)
                .setBackground(mActivity);
    }

    /**
     * Metric preference
     */

    public static int getMetricIndexFromName(Context context, String metricName) {
        DBLogInfo dbLogInfo = DatabaseManager.getInstance(context).getLogInfo();
        ResponseLoginSocial.Content userContent = new Gson().fromJson(dbLogInfo.getLogInfo(), ResponseLoginSocial.Content.class);
        for (SystemSetting.Metric metric : userContent.systemSetting.metricList) {
            if (metric.nativeName.equalsIgnoreCase(metricName))
                return metric.index;
        }
        return 0;
    }

    public static int getMeticPositionFromIndex(List<Integer> metricIndexList, int index) {
        for (int i = 0; i < metricIndexList.size(); i++) {
            if (metricIndexList.get(i) == index)
                return i;
        }
        return 0;
    }

    public static int getMeticIndexFromPosition(List<Integer> metricIndexList, int position) {
        return metricIndexList.get(position);
    }

    public static String longestWord(String s) {
        return longestWord(s.split(" "), 0, 0);
    }

    public static String longestWord(String[] words, int currentIdx, int longestIdx) {
        if (currentIdx == words.length)
            return words[longestIdx];
        int idx;  // temporarily stores the index of the current longest word
        if (words[currentIdx].length() > words[longestIdx].length())
            idx = currentIdx;
        else
            idx = longestIdx;
        return longestWord(words, currentIdx + 1, idx);
    }

    public static ArrayList<String> removeDuplicate(ArrayList<String> values) {
        Object[] st = values.toArray();
        for (Object s : st) {
            if (values.indexOf(s) != values.lastIndexOf(s)) {
                values.remove(values.lastIndexOf(s));
            }
        }
        return values;
    }


    /**
     * get the app version
     *
     * @param context
     * @return
     */
    public static String getAppVersion(Context context) {
        String versionName = "";
        if (Constants.isDevelopment) {
            versionName = BuildConfig.VERSION_NAME + " (" + BuildConfig.FLAVOR + ")";
        } else {
            versionName = BuildConfig.VERSION_NAME;
        }
        return context.getString(R.string.about_this_version__version) +" : "+ versionName;
    }


    public static String getBuildVersion(Context context) {
        return context.getString(R.string.about_this_version__build) + " : "+ BuildConfig.BuildNumber + " [" + BuildConfig.GitHash + "]";
    }

    /**
     * Check if correct Play Services version is available on the device.
     *
     * @param context
     * @return boolean
     */
    public static boolean checkGooglePlayServiceAvailability(Context context, int versionCode) {

        // Query for the status of Google Play services on the device
        int statusCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);

        if ((statusCode == ConnectionResult.SUCCESS)
                && (GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_VERSION_CODE >= versionCode)) {
            return true;
        } else {
            return false;
        }
    }

    public static void goToMainTabActivity(Activity currentActivity, final ResponseLoginSocial.Content content) {
        if (content.systemSetting != null) {
            FileUtil.setSystemSetting(currentActivity, content);
        }
        Intent mainIntent = new Intent(currentActivity, MainActivityTabBase.class);

        mainIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        currentActivity.startActivity(mainIntent);
        currentActivity.finish();
        currentActivity.overridePendingTransition(0, 0);

    }

    public static String dateStringForMixpanel() {

        final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        return dateFormat.format(new java.util.Date());
    }

    public static void showAlertDialog(Context context, String msg) {
        showAlertDialog(context,"",msg);
    }

    public static void showAlertDialog(Context context, String title, String msg) {
        if(msg == null){
            msg = "";
        }

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);
        if(title != null && !title.isEmpty()){
            alertDialogBuilder.setTitle("");
        }

        alertDialogBuilder
                .setTitle(title)
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, close
                    }
                });


        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }


}
