package co.real.productionreal2.util;

import android.support.annotation.IntegerRes;
import android.text.TextUtils;
import android.util.Log;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

/**
 * Created by hohojo on 15/12/2015.
 */
public class ValidUtil {

    public final static boolean isValidEmail(String target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public final static boolean isValidPassword(String target) {
        return !TextUtils.isEmpty(target) && target.length()>=4 && target.length()<=20;
    }

    public final static boolean isValidPhoneNumber(String countryCode,String phoneNum) {
        Log.d("isValidPhoneNumber", "countryCode: "+countryCode+ " "+phoneNum);
        if (phoneNum.isEmpty() || countryCode.isEmpty())
            return false;
        int cc= Integer.valueOf(countryCode.trim().replace("+",""));
        long pn=Long.valueOf(phoneNum.trim());
        Phonenumber.PhoneNumber mbileNumber =
                new Phonenumber.PhoneNumber().setCountryCode(cc).setNationalNumber(pn);
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
        return phoneUtil.isValidNumber(mbileNumber);
    }
}
