package co.real.productionreal2.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import co.real.productionreal2.R;

/**
 * Created by kelvinsun on 30/12/15.
 */
public class SearchResultRow extends LinearLayout {
    private TextView tVAddressName;
    private TextView tvNumOfAgents;
    private Context context;

    public SearchResultRow(Context context) {
        super(context);
        this.context=context;
        initView();
    }

    public SearchResultRow(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context=context;
        initView();
    }

    private void initView() {
        inflate(getContext(), R.layout.view_search_result_row, this);

        // find views
        tVAddressName = (TextView) findViewById(R.id.tv_address_name);
        tvNumOfAgents = (TextView) findViewById(R.id.tv_num_of_agent);
        addListener();
    }

    private void addListener() {
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
    }

    public void setRegion(String region){
        tVAddressName.setText(region);
    }

    public void setNumOfAgent(String numOfAgent){
        tvNumOfAgents.setText(numOfAgent);
    }

}
