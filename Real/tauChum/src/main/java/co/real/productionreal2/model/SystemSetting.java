package co.real.productionreal2.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;


public class SystemSetting implements Serializable {
    private static final long serialVersionUID = 7038950194227279279L;

    public static Metric[] metricArray;

    public static String[] currencyArray;

    @SerializedName("Version")
    public int version;

    @SerializedName("SMSCountryCodeList")
    public List<CountryCodeInfo> smsCountryCodeInfoList;

    @SerializedName("SpokenLanguageList")
    public List<SpokenLanguage> spokenLangList;

    @SerializedName("SystemLanguageList")
    public List<SystemLanguage> sysLangList;

    @SerializedName("PropertyTypeList")
    public List<PropertyType> propertyTypeList;


    ///////////////////////////

    @SerializedName("MetricList")
    public List<Metric> metricList;


    @SerializedName("CurrencyList")
    public List<String> currencyList;

    @SerializedName("GoogleAddressLanguageList")
    public List<GoogleAddressLanguage> googleAddressLanguageList;

    @SerializedName("SloganList")
    public List<Slogan> sloganList;

    @SerializedName("TopFiveCitiesList")
    public List<TopFiveCities> topFiveCitiesList;

    public ArrayList<Metric> obtainMetricList(int langIndex){
        ArrayList<Metric> currentMetricArrayList=new ArrayList<>();
        for (Metric metric:metricList)
        {
            if (metric.langIndex==langIndex)
            currentMetricArrayList.add(metric);
        }
        return currentMetricArrayList;
    }

    public List<String> obtainMetricNameList(int langIndex){
        ArrayList<String> currentMetricArrayList=new ArrayList<>();
        for (Metric metric:metricList)
        {
            if (metric.langIndex==langIndex) {
                currentMetricArrayList.add(metric.nativeName);
            }
        }
        return currentMetricArrayList;
    }

    public List<Integer> obtainMetricIndexList(int langIndex){
        ArrayList<Integer> currentMetricIndexArrayList=new ArrayList<>();
        for (Metric metric:metricList)
        {
            if (metric.langIndex==langIndex) {
                currentMetricIndexArrayList.add(metric.index);
            }
        }
        return currentMetricIndexArrayList;
    }


    public List<String> obtainCurrencyNameList(){
        ArrayList<String> currencyArrayList=new ArrayList<>();
        for (String currency:currencyList)
        {
            currencyArrayList.add(currency);
        }
        return currencyArrayList;
    }

    public static class GoogleAddressLanguage implements Serializable, Parcelable {
        private static final long serialVersionUID = -940363822314963654L;

        @SerializedName("Country")
        public String country;

        @SerializedName("Lang")
        public ArrayList<String> langList;



        public GoogleAddressLanguage(Parcel source) {
            country = source.readString();
            langList = new ArrayList<String>();
            source.readStringList(langList);
//            source.readTypedList(spokenLangList,  String.CREATOR);
        }


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(country);
            dest.writeStringList(langList);

        }

        public static final Parcelable.Creator<GoogleAddressLanguage> CREATOR = new Creator<GoogleAddressLanguage>() {
            public GoogleAddressLanguage createFromParcel(Parcel source) {
                return new GoogleAddressLanguage(source);
            }

            @Override
            public GoogleAddressLanguage[] newArray(int size) {
                return new GoogleAddressLanguage[size];
            }
        };


    }
    public static class CountryCodeInfo implements Serializable, Parcelable {
        @SerializedName("k")
        public String countryShortName;
        @SerializedName("v")
        public String countryCode;

        public CountryCodeInfo(Parcel source) {
            countryShortName = source.readString();
            countryCode = source.readString();
        }


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(countryShortName);
            dest.writeString(countryCode);

        }


        public static final Parcelable.Creator<SpokenLanguage> CREATOR = new Creator<SpokenLanguage>() {
            public SpokenLanguage createFromParcel(Parcel source) {
                return new SpokenLanguage(source);
            }

            @Override
            public SpokenLanguage[] newArray(int size) {
                return new SpokenLanguage[size];
            }
        };


        @Override
        public String toString() {
            return "" + countryShortName;
        }
    }


    public static class SpokenLanguage implements Serializable, Parcelable {
        private static final long serialVersionUID = 1401463434790124267L;

        @SerializedName("Index")
        public int index;

        @SerializedName("Position")
        public int position;

        @SerializedName("Language")
        public String lang;

        @SerializedName("NativeName")
        public String nativeName;


        public SpokenLanguage(Parcel source) {
            index = source.readInt();
            position = source.readInt();
            lang = source.readString();
            nativeName = source.readString();
        }


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(index);
            dest.writeInt(position);
            dest.writeString(lang);
            dest.writeString(nativeName);

        }


        public static final Parcelable.Creator<SpokenLanguage> CREATOR = new Creator<SpokenLanguage>() {
            public SpokenLanguage createFromParcel(Parcel source) {
                return new SpokenLanguage(source);
            }

            @Override
            public SpokenLanguage[] newArray(int size) {
                return new SpokenLanguage[size];
            }
        };

        @Override
        public String toString() {
            return "SpokenLanguage{" +
                    "index=" + index +
                    ", position=" + position +
                    ", lang='" + lang + '\'' +
                    ", nativeName='" + nativeName + '\'' +
                    '}';
        }


        // Comparator
        public static class CompPosition implements Comparator<SpokenLanguage> {
            @Override
            public int compare(SpokenLanguage arg0, SpokenLanguage arg1) {
                return arg0.position - arg1.position;
            }
        }



    }

    public static class SystemLanguage implements Serializable, Parcelable {
        private static final long serialVersionUID = 1311342826857827410L;

        @SerializedName("Index")
        public int index;

        @SerializedName("Position")
        public int position;

        @SerializedName("Language")
        public String lang;

        @SerializedName("Code")
        public String code;

        @SerializedName("NativeName")
        public String nativeName;

        public boolean isEnable=true;


        public SystemLanguage(Parcel source) {
            index = source.readInt();
            position = source.readInt();
            lang = source.readString();
            code = source.readString();
            nativeName = source.readString();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(index);
            dest.writeInt(position);
            dest.writeString(lang);
            dest.writeString(code);
            dest.writeString(nativeName);

        }


        public static final Parcelable.Creator<SystemLanguage> CREATOR = new Creator<SystemLanguage>() {
            public SystemLanguage createFromParcel(Parcel source) {
                return new SystemLanguage(source);
            }

            @Override
            public SystemLanguage[] newArray(int size) {
                return new SystemLanguage[size];
            }
        };


        @Override
        public String toString() {
            return "SystemLanguage{" +
                    "index=" + index +
                    ", position=" + position +
                    ", lang='" + lang + '\'' +
                    ", code='" + code + '\'' +
                    ", nativeName='" + nativeName + '\'' +
                    ", isEnable=" + isEnable +
                    '}';
        }


        // Comparator
        public static class CompPosition implements Comparator<SystemLanguage> {
            @Override
            public int compare( SystemLanguage arg0, SystemLanguage arg1) {
                return arg0.position - arg1.position;
            }
        }

    }

    /////////////////////
    /////////////////////
    /////////////////////

    public static class PropertyType implements Serializable, Parcelable {
        private static final long serialVersionUID = 5235200683662084848L;

        @SerializedName("Index")
        public int index;

        @SerializedName("Position")
        public int position;

        @SerializedName("LanguageIndex")
        public int langIndex;

        @SerializedName("PropertyTypeName")
        public String propertyTypeName;

        @SerializedName("SpaceTypeList")
        public List<SpaceType> spaceTypeList;

        public PropertyType(Parcel source) {
            index = source.readInt();
            position = source.readInt();
            langIndex = source.readInt();
            propertyTypeName = source.readString();

            spaceTypeList = new ArrayList<SpaceType>();
            source.readTypedList(spaceTypeList, SpaceType.CREATOR);

        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(index);
            dest.writeInt(position);
            dest.writeInt(langIndex);
            dest.writeString(propertyTypeName);
            dest.writeTypedList(spaceTypeList);


        }


        public static final Parcelable.Creator<PropertyType> CREATOR = new Creator<PropertyType>() {
            public PropertyType createFromParcel(Parcel source) {
                return new PropertyType(source);
            }

            @Override
            public PropertyType[] newArray(int size) {
                return new PropertyType[size];
            }
        };


        @Override
        public String toString() {
            return "" + propertyTypeName+ " ["+index +"]";
        }


    }

    //////////////////////////////
    public static class SpaceType implements Serializable, Parcelable {


        private static final long serialVersionUID = 749323534448308389L;

        public int propertyTypeIndex;

        @SerializedName("Index")
        public int index;

        @SerializedName("Position")
        public int position;

        @SerializedName("SpaceTypeName")
        public String spaceTypeName;

        public SpaceType(Parcel source) {
            index = source.readInt();
            position = source.readInt();
            spaceTypeName = source.readString();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(index);
            dest.writeInt(position);
            dest.writeString(spaceTypeName);

        }


        public static final Parcelable.Creator<SpaceType> CREATOR = new Creator<SpaceType>() {
            public SpaceType createFromParcel(Parcel source) {
                return new SpaceType(source);
            }

            @Override
            public SpaceType[] newArray(int size) {
                return new SpaceType[size];
            }
        };

        @Override
        public String toString() {
            return "" + spaceTypeName+ "["+ index+"]";
        }
    }

    public static class Metric implements Serializable, Parcelable {
        private static final long serialVersionUID = -940363822314963895L;

        @SerializedName("Index")
        public int index;

        @SerializedName("Position")
        public int position;

        @SerializedName("LanguageIndex")
        public int langIndex;

        @SerializedName("NativeName")
        public String nativeName;

        public Metric(Parcel source) {
            index = source.readInt();
            position = source.readInt();
            langIndex = source.readInt();
            nativeName = source.readString();
        }


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(index);
            dest.writeInt(position);
            dest.writeInt(langIndex);
            dest.writeString(nativeName);

        }

        public static final Parcelable.Creator<Metric> CREATOR = new Creator<Metric>() {
            public Metric createFromParcel(Parcel source) {
                return new Metric(source);
            }

            @Override
            public Metric[] newArray(int size) {
                return new Metric[size];
            }
        };


        // Comparator
        public static class CompPosition implements Comparator<Metric> {
            @Override
            public int compare(Metric arg0, Metric arg1) {
                return arg0.position - arg1.position;
            }
        }

        @Override
        public String toString() {
            return "" + nativeName;
        }

    }

    public static class Slogan implements Serializable,Parcelable {

        @SerializedName("Index")
        public int index;
        @SerializedName("Position")
        public int position;
        @SerializedName("LanguageIndex")
        public int langIndex;
        @SerializedName("Slogan")
        public String slogan;

        public Slogan(Parcel source) {
            index = source.readInt();
            position = source.readInt();
            langIndex = source.readInt();
            slogan = source.readString();
        }


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(index);
            dest.writeInt(position);
            dest.writeInt(langIndex);
            dest.writeString(slogan);

        }

        public static final Parcelable.Creator<Slogan> CREATOR = new Creator<Slogan>() {
            public Slogan createFromParcel(Parcel source) {
                return new Slogan(source);
            }

            @Override
            public Slogan[] newArray(int size) {
                return new Slogan[size];
            }
        };

        @Override
        public String toString() {
            return slogan;
        }
    }


    public static class TopFiveCities implements Serializable,Parcelable {

        @SerializedName("CityName")
        public String cityName;
        @SerializedName("AgentCount")
        public int agentCount;
        @SerializedName("PlaceID")
        public String placeId;

        public TopFiveCities(Parcel source) {
            cityName = source.readString();
            agentCount = source.readInt();
            placeId = source.readString();
        }


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(cityName);
            dest.writeInt(agentCount);
            dest.writeString(placeId);

        }

        public static final Parcelable.Creator<TopFiveCities> CREATOR = new Creator<TopFiveCities>() {
            public TopFiveCities createFromParcel(Parcel source) {
                return new TopFiveCities(source);
            }

            @Override
            public TopFiveCities[] newArray(int size) {
                return new TopFiveCities[size];
            }
        };

    }



}
