package co.real.productionreal2.view;

import android.content.Context;
import android.graphics.drawable.StateListDrawable;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import co.real.productionreal2.R;
import co.real.productionreal2.model.SystemSetting;
import co.real.productionreal2.util.AppUtil;

/**
 * Created by kelvinsun on 30/12/15.
 */
public class SpaceTypeItem extends LinearLayout {


    private ImageView ivSpaceType;
    private TextView tvSpaceType;

    private SystemSetting.SpaceType spaceType;

    private int mPropertyTypeIndex = 0;
    private int mSpaceTypeIndex = 0;
    private Context context;

    public SpaceTypeItem(Context context, SystemSetting.SpaceType spaceType) {
        super(context);
        this.context = context;
        this.spaceType = spaceType;
        initView();
    }

    public SpaceTypeItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initView();
    }

    private void initView() {
        inflate(getContext(), R.layout.view_horizontal_list_item, this);

        // find views
        tvSpaceType = (TextView) findViewById(R.id.title);
        ivSpaceType = (ImageView) findViewById(R.id.image);
        addListener();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
    }

    private void addListener() {
//        setOnClickListener(mOnButtonClicked);
    }


    public void updateUI(SystemSetting.SpaceType spaceType) {

        this.spaceType = spaceType;
        mPropertyTypeIndex = spaceType.propertyTypeIndex;
        mSpaceTypeIndex = spaceType.index;

        tvSpaceType.setText(spaceType.spaceTypeName);

        String imageName = AppUtil.getPropertyTypeImageName(mPropertyTypeIndex, mSpaceTypeIndex);

        String imagePressed = imageName + "_on";
        String imageNormal = imageName + "_off";

        StateListDrawable states = new StateListDrawable();
        states.addState(new int[]{android.R.attr.state_pressed},
                context.getResources().getDrawable(context.getResources().getIdentifier(imagePressed, "drawable", context.getPackageName())));
        states.addState(new int[]{android.R.attr.state_focused},
                context.getResources().getDrawable(context.getResources().getIdentifier(imagePressed, "drawable", context.getPackageName())));
        states.addState(new int[]{android.R.attr.state_selected},
                context.getResources().getDrawable(context.getResources().getIdentifier(imagePressed, "drawable", context.getPackageName())));
        states.addState(new int[]{},
                context.getResources().getDrawable(context.getResources().getIdentifier(imageNormal, "drawable", context.getPackageName())));
        ivSpaceType.setImageDrawable(states);

        ivSpaceType.setTag(spaceType.spaceTypeName);
    }

    public int getmSpaceTypeIndex() {
        return mSpaceTypeIndex;
    }

    public void setmSpaceTypeIndex(int mSpaceTypeIndex) {
        this.mSpaceTypeIndex = mSpaceTypeIndex;
    }

    public int getmPropertyTypeIndex() {
        return mPropertyTypeIndex;
    }

    public void setmPropertyTypeIndex(int mPropertyTypeIndex) {
        this.mPropertyTypeIndex = mPropertyTypeIndex;
    }

}
