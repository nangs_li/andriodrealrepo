package co.real.productionreal2.service.model.request;

import com.google.gson.annotations.SerializedName;

public class RequestQBIDUpdate extends RequestBase{

	public RequestQBIDUpdate(int memId, String session) {
		super(memId, session);		
	}


	
	@SerializedName("QBID")
	public String QBID;
	
	

	
}
