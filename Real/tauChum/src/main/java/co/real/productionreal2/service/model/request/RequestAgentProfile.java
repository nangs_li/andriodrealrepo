package co.real.productionreal2.service.model.request;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import co.real.productionreal2.callback.ApiCallback;
import co.real.productionreal2.model.InputRequest;
import co.real.productionreal2.service.AdminService;
import co.real.productionreal2.service.model.AgentProfiles;
import co.real.productionreal2.service.model.ApiResponse;
import co.real.productionreal2.service.model.response.ResponseAgentProfile;
import co.real.productionreal2.util.NetworkUtil;
import co.real.productionreal2.view.Dialog;

import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by kelvinsun on 12/4/16.
 */
public class RequestAgentProfile extends RequestBase {
    @SerializedName("ClosedListingCount")
    public int closedListingCount;

    public RequestAgentProfile(int memId, String session) {
        super(memId, session);
    }


    public RequestAgentProfile(int memId, String session, int closedListingCount) {
        super(memId, session);
        this.closedListingCount = closedListingCount;
    }

    public void getAgentProfile(final Context context, final RequestBase request, final ApiCallback callback) {

        String json = new Gson().toJson(request);

        final AdminService weatherService = new AdminService(context);

        weatherService.getCoffeeService().agentProfileGet(
                new InputRequest(json),
                new retrofit.Callback<ApiResponse>() {

                    @Override
                    public void failure(RetrofitError error) {
//                        getAgentProfile(context, request, callback);
                        callback.failure("");
                        NetworkUtil.showNoNetworkMsg(context);
                    }

                    @Override
                    public void success(ApiResponse apiResponse, Response response) {
                        Log.d("getAgentProfile", "getAgentProfile success");
                        ResponseAgentProfile agentProfileResponse = new Gson().fromJson(apiResponse.jsonContent, ResponseAgentProfile.class);
                        if (agentProfileResponse.errorCode == 4) {
                            Dialog.accessErrorForceLogoutDialog(context).show();
                        } else if (agentProfileResponse.errorCode == 26) {
                            Dialog.suspendedErrorForceLogoutDialog(context).show();
                        } else if (agentProfileResponse.errorCode != 0 && agentProfileResponse.errorCode != 20 && agentProfileResponse.errorCode != 21 &&
                                agentProfileResponse.errorCode != 22 && agentProfileResponse.errorCode != 23) {
                            callback.failure(agentProfileResponse.errorMsg);
                        } else {
                            callback.success(apiResponse);
                            AgentProfiles agentProfiles = agentProfileResponse.content.profile;
                        }

                    }
                });
    }

}
