package co.real.productionreal2.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

import java.util.List;

/**
 * Created by hohojo on 12/10/2015.
 */
public class Top3ReasonsPagerAdapter extends FragmentStatePagerAdapter {
    private static final String TAG = "Top3ReasonsPagerAdapter";
    private int langIndex;

    private FragmentManager mFragmentManager;
    private List<Fragment> fragments;

    public Top3ReasonsPagerAdapter(FragmentManager fragmentManager, List<Fragment> fragments) {
        super(fragmentManager);
        mFragmentManager = fragmentManager;
        this.fragments=fragments;
    }


    public Top3ReasonsPagerAdapter(FragmentManager fm, int langIndex) {
        super(fm);
        this.langIndex = langIndex;
    }

    @Override
    public int getCount() {
        return 3;
    }


    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public Fragment getItem(int position) {
        Log.d(TAG, "langList: langIndex: getItemPosition: " + langIndex);
        switch (position) {
            case 0:
                return fragments.get(0);
            case 1:
                return fragments.get(1);
            case 2:
                return fragments.get(2);
            default:
                return fragments.get(0);
        }
    }



    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }
}
