package widget;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

public class ComputeVerticalScrollOffAbleRecyclerView extends RecyclerView {
    public ComputeVerticalScrollOffAbleRecyclerView(Context context) {
        super(context);
    }

    public ComputeVerticalScrollOffAbleRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ComputeVerticalScrollOffAbleRecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public int computeVerticalScrollOffset() {
        return super.computeVerticalScrollOffset();
    }
}