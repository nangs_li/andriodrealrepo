package widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.RelativeLayout;

/**
 * Created by edwinchan on 22/1/16.
 */
public class InterceptableRelativeLayout extends RelativeLayout {

    InterceptableListener listener;

    public InterceptableRelativeLayout(Context context) {
        super(context);
    }

    public InterceptableRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public InterceptableRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if(listener != null){
            return listener.onInterceptTouchEvent(ev);
        }
        return super.onInterceptTouchEvent(ev);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        if(listener != null){
            return listener.onTouchEvent(ev);
        }
        return super.onTouchEvent(ev);
    }

    public void setInterceptableListener(InterceptableListener myInterceptableListener){
        listener = myInterceptableListener;

    }

}
