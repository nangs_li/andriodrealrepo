package widget;

import android.view.MotionEvent;

/**
 * Created by edwinchan on 22/1/16.
 */
public interface InterceptableListener {

    public boolean onInterceptTouchEvent(MotionEvent motionEvent);
    public boolean onTouchEvent(MotionEvent motionEvent);

}
