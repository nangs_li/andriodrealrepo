package com.hohojo.tauchum;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;

/**
 * Created by hohojo on 20/7/2015.
 */
public class RealConfigDaoGenerator {
    private static final String PROJECT_DIR = System.getProperty("user.dir").replace("\\", "/");
    private static final String OUT_DIR = PROJECT_DIR + "/tauchum/src/main/java";

    public static void main(String args[]) throws Exception {
        Schema schema = new Schema(1, "co.real.productionreal2.dao.config");

        addTables(schema);

        new DaoGenerator().generateAll(schema, OUT_DIR);
    }

    public static void addTables(Schema schema) {
        Entity main = addMain(schema);
        Entity currentFollower = addCurrentFollower(schema);
    }

    public static Entity addMain(Schema schema) {
        Entity main = schema.addEntity("DBMain");
        main.addIdProperty().primaryKey().autoincrement();
        main.addIntProperty("MemberID");
        main.addBooleanProperty("status");
        return main;
    }

    public static Entity addCurrentFollower(Schema schema) {
        Entity currentFollower = schema.addEntity("DBCurrentFollower");
        currentFollower.addIdProperty().primaryKey().autoincrement();
        currentFollower.addIntProperty("MemberID");
        currentFollower.addIntProperty("FollowingMemberID");
        currentFollower.addBooleanProperty("IsFollowing");
        return currentFollower;
    }
}
