package com.hohojo.tauchum;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Index;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.Schema;
import de.greenrobot.daogenerator.ToMany;

/**
 * @author Octa
 */
public class RealDaoGenerator {

    private static final String PROJECT_DIR = System.getProperty("user.dir").replace("\\", "/");
    private static final String OUT_DIR = PROJECT_DIR + "/tauchum/src/main/java";

    public static void main(String args[]) throws Exception {
        Schema schema = new Schema(4, "co.real.productionreal2.dao.account");

        addTables(schema);

        new DaoGenerator().generateAll(schema, OUT_DIR);
    }

    private static void addTables(Schema schema) {


        Entity chatMessage = addQBChatMessage(schema);
        Entity chatDialog = addChatDialog(schema);
        Entity qbUser = addQBUser(schema);
        Entity chatroomAgentProfiles = addChatroomAgentProfiles(schema);
        Entity logInfo = addLogInfo(schema);
        Entity phoneBook=addPhonebook(schema);
//        Entity searchAgentProfileArray = addSearchAgentProfileArray(schema);


        /* properties */

//        Property searchAgentProfileArrayIdForChatroomAgentProfiles = chatroomAgentProfiles.addLongProperty("searchAgentProfileArrayID").notNull().getProperty();
//        Property searchAgentProfileArrayIdForQbUser = qbUser.addLongProperty("searchAgentProfileArrayID").notNull().getProperty();
//
        Property qbUserIdForChatDialog = chatDialog.addLongProperty("qbUserID").notNull().getProperty();
        Property qbUserIdForChatroomAgentProfiles = chatroomAgentProfiles.addLongProperty("qbUserID").notNull().getProperty();
        Property chatroomAgentProfilesIdForQbUser= qbUser.addLongProperty("chatroomAgentProfilesId").notNull().getProperty();
//
//        Property chatDialogIdForDidReadMessage = didReadMessage.addLongProperty("chatDialogID").notNull().getProperty();
//        Property chatDialogIdForDidDeliverMessage = didDeliverMessage.addLongProperty("chatDialogID").notNull().getProperty();
//        Property chatDialogIdForChatMessage = chatMessage.addLongProperty("chatDialogID").notNull().getProperty();


        /* relationships between entities */

//        ToMany searchAgentProfileArrayToChatroomAgentProfiles = searchAgentProfileArray.addToMany(chatroomAgentProfiles, searchAgentProfileArrayIdForChatroomAgentProfiles);
//        searchAgentProfileArrayToChatroomAgentProfiles.setName("chatroomAgentProfiles");
//        ToMany searchAgentProfileArrayToQbUser = searchAgentProfileArray.addToMany(qbUser, searchAgentProfileArrayIdForQbUser);
//        searchAgentProfileArrayToQbUser.setName("qbUsers");
//
        chatroomAgentProfiles.addToOne(qbUser, qbUserIdForChatroomAgentProfiles, "qbUsers");
        qbUser.addToOne(chatroomAgentProfiles, chatroomAgentProfilesIdForQbUser, "chatroomAgentProfiles");
//
        ToMany qbUserToChatDialogs = qbUser.addToMany(chatDialog, qbUserIdForChatDialog);
        qbUserToChatDialogs.setName("chatDialogs");
//
//        ToMany chatDialogToDidReadMessage = chatDialog.addToMany(didReadMessage, chatDialogIdForDidReadMessage);
//        chatDialogToDidReadMessage.setName("didReadMessages");
//        ToMany chatDialogToDidDeliverMessage = chatDialog.addToMany(didDeliverMessage, chatDialogIdForDidDeliverMessage);
//        chatDialogToDidDeliverMessage.setName("didDeliverMessages");
//        ToMany chatDialogToChatMessage = chatDialog.addToMany(chatMessage, chatDialogIdForChatMessage);
//        chatDialogToChatMessage.setName("chatMessages");

    }


    private static Entity addQBChatMessage(Schema schema) {
        Entity chatMessage = schema.addEntity("DBQBChatMessage");
        Property id = chatMessage.addIdProperty().primaryKey().autoincrement().getProperty();
        Property messageId = chatMessage.addStringProperty("MessageID").getProperty();
        Property chatDialogID = chatMessage.addStringProperty("ChatDialogID").getProperty();
        Property messageType = chatMessage.addStringProperty("MessageType").getProperty();
        Property messageCategory = chatMessage.addStringProperty("MessageCategory").getProperty();
        chatMessage.addIntProperty("MemberID");
        chatMessage.addIntProperty("RecipientID");
        Property senderID = chatMessage.addIntProperty("SenderID").getProperty();
        Property dateSent = chatMessage.addStringProperty("DateSent").getProperty();
        chatMessage.addStringProperty("InsertDBDateTime");
        //QBChatMessage json
        chatMessage.addStringProperty("QBChatMessage");
        chatMessage.addStringProperty("PhotoURL");
        chatMessage.addStringProperty("PhotoLocalPath");
        Property photoName = chatMessage.addStringProperty("PhotoName").getProperty();
        chatMessage.addStringProperty("Body");
        Property bindingAgentListingId = chatMessage.addStringProperty("bindingAgentListingId").getProperty();
        chatMessage.addBooleanProperty("PhotoType");
        // new, sending, sent, sentUser, read. error
        Property senderStatus = chatMessage.addStringProperty("SendStatus").getProperty();
        //recevied , sentRead, error
        Property receiveStatus = chatMessage.addStringProperty("ReceiveStatus").getProperty();
        chatMessage.addBooleanProperty("EnableStatus");
        chatMessage.addBooleanProperty("EnableReadMsg");
        Property isUnreadFirstMessage = chatMessage.addBooleanProperty("IsUnreadFirstMessage").getProperty();
        chatMessage.addStringProperty("AgentListing");
        Property isDeleted = chatMessage.addBooleanProperty("IsDeleted").getProperty();
        chatMessage.addBooleanProperty("IsEncrypted");

        Index index = new Index();
        index.addProperty(chatDialogID);
        index.addPropertyAsc(id);
        chatMessage.addIndex(index);

        index = new Index();
        index.addProperty(chatDialogID);
        index.addProperty(messageCategory);
        index.addPropertyAsc(id);
        chatMessage.addIndex(index);

        index = new Index();
        index.addProperty(chatDialogID);
        index.addProperty(isUnreadFirstMessage);
        index.addPropertyAsc(id);
        chatMessage.addIndex(index);

        index = new Index();
        index.addProperty(chatDialogID);
        index.addProperty(id);
        index.addProperty(messageCategory);
        index.addPropertyDesc(id);
        chatMessage.addIndex(index);

        index = new Index();
        index.addProperty(chatDialogID);
        index.addProperty(messageCategory);
        index.addPropertyDesc(id);
        chatMessage.addIndex(index);

        index = new Index();
        index.addProperty(chatDialogID);
        index.addProperty(messageCategory);
        index.addProperty(id);
        index.addProperty(bindingAgentListingId);
        index.addPropertyDesc(id);
        chatMessage.addIndex(index);

        index = new Index();
        index.addProperty(chatDialogID);
        index.addProperty(messageCategory);
        index.addProperty(bindingAgentListingId);
        index.addPropertyDesc(id);
        chatMessage.addIndex(index);

        index = new Index();
        index.addProperty(chatDialogID);
        index.addProperty(isDeleted);
        index.addProperty(id);
        index.addPropertyAsc(id);
        chatMessage.addIndex(index);

        index = new Index();
        index.addProperty(chatDialogID);
        index.addProperty(isDeleted);
        index.addPropertyAsc(id);
        chatMessage.addIndex(index);

        index = new Index();
        index.addProperty(messageId);
        chatMessage.addIndex(index);

        index = new Index();
        index.addProperty(chatDialogID);
        index.addProperty(messageType);
        index.addProperty(senderStatus);
        index.addPropertyAsc(dateSent);
        chatMessage.addIndex(index);

        index = new Index();
        index.addProperty(chatDialogID);
        index.addProperty(messageCategory);
        index.addProperty(messageType);
        index.addProperty(receiveStatus);
        index.addPropertyAsc(dateSent);
        chatMessage.addIndex(index);

        index = new Index();
        index.addProperty(senderStatus);
        index.addProperty(messageType);
        index.addProperty(chatDialogID);
        index.addPropertyAsc(dateSent);
        chatMessage.addIndex(index);

        index = new Index();
        index.addProperty(chatDialogID);
        index.addProperty(senderID);
        index.addPropertyDesc(messageId);
        chatMessage.addIndex(index);

        return chatMessage;
    }

    private static Entity addChatDialog(Schema schema) {
        Entity chatDialog = schema.addEntity("DBQBChatDialog");
        chatDialog.addIdProperty().primaryKey().autoincrement();
        chatDialog.addStringProperty("DialogID");
        chatDialog.addStringProperty("LastMessageText");
//        chatDialog.addIntProperty("MemberID");
        chatDialog.addStringProperty("LastMessageDate");
        //QBChatDialog json
        chatDialog.addStringProperty("QBChatDialog");
//        chatDialog.addBooleanProperty("IsAgent");
        chatDialog.addIntProperty("LastMessageUserID");
        chatDialog.addIntProperty("Blocked");
        chatDialog.addIntProperty("Muted");
        chatDialog.addIntProperty("Deleted");
        chatDialog.addIntProperty("Exited");
        chatDialog.addIntProperty("BlockedOpponent");
        chatDialog.addIntProperty("MutedOpponent");
        chatDialog.addIntProperty("DeletedOpponent");
        chatDialog.addIntProperty("ExitedOpponent");
        chatDialog.addIntProperty("UnReadMessageCount");
        chatDialog.addBooleanProperty("NeedInit");
        chatDialog.addBooleanProperty("IsEncrypted");

//        chatDialog.addBooleanProperty("QBSuspend");
//        chatDialog.addBooleanProperty("RealSuspend");

        return chatDialog;
    }

//    private static Entity addDidReadMessage(Schema schema) {
//        Entity didReadMessage = schema.addEntity("DBDidReadMessage");
//        didReadMessage.addIdProperty().primaryKey().autoincrement();
//        didReadMessage.addIntProperty("MemberID");
//        didReadMessage.addStringProperty("Date");
//
//        return didReadMessage;
//    }
//
//    private static Entity addDidDeliverMessage(Schema schema) {
//        Entity didDeliverMessage = schema.addEntity("DBDidDeliverMessage");
//        didDeliverMessage.addIdProperty().primaryKey().autoincrement();
//        didDeliverMessage.addIntProperty("MemberID");
//        didDeliverMessage.addStringProperty("Date");
//
//        return didDeliverMessage;
//    }

    private static Entity addQBUser(Schema schema) {
        Entity qbUser = schema.addEntity("DBQBUser");
        qbUser.addIdProperty().primaryKey().autoincrement();
        qbUser.addIntProperty("UserID");
        qbUser.addIntProperty("MemberID");
//        qbUser.addStringProperty("Date");
        qbUser.addStringProperty("Name");
        //QBUser json
        qbUser.addStringProperty("QBUser");
//        qbUser.addBooleanProperty("IsAgent");
        qbUser.addIntProperty("EnableStatus");
        qbUser.addIntProperty("EnableReadMsg");

        qbUser.addBooleanProperty("isHidden");
        qbUser.addBooleanProperty("isClear");

        return qbUser;
    }

    private static Entity addChatroomAgentProfiles(Schema schema) {
        Entity chatRoomAgentProfiles = schema.addEntity("DBChatroomAgentProfiles");
        chatRoomAgentProfiles.addIdProperty().primaryKey().autoincrement();
        Property memberID = chatRoomAgentProfiles.addIntProperty("MemberID").getProperty();
//        chatRoomAgentProfiles.addStringProperty("Date");
        //ChatroomAgentProfiles json
        chatRoomAgentProfiles.addStringProperty("ChatroomAgentProfiles");
        chatRoomAgentProfiles.addBooleanProperty("IsAgent");
        Property qbId = chatRoomAgentProfiles.addStringProperty("QBID").getProperty();
        chatRoomAgentProfiles.addIntProperty("FollowerCount");
        chatRoomAgentProfiles.addIntProperty("FollowingCount");
        chatRoomAgentProfiles.addIntProperty("IsFollowing");
        chatRoomAgentProfiles.addLongProperty("LatUpdateTime");

        Index index = new Index();
        index.addProperty(qbId);
        chatRoomAgentProfiles.addIndex(index);

        index = new Index();
        index.addProperty(memberID);
        chatRoomAgentProfiles.addIndex(index);

        return chatRoomAgentProfiles;
    }

    private static Entity addLogInfo(Schema schema) {
        Entity logInfo = schema.addEntity("DBLogInfo");
        logInfo.addIdProperty().primaryKey().autoincrement();
        logInfo.addIntProperty("MemberID");
//        logInfo.addStringProperty("Date");
        logInfo.addIntProperty("qbUserId");
        logInfo.addStringProperty("qbUser");
        //LogInfo json
        logInfo.addStringProperty("LogInfo");
        logInfo.addBooleanProperty("EnableStatus");
        logInfo.addBooleanProperty("EnableReadMsg");
        logInfo.addBooleanProperty("PhotoAutoDownload");
        logInfo.addIntProperty("MetricIndex");


        return logInfo;
    }

//    private static Entity addSearchAgentProfileArray(Schema schema) {
//        Entity searchAgentProfileArray = schema.addEntity("DBSearchAgentProfileArray");
//        searchAgentProfileArray.addIdProperty().primaryKey().autoincrement();
//        searchAgentProfileArray.addIntProperty("MemberID");
//        searchAgentProfileArray.addStringProperty("Date");
//        //SearchAgentProfileArray json
//        searchAgentProfileArray.addStringProperty("SearchAgentProfileArray");
//
//        return searchAgentProfileArray;
//    }

    private static Entity addPhonebook(Schema schema) {
        Entity phoneBook = schema.addEntity("DBPhoneBook");
        phoneBook.addIdProperty().primaryKey().autoincrement();
        phoneBook.addLongProperty("phoneNumber").unique();
        phoneBook.addStringProperty("countryCode");
        phoneBook.addStringProperty("name");
        phoneBook.addBooleanProperty("hasSent");
        phoneBook.addBooleanProperty("hasChecked");

        return phoneBook;
    }

}
